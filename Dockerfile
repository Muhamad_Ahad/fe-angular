# Stage 0, "build-stage", based on Node.js, to build and compile the frontend
FROM node:12 as build-stage
WORKDIR /app
COPY ./angular-admin-sig-v2.0 /app/
RUN npm install
RUN npm uninstall @types/googlemaps
RUN npm install @types/googlemaps@3.39.12
RUN npm uninstall ng-otp-input
RUN npm install ng-otp-input@1.7.0
RUN npm run build

# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx
#Copy ci-dashboard-dist
COPY --from=build-stage /app/dist/ /usr/share/nginx/html
#Copy default nginx configuration
COPY ./config/nginx-custom.conf /etc/nginx/conf.d/default.conf
