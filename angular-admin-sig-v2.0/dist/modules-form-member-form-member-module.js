(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-form-member-form-member-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/form-member/add-bulk/form-member.add-bulk.component.html":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/form-member/add-bulk/form-member.add-bulk.component.html ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n\r\n    <app-page-header [heading]=\"'Bulk Update Member'\" [icon]=\"'fa-table'\"></app-page-header>\r\n\r\n    <div>\r\n        <div class=\"card card-detail\">\r\n            <div class=\"card-header\">\r\n                <!-- <button class=\"btn save_button\" (click)=\"formSubmitAddMember()\" type=\"submit\"\r\n                    [routerLinkActive]=\"['router-link-active']\"><i class=\"fa fa-fw fa-save\"\r\n                        style=\"transform: translate(-3px, -1px)\"></i>Submit</button> -->\r\n                <button class=\"btn back_button\" (click)=\"backTo()\"><i class=\"fa fa-fw fa-angle-left\"></i>Back</button>\r\n            </div>\r\n\r\n            <div class=\"card-content\">\r\n                <div class=\"member-detail\">\r\n                    <div class=\"row\">\r\n                        <!-- <div class=\"col-md-6\">\r\n                            <div class=\"card mb-3\">\r\n                                <div class=\"card-header\">\r\n                                    <h2>Data Pemilik &amp; Data Toko</h2>\r\n                                </div>\r\n                                <div class=\"card-content\">\r\n                                    <div class=\"col-md-12\">\r\n\r\n                                        <label>ID Pelanggan</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.id_pel\" [placeholder]=\"'ID Pelanggan'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Nama Toko</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.nama_toko\" [placeholder]=\"'Nama Toko'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Nama Distributor</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.nama_distributor\" [placeholder]=\"'Nama Distributor'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Alamat Toko</label>\r\n                                        <form-input name=\"id\" [type]=\"'textarea'\" [(ngModel)]=\"form.alamat_toko\" [placeholder]=\"'Alamat Toko'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Nama Pemilik</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.nama_pemilik\" [placeholder]=\"'Nama Pemilik'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>No. Telp Pemilik</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.telp_pemilik\" [placeholder]=\"'No. Telp Pemilik'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>No. WA Pemilik</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.no_wa_pemilik\" [placeholder]=\"'No. WA Pemilik'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>No. KTP Pemilik</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.ktp_pemilik\" [placeholder]=\"'No. KTP Pemilik'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>No. NPWP Pemilik</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.npwp_pemilik\" [placeholder]=\"'No. NPWP Pemilik'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Alamat Rumah</label>\r\n                                        <form-input name=\"id\" [type]=\"'textarea'\" [(ngModel)]=\"form.alamat_rumah\" [placeholder]=\"'Alamat Rumah'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Desa/Kelurahan Rumah</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.kelurahan_rumah\" [placeholder]=\"'Desa/Kelurahan Rumah'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Kecamatan Rumah</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.kecamatan_rumah\" [placeholder]=\"'Kecamatan Rumah'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Kabupaten/Kota Rumah</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.kota_rumah\" [placeholder]=\"'Kabupaten/Kota Rumah'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Provinsi Rumah</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.provinsi_rumah\" [placeholder]=\"'Provinsi Rumah'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Kode Pos Rumah</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.kode_pos_rumah\" [placeholder]=\"'Kode Pos Rumah'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <div class=\"form-group\">\r\n                                            <label>Hadiah Dikuasakan</label>\r\n                                            <form-select [(ngModel)]=\"form.hadiah_dikuasakan\" [data]=\"kuasa_option\"></form-select>\r\n                                        </div>\r\n\r\n                                        <div class=\"form-group\">\r\n                                            <label>Group</label>\r\n                                            <form-select [(ngModel)]=\"form.description\" [data]=\"group_desc\"></form-select>\r\n                                        </div>\r\n                                        \r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div> -->\r\n                        <div class=\"col-md-6\">\r\n                            <div class=\"card mb-3\">\r\n                                <input #updateBulk type=\"file\" style=\"display:none\" (change)=\"onFileSelected($event)\"\r\n                                />\r\n\r\n                                <div class=\"card-header\">\r\n                                    <h2>Update Member Bulk</h2>\r\n                                </div>\r\n                                <div class=\"card-content\">\r\n                                    <div class=\"col-md-12\">\r\n                                        <label>Upload Your File Here</label>\r\n                                        <div class=\"member-bulk-update-container\">\r\n                                            <span *ngIf=\"selectedFile && prodOnUpload == false\">{{selectedFile.name}}</span>\r\n                                        </div>\r\n\r\n                                        <div class=\"form-group uploaded\" *ngIf=\"prodOnUpload == false && startUploading == true && cancel == false\">\r\n                                            <div class=\"loading\">\r\n                                              <div class=\"sk-fading-circle\">\r\n                                                <div class=\"sk-circle1 sk-circle\"></div>\r\n                                                <div class=\"sk-circle2 sk-circle\"></div>\r\n                                                <div class=\"sk-circle3 sk-circle\"></div>\r\n                                                <div class=\"sk-circle4 sk-circle\"></div>\r\n                                                <div class=\"sk-circle5 sk-circle\"></div>\r\n                                                <div class=\"sk-circle6 sk-circle\"></div>\r\n                                                <div class=\"sk-circle7 sk-circle\"></div>\r\n                                                <div class=\"sk-circle8 sk-circle\"></div>\r\n                                                <div class=\"sk-circle9 sk-circle\"></div>\r\n                                                <div class=\"sk-circle10 sk-circle\"></div>\r\n                                                <div class=\"sk-circle11 sk-circle\"></div>\r\n                                                <div class=\"sk-circle12 sk-circle\"></div>\r\n                                              </div>\r\n                                            </div>\r\n                                        </div>\r\n\r\n                                        <div class=\"container-two-button\">\r\n                                            <div class=\"img-upload\" *ngIf=\"!selectedFile; else upload_image\">\r\n                                                <button class=\"btn btn-upload\" (click)=\"updateBulk.click()\">\r\n                                                    <i class=\"fa fa-fw fa-upload\"></i>\r\n                                                    <span>upload</span>\r\n                                                </button>\r\n                                            </div>\r\n    \r\n                                            <div class=\"img-upload\" *ngIf=\"selectedFile\">\r\n                                                <button class=\"btn btn-submit\" (click)=\"updateDataBulk()\">\r\n                                                    <i class=\"fa fa-fw fa-save\"></i>\r\n                                                    <span>submit</span>\r\n                                                </button>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                    <ng-template #upload_image>\r\n                                        <button class=\"btn btn-upload\" (click)=\"updateBulk.click()\">\r\n                                            <i class=\"fa fa-fw fa-upload\"></i>\r\n                                            <span>re-upload</span>\r\n                                        </button>\r\n                                    </ng-template>\r\n\r\n                                    <div class=\"col-md-12 col-sm-12 error-msg-upload\">\r\n                                        <div *ngIf=\"errorFile\"><mark>Error: {{errorFile}}</mark></div>                       \r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n\r\n                        </div>\r\n                        \r\n                        <!-- <div class=\"col-md-6\">\r\n                            <div class=\"card mb-3\">\r\n                                <div class=\"card-header\">\r\n                                    <h2>Member &amp; Status Member</h2>\r\n                                </div>\r\n                                <div class=\"card-content\">\r\n                                    <div class=\"col-md-12\">\r\n\r\n                                        <label>Password</label>\r\n                                        <form-input name=\"password\" [type]=\"'password'\" [placeholder]=\"'Password'\"\r\n                                            [(ngModel)]=\"form.password\" autofocus required>\r\n                                        </form-input>\r\n\r\n                                        <label>Email</label>\r\n                                        <form-input name=\"email\" [(ngModel)]=\"form.email\" [type]=\"'text'\" [placeholder]=\"'Email'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Cell Phone</label>\r\n                                        <form-input name=\"cell_phone\" [type]=\"'number'\" [(ngModel)]=\"form.cell_phone\" [placeholder]=\"'Cell Phone'\"\r\n                                            autofocus required>\r\n                                        </form-input>\r\n\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div> -->\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <!-- <form #form=\"ngForm\" (ngSubmit)=\"formSubmitAddMember(form.value)\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-6\">\r\n                        <label>Full Name</label>\r\n                        <form-input name=\"full_name\" [type]=\"'text'\" [placeholder]=\"'Full Name'\" [(ngModel)]=\"full_name\"\r\n                            required></form-input>\r\n\r\n                        <label>Email</label>\r\n                        <form-input name=\"email\" [placeholder]=\"'Email'\" [type]=\"'text'\" [(ngModel)]=\"email\" required>\r\n                        </form-input>\r\n\r\n                        <label>Password</label>\r\n                        <form-input name=\"password\" [placeholder]=\"'Password'\" [type]=\"'password'\"\r\n                            [(ngModel)]=\"password\" required></form-input>\r\n\r\n                    \r\n                        <label>Cell Phone</label>\r\n                        <form-input name=\"cell_phone\" [placeholder]=\"'Cell Phone'\" [type]=\"'number'\"\r\n                            [(ngModel)]=\"cell_phone\" required></form-input>\r\n\r\n                        <label>Date of Birth</label>\r\n                        <div class=\"form-group\">\r\n                            <div class=\"input-group datepicker-input\">\r\n                                <input class=\"form-control\" placeholder=\"yyyy-mm-dd\" name=\"dob\" [(ngModel)]=\"dob\"\r\n                                    ngbDatepicker #d=\"ngbDatepicker\">\r\n                                <button class=\"input-group-addon\" (click)=\"d.toggle()\" type=\"button\">\r\n                                    <span class=\"fa fa-calendar\"></span>\r\n                                </button>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <label>Date of Birth</label>\r\n                        <div class=\"form-group\">\r\n                            <div class=\"input-group datepicker-input\">\r\n                                <input class=\"form-control\" placeholder=\"yyyy-mm-dd\" name=\"dob\" [(ngModel)]=\"dob\"\r\n                                    ngbDatepicker #d=\"ngbDatepicker\">\r\n                                <button class=\"input-group-addon\" (click)=\"d.toggle()\" type=\"button\">\r\n                                    <span class=\"fa fa-calendar\"></span>\r\n                                </button>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <label>City</label>\r\n                        <form-input name=\"mcity\" [placeholder]=\"'City'\" [type]=\"'text'\" [(ngModel)]=\"mcity\" required>\r\n                        </form-input>\r\n                    </div>\r\n\r\n                    <div class=\"col-md-6\">\r\n                        <label>State</label>\r\n                        <form-input name=\"mstate\" [placeholder]=\"'State'\" [type]=\"'text'\" [(ngModel)]=\"mstate\" required>\r\n                        </form-input>\r\n\r\n                        <label>Country</label>\r\n                        <form-input name=\"mcountry\" [placeholder]=\"'Country'\" [type]=\"'text'\" [(ngModel)]=\"mcountry\"\r\n                            required></form-input>\r\n\r\n                        <label>Address</label>\r\n                        <form-input name=\"maddress1\" [placeholder]=\"'Address'\" [type]=\"'textarea'\"\r\n                            [(ngModel)]=\"maddress1\" required></form-input>\r\n\r\n                        <label>Post Code</label>\r\n                        <form-input name=\"mpostcode\" [placeholder]=\"'Post Code'\" [type]=\"'number'\"\r\n                            [(ngModel)]=\"mpostcode\" required></form-input>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label>Gender</label>\r\n                            <select class=\"form-control\" name=\"gender\" [(ngModel)]=\"gender\">\r\n                                <option *ngFor=\"let d of gender_type\" value=\"{{d.value}}\">{{d.label}}</option>\r\n                            </select>\r\n                        </div>\r\n\r\n\r\n                        <label>Marital Status</label>\r\n                        <form-select name=\"marital_status\" [(ngModel)]=\"marital_status\" [data]=\"marital_status_type\">\r\n                        </form-select>\r\n\r\n                        <label>Member Type</label>\r\n                        <form-select name=\"type\" [(ngModel)]=\"type\" [data]=\"member_type\"></form-select>\r\n                    </div>\r\n                </div>\r\n\r\n            </form> -->\r\n        </div>\r\n    </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/form-member/add/form-member.add.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/form-member/add/form-member.add.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n\r\n    <app-page-header [heading]=\"'Add New Member'\" [icon]=\"'fa-table'\"></app-page-header>\r\n\r\n    <div *ngIf=\"!showPasswordPage\">\r\n        <div class=\"card card-detail\">\r\n            <div class=\"card-header\">\r\n                <!-- <button class=\"btn save_button\" (click)=\"formSubmitAddMember()\" type=\"submit\"\r\n                    [routerLinkActive]=\"['router-link-active']\"><i class=\"fa fa-fw fa-save\"\r\n                        style=\"transform: translate(-3px, -1px)\"></i>Submit Single Member</button> -->\r\n                <button class=\"btn back_button\" (click)=\"backTo()\"><i class=\"fa fa-fw fa-angle-left\"></i>Back</button>\r\n            </div>\r\n\r\n            <div class=\"card-content\">\r\n                <div class=\"member-detail\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-6\">\r\n                            <div class=\"card mb-3\">\r\n                                <div class=\"card-header\">\r\n                                    <h2 *ngIf=\"programType != 'custom_kontraktual'\">Data Pemilik &amp; Data Toko</h2>\r\n                                    <h2 *ngIf=\"programType == 'custom_kontraktual'\">Data Entitas</h2>\r\n                                </div>\r\n                                <div class=\"card-content\">\r\n                                    <div class=\"col-md-12\">\r\n\r\n                                        <label>ID Pelanggan <span class=\"star-required\">*</span></label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.id_pel\" [placeholder]=\"'ID Pelanggan'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n                                        <div class=\"text-required\" *ngIf=\"warnIDPelanggan\">ID Pelanggan wajib diisi</div>\r\n\r\n                                        <div *ngIf=\"programType == 'reguler'\">\r\n                                            <label>Program/Group</label>\r\n                                            <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.description\" [placeholder]=\"'Program/Group'\" autofocus\r\n                                                required>\r\n                                            </form-input>\r\n                                        </div>\r\n                                        \r\n                                        <label *ngIf=\"programType == 'custom_kontraktual'\">Nama PIC <span class=\"star-required\">*</span></label>\r\n                                        <label *ngIf=\"programType != 'custom_kontraktual'\">Nama Pemilik <span class=\"star-required\">*</span></label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.nama_pemilik\" [placeholder]=\"'Nama Pemilik'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n                                        <div class=\"text-required\" *ngIf=\"warnNamaPemilik && programType == 'custom_kontraktual'\">Nama PIC wajib diisi</div>\r\n                                        <div class=\"text-required\" *ngIf=\"warnNamaPemilik && programType != 'custom_kontraktual'\">Nama Pemilik wajib diisi</div>\r\n\r\n                                        <label *ngIf=\"programType == 'custom_kontraktual'\">No. Telp PIC <span class=\"star-required\">*</span></label>\r\n                                        <label *ngIf=\"programType != 'custom_kontraktual'\">No. Telp Pemilik <span class=\"star-required\">*</span></label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.telp_pemilik\" [placeholder]=\"'No. Telp Pemilik'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n                                        <div class=\"text-required\" *ngIf=\"warnTelpPemilik && programType == 'custom_kontraktual'\">No. Telp PIC wajib diisi</div>\r\n                                        <div class=\"text-required\" *ngIf=\"warnTelpPemilik && programType != 'custom_kontraktual'\">No. Telp Pemilik wajib diisi</div>\r\n\r\n                                        <label *ngIf=\"programType == 'custom_kontraktual'\">No. WA PIC</label>\r\n                                        <label *ngIf=\"programType != 'custom_kontraktual'\">No. WA Pemilik</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.no_wa_pemilik\" [placeholder]=\"'No. WA Pemilik'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n                                        <div class=\"text-required\" *ngIf=\"warnWAPemilik && programType == 'custom_kontraktual'\">No. WA PIC wajib diisi</div>\r\n                                        <div class=\"text-required\" *ngIf=\"warnWAPemilik && programType != 'custom_kontraktual'\">No. WA Pemilik wajib diisi</div>\r\n\r\n                                        <div *ngIf=\"programType == 'reguler'\">\r\n                                            <label>Email Pelanggan</label>\r\n                                            <form-input name=\"email\" [type]=\"'text'\" [(ngModel)]=\"form.email\" [placeholder]=\"'Email Pelanggan'\" autofocus\r\n                                                required>\r\n                                            </form-input>\r\n                                        </div>\r\n\r\n                                        <div *ngIf=\"programType != 'custom_kontraktual'\">\r\n                                            <label>Nama Toko</label>\r\n                                            <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.nama_toko\" [placeholder]=\"'Nama Toko'\" autofocus\r\n                                                required>\r\n                                            </form-input>\r\n                                        </div>\r\n\r\n                                        <div *ngIf=\"programType == 'custom_kontraktual'\">\r\n                                            <label>Nama Entitas</label>\r\n                                            <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.nama_toko\" [placeholder]=\"'Nama Entitas'\" autofocus\r\n                                                required>\r\n                                            </form-input>\r\n                                        </div>\r\n\r\n                                        <label>Nama Distributor</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.nama_distributor\" [placeholder]=\"'Nama Distributor'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <div *ngIf=\"programType != 'custom_kontraktual'\">\r\n                                            <label>Alamat Toko</label>\r\n                                            <form-input name=\"id\" [type]=\"'textarea'\" [(ngModel)]=\"form.alamat_toko\" [placeholder]=\"'Alamat Toko'\" autofocus\r\n                                                required>\r\n                                            </form-input>\r\n                                        </div>\r\n\r\n                                        <div *ngIf=\"programType == 'custom_kontraktual'\">\r\n                                            <label>Alamat Entitas</label>\r\n                                            <form-input name=\"id\" [type]=\"'textarea'\" [(ngModel)]=\"form.alamat_toko\" [placeholder]=\"'Alamat Entitas'\" autofocus\r\n                                                required>\r\n                                            </form-input>\r\n                                        </div>\r\n\r\n                                        <!-- <label>No. KTP Pemilik</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.ktp_pemilik\" [placeholder]=\"'No. KTP Pemilik'\" autofocus\r\n                                            required>\r\n                                        </form-input> -->\r\n\r\n                                        <!-- <label>No. NPWP Pemilik</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.npwp_pemilik\" [placeholder]=\"'No. NPWP Pemilik'\" autofocus\r\n                                            required>\r\n                                        </form-input> -->\r\n\r\n                                        <label>Alamat Rumah</label>\r\n                                        <form-input name=\"id\" [type]=\"'textarea'\" [(ngModel)]=\"form.alamat_rumah\" [placeholder]=\"'Alamat Rumah'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Provinsi Rumah</label>\r\n                                        <!-- <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.provinsi_rumah\" [placeholder]=\"'Provinsi Rumah'\" autofocus\r\n                                            required>\r\n                                        </form-input> -->\r\n\r\n                                        <div class=\"ng-autocomplete\">\r\n                                            <ng-autocomplete \r\n                                              [data]=\"dataProvince\"\r\n                                              [(ngModel)]=\"form.provinsi_rumah\"\r\n                                              [searchKeyword]=\"keywordProvince\"\r\n                                              (selected)='selectEvent($event, \"province\")'\r\n                                              (inputChanged)='inputRegion($event, \"province\")'\r\n                                              [itemTemplate]=\"templateProvince\"\r\n                                              [placeHolder]=\"placeholderProvince\"\r\n                                              [isLoading]=\"loadingProvince\"\r\n                                              >                                 \r\n                                            </ng-autocomplete>\r\n                                            \r\n                                            <ng-template #templateProvince let-item>\r\n                                                <a [innerHTML]=\"item.province\"></a>\r\n                                            </ng-template>\r\n                                        </div>\r\n\r\n                                        <label>Kabupaten/Kota Rumah</label>\r\n                                        <!-- <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.kota_rumah\" [placeholder]=\"'Kabupaten/Kota Rumah'\" autofocus\r\n                                            required>\r\n                                        </form-input> -->\r\n                                        <div class=\"ng-autocomplete\">\r\n                                            <ng-autocomplete \r\n                                              [data]=\"dataCity\"\r\n                                              [(ngModel)]=\"form.kota_rumah\"\r\n                                              [searchKeyword]=\"keywordCity\"\r\n                                              (selected)='selectEvent($event, \"city\")'\r\n                                              (inputChanged)='inputRegion($event, \"city\")'\r\n                                              [itemTemplate]=\"templateCity\"\r\n                                              [placeHolder]=\"placeholderCity\"\r\n                                              [isLoading]=\"loadingCity\"\r\n                                              [disabled] = \"programType != 'reguler' && !province_code\">                                 \r\n                                            </ng-autocomplete>\r\n                                            \r\n                                            <ng-template #templateCity let-item>\r\n                                                <a [innerHTML]=\"item.city\"></a>\r\n                                            </ng-template>\r\n                                        </div>\r\n\r\n                                        <label>Kecamatan Rumah</label>\r\n                                        <!-- <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.kecamatan_rumah\" [placeholder]=\"'Kecamatan Rumah'\" autofocus\r\n                                            required>\r\n                                        </form-input> -->\r\n                                        <div class=\"ng-autocomplete\">\r\n                                            <ng-autocomplete \r\n                                              [data]=\"dataSubDistrict\"\r\n                                              [(ngModel)]=\"form.kecamatan_rumah\"\r\n                                              [searchKeyword]=\"keywordSubDistrict\"\r\n                                              (selected)='selectEvent($event, \"subdistrict\")'\r\n                                              (inputChanged)='inputRegion($event, \"subdistrict\")'\r\n                                              [itemTemplate]=\"templateSubDistrict\"\r\n                                              [placeHolder]=\"placeholderSubDistrict\"\r\n                                              [isLoading]=\"loadingSubDistrict\"\r\n                                              [disabled] = \"programType != 'reguler' && !city_code\">                                 \r\n                                            </ng-autocomplete>\r\n                                            \r\n                                            <ng-template #templateSubDistrict let-item>\r\n                                                <a [innerHTML]=\"item.subdistrict\"></a>\r\n                                            </ng-template>\r\n                                        </div>\r\n\r\n                                        <label>Desa/Kelurahan Rumah</label>\r\n                                        <!-- <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.kelurahan_rumah\" [placeholder]=\"'Desa/Kelurahan Rumah'\" autofocus\r\n                                            required>\r\n                                        </form-input> -->\r\n                                        <div class=\"ng-autocomplete\">\r\n                                            <ng-autocomplete \r\n                                              [data]=\"dataVillage\"\r\n                                              [(ngModel)]=\"form.kelurahan_rumah\"\r\n                                              [searchKeyword]=\"keywordVillage\"\r\n                                              (selected)='selectEvent($event, \"village\")'\r\n                                              (inputChanged)='inputRegion($event, \"village\")'\r\n                                              [itemTemplate]=\"templateVillage\"\r\n                                              [placeHolder]=\"placeholderVillage\"\r\n                                              [isLoading]=\"loadingVillage\"\r\n                                              [disabled] = \"programType != 'reguler' && !subdistrict_code\">                                 \r\n                                            </ng-autocomplete>\r\n                                            \r\n                                            <ng-template #templateVillage let-item>\r\n                                                <a [innerHTML]=\"item.village\"></a>\r\n                                            </ng-template>\r\n                                        </div>\r\n\r\n                                        <label>Kode Pos Rumah</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.kode_pos_rumah\" [placeholder]=\"'Kode Pos Rumah'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <div class=\"form-group\">\r\n                                            <label>Hadiah Dikuasakan</label>\r\n                                            <form-select [(ngModel)]=\"form.hadiah_dikuasakan\" [data]=\"kuasa_option\"></form-select>\r\n                                        </div>\r\n\r\n                                        <div class=\"form-group\" *ngIf=\"programType != 'reguler' && programType != 'custom_kontraktual'\">\r\n                                            <label>Group</label>\r\n                                            <form-select [(ngModel)]=\"form.description\" [data]=\"group_desc\"></form-select>\r\n                                        </div>\r\n\r\n                                        <div class=\"form-group\" *ngIf=\"programType == 'custom_kontraktual'\">\r\n                                            <label>Jabatan</label>\r\n                                            <form-input name=\"cluster\" [type]=\"'text'\" [(ngModel)]=\"form.description\" [placeholder]=\"'Group'\" autofocus\r\n                                                required>\r\n                                            </form-input>\r\n                                        </div>\r\n\r\n                                        <div *ngIf=\"programType != 'reguler'\">\r\n                                            <label>Program/Cluster</label>\r\n                                            <form-input name=\"cluster\" [type]=\"'text'\" [(ngModel)]=\"form.cluster\" [placeholder]=\"'Program/Cluster'\" autofocus\r\n                                                required>\r\n                                            </form-input>\r\n                                        </div>\r\n\r\n                                        <button class=\"btn save-button\" (click)=\"formSubmitAddMember()\" type=\"submit\"\r\n                                            [routerLinkActive]=\"['router-link-active']\"><i class=\"fa fa-fw fa-save\"\r\n                                                style=\"transform: translate(-3px, -1px)\"></i>Submit</button>\r\n                                        \r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"col-md-6\">\r\n                            <div class=\"card mb-3\">\r\n                                <input #addBulk type=\"file\" style=\"display:none\" (change)=\"onFileSelected($event)\"\r\n                                />\r\n\r\n                                <div class=\"card-header\">\r\n                                    <h2>Add Member Bulk</h2>\r\n                                </div>\r\n                                <div class=\"card-content\">\r\n                                    <div class=\"col-md-12\">\r\n                                        <label>Upload Your File Here</label>\r\n                                        <div class=\"member-bulk-update-container\">\r\n                                            <span *ngIf=\"selectedFile && prodOnUpload == false\">{{selectedFile.name}}</span>\r\n                                        </div>\r\n\r\n                                        <div class=\"form-group uploaded\" *ngIf=\"prodOnUpload == false && startUploading == true && cancel == false\">\r\n                                            <div class=\"loading\">\r\n                                              <div class=\"sk-fading-circle\">\r\n                                                <div class=\"sk-circle1 sk-circle\"></div>\r\n                                                <div class=\"sk-circle2 sk-circle\"></div>\r\n                                                <div class=\"sk-circle3 sk-circle\"></div>\r\n                                                <div class=\"sk-circle4 sk-circle\"></div>\r\n                                                <div class=\"sk-circle5 sk-circle\"></div>\r\n                                                <div class=\"sk-circle6 sk-circle\"></div>\r\n                                                <div class=\"sk-circle7 sk-circle\"></div>\r\n                                                <div class=\"sk-circle8 sk-circle\"></div>\r\n                                                <div class=\"sk-circle9 sk-circle\"></div>\r\n                                                <div class=\"sk-circle10 sk-circle\"></div>\r\n                                                <div class=\"sk-circle11 sk-circle\"></div>\r\n                                                <div class=\"sk-circle12 sk-circle\"></div>\r\n                                              </div>\r\n                                            </div>\r\n                                        </div>\r\n\r\n                                        <div class=\"container-two-button\">\r\n                                            <div class=\"img-upload\" *ngIf=\"!selectedFile; else upload_image\">\r\n                                                <button class=\"btn btn-upload\" (click)=\"addBulk.click()\">\r\n                                                    <i class=\"fa fa-fw fa-upload\"></i>\r\n                                                    <span>upload</span>\r\n                                                </button>\r\n                                            </div>\r\n    \r\n                                            <div class=\"img-upload\" *ngIf=\"selectedFile\">\r\n                                                <button class=\"btn btn-submit\" (click)=\"updateDataBulk()\">\r\n                                                    <i class=\"fa fa-fw fa-save\"></i>\r\n                                                    <span>submit</span>\r\n                                                </button>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                    <ng-template #upload_image>\r\n                                        <button class=\"btn btn-upload\" (click)=\"addBulk.click()\">\r\n                                            <i class=\"fa fa-fw fa-upload\"></i>\r\n                                            <span>re-upload</span>\r\n                                        </button>\r\n                                    </ng-template>\r\n\r\n                                    <div class=\"col-md-12 col-sm-12 error-msg-upload\">\r\n                                        <div *ngIf=\"errorFile\"><mark>Error: {{errorFile}}</mark></div>                       \r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <!-- <div class=\"col-md-6\">\r\n                            <div class=\"card mb-3\">\r\n                                <div class=\"card-header\">\r\n                                    <h2>Member &amp; Status Member</h2>\r\n                                </div>\r\n                                <div class=\"card-content\">\r\n                                    <div class=\"col-md-12\">\r\n\r\n                                        <label>Password</label>\r\n                                        <form-input name=\"password\" [type]=\"'password'\" [placeholder]=\"'Password'\"\r\n                                            [(ngModel)]=\"form.password\" autofocus required>\r\n                                        </form-input>\r\n\r\n                                        <label>Email</label>\r\n                                        <form-input name=\"email\" [(ngModel)]=\"form.email\" [type]=\"'text'\" [placeholder]=\"'Email'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Cell Phone</label>\r\n                                        <form-input name=\"cell_phone\" [type]=\"'number'\" [(ngModel)]=\"form.cell_phone\" [placeholder]=\"'Cell Phone'\"\r\n                                            autofocus required>\r\n                                        </form-input>\r\n\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div> -->\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <!-- <form #form=\"ngForm\" (ngSubmit)=\"formSubmitAddMember(form.value)\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-6\">\r\n                        <label>Full Name</label>\r\n                        <form-input name=\"full_name\" [type]=\"'text'\" [placeholder]=\"'Full Name'\" [(ngModel)]=\"full_name\"\r\n                            required></form-input>\r\n\r\n                        <label>Email</label>\r\n                        <form-input name=\"email\" [placeholder]=\"'Email'\" [type]=\"'text'\" [(ngModel)]=\"email\" required>\r\n                        </form-input>\r\n\r\n                        <label>Password</label>\r\n                        <form-input name=\"password\" [placeholder]=\"'Password'\" [type]=\"'password'\"\r\n                            [(ngModel)]=\"password\" required></form-input>\r\n\r\n                    \r\n                        <label>Cell Phone</label>\r\n                        <form-input name=\"cell_phone\" [placeholder]=\"'Cell Phone'\" [type]=\"'number'\"\r\n                            [(ngModel)]=\"cell_phone\" required></form-input>\r\n\r\n                        <label>Date of Birth</label>\r\n                        <div class=\"form-group\">\r\n                            <div class=\"input-group datepicker-input\">\r\n                                <input class=\"form-control\" placeholder=\"yyyy-mm-dd\" name=\"dob\" [(ngModel)]=\"dob\"\r\n                                    ngbDatepicker #d=\"ngbDatepicker\">\r\n                                <button class=\"input-group-addon\" (click)=\"d.toggle()\" type=\"button\">\r\n                                    <span class=\"fa fa-calendar\"></span>\r\n                                </button>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <label>Date of Birth</label>\r\n                        <div class=\"form-group\">\r\n                            <div class=\"input-group datepicker-input\">\r\n                                <input class=\"form-control\" placeholder=\"yyyy-mm-dd\" name=\"dob\" [(ngModel)]=\"dob\"\r\n                                    ngbDatepicker #d=\"ngbDatepicker\">\r\n                                <button class=\"input-group-addon\" (click)=\"d.toggle()\" type=\"button\">\r\n                                    <span class=\"fa fa-calendar\"></span>\r\n                                </button>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <label>City</label>\r\n                        <form-input name=\"mcity\" [placeholder]=\"'City'\" [type]=\"'text'\" [(ngModel)]=\"mcity\" required>\r\n                        </form-input>\r\n                    </div>\r\n\r\n                    <div class=\"col-md-6\">\r\n                        <label>State</label>\r\n                        <form-input name=\"mstate\" [placeholder]=\"'State'\" [type]=\"'text'\" [(ngModel)]=\"mstate\" required>\r\n                        </form-input>\r\n\r\n                        <label>Country</label>\r\n                        <form-input name=\"mcountry\" [placeholder]=\"'Country'\" [type]=\"'text'\" [(ngModel)]=\"mcountry\"\r\n                            required></form-input>\r\n\r\n                        <label>Address</label>\r\n                        <form-input name=\"maddress1\" [placeholder]=\"'Address'\" [type]=\"'textarea'\"\r\n                            [(ngModel)]=\"maddress1\" required></form-input>\r\n\r\n                        <label>Post Code</label>\r\n                        <form-input name=\"mpostcode\" [placeholder]=\"'Post Code'\" [type]=\"'number'\"\r\n                            [(ngModel)]=\"mpostcode\" required></form-input>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label>Gender</label>\r\n                            <select class=\"form-control\" name=\"gender\" [(ngModel)]=\"gender\">\r\n                                <option *ngFor=\"let d of gender_type\" value=\"{{d.value}}\">{{d.label}}</option>\r\n                            </select>\r\n                        </div>\r\n\r\n\r\n                        <label>Marital Status</label>\r\n                        <form-select name=\"marital_status\" [(ngModel)]=\"marital_status\" [data]=\"marital_status_type\">\r\n                        </form-select>\r\n\r\n                        <label>Member Type</label>\r\n                        <form-select name=\"type\" [(ngModel)]=\"type\" [data]=\"member_type\"></form-select>\r\n                    </div>\r\n                </div>\r\n\r\n            </form> -->\r\n        </div>\r\n    </div>\r\n\r\n    <div *ngIf=\"showPasswordPage\">\r\n        <app-show-password [back]=\"[this, 'showThisPassword']\" [detail]=\"data\"></app-show-password>\r\n    </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/form-member/additional-document/additional-document.component.html":
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/form-member/additional-document/additional-document.component.html ***!
  \*****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!edit\">\r\n    <div class=\"card card-detail\">\r\n        <div class=\"card-header\">\r\n            <button class=\"btn back_button\" (click)=\"backToDetail()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button><h1>{{detail.member_id}}</h1>\r\n            <div class=\"edit_button\">\r\n                <button class=\"btn btn-right\" (click)=\"editThis()\"><i class=\"fa fa-fw fa-pencil-alt\"></i>edit</button>\r\n            </div>\r\n        </div>\r\n        <div class=\"card-content\" *ngIf=\"!errorMessage\">\r\n            <div class=\"member-detail\">\r\n        \r\n                <div class=\"row\">\r\n\r\n                    <!-- <div class=\"col-md-4 col-sm-12\">\r\n                        <div class=\"card mb-3\">\r\n                            <div class=\"card-header\"><h2>Dokumen Tambahan</h2></div>\r\n                            <div class=\"card-content\">\r\n                                <div class=\"col-md-12\">             \r\n    \r\n                                    <label class=\"label-bold\">Foto BAST</label>\r\n                                    <ng-container *ngIf=\"detail.additional_info && detail.additional_info.bast && isArray(detail.additional_info.bast) && detail.additional_info.bast.length > 0; else empty_image\">\r\n                                        <div class=\"member-detail-image-container\"  *ngFor=\"let itemImage of detail.additional_info.bast; let i = index\">\r\n                                            <ng-container>\r\n                                                <a href={{itemImage.pic_file_name}} target=\"_blank\">\r\n                                                    <img src={{itemImage.pic_file_name}} class=\"member-detail-image\" />\r\n                                                </a>\r\n                                                <div class=\"btn-download-container\">\r\n                                                    <button (click)=\"downloadImage(itemImage.pic_file_name)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                                </div>\r\n                                                <div style=\"text-align: center;\">\r\n                                                    <span>\r\n                                                        <b>Last updated : {{convertDate(itemImage.updated_date)}}</b>\r\n                                                    </span>\r\n                                                </div>\r\n    \r\n                                                <br/>\r\n                                            </ng-container>\r\n                                        </div>\r\n                                    </ng-container>\r\n    \r\n                                    <ng-template #empty_image>\r\n                                        <div>\r\n                                            empty\r\n                                        </div>\r\n                                    </ng-template>\r\n    \r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div> -->\r\n\r\n                    <!-- <div class=\"col-md-4 col-sm-12\">\r\n                        <div class=\"card mb-3\">\r\n                            <div class=\"card-header\"><h2>Dokumen Tambahan</h2></div>\r\n                            <div class=\"card-content\">\r\n                                <div class=\"col-md-12\">             \r\n    \r\n                                    <ng-template #empty_image>\r\n                                        <div>\r\n                                            empty\r\n                                        </div>\r\n                                    </ng-template>\r\n    \r\n    \r\n                                    <label class=\"label-bold\">Foto Surat Kuasa</label>\r\n                                    <ng-container *ngIf=\"detail.additional_info && detail.additional_info.surat_kuasa && isArray(detail.additional_info.surat_kuasa) && detail.additional_info.surat_kuasa.length > 0; else empty_image\">\r\n                                        <div class=\"member-detail-image-container\"   *ngFor=\"let itemImage of detail.additional_info.surat_kuasa; let i = index\">\r\n                                            <ng-container>\r\n                                                <a href={{itemImage.pic_file_name}} target=\"_blank\">\r\n                                                    <img src={{itemImage.pic_file_name}} class=\"member-detail-image\" />\r\n                                                </a>\r\n                                                <div class=\"btn-download-container\">\r\n                                                    <button (click)=\"downloadImage(itemImage.pic_file_name)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                                </div>\r\n                                                <div style=\"text-align: center;\">\r\n                                                    <span>\r\n                                                        <b>Last updated : {{convertDate(itemImage.updated_date)}}</b>\r\n                                                    </span>\r\n                                                </div>\r\n                                                <br/>\r\n                                            </ng-container>\r\n                                        </div>\r\n                                    </ng-container>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div> -->\r\n\r\n                    <div class=\"col-md-4 col-sm-12\">\r\n                        <div class=\"card mb-3\">\r\n                            <div class=\"card-header\"><h2>Dokumen Tambahan</h2></div>\r\n                            <div class=\"card-content\">\r\n                                <div class=\"col-md-12\">             \r\n    \r\n                                    <ng-template #empty_image>\r\n                                        <div>\r\n                                            empty\r\n                                        </div>\r\n                                    </ng-template>\r\n    \r\n    \r\n                                    <label class=\"label-bold\">Foto Surat Pernyataan</label>\r\n                                    <ng-container *ngIf=\"detail.additional_info && detail.additional_info.surat_pernyataan && isArray(detail.additional_info.surat_pernyataan) && detail.additional_info.surat_pernyataan.length > 0; else empty_image\">\r\n                                        <div class=\"member-detail-image-container\"   *ngFor=\"let itemImage of detail.additional_info.surat_pernyataan; let i = index\">\r\n                                            <ng-container>\r\n                                                <a href={{itemImage.pic_file_name}} target=\"_blank\">\r\n                                                    <img *ngIf=\"checkFileType(itemImage.pic_file_name) == 'image'\" src={{itemImage.pic_file_name}} class=\"member-detail-image\" />\r\n                                                    <pdf-viewer \r\n                                                        *ngIf=\"checkFileType(itemImage.pic_file_name) == 'document'\"\r\n                                                        [src]=\"itemImage.pic_file_name\"\r\n                                                        [render-text]=\"false\"\r\n                                                        [original-size]=\"false\"\r\n                                                        [page] = \"1\"\r\n                                                        [show-all]=\"false\"\r\n                                                        style=\"width: 100%; height: 100%\">\r\n                                                    </pdf-viewer>\r\n                                                </a>\r\n                                                <div class=\"btn-download-container\">\r\n                                                    <button (click)=\"downloadImage(itemImage.pic_file_name)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                                </div>\r\n                                                <div style=\"text-align: center;\">\r\n                                                    <span>\r\n                                                        <b>Last updated : {{convertDate(itemImage.updated_date)}}</b>\r\n                                                    </span>\r\n                                                </div>\r\n                                                <br/>\r\n                                            </ng-container>\r\n                                        </div>\r\n                                    </ng-container>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n\r\n                    \r\n                </div>\r\n         \r\n             </div>\r\n        </div>\r\n        <div *ngIf=\"errorMessage\" class=\"error-message\">\r\n            <div class=\"text-message\">\r\n                <i class=\"fas fa-ban\"></i>\r\n                {{errorMessage}}\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div *ngIf=\"edit\">\r\n    <app-additional-document-edit [back]=\"[this, 'editThis']\" [detail]=\"memberDetail\"></app-additional-document-edit>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/form-member/additional-document/edit/additional-document.edit.component.html":
/*!***************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/form-member/additional-document/edit/additional-document.edit.component.html ***!
  \***************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card card-detail\">\r\n    <ng-container *ngIf=\"allBast.length > 0\">\r\n        <ng-container *ngFor=\"let item of allBast; let i = index\">\r\n            <input type=\"file\"  (change)=\"onFileSelected($event, 'bast', i)\" \r\n                accept=\"image/x-png,image/gif,image/jpeg,application/pdf\" style=\"display:none;\" [id]=\"'bast'+i\"/>\r\n        </ng-container>\r\n    </ng-container>\r\n\r\n    <ng-container *ngIf=\"allSuratKuasa.length > 0\">\r\n        <ng-container *ngFor=\"let item of allSuratKuasa; let i = index\">\r\n            <input type=\"file\"  (change)=\"onFileSelected($event, 'suratKuasa', i)\" \r\n                accept=\"image/x-png,image/gif,image/jpeg,application/pdf\" style=\"display:none;\" [id]=\"'suratKuasa'+i\"/>\r\n        </ng-container>\r\n    </ng-container>\r\n\r\n    <ng-container *ngIf=\"allSuratPernyataan.length > 0\">\r\n        <ng-container *ngFor=\"let item of allSuratPernyataan; let i = index\">\r\n            <input type=\"file\"  (change)=\"onFileSelected($event, 'suratPernyataan', i)\" \r\n                accept=\"image/x-png,image/gif,image/jpeg,application/pdf\" style=\"display:none;\" [id]=\"'suratPernyataan'+i\"/>\r\n        </ng-container>\r\n    </ng-container>\r\n    \r\n    \r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToDetail()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button><h1>{{detail.member_id}}</h1>\r\n        <button class=\"btn save_button {{loading}}\" type=\"submit\" (click)=\"saveThis()\"><i class=\"fa fa-fw fa-save\"></i> <span  *ngIf=\"!loading\">Save</span> <span *ngIf=\"loading\">loading</span></button>\r\n    </div>\r\n    <div class=\"card-content\">\r\n        <div class=\"member-detail\">\r\n    \r\n            <div class=\"row\">\r\n                <!-- <div class=\"col-md-4 col-sm-12\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Dokumen Tambahan</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">             \r\n                                \r\n                                <label class=\"label-bold\">Foto BAST</label>\r\n                                <ng-container *ngIf=\"updateSingle && updateSingle.bast && updateSingle.bast.length > 0\">\r\n                                        <div class=\"member-detail-image-container\" *ngFor=\"let itemImage of updateSingle.bast; let i = index\">\r\n                                            <div *ngIf=\"validURL(itemImage.pic_file_name) && !loadingBAST[i]\" class=\"img-upload\">\r\n                                                <img src={{itemImage.pic_file_name}} class=\"member-detail-image\" />\r\n                                            </div>\r\n        \r\n                                            <div class=\"form-group uploaded\" *ngIf=\"loadingBAST[i]\">\r\n                                                <div class=\"loading\">\r\n                                                  <div class=\"sk-fading-circle\">\r\n                                                    <div class=\"sk-circle1 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle2 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle3 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle4 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle5 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle6 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle7 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle8 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle9 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle10 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle11 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle12 sk-circle\"></div>\r\n                                                  </div>\r\n                                                </div>\r\n                                            </div>\r\n        \r\n                                            <div class=\"img-upload\">\r\n                                                <button class=\"btn_upload\" (click)=\"openUpload('bast', i)\">\r\n                                                    <i class=\"fa fa-fw fa-upload\"></i>\r\n                                                    <span *ngIf=\"validURL(itemImage.pic_file_name); else upload_image\">Ubah</span>\r\n                                                </button>\r\n                                                <button  *ngIf=\"validURL(itemImage.pic_file_name)\" class=\"btn_delete\" (click)=\"deleteImage('bast', i, itemImage.url)\">\r\n                                                    <i class=\"fa fa-fw fa-trash-alt\"></i>\r\n                                                    <span>Hapus</span>\r\n                                                </button>\r\n                                            </div>\r\n                                            <div *ngIf=\"validURL(itemImage.pic_file_name)\" style=\"text-align: center;\">\r\n                                                <span>\r\n                                                    <b>Last updated : {{convertDate(itemImage.updated_date)}}</b>\r\n                                                </span>\r\n                                            </div>\r\n\r\n                                        </div>\r\n                                </ng-container>\r\n\r\n                                <div class=\"add-more-container\">\r\n                                    <button (click)=\"addMoreUpload('bast')\" class=\"add-more-image\"><i class=\"fa fa-fw fa-plus-square\"></i> Tambah foto BAST</button>\r\n                                </div>\r\n                                \r\n                                <hr/>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div> -->\r\n\r\n                <!-- <div class=\"col-md-4 col-sm-12\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Dokumen Tambahan</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\"> \r\n\r\n                                <label  class=\"label-bold\">Foto Surat Kuasa</label>\r\n                                <ng-container *ngIf=\"updateSingle && updateSingle.surat_kuasa && updateSingle.surat_kuasa.length > 0\">\r\n                                    <div class=\"member-detail-image-container\"  *ngFor=\"let itemImage of updateSingle.surat_kuasa; let i = index\">\r\n                                        <div *ngIf=\"validURL(itemImage.pic_file_name) && !loadingSuratKuasa[i]\" class=\"img-upload\">\r\n                                            <img src={{itemImage.pic_file_name}} class=\"member-detail-image\" />\r\n                                        </div>\r\n    \r\n                                        <div class=\"form-group uploaded\" *ngIf=\"loadingSuratKuasa[i]\">\r\n                                            <div class=\"loading\">\r\n                                              <div class=\"sk-fading-circle\">\r\n                                                <div class=\"sk-circle1 sk-circle\"></div>\r\n                                                <div class=\"sk-circle2 sk-circle\"></div>\r\n                                                <div class=\"sk-circle3 sk-circle\"></div>\r\n                                                <div class=\"sk-circle4 sk-circle\"></div>\r\n                                                <div class=\"sk-circle5 sk-circle\"></div>\r\n                                                <div class=\"sk-circle6 sk-circle\"></div>\r\n                                                <div class=\"sk-circle7 sk-circle\"></div>\r\n                                                <div class=\"sk-circle8 sk-circle\"></div>\r\n                                                <div class=\"sk-circle9 sk-circle\"></div>\r\n                                                <div class=\"sk-circle10 sk-circle\"></div>\r\n                                                <div class=\"sk-circle11 sk-circle\"></div>\r\n                                                <div class=\"sk-circle12 sk-circle\"></div>\r\n                                              </div>\r\n                                            </div>\r\n                                          </div>\r\n    \r\n                                        <div class=\"img-upload\">\r\n                                            <button class=\"btn_upload\" (click)=\"openUpload('suratKuasa', i)\">\r\n                                                <i class=\"fa fa-fw fa-upload\"></i>\r\n                                                <span *ngIf=\"validURL(itemImage.pic_file_name); else upload_image\">Ubah</span>\r\n                                            </button>\r\n                                            <button  *ngIf=\"validURL(itemImage.pic_file_name)\" class=\"btn_delete\" (click)=\"deleteImage('suratKuasa', i, itemImage.url)\">\r\n                                                <i class=\"fa fa-fw fa-trash-alt\"></i>\r\n                                                <span>Hapus</span>\r\n                                            </button>\r\n                                        </div>\r\n\r\n                                        <div *ngIf=\"validURL(itemImage.pic_file_name)\" style=\"text-align: center;\">\r\n                                            <span>\r\n                                                <b>Last updated : {{convertDate(itemImage.updated_date)}}</b>\r\n                                            </span>\r\n                                        </div>\r\n\r\n                                    </div>\r\n                                </ng-container>\r\n\r\n                                <div class=\"add-more-container\">\r\n                                    <button (click)=\"addMoreUpload('suratKuasa')\" class=\"add-more-image\"><i class=\"fa fa-fw fa-plus-square\"></i> Tambah foto Surat Kuasa</button>\r\n                                </div>\r\n\r\n                                <hr/>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div> -->\r\n\r\n                <div class=\"col-md-4 col-sm-12\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Dokumen Tambahan</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\"> \r\n\r\n                                <label  class=\"label-bold\">Foto Surat Pernyataan</label>\r\n                                <ng-container *ngIf=\"updateSingle && updateSingle.surat_pernyataan && updateSingle.surat_pernyataan.length > 0\">\r\n                                    <div class=\"member-detail-image-container\"  *ngFor=\"let itemImage of updateSingle.surat_pernyataan; let i = index\">\r\n                                        <div *ngIf=\"validURL(itemImage.pic_file_name) && !loadingSuratPernyataan[i]\" class=\"img-upload\">\r\n                                            <pdf-viewer \r\n                                                *ngIf=\"checkFileType(itemImage.pic_file_name) == 'document'\"\r\n                                                [src]=\"itemImage.pic_file_name\"\r\n                                                [render-text]=\"false\"\r\n                                                [original-size]=\"false\"\r\n                                                [page] = \"1\"\r\n                                                [show-all]=\"false\"\r\n                                                style=\"width: 100%; height: 100%\">\r\n                                            </pdf-viewer>\r\n                                            <img *ngIf=\"checkFileType(itemImage.pic_file_name) == 'image'\" src={{itemImage.pic_file_name}} class=\"member-detail-image\" />\r\n                                        </div>\r\n    \r\n                                        <div class=\"form-group uploaded\" *ngIf=\"loadingSuratPernyataan[i]\">\r\n                                            <div class=\"loading\">\r\n                                              <div class=\"sk-fading-circle\">\r\n                                                <div class=\"sk-circle1 sk-circle\"></div>\r\n                                                <div class=\"sk-circle2 sk-circle\"></div>\r\n                                                <div class=\"sk-circle3 sk-circle\"></div>\r\n                                                <div class=\"sk-circle4 sk-circle\"></div>\r\n                                                <div class=\"sk-circle5 sk-circle\"></div>\r\n                                                <div class=\"sk-circle6 sk-circle\"></div>\r\n                                                <div class=\"sk-circle7 sk-circle\"></div>\r\n                                                <div class=\"sk-circle8 sk-circle\"></div>\r\n                                                <div class=\"sk-circle9 sk-circle\"></div>\r\n                                                <div class=\"sk-circle10 sk-circle\"></div>\r\n                                                <div class=\"sk-circle11 sk-circle\"></div>\r\n                                                <div class=\"sk-circle12 sk-circle\"></div>\r\n                                              </div>\r\n                                            </div>\r\n                                          </div>\r\n    \r\n                                        <div class=\"img-upload\">\r\n                                            <button class=\"btn_upload\" (click)=\"openUpload('suratPernyataan', i)\">\r\n                                                <i class=\"fa fa-fw fa-upload\"></i>\r\n                                                <span *ngIf=\"validURL(itemImage.pic_file_name); else upload_image\">Ubah</span>\r\n                                            </button>\r\n                                            <button  *ngIf=\"validURL(itemImage.pic_file_name)\" class=\"btn_delete\" (click)=\"deleteImage('suratPernyataan', i, itemImage.url)\">\r\n                                                <i class=\"fa fa-fw fa-trash-alt\"></i>\r\n                                                <span>Hapus</span>\r\n                                            </button>\r\n                                        </div>\r\n\r\n                                        <div *ngIf=\"validURL(itemImage.pic_file_name)\" style=\"text-align: center;\">\r\n                                            <span>\r\n                                                <b>Last updated : {{convertDate(itemImage.updated_date)}}</b>\r\n                                            </span>\r\n                                        </div>\r\n\r\n                                    </div>\r\n                                </ng-container>\r\n\r\n                                <div class=\"add-more-container\">\r\n                                    <button (click)=\"addMoreUpload('suratPernyataan')\" class=\"add-more-image\"><i class=\"fa fa-fw fa-plus-square\"></i> Tambah foto Surat Pernyataan</button>\r\n                                </div>\r\n\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n     \r\n         </div>\r\n    </div>\r\n    <ng-template #upload_image>\r\n        <span>\r\n            upload\r\n        </span>\r\n    </ng-template>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/form-member/detail/form-member.detail.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/form-member/detail/form-member.detail.component.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!edit && !isNoProcess && !isAddDocument && !isLoginRedeem\" class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToTable()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n        <!-- <button class=\"btn edit_button\" (click)=\"editThis()\"><i class=\"fa fa-fw fa-pencil-alt\"></i> edit</button> -->\r\n        <div class=\"edit_button\">\r\n            <button class=\"btn btn-right-redeem\" (click)=\"redeemLogin()\" [disabled]=\"!isValidForRedeem\">Redeem</button>\r\n            <button class=\"btn btn-right-redeem\" (click)=\"setCompleteMember()\" *ngIf=\"!(memberDetail && memberDetail.additional_info && memberDetail.additional_info.data_complete == 'ya')\">Set Member Complete</button>\r\n            <button *ngIf=\"!mci_project\" class=\"btn btn-right\" (click)=\"noProcess()\"><i class=\"fa fa-fw fa-file-image\"></i>Archived Photos</button>\r\n            <button *ngIf=\"!mci_project\" class=\"btn btn-right\" (click)=\"addDocument()\"><i class=\"fa fa-fw fa-file-image\"></i>BAST, Surat Kuasa, Receipt</button>\r\n            <button class=\"btn btn-right\" (click)=\"editThis()\"><i class=\"fa fa-fw fa-pencil-alt\"></i> edit</button>\r\n        </div>\r\n    </div>\r\n\r\n    <ng-template #empty_image><span>\r\n        empty\r\n    </span></ng-template>\r\n  \r\n    <div class=\"card-content\" *ngIf=\"!errorMessage\">\r\n        <div class=\"member-detail\">\r\n    \r\n            <div class=\"row\">\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"card mb-3\">\r\n                        <div *ngIf=\"programType != 'reguler' && programType != 'custom_kontraktual'\" class=\"card-header\"><h2>Data Pemilik</h2></div>\r\n                        <div *ngIf=\"programType == 'custom_kontraktual'\" class=\"card-header\"><h2>Data Entitas</h2></div>\r\n                        <div *ngIf=\"programType == 'reguler'\" class=\"card-header\"><h2>Data Pelanggan</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">             \r\n                                <label>ID Pelanggan</label>\r\n                                <div name=\"member_id\">{{memberDetail.username}}</div>\r\n\r\n                                <label>Data Complete</label>\r\n                                <div name=\"member_id\">{{memberDetail.additional_info && memberDetail.additional_info.data_complete ? memberDetail.additional_info.data_complete : '-'}}</div>\r\n\r\n                                <div *ngIf=\"programType != 'reguler' && programType != 'custom_kontraktual'\">\r\n                                    <label>Group</label>\r\n                                    <div name=\"description\">{{memberDetail.input_form_data.description}}</div>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"!mci_project\">\r\n                                    <label>Nama Program</label>\r\n                                    <div name=\"description\">{{memberDetail.input_form_data.cluster}}</div>\r\n                                </div>\r\n                                \r\n                                <div *ngIf=\"mci_project\">\r\n                                    <label>Program</label>\r\n                                    <div name=\"description\">{{memberDetail.input_form_data.description}}</div>\r\n                                </div>\r\n                                \r\n                                <div *ngIf=\"programType != 'reguler' && programType != 'custom_kontraktual'\">\r\n                                    <label>Nama Toko</label>\r\n                                    <div name=\"full_name\">{{memberDetail.input_form_data.nama_toko}}</div>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"programType != 'reguler' && programType == 'custom_kontraktual'\">\r\n                                    <label>Nama Entitas</label>\r\n                                    <div name=\"full_name\">{{memberDetail.input_form_data.nama_toko}}</div>\r\n                                </div>\r\n                                \r\n                                <div *ngIf=\"!mci_project\">\r\n                                    <label>Nama Distributor</label>\r\n                                    <div name=\"full_name\">{{sig_kontraktual? 'Semen Indonesia' : memberDetail.input_form_data.nama_distributor}}</div>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"programType != 'reguler' && programType != 'custom_kontraktual'\">\r\n                                    <label>Alamat Toko</label>\r\n                                    <div name=\"full_name\">{{memberDetail.input_form_data.alamat_toko}}</div>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"programType != 'reguler' && programType == 'custom_kontraktual'\">\r\n                                    <label>Alamat Entitas</label>\r\n                                    <div name=\"full_name\">{{memberDetail.input_form_data.alamat_toko}}</div>\r\n                                </div>\r\n\r\n                                <label *ngIf=\"programType == 'reguler'\">ID Bisnis</label>\r\n                                <div *ngIf=\"programType == 'reguler'\" name=\"business_id\">{{memberDetail.input_form_data.business_id}}</div>\r\n\r\n                                <label *ngIf=\"programType == 'custom'\">Nama Pemilik</label>\r\n                                <label *ngIf=\"programType == 'custom_kontraktual'\">Nama PIC</label>\r\n                                <label *ngIf=\"programType == 'reguler'\">Nama Pelanggan</label>\r\n                                <div name=\"full_name\">{{memberDetail.input_form_data.nama_pemilik}}</div>\r\n\r\n                                <label *ngIf=\"programType == 'reguler'\">No. Telepon Pelanggan</label>\r\n                                <div *ngIf=\"programType == 'reguler'\" name=\"full_name\">{{memberDetail.input_form_data.telp_pemilik}}</div>\r\n\r\n                                <label *ngIf=\"programType == 'custom'\">No. WA Pemilik</label>\r\n                                <label *ngIf=\"programType == 'custom_kontraktual'\">No. WA PIC</label>\r\n                                <!-- <label *ngIf=\"mci_project\">No. WA Pelanggan</label> -->\r\n                                <div *ngIf=\"programType != 'reguler'\" name=\"full_name\">{{memberDetail.input_form_data.no_wa_pemilik}}</div>\r\n\r\n                                <label *ngIf=\"programType == 'reguler'\">Email Pelanggan</label>\r\n                                <div *ngIf=\"programType == 'reguler'\" name=\"email\">{{memberDetail.input_form_data.email}}</div>\r\n                                \r\n                                <div *ngIf=\"programType != 'reguler'\">\r\n                                    <label *ngIf=\"programType == 'custom'\">No. KTP Pemilik</label>\r\n                                    <label *ngIf=\"programType == 'custom_kontraktual'\">No. KTP PIC</label>\r\n                                    <div name=\"full_name\">{{memberDetail.input_form_data.ktp_pemilik}}</div>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"programType != 'reguler'\">\r\n                                    <label *ngIf=\"programType == 'custom'\">Foto KTP Pemilik</label>\r\n                                    <label *ngIf=\"programType == 'custom_kontraktual'\">Foto KTP PIC</label>\r\n                                    <div class=\"member-detail-image-container\">\r\n                                        <ng-container  *ngIf=\"validURL(memberDetail.input_form_data.foto_ktp_pemilik); else empty_image\">\r\n                                            <a href={{memberDetail.input_form_data.foto_ktp_pemilik}} target=\"_blank\">\r\n                                                <img *ngIf=\"checkFileType(memberDetail.input_form_data.foto_ktp_pemilik) == 'image'\" src={{memberDetail.input_form_data.foto_ktp_pemilik}} class=\"member-detail-image\" />\r\n                                                <pdf-viewer \r\n                                                    *ngIf=\"checkFileType(memberDetail.input_form_data.foto_ktp_pemilik) == 'document'\"\r\n                                                    [src]=\"memberDetail.input_form_data.foto_ktp_pemilik\"\r\n                                                    [render-text]=\"false\"\r\n                                                    [original-size]=\"false\"\r\n                                                    [page] = \"1\"\r\n                                                    [show-all]=\"false\"\r\n                                                    style=\"width: 100%; height: 100%\">\r\n                                                </pdf-viewer>\r\n                                            </a>\r\n                                            <div class=\"btn-download-container\">\r\n                                                <button (click)=\"downloadImage(memberDetail.input_form_data.foto_ktp_pemilik)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                            </div>\r\n                                        </ng-container>\r\n                                        \r\n                                        \r\n                                    </div>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"programType != 'reguler'\">\r\n                                    <label *ngIf=\"programType == 'custom'\">No. NPWP Pemilik</label>\r\n                                    <label *ngIf=\"programType == 'custom_kontraktual'\">No. NPWP PIC</label>\r\n                                    <div name=\"full_name\">{{memberDetail.input_form_data.npwp_pemilik}}</div>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"programType != 'reguler'\">\r\n                                    <label *ngIf=\"programType == 'custom'\">Foto NPWP Pemilik</label>\r\n                                    <label *ngIf=\"programType == 'custom_kontraktual'\">Foto NPWP PIC</label>\r\n                                    <div class=\"member-detail-image-container\">\r\n                                        <ng-container  *ngIf=\"validURL(memberDetail.input_form_data.foto_npwp_pemilik); else empty_image\">\r\n                                            <a href={{memberDetail.input_form_data.foto_npwp_pemilik}} target=\"_blank\">\r\n                                                <img *ngIf=\"checkFileType(memberDetail.input_form_data.foto_npwp_pemilik) == 'image'\" src={{memberDetail.input_form_data.foto_npwp_pemilik}} class=\"member-detail-image\" />\r\n                                                <pdf-viewer \r\n                                                    *ngIf=\"checkFileType(memberDetail.input_form_data.foto_npwp_pemilik) == 'document'\"\r\n                                                    [src]=\"memberDetail.input_form_data.foto_npwp_pemilik\"\r\n                                                    [render-text]=\"false\"\r\n                                                    [original-size]=\"false\"\r\n                                                    [page] = \"1\"\r\n                                                    [show-all]=\"false\"\r\n                                                    style=\"width: 100%; height: 100%\">\r\n                                                </pdf-viewer>\r\n                                            </a>\r\n                                            <div class=\"btn-download-container\">\r\n                                                <button (click)=\"downloadImage(memberDetail.input_form_data.foto_npwp_pemilik)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                            </div>\r\n                                        </ng-container>\r\n                                    </div>\r\n                                </div>\r\n\r\n                            </div>\r\n                        \r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\" col-md-4\" >\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Data Penerima </h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n\r\n                                <div *ngIf=\"!mci_project\">\r\n                                    <label>Hadiah Dikuasakan</label>\r\n                                    <div name=\"email\">{{memberDetail.input_form_data.hadiah_dikuasakan}}</div>\r\n\r\n                                    <label>Nama Penerima</label>\r\n                                    <div name=\"email\">{{memberDetail.input_form_data.nama_penerima_gopay}}</div>\r\n\r\n                                    <div *ngIf=\"programType == 'custom_kontraktual'\">\r\n                                        <label>Jabatan</label>\r\n                                        <div name=\"description\">{{memberDetail.input_form_data.description}}</div>\r\n                                    </div>\r\n\r\n                                    <label>No. WA Penerima</label>\r\n                                    <div name=\"email\">{{memberDetail.input_form_data.no_wa_penerima}}</div>\r\n\r\n                                    <div *ngIf=\"programType != 'reguler' && programType != 'custom_kontraktual'\">\r\n                                        <label>No. Gopay Penerima<div style=\"font-size: 10px;\">(Jika hadiah berupa Saldo Gopay, isi dengan No. Gopay Penerima)</div></label>\r\n                                        <div name=\"email\">{{memberDetail.input_form_data.telp_penerima_gopay}}</div>\r\n                                    </div>\r\n\r\n                                    <div *ngIf=\"programType != 'reguler' && programType == 'custom_kontraktual'\">\r\n                                        <label>No. E-Wallet Penerima<div style=\"font-size: 10px;\">(Jika hadiah berupa Saldo E-Wallet, isi dengan No. E-Wallet Penerima)</div></label>\r\n                                        <div name=\"email\">{{memberDetail.input_form_data.telp_penerima_gopay}}</div>\r\n                                    </div>\r\n                                </div>\r\n                                \r\n                               <label>Alamat Pengiriman</label>\r\n                               <div name=\"alamat_rumah\">{{memberDetail.input_form_data.alamat_rumah}}</div>\r\n                               <div *ngIf=\"alertAddress\" class=\"red-alert\">Alamat wajib dilengkapi</div>\r\n\r\n                               <label>Provinsi</label>\r\n                               <div name=\"provinsi_rumah\">{{memberDetail.input_form_data.provinsi_rumah}}</div>\r\n                               <div *ngIf=\"alertProvinsi\" class=\"red-alert\">Provinsi wajib dilengkapi</div>\r\n\r\n                               <label>Kota</label>\r\n                               <div name=\"kota_rumah\">{{memberDetail.input_form_data.kota_rumah}}</div>\r\n                               <div *ngIf=\"alertKota\" class=\"red-alert\">Kota wajib dilengkapi</div>\r\n\r\n                               <label>Kecamatan</label>\r\n                               <div name=\"kecamatan_rumah\">{{memberDetail.input_form_data.kecamatan_rumah}}</div>\r\n                               <div *ngIf=\"alertKecamatan\" class=\"red-alert\">Kecamatan wajib dilengkapi</div>\r\n\r\n                               <label>Kelurahan</label>\r\n                               <div name=\"kelurahan_rumah\">{{memberDetail.input_form_data.kelurahan_rumah}}</div>\r\n                               <div *ngIf=\"alertKelurahan\" class=\"red-alert\">Kelurahan wajib dilengkapi</div>\r\n\r\n                               <label>Kode Pos</label>\r\n                               <div name=\"kode_pos_rumah\">{{memberDetail.input_form_data.kode_pos_rumah}}</div>\r\n                               <div *ngIf=\"alertKodePos\" class=\"red-alert\">Kode Pos wajib dilengkapi</div>\r\n\r\n                               <!-- <div *ngIf=mci_project>\r\n                                    <label>Kota Penerima</label>\r\n                                    <div name=\"email\">{{memberDetail.input_form_data.kota_rumah}}</div>\r\n                                </div> -->\r\n\r\n                               <!-- <div *ngIf=mci_project>\r\n                                    <label>Kode Pos Penerima</label>\r\n                                    <div name=\"email\">{{memberDetail.input_form_data.kode_pos_rumah}}</div>\r\n                                </div> -->\r\n\r\n                                <div *ngIf=!mci_project>\r\n                                    <label>No. KTP Penerima</label>\r\n                                    <div name=\"email\">{{memberDetail.input_form_data.ktp_penerima}}</div>\r\n                                </div>\r\n\r\n                                <div *ngIf=!mci_project>\r\n                                    <label>Foto KTP Penerima</label>\r\n                                    <div class=\"member-detail-image-container\">\r\n                                        <ng-container *ngIf=\"validURL(memberDetail.input_form_data.foto_ktp_penerima); else empty_image\">\r\n                                            <a href={{memberDetail.input_form_data.foto_ktp_penerima}} target=\"_blank\" >\r\n                                                <img *ngIf=\"checkFileType(memberDetail.input_form_data.foto_ktp_penerima) == 'image'\" src={{memberDetail.input_form_data.foto_ktp_penerima}} class=\"member-detail-image\" />\r\n                                                <pdf-viewer \r\n                                                    *ngIf=\"checkFileType(memberDetail.input_form_data.foto_ktp_penerima) == 'document'\"\r\n                                                    [src]=\"memberDetail.input_form_data.foto_ktp_penerima\"\r\n                                                    [render-text]=\"false\"\r\n                                                    [original-size]=\"false\"\r\n                                                    [page] = \"1\"\r\n                                                    [show-all]=\"false\"\r\n                                                    style=\"width: 100%; height: 100%\">\r\n                                                </pdf-viewer>\r\n                                            </a>\r\n                                            <div class=\"btn-download-container\">\r\n                                                <button (click)=\"downloadImage(memberDetail.input_form_data.foto_ktp_penerima)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                            </div>\r\n                                        </ng-container>\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <div *ngIf=!mci_project>\r\n                                    <label>No. NPWP Penerima</label>\r\n                                    <div name=\"email\">{{memberDetail.input_form_data.npwp_penerima}}</div>\r\n                                </div>\r\n\r\n                                <div *ngIf=!mci_project>\r\n                                    <label>Foto NPWP Penerima</label>\r\n                                    <div class=\"member-detail-image-container\">\r\n                                        <ng-container *ngIf=\"validURL(memberDetail.input_form_data.foto_npwp_penerima); else empty_image\">\r\n                                            <a href={{memberDetail.input_form_data.foto_npwp_penerima}} target=\"_blank\">\r\n                                                <img *ngIf=\"checkFileType(memberDetail.input_form_data.foto_npwp_penerima) == 'image'\" src={{memberDetail.input_form_data.foto_npwp_penerima}} class=\"member-detail-image\" />\r\n                                                <pdf-viewer \r\n                                                    *ngIf=\"checkFileType(memberDetail.input_form_data.foto_npwp_penerima) == 'document'\"\r\n                                                    [src]=\"memberDetail.input_form_data.foto_npwp_penerima\"\r\n                                                    [render-text]=\"false\"\r\n                                                    [original-size]=\"false\"\r\n                                                    [page] = \"1\"\r\n                                                    [show-all]=\"false\"\r\n                                                    style=\"width: 100%; height: 100%\">\r\n                                                </pdf-viewer>\r\n                                            </a>\r\n                                            <div class=\"btn-download-container\">\r\n                                                <button (click)=\"downloadImage(memberDetail.input_form_data.foto_npwp_penerima)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                            </div>\r\n                                        </ng-container>\r\n                                        </div>\r\n                                </div>\r\n                            </div>\r\n                         </div>\r\n                    </div>\r\n                    \r\n                </div>\r\n                <!-- <div class=\"col-md-4\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Data Foto</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">             \r\n\r\n                                <label class=\"label-bold\">Foto BAST</label>\r\n                                <ng-container *ngIf=\"memberDetail.input_form_data.foto_bast && isArray(memberDetail.input_form_data.foto_bast) && memberDetail.input_form_data.foto_bast.length > 0; else empty_image\">\r\n                                    <div class=\"member-detail-image-container\"  *ngFor=\"let itemImage of memberDetail.input_form_data.foto_bast; let i = index\">\r\n                                        <ng-container *ngIf=\"validURL(itemImage.url); else empty_image\">\r\n                                            <a href={{itemImage.url}} target=\"_blank\">\r\n                                                <img src={{itemImage.url}} class=\"member-detail-image\" />\r\n                                            </a>\r\n                                            <div class=\"btn-download-container\">\r\n                                                <button (click)=\"downloadImage(itemImage.url)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                            </div>\r\n                                            <div style=\"text-align: center;\">\r\n                                                <span>\r\n                                                    <b>Last updated : {{convertDate(itemImage.updated_date)}}</b>\r\n                                                </span>\r\n                                            </div>\r\n\r\n                                            <br/>\r\n                                            <div>\r\n                                                <span><b>Desription :</b></span>\r\n                                                <span>\r\n                                                    {{itemImage.desc}}\r\n                                                </span>\r\n                                            </div>\r\n\r\n                                        </ng-container>\r\n                                    </div>\r\n                                </ng-container>\r\n\r\n                                <ng-template #empty_image>\r\n                                    <div>\r\n                                        empty\r\n                                    </div>\r\n                                </ng-template>\r\n\r\n                                <hr style=\"margin:35px 0 20px 0;\"/>\r\n\r\n                                <label class=\"label-bold\">Foto Surat Kuasa</label>\r\n                                <ng-container *ngIf=\"memberDetail.input_form_data.foto_surat_kuasa && isArray(memberDetail.input_form_data.foto_surat_kuasa) && memberDetail.input_form_data.foto_surat_kuasa.length > 0; else empty_image\">\r\n                                    <div class=\"member-detail-image-container\"   *ngFor=\"let itemImage of memberDetail.input_form_data.foto_surat_kuasa; let i = index\">\r\n                                        <ng-container *ngIf=\"validURL(itemImage.url); else empty_image\">\r\n                                            <a href={{itemImage.url}} target=\"_blank\">\r\n                                                <img src={{itemImage.url}} class=\"member-detail-image\" />\r\n                                            </a>\r\n                                            <div class=\"btn-download-container\">\r\n                                                <button (click)=\"downloadImage(itemImage.url)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                            </div>\r\n                                            <div style=\"text-align: center;\">\r\n                                                <span>\r\n                                                    <b>Last updated : {{convertDate(itemImage.updated_date)}}</b>\r\n                                                </span>\r\n                                            </div>\r\n                                            <br/>\r\n                                            <div>\r\n                                                <span><b>Desription :</b></span>\r\n                                                <span>\r\n                                                    {{itemImage.desc}}\r\n                                                </span>\r\n                                            </div>\r\n                                        </ng-container>\r\n                                    </div>\r\n                                </ng-container>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div> -->\r\n            </div>\r\n          \r\n            \r\n         </div>\r\n    </div>\r\n    <div *ngIf=\"errorMessage\" class=\"error-message\">\r\n        <div class=\"text-message\">\r\n            <i class=\"fas fa-ban\"></i>\r\n            {{errorMessage}}\r\n        </div>\r\n    </div>\r\n</div>\r\n<div *ngIf=\"edit\">\r\n    <app-form-member-edit [back]=\"[this, 'editThis']\" [detail]=\"memberDetail\"></app-form-member-edit>\r\n</div>\r\n\r\n<div *ngIf=\"isAddDocument\">\r\n    <app-list-order [back]=\"[this, 'addDocument']\" [detail]=\"memberDetail\"></app-list-order>\r\n</div>\r\n\r\n<div *ngIf=\"isLoginRedeem\">\r\n    <app-redeem-login [back]=\"[this, 'redeemLogin']\" [detail]=\"memberDetail\"></app-redeem-login>\r\n</div>\r\n\r\n<div *ngIf=\"isNoProcess\">\r\n    <app-additional-document [back]=\"[this, 'noProcess']\" [detail]=\"memberDetail\"></app-additional-document>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/form-member/edit/form-member.edit.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/form-member/edit/form-member.edit.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card card-detail\">\r\n    <input #ktpPemilik type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'ktpPemilik')\"\r\n    accept=\"image/x-png,image/gif,image/jpeg,application/pdf\"/>\r\n    <input #npwpPemilik type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'npwpPemilik')\"\r\n    accept=\"image/x-png,image/gif,image/jpeg,application/pdf\"/>\r\n    <input #ktpPenerima type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'ktpPenerima')\"\r\n    accept=\"image/x-png,image/gif,image/jpeg,application/pdf\"/>\r\n    <input #npwpPenerima type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'npwpPenerima')\"\r\n    accept=\"image/x-png,image/gif,image/jpeg,application/pdf\"/>\r\n\r\n    <ng-container *ngIf=\"allBast.length > 0\">\r\n        <ng-container *ngFor=\"let item of allBast; let i = index\">\r\n            <input type=\"file\"  (change)=\"onFileSelected($event, 'bast', i)\" \r\n                accept=\"image/x-png,image/gif,image/jpeg,application/pdf\" style=\"display:none;\" [id]=\"'bast'+i\"/>\r\n        </ng-container>\r\n    </ng-container>\r\n\r\n    <ng-container *ngIf=\"allSuratKuasa.length > 0\">\r\n        <ng-container *ngFor=\"let item of allSuratKuasa; let i = index\">\r\n            <input type=\"file\"  (change)=\"onFileSelected($event, 'suratKuasa', i)\" \r\n                accept=\"image/x-png,image/gif,image/jpeg,application/pdf\" style=\"display:none;\" [id]=\"'suratKuasa'+i\"/>\r\n        </ng-container>\r\n    </ng-container>\r\n    \r\n    \r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToDetail()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button><h1>{{detail.member_id}}</h1>\r\n        <button class=\"btn save_button {{loading}}\" type=\"submit\" (click)=\"saveThis()\"><i class=\"fa fa-fw fa-save\"></i> <span  *ngIf=\"!loading\">Save</span> <span *ngIf=\"loading\">loading</span></button>\r\n    </div>\r\n    <div class=\"card-content\">\r\n        <div class=\"member-detail\">\r\n    \r\n            <div class=\"row\">\r\n                <div class=\" col-md-4\" >\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\" *ngIf=\"programType != 'reguler' && programType != 'custom_kontraktual'\"><h2>Data Pemilik</h2></div>\r\n                        <div class=\"card-header\" *ngIf=\"programType == 'custom_kontraktual'\"><h2>Data Entitas</h2></div>\r\n                        <div class=\"card-header\" *ngIf=\"programType == 'reguler'\"><h2>Data Pelanggan</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n\r\n                                <label>ID Pelanggan</label>\r\n                                <form-input  name=\"id_pel\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'ID Pelanggan'\"  [(ngModel)]=\"updateSingle.id_pel\" autofocus required></form-input>\r\n\r\n                                <div *ngIf=\"programType != 'reguler' && programType != 'custom_kontraktual'\">\r\n                                    <label>Group</label>\r\n                                    <form-select [(ngModel)]=\"updateSingle.description\" [data]=\"optionsGroup\" *ngIf=\"updateSingle.description == 'nonupfront' || updateSingle.description == 'upfront' || updateSingle.description == '-'\"></form-select>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"programType != 'reguler'\">\r\n                                    <label>Nama Program</label>\r\n                                    <form-input name=\"cluster\" [(ngModel)]=\"updateSingle.cluster\" [type]=\"'text'\" [placeholder]=\"'Program/Cluster'\" autofocus required></form-input>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"programType == 'reguler'\">\r\n                                    <label>Program</label>\r\n                                    <form-input  name=\"program\" [type]=\"'text'\" [placeholder]=\"'Program'\"  [(ngModel)]=\"updateSingle.description\" autofocus required></form-input>\r\n                                </div>\r\n                                \r\n                                <div *ngIf=\"programType != 'reguler' && programType != 'custom_kontraktual'\">\r\n                                    <label>Nama Toko</label>\r\n                                    <form-input name=\"nama_toko\"  [type]=\"'text'\" [placeholder]=\"'Nama Toko'\"  [(ngModel)]=\"updateSingle.nama_toko\" autofocus required></form-input>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"programType != 'reguler' && programType == 'custom_kontraktual'\">\r\n                                    <label>Nama Entitas</label>\r\n                                    <form-input name=\"nama_toko\"  [type]=\"'text'\" [placeholder]=\"'Nama Entitas'\"  [(ngModel)]=\"updateSingle.nama_toko\" autofocus required></form-input>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"programType != 'reguler'\">\r\n                                    <label>Nama Distributor</label>\r\n                                    <form-input name=\"nama_distributor\"  [type]=\"'text'\" [placeholder]=\"'Nama Distributor'\"  [(ngModel)]=\"sig_kontraktual? 'Semen Indonesia' : updateSingle.nama_distributor\" autofocus required></form-input>\r\n                                </div>\r\n                                \r\n                                <div *ngIf=\"programType != 'reguler' && programType != 'custom_kontraktual'\">\r\n                                    <label>Alamat Toko</label>\r\n                                    <form-input name=\"alamat_toko\"  [type]=\"'text'\" [placeholder]=\"'Alamat Toko'\"  [(ngModel)]=\"updateSingle.alamat_toko\" autofocus required></form-input>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"programType != 'reguler' && programType == 'custom_kontraktual'\">\r\n                                    <label>Alamat Entitas</label>\r\n                                    <form-input name=\"alamat_toko\"  [type]=\"'text'\" [placeholder]=\"'Alamat Entitas'\"  [(ngModel)]=\"updateSingle.alamat_toko\" autofocus required></form-input>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"programType == 'reguler'\">\r\n                                    <label>ID Bisnis</label>\r\n                                    <form-input name=\"business_id\"  [type]=\"'text'\" [placeholder]=\"'ID Bisnis'\"  [(ngModel)]=\"updateSingle.business_id\" autofocus required></form-input>\r\n                                </div>\r\n\r\n                                <label *ngIf=\"programType == 'custom'\">Nama Pemilik</label>\r\n                                <label *ngIf=\"programType == 'custom_kontraktual'\">Nama PIC</label>\r\n                                <label *ngIf=\"programType == 'reguler'\">Nama Pelanggan</label>\r\n                                <form-input name=\"nama_pemilik\"  [type]=\"'text'\" [placeholder]=\"'Nama Pemilik'\"  [(ngModel)]=\"updateSingle.nama_pemilik\" autofocus required></form-input>\r\n\r\n                                <div *ngIf=\"programType == 'reguler'\">\r\n                                    <label>No. Telepon Pelanggan</label>\r\n                                    <form-input name=\"telp_pemilik\"  [type]=\"'text'\" [placeholder]=\"'No. Telepon Pelanggan'\"  [(ngModel)]=\"updateSingle.telp_pemilik\" autofocus required></form-input>\r\n                                </div>\r\n                                \r\n                                <div *ngIf=\"programType != 'reguler'\">\r\n                                    <label *ngIf=\"programType == 'custom'\">No. WA Pemilik</label>\r\n                                    <label *ngIf=\"programType == 'custom_kontraktual'\">No. WA PIC</label>\r\n                                    <!-- <label *ngIf=\"mci_project\">No. WA Pelanggan</label> -->\r\n                                    <form-input name=\"no_wa_pemilik\"  [type]=\"'text'\" [placeholder]=\"'No WA Pemilik'\"  [(ngModel)]=\"updateSingle.no_wa_pemilik\" autofocus required></form-input>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"programType == 'reguler'\">\r\n                                    <label>Email Pelanggan</label>\r\n                                    <form-input name=\"email\"  [type]=\"'text'\" [placeholder]=\"'Email Pelanggan'\"  [(ngModel)]=\"updateSingle.email\" autofocus required></form-input>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"programType != 'reguler'\">\r\n                                    <label *ngIf=\"programType == 'custom'\">No. KTP Pemilik</label>\r\n                                    <label *ngIf=\"programType == 'custom_kontraktual'\">No. KTP PIC</label>\r\n                                    <form-input name=\"ktp_pemilik\"  [type]=\"'text'\" [placeholder]=\"'No. KTP Pemilik'\"  [(ngModel)]=\"updateSingle.ktp_pemilik\" autofocus required></form-input>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"programType != 'reguler'\">\r\n                                    <label *ngIf=\"programType == 'custom'\">Foto KTP Pemilik</label>\r\n                                    <label *ngIf=\"programType == 'custom_kontraktual'\">Foto KTP PIC</label>\r\n                                    <!-- <form-input name=\"foto_ktp_pemilik\"  [type]=\"'text'\" [placeholder]=\"'KTP Pemilik'\"  [(ngModel)]=\"updateSingle.foto_ktp_pemilik\" autofocus required></form-input> -->\r\n                                    <div class=\"member-detail-image-container\">\r\n                                        <div *ngIf=\"validURL(updateSingle.foto_ktp_pemilik) && !loadingKTPPemilik\" class=\"img-upload\">\r\n                                            <img *ngIf=\"checkFileType(updateSingle.foto_ktp_pemilik) == 'image'\" src={{updateSingle.foto_ktp_pemilik}} class=\"member-detail-image\" />\r\n                                            <pdf-viewer \r\n                                                *ngIf=\"checkFileType(updateSingle.foto_ktp_pemilik) == 'document'\"\r\n                                                [src]=\"updateSingle.foto_ktp_pemilik\"\r\n                                                [render-text]=\"false\"\r\n                                                [original-size]=\"false\"\r\n                                                [page] = \"1\"\r\n                                                [show-all]=\"false\"\r\n                                                style=\"width: 100%; height: 100%\">\r\n                                            </pdf-viewer>\r\n                                        </div>\r\n\r\n                                        <div class=\"form-group uploaded\" *ngIf=\"loadingKTPPemilik\">\r\n                                            <div class=\"loading\">\r\n                                            <div class=\"sk-fading-circle\">\r\n                                                <div class=\"sk-circle1 sk-circle\"></div>\r\n                                                <div class=\"sk-circle2 sk-circle\"></div>\r\n                                                <div class=\"sk-circle3 sk-circle\"></div>\r\n                                                <div class=\"sk-circle4 sk-circle\"></div>\r\n                                                <div class=\"sk-circle5 sk-circle\"></div>\r\n                                                <div class=\"sk-circle6 sk-circle\"></div>\r\n                                                <div class=\"sk-circle7 sk-circle\"></div>\r\n                                                <div class=\"sk-circle8 sk-circle\"></div>\r\n                                                <div class=\"sk-circle9 sk-circle\"></div>\r\n                                                <div class=\"sk-circle10 sk-circle\"></div>\r\n                                                <div class=\"sk-circle11 sk-circle\"></div>\r\n                                                <div class=\"sk-circle12 sk-circle\"></div>\r\n                                            </div>\r\n                                            </div>\r\n                                        </div>\r\n                                        \r\n                                        <!-- <ng-template #empty_image>\r\n                                            <span>\r\n                                                empty\r\n                                            </span>\r\n                                        </ng-template> -->\r\n\r\n                                        <div class=\"img-upload\">\r\n                                            <button class=\"btn_upload\" (click)=\"ktpPemilik.click()\">\r\n                                                <i class=\"fa fa-fw fa-upload\"></i>\r\n                                                <span *ngIf=\"validURL(updateSingle.foto_ktp_pemilik); else upload_image\">Ubah</span>\r\n                                            </button>\r\n                                            <button  *ngIf=\"validURL(updateSingle.foto_ktp_pemilik)\" class=\"btn_delete\" (click)=\"deletePhoto('foto_ktp_pemilik', updateSingle.foto_ktp_pemilik)\">\r\n                                                <i class=\"fa fa-fw fa-trash-alt\"></i>\r\n                                                <span>Hapus</span>\r\n                                            </button>\r\n                                        </div>\r\n\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"programType != 'reguler'\">\r\n                                    <label *ngIf=\"programType == 'custom'\">No. NPWP Pemilik</label>\r\n                                    <label *ngIf=\"programType == 'custom_kontraktual'\">No. NPWP PIC</label>\r\n                                    <form-input name=\"npwp_pemilik\"  [type]=\"'text'\" [placeholder]=\"'No. NPWP Pemilik'\"  [(ngModel)]=\"updateSingle.npwp_pemilik\" autofocus required></form-input>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"programType != 'reguler'\">\r\n                                    <label *ngIf=\"programType == 'custom'\">Foto NPWP Pemilik</label>\r\n                                    <label *ngIf=\"programType == 'custom_kontraktual'\">Foto NPWP PIC</label>\r\n                                    <!-- <form-input name=\"foto_npwp_pemilik\"  [type]=\"'text'\" [placeholder]=\"'NPWP Pemilik'\"  [(ngModel)]=\"updateSingle.foto_npwp_pemilik\" autofocus required></form-input> -->\r\n                                    <div class=\"member-detail-image-container\">\r\n                                        <div *ngIf=\"validURL(updateSingle.foto_npwp_pemilik) && !loadingNPWPPemilik\" class=\"img-upload\">\r\n                                            <pdf-viewer \r\n                                                *ngIf=\"checkFileType(updateSingle.foto_npwp_pemilik) == 'document'\"\r\n                                                [src]=\"updateSingle.foto_npwp_pemilik\"\r\n                                                [render-text]=\"false\"\r\n                                                [original-size]=\"false\"\r\n                                                [page] = \"1\"\r\n                                                [show-all]=\"false\"\r\n                                                style=\"width: 100%; height: 100%\">\r\n                                            </pdf-viewer>\r\n                                            <img *ngIf=\"checkFileType(updateSingle.foto_npwp_pemilik) == 'image'\" src={{updateSingle.foto_npwp_pemilik}} class=\"member-detail-image\" />\r\n                                        </div>\r\n\r\n                                        <div class=\"form-group uploaded\" *ngIf=\"loadingNPWPPemilik\">\r\n                                            <div class=\"loading\">\r\n                                            <div class=\"sk-fading-circle\">\r\n                                                <div class=\"sk-circle1 sk-circle\"></div>\r\n                                                <div class=\"sk-circle2 sk-circle\"></div>\r\n                                                <div class=\"sk-circle3 sk-circle\"></div>\r\n                                                <div class=\"sk-circle4 sk-circle\"></div>\r\n                                                <div class=\"sk-circle5 sk-circle\"></div>\r\n                                                <div class=\"sk-circle6 sk-circle\"></div>\r\n                                                <div class=\"sk-circle7 sk-circle\"></div>\r\n                                                <div class=\"sk-circle8 sk-circle\"></div>\r\n                                                <div class=\"sk-circle9 sk-circle\"></div>\r\n                                                <div class=\"sk-circle10 sk-circle\"></div>\r\n                                                <div class=\"sk-circle11 sk-circle\"></div>\r\n                                                <div class=\"sk-circle12 sk-circle\"></div>\r\n                                            </div>\r\n                                            </div>\r\n                                        </div>\r\n                                        \r\n                                        <!-- <ng-template #empty_image>\r\n                                            <span>\r\n                                                empty\r\n                                            </span>\r\n                                        </ng-template> -->\r\n                                        <div class=\"img-upload\">\r\n\r\n                                            <button class=\"btn_upload\" (click)=\"npwpPemilik.click()\">\r\n                                                <i class=\"fa fa-fw fa-upload\"></i>\r\n                                                <span *ngIf=\"validURL(updateSingle.foto_npwp_pemilik); else upload_image\">Ubah</span>\r\n                                            </button>\r\n                                            <button  *ngIf=\"validURL(updateSingle.foto_npwp_pemilik)\" class=\"btn_delete\" (click)=\"deletePhoto('foto_npwp_pemilik', updateSingle.foto_npwp_pemilik)\">\r\n                                                <i class=\"fa fa-fw fa-trash-alt\"></i>\r\n                                                <span>Hapus</span>\r\n                                            </button>\r\n\r\n                                            <!-- <input [type]=\"'file'\" [id]=\"'fileUpload'\" ref=\"'file'\"/> -->\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n\r\n                            </div>\r\n                        \r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\" col-md-4\" >\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Data Penerima</h2> </div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n\r\n                                <div *ngIf=\"programType != 'reguler'\">\r\n                                    <label>Hadiah Dikuasakan</label>\r\n                                    <form-select [(ngModel)]=\"updateSingle.hadiah_dikuasakan\" [data]=\"optionsKuasa1\" *ngIf=\"updateSingle.hadiah_dikuasakan == 'true' || updateSingle.hadiah_dikuasakan == 'false'\"></form-select>\r\n                                    <form-select [(ngModel)]=\"updateSingle.hadiah_dikuasakan\" [data]=\"optionsKuasa2\" *ngIf=\"updateSingle.hadiah_dikuasakan == 'KUASA' || updateSingle.hadiah_dikuasakan == 'TIDAK KUASA' || updateSingle.hadiah_dikuasakan == '-'\"></form-select>\r\n                                    <form-select [(ngModel)]=\"updateSingle.hadiah_dikuasakan\" [data]=\"optionsKuasa3\" *ngIf=\"updateSingle.hadiah_dikuasakan == 'ya' || updateSingle.hadiah_dikuasakan == 'tidak'\"></form-select>\r\n                                \r\n\r\n                                    <label>Nama Penerima</label>\r\n                                    <form-input name=\"nama_penerima_gopay\"  [type]=\"'text'\" [placeholder]=\"'Nama Penerima'\"  [(ngModel)]=\"updateSingle.nama_penerima_gopay\" autofocus required></form-input>\r\n\r\n                                    <div *ngIf=\"programType == 'custom_kontraktual'\">\r\n                                        <label>Jabatan</label>\r\n                                        <form-input name=\"description\" [(ngModel)]=\"updateSingle.description\" [type]=\"'text'\" [placeholder]=\"'Group'\" autofocus required></form-input>\r\n                                    </div>\r\n\r\n                                    <label>No. WA Penerima</label>\r\n                                    <form-input name=\"no_wa_penerima\"  [type]=\"'text'\" [placeholder]=\"'No. WA Penerima'\"  [(ngModel)]=\"updateSingle.no_wa_penerima\" autofocus required></form-input>\r\n\r\n                                    <div *ngIf=\"programType != 'reguler' && programType != 'custom_kontraktual'\">\r\n                                        <label>No. Gopay Penerima</label>\r\n                                        <form-input name=\"telp_penerima_gopay\"  [type]=\"'text'\" [placeholder]=\"'No Gopay Penerima'\"  [(ngModel)]=\"updateSingle.telp_penerima_gopay\" autofocus required></form-input>\r\n                                    </div>\r\n\r\n                                    <div *ngIf=\"programType != 'reguler' && programType == 'custom_kontraktual'\">\r\n                                        <label>No. E-Wallet Penerima</label>\r\n                                        <form-input name=\"telp_penerima_gopay\"  [type]=\"'text'\" [placeholder]=\"'No E-Wallet Penerima'\"  [(ngModel)]=\"updateSingle.telp_penerima_gopay\" autofocus required></form-input>\r\n                                    </div>\r\n                                    \r\n                                </div>\r\n                                \r\n                                <label>Alamat Pengiriman</label>\r\n                                <form-input name=\"alamat_rumah\"  [type]=\"'text'\" [placeholder]=\"'Alamat Pengiriman'\"  [(ngModel)]=\"updateSingle.alamat_rumah\" autofocus required></form-input>\r\n\r\n                                <label>Provinsi</label>\r\n                                <!-- <form-input name=\"provinsi_rumah\"  [type]=\"'text'\" [placeholder]=\"'Provinsi'\"  [(ngModel)]=\"updateSingle.provinsi_rumah\" autofocus required></form-input> -->\r\n                                <div class=\"ng-autocomplete\">\r\n                                    <ng-autocomplete \r\n                                      [data]=\"dataProvince\"\r\n                                      [(ngModel)]=\"updateSingle.provinsi_rumah\"\r\n                                      [searchKeyword]=\"keywordProvince\"\r\n                                      (selected)='selectEvent($event, \"province\")'\r\n                                      (inputChanged)='inputRegion($event, \"province\")'\r\n                                      [itemTemplate]=\"templateProvince\"\r\n                                      [placeHolder]=\"placeholderProvince\"\r\n                                      [isLoading]=\"loadingProvince\">                                 \r\n                                    </ng-autocomplete>\r\n                                    \r\n                                    <ng-template #templateProvince let-item>\r\n                                        <a [innerHTML]=\"item.province\"></a>\r\n                                    </ng-template>\r\n                                </div>\r\n\r\n                                <label>Kota</label>\r\n                                <!-- <form-input name=\"kota_rumah\"  [type]=\"'text'\" [placeholder]=\"'Kota'\"  [(ngModel)]=\"updateSingle.kota_rumah\" autofocus required></form-input> -->\r\n                                <div class=\"ng-autocomplete\">\r\n                                    <ng-autocomplete \r\n                                      [data]=\"dataCity\"\r\n                                      [(ngModel)]=\"updateSingle.kota_rumah\"\r\n                                      [searchKeyword]=\"keywordCity\"\r\n                                      (selected)='selectEvent($event, \"city\")'\r\n                                      (inputChanged)='inputRegion($event, \"city\")'\r\n                                      [itemTemplate]=\"templateCity\"\r\n                                      [placeHolder]=\"placeholderCity\"\r\n                                      [isLoading]=\"loadingCity\"\r\n                                      [disabled] = \"programType != 'reguler' && !province_code\">                                 \r\n                                    </ng-autocomplete>\r\n                                    \r\n                                    <ng-template #templateCity let-item>\r\n                                        <a [innerHTML]=\"item.city\"></a>\r\n                                    </ng-template>\r\n                                </div>\r\n\r\n                                <label>Kecamatan</label>\r\n                                <!-- <form-input name=\"kecamatan_rumah\"  [type]=\"'text'\" [placeholder]=\"'Kecamatan'\"  [(ngModel)]=\"updateSingle.kecamatan_rumah\" autofocus required></form-input> -->\r\n                                <div class=\"ng-autocomplete\">\r\n                                    <ng-autocomplete \r\n                                      [data]=\"dataSubDistrict\"\r\n                                      [(ngModel)]=\"updateSingle.kecamatan_rumah\"\r\n                                      [searchKeyword]=\"keywordSubDistrict\"\r\n                                      (selected)='selectEvent($event, \"subdistrict\")'\r\n                                      (inputChanged)='inputRegion($event, \"subdistrict\")'\r\n                                      [itemTemplate]=\"templateSubDistrict\"\r\n                                      [placeHolder]=\"placeholderSubDistrict\"\r\n                                      [isLoading]=\"loadingSubDistrict\"\r\n                                      [disabled] = \"programType != 'reguler' && !city_code\">                                 \r\n                                    </ng-autocomplete>\r\n                                    \r\n                                    <ng-template #templateSubDistrict let-item>\r\n                                        <a [innerHTML]=\"item.subdistrict\"></a>\r\n                                    </ng-template>\r\n                                </div>\r\n\r\n                                <label>Kelurahan</label>\r\n                                <!-- <form-input name=\"kelurahan_rumah\"  [type]=\"'text'\" [placeholder]=\"'Kelurahan'\"  [(ngModel)]=\"updateSingle.kelurahan_rumah\" autofocus required></form-input> -->\r\n                                <div class=\"ng-autocomplete\">\r\n                                    <ng-autocomplete \r\n                                      [data]=\"dataVillage\"\r\n                                      [(ngModel)]=\"updateSingle.kelurahan_rumah\"\r\n                                      [searchKeyword]=\"keywordVillage\"\r\n                                      (selected)='selectEvent($event, \"village\")'\r\n                                      (inputChanged)='inputRegion($event, \"village\")'\r\n                                      [itemTemplate]=\"templateVillage\"\r\n                                      [placeHolder]=\"placeholderVillage\"\r\n                                      [isLoading]=\"loadingVillage\"\r\n                                      [disabled] = \"programType != 'reguler' && !subdistrict_code\">                                 \r\n                                    </ng-autocomplete>\r\n                                    \r\n                                    <ng-template #templateVillage let-item>\r\n                                        <a [innerHTML]=\"item.village\"></a>\r\n                                    </ng-template>\r\n                                </div>\r\n\r\n                                <label>Kode Pos</label>\r\n                                <form-input name=\"kode_pos_rumah\"  [type]=\"'text'\" [placeholder]=\"'Kode Pos'\"  [(ngModel)]=\"updateSingle.kode_pos_rumah\" autofocus required></form-input>\r\n\r\n                                <div *ngIf=\"programType != 'reguler'\">\r\n                                    <label>No. KTP Penerima</label>\r\n                                    <form-input name=\"ktp_penerima\"  [type]=\"'text'\" [placeholder]=\"'No. KTP Penerima'\"  [(ngModel)]=\"updateSingle.ktp_penerima\" autofocus required></form-input>\r\n                                </div>\r\n\r\n                                <!-- <div *ngIf=mci_project>\r\n                                    <label>Kota Penerima</label>\r\n                                    <form-input name=\"kota_rumah\"  [type]=\"'text'\" [placeholder]=\"'Kota Penerima'\"  [(ngModel)]=\"updateSingle.kota_rumah\" autofocus required></form-input>\r\n                                </div>\r\n\r\n                                <div *ngIf=mci_project>\r\n                                    <label>Kode Pos Penerima</label>\r\n                                    <form-input name=\"kode_pos_rumah\"  [type]=\"'text'\" [placeholder]=\"'Kode Pos Penerima'\"  [(ngModel)]=\"updateSingle.kode_pos_rumah\" autofocus required></form-input>\r\n                                </div> -->\r\n\r\n                                <div *ngIf=\"programType != 'reguler'\">\r\n                                    <label>Foto KTP Penerima</label>\r\n                                    <!-- <form-input name=\"foto_ktp_penerima\"  [type]=\"'text'\" [placeholder]=\"'Nama Toko'\"  [(ngModel)]=\"updateSingle.foto_ktp_penerima\" autofocus required></form-input> -->\r\n                                    <div class=\"member-detail-image-container\">\r\n                                        <div *ngIf=\"validURL(updateSingle.foto_ktp_penerima) && !loadingKTPPenerima\" class=\"img-upload\">\r\n                                            <pdf-viewer \r\n                                                *ngIf=\"checkFileType(updateSingle.foto_ktp_penerima) == 'document'\"\r\n                                                [src]=\"updateSingle.foto_ktp_penerima\"\r\n                                                [render-text]=\"false\"\r\n                                                [original-size]=\"false\"\r\n                                                [page] = \"1\"\r\n                                                [show-all]=\"false\"\r\n                                                style=\"width: 100%; height: 100%\">\r\n                                            </pdf-viewer>\r\n                                            <img *ngIf=\"checkFileType(updateSingle.foto_ktp_penerima) == 'image'\" src={{updateSingle.foto_ktp_penerima}} class=\"member-detail-image\" />\r\n                                        </div>\r\n\r\n                                        <div class=\"form-group uploaded\" *ngIf=\"loadingKTPPenerima\">\r\n                                            <div class=\"loading\">\r\n                                            <div class=\"sk-fading-circle\">\r\n                                                <div class=\"sk-circle1 sk-circle\"></div>\r\n                                                <div class=\"sk-circle2 sk-circle\"></div>\r\n                                                <div class=\"sk-circle3 sk-circle\"></div>\r\n                                                <div class=\"sk-circle4 sk-circle\"></div>\r\n                                                <div class=\"sk-circle5 sk-circle\"></div>\r\n                                                <div class=\"sk-circle6 sk-circle\"></div>\r\n                                                <div class=\"sk-circle7 sk-circle\"></div>\r\n                                                <div class=\"sk-circle8 sk-circle\"></div>\r\n                                                <div class=\"sk-circle9 sk-circle\"></div>\r\n                                                <div class=\"sk-circle10 sk-circle\"></div>\r\n                                                <div class=\"sk-circle11 sk-circle\"></div>\r\n                                                <div class=\"sk-circle12 sk-circle\"></div>\r\n                                            </div>\r\n                                            </div>\r\n                                        </div>\r\n\r\n                                        <!-- <ng-template #empty_image>\r\n                                            <span>\r\n                                                empty\r\n                                            </span>\r\n                                        </ng-template> -->\r\n                                        <div class=\"img-upload\">\r\n                                            <button class=\"btn_upload\" (click)=\"ktpPenerima.click()\">\r\n                                                <i class=\"fa fa-fw fa-upload\"></i>\r\n                                                <span *ngIf=\"validURL(updateSingle.foto_ktp_penerima); else upload_image\">Ubah</span>\r\n                                            </button>\r\n                                            <button  *ngIf=\"validURL(updateSingle.foto_ktp_penerima)\" class=\"btn_delete\" (click)=\"deletePhoto('foto_ktp_penerima', updateSingle.foto_ktp_penerima)\">\r\n                                                <i class=\"fa fa-fw fa-trash-alt\"></i>\r\n                                                <span>Hapus</span>\r\n                                            </button>\r\n                                        </div>\r\n                                        \r\n                                    </div>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"programType != 'reguler'\">\r\n                                    <label>No. NPWP Penerima</label>\r\n                                    <form-input name=\"npwp_penerima\"  [type]=\"'text'\" [placeholder]=\"'No. NPWP Penerima'\"  [(ngModel)]=\"updateSingle.npwp_penerima\" autofocus required></form-input>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"programType != 'reguler'\">\r\n                                    <label>Foto NPWP Penerima</label>\r\n                                    <!-- <form-input name=\"foto_npwp_penerima\"  [type]=\"'text'\" [placeholder]=\"'Nama Toko'\"  [(ngModel)]=\"updateSingle.foto_npwp_penerima\" autofocus required></form-input> -->\r\n                                    <div class=\"member-detail-image-container\">\r\n                                        <div *ngIf=\"validURL(updateSingle.foto_npwp_penerima) && !loadingNPWPPenerima\" class=\"img-upload\">\r\n                                            <pdf-viewer \r\n                                                *ngIf=\"checkFileType(updateSingle.foto_npwp_penerima) == 'document'\"\r\n                                                [src]=\"updateSingle.foto_npwp_penerima\"\r\n                                                [render-text]=\"false\"\r\n                                                [original-size]=\"false\"\r\n                                                [page] = \"1\"\r\n                                                [show-all]=\"false\"\r\n                                                style=\"width: 100%; height: 100%\">\r\n                                            </pdf-viewer>\r\n                                            <img *ngIf=\"checkFileType(updateSingle.foto_npwp_penerima) == 'image'\" src={{updateSingle.foto_npwp_penerima}} class=\"member-detail-image\" />\r\n                                        </div>\r\n\r\n                                        <div class=\"form-group uploaded\" *ngIf=\"loadingNPWPPenerima\">\r\n                                            <div class=\"loading\">\r\n                                            <div class=\"sk-fading-circle\">\r\n                                                <div class=\"sk-circle1 sk-circle\"></div>\r\n                                                <div class=\"sk-circle2 sk-circle\"></div>\r\n                                                <div class=\"sk-circle3 sk-circle\"></div>\r\n                                                <div class=\"sk-circle4 sk-circle\"></div>\r\n                                                <div class=\"sk-circle5 sk-circle\"></div>\r\n                                                <div class=\"sk-circle6 sk-circle\"></div>\r\n                                                <div class=\"sk-circle7 sk-circle\"></div>\r\n                                                <div class=\"sk-circle8 sk-circle\"></div>\r\n                                                <div class=\"sk-circle9 sk-circle\"></div>\r\n                                                <div class=\"sk-circle10 sk-circle\"></div>\r\n                                                <div class=\"sk-circle11 sk-circle\"></div>\r\n                                                <div class=\"sk-circle12 sk-circle\"></div>\r\n                                            </div>\r\n                                            </div>\r\n                                        </div>\r\n                                        \r\n                                        <!-- <ng-template #empty_image>\r\n                                            <span>\r\n                                                empty\r\n                                            </span>\r\n                                        </ng-template> -->\r\n                                        <div class=\"img-upload\">\r\n                                            <button class=\"btn_upload\" (click)=\"npwpPenerima.click()\">\r\n                                                <i class=\"fa fa-fw fa-upload\"></i>\r\n                                                <span *ngIf=\"validURL(updateSingle.foto_npwp_penerima); else upload_image\">Ubah</span>\r\n                                            </button>\r\n                                            <button  *ngIf=\"validURL(updateSingle.foto_npwp_penerima)\" class=\"btn_delete\" (click)=\"deletePhoto('foto_npwp_penerima', updateSingle.foto_npwp_penerima)\">\r\n                                                <i class=\"fa fa-fw fa-trash-alt\"></i>\r\n                                                <span>Hapus</span>\r\n                                            </button>\r\n                                        </div>\r\n\r\n                                    </div>\r\n                                </div>\r\n\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <!-- <div class=\"col-md-4\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Data Foto</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">             \r\n                                \r\n                                <label class=\"label-bold\">Foto BAST</label>\r\n                                <ng-container *ngIf=\"updateSingle.foto_bast && isArray(updateSingle.foto_bast) && updateSingle.foto_bast.length > 0\">\r\n                                        <div class=\"member-detail-image-container\" *ngFor=\"let itemImage of updateSingle.foto_bast; let i = index\">\r\n                                            <div *ngIf=\"validURL(itemImage.url) && !loadingBAST[i]\" class=\"img-upload\">\r\n                                                <img src={{itemImage.url}} class=\"member-detail-image\" />\r\n                                            </div>\r\n        \r\n                                            <div class=\"form-group uploaded\" *ngIf=\"loadingBAST[i]\">\r\n                                                <div class=\"loading\">\r\n                                                  <div class=\"sk-fading-circle\">\r\n                                                    <div class=\"sk-circle1 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle2 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle3 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle4 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle5 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle6 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle7 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle8 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle9 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle10 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle11 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle12 sk-circle\"></div>\r\n                                                  </div>\r\n                                                </div>\r\n                                            </div>\r\n        \r\n                                            <div class=\"img-upload\">\r\n                                                <button class=\"btn_upload\" (click)=\"openUpload('bast', i)\">\r\n                                                    <i class=\"fa fa-fw fa-upload\"></i>\r\n                                                    <span *ngIf=\"validURL(itemImage.url); else upload_image\">Ubah</span>\r\n                                                </button>\r\n                                                <button  *ngIf=\"validURL(itemImage.url)\" class=\"btn_delete\" (click)=\"deleteImage('bast', i, itemImage.url)\">\r\n                                                    <i class=\"fa fa-fw fa-trash-alt\"></i>\r\n                                                    <span>Hapus</span>\r\n                                                </button>\r\n                                            </div>\r\n                                            <div *ngIf=\"validURL(itemImage.url)\" style=\"text-align: center;\">\r\n                                                <span>\r\n                                                    <b>Last updated : {{convertDate(itemImage.updated_date)}}</b>\r\n                                                </span>\r\n                                            </div>\r\n\r\n                                            <div >\r\n                                                <label>Description</label>\r\n                                                <form-input  name=\"'descBast'+i\" [type]=\"'text'\" [placeholder]=\"'Description'\"  [(ngModel)]=\"updateSingle.foto_bast[i].desc\"></form-input>\r\n                                            </div>\r\n                                            \r\n                                        </div>\r\n                                </ng-container>\r\n\r\n                                <div class=\"add-more-container\">\r\n                                    <button (click)=\"addMoreUpload('bast')\" class=\"add-more-image\"><i class=\"fa fa-fw fa-plus-square\"></i> Tambah foto BAST</button>\r\n                                </div>\r\n                                \r\n                                <hr/>\r\n\r\n                                <label  class=\"label-bold\">Foto Surat Kuasa</label>\r\n                                <ng-container *ngIf=\"updateSingle.foto_surat_kuasa && isArray(updateSingle.foto_surat_kuasa) && updateSingle.foto_surat_kuasa.length > 0\">\r\n                                    <div class=\"member-detail-image-container\"  *ngFor=\"let itemImage of updateSingle.foto_surat_kuasa; let i = index\">\r\n                                        <div *ngIf=\"validURL(itemImage.url) && !loadingSuratKuasa[i]\" class=\"img-upload\">\r\n                                            <img src={{itemImage.url}} class=\"member-detail-image\" />\r\n                                        </div>\r\n    \r\n                                        <div class=\"form-group uploaded\" *ngIf=\"loadingSuratKuasa[i]\">\r\n                                            <div class=\"loading\">\r\n                                              <div class=\"sk-fading-circle\">\r\n                                                <div class=\"sk-circle1 sk-circle\"></div>\r\n                                                <div class=\"sk-circle2 sk-circle\"></div>\r\n                                                <div class=\"sk-circle3 sk-circle\"></div>\r\n                                                <div class=\"sk-circle4 sk-circle\"></div>\r\n                                                <div class=\"sk-circle5 sk-circle\"></div>\r\n                                                <div class=\"sk-circle6 sk-circle\"></div>\r\n                                                <div class=\"sk-circle7 sk-circle\"></div>\r\n                                                <div class=\"sk-circle8 sk-circle\"></div>\r\n                                                <div class=\"sk-circle9 sk-circle\"></div>\r\n                                                <div class=\"sk-circle10 sk-circle\"></div>\r\n                                                <div class=\"sk-circle11 sk-circle\"></div>\r\n                                                <div class=\"sk-circle12 sk-circle\"></div>\r\n                                              </div>\r\n                                            </div>\r\n                                          </div>\r\n    \r\n                                        <div class=\"img-upload\">\r\n                                            <button class=\"btn_upload\" (click)=\"openUpload('suratKuasa', i)\">\r\n                                                <i class=\"fa fa-fw fa-upload\"></i>\r\n                                                <span *ngIf=\"validURL(itemImage.url); else upload_image\">Ubah</span>\r\n                                            </button>\r\n                                            <button  *ngIf=\"validURL(itemImage.url)\" class=\"btn_delete\" (click)=\"deleteImage('suratKuasa', i, itemImage.url)\">\r\n                                                <i class=\"fa fa-fw fa-trash-alt\"></i>\r\n                                                <span>Hapus</span>\r\n                                            </button>\r\n                                        </div>\r\n\r\n                                        <div *ngIf=\"validURL(itemImage.url)\" style=\"text-align: center;\">\r\n                                            <span>\r\n                                                <b>Last updated : {{convertDate(itemImage.updated_date)}}</b>\r\n                                            </span>\r\n                                        </div>\r\n\r\n                                        <div >\r\n                                            <label>Description</label>\r\n                                            <form-input  name=\"'descSuratKuasa'+i\" [type]=\"'text'\" [placeholder]=\"'Description'\"  [(ngModel)]=\"updateSingle.foto_surat_kuasa[i].desc\"></form-input>\r\n                                        </div>\r\n\r\n                                    </div>\r\n                                </ng-container>\r\n\r\n                                <div class=\"add-more-container\">\r\n                                    <button (click)=\"addMoreUpload('suratKuasa')\" class=\"add-more-image\"><i class=\"fa fa-fw fa-plus-square\"></i> Tambah foto Surat Kuasa</button>\r\n                                </div>\r\n\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div> -->\r\n\r\n            </div>\r\n     \r\n         </div>\r\n    </div>\r\n    <ng-template #upload_image>\r\n        <span>\r\n            upload\r\n        </span>\r\n    </ng-template>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/form-member/form-member.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/form-member/form-member.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n       \r\n  <app-page-header [heading]=\"'Member'\" [icon]=\"'fa-table'\"></app-page-header>\r\n  \r\n  <!-- <button (click)=\"getDownloadFileLint()\"><i class=\"fa fa-file-pdf-o\"></i>Download</button> -->\r\n  <!-- <iframe [src]=\"srcDownload\"></iframe> -->\r\n  <div *ngIf=\"Members&&mci_project==false&&programType!='custom_kontraktual'&&memberDetail==false&&!errorMessage\">\r\n        <app-form-builder-table \r\n        [tableFormat] = \"tableFormat\" \r\n        [table_data]  = \"Members\" \r\n        [searchCallback]= \"[service, 'searchMemberAllReport',this]\" \r\n        [total_page]=\"totalPage\"\r\n        (onPageChange)=\"doPageChange($event)\"\r\n        >\r\n            \r\n        </app-form-builder-table>\r\n  </div>\r\n\r\n  <div *ngIf=\"Members&&mci_project==true&&programType!='custom_kontraktual'&&memberDetail==false&&!errorMessage\">\r\n    <app-form-builder-table \r\n    [tableFormat] = \"tableFormat2\" \r\n    [table_data]  = \"Members\" \r\n    [searchCallback]= \"[service, 'searchMemberAllReport',this]\" \r\n    [total_page]=\"totalPage\"\r\n    (onPageChange)=\"doPageChange($event)\"\r\n    >\r\n        \r\n    </app-form-builder-table>\r\n  </div>\r\n\r\n  <div *ngIf=\"Members&&mci_project==false&&programType=='custom_kontraktual'&&memberDetail==false&&!errorMessage\">\r\n    <app-form-builder-table \r\n    [tableFormat] = \"tableFormat3\" \r\n    [table_data]  = \"Members\" \r\n    [searchCallback]= \"[service, 'searchMemberAllReport',this]\" \r\n    [total_page]=\"totalPage\"\r\n    (onPageChange)=\"doPageChange($event)\"\r\n    >\r\n        \r\n    </app-form-builder-table>\r\n  </div>\r\n\r\n  <div *ngIf=\"errorMessage\" class=\"error-message\">\r\n    <div class=\"text-message\">\r\n      <i class=\"fas fa-ban\"></i>\r\n      {{errorMessage}}\r\n    </div>\r\n  </div>\r\n  <div *ngIf=\"memberDetail\">\r\n    <app-form-member-detail [back]=\"[this,backToHere]\" [detail]=\"memberDetail\"></app-form-member-detail>\r\n  </div>\r\n \r\n</div>\r\n\r\n "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/form-member/list-order/edit/list-order.edit.component.html":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/form-member/list-order/edit/list-order.edit.component.html ***!
  \*********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card card-detail\">\r\n    <!-- <ng-container *ngIf=\"allBast.length > 0\"> -->\r\n        <!-- <ng-container *ngFor=\"let item of allBast; let i = index\"> -->\r\n            <input type=\"file\"  (change)=\"onFileSelected($event, 'bast', 0)\" \r\n                accept=\"image/x-png,image/gif,image/jpeg,image/jpeg,application/pdf\" style=\"display:none;\" id=\"bast\"/>\r\n        <!-- </ng-container> -->\r\n    <!-- </ng-container> -->\r\n\r\n    <!-- <ng-container *ngIf=\"allSuratKuasa.length > 0\"> -->\r\n        <!-- <ng-container *ngFor=\"let item of allSuratKuasa; let i = index\"> -->\r\n            <input type=\"file\"  (change)=\"onFileSelected($event, 'suratKuasa', 0)\" \r\n                accept=\"image/x-png,image/gif,image/jpeg,image/jpeg,application/pdf\" style=\"display:none;\" id=\"suratKuasa\"/>\r\n        <!-- </ng-container> -->\r\n    <!-- </ng-container> -->\r\n        \r\n            <input type=\"file\"  (change)=\"onFileSelected($event, 'others')\" \r\n                accept=\"image/x-png,image/gif,image/jpeg,image/jpeg,application/pdf\" style=\"display:none;\" id=\"others\"/>\r\n\r\n            <input type=\"file\"  (change)=\"onFileSelected($event, 'receipt')\" \r\n                accept=\"image/x-png,image/gif,image/jpeg,image/jpeg,application/pdf\" style=\"display:none;\" id=\"receipt\"/>\r\n    \r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToDetail()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n        <!-- <h1>ID Pelanggan : {{order.username}}</h1> -->\r\n        <!-- <button class=\"btn save_button {{loading}}\" type=\"submit\" (click)=\"saveThis()\"><i class=\"fa fa-fw fa-save\"></i> <span  *ngIf=\"!loading\">Save</span> <span *ngIf=\"loading\">loading</span></button> -->\r\n    </div>\r\n    <div class=\"card-content\">\r\n        <div class=\"member-detail\">\r\n            <div class=\"row\">\r\n            <div class=\"col-md-8 col-sm-12\">\r\n                <div class=\"row\">\r\n\r\n                <div class=\"col-sm-12\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Order ID : {{order.order_id}}</h2>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                        <div class=\"row\">\r\n                            <div class=\"col-md-6 col-sm-12\">             \r\n                                <label class=\"label-bold\">Foto BAST</label>\r\n\r\n                                    <div class=\"member-detail-image-container\">\r\n                                        <div *ngIf=\"order.additional_info && order.additional_info.bast && validURL(order.additional_info.bast.pic_file_name) && !loadingFotoBAST\" class=\"img-upload\">\r\n                                            <img *ngIf=\"checkFileType(order.additional_info.bast.pic_file_name) == 'image'\" src={{order.additional_info.bast.pic_file_name}} class=\"member-detail-image\" />\r\n                                            <pdf-viewer \r\n                                                *ngIf=\"checkFileType(order.additional_info.bast.pic_file_name) == 'document'\"\r\n                                                [src]=\"order.additional_info.bast.pic_file_name\"\r\n                                                [render-text]=\"false\"\r\n                                                [original-size]=\"false\"\r\n                                                [page] = \"1\"\r\n                                                [show-all]=\"false\"\r\n                                                style=\"width: 100%; height: 100%\">\r\n                                            </pdf-viewer>                                            \r\n                                            <!-- <img src={{order.additional_info.bast.pic_file_name}} class=\"member-detail-image\" /> -->\r\n                                        </div>\r\n    \r\n                                        <div class=\"form-group uploaded\" *ngIf=\"loadingFotoBAST\">\r\n                                            <div class=\"loading\">\r\n                                              <div class=\"sk-fading-circle\">\r\n                                                <div class=\"sk-circle1 sk-circle\"></div>\r\n                                                <div class=\"sk-circle2 sk-circle\"></div>\r\n                                                <div class=\"sk-circle3 sk-circle\"></div>\r\n                                                <div class=\"sk-circle4 sk-circle\"></div>\r\n                                                <div class=\"sk-circle5 sk-circle\"></div>\r\n                                                <div class=\"sk-circle6 sk-circle\"></div>\r\n                                                <div class=\"sk-circle7 sk-circle\"></div>\r\n                                                <div class=\"sk-circle8 sk-circle\"></div>\r\n                                                <div class=\"sk-circle9 sk-circle\"></div>\r\n                                                <div class=\"sk-circle10 sk-circle\"></div>\r\n                                                <div class=\"sk-circle11 sk-circle\"></div>\r\n                                                <div class=\"sk-circle12 sk-circle\"></div>\r\n                                              </div>\r\n                                            </div>\r\n                                        </div>\r\n    \r\n                                        <div class=\"img-upload\">\r\n                                            <button class=\"btn_upload\" (click)=\"openUpload('bast')\">\r\n                                                <i class=\"fa fa-fw fa-upload\"></i>\r\n                                                <span *ngIf=\"order.additional_info && order.additional_info.bast && validURL(order.additional_info.bast.pic_file_name); else upload_image\">Ubah</span>\r\n                                            </button>\r\n                                        </div>\r\n                                        <div *ngIf=\"order.additional_info && order.additional_info.bast && validURL(order.additional_info.bast.pic_file_name)\" style=\"text-align: center;\">\r\n                                            <span>\r\n                                                <b>Last updated : {{order.additional_info.bast.created_date}}</b>\r\n                                            </span>\r\n                                        </div>\r\n               \r\n                                    </div>\r\n                                \r\n                            </div>\r\n\r\n\r\n                            <div class=\"col-md-6 col-sm-12\">             \r\n                                <label class=\"label-bold\">Foto Surat Kuasa</label>\r\n\r\n                                    <div class=\"member-detail-image-container\">\r\n                                        <div *ngIf=\"order.additional_info && order.additional_info.surat_kuasa && validURL(order.additional_info.surat_kuasa.pic_file_name) && !loadingFotoSuratKuasa\" class=\"img-upload\">\r\n                                            <!-- <img src={{order.additional_info.surat_kuasa.pic_file_name}} class=\"member-detail-image\" /> -->\r\n                                            <img *ngIf=\"checkFileType(order.additional_info.surat_kuasa.pic_file_name) == 'image'\" src={{order.additional_info.surat_kuasa.pic_file_name}} class=\"member-detail-image\" />\r\n                                            <pdf-viewer \r\n                                                *ngIf=\"checkFileType(order.additional_info.surat_kuasa.pic_file_name) == 'document'\"\r\n                                                [src]=\"order.additional_info.surat_kuasa.pic_file_name\"\r\n                                                [render-text]=\"false\"\r\n                                                [original-size]=\"false\"\r\n                                                [page] = \"1\"\r\n                                                [show-all]=\"false\"\r\n                                                style=\"width: 100%; height: 100%\">\r\n                                            </pdf-viewer>                                            \r\n                                        </div>\r\n    \r\n                                        <div class=\"form-group uploaded\" *ngIf=\"loadingFotoSuratKuasa\">\r\n                                            <div class=\"loading\">\r\n                                              <div class=\"sk-fading-circle\">\r\n                                                <div class=\"sk-circle1 sk-circle\"></div>\r\n                                                <div class=\"sk-circle2 sk-circle\"></div>\r\n                                                <div class=\"sk-circle3 sk-circle\"></div>\r\n                                                <div class=\"sk-circle4 sk-circle\"></div>\r\n                                                <div class=\"sk-circle5 sk-circle\"></div>\r\n                                                <div class=\"sk-circle6 sk-circle\"></div>\r\n                                                <div class=\"sk-circle7 sk-circle\"></div>\r\n                                                <div class=\"sk-circle8 sk-circle\"></div>\r\n                                                <div class=\"sk-circle9 sk-circle\"></div>\r\n                                                <div class=\"sk-circle10 sk-circle\"></div>\r\n                                                <div class=\"sk-circle11 sk-circle\"></div>\r\n                                                <div class=\"sk-circle12 sk-circle\"></div>\r\n                                              </div>\r\n                                            </div>\r\n                                        </div>\r\n    \r\n                                        <div class=\"img-upload\">\r\n                                            <button class=\"btn_upload\" (click)=\"openUpload('suratKuasa')\">\r\n                                                <i class=\"fa fa-fw fa-upload\"></i>\r\n                                                <span *ngIf=\"order.additional_info && order.additional_info.surat_kuasa && validURL(order.additional_info.surat_kuasa.pic_file_name); else upload_image\">Ubah</span>\r\n                                            </button>\r\n                                        </div>\r\n                                        <div *ngIf=\"order.additional_info && order.additional_info.surat_kuasa && validURL(order.additional_info.surat_kuasa.pic_file_name)\" style=\"text-align: center;\">\r\n                                            <span>\r\n                                                <b>Last updated : {{order.additional_info.surat_kuasa.created_date}}</b>\r\n                                            </span>\r\n                                        </div>                                            \r\n\r\n                                    </div>\r\n                                    \r\n                            </div>\r\n                        </div>\r\n                        </div>\r\n\r\n                        <!-- Receipt -->\r\n                        <div class=\"card-content\">\r\n                            <div class=\"row\">\r\n                                <ng-container  *ngIf=\"order.additional_info && order.additional_info.receipt && order.additional_info.receipt.length > 0\">\r\n                                    <div class=\"col-md-6 col-sm-12\" *ngFor=\"let receipt of order.additional_info.receipt; let i = index\">\r\n                                        <input type=\"file\" (change)=\"onFileSelected($event, 'receipt', i)\" accept=\"image/x-png,image/gif,image/jpeg,application/pdf\" style=\"display:none;\" [id]=\"'receipt'+i\"/>             \r\n                                        <label class=\"label-bold\">Receipt</label>\r\n                                            <div class=\"member-detail-image-container\">\r\n                                                <div *ngIf=\"!loadingReceipt[i]\" class=\"img-upload\">\r\n                                                    <img *ngIf=\"checkFileType(receipt.pic_file_name) == 'image'\" src={{receipt.pic_file_name}} class=\"member-detail-image\" />\r\n                                                    <pdf-viewer \r\n                                                        *ngIf=\"checkFileType(receipt.pic_file_name) == 'document'\"\r\n                                                        [src]=\"receipt.pic_file_name\"\r\n                                                        [render-text]=\"false\"\r\n                                                        [original-size]=\"false\"\r\n                                                        [page] = \"1\"\r\n                                                        [show-all]=\"false\"\r\n                                                        style=\"width: 100%; height: 100%\">\r\n                                                    </pdf-viewer>                                                    \r\n                                                </div>\r\n\r\n                                                <div class=\"form-group uploaded\" *ngIf=\"loadingReceipt[i]\">\r\n                                                    <div class=\"loading\">\r\n                                                      <div class=\"sk-fading-circle\">\r\n                                                        <div class=\"sk-circle1 sk-circle\"></div>\r\n                                                        <div class=\"sk-circle2 sk-circle\"></div>\r\n                                                        <div class=\"sk-circle3 sk-circle\"></div>\r\n                                                        <div class=\"sk-circle4 sk-circle\"></div>\r\n                                                        <div class=\"sk-circle5 sk-circle\"></div>\r\n                                                        <div class=\"sk-circle6 sk-circle\"></div>\r\n                                                        <div class=\"sk-circle7 sk-circle\"></div>\r\n                                                        <div class=\"sk-circle8 sk-circle\"></div>\r\n                                                        <div class=\"sk-circle9 sk-circle\"></div>\r\n                                                        <div class=\"sk-circle10 sk-circle\"></div>\r\n                                                        <div class=\"sk-circle11 sk-circle\"></div>\r\n                                                        <div class=\"sk-circle12 sk-circle\"></div>\r\n                                                      </div>\r\n                                                    </div>\r\n                                                  </div>\r\n            \r\n                                                <div class=\"img-upload\"*ngIf=\"receipt.receipt_id\">\r\n                                                    <button class=\"btn_upload\" (click)=\"openUpload('receipt', i)\">\r\n                                                        <i class=\"fa fa-fw fa-upload\"></i>\r\n                                                        <span *ngIf=\"validURL(receipt.pic_file_name); else upload_image\">Ubah</span>\r\n                                                    </button>\r\n                                                    <button  *ngIf=\"validURL(receipt.pic_file_name)\" class=\"btn_delete\" (click)=\"deleteReceipt('receipt', receipt.receipt_id, receipt.pic_file_name, idx)\">\r\n                                                        <i class=\"fa fa-fw fa-trash-alt\"></i>\r\n                                                        <span>Hapus</span>\r\n                                                    </button>\r\n                                                </div>\r\n            \r\n                                                <div style=\"text-align: center;\">\r\n                                                    <span>\r\n                                                        <b>Uploaded date : {{receipt.created_date}}</b>\r\n                                                    </span>\r\n                                                </div>\r\n                                            </div>\r\n                                        </div>\r\n                                    </ng-container>\r\n\r\n                                    <div class=\"col-md-6 col-sm-12\">\r\n                                        <label class=\"label-bold\"><i class=\"fa fa-fw fa-plus\"></i>Foto Receipt/Bukti Pembayaran</label>             \r\n                                        <div class=\"member-detail-image-container\">\r\n                                            <div class=\"form-group uploaded\" *ngIf=\"loadingFotoReceipt\">\r\n                                                <div class=\"loading\">\r\n                                                  <div class=\"sk-fading-circle\">\r\n                                                    <div class=\"sk-circle1 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle2 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle3 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle4 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle5 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle6 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle7 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle8 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle9 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle10 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle11 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle12 sk-circle\"></div>\r\n                                                  </div>\r\n                                                </div>\r\n                                            </div>\r\n        \r\n                                            <div class=\"img-upload\">\r\n                                                <button class=\"btn_upload\" (click)=\"openUpload('receipt')\">\r\n                                                    <i class=\"fa fa-fw fa-upload\"></i>\r\n                                                    <span>Upload</span>\r\n                                                </button>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                        </div>\r\n\r\n                        <!-- Others -->\r\n                        <div class=\"card-content\">\r\n                            <div class=\"row\">\r\n                                <ng-container  *ngIf=\"order.additional_info && order.additional_info.others && order.additional_info.others.length > 0\">\r\n                                    <div class=\"col-md-6 col-sm-12\" *ngFor=\"let other of order.additional_info.others; let i = index\">\r\n                                        <input type=\"file\"  (change)=\"onFileSelected($event, 'others', i)\" \r\n                accept=\"image/x-png,image/gif,image/jpeg,application/pdf\" style=\"display:none;\" [id]=\"'others'+i\"/>             \r\n                                        <label class=\"label-bold\">Others</label>\r\n                                            <div class=\"member-detail-image-container\">\r\n                                                <div *ngIf=\"!loadingOthers[i]\" class=\"img-upload\">\r\n                                                    <img *ngIf=\"checkFileType(other.pic_file_name) == 'image'\" src={{other.pic_file_name}} class=\"member-detail-image\" />\r\n                                                    <pdf-viewer \r\n                                                        *ngIf=\"checkFileType(other.pic_file_name) == 'document'\"\r\n                                                        [src]=\"other.pic_file_name\"\r\n                                                        [render-text]=\"false\"\r\n                                                        [original-size]=\"false\"\r\n                                                        [page] = \"1\"\r\n                                                        [show-all]=\"false\"\r\n                                                        style=\"width: 100%; height: 100%\">\r\n                                                    </pdf-viewer>                                                    \r\n                                                </div>\r\n\r\n                                                <div class=\"form-group uploaded\" *ngIf=\"loadingOthers[i]\">\r\n                                                    <div class=\"loading\">\r\n                                                      <div class=\"sk-fading-circle\">\r\n                                                        <div class=\"sk-circle1 sk-circle\"></div>\r\n                                                        <div class=\"sk-circle2 sk-circle\"></div>\r\n                                                        <div class=\"sk-circle3 sk-circle\"></div>\r\n                                                        <div class=\"sk-circle4 sk-circle\"></div>\r\n                                                        <div class=\"sk-circle5 sk-circle\"></div>\r\n                                                        <div class=\"sk-circle6 sk-circle\"></div>\r\n                                                        <div class=\"sk-circle7 sk-circle\"></div>\r\n                                                        <div class=\"sk-circle8 sk-circle\"></div>\r\n                                                        <div class=\"sk-circle9 sk-circle\"></div>\r\n                                                        <div class=\"sk-circle10 sk-circle\"></div>\r\n                                                        <div class=\"sk-circle11 sk-circle\"></div>\r\n                                                        <div class=\"sk-circle12 sk-circle\"></div>\r\n                                                      </div>\r\n                                                    </div>\r\n                                                  </div>\r\n            \r\n                                                <div class=\"img-upload\"*ngIf=\"other.id\">\r\n                                                    <button class=\"btn_upload\" (click)=\"openUpload('others', i)\">\r\n                                                        <i class=\"fa fa-fw fa-upload\"></i>\r\n                                                        <span *ngIf=\"validURL(other.pic_file_name); else upload_image\">Ubah</span>\r\n                                                    </button>\r\n                                                    <button  *ngIf=\"validURL(other.pic_file_name)\" class=\"btn_delete\" (click)=\"deleteImage('others', other.id, other.pic_file_name, idx)\">\r\n                                                        <i class=\"fa fa-fw fa-trash-alt\"></i>\r\n                                                        <span>Hapus</span>\r\n                                                    </button>\r\n                                                </div>\r\n            \r\n                                                <div style=\"text-align: center;\">\r\n                                                    <span>\r\n                                                        <b>Uploaded date : {{other.created_date}}</b>\r\n                                                    </span>\r\n                                                </div>\r\n                       \r\n                                            </div>\r\n                                        \r\n                                        </div>\r\n                                </ng-container>\r\n                                \r\n\r\n                                    <div class=\"col-md-6 col-sm-12\">\r\n                                        <label class=\"label-bold\"><i class=\"fa fa-fw fa-plus\"></i>Tambah foto lain</label>             \r\n                                        <div class=\"member-detail-image-container\">\r\n                                            <div class=\"form-group uploaded\" *ngIf=\"loadingFotoOthers\">\r\n                                                <div class=\"loading\">\r\n                                                  <div class=\"sk-fading-circle\">\r\n                                                    <div class=\"sk-circle1 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle2 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle3 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle4 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle5 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle6 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle7 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle8 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle9 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle10 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle11 sk-circle\"></div>\r\n                                                    <div class=\"sk-circle12 sk-circle\"></div>\r\n                                                  </div>\r\n                                                </div>\r\n                                            </div>\r\n        \r\n                                            <div class=\"img-upload\">\r\n                                                <button class=\"btn_upload\" (click)=\"openUpload('others')\">\r\n                                                    <i class=\"fa fa-fw fa-upload\"></i>\r\n                                                    <span>Upload</span>\r\n                                                </button>\r\n                                            </div>\r\n                   \r\n                                        </div>\r\n                                    \r\n                                    </div>\r\n\r\n                                </div>\r\n                        </div>\r\n\r\n                        </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                \r\n                <div class=\"col-md-4 col-sm-12\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-sm-12\">\r\n                            <div class=\"card mb-3\">\r\n                                <div class=\"card-header\"><h2>Data Pemilik dan Toko</h2></div>\r\n                                <div class=\"card-content\">\r\n                                    <div class=\"col-md-12\">             \r\n                                        <label>ID Pelanggan</label>\r\n                                        <div name=\"member_id\">{{memberDetail.username}}</div>\r\n        \r\n                                        <label>Data Complete</label>\r\n                                        <div name=\"member_id\">{{memberDetail.additional_info && memberDetail.additional_info.data_complete ? memberDetail.additional_info.data_complete : '-'}}</div>\r\n                                        \r\n                                        <label>Nama Toko</label>\r\n                                        <div name=\"full_name\">{{memberDetail.input_form_data.nama_toko}}</div>\r\n                                        \r\n                                        <label>Nama Distributor</label>\r\n                                        <div name=\"full_name\">{{memberDetail.input_form_data.nama_distributor}}</div>\r\n        \r\n                                        <label>Alamat Toko</label>\r\n                                        <div name=\"full_name\">{{memberDetail.input_form_data.alamat_toko}}</div>\r\n        \r\n                                        <label>Nama Pemilik</label>\r\n                                        <div name=\"full_name\">{{memberDetail.input_form_data.nama_pemilik}}</div>\r\n        \r\n                                        <label>No. WA Pemilik</label>\r\n                                        <div name=\"full_name\">{{memberDetail.input_form_data.no_wa_pemilik}}</div>\r\n                                        \r\n                                        <label>No. KTP Pemilik</label>\r\n                                        <div name=\"full_name\">{{memberDetail.input_form_data.ktp_pemilik}}</div>\r\n        \r\n                                        <label>Foto KTP Pemilik</label>\r\n                                        <div class=\"member-detail-image-container\">\r\n                                            <ng-container  *ngIf=\"validURL(memberDetail.input_form_data.foto_ktp_pemilik); else empty_image\">\r\n                                                <a href={{memberDetail.input_form_data.foto_ktp_pemilik}} target=\"_blank\">\r\n                                                    <img *ngIf=\"checkFileType(memberDetail.input_form_data.foto_ktp_pemilik) == 'image'\" src={{memberDetail.input_form_data.foto_ktp_pemilik}} class=\"member-detail-image\" />\r\n                                                    <pdf-viewer \r\n                                                        *ngIf=\"checkFileType(memberDetail.input_form_data.foto_ktp_pemilik) == 'document'\"\r\n                                                        [src]=\"memberDetail.input_form_data.foto_ktp_pemilik\"\r\n                                                        [render-text]=\"false\"\r\n                                                        [original-size]=\"false\"\r\n                                                        [page] = \"1\"\r\n                                                        [show-all]=\"false\"\r\n                                                        style=\"width: 100%; height: 100%\">\r\n                                                    </pdf-viewer>\r\n                                                </a>\r\n                                                <div class=\"btn-download-container\">\r\n                                                    <button (click)=\"downloadImage(memberDetail.input_form_data.foto_ktp_pemilik)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                                </div>\r\n                                            </ng-container>\r\n                                            \r\n                                            \r\n                                        </div>\r\n        \r\n                                        <label>No. NPWP Pemilik</label>\r\n                                        <div name=\"full_name\">{{memberDetail.input_form_data.npwp_pemilik}}</div>\r\n        \r\n                                        <label>Foto NPWP Pemilik</label>\r\n                                        <div class=\"member-detail-image-container\">\r\n                                            <ng-container  *ngIf=\"validURL(memberDetail.input_form_data.foto_npwp_pemilik); else empty_image\">\r\n                                                <a href={{memberDetail.input_form_data.foto_npwp_pemilik}} target=\"_blank\">\r\n                                                    <img *ngIf=\"checkFileType(memberDetail.input_form_data.foto_npwp_pemilik) == 'image'\" src={{memberDetail.input_form_data.foto_npwp_pemilik}} class=\"member-detail-image\" />\r\n                                                    <pdf-viewer \r\n                                                        *ngIf=\"checkFileType(memberDetail.input_form_data.foto_npwp_pemilik) == 'document'\"\r\n                                                        [src]=\"memberDetail.input_form_data.foto_npwp_pemilik\"\r\n                                                        [render-text]=\"false\"\r\n                                                        [original-size]=\"false\"\r\n                                                        [page] = \"1\"\r\n                                                        [show-all]=\"false\"\r\n                                                        style=\"width: 100%; height: 100%\">\r\n                                                    </pdf-viewer>\r\n                                                </a>\r\n                                                <div class=\"btn-download-container\">\r\n                                                    <button (click)=\"downloadImage(memberDetail.input_form_data.foto_npwp_pemilik)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                                </div>\r\n                                            </ng-container>\r\n                                        </div>\r\n        \r\n                                    </div>\r\n                                \r\n                                </div>\r\n                            </div>\r\n    \r\n                            <div class=\"card mb-3\">\r\n                                <div class=\"card-header\"><h2>Data Penerima </h2></div>\r\n                                <div class=\"card-content\">\r\n                                    <div class=\"col-md-12\">\r\n        \r\n                                        <label>Hadiah Dikuasakan</label>\r\n                                        <div name=\"email\">{{memberDetail.input_form_data.hadiah_dikuasakan}}</div>\r\n                                    \r\n                                        <label>Nama Penerima</label>\r\n                                        <div name=\"email\">{{memberDetail.input_form_data.nama_penerima_gopay}}</div>\r\n        \r\n                                        <label>No. WA Penerima</label>\r\n                                        <div name=\"email\">{{memberDetail.input_form_data.no_wa_penerima}}</div>\r\n        \r\n                                        <label>No. Gopay Penerima<div style=\"font-size: 10px;\">(Jika hadiah berupa Saldo Gopay, isi dengan No. Gopay Penerima)</div></label>\r\n                                        <div name=\"email\">{{memberDetail.input_form_data.telp_penerima_gopay}}</div>\r\n        \r\n                                       <label>Alamat Pengiriman</label>\r\n                                       <div name=\"email\">{{memberDetail.input_form_data.alamat_rumah}}</div>\r\n        \r\n                                       <label>No. KTP Penerima</label>\r\n                                       <div name=\"email\">{{memberDetail.input_form_data.ktp_penerima}}</div>\r\n        \r\n                                       <label>Foto KTP Penerima</label>\r\n                                        <div class=\"member-detail-image-container\">\r\n                                            <ng-container *ngIf=\"validURL(memberDetail.input_form_data.foto_ktp_penerima); else empty_image\">\r\n                                                <a href={{memberDetail.input_form_data.foto_ktp_penerima}} target=\"_blank\" >\r\n                                                    <img *ngIf=\"checkFileType(memberDetail.input_form_data.foto_ktp_penerima) == 'image'\" src={{memberDetail.input_form_data.foto_ktp_penerima}} class=\"member-detail-image\" />\r\n                                                    <pdf-viewer \r\n                                                        *ngIf=\"checkFileType(memberDetail.input_form_data.foto_ktp_penerima) == 'document'\"\r\n                                                        [src]=\"memberDetail.input_form_data.foto_ktp_penerima\"\r\n                                                        [render-text]=\"false\"\r\n                                                        [original-size]=\"false\"\r\n                                                        [page] = \"1\"\r\n                                                        [show-all]=\"false\"\r\n                                                        style=\"width: 100%; height: 100%\">\r\n                                                    </pdf-viewer>\r\n                                                </a>\r\n                                                <div class=\"btn-download-container\">\r\n                                                    <button (click)=\"downloadImage(memberDetail.input_form_data.foto_ktp_penerima)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                                </div>\r\n                                            </ng-container>\r\n                                        </div>\r\n        \r\n                                       <label>No. NPWP Penerima</label>\r\n                                       <div name=\"email\">{{memberDetail.input_form_data.npwp_penerima}}</div>\r\n        \r\n                                       <label>Foto NPWP Penerima</label>\r\n                                       <div class=\"member-detail-image-container\">\r\n                                           <ng-container *ngIf=\"validURL(memberDetail.input_form_data.foto_npwp_penerima); else empty_image\">\r\n                                               <a href={{memberDetail.input_form_data.foto_npwp_penerima}} target=\"_blank\">\r\n                                                <img *ngIf=\"checkFileType(memberDetail.input_form_data.foto_npwp_penerima) == 'image'\" src={{memberDetail.input_form_data.foto_npwp_penerima}} class=\"member-detail-image\" />\r\n                                                <pdf-viewer \r\n                                                    *ngIf=\"checkFileType(memberDetail.input_form_data.foto_npwp_penerima) == 'document'\"\r\n                                                    [src]=\"memberDetail.input_form_data.foto_npwp_penerima\"\r\n                                                    [render-text]=\"false\"\r\n                                                    [original-size]=\"false\"\r\n                                                    [page] = \"1\"\r\n                                                    [show-all]=\"false\"\r\n                                                    style=\"width: 100%; height: 100%\">\r\n                                                </pdf-viewer>\r\n                                               </a>\r\n                                               <div class=\"btn-download-container\">\r\n                                                   <button (click)=\"downloadImage(memberDetail.input_form_data.foto_npwp_penerima)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                               </div>\r\n                                           </ng-container>\r\n                                        </div>\r\n                                    </div>\r\n                                 </div>\r\n                            </div> \r\n                        </div> \r\n                    </div>             \r\n                </div>\r\n         </div>\r\n    </div>\r\n    <ng-template #upload_image>\r\n        <span>\r\n            upload\r\n        </span>\r\n    </ng-template>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/form-member/list-order/list-order.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/form-member/list-order/list-order.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!edit\">\r\n    <div class=\"card card-detail\">\r\n        <div class=\"card-header\">\r\n            <button class=\"btn back_button\" (click)=\"backToDetail()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n            <!-- <h1>ID Pelanggan : {{memberDetail.username}}</h1> -->\r\n            <!-- <div class=\"edit_button\">\r\n                <button class=\"btn btn-right\" (click)=\"editThis()\"><i class=\"fa fa-fw fa-pencil-alt\"></i>edit</button>\r\n            </div> -->\r\n        </div>\r\n        <div class=\"card-content\" *ngIf=\"!errorMessage\">\r\n            <div class=\"member-detail\">\r\n                <div class=\"row\">\r\n                <div class=\"col-md-8 col-sm-12\">\r\n                    <div class=\"row\">\r\n\r\n                    <div class=\"col-sm-12\" *ngFor=\"let order of listOrder\">\r\n                        <div class=\"card mb-3\">\r\n                            <div class=\"card-header\"><h2>Order ID : {{order.order_id}}</h2>\r\n                                <div class=\"edit_button\">\r\n                                    <button class=\"btn btn-right\" (click)=\"editThis(order)\"><i class=\"fa fa-fw fa-pencil-alt\"></i>edit</button>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"card-content\">\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-6 col-sm-12\">             \r\n                                    <label class=\"label-bold\">Foto BAST</label>\r\n                                    <ng-container *ngIf=\"order.additional_info; else empty_image\">\r\n\r\n                                        <div class=\"member-detail-image-container\" *ngIf=\"order.additional_info.bast && order.additional_info.bast.pic_file_name; else empty_image\">\r\n                                            <ng-container>\r\n                                                <a href={{order.additional_info.bast.pic_file_name}} target=\"_blank\">\r\n                                                    <img *ngIf=\"checkFileType(order.additional_info.bast.pic_file_name) == 'image'\" src={{order.additional_info.bast.pic_file_name}} class=\"member-detail-image\" />\r\n                                                    <pdf-viewer \r\n                                                        *ngIf=\"checkFileType(order.additional_info.bast.pic_file_name) == 'document'\"\r\n                                                        [src]=\"order.additional_info.bast.pic_file_name\"\r\n                                                        [render-text]=\"false\"\r\n                                                        [original-size]=\"false\"\r\n                                                        [page] = \"1\"\r\n                                                        [show-all]=\"false\"\r\n                                                        style=\"width: 100%; height: 100%\">\r\n                                                    </pdf-viewer>\r\n                                                </a>\r\n                                                <div class=\"btn-download-container\">\r\n                                                    <button (click)=\"downloadImage(order.additional_info.bast.pic_file_name)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                                </div>\r\n                                                <div style=\"text-align: center;\">\r\n                                                    <span>\r\n                                                        <b>Last updated : {{order.additional_info.bast.created_date}}</b>\r\n                                                    </span>\r\n                                                </div>\r\n    \r\n                                                <br/>\r\n                                                <!-- <div>\r\n                                                    <span><b>Description :</b></span>\r\n                                                    <span>\r\n                                                        {{itemImage.desc}}\r\n                                                    </span>\r\n                                                </div> -->\r\n    \r\n                                            </ng-container>\r\n                                        </div>\r\n\r\n                                        <!-- <div class=\"member-detail-image-container\" *ngIf=\"order.additional_info.surat_kuasa && order.additional_info.surat_kuasa.pic_file_name\">\r\n                                            <ng-container>\r\n                                                <a href={{order.additional_info.surat_kuasa.pic_file_name}} target=\"_blank\">\r\n                                                    <img src={{order.additional_info.surat_kuasa.pic_file_name}} class=\"member-detail-image\" />\r\n                                                </a>\r\n                                                <div class=\"btn-download-container\">\r\n                                                    <button (click)=\"downloadImage(order.additional_info.surat_kuasa.pic_file_name)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                                </div>\r\n                                                <div style=\"text-align: center;\">\r\n                                                    <span>\r\n                                                        <b>Last updated : {{order.additional_info.surat_kuasa.created_date}}</b>\r\n                                                    </span>\r\n                                                </div>\r\n    \r\n                                                <br/>\r\n    \r\n                                            </ng-container>\r\n                                        </div> -->\r\n\r\n                                        \r\n                                    </ng-container>\r\n    \r\n                                    \r\n                                </div>\r\n\r\n\r\n                                <div class=\"col-md-6 col-sm-12\">             \r\n                                    <label class=\"label-bold\">Foto Surat Kuasa</label>\r\n                                    <ng-container *ngIf=\"order.additional_info; else empty_image\">\r\n\r\n                                        <!-- <div class=\"member-detail-image-container\" *ngIf=\"order.additional_info.bast && order.additional_info.bast.pic_file_name\">\r\n                                            <ng-container>\r\n                                                <a href={{order.additional_info.bast.pic_file_name}} target=\"_blank\">\r\n                                                    <img src={{order.additional_info.bast.pic_file_name}} class=\"member-detail-image\" />\r\n                                                </a>\r\n                                                <div class=\"btn-download-container\">\r\n                                                    <button (click)=\"downloadImage(order.additional_info.bast.pic_file_name)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                                </div>\r\n                                                <div style=\"text-align: center;\">\r\n                                                    <span>\r\n                                                        <b>Last updated : {{order.additional_info.bast.created_date}}</b>\r\n                                                    </span>\r\n                                                </div>\r\n    \r\n                                                <br/>\r\n    \r\n                                            </ng-container>\r\n                                        </div> -->\r\n\r\n                                        <div class=\"member-detail-image-container\" *ngIf=\"order.additional_info.surat_kuasa && order.additional_info.surat_kuasa.pic_file_name; else empty_image\">\r\n                                            <ng-container>\r\n                                                <a href={{order.additional_info.surat_kuasa.pic_file_name}} target=\"_blank\">\r\n                                                    <img *ngIf=\"checkFileType(order.additional_info.surat_kuasa.pic_file_name) == 'image'\" src={{order.additional_info.surat_kuasa.pic_file_name}} class=\"member-detail-image\" />\r\n                                                    <pdf-viewer \r\n                                                        *ngIf=\"checkFileType(order.additional_info.surat_kuasa.pic_file_name) == 'document'\"\r\n                                                        [src]=\"order.additional_info.surat_kuasa.pic_file_name\"\r\n                                                        [render-text]=\"false\"\r\n                                                        [original-size]=\"false\"\r\n                                                        [page] = \"1\"\r\n                                                        [show-all]=\"false\"\r\n                                                        style=\"width: 100%; height: 100%\">\r\n                                                    </pdf-viewer>\r\n                                                </a>\r\n                                                <div class=\"btn-download-container\">\r\n                                                    <button (click)=\"downloadImage(order.additional_info.surat_kuasa.pic_file_name)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                                </div>\r\n                                                <div style=\"text-align: center;\">\r\n                                                    <span>\r\n                                                        <b>Last updated : {{order.additional_info.surat_kuasa.created_date}}</b>\r\n                                                    </span>\r\n                                                </div>\r\n    \r\n                                                <br/>\r\n    \r\n                                            </ng-container>\r\n                                        </div>\r\n\r\n                                        \r\n                                    </ng-container>\r\n                                </div>\r\n                            </div>\r\n                            </div>\r\n\r\n                            <!-- Receipt -->\r\n                            <div class=\"card-content\" *ngIf=\"order.additional_info && order.additional_info.receipt && order.additional_info.receipt.length > 0; else empty_receipt\">\r\n                                <label class=\"label-bold\">Receipt</label>\r\n                                <div class=\"row\">\r\n                                    <ng-container>\r\n                                        <div class=\"col-md-6 col-sm-12\" *ngFor=\"let receipt of order.additional_info.receipt\">             \r\n                                                <div class=\"member-detail-image-container\">\r\n                                                    <a href={{receipt.pic_file_name}} target=\"_blank\">\r\n                                                        <img *ngIf=\"checkFileType(receipt.pic_file_name) == 'image'\" src={{receipt.pic_file_name}} class=\"member-detail-image\" />\r\n                                                        <pdf-viewer \r\n                                                            *ngIf=\"checkFileType(receipt.pic_file_name) == 'document'\"\r\n                                                            [src]=\"receipt.pic_file_name\"\r\n                                                            [render-text]=\"false\"\r\n                                                            [original-size]=\"false\"\r\n                                                            [page] = \"1\"\r\n                                                            [show-all]=\"false\"\r\n                                                            style=\"width: 100%; height: 100%\">\r\n                                                        </pdf-viewer>\r\n                                                    </a>\r\n                                                    <div class=\"btn-download-container\">\r\n                                                        <button (click)=\"downloadImage(receipt.pic_file_name)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                                    </div>\r\n                                                    <div style=\"text-align: center;\">\r\n                                                        <span>\r\n                                                            <b>Uploaded date : {{receipt.created_date}}</b>\r\n                                                        </span>\r\n                                                    </div>\r\n        \r\n                                                    <br/>\r\n                           \r\n                                                </div>\r\n                                            \r\n                                            </div>\r\n                                    </ng-container>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <!-- Others -->\r\n                            <div class=\"card-content\" *ngIf=\"order.additional_info && order.additional_info.others && order.additional_info.others.length > 0; else empty_others\">\r\n                                <label class=\"label-bold\">Others</label>\r\n                                <div class=\"row\">\r\n                                    <ng-container>\r\n                                        <div class=\"col-md-6 col-sm-12\" *ngFor=\"let other of order.additional_info.others\">             \r\n                                                <div class=\"member-detail-image-container\">\r\n                                                    <!-- <div class=\"img-upload\">\r\n                                                        <img src={{other.pic_file_name}} class=\"member-detail-image\" />\r\n                                                    </div>\r\n                \r\n                                                    <div style=\"text-align: center;\">\r\n                                                        <span>\r\n                                                            <b>Uploaded date : {{other.created_date}}</b>\r\n                                                        </span>\r\n                                                    </div> -->\r\n\r\n                                                    <a href={{other.pic_file_name}} target=\"_blank\">\r\n                                                        <img *ngIf=\"checkFileType(other.pic_file_name) == 'image'\" src={{other.pic_file_name}} class=\"member-detail-image\" />\r\n                                                        <pdf-viewer \r\n                                                            *ngIf=\"checkFileType(other.pic_file_name) == 'document'\"\r\n                                                            [src]=\"other.pic_file_name\"\r\n                                                            [render-text]=\"false\"\r\n                                                            [original-size]=\"false\"\r\n                                                            [page] = \"1\"\r\n                                                            [show-all]=\"false\"\r\n                                                            style=\"width: 100%; height: 100%\">\r\n                                                        </pdf-viewer>\r\n                                                    </a>\r\n                                                    <div class=\"btn-download-container\">\r\n                                                        <button (click)=\"downloadImage(other.pic_file_name)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                                    </div>\r\n                                                    <div style=\"text-align: center;\">\r\n                                                        <span>\r\n                                                            <b>Uploaded date : {{other.created_date}}</b>\r\n                                                        </span>\r\n                                                    </div>\r\n        \r\n                                                    <br/>\r\n                           \r\n                                                </div>\r\n                                            \r\n                                            </div>\r\n                                    </ng-container>\r\n                                </div>\r\n                            </div>\r\n                            <ng-template  #empty_others>\r\n                                <div class=\"card-content\">\r\n                                    <div class=\"row\">\r\n                                            <div class=\"col-md-6 col-sm-12\">             \r\n                                                <label class=\"label-bold\">Others</label>\r\n                                                    <div class=\"member-detail-image-container\">\r\n                                                        <div style=\"width:100%; text-align: center;\">\r\n                                                            empty\r\n                                                        </div>\r\n                                                    </div>\r\n                                                \r\n                                            </div>\r\n                                    </div>\r\n                                </div>\r\n                            </ng-template>\r\n\r\n                            <ng-template  #empty_receipt>\r\n                                <div class=\"card-content\">\r\n                                    <div class=\"row\">\r\n                                            <div class=\"col-md-6 col-sm-12\">             \r\n                                                <label class=\"label-bold\">Receipt</label>\r\n                                                    <div class=\"member-detail-image-container\">\r\n                                                        <div style=\"width:100%; text-align: center;\">\r\n                                                            empty\r\n                                                        </div>\r\n                                                    </div>\r\n                                                \r\n                                            </div>\r\n                                    </div>\r\n                                </div>\r\n                            </ng-template>\r\n                            \r\n\r\n                        </div>\r\n                    </div>\r\n\r\n                    <!-- <div class=\"col-md-4 col-sm-12\">\r\n                        <div class=\"card mb-3\">\r\n                            <div class=\"card-header\"><h2>Dokumen Tambahan</h2></div>\r\n                            <div class=\"card-content\">\r\n                                <div class=\"col-md-12\"> \r\n    \r\n    \r\n                                    <label class=\"label-bold\">Foto Surat Kuasa</label>\r\n                                    <ng-container *ngIf=\"detail.additional_info && detail.additional_info.surat_kuasa && isArray(detail.additional_info.surat_kuasa) && detail.additional_info.surat_kuasa.length > 0; else empty_image\">\r\n                                        <div class=\"member-detail-image-container\"   *ngFor=\"let itemImage of detail.additional_info.surat_kuasa; let i = index\">\r\n                                            <ng-container>\r\n                                                <a href={{itemImage.pic_file_name}} target=\"_blank\">\r\n                                                    <img src={{itemImage.pic_file_name}} class=\"member-detail-image\" />\r\n                                                </a>\r\n                                                <div class=\"btn-download-container\">\r\n                                                    <button (click)=\"downloadImage(itemImage.pic_file_name)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                                </div>\r\n                                                <div style=\"text-align: center;\">\r\n                                                    <span>\r\n                                                        <b>Last updated : {{convertDate(itemImage.updated_date)}}</b>\r\n                                                    </span>\r\n                                                </div>\r\n                                                <br/>\r\n                                            </ng-container>\r\n                                        </div>\r\n                                    </ng-container>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div> -->\r\n\r\n                    \r\n                </div>\r\n            </div>\r\n\r\n\r\n            <div class=\"col-md-4 col-sm-12\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-sm-12\">\r\n                        <div class=\"card mb-3\">\r\n                            <div class=\"card-header\"><h2>Data {{programType == 'custom_kontraktual' ? 'PIC' : 'Pemilik'}}</h2></div>\r\n                            <div class=\"card-content\">\r\n                                <div class=\"col-md-12\">             \r\n                                    <label>ID Pelanggan</label>\r\n                                    <div name=\"member_id\">{{memberDetail.username}}</div>\r\n    \r\n                                    <label>Data Complete</label>\r\n                                    <div name=\"member_id\">{{memberDetail.additional_info && memberDetail.additional_info.data_complete ? memberDetail.additional_info.data_complete : '-'}}</div>\r\n\r\n                                    <div *ngIf=\"programType != 'reguler' && programType == 'custom_kontraktual'\">\r\n                                        <label>Nama Program</label>\r\n                                        <div name=\"full_name\">{{memberDetail.input_form_data.cluster}}</div>\r\n                                    </div>\r\n                                    \r\n                                    <div *ngIf=\"programType != 'reguler' && programType != 'custom_kontraktual'\">\r\n                                        <label>Nama Toko</label>\r\n                                        <div name=\"full_name\">{{memberDetail.input_form_data.nama_toko}}</div>\r\n                                    </div>\r\n\r\n                                    <div *ngIf=\"programType != 'reguler' && programType == 'custom_kontraktual'\">\r\n                                        <label>Nama Entitas</label>\r\n                                        <div name=\"full_name\">{{memberDetail.input_form_data.nama_toko}}</div>\r\n                                    </div>\r\n                                    \r\n                                    <label>Nama Distributor</label>\r\n                                    <div name=\"full_name\">{{memberDetail.input_form_data.nama_distributor}}</div>\r\n    \r\n                                    <div *ngIf=\"programType != 'reguler' && programType != 'custom_kontraktual'\">\r\n                                        <label>Alamat Toko</label>\r\n                                        <div name=\"full_name\">{{memberDetail.input_form_data.alamat_toko}}</div>\r\n                                    </div>\r\n\r\n                                    <div *ngIf=\"programType != 'reguler' && programType == 'custom_kontraktual'\">\r\n                                        <label>Alamat Entitas</label>\r\n                                        <div name=\"full_name\">{{memberDetail.input_form_data.alamat_toko}}</div>\r\n                                    </div>\r\n    \r\n                                    <label>Nama {{programType == 'custom_kontraktual' ? 'PIC' : 'Pemilik'}}</label>\r\n                                    <div name=\"full_name\">{{memberDetail.input_form_data.nama_pemilik}}</div>\r\n    \r\n                                    <label>No. WA {{programType == 'custom_kontraktual' ? 'PIC' : 'Pemilik'}}</label>\r\n                                    <div name=\"full_name\">{{memberDetail.input_form_data.no_wa_pemilik}}</div>\r\n                                    \r\n                                    <label>No. KTP {{programType == 'custom_kontraktual' ? 'PIC' : 'Pemilik'}}</label>\r\n                                    <div name=\"full_name\">{{memberDetail.input_form_data.ktp_pemilik}}</div>\r\n    \r\n                                    <label>Foto KTP {{programType == 'custom_kontraktual' ? 'PIC' : 'Pemilik'}}</label>\r\n                                    <div class=\"member-detail-image-container\">\r\n                                        <ng-container  *ngIf=\"validURL(memberDetail.input_form_data.foto_ktp_pemilik); else empty_image\">\r\n                                            <a href={{memberDetail.input_form_data.foto_ktp_pemilik}} target=\"_blank\">\r\n                                                <img *ngIf=\"checkFileType(memberDetail.input_form_data.foto_ktp_pemilik) == 'image'\" src={{memberDetail.input_form_data.foto_ktp_pemilik}} class=\"member-detail-image\" />\r\n                                                <pdf-viewer \r\n                                                    *ngIf=\"checkFileType(memberDetail.input_form_data.foto_ktp_pemilik) == 'document'\"\r\n                                                    [src]=\"memberDetail.input_form_data.foto_ktp_pemilik\"\r\n                                                    [render-text]=\"false\"\r\n                                                    [original-size]=\"false\"\r\n                                                    [page] = \"1\"\r\n                                                    [show-all]=\"false\"\r\n                                                    style=\"width: 100%; height: 100%\">\r\n                                                </pdf-viewer>\r\n                                            </a>\r\n                                            <div class=\"btn-download-container\">\r\n                                                <button (click)=\"downloadImage(memberDetail.input_form_data.foto_ktp_pemilik)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                            </div>\r\n                                        </ng-container>\r\n                                        \r\n                                        \r\n                                    </div>\r\n    \r\n                                    <label>No. NPWP {{programType == 'custom_kontraktual' ? 'PIC' : 'Pemilik'}}</label>\r\n                                    <div name=\"full_name\">{{memberDetail.input_form_data.npwp_pemilik}}</div>\r\n    \r\n                                    <label>Foto NPWP {{programType == 'custom_kontraktual' ? 'PIC' : 'Pemilik'}}</label>\r\n                                    <div class=\"member-detail-image-container\">\r\n                                        <ng-container  *ngIf=\"validURL(memberDetail.input_form_data.foto_npwp_pemilik); else empty_image\">\r\n                                            <a href={{memberDetail.input_form_data.foto_npwp_pemilik}} target=\"_blank\">\r\n                                                <img *ngIf=\"checkFileType(memberDetail.input_form_data.foto_npwp_pemilik) == 'image'\" src={{memberDetail.input_form_data.foto_npwp_pemilik}} class=\"member-detail-image\" />\r\n                                                <pdf-viewer \r\n                                                    *ngIf=\"checkFileType(memberDetail.input_form_data.foto_npwp_pemilik) == 'document'\"\r\n                                                    [src]=\"memberDetail.input_form_data.foto_npwp_pemilik\"\r\n                                                    [render-text]=\"false\"\r\n                                                    [original-size]=\"false\"\r\n                                                    [page] = \"1\"\r\n                                                    [show-all]=\"false\"\r\n                                                    style=\"width: 100%; height: 100%\">\r\n                                                </pdf-viewer>\r\n                                                <img src={{memberDetail.input_form_data.foto_npwp_pemilik}} class=\"member-detail-image\" />\r\n                                            </a>\r\n                                            <div class=\"btn-download-container\">\r\n                                                <button (click)=\"downloadImage(memberDetail.input_form_data.foto_npwp_pemilik)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                            </div>\r\n                                        </ng-container>\r\n                                    </div>\r\n    \r\n                                </div>\r\n                            \r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"card mb-3\">\r\n                            <div class=\"card-header\"><h2>Data Penerima </h2></div>\r\n                            <div class=\"card-content\">\r\n                                <div class=\"col-md-12\">\r\n    \r\n                                    <label>Hadiah Dikuasakan</label>\r\n                                    <div name=\"email\">{{memberDetail.input_form_data.hadiah_dikuasakan}}</div>\r\n                                \r\n                                    <label>Nama Penerima</label>\r\n                                    <div name=\"email\">{{memberDetail.input_form_data.nama_penerima_gopay}}</div>\r\n\r\n                                    <div *ngIf=\"programType != 'reguler' && programType == 'custom_kontraktual'\">\r\n                                        <label>Jabatan</label>\r\n                                        <div name=\"email\">{{memberDetail.input_form_data.description}}</div>\r\n                                    </div>\r\n    \r\n                                    <label>No. WA Penerima</label>\r\n                                    <div name=\"email\">{{memberDetail.input_form_data.no_wa_penerima}}</div>\r\n    \r\n                                    <div *ngIf=\"programType != 'reguler' && programType != 'custom_kontraktual'\">\r\n                                        <label>No. Gopay Penerima<div style=\"font-size: 10px;\">(Jika hadiah berupa Saldo Gopay, isi dengan No. Gopay Penerima)</div></label>\r\n                                        <div name=\"email\">{{memberDetail.input_form_data.telp_penerima_gopay}}</div>\r\n                                    </div>\r\n\r\n                                    <div *ngIf=\"programType != 'reguler' && programType == 'custom_kontraktual'\">\r\n                                        <label>No. E-Wallet Penerima<div style=\"font-size: 10px;\">(Jika hadiah berupa Saldo E-Wallet, isi dengan No. E-Wallet Penerima)</div></label>\r\n                                        <div name=\"email\">{{memberDetail.input_form_data.telp_penerima_gopay}}</div>\r\n                                    </div>\r\n    \r\n                                   <label>Alamat Pengiriman</label>\r\n                                   <div name=\"email\">{{memberDetail.input_form_data.alamat_rumah}}</div>\r\n    \r\n                                   <label>No. KTP Penerima</label>\r\n                                   <div name=\"email\">{{memberDetail.input_form_data.ktp_penerima}}</div>\r\n    \r\n                                   <label>Foto KTP Penerima</label>\r\n                                    <div class=\"member-detail-image-container\">\r\n                                        <ng-container *ngIf=\"validURL(memberDetail.input_form_data.foto_ktp_penerima); else empty_image\">\r\n                                            <a href={{memberDetail.input_form_data.foto_ktp_penerima}} target=\"_blank\" >\r\n                                                <img *ngIf=\"checkFileType(memberDetail.input_form_data.foto_ktp_penerima) == 'image'\" src={{memberDetail.input_form_data.foto_ktp_penerima}} class=\"member-detail-image\" />\r\n                                                <pdf-viewer \r\n                                                    *ngIf=\"checkFileType(memberDetail.input_form_data.foto_ktp_penerima) == 'document'\"\r\n                                                    [src]=\"memberDetail.input_form_data.foto_ktp_penerima\"\r\n                                                    [render-text]=\"false\"\r\n                                                    [original-size]=\"false\"\r\n                                                    [page] = \"1\"\r\n                                                    [show-all]=\"false\"\r\n                                                    style=\"width: 100%; height: 100%\">\r\n                                                </pdf-viewer>\r\n                                            </a>\r\n                                            <div class=\"btn-download-container\">\r\n                                                <button (click)=\"downloadImage(memberDetail.input_form_data.foto_ktp_penerima)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                            </div>\r\n                                        </ng-container>\r\n                                    </div>\r\n    \r\n                                   <label>No. NPWP Penerima</label>\r\n                                   <div name=\"email\">{{memberDetail.input_form_data.npwp_penerima}}</div>\r\n    \r\n                                   <label>Foto NPWP Penerima</label>\r\n                                   <div class=\"member-detail-image-container\">\r\n                                       <ng-container *ngIf=\"validURL(memberDetail.input_form_data.foto_npwp_penerima); else empty_image\">\r\n                                           <a href={{memberDetail.input_form_data.foto_npwp_penerima}} target=\"_blank\">\r\n                                            <img *ngIf=\"checkFileType(memberDetail.input_form_data.foto_npwp_penerima) == 'image'\" src={{memberDetail.input_form_data.foto_npwp_penerima}} class=\"member-detail-image\" />\r\n                                            <pdf-viewer \r\n                                                *ngIf=\"checkFileType(memberDetail.input_form_data.foto_npwp_penerima) == 'document'\"\r\n                                                [src]=\"memberDetail.input_form_data.foto_npwp_penerima\"\r\n                                                [render-text]=\"false\"\r\n                                                [original-size]=\"false\"\r\n                                                [page] = \"1\"\r\n                                                [show-all]=\"false\"\r\n                                                style=\"width: 100%; height: 100%\">\r\n                                            </pdf-viewer>\r\n                                           </a>\r\n                                           <div class=\"btn-download-container\">\r\n                                               <button (click)=\"downloadImage(memberDetail.input_form_data.foto_npwp_penerima)\" class=\"btn-download-img\"><i class=\"fa fa-fw fa-download\"></i> Download</button>\r\n                                           </div>\r\n                                       </ng-container>\r\n                                    </div>\r\n                                </div>\r\n                             </div>\r\n                        </div>\r\n\r\n                    </div>\r\n                </div>\r\n            </div>\r\n         \r\n            </div>\r\n        </div>\r\n    </div>\r\n        <ng-template #empty_image>\r\n            <div style=\"width:100%; text-align: center;\">\r\n                empty\r\n            </div>\r\n        </ng-template>\r\n\r\n        <div *ngIf=\"errorMessage\" class=\"error-message\">\r\n            <div class=\"text-message\">\r\n                <i class=\"fas fa-ban\"></i>\r\n                {{errorMessage}}\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div *ngIf=\"edit\">\r\n    <app-list-order-edit [back]=\"[this, 'editThis']\" [detail]=\"memberDetail\" [detailOrder]=\"detailOrder\"></app-list-order-edit>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/form-member/redeem-login/redeem-login.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/form-member/redeem-login/redeem-login.component.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!showPasswordPage\">\r\n    <div class=\"card card-detail\">\r\n        <div class=\"card-header\">\r\n            <button class=\"btn back_button\" (click)=\"backToDetail()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button><h1>{{detail.member_id}}</h1>\r\n        </div>\r\n        <div class=\"card-content\">\r\n            <div class=\"member-detail\">\r\n        \r\n                <div class=\"row\">\r\n                    <div class=\"col-md-4 col-sm-12\" >\r\n                        <div class=\"card mb-3\">\r\n                            <div class=\"card-header\"><h2>Login Member Access</h2></div>\r\n                            <div class=\"card-content\">\r\n                                <div class=\"col-md-12\">\r\n    \r\n                                    <label>ID Pelanggan</label>\r\n                                    <form-input  name=\"id_pel\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'ID Pelanggan'\"  [(ngModel)]=\"updateSingle.id_pel\" autofocus required></form-input>\r\n\r\n                                    <label>Password</label>\r\n                                    <form-input  name=\"password\" [type]=\"'text'\" [placeholder]=\"'Password'\"  [(ngModel)]=\"password\" autofocus required></form-input>\r\n\r\n                                    <div class=\"add-more-container\">\r\n                                        <button (click)=\"redeemLogin()\" class=\"add-more-image btn-change-password\">Login</button>\r\n                                    </div>\r\n    \r\n                                </div>\r\n                            \r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    \r\n                </div>\r\n         \r\n             </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/form-member/show-password/show-password.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/form-member/show-password/show-password.component.html ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToDetail()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button><h1>{{detail.member_id}}</h1>\r\n    </div>\r\n    <div class=\"card-content\">\r\n        <div class=\"member-detail\">\r\n    \r\n            <div class=\"row\">\r\n                <div class=\" col-md-4\" >\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Show Password</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n\r\n                                <label>ID Pelanggan</label>\r\n                                <form-input  name=\"id_pel\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'ID Pelanggan'\"  [(ngModel)]=\"updateSingle.username\" autofocus required></form-input>\r\n\r\n                                <label>Current Password</label>\r\n                                <form-input  name=\"password\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Current Password'\"  [(ngModel)]=\"updateSingle.password\" autofocus required></form-input>\r\n                \r\n                            </div>\r\n                        \r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                \r\n            </div>\r\n     \r\n         </div>\r\n\r\n         <div>\r\n            <p style=\"color: red;\">Note: Password hanya bisa dilihat satu kali. Mohon segera simpan Password dari Pelanggan ini.</p>\r\n        </div>\r\n    </div>\r\n\r\n    \r\n</div>"

/***/ }),

/***/ "./src/app/layout/modules/form-member/add-bulk/form-member.add-bulk.component.scss":
/*!*****************************************************************************************!*\
  !*** ./src/app/layout/modules/form-member/add-bulk/form-member.add-bulk.component.scss ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".pb-framer {\n  text-align: center;\n  position: relative;\n  height: 25px;\n  color: #e67e22;\n}\n.pb-framer .progressbar {\n  overflow: hidden;\n  background: #3498db;\n  position: absolute;\n  height: 25px;\n  z-index: -1;\n}\n.pb-framer .terminated.progressbar {\n  background-color: red;\n}\n.pb-framer .clr-white {\n  color: white;\n}\n.file-selected {\n  color: red;\n}\n.error-msg-upload {\n  margin-top: 10px;\n  padding-left: 0px !important;\n  padding-right: 0px !important;\n}\n.container-two-button {\n  display: flex;\n}\n.btn-add-bulk {\n  margin-bottom: 20px;\n}\n.btn-upload, .btn-submit {\n  margin-right: 10px;\n}\n.btn-upload {\n  border-color: black;\n}\n.btn-submit {\n  background-color: #3498db;\n  color: white;\n}\n.member-bulk-update-container {\n  padding: 10px !important;\n}\n.bg-delete {\n  background-color: #ffe3f3;\n  padding: 10px;\n}\n.ng-pristine {\n  color: white !important;\n}\n.ng-pristine {\n  color: black !important;\n  width: 100%;\n  border-radius: 5px;\n  padding: 6px 0px;\n}\nlabel {\n  color: black;\n}\n.card-detail > .card-header {\n  position: relative;\n  min-height: 48px;\n}\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 10px;\n  background-color: #3498db;\n  color: white;\n  height: 30px;\n  align-items: center;\n  display: flex;\n  right: 10px;\n  padding-top: 0px;\n  padding-right: 10px;\n  padding-bottom: 0;\n  padding-left: 10px;\n  font-weight: bold;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #000;\n  margin-bottom: 10px;\n  margin-top: 20px;\n}\n.card-detail .member-detail form-select[name=status] .form-group {\n  margin-bottom: 0px;\n}\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: normal;\n  padding: 0px;\n  border: 1px solid #f8f9fa;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n.rounded-btn {\n  margin-top: 20px;\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  background: #56a4ff;\n}\n.rounded-btn-danger {\n  margin-top: 20px;\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  color: #555;\n  margin-left: 10px;\n}\n.rounded-btn:hover {\n  background: #0a7bff;\n  outline: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZm9ybS1tZW1iZXIvYWRkLWJ1bGsvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxmb3JtLW1lbWJlclxcYWRkLWJ1bGtcXGZvcm0tbWVtYmVyLmFkZC1idWxrLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9mb3JtLW1lbWJlci9hZGQtYnVsay9mb3JtLW1lbWJlci5hZGQtYnVsay5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtBQ0NKO0FEQUk7RUFDSSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQ0VSO0FEQUk7RUFDSSxxQkFBQTtBQ0VSO0FEQUk7RUFDSSxZQUFBO0FDRVI7QURFQTtFQUNJLFVBQUE7QUNDSjtBREVBO0VBQ0ksZ0JBQUE7RUFDQSw0QkFBQTtFQUNBLDZCQUFBO0FDQ0o7QURFQTtFQUNJLGFBQUE7QUNDSjtBREVBO0VBQ0ksbUJBQUE7QUNDSjtBREVBO0VBQ0ksa0JBQUE7QUNDSjtBREVBO0VBQ0ksbUJBQUE7QUNDSjtBREVBO0VBQ0kseUJBQUE7RUFDQSxZQUFBO0FDQ0o7QURFQTtFQUNJLHdCQUFBO0FDQ0o7QURFQTtFQUNJLHlCQUFBO0VBQ0EsYUFBQTtBQ0NKO0FERUE7RUFDSSx1QkFBQTtBQ0NKO0FERUE7RUFDSSx1QkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FDQ0o7QURFQTtFQUNJLFlBQUE7QUNDSjtBREVJO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtBQ0NSO0FEQVE7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSw2QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEseUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0FDQ1o7QURBWTtFQUNJLGlCQUFBO0FDRWhCO0FEQ1E7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUVBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUNBWjtBREdJO0VBQ0ksYUFBQTtBQ0RSO0FERVE7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0FDQVo7QURJSTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFFQSxtQkFBQTtFQUNBLGdCQUFBO0FDSFI7QURNUTtFQUNJLGtCQUFBO0FDSlo7QURNUTtFQUNJLGdCQUFBO0FDSlo7QURNUTtFQUNJLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUNKWjtBRFFRO0VBQ0ksc0JBQUE7QUNOWjtBRE9ZO0VBQ0ksWUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBQ0EsMEJBQUE7QUNMaEI7QURXQTtFQUNJLGdCQUFBO0VBRUEsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FDUko7QURhQTtFQUNJLGdCQUFBO0VBRUEsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtBQ1ZKO0FEWUE7RUFDUSxtQkFBQTtFQUVBLGFBQUE7QUNWUiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2Zvcm0tbWVtYmVyL2FkZC1idWxrL2Zvcm0tbWVtYmVyLmFkZC1idWxrLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnBiLWZyYW1lcntcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGhlaWdodDogMjVweDtcclxuICAgIGNvbG9yOiAjZTY3ZTIyO1xyXG4gICAgLnByb2dyZXNzYmFye1xyXG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzM0OThkYjtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgICAgIHotaW5kZXg6IC0xO1xyXG4gICAgfVxyXG4gICAgLnRlcm1pbmF0ZWQucHJvZ3Jlc3NiYXJ7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xyXG4gICAgfVxyXG4gICAgLmNsci13aGl0ZXtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5maWxlLXNlbGVjdGVkIHtcclxuICAgIGNvbG9yOiByZWQ7XHJcbn1cclxuXHJcbi5lcnJvci1tc2ctdXBsb2FkIHtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDBweCAhaW1wb3J0YW50O1xyXG4gICAgcGFkZGluZy1yaWdodDogMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5jb250YWluZXItdHdvLWJ1dHRvbiB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG59XHJcblxyXG4uYnRuLWFkZC1idWxrIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbn1cclxuXHJcbi5idG4tdXBsb2FkLCAuYnRuLXN1Ym1pdCB7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbn1cclxuXHJcbi5idG4tdXBsb2FkIHtcclxuICAgIGJvcmRlci1jb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbi5idG4tc3VibWl0IHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi5tZW1iZXItYnVsay11cGRhdGUtY29udGFpbmVyIHtcclxuICAgIHBhZGRpbmc6IDEwcHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLmJnLWRlbGV0ZXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmUzZjM7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG59XHJcblxyXG4ubmctcHJpc3RpbmV7XHJcbiAgICBjb2xvciA6IHdoaXRlICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5uZy1wcmlzdGluZXtcclxuICAgIGNvbG9yOiBibGFjayAhaW1wb3J0YW50O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBwYWRkaW5nOiA2cHggMHB4O1xyXG59XHJcblxyXG5sYWJlbHtcclxuICAgIGNvbG9yOiBibGFjaztcclxufVxyXG4uY2FyZC1kZXRhaWx7XHJcbiAgICA+LmNhcmQtaGVhZGVye1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBtaW4taGVpZ2h0OiA0OHB4O1xyXG4gICAgICAgIC5iYWNrX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAtNHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogLTEwcHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAuc2F2ZV9idXR0b257XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgdG9wOiAxMHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgICAgIC8vIG1hcmdpbi10b3A6IDVweDtcclxuICAgICAgICAgICAgcGFkZGluZy10b3A6IDBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMTBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDA7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMTBweDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmNhcmQtY29udGVudHtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgID4uY29sLW1kLTEye1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBoMXtcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgY29sb3I6ICMwMDA7XHJcbiAgICAgICAgLy8gdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgIH1cclxuICAgIC5tZW1iZXItZGV0YWlse1xyXG4gICAgICAgIGZvcm0tc2VsZWN0W25hbWU9c3RhdHVzXSAuZm9ybS1ncm91cCB7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWx7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxhYmVsK2RpdntcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6bm9ybWFsO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHg7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNmOGY5ZmE7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgY29sb3I6ICM2NjY7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBcclxuICAgICAgICAuY2FyZC1oZWFkZXJ7IFxyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoMntcclxuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46IDBweDtcclxuICAgICAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbi5yb3VuZGVkLWJ0biB7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNzBweDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIGxpbmUtaGVpZ2h0OiA0MHB4O1xyXG4gICAgcGFkZGluZzogMCA0MHB4O1xyXG4gICAgYmFja2dyb3VuZDogIzU2YTRmZjtcclxuICAgIC8vIG1hcmdpbi1sZWZ0OiA0MCU7XHJcbiAgICAvLyBtYXJnaW4tcmlnaHQ6IDUwJTtcclxuICAgIH1cclxuXHJcbi5yb3VuZGVkLWJ0bi1kYW5nZXIge1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNzBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDcwcHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBsaW5lLWhlaWdodDogNDBweDtcclxuICAgIHBhZGRpbmc6IDAgNDBweDtcclxuICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgICB9XHJcbi5yb3VuZGVkLWJ0bjpob3ZlciB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogZGFya2VuKCM1NkE0RkYsIDE1JSk7XHJcbiAgICAgICAgLy8gYm9yZGVyOiAycHggc29saWQgYmx1ZTtcclxuICAgICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgfVxyXG4vLyAuZGF0ZXBpY2tlci1pbnB1dHtcclxuLy8gICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTsgICAgXHJcbi8vICAgICB9XHJcbi8vIC5mb3JtLWNvbnRyb2x7XHJcbi8vICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbi8vICAgICB0b3A6IDEwcHg7XHJcbi8vICAgICBsZWZ0OiAwcHg7XHJcbi8vIH1cclxuIiwiLnBiLWZyYW1lciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBoZWlnaHQ6IDI1cHg7XG4gIGNvbG9yOiAjZTY3ZTIyO1xufVxuLnBiLWZyYW1lciAucHJvZ3Jlc3NiYXIge1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBiYWNrZ3JvdW5kOiAjMzQ5OGRiO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogMjVweDtcbiAgei1pbmRleDogLTE7XG59XG4ucGItZnJhbWVyIC50ZXJtaW5hdGVkLnByb2dyZXNzYmFyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xufVxuLnBiLWZyYW1lciAuY2xyLXdoaXRlIHtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4uZmlsZS1zZWxlY3RlZCB7XG4gIGNvbG9yOiByZWQ7XG59XG5cbi5lcnJvci1tc2ctdXBsb2FkIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgcGFkZGluZy1sZWZ0OiAwcHggIWltcG9ydGFudDtcbiAgcGFkZGluZy1yaWdodDogMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5jb250YWluZXItdHdvLWJ1dHRvbiB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG5cbi5idG4tYWRkLWJ1bGsge1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuXG4uYnRuLXVwbG9hZCwgLmJ0bi1zdWJtaXQge1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5cbi5idG4tdXBsb2FkIHtcbiAgYm9yZGVyLWNvbG9yOiBibGFjaztcbn1cblxuLmJ0bi1zdWJtaXQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5tZW1iZXItYnVsay11cGRhdGUtY29udGFpbmVyIHtcbiAgcGFkZGluZzogMTBweCAhaW1wb3J0YW50O1xufVxuXG4uYmctZGVsZXRlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTNmMztcbiAgcGFkZGluZzogMTBweDtcbn1cblxuLm5nLXByaXN0aW5lIHtcbiAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG59XG5cbi5uZy1wcmlzdGluZSB7XG4gIGNvbG9yOiBibGFjayAhaW1wb3J0YW50O1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBwYWRkaW5nOiA2cHggMHB4O1xufVxuXG5sYWJlbCB7XG4gIGNvbG9yOiBibGFjaztcbn1cblxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtaW4taGVpZ2h0OiA0OHB4O1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLXRvcDogLTRweDtcbiAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgY29sb3I6ICM1NTU7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuc2F2ZV9idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcbiAgY29sb3I6IHdoaXRlO1xuICBoZWlnaHQ6IDMwcHg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHJpZ2h0OiAxMHB4O1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogMTVweDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50ID4gLmNvbC1tZC0xMiB7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgaDEge1xuICBmb250LXNpemU6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzAwMDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgbWFyZ2luLXRvcDogMjBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBmb3JtLXNlbGVjdFtuYW1lPXN0YXR1c10gLmZvcm0tZ3JvdXAge1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGxhYmVsICsgZGl2IHtcbiAgYmFja2dyb3VuZDogI2YwZjBmMDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgcGFkZGluZzogMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjZjhmOWZhO1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtaW4taGVpZ2h0OiAyN3B4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNjY2O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIGgyIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGxpbmUtaGVpZ2h0OiB1bnNldDtcbiAgaGVpZ2h0OiB1bnNldDtcbiAgbWFyZ2luOiAwcHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuXG4ucm91bmRlZC1idG4ge1xuICBtYXJnaW4tdG9wOiAyMHB4O1xuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDcwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDcwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBsaW5lLWhlaWdodDogNDBweDtcbiAgcGFkZGluZzogMCA0MHB4O1xuICBiYWNrZ3JvdW5kOiAjNTZhNGZmO1xufVxuXG4ucm91bmRlZC1idG4tZGFuZ2VyIHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA3MHB4O1xuICBib3JkZXItcmFkaXVzOiA3MHB4O1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XG4gIHBhZGRpbmc6IDAgNDBweDtcbiAgY29sb3I6ICM1NTU7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuXG4ucm91bmRlZC1idG46aG92ZXIge1xuICBiYWNrZ3JvdW5kOiAjMGE3YmZmO1xuICBvdXRsaW5lOiBub25lO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/form-member/add-bulk/form-member.add-bulk.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/layout/modules/form-member/add-bulk/form-member.add-bulk.component.ts ***!
  \***************************************************************************************/
/*! exports provided: FormMemberAddBulkComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormMemberAddBulkComponent", function() { return FormMemberAddBulkComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var FormMemberAddBulkComponent = /** @class */ (function () {
    function FormMemberAddBulkComponent(memberService) {
        this.memberService = memberService;
        this.name = "";
        this.Members = [];
        this.startUploading = false;
        this.form = {
            id_pel: "",
            nama_toko: "",
            nama_distributor: "",
            alamat_toko: "",
            kelurahan_toko: "-",
            kecamatan_toko: "-",
            kota_toko: "-",
            provinsi_toko: "-",
            kode_pos_toko: "-",
            landmark_toko: "-",
            nama_pemilik: "",
            telp_pemilik: "",
            no_wa_pemilik: "",
            ktp_pemilik: "",
            foto_ktp_pemilik: "-",
            npwp_pemilik: "",
            foto_npwp_pemilik: "-",
            alamat_rumah: "",
            kelurahan_rumah: "",
            kecamatan_rumah: "",
            kota_rumah: "",
            provinsi_rumah: "",
            kode_pos_rumah: "",
            landmark_rumah: "-",
            description: "nonupfront",
            hadiah_dikuasakan: "TIDAK KUASA",
            nama_penerima_gopay: "-",
            ktp_penerima: "-",
            foto_ktp_penerima: "-",
            npwp_penerima: "-",
            foto_npwp_penerima: "-",
            telp_penerima_gopay: "-",
            no_wa_penerima: "-",
            nama_id_gopay: "-",
            jenis_akun: "-",
            saldo_terakhir: "-",
        };
        this.errorLabel = false;
        this.member_status_type = [
            { label: 'Superuser', value: 'superuser' },
            { label: 'Member', value: 'member', selected: 1 },
            { label: 'Marketing', value: 'marketing' },
            { label: 'Admin', value: 'admin' },
            { label: 'Merchant', value: 'merchant' }
        ];
        this.group_desc = [
            { label: 'Nonupfront', value: 'nonupfront' },
            { label: 'Upfront', value: 'upfront' }
        ];
        this.kuasa_option = [
            { label: 'TIDAK KUASA', value: 'TIDAK KUASA' },
            { label: 'KUASA', value: 'KUASA' }
        ];
        this.isBulkUpdate = false;
        this.showUploadButton = false;
        this.selectedFile = null;
        this.cancel = false;
        this.progressBar = 0;
        this.errorFile = false;
        this.prodOnUpload = false;
    }
    FormMemberAddBulkComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    FormMemberAddBulkComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                // this.service = this.memberService;
                // let result: any = await this.memberService.getMembersLint();
                // this.Members = result.result;
                this.selectedFile = null;
                // this.prodOnUpload = false;
                this.startUploading = false;
                return [2 /*return*/];
            });
        });
    };
    FormMemberAddBulkComponent.prototype.formSubmitAddMember = function () {
        return __awaiter(this, void 0, void 0, function () {
            var form_add, result, e_1, message;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        form_add = this.form;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        this.service = this.memberService;
                        return [4 /*yield*/, this.memberService.addNewMember(form_add)];
                    case 2:
                        result = _a.sent();
                        console.warn("result add member", result);
                        // this.Members = result.result;
                        // alert("Member Has Been Added!")
                        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                            title: 'Success',
                            text: 'Pelanggan berhasil ditambahkan',
                            icon: 'success',
                            confirmButtonText: 'Ok',
                        }).then(function (result) {
                            if (result.isConfirmed) {
                                _this.backTo();
                            }
                        });
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        alert("Error! Please try again later");
                        this.errorLabel = (e_1.message); //conversion to Error type
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    FormMemberAddBulkComponent.prototype.onFileSelected = function (event) {
        var _this = this;
        this.errorFile = false;
        var fileMaxSize = 3000000; // let say 3Mb
        // var reader = new FileReader();
        // reader.readAsDataURL(event.target.files[0]); //
        Array.from(event.target.files).forEach(function (file) {
            if (file.size > fileMaxSize) {
                _this.errorFile = "maximum file is 3Mb";
            }
        });
        this.selectedFile = event.target.files[0];
        this.updateBulk.nativeElement.value = '';
    };
    FormMemberAddBulkComponent.prototype.cancelThis = function () {
        this.cancel = !this.cancel;
    };
    FormMemberAddBulkComponent.prototype.updateDataBulk = function () {
        return __awaiter(this, void 0, void 0, function () {
            var payload, result, e_2, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.startUploading = true;
                        this.cancel = false;
                        payload = {
                            type: 'application/form-data',
                        };
                        if (!this.selectedFile) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.memberService.updateBulkMember(this.selectedFile, this, payload)];
                    case 1:
                        result = _a.sent();
                        if (result) {
                            this.firstLoad();
                        }
                        _a.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        e_2 = _a.sent();
                        this.startUploading = false;
                        this.errorLabel = (e_2.message); //conversion to Error type
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    FormMemberAddBulkComponent.prototype.updateProgressBar = function (value) {
        this.progressBar = value;
    };
    FormMemberAddBulkComponent.prototype.backTo = function () {
        window.history.back();
    };
    FormMemberAddBulkComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('updateBulk', { static: false }),
        __metadata("design:type", Object)
    ], FormMemberAddBulkComponent.prototype, "updateBulk", void 0);
    FormMemberAddBulkComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-form-member-add-bulk',
            template: __webpack_require__(/*! raw-loader!./form-member.add-bulk.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/form-member/add-bulk/form-member.add-bulk.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./form-member.add-bulk.component.scss */ "./src/app/layout/modules/form-member/add-bulk/form-member.add-bulk.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"]])
    ], FormMemberAddBulkComponent);
    return FormMemberAddBulkComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/form-member/add/form-member.add.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/layout/modules/form-member/add/form-member.add.component.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg-delete {\n  background-color: #ffe3f3;\n  padding: 10px;\n}\n\n.ng-pristine {\n  color: white !important;\n}\n\n.ng-pristine {\n  color: black !important;\n  width: 100%;\n  border-radius: 5px;\n  padding: 6px 0px;\n}\n\n.member-bulk-update-container {\n  padding: 10px !important;\n}\n\n.container-two-button {\n  display: flex;\n}\n\n.error-msg-upload {\n  margin-top: 10px;\n  padding-left: 0px !important;\n  padding-right: 0px !important;\n}\n\n.file-selected {\n  color: red;\n}\n\n.btn-add-bulk {\n  margin-bottom: 20px;\n}\n\n.btn-upload, .btn-submit {\n  margin-right: 10px;\n}\n\n.btn-upload {\n  border-color: black;\n}\n\n.btn-submit {\n  background-color: #3498db;\n  color: white;\n}\n\n.star-required {\n  color: red;\n}\n\n.text-required {\n  color: red;\n  font-size: 11px;\n  margin-top: -15px;\n}\n\nlabel {\n  color: black;\n}\n\n.card-detail > .card-header {\n  position: relative;\n  min-height: 48px;\n}\n\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 10px;\n  background-color: #3498db;\n  color: white;\n  height: 30px;\n  align-items: center;\n  display: flex;\n  right: 10px;\n  padding-top: 0px;\n  padding-right: 10px;\n  padding-bottom: 0;\n  padding-left: 10px;\n  font-weight: bold;\n}\n\n.card-detail .card-content {\n  padding: 15px;\n}\n\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail form-select[name=status] .form-group {\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: normal;\n  padding: 0px;\n  border: 1px solid #f8f9fa;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n\n.rounded-btn {\n  margin-top: 20px;\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  background: #56a4ff;\n}\n\n.rounded-btn-danger {\n  margin-top: 20px;\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  color: #555;\n  margin-left: 10px;\n}\n\n.rounded-btn:hover {\n  background: #0a7bff;\n  outline: none;\n}\n\n.save-button {\n  background-color: #3498db;\n  color: white;\n  margin-top: 20px;\n}\n\n::ng-deep .ng-autocomplete {\n  width: 100% !important;\n}\n\n::ng-deep .ng-autocomplete .autocomplete-container {\n  box-shadow: none !important;\n}\n\n::ng-deep .ng-autocomplete .autocomplete-container .x {\n  display: none;\n}\n\n::ng-deep .ng-autocomplete .autocomplete-container input {\n  display: block;\n  padding: 0.375rem 0.75rem !important;\n  font-size: 1rem !important;\n  line-height: 1.5 !important;\n  color: #495057 !important;\n  background-color: #fff !important;\n  background-clip: padding-box;\n  border: 1px solid #ced4da !important;\n  border-radius: 0.25rem;\n  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;\n}\n\n::ng-deep .ng-autocomplete .autocomplete-container input::-webkit-input-placeholder {\n  color: #878E95 !important;\n}\n\n::ng-deep .ng-autocomplete .autocomplete-container input::-moz-placeholder {\n  color: #878E95 !important;\n}\n\n::ng-deep .ng-autocomplete .autocomplete-container input:-ms-input-placeholder {\n  color: #878E95 !important;\n}\n\n::ng-deep .ng-autocomplete .autocomplete-container input::-ms-input-placeholder {\n  color: #878E95 !important;\n}\n\n::ng-deep .ng-autocomplete .autocomplete-container input::placeholder {\n  color: #878E95 !important;\n}\n\n::ng-deep .ng-autocomplete .autocomplete-container input:disabled {\n  background-color: #eee !important;\n  color: #666 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZm9ybS1tZW1iZXIvYWRkL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcZm9ybS1tZW1iZXJcXGFkZFxcZm9ybS1tZW1iZXIuYWRkLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9mb3JtLW1lbWJlci9hZGQvZm9ybS1tZW1iZXIuYWRkLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQUE7RUFDQSxhQUFBO0FDQ0o7O0FERUE7RUFDSSx1QkFBQTtBQ0NKOztBREVBO0VBQ0ksdUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ0NKOztBREVBO0VBQ0ksd0JBQUE7QUNDSjs7QURFQTtFQUNJLGFBQUE7QUNDSjs7QURFQTtFQUNJLGdCQUFBO0VBQ0EsNEJBQUE7RUFDQSw2QkFBQTtBQ0NKOztBREVBO0VBQ0ksVUFBQTtBQ0NKOztBREVBO0VBQ0ksbUJBQUE7QUNDSjs7QURFQTtFQUNJLGtCQUFBO0FDQ0o7O0FERUE7RUFDSSxtQkFBQTtBQ0NKOztBREVBO0VBQ0kseUJBQUE7RUFDQSxZQUFBO0FDQ0o7O0FERUE7RUFDSSxVQUFBO0FDQ0o7O0FERUE7RUFDSSxVQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDQ0o7O0FERUE7RUFDSSxZQUFBO0FDQ0o7O0FERUk7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0FDQ1I7O0FEQVE7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSw2QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEseUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0FDQ1o7O0FEQVk7RUFDSSxpQkFBQTtBQ0VoQjs7QURDUTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBRUEsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQ0FaOztBREdJO0VBQ0ksYUFBQTtBQ0RSOztBREVRO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ0FaOztBRElJO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNGUjs7QURLUTtFQUNJLGtCQUFBO0FDSFo7O0FES1E7RUFDSSxnQkFBQTtBQ0haOztBREtRO0VBQ0ksbUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ0haOztBRE9RO0VBQ0ksc0JBQUE7QUNMWjs7QURNWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDSmhCOztBRFVBO0VBQ0ksZ0JBQUE7RUFFQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7QUNQSjs7QURZQTtFQUNJLGdCQUFBO0VBRUEsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtBQ1RKOztBRFdBO0VBQ1EsbUJBQUE7RUFFQSxhQUFBO0FDVFI7O0FEb0JBO0VBQ0kseUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUNqQko7O0FEb0JBO0VBQ0ksc0JBQUE7QUNqQko7O0FEbUJJO0VBQ0ksMkJBQUE7QUNqQlI7O0FEbUJRO0VBQ0ksYUFBQTtBQ2pCWjs7QURvQlE7RUFDSSxjQUFBO0VBQ0Esb0NBQUE7RUFDQSwwQkFBQTtFQUNBLDJCQUFBO0VBQ0EseUJBQUE7RUFDQSxpQ0FBQTtFQUNBLDRCQUFBO0VBQ0Esb0NBQUE7RUFDQSxzQkFBQTtFQUNBLHdFQUFBO0FDbEJaOztBRHFCUTtFQUNJLHlCQUFBO0FDbkJaOztBRGtCUTtFQUNJLHlCQUFBO0FDbkJaOztBRGtCUTtFQUNJLHlCQUFBO0FDbkJaOztBRGtCUTtFQUNJLHlCQUFBO0FDbkJaOztBRGtCUTtFQUNJLHlCQUFBO0FDbkJaOztBRHNCUTtFQUNJLGlDQUFBO0VBQ0Esc0JBQUE7QUNwQloiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9mb3JtLW1lbWJlci9hZGQvZm9ybS1tZW1iZXIuYWRkLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJnLWRlbGV0ZXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmUzZjM7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG59XHJcblxyXG4ubmctcHJpc3RpbmV7XHJcbiAgICBjb2xvciA6IHdoaXRlICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5uZy1wcmlzdGluZXtcclxuICAgIGNvbG9yOiBibGFjayAhaW1wb3J0YW50O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBwYWRkaW5nOiA2cHggMHB4O1xyXG59XHJcblxyXG4ubWVtYmVyLWJ1bGstdXBkYXRlLWNvbnRhaW5lciB7XHJcbiAgICBwYWRkaW5nOiAxMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5jb250YWluZXItdHdvLWJ1dHRvbiB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG59XHJcblxyXG4uZXJyb3ItbXNnLXVwbG9hZCB7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OiAwcHggIWltcG9ydGFudDtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDBweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uZmlsZS1zZWxlY3RlZCB7XHJcbiAgICBjb2xvcjogcmVkO1xyXG59XHJcblxyXG4uYnRuLWFkZC1idWxrIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbn1cclxuXHJcbi5idG4tdXBsb2FkLCAuYnRuLXN1Ym1pdCB7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbn1cclxuXHJcbi5idG4tdXBsb2FkIHtcclxuICAgIGJvcmRlci1jb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbi5idG4tc3VibWl0IHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi5zdGFyLXJlcXVpcmVkIHtcclxuICAgIGNvbG9yOiByZWQ7XHJcbn1cclxuXHJcbi50ZXh0LXJlcXVpcmVkIHtcclxuICAgIGNvbG9yOiByZWQ7XHJcbiAgICBmb250LXNpemU6IDExcHg7XHJcbiAgICBtYXJnaW4tdG9wOiAtMTVweDtcclxufVxyXG5cclxubGFiZWx7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbn1cclxuLmNhcmQtZGV0YWlse1xyXG4gICAgPi5jYXJkLWhlYWRlcntcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgbWluLWhlaWdodDogNDhweDtcclxuICAgICAgICAuYmFja19idXR0b257XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogLTRweDtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLnNhdmVfYnV0dG9ue1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIHRvcDogMTBweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIHJpZ2h0OiAxMHB4O1xyXG4gICAgICAgICAgICAvLyBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAwO1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgICAgICA+LmNvbC1tZC0xMntcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaDF7XHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICB9XHJcbiAgICAubWVtYmVyLWRldGFpbHtcclxuICAgICAgICBmb3JtLXNlbGVjdFtuYW1lPXN0YXR1c10gLmZvcm0tZ3JvdXAge1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxhYmVse1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYWJlbCtkaXZ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmMGYwZjA7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0Om5vcm1hbDtcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjZjhmOWZhO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgICAgICAgICBtaW4taGVpZ2h0OiAyN3B4O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNjY2O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgXHJcbiAgICAgICAgLmNhcmQtaGVhZGVyeyBcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaDJ7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4ucm91bmRlZC1idG4ge1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNzBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDcwcHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBsaW5lLWhlaWdodDogNDBweDtcclxuICAgIHBhZGRpbmc6IDAgNDBweDtcclxuICAgIGJhY2tncm91bmQ6ICM1NmE0ZmY7XHJcbiAgICAvLyBtYXJnaW4tbGVmdDogNDAlO1xyXG4gICAgLy8gbWFyZ2luLXJpZ2h0OiA1MCU7XHJcbiAgICB9XHJcblxyXG4ucm91bmRlZC1idG4tZGFuZ2VyIHtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDcwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDQwcHg7XHJcbiAgICBwYWRkaW5nOiAwIDQwcHg7XHJcbiAgICBjb2xvcjogIzU1NTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgfVxyXG4ucm91bmRlZC1idG46aG92ZXIge1xyXG4gICAgICAgIGJhY2tncm91bmQ6IGRhcmtlbigjNTZBNEZGLCAxNSUpO1xyXG4gICAgICAgIC8vIGJvcmRlcjogMnB4IHNvbGlkIGJsdWU7XHJcbiAgICAgICAgb3V0bGluZTogbm9uZTtcclxuICAgIH1cclxuLy8gLmRhdGVwaWNrZXItaW5wdXR7XHJcbi8vICAgICBwb3NpdGlvbjogYWJzb2x1dGU7ICAgIFxyXG4vLyAgICAgfVxyXG4vLyAuZm9ybS1jb250cm9se1xyXG4vLyAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4vLyAgICAgdG9wOiAxMHB4O1xyXG4vLyAgICAgbGVmdDogMHB4O1xyXG4vLyB9XHJcblxyXG4uc2F2ZS1idXR0b24ge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubmctYXV0b2NvbXBsZXRlIHtcclxuICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XHJcblxyXG4gICAgLmF1dG9jb21wbGV0ZS1jb250YWluZXIge1xyXG4gICAgICAgIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcclxuXHJcbiAgICAgICAgLngge1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaW5wdXQge1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgcGFkZGluZzogMC4zNzVyZW0gMC43NXJlbSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDFyZW0gIWltcG9ydGFudDtcclxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDEuNSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICBjb2xvcjogIzQ5NTA1NyAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY2xpcDogcGFkZGluZy1ib3g7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjZWQ0ZGEgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMC4yNXJlbTtcclxuICAgICAgICAgICAgdHJhbnNpdGlvbjogYm9yZGVyLWNvbG9yIDAuMTVzIGVhc2UtaW4tb3V0LCBib3gtc2hhZG93IDAuMTVzIGVhc2UtaW4tb3V0O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaW5wdXQ6OnBsYWNlaG9sZGVyIHtcclxuICAgICAgICAgICAgY29sb3I6ICM4NzhFOTUgIWltcG9ydGFudDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlucHV0OmRpc2FibGVkIHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2VlZSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICBjb2xvcjogIzY2NiAhaW1wb3J0YW50O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufSIsIi5iZy1kZWxldGUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlM2YzO1xuICBwYWRkaW5nOiAxMHB4O1xufVxuXG4ubmctcHJpc3RpbmUge1xuICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbn1cblxuLm5nLXByaXN0aW5lIHtcbiAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIHBhZGRpbmc6IDZweCAwcHg7XG59XG5cbi5tZW1iZXItYnVsay11cGRhdGUtY29udGFpbmVyIHtcbiAgcGFkZGluZzogMTBweCAhaW1wb3J0YW50O1xufVxuXG4uY29udGFpbmVyLXR3by1idXR0b24ge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuXG4uZXJyb3ItbXNnLXVwbG9hZCB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIHBhZGRpbmctbGVmdDogMHB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctcmlnaHQ6IDBweCAhaW1wb3J0YW50O1xufVxuXG4uZmlsZS1zZWxlY3RlZCB7XG4gIGNvbG9yOiByZWQ7XG59XG5cbi5idG4tYWRkLWJ1bGsge1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuXG4uYnRuLXVwbG9hZCwgLmJ0bi1zdWJtaXQge1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5cbi5idG4tdXBsb2FkIHtcbiAgYm9yZGVyLWNvbG9yOiBibGFjaztcbn1cblxuLmJ0bi1zdWJtaXQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5zdGFyLXJlcXVpcmVkIHtcbiAgY29sb3I6IHJlZDtcbn1cblxuLnRleHQtcmVxdWlyZWQge1xuICBjb2xvcjogcmVkO1xuICBmb250LXNpemU6IDExcHg7XG4gIG1hcmdpbi10b3A6IC0xNXB4O1xufVxuXG5sYWJlbCB7XG4gIGNvbG9yOiBibGFjaztcbn1cblxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtaW4taGVpZ2h0OiA0OHB4O1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLXRvcDogLTRweDtcbiAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgY29sb3I6ICM1NTU7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuc2F2ZV9idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcbiAgY29sb3I6IHdoaXRlO1xuICBoZWlnaHQ6IDMwcHg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHJpZ2h0OiAxMHB4O1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogMTVweDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50ID4gLmNvbC1tZC0xMiB7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgaDEge1xuICBmb250LXNpemU6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzU1NTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgZm9ybS1zZWxlY3RbbmFtZT1zdGF0dXNdIC5mb3JtLWdyb3VwIHtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGxhYmVsIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBsYWJlbCArIGRpdiB7XG4gIGJhY2tncm91bmQ6ICNmMGYwZjA7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIHBhZGRpbmc6IDBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2Y4ZjlmYTtcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgbWluLWhlaWdodDogMjdweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzY2Njtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciBoMiB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBsaW5lLWhlaWdodDogdW5zZXQ7XG4gIGhlaWdodDogdW5zZXQ7XG4gIG1hcmdpbjogMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cblxuLnJvdW5kZWQtYnRuIHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA3MHB4O1xuICBib3JkZXItcmFkaXVzOiA3MHB4O1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XG4gIHBhZGRpbmc6IDAgNDBweDtcbiAgYmFja2dyb3VuZDogIzU2YTRmZjtcbn1cblxuLnJvdW5kZWQtYnRuLWRhbmdlciB7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNzBweDtcbiAgYm9yZGVyLXJhZGl1czogNzBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE4cHg7XG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xuICBwYWRkaW5nOiAwIDQwcHg7XG4gIGNvbG9yOiAjNTU1O1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cblxuLnJvdW5kZWQtYnRuOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogIzBhN2JmZjtcbiAgb3V0bGluZTogbm9uZTtcbn1cblxuLnNhdmUtYnV0dG9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcbiAgY29sb3I6IHdoaXRlO1xuICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuXG46Om5nLWRlZXAgLm5nLWF1dG9jb21wbGV0ZSB7XG4gIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG59XG46Om5nLWRlZXAgLm5nLWF1dG9jb21wbGV0ZSAuYXV0b2NvbXBsZXRlLWNvbnRhaW5lciB7XG4gIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcbn1cbjo6bmctZGVlcCAubmctYXV0b2NvbXBsZXRlIC5hdXRvY29tcGxldGUtY29udGFpbmVyIC54IHtcbiAgZGlzcGxheTogbm9uZTtcbn1cbjo6bmctZGVlcCAubmctYXV0b2NvbXBsZXRlIC5hdXRvY29tcGxldGUtY29udGFpbmVyIGlucHV0IHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBhZGRpbmc6IDAuMzc1cmVtIDAuNzVyZW0gIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxcmVtICFpbXBvcnRhbnQ7XG4gIGxpbmUtaGVpZ2h0OiAxLjUgIWltcG9ydGFudDtcbiAgY29sb3I6ICM0OTUwNTcgIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZiAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kLWNsaXA6IHBhZGRpbmctYm94O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2VkNGRhICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1yYWRpdXM6IDAuMjVyZW07XG4gIHRyYW5zaXRpb246IGJvcmRlci1jb2xvciAwLjE1cyBlYXNlLWluLW91dCwgYm94LXNoYWRvdyAwLjE1cyBlYXNlLWluLW91dDtcbn1cbjo6bmctZGVlcCAubmctYXV0b2NvbXBsZXRlIC5hdXRvY29tcGxldGUtY29udGFpbmVyIGlucHV0OjpwbGFjZWhvbGRlciB7XG4gIGNvbG9yOiAjODc4RTk1ICFpbXBvcnRhbnQ7XG59XG46Om5nLWRlZXAgLm5nLWF1dG9jb21wbGV0ZSAuYXV0b2NvbXBsZXRlLWNvbnRhaW5lciBpbnB1dDpkaXNhYmxlZCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlZWUgIWltcG9ydGFudDtcbiAgY29sb3I6ICM2NjYgIWltcG9ydGFudDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/form-member/add/form-member.add.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/layout/modules/form-member/add/form-member.add.component.ts ***!
  \*****************************************************************************/
/*! exports provided: FormMemberAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormMemberAddComponent", function() { return FormMemberAddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var FormMemberAddComponent = /** @class */ (function () {
    function FormMemberAddComponent(memberService) {
        this.memberService = memberService;
        this.contentList = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4__;
        this.name = "";
        // mci_project: any = false;
        this.programType = "";
        this.Members = [];
        this.form = {
            id_pel: "",
            nama_toko: "",
            nama_distributor: "",
            alamat_toko: "",
            kelurahan_toko: "-",
            kecamatan_toko: "-",
            kota_toko: "-",
            provinsi_toko: "-",
            kode_pos_toko: "-",
            landmark_toko: "-",
            nama_pemilik: "",
            telp_pemilik: "",
            no_wa_pemilik: "",
            ktp_pemilik: "",
            foto_ktp_pemilik: "-",
            npwp_pemilik: "",
            foto_npwp_pemilik: "-",
            alamat_rumah: "",
            kelurahan_rumah: "",
            kecamatan_rumah: "",
            kota_rumah: "",
            provinsi_rumah: "",
            kode_pos_rumah: "",
            landmark_rumah: "-",
            description: "nonupfront",
            cluster: "-",
            // description_mci: "",
            hadiah_dikuasakan: "TIDAK KUASA",
            nama_penerima_gopay: "-",
            ktp_penerima: "-",
            foto_ktp_penerima: "-",
            npwp_penerima: "-",
            foto_npwp_penerima: "-",
            telp_penerima_gopay: "-",
            no_wa_penerima: "-",
            nama_id_gopay: "-",
            jenis_akun: "-",
            saldo_terakhir: "-",
        };
        this.data = {
            username: "",
            password: "",
        };
        this.errorLabel = false;
        this.member_status_type = [
            { label: 'Superuser', value: 'superuser' },
            { label: 'Member', value: 'member', selected: 1 },
            { label: 'Marketing', value: 'marketing' },
            { label: 'Admin', value: 'admin' },
            { label: 'Merchant', value: 'merchant' }
        ];
        this.group_desc = [
            { label: 'Nonupfront', value: 'nonupfront' },
            { label: 'Upfront', value: 'upfront' }
        ];
        this.kuasa_option = [
            { label: 'TIDAK KUASA', value: 'TIDAK KUASA' },
            { label: 'KUASA', value: 'KUASA' }
        ];
        this.isBulkUpdate = false;
        this.selectedFile = null;
        this.cancel = false;
        this.progressBar = 0;
        this.errorFile = false;
        this.prodOnUpload = false;
        this.startUploading = false;
        this.showUploadButton = false;
        this.successUpload = false;
        this.urlDownload = "";
        this.warnIDPelanggan = false;
        this.warnNamaPemilik = false;
        this.warnTelpPemilik = false;
        this.warnWAPemilik = false;
        this.showPasswordPage = false;
        this.keywordProvince = 'province';
        this.keywordCity = 'city';
        this.keywordSubDistrict = 'subdistrict';
        this.keywordVillage = 'village';
        this.dataProvince = [];
        this.dataCity = [];
        this.dataSubDistrict = [];
        this.dataVillage = [];
        this.loadingProvince = false;
        this.loadingCity = false;
        this.loadingSubDistrict = false;
        this.loadingVillage = false;
        this.placeholderProvince = 'Provinsi Rumah';
        this.placeholderCity = 'Kabupaten/Kota Rumah';
        this.placeholderSubDistrict = 'Kecamatan Rumah';
        this.placeholderVillage = 'Desa/Kelurahan Rumah';
    }
    FormMemberAddComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    FormMemberAddComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var program, _this;
            var _this_1 = this;
            return __generator(this, function (_a) {
                program = localStorage.getItem('programName');
                _this = this;
                this.contentList.filter(function (element) {
                    if (element.appLabel == program) {
                        if (element.type == "reguler") {
                            _this.programType = "reguler";
                        }
                        else if (element.type == "custom_kontraktual") {
                            _this.programType = "custom_kontraktual";
                            _this.form.description = "-";
                        }
                        else {
                            _this.programType = "custom";
                        }
                    }
                });
                this.selectedFile = null;
                this.startUploading = false;
                this.kuasa_option.forEach(function (element, index) {
                    if (element.value == "TIDAK KUASA") {
                        _this_1.kuasa_option[index].selected = 1;
                    }
                });
                this.group_desc.forEach(function (element, index) {
                    if (element.value == "nonupfront") {
                        _this_1.group_desc[index].selected = 1;
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    FormMemberAddComponent.prototype.formSubmitAddMember = function () {
        return __awaiter(this, void 0, void 0, function () {
            var form_add, result, e_1, message;
            var _this_1 = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        form_add = this.form;
                        //check if object from autocomplete component
                        if (form_add) {
                            if (form_add.provinsi_rumah && form_add.provinsi_rumah && typeof form_add.provinsi_rumah === 'object' && form_add.provinsi_rumah !== null && form_add.provinsi_rumah.province) {
                                form_add.provinsi_rumah = form_add.provinsi_rumah.province;
                            }
                            if (form_add.kota_rumah && form_add.kota_rumah && typeof form_add.kota_rumah === 'object' && form_add.kota_rumah !== null && form_add.kota_rumah.city) {
                                form_add.kota_rumah = form_add.kota_rumah.city;
                            }
                            if (form_add.kecamatan_rumah && form_add.kecamatan_rumah && typeof form_add.kecamatan_rumah === 'object' && form_add.kecamatan_rumah !== null && form_add.kecamatan_rumah.subdistrict) {
                                form_add.kecamatan_rumah = form_add.kecamatan_rumah.subdistrict;
                            }
                            if (form_add.kelurahan_rumah && form_add.kelurahan_rumah && typeof form_add.kelurahan_rumah === 'object' && form_add.kelurahan_rumah !== null && form_add.kelurahan_rumah.village) {
                                form_add.kelurahan_rumah = form_add.kelurahan_rumah.village;
                            }
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        this.service = this.memberService;
                        return [4 /*yield*/, this.memberService.addNewMember(form_add)];
                    case 2:
                        result = _a.sent();
                        console.warn("result add member", result);
                        if (result && result.data) {
                            this.getPassword = result.data.password;
                        }
                        this.data.username = this.form.id_pel;
                        this.data.password = this.getPassword;
                        // this.Members = result.result;
                        // alert("Member Has Been Added!")
                        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                            title: 'Success',
                            text: 'Pelanggan berhasil ditambahkan',
                            icon: 'success',
                            confirmButtonText: 'Ok',
                        }).then(function (result) {
                            if (result.isConfirmed) {
                                // this.backTo();
                                _this_1.showThisPassword();
                            }
                        });
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        console.warn("error", e_1.message);
                        // alert(e.message);
                        this.errorLabel = (e_1.message);
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                            title: 'Failed',
                            text: message,
                            icon: 'warning',
                            confirmButtonText: 'Ok',
                        }).then(function (result) {
                            if (result.isConfirmed) {
                                // this.backTo();
                                _this_1.firstLoad();
                            }
                        });
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    FormMemberAddComponent.prototype.showThisPassword = function () {
        this.showPasswordPage = !this.showPasswordPage;
        if (this.showPasswordPage == false) {
            this.firstLoad();
        }
        // console.log(this.edit );
    };
    FormMemberAddComponent.prototype.onFileSelected = function (event) {
        var _this_1 = this;
        this.errorFile = false;
        var fileMaxSize = 3000000; // let say 3Mb
        // var reader = new FileReader();
        // reader.readAsDataURL(event.target.files[0]); //
        Array.from(event.target.files).forEach(function (file) {
            if (file.size > fileMaxSize) {
                _this_1.errorFile = "maximum file is 3Mb";
            }
        });
        this.selectedFile = event.target.files[0];
        this.addBulk.nativeElement.value = '';
    };
    FormMemberAddComponent.prototype.cancelThis = function () {
        this.cancel = !this.cancel;
    };
    FormMemberAddComponent.prototype.actionShowUploadButton = function () {
        this.showUploadButton = !this.showUploadButton;
    };
    FormMemberAddComponent.prototype.updateDataBulk = function () {
        return __awaiter(this, void 0, void 0, function () {
            var payload, result, e_2, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.startUploading = true;
                        this.cancel = false;
                        payload = {
                            type: 'application/form-data',
                        };
                        console.warn("start upload", this.startUploading);
                        if (!this.selectedFile) return [3 /*break*/, 2];
                        console.log('file', this.selectedFile, this);
                        return [4 /*yield*/, this.memberService.registerBulkMember(this.selectedFile, this, payload)];
                    case 1:
                        result = _a.sent();
                        if (result) {
                            this.firstLoad();
                        }
                        _a.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        e_2 = _a.sent();
                        this.startUploading = false;
                        this.errorLabel = (e_2.message); //conversion to Error type
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    FormMemberAddComponent.prototype.updateProgressBar = function (value) {
        this.progressBar = value;
    };
    FormMemberAddComponent.prototype.backTo = function () {
        window.history.back();
    };
    FormMemberAddComponent.prototype.selectEvent = function (item, region) {
        // do something with selected item
        if (region == "province") {
            this.form.provinsi_rumah = item.province;
            this.province_code = item.province_code;
        }
        else if (region == "city") {
            this.form.kota_rumah = item.city;
            this.city_code = item.city_code;
        }
        else if (region == "subdistrict") {
            this.form.kecamatan_rumah = item.subdistrict;
            this.subdistrict_code = item.subdistrict_code;
        }
        else if (region == "village") {
            this.form.kelurahan_rumah = item.village;
            this.village_code = item.village_code;
        }
    };
    FormMemberAddComponent.prototype.inputRegion = function (val, region) {
        return __awaiter(this, void 0, void 0, function () {
            var _this_1 = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: 
                    //prevent calling API twice or more
                    return [4 /*yield*/, clearTimeout(this.inputTimeOut)];
                    case 1:
                        //prevent calling API twice or more
                        _a.sent();
                        this.isInputComplete = false;
                        //set loading indicator
                        if (region == "province") {
                            this.loadingProvince = true;
                            if (this.programType != "reguler") {
                                this.form.kota_rumah = "";
                                this.form.kecamatan_rumah = "";
                                this.form.kelurahan_rumah = "";
                                delete this.province_code;
                                delete this.city_code;
                                delete this.subdistrict_code;
                            }
                        }
                        else if (region == "city") {
                            this.loadingCity = true;
                            if (this.programType != "reguler") {
                                this.form.kecamatan_rumah = "";
                                this.form.kelurahan_rumah = "";
                                delete this.city_code;
                                delete this.subdistrict_code;
                            }
                        }
                        else if (region == "subdistrict") {
                            this.loadingSubDistrict = true;
                            if (this.programType != "reguler") {
                                delete this.subdistrict_code;
                                this.form.kelurahan_rumah = "";
                            }
                        }
                        else if (region == "village") {
                            this.loadingVillage = true;
                        }
                        this.inputTimeOut = setTimeout(function () {
                            _this_1.isInputComplete = true;
                            if (_this_1.isInputComplete) {
                                _this_1.filterRegion(val, region);
                            }
                        }, 300);
                        return [2 /*return*/];
                }
            });
        });
    };
    FormMemberAddComponent.prototype.filterRegion = function (search, region) {
        var _this_1 = this;
        var payload = {
            "region": region,
            "search": search
        };
        if (region == "city") {
            payload.province_code = this.province_code;
        }
        else if (region == "subdistrict") {
            payload.city_code = this.city_code;
        }
        else if (region == "village") {
            payload.subdistrict_code = this.subdistrict_code;
        }
        console.log(payload);
        // Set val to the value of the searchbar
        if (search.length > 0) {
            this.memberService.searchRegion(payload).then(function (result) {
                _this_1.loadingProvince = false;
                _this_1.loadingCity = false;
                _this_1.loadingSubDistrict = false;
                _this_1.loadingVillage = false;
                if (result && result.length > 0) {
                    if (region == "province") {
                        _this_1.dataProvince = result;
                    }
                    else if (region == "city") {
                        _this_1.dataCity = result;
                    }
                    else if (region == "subdistrict") {
                        console.log("result", result);
                        _this_1.dataSubDistrict = result;
                    }
                    else if (region == "village") {
                        _this_1.dataVillage = result;
                    }
                }
            }, function (err) {
                console.log(err);
                _this_1.loadingProvince = false;
                _this_1.loadingCity = false;
                _this_1.loadingSubDistrict = false;
                _this_1.loadingVillage = false;
                _this_1.dataProvince = [];
                _this_1.dataCity = [];
                _this_1.dataSubDistrict = [];
                _this_1.dataVillage = [];
            });
        }
        else {
            this.loadingProvince = false;
            this.loadingCity = false;
            this.loadingSubDistrict = false;
            this.loadingVillage = false;
            this.dataProvince = [];
            this.dataCity = [];
            this.dataSubDistrict = [];
            this.dataVillage = [];
        }
    };
    FormMemberAddComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FormMemberAddComponent.prototype, "back", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FormMemberAddComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('addBulk', { static: false }),
        __metadata("design:type", Object)
    ], FormMemberAddComponent.prototype, "addBulk", void 0);
    FormMemberAddComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-form-member-add',
            template: __webpack_require__(/*! raw-loader!./form-member.add.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/form-member/add/form-member.add.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./form-member.add.component.scss */ "./src/app/layout/modules/form-member/add/form-member.add.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"]])
    ], FormMemberAddComponent);
    return FormMemberAddComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/form-member/additional-document/additional-document.component.scss":
/*!***************************************************************************************************!*\
  !*** ./src/app/layout/modules/form-member/additional-document/additional-document.component.scss ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n}\n.card-detail > .card-header .back_button {\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 7px;\n  background-color: #3498db;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n.card-detail > .card-header .save_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button.true {\n  background-color: #e67e22;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .member-detail .image {\n  overflow: hidden;\n}\n.card-detail .member-detail .image > div {\n  width: 33.3333%;\n  padding: 5px;\n  float: left;\n}\n.card-detail .member-detail .image .card-header {\n  background-color: #f0f0f0;\n}\n.card-detail .member-detail .image h3 {\n  font-size: 12px;\n  margin-bottom: 0px;\n  color: #555;\n}\n.card-detail .member-detail .image .card-content {\n  overflow: hidden;\n  width: 100%;\n  height: auto;\n}\n.card-detail .member-detail .image .card-content img {\n  max-width: 100%;\n  width: 100%;\n}\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n.rounded-btn {\n  margin-top: 20px;\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  background: #56a4ff;\n}\n.rounded-btn-danger {\n  margin-top: 20px;\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  color: #555;\n  margin-left: 10px;\n}\n.rounded-btn:hover {\n  background: #0a7bff;\n  outline: none;\n}\n.member-detail-image-container {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.member-detail-image-container .img-upload {\n  display: flex;\n  justify-content: space-around;\n  align-items: center;\n  margin: 20px 10px;\n}\n.member-detail-image-container .img-upload .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.member-detail-image-container .form-group.uploaded {\n  position: relative;\n  margin-bottom: 0px;\n  height: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading {\n  height: 100%;\n  width: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading .sk-fading-circle {\n  top: 50%;\n}\nbutton.btn_upload, button.add-more-image, button.btn_delete {\n  background-color: #3498db;\n  color: #fff;\n  padding: 5px 15px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 4px;\n  border: 0;\n  cursor: pointer;\n}\nbutton.btn-change-password {\n  width: 100%;\n}\nbutton.btn_delete {\n  background-color: salmon;\n}\n.add-more-container {\n  display: flex;\n  width: 100%;\n  align-items: center;\n  justify-content: center;\n}\n.label-bold {\n  font-weight: 600;\n}\n.error-msg-upload {\n  margin-top: 10px;\n  padding-left: 0px !important;\n  padding-right: 0px !important;\n}\n.container-two-button {\n  display: flex;\n}\n.file-selected {\n  color: red;\n}\n.btn-add-bulk {\n  margin-bottom: 20px;\n}\n.btn-upload {\n  margin-right: 10px;\n}\n.member-detail-image-container {\n  background: #f0f0f0;\n  font-weight: normal;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.member-detail-image-container img {\n  width: 100%;\n}\n.member-detail-image-container a {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.member-detail-image-container a .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.member-detail-image-container .btn-download-container {\n  margin-top: 5px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.member-detail-image-container .img-upload {\n  display: flex;\n  justify-content: space-around;\n  align-items: center;\n  margin: 20px 10px;\n}\n.member-detail-image-container .img-upload .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\nbutton.btn-download-img {\n  margin-top: 10px;\n  background-color: #3498db;\n  color: #fff;\n  padding: 5px 15px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 4px;\n  border: 0;\n  cursor: pointer;\n}\n.edit_button {\n  position: absolute;\n  top: 7px;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n.edit_button i {\n  font-weight: bold;\n}\n.btn-right {\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-left: 10px;\n}\n.error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 26px;\n  padding-top: 10px;\n}\n.error-message .text-message {\n  background: #E74C3C;\n  padding: 20px;\n  margin: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZm9ybS1tZW1iZXIvYWRkaXRpb25hbC1kb2N1bWVudC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXGZvcm0tbWVtYmVyXFxhZGRpdGlvbmFsLWRvY3VtZW50XFxhZGRpdGlvbmFsLWRvY3VtZW50LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9mb3JtLW1lbWJlci9hZGRpdGlvbmFsLWRvY3VtZW50L2FkZGl0aW9uYWwtZG9jdW1lbnQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxrQkFBQTtBQ0FSO0FEQ1E7RUFFSSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0RaO0FERVk7RUFDSSxpQkFBQTtBQ0FoQjtBREdRO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0EseUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFLQSxXQUFBO0FDTlo7QURHWTtFQUNJLGlCQUFBO0FDRGhCO0FES1E7RUFDSSx5QkFBQTtBQ0haO0FETUk7RUFDSSxhQUFBO0FDSlI7QURNUTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7QUNKWjtBRFFJO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNOUjtBRFVRO0VBQ0ksZ0JBQUE7QUNSWjtBRFVRO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ1JaO0FEVVE7RUFDSSxnQkFBQTtBQ1JaO0FEU1k7RUFDSSxlQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUNQaEI7QURTWTtFQUNJLHlCQUFBO0FDUGhCO0FEU1k7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FDUGhCO0FEU1k7RUFDSSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FDUGhCO0FEUWdCO0VBQ0ksZUFBQTtFQUNBLFdBQUE7QUNOcEI7QURZUTtFQUNJLHNCQUFBO0FDVlo7QURXWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDVGhCO0FEZ0JBO0VBQ0ksZ0JBQUE7RUFFQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7QUNiSjtBRGtCQTtFQUNJLGdCQUFBO0VBRUEsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtBQ2ZKO0FEaUJBO0VBQ0ksbUJBQUE7RUFFQSxhQUFBO0FDZko7QURrQkE7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDZko7QURnQkk7RUFDSSxhQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDZFI7QURnQlE7RUFDSSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtLQUFBLG1CQUFBO0FDZFo7QURrQkk7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQ2hCUjtBRGlCUTtFQUNJLFlBQUE7RUFFQSxXQUFBO0FDaEJaO0FEaUJZO0VBRUksUUFBQTtBQ2hCaEI7QUR1QkE7RUFDSSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtBQ3BCSjtBRHVCQTtFQUNJLFdBQUE7QUNwQko7QUR1QkE7RUFDSSx3QkFBQTtBQ3BCSjtBRHVCQTtFQUNJLGFBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQ3BCSjtBRHVCQTtFQUNJLGdCQUFBO0FDcEJKO0FEdUJBO0VBQ0ksZ0JBQUE7RUFDQSw0QkFBQTtFQUNBLDZCQUFBO0FDcEJKO0FEdUJBO0VBQ0ksYUFBQTtBQ3BCSjtBRHVCQTtFQUNJLFVBQUE7QUNwQko7QUR1QkE7RUFDSSxtQkFBQTtBQ3BCSjtBRHVCQTtFQUNJLGtCQUFBO0FDcEJKO0FEdUJBO0VBQ0ksbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ3BCSjtBRHFCSTtFQUNJLFdBQUE7QUNuQlI7QURxQkk7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQ25CUjtBRG9CUTtFQUNJLFlBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0tBQUEsbUJBQUE7QUNsQlo7QURxQkk7RUFDSSxlQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUNuQlI7QURxQkk7RUFDSSxhQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDbkJSO0FEcUJRO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7S0FBQSxtQkFBQTtBQ25CWjtBRHdCQTtFQUNJLGdCQUFBO0VBQ0EseUJBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGVBQUE7QUNyQko7QUR3QkE7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFFQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFJQSxXQUFBO0FDekJKO0FEc0JJO0VBQ0ksaUJBQUE7QUNwQlI7QUR3QkE7RUFDSSxzQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDckJKO0FEd0JBO0VBQ0ksaUJBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUNyQko7QUR1Qkk7RUFDSSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0FDckJSIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZm9ybS1tZW1iZXIvYWRkaXRpb25hbC1kb2N1bWVudC9hZGRpdGlvbmFsLWRvY3VtZW50LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNhcmQtZGV0YWlse1xyXG4gICAgPi5jYXJkLWhlYWRlcntcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgLmJhY2tfYnV0dG9ue1xyXG4gICAgICAgICAgICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC00cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtMTBweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zYXZlX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDdweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICBcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJpZ2h0OiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuc2F2ZV9idXR0b24udHJ1ZXtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2U2N2UyMjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICBcclxuICAgICAgICA+LmNvbC1tZC0xMntcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBcclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLm1lbWJlci1kZXRhaWx7XHJcbiAgICAgICBcclxuICAgICAgICBsYWJlbHtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWwrZGl2e1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDpub3JtYTtcclxuICAgICAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgICAgICAgICAgbWluLWhlaWdodDogMjdweDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICBjb2xvcjogIzY2NjtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmltYWdle1xyXG4gICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICA+ZGl2e1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDMzLjMzMzMlO1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICAgICAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLmNhcmQtaGVhZGVye1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBoM3tcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgICAgICAgICAgICBpbWd7XHJcbiAgICAgICAgICAgICAgICAgICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5jYXJkLWhlYWRlcnsgXHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGgye1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi5yb3VuZGVkLWJ0biB7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNzBweDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIGxpbmUtaGVpZ2h0OiA0MHB4O1xyXG4gICAgcGFkZGluZzogMCA0MHB4O1xyXG4gICAgYmFja2dyb3VuZDogIzU2YTRmZjtcclxuICAgIC8vIG1hcmdpbi1sZWZ0OiA0MCU7XHJcbiAgICAvLyBtYXJnaW4tcmlnaHQ6IDUwJTtcclxufVxyXG5cclxuLnJvdW5kZWQtYnRuLWRhbmdlciB7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNzBweDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIGxpbmUtaGVpZ2h0OiA0MHB4O1xyXG4gICAgcGFkZGluZzogMCA0MHB4O1xyXG4gICAgY29sb3I6ICM1NTU7XHJcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcclxufVxyXG4ucm91bmRlZC1idG46aG92ZXIge1xyXG4gICAgYmFja2dyb3VuZDogZGFya2VuKCM1NkE0RkYsIDE1JSk7XHJcbiAgICAvLyBib3JkZXI6IDJweCBzb2xpZCBibHVlO1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxufVxyXG5cclxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIHtcclxuICAgIGJhY2tncm91bmQ6ICNmMGYwZjA7XHJcbiAgICBmb250LXdlaWdodDogbm9ybWE7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICBtaW4taGVpZ2h0OiAyN3B4O1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgY29sb3I6ICM2NjY7XHJcbiAgICAuaW1nLXVwbG9hZCB7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbjoyMHB4IDEwcHg7XHJcblxyXG4gICAgICAgIC5tZW1iZXItZGV0YWlsLWltYWdlIHtcclxuICAgICAgICAgICAgd2lkdGg6MjAwcHg7XHJcbiAgICAgICAgICAgIG1heC1oZWlnaHQ6IDIwMHB4O1xyXG4gICAgICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuZm9ybS1ncm91cC51cGxvYWRlZCB7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgLmxvYWRpbmcge1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgIC8vIGJhY2tncm91bmQtY29sb3I6ICMwMDA7XHJcbiAgICAgICAgICAgIHdpZHRoOjEwMCU7XHJcbiAgICAgICAgICAgIC5zay1mYWRpbmctY2lyY2xlIHtcclxuICAgICAgICAgICAgICAgIC8vIGJhY2tncm91bmQtY29sb3I6cmVkO1xyXG4gICAgICAgICAgICAgICAgdG9wOiA1MCU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5idXR0b24uYnRuX3VwbG9hZCwgYnV0dG9uLmFkZC1tb3JlLWltYWdlLCBidXR0b24uYnRuX2RlbGV0ZSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBwYWRkaW5nOiA1cHggMTVweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgICBib3JkZXI6MDtcclxuICAgIGN1cnNvcjpwb2ludGVyO1xyXG59XHJcblxyXG5idXR0b24uYnRuLWNoYW5nZS1wYXNzd29yZCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuYnV0dG9uLmJ0bl9kZWxldGV7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBzYWxtb247XHJcbn1cclxuXHJcbi5hZGQtbW9yZS1jb250YWluZXIge1xyXG4gICAgZGlzcGxheTpmbGV4O1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGFsaWduLWl0ZW1zOmNlbnRlcjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG59XHJcblxyXG4ubGFiZWwtYm9sZCB7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG59XHJcblxyXG4uZXJyb3ItbXNnLXVwbG9hZCB7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OiAwcHggIWltcG9ydGFudDtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDBweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uY29udGFpbmVyLXR3by1idXR0b24ge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxufVxyXG5cclxuLmZpbGUtc2VsZWN0ZWQge1xyXG4gICAgY29sb3I6IHJlZDtcclxufVxyXG5cclxuLmJ0bi1hZGQtYnVsayB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG59XHJcblxyXG4uYnRuLXVwbG9hZCB7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbn1cclxuXHJcbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBjb2xvcjogIzY2NjtcclxuICAgIGltZyB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbiAgICBhIHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgLm1lbWJlci1kZXRhaWwtaW1hZ2Uge1xyXG4gICAgICAgICAgICB3aWR0aDoyMDBweDtcclxuICAgICAgICAgICAgbWF4LWhlaWdodDogMjAwcHg7XHJcbiAgICAgICAgICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmJ0bi1kb3dubG9hZC1jb250YWluZXIge1xyXG4gICAgICAgIG1hcmdpbi10b3A6NXB4O1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIH1cclxuICAgIC5pbWctdXBsb2FkIHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luOjIwcHggMTBweDtcclxuXHJcbiAgICAgICAgLm1lbWJlci1kZXRhaWwtaW1hZ2Uge1xyXG4gICAgICAgICAgICB3aWR0aDoyMDBweDtcclxuICAgICAgICAgICAgbWF4LWhlaWdodDogMjAwcHg7XHJcbiAgICAgICAgICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5idXR0b24uYnRuLWRvd25sb2FkLWltZyB7XHJcbiAgICBtYXJnaW4tdG9wOjEwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBwYWRkaW5nOiA1cHggMTVweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgICBib3JkZXI6MDtcclxuICAgIGN1cnNvcjpwb2ludGVyO1xyXG59XHJcblxyXG4uZWRpdF9idXR0b257XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDdweDtcclxuICAgIC8vIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIGhlaWdodDogMzBweDtcclxuICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGl7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB9XHJcbiAgICByaWdodDogMTBweDtcclxufVxyXG4uYnRuLXJpZ2h0IHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIGhlaWdodDogMzBweDtcclxuICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG59XHJcblxyXG4uZXJyb3ItbWVzc2FnZSB7XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIGhlaWdodDoxMDB2aDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZToyNnB4O1xyXG4gICAgcGFkZGluZy10b3A6IDEwcHg7XHJcblxyXG4gICAgLnRleHQtbWVzc2FnZSB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI0U3NEMzQztcclxuICAgICAgICBwYWRkaW5nOiAyMHB4O1xyXG4gICAgICAgIG1hcmdpbjogMCAxMHB4O1xyXG4gICAgfVxyXG59IiwiLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiB7XG4gIG1hcmdpbi10b3A6IC00cHg7XG4gIG1hcmdpbi1sZWZ0OiAtMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGNvbG9yOiAjNTU1O1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLnNhdmVfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDdweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcbiAgY29sb3I6ICNmZmY7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICByaWdodDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuc2F2ZV9idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbi50cnVlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U2N2UyMjtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogMTVweDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50ID4gLmNvbC1tZC0xMiB7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgaDEge1xuICBmb250LXNpemU6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzU1NTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGxhYmVsICsgZGl2IHtcbiAgYmFja2dyb3VuZDogI2YwZjBmMDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtaW4taGVpZ2h0OiAyN3B4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNjY2O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlID4gZGl2IHtcbiAgd2lkdGg6IDMzLjMzMzMlO1xuICBwYWRkaW5nOiA1cHg7XG4gIGZsb2F0OiBsZWZ0O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSAuY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmMGYwO1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSBoMyB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBjb2xvcjogIzU1NTtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2UgLmNhcmQtY29udGVudCB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IGF1dG87XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlIC5jYXJkLWNvbnRlbnQgaW1nIHtcbiAgbWF4LXdpZHRoOiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciBoMiB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBsaW5lLWhlaWdodDogdW5zZXQ7XG4gIGhlaWdodDogdW5zZXQ7XG4gIG1hcmdpbjogMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cblxuLnJvdW5kZWQtYnRuIHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA3MHB4O1xuICBib3JkZXItcmFkaXVzOiA3MHB4O1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XG4gIHBhZGRpbmc6IDAgNDBweDtcbiAgYmFja2dyb3VuZDogIzU2YTRmZjtcbn1cblxuLnJvdW5kZWQtYnRuLWRhbmdlciB7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNzBweDtcbiAgYm9yZGVyLXJhZGl1czogNzBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE4cHg7XG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xuICBwYWRkaW5nOiAwIDQwcHg7XG4gIGNvbG9yOiAjNTU1O1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cblxuLnJvdW5kZWQtYnRuOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogIzBhN2JmZjtcbiAgb3V0bGluZTogbm9uZTtcbn1cblxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIHtcbiAgYmFja2dyb3VuZDogI2YwZjBmMDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtaW4taGVpZ2h0OiAyN3B4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNjY2O1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5pbWctdXBsb2FkIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbjogMjBweCAxMHB4O1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5pbWctdXBsb2FkIC5tZW1iZXItZGV0YWlsLWltYWdlIHtcbiAgd2lkdGg6IDIwMHB4O1xuICBtYXgtaGVpZ2h0OiAyMDBweDtcbiAgb2JqZWN0LWZpdDogY29udGFpbjtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuZm9ybS1ncm91cC51cGxvYWRlZCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBoZWlnaHQ6IDEwMCU7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLmxvYWRpbmcge1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC5sb2FkaW5nIC5zay1mYWRpbmctY2lyY2xlIHtcbiAgdG9wOiA1MCU7XG59XG5cbmJ1dHRvbi5idG5fdXBsb2FkLCBidXR0b24uYWRkLW1vcmUtaW1hZ2UsIGJ1dHRvbi5idG5fZGVsZXRlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcbiAgY29sb3I6ICNmZmY7XG4gIHBhZGRpbmc6IDVweCAxNXB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBib3JkZXI6IDA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuYnV0dG9uLmJ0bi1jaGFuZ2UtcGFzc3dvcmQge1xuICB3aWR0aDogMTAwJTtcbn1cblxuYnV0dG9uLmJ0bl9kZWxldGUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBzYWxtb247XG59XG5cbi5hZGQtbW9yZS1jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICB3aWR0aDogMTAwJTtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5sYWJlbC1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cblxuLmVycm9yLW1zZy11cGxvYWQge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBwYWRkaW5nLWxlZnQ6IDBweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nLXJpZ2h0OiAwcHggIWltcG9ydGFudDtcbn1cblxuLmNvbnRhaW5lci10d28tYnV0dG9uIHtcbiAgZGlzcGxheTogZmxleDtcbn1cblxuLmZpbGUtc2VsZWN0ZWQge1xuICBjb2xvcjogcmVkO1xufVxuXG4uYnRuLWFkZC1idWxrIHtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cblxuLmJ0bi11cGxvYWQge1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciB7XG4gIGJhY2tncm91bmQ6ICNmMGYwZjA7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgYSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIGEgLm1lbWJlci1kZXRhaWwtaW1hZ2Uge1xuICB3aWR0aDogMjAwcHg7XG4gIG1heC1oZWlnaHQ6IDIwMHB4O1xuICBvYmplY3QtZml0OiBjb250YWluO1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5idG4tZG93bmxvYWQtY29udGFpbmVyIHtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuaW1nLXVwbG9hZCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW46IDIwcHggMTBweDtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuaW1nLXVwbG9hZCAubWVtYmVyLWRldGFpbC1pbWFnZSB7XG4gIHdpZHRoOiAyMDBweDtcbiAgbWF4LWhlaWdodDogMjAwcHg7XG4gIG9iamVjdC1maXQ6IGNvbnRhaW47XG59XG5cbmJ1dHRvbi5idG4tZG93bmxvYWQtaW1nIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcbiAgY29sb3I6ICNmZmY7XG4gIHBhZGRpbmc6IDVweCAxNXB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBib3JkZXI6IDA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLmVkaXRfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDdweDtcbiAgY29sb3I6ICNmZmY7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICByaWdodDogMTBweDtcbn1cbi5lZGl0X2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5idG4tcmlnaHQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuXG4uZXJyb3ItbWVzc2FnZSB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBoZWlnaHQ6IDEwMHZoO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMjZweDtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59XG4uZXJyb3ItbWVzc2FnZSAudGV4dC1tZXNzYWdlIHtcbiAgYmFja2dyb3VuZDogI0U3NEMzQztcbiAgcGFkZGluZzogMjBweDtcbiAgbWFyZ2luOiAwIDEwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/form-member/additional-document/additional-document.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/layout/modules/form-member/additional-document/additional-document.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: AdditonalDocumentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdditonalDocumentComponent", function() { return AdditonalDocumentComponent; });
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





// import Swal from 'sweetalert2';
// import { basename } from 'path';
// import { ProductService } from '../../../../services/product/product.service';
var AdditonalDocumentComponent = /** @class */ (function () {
    function AdditonalDocumentComponent(memberService, productService, zone, orderHistoryService) {
        this.memberService = memberService;
        this.productService = productService;
        this.zone = zone;
        this.orderHistoryService = orderHistoryService;
        this.errorLabel = false;
        this.errorMessage = false;
        this.edit = false;
        this.selectedFile = null;
        this.cancel = false;
        this.progressBar = 0;
        this.errorFile = false;
        this.allBast = [];
        this.allSuratKuasa = [];
        this.loading = false;
        this.loadingBAST = [];
        this.loadingSuratKuasa = [];
        this.password = "";
    }
    AdditonalDocumentComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    AdditonalDocumentComponent.prototype.isArray = function (curVar) {
        return Array.isArray(curVar) ? true : false;
    };
    AdditonalDocumentComponent.prototype.assignArrayBoolean = function (curVar, varCont) {
        if (this.isArray(curVar)) {
            curVar.forEach(function (element) {
                varCont.push(false);
            });
        }
    };
    AdditonalDocumentComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var query, result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // console.warn("this detail", this.detail);
                        this.memberDetail = this.detail;
                        console.log("member detail", this.memberDetail);
                        query = {
                            search: {
                                username: this.memberDetail.username
                            },
                            limit_per_page: 0
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.orderHistoryService.searchAllReportSalesOrder(query)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    AdditonalDocumentComponent.prototype.backToDetail = function () {
        // console.log("Edit member",this.back);
        this.back[0][this.back[1]]();
        // this.back[0]();
    };
    AdditonalDocumentComponent.prototype.validURL = function (str) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return pattern.test(str);
    };
    AdditonalDocumentComponent.prototype.checkFileType = function (text) {
        var splitFileName = text.split('.');
        var extensFile = splitFileName[splitFileName.length - 1];
        var extens = extensFile.toLowerCase();
        return extens == 'pdf' ? 'document' : 'image';
    };
    AdditonalDocumentComponent.prototype.editThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.edit = !this.edit;
                        if (!(this.edit == false)) return [3 /*break*/, 4];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.memberService.getFormMemberByID(this.detail.username)];
                    case 2:
                        result = _a.sent();
                        if (result.length > 0) {
                            if (result[0].username == this.detail.username) {
                                this.detail = result[0];
                                this.firstLoad();
                            }
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    AdditonalDocumentComponent.prototype.downloadImage = function (url) {
        var filename;
        filename = this.lastPathURL(url);
        fetch(url).then(function (t) {
            return t.blob().then(function (b) {
                var a = document.createElement("a");
                a.href = URL.createObjectURL(b);
                a.setAttribute("download", filename);
                a.click();
            });
        });
    };
    AdditonalDocumentComponent.prototype.lastPathURL = function (url) {
        var resURL = url.split("/");
        console.log("resURL", resURL);
        var lastNumber = resURL.length - 1;
        return resURL[lastNumber];
    };
    AdditonalDocumentComponent.prototype.convertDate = function (time) {
        var date = new Date(time);
        var dateString = date.toString();
        var retDate = dateString.split("GMT");
        return retDate[0];
    };
    AdditonalDocumentComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"] },
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__["ProductService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] },
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_4__["OrderhistoryService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], AdditonalDocumentComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], AdditonalDocumentComponent.prototype, "back", void 0);
    AdditonalDocumentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-additional-document',
            template: __webpack_require__(/*! raw-loader!./additional-document.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/form-member/additional-document/additional-document.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_2__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./additional-document.component.scss */ "./src/app/layout/modules/form-member/additional-document/additional-document.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"],
            _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__["ProductService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"],
            _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_4__["OrderhistoryService"]])
    ], AdditonalDocumentComponent);
    return AdditonalDocumentComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/form-member/additional-document/edit/additional-document.edit.component.scss":
/*!*************************************************************************************************************!*\
  !*** ./src/app/layout/modules/form-member/additional-document/edit/additional-document.edit.component.scss ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n}\n.card-detail > .card-header .back_button {\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 7px;\n  background-color: #3498db;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n.card-detail > .card-header .save_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button.true {\n  background-color: #e67e22;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .member-detail .image {\n  overflow: hidden;\n}\n.card-detail .member-detail .image > div {\n  width: 33.3333%;\n  padding: 5px;\n  float: left;\n}\n.card-detail .member-detail .image .card-header {\n  background-color: #f0f0f0;\n}\n.card-detail .member-detail .image h3 {\n  font-size: 12px;\n  margin-bottom: 0px;\n  color: #555;\n}\n.card-detail .member-detail .image .card-content {\n  overflow: hidden;\n  width: 100%;\n  height: auto;\n}\n.card-detail .member-detail .image .card-content img {\n  max-width: 100%;\n  width: 100%;\n}\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n.member-detail-image-container {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.member-detail-image-container .img-upload {\n  display: flex;\n  justify-content: space-around;\n  align-items: center;\n  margin: 20px 10px;\n}\n.member-detail-image-container .img-upload .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.member-detail-image-container .form-group.uploaded {\n  position: relative;\n  margin-bottom: 0px;\n  height: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading {\n  height: 100%;\n  width: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading .sk-fading-circle {\n  top: 50%;\n}\nbutton.btn_upload, button.add-more-image, button.btn_delete {\n  background-color: #3498db;\n  color: #fff;\n  padding: 5px 15px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 4px;\n  border: 0;\n  cursor: pointer;\n}\nbutton.btn_delete {\n  background-color: salmon;\n}\n.add-more-container {\n  display: flex;\n  width: 100%;\n  align-items: center;\n  justify-content: center;\n}\n.label-bold {\n  font-weight: 600;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZm9ybS1tZW1iZXIvYWRkaXRpb25hbC1kb2N1bWVudC9lZGl0L0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcZm9ybS1tZW1iZXJcXGFkZGl0aW9uYWwtZG9jdW1lbnRcXGVkaXRcXGFkZGl0aW9uYWwtZG9jdW1lbnQuZWRpdC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZm9ybS1tZW1iZXIvYWRkaXRpb25hbC1kb2N1bWVudC9lZGl0L2FkZGl0aW9uYWwtZG9jdW1lbnQuZWRpdC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLGtCQUFBO0FDQVI7QURDUTtFQUVJLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSw2QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEseUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0FDRFo7QURFWTtFQUNJLGlCQUFBO0FDQWhCO0FER1E7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUtBLFdBQUE7QUNOWjtBREdZO0VBQ0ksaUJBQUE7QUNEaEI7QURLUTtFQUNJLHlCQUFBO0FDSFo7QURNSTtFQUNJLGFBQUE7QUNKUjtBRE1RO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ0paO0FEUUk7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQ05SO0FEVVE7RUFDSSxnQkFBQTtBQ1JaO0FEVVE7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDUlo7QURVUTtFQUNJLGdCQUFBO0FDUlo7QURTWTtFQUNJLGVBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQ1BoQjtBRFNZO0VBQ0kseUJBQUE7QUNQaEI7QURTWTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUNQaEI7QURTWTtFQUNJLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUNQaEI7QURRZ0I7RUFDSSxlQUFBO0VBQ0EsV0FBQTtBQ05wQjtBRFlRO0VBQ0ksc0JBQUE7QUNWWjtBRFdZO0VBQ0ksWUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBQ0EsMEJBQUE7QUNUaEI7QURnQkE7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDYko7QURjSTtFQUNJLGFBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7QUNaUjtBRGNRO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7S0FBQSxtQkFBQTtBQ1paO0FEZ0JJO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUNkUjtBRGVRO0VBQ0ksWUFBQTtFQUVBLFdBQUE7QUNkWjtBRGVZO0VBRUksUUFBQTtBQ2RoQjtBRHFCQTtFQUNJLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxlQUFBO0FDbEJKO0FEcUJBO0VBQ0ksd0JBQUE7QUNsQko7QURxQkE7RUFDSSxhQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUNsQko7QURxQkE7RUFDSSxnQkFBQTtBQ2xCSiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2Zvcm0tbWVtYmVyL2FkZGl0aW9uYWwtZG9jdW1lbnQvZWRpdC9hZGRpdGlvbmFsLWRvY3VtZW50LmVkaXQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2FyZC1kZXRhaWx7XHJcbiAgICA+LmNhcmQtaGVhZGVye1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAuYmFja19idXR0b257XHJcbiAgICAgICAgICAgIC8vIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogLTRweDtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLnNhdmVfYnV0dG9ue1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIHRvcDogN3B4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgIFxyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zYXZlX2J1dHRvbi50cnVle1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTY3ZTIyO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgICAgIFxyXG4gICAgICAgID4uY29sLW1kLTEye1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIFxyXG4gICAgaDF7XHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICB9XHJcbiAgICAubWVtYmVyLWRldGFpbHtcclxuICAgICAgIFxyXG4gICAgICAgIGxhYmVse1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYWJlbCtkaXZ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmMGYwZjA7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0Om5vcm1hO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgICAgICAgICBtaW4taGVpZ2h0OiAyN3B4O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNjY2O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuaW1hZ2V7XHJcbiAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICAgID5kaXZ7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMzMuMzMzMyU7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICAgICAgICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAuY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGgze1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLmNhcmQtY29udGVudHtcclxuICAgICAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgICAgICAgICAgIGltZ3tcclxuICAgICAgICAgICAgICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICBcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmNhcmQtaGVhZGVyeyBcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaDJ7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIHtcclxuICAgIGJhY2tncm91bmQ6ICNmMGYwZjA7XHJcbiAgICBmb250LXdlaWdodDogbm9ybWE7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICBtaW4taGVpZ2h0OiAyN3B4O1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgY29sb3I6ICM2NjY7XHJcbiAgICAuaW1nLXVwbG9hZCB7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbjoyMHB4IDEwcHg7XHJcblxyXG4gICAgICAgIC5tZW1iZXItZGV0YWlsLWltYWdlIHtcclxuICAgICAgICAgICAgd2lkdGg6MjAwcHg7XHJcbiAgICAgICAgICAgIG1heC1oZWlnaHQ6IDIwMHB4O1xyXG4gICAgICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuZm9ybS1ncm91cC51cGxvYWRlZCB7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgLmxvYWRpbmcge1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgIC8vIGJhY2tncm91bmQtY29sb3I6ICMwMDA7XHJcbiAgICAgICAgICAgIHdpZHRoOjEwMCU7XHJcbiAgICAgICAgICAgIC5zay1mYWRpbmctY2lyY2xlIHtcclxuICAgICAgICAgICAgICAgIC8vIGJhY2tncm91bmQtY29sb3I6cmVkO1xyXG4gICAgICAgICAgICAgICAgdG9wOiA1MCU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5idXR0b24uYnRuX3VwbG9hZCwgYnV0dG9uLmFkZC1tb3JlLWltYWdlLCBidXR0b24uYnRuX2RlbGV0ZSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBwYWRkaW5nOiA1cHggMTVweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgICBib3JkZXI6MDtcclxuICAgIGN1cnNvcjpwb2ludGVyO1xyXG59XHJcblxyXG5idXR0b24uYnRuX2RlbGV0ZXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHNhbG1vbjtcclxufVxyXG5cclxuLmFkZC1tb3JlLWNvbnRhaW5lciB7XHJcbiAgICBkaXNwbGF5OmZsZXg7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgYWxpZ24taXRlbXM6Y2VudGVyO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuXHJcbi5sYWJlbC1ib2xkIHtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbn1cclxuIiwiLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiB7XG4gIG1hcmdpbi10b3A6IC00cHg7XG4gIG1hcmdpbi1sZWZ0OiAtMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGNvbG9yOiAjNTU1O1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLnNhdmVfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDdweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcbiAgY29sb3I6ICNmZmY7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICByaWdodDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuc2F2ZV9idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbi50cnVlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U2N2UyMjtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogMTVweDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50ID4gLmNvbC1tZC0xMiB7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgaDEge1xuICBmb250LXNpemU6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzU1NTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGxhYmVsICsgZGl2IHtcbiAgYmFja2dyb3VuZDogI2YwZjBmMDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtaW4taGVpZ2h0OiAyN3B4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNjY2O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlID4gZGl2IHtcbiAgd2lkdGg6IDMzLjMzMzMlO1xuICBwYWRkaW5nOiA1cHg7XG4gIGZsb2F0OiBsZWZ0O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSAuY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmMGYwO1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSBoMyB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBjb2xvcjogIzU1NTtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2UgLmNhcmQtY29udGVudCB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IGF1dG87XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlIC5jYXJkLWNvbnRlbnQgaW1nIHtcbiAgbWF4LXdpZHRoOiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciBoMiB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBsaW5lLWhlaWdodDogdW5zZXQ7XG4gIGhlaWdodDogdW5zZXQ7XG4gIG1hcmdpbjogMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cblxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIHtcbiAgYmFja2dyb3VuZDogI2YwZjBmMDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtaW4taGVpZ2h0OiAyN3B4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNjY2O1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5pbWctdXBsb2FkIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbjogMjBweCAxMHB4O1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5pbWctdXBsb2FkIC5tZW1iZXItZGV0YWlsLWltYWdlIHtcbiAgd2lkdGg6IDIwMHB4O1xuICBtYXgtaGVpZ2h0OiAyMDBweDtcbiAgb2JqZWN0LWZpdDogY29udGFpbjtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuZm9ybS1ncm91cC51cGxvYWRlZCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBoZWlnaHQ6IDEwMCU7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLmxvYWRpbmcge1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC5sb2FkaW5nIC5zay1mYWRpbmctY2lyY2xlIHtcbiAgdG9wOiA1MCU7XG59XG5cbmJ1dHRvbi5idG5fdXBsb2FkLCBidXR0b24uYWRkLW1vcmUtaW1hZ2UsIGJ1dHRvbi5idG5fZGVsZXRlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcbiAgY29sb3I6ICNmZmY7XG4gIHBhZGRpbmc6IDVweCAxNXB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBib3JkZXI6IDA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuYnV0dG9uLmJ0bl9kZWxldGUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBzYWxtb247XG59XG5cbi5hZGQtbW9yZS1jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICB3aWR0aDogMTAwJTtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5sYWJlbC1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/form-member/additional-document/edit/additional-document.edit.component.ts":
/*!***********************************************************************************************************!*\
  !*** ./src/app/layout/modules/form-member/additional-document/edit/additional-document.edit.component.ts ***!
  \***********************************************************************************************************/
/*! exports provided: EditAdditonalDocumentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditAdditonalDocumentComponent", function() { return EditAdditonalDocumentComponent; });
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





// import { ProductService } from '../../../../services/product/product.service';
var EditAdditonalDocumentComponent = /** @class */ (function () {
    function EditAdditonalDocumentComponent(memberService, productService, zone) {
        this.memberService = memberService;
        this.productService = productService;
        this.zone = zone;
        this.errorLabel = false;
        this.new_form = {};
        this.allBast = [];
        this.allSuratKuasa = [];
        this.allSuratPernyataan = [];
        this.loading = false;
        this.errorFile = false;
        // loadingKTPPemilik: boolean = false;
        // loadingNPWPPemilik: boolean = false;
        // loadingKTPPenerima: boolean = false;
        // loadingNPWPPenerima: boolean = false;
        this.loadingBAST = [];
        this.loadingSuratKuasa = [];
        this.loadingSuratPernyataan = [];
    }
    EditAdditonalDocumentComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    EditAdditonalDocumentComponent.prototype.isArray = function (curVar) {
        return Array.isArray(curVar) ? true : false;
    };
    EditAdditonalDocumentComponent.prototype.assignArrayBoolean = function (curVar, varCont) {
        if (this.isArray(curVar)) {
            curVar.forEach(function (element) {
                varCont.push(false);
            });
        }
    };
    EditAdditonalDocumentComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (!this.detail.additional_info)
                    this.detail.additional_info = { bast: [], surat_kuasa: [], surat_pernyataan: [] };
                if (!this.detail.additional_info.bast)
                    this.detail.additional_info.bast = [];
                if (!this.detail.additional_info.surat_kuasa)
                    this.detail.additional_info.surat_kuasa = [];
                if (!this.detail.additional_info.surat_pernyataan)
                    this.detail.additional_info.surat_pernyataan = [];
                this.updateSingle = JSON.parse(JSON.stringify(this.detail.additional_info));
                this.updateSingle.bast = this.isArray(this.updateSingle.bast) ? this.updateSingle.bast : [];
                this.allBast = this.updateSingle.bast;
                this.updateSingle.surat_kuasa = this.isArray(this.updateSingle.surat_kuasa) ? this.updateSingle.surat_kuasa : [];
                this.allSuratKuasa = this.updateSingle.surat_kuasa;
                this.updateSingle.surat_pernyataan = this.isArray(this.updateSingle.surat_pernyataan) ? this.updateSingle.surat_pernyataan : [];
                this.allSuratPernyataan = this.updateSingle.surat_pernyataan;
                this.assignArrayBoolean(this.allBast, this.loadingBAST);
                this.assignArrayBoolean(this.allSuratKuasa, this.loadingSuratKuasa);
                this.assignArrayBoolean(this.allSuratPernyataan, this.loadingSuratPernyataan);
                return [2 /*return*/];
            });
        });
    };
    EditAdditonalDocumentComponent.prototype.backToDetail = function () {
        this.back[0][this.back[1]]();
    };
    EditAdditonalDocumentComponent.prototype.validURL = function (str) {
        return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(str);
    };
    EditAdditonalDocumentComponent.prototype.checkFileType = function (text) {
        var splitFileName = text.split('.');
        var extensFile = splitFileName[splitFileName.length - 1];
        var extens = extensFile.toLowerCase();
        return extens == 'pdf' ? 'document' : 'image';
    };
    EditAdditonalDocumentComponent.prototype.openUpload = function (type, index) {
        var upFile = document.getElementById(type + index);
        upFile.click();
    };
    EditAdditonalDocumentComponent.prototype.saveThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var arrAddBAST, arrAddSK, arrAddSP, arrDeleteBAST, arrDeleteSK, arrDeleteSP, i, found, j, i, i, found, j, i, i, found, j, i, payloadRemove, payloadAdd, e_1, message;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        arrAddBAST = [];
                        arrAddSK = [];
                        arrAddSP = [];
                        arrDeleteBAST = [];
                        arrDeleteSK = [];
                        arrDeleteSP = [];
                        //validate BAST
                        for (i = 0; i < this.detail.additional_info.bast.length; i++) {
                            found = false;
                            for (j = 0; j < this.updateSingle.bast.length; j++) {
                                if (this.updateSingle.bast[j].id == this.detail.additional_info.bast[i].id) {
                                    found = true;
                                    break;
                                }
                            }
                            if (!found) {
                                arrDeleteBAST.push(this.detail.additional_info.bast[i].id);
                            }
                        }
                        for (i = 0; i < this.updateSingle.bast.length; i++) {
                            if (this.updateSingle.bast[i].id == null && this.updateSingle.bast[i].pic_file_name) {
                                arrAddBAST.push(this.updateSingle.bast[i].pic_file_name);
                            }
                        }
                        // Validate Surat Kuasa
                        for (i = 0; i < this.detail.additional_info.surat_kuasa.length; i++) {
                            found = false;
                            for (j = 0; j < this.updateSingle.surat_kuasa.length; j++) {
                                if (this.updateSingle.surat_kuasa[j].id == this.detail.additional_info.surat_kuasa[i].id) {
                                    found = true;
                                    break;
                                }
                            }
                            if (!found) {
                                arrDeleteSK.push(this.detail.additional_info.surat_kuasa[i].id);
                            }
                        }
                        for (i = 0; i < this.updateSingle.surat_kuasa.length; i++) {
                            if (this.updateSingle.surat_kuasa[i].id == null && this.updateSingle.surat_kuasa[i].pic_file_name) {
                                arrAddSK.push(this.updateSingle.surat_kuasa[i].pic_file_name);
                            }
                        }
                        // Validate Surat Pernyataan
                        for (i = 0; i < this.detail.additional_info.surat_pernyataan.length; i++) {
                            found = false;
                            for (j = 0; j < this.updateSingle.surat_pernyataan.length; j++) {
                                if (this.updateSingle.surat_pernyataan[j].id == this.detail.additional_info.surat_pernyataan[i].id) {
                                    found = true;
                                    break;
                                }
                            }
                            if (!found) {
                                arrDeleteSP.push(this.detail.additional_info.surat_pernyataan[i].id);
                            }
                        }
                        for (i = 0; i < this.updateSingle.surat_pernyataan.length; i++) {
                            if (this.updateSingle.surat_pernyataan[i].id == null && this.updateSingle.surat_pernyataan[i].pic_file_name) {
                                arrAddSP.push(this.updateSingle.surat_pernyataan[i].pic_file_name);
                            }
                        }
                        payloadRemove = {
                            username: this.detail.username,
                            bast: arrDeleteBAST,
                            surat_kuasa: arrDeleteSK,
                            surat_pernyataan: arrDeleteSP
                        };
                        payloadAdd = {
                            username: this.detail.username,
                            bast: arrAddBAST,
                            surat_kuasa: arrAddSK,
                            surat_pernyataan: arrAddSP
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 6, , 7]);
                        if (!(payloadAdd.bast.length > 0 || payloadAdd.surat_kuasa.length > 0 || payloadAdd.surat_pernyataan.length > 0)) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.memberService.addBastSk(payloadAdd)];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        if (!(payloadRemove.bast.length > 0 || payloadRemove.surat_kuasa.length > 0 || payloadRemove.surat_pernyataan.length > 0)) return [3 /*break*/, 5];
                        return [4 /*yield*/, this.memberService.deleteBastSk(payloadRemove)];
                    case 4:
                        _a.sent();
                        _a.label = 5;
                    case 5:
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                            title: 'Success',
                            text: 'Edit data pelanggan berhasil',
                            icon: 'success',
                            confirmButtonText: 'Ok',
                        }).then(function (result) {
                            if (result.isConfirmed) {
                                _this.backToDetail();
                            }
                        });
                        return [3 /*break*/, 7];
                    case 6:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 7];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    // async oldsaveThis() {
    //   this.new_form = {
    //     username: this.detail.username
    //   }
    //   if(this.updateSingle.bast && this.updateSingle.bast.length > 0){
    //     let currentBast = this.updateSingle.bast.filter((element : any) => {
    //       if(element.url && this.validURL(element.url)){
    //         return element;
    //       }
    //     });
    //     this.updateSingle.bast = currentBast;
    //   }
    //   if(this.updateSingle.surat_kuasa && this.updateSingle.surat_kuasa.length > 0){
    //     let currentSuratKuasa = this.updateSingle.surat_kuasa.filter((element:any) => {
    //       if(element.url && this.validURL(element.url)){
    //         return element;
    //       }
    //     });
    //     this.updateSingle.surat_kuasa = currentSuratKuasa;
    //   }
    //   for(let prop in this.updateSingle){
    //     if(this.updateSingle[prop] != this.detail[prop] && this.updateSingle[prop] != ""){
    //         this.new_form[prop] = this.updateSingle[prop];
    //     }
    //     if(prop == 'bast' || prop == 'surat_kuasa'){
    //       this.new_form[prop] = this.updateSingle[prop];
    //     }
    //   }
    //   console.warn("new form", this.new_form);
    //   console.warn("updateSingle", this.updateSingle);
    //   console.warn("detail", this.detail);
    //   try {
    //     await this.memberService.updateBASTandSK(new_form);
    //     this.detail = JSON.parse(JSON.stringify(this.updateSingle));
    //     //  Swal.fire('Success', 'Edit data pelanggan berhasil', 'success');
    //     Swal.fire({
    //       title: 'Success',
    //       text: 'Edit data pelanggan berhasil',
    //       icon: 'success',
    //       confirmButtonText: 'Ok',
    //     }).then((result) => {
    //       if(result.isConfirmed){
    //         this.backToDetail();
    //       }
    //     });
    //   } catch (e) {
    //     this.errorLabel = ((<Error>e).message);//conversion to Error type
    //     alert("Error");
    //   }
    // }
    // async onFileSelected2(event, img) {
    // 	var reader = new FileReader()
    // 	try {
    // 		this.errorFile = false;
    // 		let fileMaxSize = 3000000; // let say 3Mb
    // 		Array.from(event.target.files).forEach((file: any) => {
    // 			if (file.size > fileMaxSize) {
    // 				this.errorFile = 'Maximum File Upload is 3MB';
    // 			}
    // 		});
    // 		this.selectedFile = event.target.files[0];
    // 		reader.onload = (event: any) => {
    // 			this.selFile = event.target.result;
    // 		}
    // 		if(event.target.files[0]){
    //       reader.readAsDataURL(event.target.files[0]);
    //     }
    // 		if (this.selectedFile) {
    //       let valueImage = this.checkLoading(img, true);
    // 			const logo = await this.productService.upload(this.selectedFile, this, 'image', (result) => {
    //         this.updateSingle[valueImage] = result.base_url + result.pic_big_path;
    //         this.checkLoading(img, false);
    // 				// console.log("updateSingle", this.updateSingle);
    // 				// this.callImage(result, img);
    // 			});
    // 		}
    // 	} catch (e) {
    // 		this.errorLabel = (<Error>e).message; //conversion to Error type
    // 	}
    // }
    EditAdditonalDocumentComponent.prototype.onFileSelected = function (event, typeImg, idx) {
        return __awaiter(this, void 0, void 0, function () {
            var reader, programName, fileExtens, fileMaxSize_1, splitFileName, extensFile, extens, typeFile, newFileName, e_2, message;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (typeImg == 'bast') {
                            this.loadingBAST[idx] = true;
                        }
                        else if (typeImg == 'suratKuasa') {
                            this.loadingSuratKuasa[idx] = true;
                        }
                        else if (typeImg == 'suratPernyataan') {
                            this.loadingSuratPernyataan[idx] = true;
                        }
                        reader = new FileReader();
                        programName = localStorage.getItem('programName');
                        fileExtens = ['jpg', 'jpeg', 'png', 'gif', 'pdf'];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        this.errorFile = false;
                        fileMaxSize_1 = 2064000;
                        Array.from(event.target.files).forEach(function (file) {
                            if (file.size > fileMaxSize_1) {
                                _this.errorFile = 'Maximum File Upload is 2MB';
                            }
                            var splitFileName = file.name.split('.');
                            if (splitFileName.length < 1) {
                                _this.errorFile = "file extention is required";
                            }
                            else {
                                var extensFile = splitFileName[splitFileName.length - 1];
                                var extens = extensFile.toLowerCase();
                                if (!fileExtens.includes(extens))
                                    _this.errorFile = "extension " + extens + " is not Allowed";
                            }
                        });
                        this.selectedFile = event.target.files[0];
                        reader.onload = function (event) {
                            _this.selFile = event.target.result;
                        };
                        if (event.target.files[0]) {
                            reader.readAsDataURL(event.target.files[0]);
                        }
                        if (!this.selectedFile) return [3 /*break*/, 3];
                        splitFileName = this.selectedFile.name.split('.');
                        extensFile = splitFileName[splitFileName.length - 1];
                        extens = extensFile.toLowerCase();
                        typeFile = extens == 'pdf' ? 'document' : 'image';
                        newFileName = programName + '-' + this.detail.username + '-' + typeImg + '.' + extens;
                        return [4 /*yield*/, this.productService.upload(this.selectedFile, this, typeFile, function (result) {
                                var dateNow = Date.now();
                                var dataImage = {
                                    pic_file_name: typeof result == 'object' && result.base_url ? result.base_url + result.pic_big_path : result,
                                    updated_date: dateNow,
                                    created_date: dateNow,
                                    id: null
                                };
                                if (typeImg == 'bast') {
                                    _this.updateSingle.bast[idx] = __assign({}, _this.updateSingle.bast[idx], dataImage);
                                    _this.loadingBAST[idx] = false;
                                }
                                else if (typeImg == 'suratKuasa') {
                                    _this.updateSingle.surat_kuasa[idx] = __assign({}, _this.updateSingle.surat_kuasa[idx], dataImage);
                                    _this.loadingSuratKuasa[idx] = false;
                                }
                                else if (typeImg == 'suratPernyataan') {
                                    _this.updateSingle.surat_pernyataan[idx] = __assign({}, _this.updateSingle.surat_pernyataan[idx], dataImage);
                                    _this.loadingSuratPernyataan[idx] = false;
                                }
                            }, newFileName)];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3: return [3 /*break*/, 5];
                    case 4:
                        e_2 = _a.sent();
                        this.errorLabel = e_2.message; //conversion to Error type
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    EditAdditonalDocumentComponent.prototype.addMoreUpload = function (typeImg) {
        var dummyImg = {
            created_by: null,
            created_date: null,
            id: null,
            pic_file_name: null,
            updated_by: null,
            updated_date: null
        };
        if (typeImg == 'bast') {
            this.updateSingle.bast.push(dummyImg);
            this.loadingBAST.push(false);
        }
        else if (typeImg == 'suratKuasa') {
            this.updateSingle.surat_kuasa.push(dummyImg);
            this.loadingSuratKuasa.push(false);
        }
        else if (typeImg == 'suratPernyataan') {
            this.updateSingle.surat_pernyataan.push(dummyImg);
            this.loadingSuratPernyataan.push(false);
        }
    };
    EditAdditonalDocumentComponent.prototype.deleteImage = function (type, idx, url) {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: "Delete",
            text: "Are you sure want to delete this ?",
            imageUrl: url,
            imageHeight: 250,
            imageAlt: type,
            showCancelButton: true,
            focusConfirm: false,
        }).then(function (res) {
            if (res.isConfirmed) {
                if (type == "bast") {
                    _this.updateSingle.bast.splice(idx, 1);
                    _this.loadingBAST.splice(idx, 1);
                    // console.log("BAST DELETE", this.updateSingle.bast,this.allBast);
                }
                else if (type = "suratKuasa") {
                    _this.updateSingle.surat_kuasa.splice(idx, 1);
                    _this.loadingSuratKuasa.splice(idx, 1);
                    // console.log("SURAT KUASA DELETE", this.updateSingle.surat_kuasa,this.allSuratKuasa);
                }
                else if (type = "suratPernyataan") {
                    _this.updateSingle.surat_pernyataan.splice(idx, 1);
                    _this.loadingSuratPernyataan.splice(idx, 1);
                    // console.log("SURAT PERNYATAAN DELETE", this.updateSingle.surat_pernyataan,this.allSuratPernyataan);
                }
            }
        });
    };
    EditAdditonalDocumentComponent.prototype.convertDate = function (time) {
        var date = new Date(time);
        var dateString = date.toString();
        var retDate = dateString.split("GMT");
        return retDate[0];
    };
    EditAdditonalDocumentComponent.prototype.checkLoading = function (fieldName, fieldValue) {
        var valueImage;
        switch (fieldName) {
            case 'bast':
                valueImage = 'bast';
                this.loadingBAST = fieldValue;
                break;
            case 'suratKuasa':
                valueImage = 'surat_kuasa';
                this.loadingSuratKuasa = fieldValue;
            case 'suratPernyataan':
                valueImage = 'surat_pernyataan';
                this.loadingSuratPernyataan = fieldValue;
        }
        return valueImage;
    };
    EditAdditonalDocumentComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"] },
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__["ProductService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], EditAdditonalDocumentComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], EditAdditonalDocumentComponent.prototype, "back", void 0);
    EditAdditonalDocumentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-additional-document-edit',
            template: __webpack_require__(/*! raw-loader!./additional-document.edit.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/form-member/additional-document/edit/additional-document.edit.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_2__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./additional-document.edit.component.scss */ "./src/app/layout/modules/form-member/additional-document/edit/additional-document.edit.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"], _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__["ProductService"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]])
    ], EditAdditonalDocumentComponent);
    return EditAdditonalDocumentComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/form-member/detail/form-member.detail.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/layout/modules/form-member/detail/form-member.detail.component.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg-delete {\n  background-color: #ffe3f3;\n  padding: 10px;\n}\n\n.member-detail-image-container {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n\n.member-detail-image-container a {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.member-detail-image-container a .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.member-detail-image-container .btn-download-container {\n  margin-top: 5px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\nbutton.btn-download-img {\n  margin-top: 10px;\n  background-color: #3498db;\n  color: #fff;\n  padding: 5px 15px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 4px;\n  border: 0;\n  cursor: pointer;\n}\n\n.card-detail > .card-header {\n  position: relative;\n}\n\n.card-detail > .card-header .back_button {\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .edit_button {\n  position: absolute;\n  top: 7px;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n\n.card-detail > .card-header .edit_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .btn-right {\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-left: 10px;\n}\n\n.card-detail > .card-header .btn-right-redeem {\n  background-color: #3498db;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-left: 10px;\n}\n\n.card-detail .card-content {\n  padding: 15px;\n}\n\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail form-select[name=status] .form-group {\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n\n.error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 26px;\n  padding-top: 10px;\n}\n\n.error-message .text-message {\n  background: #E74C3C;\n  padding: 20px;\n  margin: 0 10px;\n}\n\n.red-alert {\n  color: red;\n  font-size: 10px;\n  position: relative;\n  bottom: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZm9ybS1tZW1iZXIvZGV0YWlsL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcZm9ybS1tZW1iZXJcXGRldGFpbFxcZm9ybS1tZW1iZXIuZGV0YWlsLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9mb3JtLW1lbWJlci9kZXRhaWwvZm9ybS1tZW1iZXIuZGV0YWlsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQUE7RUFDQSxhQUFBO0FDQ0o7O0FEQ0E7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDRUo7O0FEREk7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQ0dSOztBREZRO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7S0FBQSxtQkFBQTtBQ0laOztBRERJO0VBQ0ksZUFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FDR1I7O0FERUE7RUFDSSxnQkFBQTtFQUNBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxlQUFBO0FDQ0o7O0FER0k7RUFDSSxrQkFBQTtBQ0FSOztBRENRO0VBRUksZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLDZCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSx5QkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7QUNEWjs7QURFWTtFQUNJLGlCQUFBO0FDQWhCOztBREdRO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBRUEsV0FBQTtFQUNBLFlBQUE7RUFDQSwwQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBSUEsV0FBQTtBQ0xaOztBREVZO0VBQ0ksaUJBQUE7QUNBaEI7O0FESVE7RUFDSSxzQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDRlo7O0FESVE7RUFDSSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDRlo7O0FES0k7RUFDSSxhQUFBO0FDSFI7O0FESVE7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0FDRlo7O0FEaUJJO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNmUjs7QURrQlE7RUFDSSxrQkFBQTtBQ2hCWjs7QURrQlE7RUFDSSxnQkFBQTtBQ2hCWjs7QURrQlE7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDaEJaOztBRG1CUTtFQUNJLHNCQUFBO0FDakJaOztBRGtCWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDaEJoQjs7QUR1QkE7RUFDSSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ3BCSjs7QURzQkk7RUFDSSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0FDcEJSOztBRHdCQTtFQUNJLFVBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FDckJKIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZm9ybS1tZW1iZXIvZGV0YWlsL2Zvcm0tbWVtYmVyLmRldGFpbC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5iZy1kZWxldGV7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlM2YzO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxufVxyXG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIge1xyXG4gICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgIGZvbnQtd2VpZ2h0OiBub3JtYTtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBjb2xvcjogIzY2NjtcclxuICAgIGEge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAubWVtYmVyLWRldGFpbC1pbWFnZSB7XHJcbiAgICAgICAgICAgIHdpZHRoOjIwMHB4O1xyXG4gICAgICAgICAgICBtYXgtaGVpZ2h0OiAyMDBweDtcclxuICAgICAgICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuYnRuLWRvd25sb2FkLWNvbnRhaW5lciB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDo1cHg7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuYnV0dG9uLmJ0bi1kb3dubG9hZC1pbWcge1xyXG4gICAgbWFyZ2luLXRvcDoxMHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgcGFkZGluZzogNXB4IDE1cHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gICAgYm9yZGVyOjA7XHJcbiAgICBjdXJzb3I6cG9pbnRlcjtcclxufVxyXG5cclxuLmNhcmQtZGV0YWlse1xyXG4gICAgPi5jYXJkLWhlYWRlcntcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgLmJhY2tfYnV0dG9ue1xyXG4gICAgICAgICAgICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC00cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtMTBweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5lZGl0X2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDdweDtcclxuICAgICAgICAgICAgLy8gYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5idG4tcmlnaHQge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuYnRuLXJpZ2h0LXJlZGVlbSB7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmNhcmQtY29udGVudHtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgID4uY29sLW1kLTEye1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC8vIC5pbWFnZXtcclxuICAgIC8vICAgICBoM3tcclxuICAgIC8vICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgLy8gICAgIH1cclxuICAgIC8vICAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgLy8gICAgICAgICBpbWd7XHJcbiAgICAvLyAgICAgICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICAvLyAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuXHJcbiAgICAvLyAgICAgICAgIH1cclxuICAgIC8vICAgICB9XHJcbiAgICAvLyB9XHJcbiAgICBoMXtcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgIH1cclxuICAgIC5tZW1iZXItZGV0YWlse1xyXG4gICAgICAgIGZvcm0tc2VsZWN0W25hbWU9c3RhdHVzXSAuZm9ybS1ncm91cCB7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWx7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxhYmVsK2RpdntcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6bm9ybWE7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgY29sb3I6ICM2NjY7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIC5jYXJkLWhlYWRlcnsgXHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGgye1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi5lcnJvci1tZXNzYWdlIHtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgaGVpZ2h0OjEwMHZoO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOjI2cHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcclxuXHJcbiAgICAudGV4dC1tZXNzYWdlIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjRTc0QzNDO1xyXG4gICAgICAgIHBhZGRpbmc6IDIwcHg7XHJcbiAgICAgICAgbWFyZ2luOiAwIDEwcHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5yZWQtYWxlcnQge1xyXG4gICAgY29sb3I6IHJlZDtcclxuICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGJvdHRvbTogMTBweDtcclxufSIsIi5iZy1kZWxldGUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlM2YzO1xuICBwYWRkaW5nOiAxMHB4O1xufVxuXG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWE7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgYSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIGEgLm1lbWJlci1kZXRhaWwtaW1hZ2Uge1xuICB3aWR0aDogMjAwcHg7XG4gIG1heC1oZWlnaHQ6IDIwMHB4O1xuICBvYmplY3QtZml0OiBjb250YWluO1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5idG4tZG93bmxvYWQtY29udGFpbmVyIHtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cblxuYnV0dG9uLmJ0bi1kb3dubG9hZC1pbWcge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogI2ZmZjtcbiAgcGFkZGluZzogNXB4IDE1cHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIGJvcmRlcjogMDtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIHtcbiAgbWFyZ2luLXRvcDogLTRweDtcbiAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgY29sb3I6ICM1NTU7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuZWRpdF9idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogN3B4O1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHJpZ2h0OiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5lZGl0X2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJ0bi1yaWdodCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG4gIGNvbG9yOiAjZmZmO1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJ0bi1yaWdodC1yZWRlZW0ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQge1xuICBwYWRkaW5nOiAxNXB4O1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQgPiAuY29sLW1kLTEyIHtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDBweDtcbn1cbi5jYXJkLWRldGFpbCBoMSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjNTU1O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBmb3JtLXNlbGVjdFtuYW1lPXN0YXR1c10gLmZvcm0tZ3JvdXAge1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGxhYmVsICsgZGl2IHtcbiAgYmFja2dyb3VuZDogI2YwZjBmMDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtaW4taGVpZ2h0OiAyN3B4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNjY2O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIGgyIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGxpbmUtaGVpZ2h0OiB1bnNldDtcbiAgaGVpZ2h0OiB1bnNldDtcbiAgbWFyZ2luOiAwcHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuXG4uZXJyb3ItbWVzc2FnZSB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBoZWlnaHQ6IDEwMHZoO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMjZweDtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59XG4uZXJyb3ItbWVzc2FnZSAudGV4dC1tZXNzYWdlIHtcbiAgYmFja2dyb3VuZDogI0U3NEMzQztcbiAgcGFkZGluZzogMjBweDtcbiAgbWFyZ2luOiAwIDEwcHg7XG59XG5cbi5yZWQtYWxlcnQge1xuICBjb2xvcjogcmVkO1xuICBmb250LXNpemU6IDEwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYm90dG9tOiAxMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/form-member/detail/form-member.detail.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/layout/modules/form-member/detail/form-member.detail.component.ts ***!
  \***********************************************************************************/
/*! exports provided: FormMemberDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormMemberDetailComponent", function() { return FormMemberDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_6___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var FormMemberDetailComponent = /** @class */ (function () {
    function FormMemberDetailComponent(memberService, router, route) {
        this.memberService = memberService;
        this.router = router;
        this.route = route;
        this.contentList = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_6__;
        this.mci_project = false;
        this.edit = false;
        this.isLoginRedeem = false;
        this.errorLabel = false;
        this.errorMessage = false;
        this.isAddDocument = false;
        this.isNoProcess = false;
        this.alertAddress = false;
        this.alertKelurahan = false;
        this.alertKecamatan = false;
        this.alertKota = false;
        this.alertProvinsi = false;
        this.alertKodePos = false;
        this.programType = "";
        this.sig_kontraktual = false;
        this.isValidForRedeem = true;
    }
    FormMemberDetailComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    FormMemberDetailComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var program, _this, id_pel, result, e_1;
            var _this_1 = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.alertAddress = false;
                        this.alertKelurahan = false;
                        this.alertKecamatan = false;
                        this.alertKota = false;
                        this.alertProvinsi = false;
                        this.alertKodePos = false;
                        this.isValidForRedeem = true;
                        program = localStorage.getItem('programName');
                        _this = this;
                        this.contentList.filter(function (element) {
                            if (element.appLabel == program) {
                                if (element.type == "reguler") {
                                    _this.mci_project = true;
                                    _this.programType = "reguler";
                                }
                                else if (element.type == "custom_kontraktual") {
                                    _this.programType = "custom_kontraktual";
                                    _this.sig_kontraktual = true;
                                }
                                else {
                                    _this.programType = "custom";
                                    _this.mci_project = false;
                                }
                            }
                        });
                        id_pel = this.detail.username;
                        this.memberDetail = this.detail;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.memberService.getFormMemberByID(id_pel)];
                    case 2:
                        result = _a.sent();
                        if (result.length > 0) {
                            result.forEach(function (element) {
                                if (element.username == _this_1.detail.username) {
                                    _this_1.memberDetail = element;
                                }
                            });
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 4];
                    case 4:
                        if (this.memberDetail && this.memberDetail.input_form_data) {
                            if (!this.memberDetail.input_form_data.alamat_rumah || this.memberDetail.input_form_data.alamat_rumah == "-") {
                                this.alertAddress = true;
                                this.isValidForRedeem = false;
                            }
                            if (!this.mci_project && (!this.memberDetail.input_form_data.kelurahan_rumah || this.memberDetail.input_form_data.kelurahan_rumah == "-")) {
                                this.alertKelurahan = true;
                                this.isValidForRedeem = false;
                            }
                            if (!this.mci_project && (!this.memberDetail.input_form_data.kecamatan_rumah || this.memberDetail.input_form_data.kecamatan_rumah == "-")) {
                                this.alertKecamatan = true;
                                this.isValidForRedeem = false;
                            }
                            if (!this.mci_project && (!this.memberDetail.input_form_data.kota_rumah || this.memberDetail.input_form_data.kota_rumah == "-")) {
                                this.alertKota = true;
                                this.isValidForRedeem = false;
                            }
                            if (!this.mci_project && (!this.memberDetail.input_form_data.provinsi_rumah || this.memberDetail.input_form_data.provinsi_rumah == "-")) {
                                this.alertProvinsi = true;
                                this.isValidForRedeem = false;
                            }
                            if (!this.mci_project && (!this.memberDetail.input_form_data.kode_pos_rumah || this.memberDetail.input_form_data.kode_pos_rumah == "-")) {
                                this.alertKodePos = true;
                                this.isValidForRedeem = false;
                            }
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    FormMemberDetailComponent.prototype.addDocument = function () {
        this.isAddDocument = !this.isAddDocument;
        if (this.isAddDocument == false) {
            this.firstLoad();
        }
    };
    FormMemberDetailComponent.prototype.checkFileType = function (text) {
        var splitFileName = text.split('.');
        var extensFile = splitFileName[splitFileName.length - 1];
        var extens = extensFile.toLowerCase();
        return extens == 'pdf' ? 'document' : 'image';
    };
    FormMemberDetailComponent.prototype.noProcess = function () {
        this.isNoProcess = !this.isNoProcess;
        if (this.isNoProcess == false) {
            this.firstLoad();
        }
    };
    FormMemberDetailComponent.prototype.downloadImage = function (url) {
        var filename;
        filename = this.lastPathURL(url);
        fetch(url).then(function (t) {
            return t.blob().then(function (b) {
                var a = document.createElement("a");
                a.href = URL.createObjectURL(b);
                a.setAttribute("download", filename);
                a.click();
            });
        });
    };
    FormMemberDetailComponent.prototype.isArray = function (curVar) {
        return Array.isArray(curVar) ? true : false;
    };
    FormMemberDetailComponent.prototype.convertDate = function (time) {
        var date = new Date(time);
        var dateString = date.toString();
        var retDate = dateString.split("GMT");
        return retDate[0];
    };
    FormMemberDetailComponent.prototype.lastPathURL = function (url) {
        var resURL = url.split("/");
        console.log("resURL", resURL);
        var lastNumber = resURL.length - 1;
        return resURL[lastNumber];
    };
    FormMemberDetailComponent.prototype.editThis = function () {
        // console.log(this.edit );
        this.edit = !this.edit;
        if (this.edit == false) {
            this.firstLoad();
        }
        // console.log(this.edit );
    };
    // redeemLogin() {
    //   // console.log(this.edit );
    //   this.isLoginRedeem = !this.isLoginRedeem;
    //   if(this.isLoginRedeem == false){
    //     this.firstLoad();
    //   }
    //   // console.log(this.edit );
    // }
    FormMemberDetailComponent.prototype.redeemLogin = function () {
        return __awaiter(this, void 0, void 0, function () {
            var updatePayload, payload, result, webLink, appLabel, e_2, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        if (!(this.mci_project == true)) return [3 /*break*/, 2];
                        updatePayload = JSON.parse(JSON.stringify(this.memberDetail.input_form_data));
                        if (!(updatePayload.nama_penerima_gopay != updatePayload.nama_pemilik ||
                            updatePayload.no_wa_pemilik != updatePayload.telp_pemilik ||
                            updatePayload.no_wa_penerima != updatePayload.telp_pemilik)) return [3 /*break*/, 2];
                        updatePayload.nama_penerima_gopay = updatePayload.nama_pemilik;
                        updatePayload.no_wa_pemilik = updatePayload.telp_pemilik;
                        updatePayload.no_wa_penerima = updatePayload.telp_pemilik;
                        return [4 /*yield*/, this.memberService.updateSingleFormMember(updatePayload)];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        payload = {
                            username: this.memberDetail.username
                        };
                        console.log("payload", payload);
                        return [4 /*yield*/, this.memberService.getTokenMember(payload)];
                    case 3:
                        result = _a.sent();
                        if (result && result.token) {
                            webLink = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["getLink"])();
                            appLabel = localStorage.getItem('programName');
                            window.open(webLink + "?app=" + appLabel + "&param=" + encodeURIComponent(result.token), '_blank');
                        }
                        return [3 /*break*/, 5];
                    case 4:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    FormMemberDetailComponent.prototype.backToTable = function () {
        console.warn("back member detail", this.back);
        this.back[1](this.back[0]);
    };
    FormMemberDetailComponent.prototype.validURL = function (str) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return !!pattern.test(str);
    };
    FormMemberDetailComponent.prototype.delete = function () {
        return __awaiter(this, void 0, void 0, function () {
            var delResult, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.memberService.deleteMember(this.detail)];
                    case 1:
                        delResult = _a.sent();
                        // console.log(delResult);
                        if (delResult.error == false) {
                            console.log(this.back[0]);
                            this.back[0].memberDetail = false;
                            this.back[0].firstLoad();
                            // delete this.back[0].prodDetail;
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        throw new TypeError(error_1.error.error);
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    FormMemberDetailComponent.prototype.catch = function (e) {
        this.errorLabel = (e.message); //conversion to Error type
    };
    FormMemberDetailComponent.prototype.pointHistory = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.log('here');
                // this.router.navigate(['/member-point-history']);
                this.router.navigate(['administrator/memberadmin/point-history'], { queryParams: { id: this.detail.email } });
                return [2 /*return*/];
            });
        });
    };
    FormMemberDetailComponent.prototype.setCompleteMember = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this_1 = this;
            return __generator(this, function (_a) {
                if (this.detail && this.detail.username) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                        title: 'Confirmation',
                        text: 'Apakah anda yakin ingin memproses complete member ini?',
                        // icon: 'success',
                        confirmButtonText: 'Process',
                        cancelButtonText: "Cancel",
                        showCancelButton: true
                    }).then(function (result) { return __awaiter(_this_1, void 0, void 0, function () {
                        var payload, e_3;
                        var _this_1 = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!result.isConfirmed) return [3 /*break*/, 5];
                                    _a.label = 1;
                                case 1:
                                    _a.trys.push([1, 3, , 4]);
                                    payload = {
                                        "member_id": [
                                            this.detail.username
                                        ],
                                        "status": "ya"
                                    };
                                    return [4 /*yield*/, this.memberService.setCompleteMember(payload)];
                                case 2:
                                    _a.sent();
                                    return [3 /*break*/, 4];
                                case 3:
                                    e_3 = _a.sent();
                                    console.log(e_3);
                                    return [3 /*break*/, 4];
                                case 4:
                                    // console.log("member ids", memberIds);
                                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                                        title: 'Success',
                                        text: 'Member telah complete',
                                        icon: 'success',
                                        confirmButtonText: 'Ok',
                                        showCancelButton: false
                                    }).then(function (result) { return __awaiter(_this_1, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            if (result.isConfirmed) {
                                                this.firstLoad();
                                            }
                                            return [2 /*return*/];
                                        });
                                    }); });
                                    _a.label = 5;
                                case 5: return [2 /*return*/];
                            }
                        });
                    }); });
                }
                return [2 /*return*/];
            });
        });
    };
    FormMemberDetailComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FormMemberDetailComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FormMemberDetailComponent.prototype, "back", void 0);
    FormMemberDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-form-member-detail',
            template: __webpack_require__(/*! raw-loader!./form-member.detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/form-member/detail/form-member.detail.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./form-member.detail.component.scss */ "./src/app/layout/modules/form-member/detail/form-member.detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])
    ], FormMemberDetailComponent);
    return FormMemberDetailComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/form-member/edit/form-member.edit.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/layout/modules/form-member/edit/form-member.edit.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n}\n.card-detail > .card-header .back_button {\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 7px;\n  background-color: #3498db;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n.card-detail > .card-header .save_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button.true {\n  background-color: #e67e22;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .member-detail .image {\n  overflow: hidden;\n}\n.card-detail .member-detail .image > div {\n  width: 33.3333%;\n  padding: 5px;\n  float: left;\n}\n.card-detail .member-detail .image .card-header {\n  background-color: #f0f0f0;\n}\n.card-detail .member-detail .image h3 {\n  font-size: 12px;\n  margin-bottom: 0px;\n  color: #555;\n}\n.card-detail .member-detail .image .card-content {\n  overflow: hidden;\n  width: 100%;\n  height: auto;\n}\n.card-detail .member-detail .image .card-content img {\n  max-width: 100%;\n  width: 100%;\n}\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n.member-detail-image-container {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.member-detail-image-container .img-upload {\n  display: flex;\n  justify-content: space-around;\n  align-items: center;\n  margin: 20px 10px;\n}\n.member-detail-image-container .img-upload .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.member-detail-image-container .form-group.uploaded {\n  position: relative;\n  margin-bottom: 0px;\n  height: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading {\n  height: 100%;\n  width: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading .sk-fading-circle {\n  top: 50%;\n}\nbutton.btn_upload, button.add-more-image, button.btn_delete {\n  background-color: #3498db;\n  color: #fff;\n  padding: 5px 15px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 4px;\n  border: 0;\n  cursor: pointer;\n}\nbutton.btn_delete {\n  background-color: salmon;\n}\n.add-more-container {\n  display: flex;\n  width: 100%;\n  align-items: center;\n  justify-content: center;\n}\n.label-bold {\n  font-weight: 600;\n}\n::ng-deep .ng-autocomplete {\n  width: 100% !important;\n  padding: 0 !important;\n  border: none !important;\n}\n::ng-deep .ng-autocomplete .autocomplete-container {\n  box-shadow: none !important;\n}\n::ng-deep .ng-autocomplete .autocomplete-container .x {\n  display: none;\n}\n::ng-deep .ng-autocomplete .autocomplete-container input {\n  display: block;\n  padding: 0.375rem 0.75rem !important;\n  font-size: 1rem !important;\n  line-height: 1.5 !important;\n  color: #495057 !important;\n  background-color: #fff !important;\n  background-clip: padding-box;\n  border: 1px solid #ced4da !important;\n  border-radius: 0.25rem;\n  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;\n}\n::ng-deep .ng-autocomplete .autocomplete-container input::-webkit-input-placeholder {\n  color: #878E95 !important;\n}\n::ng-deep .ng-autocomplete .autocomplete-container input::-moz-placeholder {\n  color: #878E95 !important;\n}\n::ng-deep .ng-autocomplete .autocomplete-container input:-ms-input-placeholder {\n  color: #878E95 !important;\n}\n::ng-deep .ng-autocomplete .autocomplete-container input::-ms-input-placeholder {\n  color: #878E95 !important;\n}\n::ng-deep .ng-autocomplete .autocomplete-container input::placeholder {\n  color: #878E95 !important;\n}\n::ng-deep .ng-autocomplete .autocomplete-container input:disabled {\n  background-color: #eee !important;\n  color: #666 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZm9ybS1tZW1iZXIvZWRpdC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXGZvcm0tbWVtYmVyXFxlZGl0XFxmb3JtLW1lbWJlci5lZGl0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9mb3JtLW1lbWJlci9lZGl0L2Zvcm0tbWVtYmVyLmVkaXQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxrQkFBQTtBQ0FSO0FEQ1E7RUFFSSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0RaO0FERVk7RUFDSSxpQkFBQTtBQ0FoQjtBREdRO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0EseUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFLQSxXQUFBO0FDTlo7QURHWTtFQUNJLGlCQUFBO0FDRGhCO0FES1E7RUFDSSx5QkFBQTtBQ0haO0FETUk7RUFDSSxhQUFBO0FDSlI7QURNUTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7QUNKWjtBRFFJO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNOUjtBRFVRO0VBQ0ksZ0JBQUE7QUNSWjtBRFVRO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ1JaO0FEVVE7RUFDSSxnQkFBQTtBQ1JaO0FEU1k7RUFDSSxlQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUNQaEI7QURTWTtFQUNJLHlCQUFBO0FDUGhCO0FEU1k7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FDUGhCO0FEU1k7RUFDSSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FDUGhCO0FEUWdCO0VBQ0ksZUFBQTtFQUNBLFdBQUE7QUNOcEI7QURZUTtFQUNJLHNCQUFBO0FDVlo7QURXWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDVGhCO0FEZ0JBO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ2JKO0FEY0k7RUFDSSxhQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDWlI7QURjUTtFQUNJLFlBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0tBQUEsbUJBQUE7QUNaWjtBRGdCSTtFQUNJLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FDZFI7QURlUTtFQUNJLFlBQUE7RUFFQSxXQUFBO0FDZFo7QURlWTtFQUVJLFFBQUE7QUNkaEI7QURxQkE7RUFDSSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtBQ2xCSjtBRHFCQTtFQUNJLHdCQUFBO0FDbEJKO0FEcUJBO0VBQ0ksYUFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FDbEJKO0FEcUJBO0VBQ0ksZ0JBQUE7QUNsQko7QURxQkE7RUFDSSxzQkFBQTtFQUNBLHFCQUFBO0VBQ0EsdUJBQUE7QUNsQko7QURvQkk7RUFDSSwyQkFBQTtBQ2xCUjtBRG9CUTtFQUNJLGFBQUE7QUNsQlo7QURxQlE7RUFDSSxjQUFBO0VBQ0Esb0NBQUE7RUFDQSwwQkFBQTtFQUNBLDJCQUFBO0VBQ0EseUJBQUE7RUFDQSxpQ0FBQTtFQUNBLDRCQUFBO0VBQ0Esb0NBQUE7RUFDQSxzQkFBQTtFQUNBLHdFQUFBO0FDbkJaO0FEc0JRO0VBQ0kseUJBQUE7QUNwQlo7QURtQlE7RUFDSSx5QkFBQTtBQ3BCWjtBRG1CUTtFQUNJLHlCQUFBO0FDcEJaO0FEbUJRO0VBQ0kseUJBQUE7QUNwQlo7QURtQlE7RUFDSSx5QkFBQTtBQ3BCWjtBRHVCUTtFQUNJLGlDQUFBO0VBQ0Esc0JBQUE7QUNyQloiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9mb3JtLW1lbWJlci9lZGl0L2Zvcm0tbWVtYmVyLmVkaXQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2FyZC1kZXRhaWx7XHJcbiAgICA+LmNhcmQtaGVhZGVye1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAuYmFja19idXR0b257XHJcbiAgICAgICAgICAgIC8vIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogLTRweDtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLnNhdmVfYnV0dG9ue1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIHRvcDogN3B4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgIFxyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zYXZlX2J1dHRvbi50cnVle1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTY3ZTIyO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgICAgIFxyXG4gICAgICAgID4uY29sLW1kLTEye1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIFxyXG4gICAgaDF7XHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICB9XHJcbiAgICAubWVtYmVyLWRldGFpbHtcclxuICAgICAgIFxyXG4gICAgICAgIGxhYmVse1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYWJlbCtkaXZ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmMGYwZjA7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0Om5vcm1hO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgICAgICAgICBtaW4taGVpZ2h0OiAyN3B4O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNjY2O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuaW1hZ2V7XHJcbiAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICAgID5kaXZ7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMzMuMzMzMyU7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICAgICAgICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAuY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGgze1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLmNhcmQtY29udGVudHtcclxuICAgICAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgICAgICAgICAgIGltZ3tcclxuICAgICAgICAgICAgICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICBcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmNhcmQtaGVhZGVyeyBcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaDJ7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIHtcclxuICAgIGJhY2tncm91bmQ6ICNmMGYwZjA7XHJcbiAgICBmb250LXdlaWdodDogbm9ybWE7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICBtaW4taGVpZ2h0OiAyN3B4O1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgY29sb3I6ICM2NjY7XHJcbiAgICAuaW1nLXVwbG9hZCB7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbjoyMHB4IDEwcHg7XHJcblxyXG4gICAgICAgIC5tZW1iZXItZGV0YWlsLWltYWdlIHtcclxuICAgICAgICAgICAgd2lkdGg6MjAwcHg7XHJcbiAgICAgICAgICAgIG1heC1oZWlnaHQ6IDIwMHB4O1xyXG4gICAgICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuZm9ybS1ncm91cC51cGxvYWRlZCB7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgLmxvYWRpbmcge1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgIC8vIGJhY2tncm91bmQtY29sb3I6ICMwMDA7XHJcbiAgICAgICAgICAgIHdpZHRoOjEwMCU7XHJcbiAgICAgICAgICAgIC5zay1mYWRpbmctY2lyY2xlIHtcclxuICAgICAgICAgICAgICAgIC8vIGJhY2tncm91bmQtY29sb3I6cmVkO1xyXG4gICAgICAgICAgICAgICAgdG9wOiA1MCU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5idXR0b24uYnRuX3VwbG9hZCwgYnV0dG9uLmFkZC1tb3JlLWltYWdlLCBidXR0b24uYnRuX2RlbGV0ZSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBwYWRkaW5nOiA1cHggMTVweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgICBib3JkZXI6MDtcclxuICAgIGN1cnNvcjpwb2ludGVyO1xyXG59XHJcblxyXG5idXR0b24uYnRuX2RlbGV0ZXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHNhbG1vbjtcclxufVxyXG5cclxuLmFkZC1tb3JlLWNvbnRhaW5lciB7XHJcbiAgICBkaXNwbGF5OmZsZXg7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgYWxpZ24taXRlbXM6Y2VudGVyO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuXHJcbi5sYWJlbC1ib2xkIHtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubmctYXV0b2NvbXBsZXRlIHtcclxuICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XHJcbiAgICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXI6IG5vbmUgIWltcG9ydGFudDtcclxuXHJcbiAgICAuYXV0b2NvbXBsZXRlLWNvbnRhaW5lciB7XHJcbiAgICAgICAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xyXG5cclxuICAgICAgICAueCB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpbnB1dCB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwLjM3NXJlbSAwLjc1cmVtICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMXJlbSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICBsaW5lLWhlaWdodDogMS41ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNDk1MDU3ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmYgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jbGlwOiBwYWRkaW5nLWJveDtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NlZDRkYSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAwLjI1cmVtO1xyXG4gICAgICAgICAgICB0cmFuc2l0aW9uOiBib3JkZXItY29sb3IgMC4xNXMgZWFzZS1pbi1vdXQsIGJveC1zaGFkb3cgMC4xNXMgZWFzZS1pbi1vdXQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpbnB1dDo6cGxhY2Vob2xkZXIge1xyXG4gICAgICAgICAgICBjb2xvcjogIzg3OEU5NSAhaW1wb3J0YW50O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaW5wdXQ6ZGlzYWJsZWQge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNjY2ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59IiwiLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiB7XG4gIG1hcmdpbi10b3A6IC00cHg7XG4gIG1hcmdpbi1sZWZ0OiAtMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGNvbG9yOiAjNTU1O1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLnNhdmVfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDdweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcbiAgY29sb3I6ICNmZmY7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICByaWdodDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuc2F2ZV9idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbi50cnVlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U2N2UyMjtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogMTVweDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50ID4gLmNvbC1tZC0xMiB7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgaDEge1xuICBmb250LXNpemU6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzU1NTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGxhYmVsICsgZGl2IHtcbiAgYmFja2dyb3VuZDogI2YwZjBmMDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtaW4taGVpZ2h0OiAyN3B4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNjY2O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlID4gZGl2IHtcbiAgd2lkdGg6IDMzLjMzMzMlO1xuICBwYWRkaW5nOiA1cHg7XG4gIGZsb2F0OiBsZWZ0O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSAuY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmMGYwO1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSBoMyB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBjb2xvcjogIzU1NTtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2UgLmNhcmQtY29udGVudCB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IGF1dG87XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlIC5jYXJkLWNvbnRlbnQgaW1nIHtcbiAgbWF4LXdpZHRoOiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciBoMiB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBsaW5lLWhlaWdodDogdW5zZXQ7XG4gIGhlaWdodDogdW5zZXQ7XG4gIG1hcmdpbjogMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cblxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIHtcbiAgYmFja2dyb3VuZDogI2YwZjBmMDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtaW4taGVpZ2h0OiAyN3B4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNjY2O1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5pbWctdXBsb2FkIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbjogMjBweCAxMHB4O1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5pbWctdXBsb2FkIC5tZW1iZXItZGV0YWlsLWltYWdlIHtcbiAgd2lkdGg6IDIwMHB4O1xuICBtYXgtaGVpZ2h0OiAyMDBweDtcbiAgb2JqZWN0LWZpdDogY29udGFpbjtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuZm9ybS1ncm91cC51cGxvYWRlZCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBoZWlnaHQ6IDEwMCU7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLmxvYWRpbmcge1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC5sb2FkaW5nIC5zay1mYWRpbmctY2lyY2xlIHtcbiAgdG9wOiA1MCU7XG59XG5cbmJ1dHRvbi5idG5fdXBsb2FkLCBidXR0b24uYWRkLW1vcmUtaW1hZ2UsIGJ1dHRvbi5idG5fZGVsZXRlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcbiAgY29sb3I6ICNmZmY7XG4gIHBhZGRpbmc6IDVweCAxNXB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBib3JkZXI6IDA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuYnV0dG9uLmJ0bl9kZWxldGUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBzYWxtb247XG59XG5cbi5hZGQtbW9yZS1jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICB3aWR0aDogMTAwJTtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5sYWJlbC1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cblxuOjpuZy1kZWVwIC5uZy1hdXRvY29tcGxldGUge1xuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XG4gIGJvcmRlcjogbm9uZSAhaW1wb3J0YW50O1xufVxuOjpuZy1kZWVwIC5uZy1hdXRvY29tcGxldGUgLmF1dG9jb21wbGV0ZS1jb250YWluZXIge1xuICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XG59XG46Om5nLWRlZXAgLm5nLWF1dG9jb21wbGV0ZSAuYXV0b2NvbXBsZXRlLWNvbnRhaW5lciAueCB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG46Om5nLWRlZXAgLm5nLWF1dG9jb21wbGV0ZSAuYXV0b2NvbXBsZXRlLWNvbnRhaW5lciBpbnB1dCB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwYWRkaW5nOiAwLjM3NXJlbSAwLjc1cmVtICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMXJlbSAhaW1wb3J0YW50O1xuICBsaW5lLWhlaWdodDogMS41ICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjNDk1MDU3ICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmYgIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZC1jbGlwOiBwYWRkaW5nLWJveDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NlZDRkYSAhaW1wb3J0YW50O1xuICBib3JkZXItcmFkaXVzOiAwLjI1cmVtO1xuICB0cmFuc2l0aW9uOiBib3JkZXItY29sb3IgMC4xNXMgZWFzZS1pbi1vdXQsIGJveC1zaGFkb3cgMC4xNXMgZWFzZS1pbi1vdXQ7XG59XG46Om5nLWRlZXAgLm5nLWF1dG9jb21wbGV0ZSAuYXV0b2NvbXBsZXRlLWNvbnRhaW5lciBpbnB1dDo6cGxhY2Vob2xkZXIge1xuICBjb2xvcjogIzg3OEU5NSAhaW1wb3J0YW50O1xufVxuOjpuZy1kZWVwIC5uZy1hdXRvY29tcGxldGUgLmF1dG9jb21wbGV0ZS1jb250YWluZXIgaW5wdXQ6ZGlzYWJsZWQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjNjY2ICFpbXBvcnRhbnQ7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/form-member/edit/form-member.edit.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/layout/modules/form-member/edit/form-member.edit.component.ts ***!
  \*******************************************************************************/
/*! exports provided: FormMemberEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormMemberEditComponent", function() { return FormMemberEditComponent; });
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_5___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





// import { ProductService } from '../../../../services/product/product.service';

var FormMemberEditComponent = /** @class */ (function () {
    function FormMemberEditComponent(memberService, productService, zone) {
        this.memberService = memberService;
        this.productService = productService;
        this.zone = zone;
        this.contentList = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_5__;
        this.errorLabel = false;
        // mci_project: any = false;
        this.sig_kontraktual = false;
        this.programType = "";
        this.allBast = [];
        this.allSuratKuasa = [];
        this.optionsKuasa1 = [
            { label: "KUASA", value: "true" },
            { label: "TIDAK KUASA", value: "false" }
        ];
        this.optionsKuasa2 = [
            { label: "KUASA", value: "KUASA" },
            { label: "TIDAK KUASA", value: "TIDAK KUASA" }
        ];
        this.optionsKuasa3 = [
            { label: "KUASA", value: "ya" },
            { label: "TIDAK KUASA", value: "tidak" }
        ];
        this.optionsGroup = [
            { label: "nonupfront", value: "nonupfront" },
            { label: "upfront", value: "upfront" }
        ];
        this.loading = false;
        this.activationStatus = [
            { label: 'ACTIVATED', value: 'ACTIVATED' },
            { label: 'INACTIVED', value: 'INACTIVED' }
        ];
        this.memberStatus = [
            { label: 'ACTIVE', value: 'ACTIVE' },
            { label: 'INACTIVE', value: 'INACTIVE' }
        ];
        this.errorFile = false;
        this.loadingKTPPemilik = false;
        this.loadingNPWPPemilik = false;
        this.loadingKTPPenerima = false;
        this.loadingNPWPPenerima = false;
        this.loadingBAST = [];
        this.loadingSuratKuasa = [];
        this.keywordProvince = 'province';
        this.keywordCity = 'city';
        this.keywordSubDistrict = 'subdistrict';
        this.keywordVillage = 'village';
        this.dataProvince = [];
        this.dataCity = [];
        this.dataSubDistrict = [];
        this.dataVillage = [];
        this.loadingProvince = false;
        this.loadingCity = false;
        this.loadingSubDistrict = false;
        this.loadingVillage = false;
        this.placeholderProvince = 'Provinsi';
        this.placeholderCity = 'Kota';
        this.placeholderSubDistrict = 'Kecamatan';
        this.placeholderVillage = 'Kelurahan';
    }
    FormMemberEditComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    FormMemberEditComponent.prototype.checkValueKuasa = function () {
        var _this_1 = this;
        if (this.updateSingle["hadiah_dikuasakan"] == "true" || this.updateSingle["hadiah_dikuasakan"] == "false") {
            var options = this.optionsKuasa1.map(function (el) {
                if (el.value == _this_1.updateSingle["hadiah_dikuasakan"]) {
                    el.selected = 1;
                }
                return el;
            });
            this.optionsKuasa1 = options;
        }
        if (this.updateSingle["hadiah_dikuasakan"] == "KUASA" || this.updateSingle["hadiah_dikuasakan"] == "TIDAK KUASA") {
            var options = this.optionsKuasa2.map(function (el) {
                if (el.value == _this_1.updateSingle["hadiah_dikuasakan"]) {
                    el.selected = 1;
                }
                return el;
            });
            this.optionsKuasa2 = options;
        }
        if (this.updateSingle["hadiah_dikuasakan"] == "ya" || this.updateSingle["hadiah_dikuasakan"] == "tidak") {
            var options = this.optionsKuasa3.map(function (el) {
                if (el.value == _this_1.updateSingle["hadiah_dikuasakan"]) {
                    el.selected = 1;
                }
                return el;
            });
            this.optionsKuasa3 = options;
        }
        if (this.updateSingle["hadiah_dikuasakan"] == "-") {
            console.warn("im here!");
            var options = this.optionsKuasa2.map(function (el) {
                if (el.value == _this_1.updateSingle["hadiah_dikuasakan"]) {
                    el.selected = 1;
                }
                return el;
            });
            this.optionsKuasa2 = options;
        }
    };
    FormMemberEditComponent.prototype.checkValueGroup = function () {
        var _this_1 = this;
        if (this.updateSingle["description"] == "upfront" || this.updateSingle["description"] == "nonupfront") {
            var options = this.optionsGroup.map(function (el) {
                if (el.value == _this_1.updateSingle["description"]) {
                    el.selected = 1;
                }
                return el;
            });
            this.optionsGroup = options;
        }
    };
    FormMemberEditComponent.prototype.isArray = function (curVar) {
        return Array.isArray(curVar) ? true : false;
    };
    FormMemberEditComponent.prototype.assignArrayBoolean = function (curVar, varCont) {
        if (this.isArray(curVar)) {
            curVar.forEach(function (element) {
                varCont.push(false);
            });
        }
    };
    FormMemberEditComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var program, _this;
            var _this_1 = this;
            return __generator(this, function (_a) {
                console.warn("detail", this.detail);
                program = localStorage.getItem('programName');
                _this = this;
                this.contentList.filter(function (element) {
                    if (element.appLabel == program) {
                        if (element.type == "reguler") {
                            _this.programType = "reguler";
                        }
                        else if (element.type == "custom_kontraktual") {
                            _this.programType = "custom_kontraktual";
                            _this.sig_kontraktual = true;
                        }
                        else {
                            _this.programType = "custom";
                        }
                    }
                });
                this.updateSingle = JSON.parse(JSON.stringify(this.detail.input_form_data));
                this.updateSingle.foto_bast = this.isArray(this.updateSingle.foto_bast) ? this.updateSingle.foto_bast : [];
                this.allBast = this.updateSingle.foto_bast;
                this.updateSingle.foto_surat_kuasa = this.isArray(this.updateSingle.foto_surat_kuasa) ? this.updateSingle.foto_surat_kuasa : [];
                this.allSuratKuasa = this.updateSingle.foto_surat_kuasa;
                this.assignArrayBoolean(this.allBast, this.loadingBAST);
                this.assignArrayBoolean(this.allSuratKuasa, this.loadingSuratKuasa);
                // console.log("loadingBAST", this.loadingBAST);
                // this.assignArrayBoolean(this.allSuratKuasa);
                // console.log("count", this.allBast, this.allSuratKuasa);
                this.checkValueKuasa();
                this.checkValueGroup();
                this.detail.previous_member_id = this.detail.member_id;
                this.activationStatus.forEach(function (element, index, products_ids) {
                    if (element.value == _this_1.detail.activation_status) {
                        _this_1.activationStatus[index].selected = 1;
                    }
                });
                this.detail.previous_member_id = this.detail.member_id;
                this.memberStatus.forEach(function (element, index, products_ids) {
                    if (element.value == _this_1.detail.member_status) {
                        _this_1.memberStatus[index].selected = 1;
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    FormMemberEditComponent.prototype.backToDetail = function () {
        // console.log("Edit member",this.back);
        this.back[0][this.back[1]]();
        // this.back[0]();
    };
    FormMemberEditComponent.prototype.validURL = function (str) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return !!pattern.test(str);
    };
    FormMemberEditComponent.prototype.openUpload = function (type, index) {
        var upFile = document.getElementById(type + index);
        upFile.click();
    };
    FormMemberEditComponent.prototype.saveThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var new_form, currentBast, currentSuratKuasa, prop, kuasa, nonKuasa, e_1, message;
            var _this_1 = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        new_form = {
                            // key:"ps-form-1",
                            id_pel: this.detail.input_form_data.id_pel,
                        };
                        if (this.updateSingle.foto_bast && this.updateSingle.foto_bast.length > 0) {
                            currentBast = this.updateSingle.foto_bast.filter(function (element) {
                                if (element.url && _this_1.validURL(element.url)) {
                                    return element;
                                }
                            });
                            this.updateSingle.foto_bast = currentBast;
                        }
                        if (this.updateSingle.foto_surat_kuasa && this.updateSingle.foto_surat_kuasa.length > 0) {
                            currentSuratKuasa = this.updateSingle.foto_surat_kuasa.filter(function (element) {
                                if (element.url && _this_1.validURL(element.url)) {
                                    return element;
                                }
                            });
                            this.updateSingle.foto_surat_kuasa = currentSuratKuasa;
                        }
                        for (prop in this.updateSingle) {
                            if (this.updateSingle[prop] != this.detail[prop] && this.updateSingle[prop] != "") {
                                if (prop == 'hadiah_dikuasakan') {
                                    kuasa = ["Ya", "true", "ya"];
                                    nonKuasa = ["Tidak", "false", "tidak"];
                                    if (kuasa.includes(this.updateSingle[prop])) {
                                        new_form[prop] = "KUASA";
                                    }
                                    else if (nonKuasa.includes(this.updateSingle[prop])) {
                                        new_form[prop] = "TIDAK KUASA";
                                    }
                                    else {
                                        new_form[prop] = this.updateSingle[prop];
                                    }
                                }
                                else {
                                    new_form[prop] = this.updateSingle[prop];
                                }
                            }
                            if (prop == 'foto_bast' || prop == 'foto_surat_kuasa') {
                                new_form[prop] = this.updateSingle[prop];
                            }
                        }
                        //check if object from autocomplete component
                        if (new_form) {
                            if (new_form.provinsi_rumah && new_form.provinsi_rumah && typeof new_form.provinsi_rumah === 'object' && new_form.provinsi_rumah !== null && new_form.provinsi_rumah.province) {
                                new_form.provinsi_rumah = new_form.provinsi_rumah.province;
                            }
                            if (new_form.kota_rumah && new_form.kota_rumah && typeof new_form.kota_rumah === 'object' && new_form.kota_rumah !== null && new_form.kota_rumah.city) {
                                new_form.kota_rumah = new_form.kota_rumah.city;
                            }
                            if (new_form.kecamatan_rumah && new_form.kecamatan_rumah && typeof new_form.kecamatan_rumah === 'object' && new_form.kecamatan_rumah !== null && new_form.kecamatan_rumah.subdistrict) {
                                new_form.kecamatan_rumah = new_form.kecamatan_rumah.subdistrict;
                            }
                            if (new_form.kelurahan_rumah && new_form.kelurahan_rumah && typeof new_form.kelurahan_rumah === 'object' && new_form.kelurahan_rumah !== null && new_form.kelurahan_rumah.village) {
                                new_form.kelurahan_rumah = new_form.kelurahan_rumah.village;
                            }
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.memberService.updateSingleFormMember(new_form)];
                    case 2:
                        _a.sent();
                        this.detail = JSON.parse(JSON.stringify(this.updateSingle));
                        //  Swal.fire('Success', 'Edit data pelanggan berhasil', 'success');
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                            title: 'Success',
                            text: 'Edit data pelanggan berhasil',
                            icon: 'success',
                            confirmButtonText: 'Ok',
                        }).then(function (result) {
                            if (result.isConfirmed) {
                                _this_1.backToDetail();
                            }
                        });
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    FormMemberEditComponent.prototype.onFileSelected2 = function (event, img) {
        return __awaiter(this, void 0, void 0, function () {
            var reader, programName, fileExtens, fileMaxSize_1, valueImage_1, splitFileName, extensFile, extens, typeFile, newFileName, logo, e_2;
            var _this_1 = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        reader = new FileReader();
                        programName = localStorage.getItem('programName');
                        fileExtens = ['jpg', 'jpeg', 'png', 'gif', 'pdf'];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        this.errorFile = false;
                        fileMaxSize_1 = 2064000;
                        Array.from(event.target.files).forEach(function (file) {
                            if (file.size > fileMaxSize_1) {
                                _this_1.errorFile = 'Maximum File Upload is 2MB';
                            }
                            var splitFileName = file.name.split('.');
                            if (splitFileName.length < 1) {
                                _this_1.errorFile = "file extention is required";
                            }
                            else {
                                var extensFile = splitFileName[splitFileName.length - 1];
                                var extens = extensFile.toLowerCase();
                                if (!fileExtens.includes(extens))
                                    _this_1.errorFile = "extention ${extens} is not Allowed";
                            }
                        });
                        this.selectedFile = event.target.files[0];
                        reader.onload = function (event) {
                            _this_1.selFile = event.target.result;
                        };
                        if (event.target.files[0]) {
                            reader.readAsDataURL(event.target.files[0]);
                        }
                        if (!this.selectedFile) return [3 /*break*/, 3];
                        valueImage_1 = this.checkLoading(img, true);
                        splitFileName = this.selectedFile.name.split('.');
                        extensFile = splitFileName[splitFileName.length - 1];
                        extens = extensFile.toLowerCase();
                        typeFile = extens == 'pdf' ? 'document' : 'image';
                        newFileName = programName + '-' + this.detail.username + '-' + img + '.' + extens;
                        return [4 /*yield*/, this.productService.upload(this.selectedFile, this, typeFile, function (result) {
                                if (typeof result == 'object' && result.base_url) {
                                    _this_1.updateSingle[valueImage_1] = result.base_url + result.pic_big_path;
                                }
                                else {
                                    _this_1.updateSingle[valueImage_1] = result;
                                }
                                _this_1.checkLoading(img, false);
                                // console.log("updateSingle", this.updateSingle);
                                // this.callImage(result, img);
                            }, newFileName)];
                    case 2:
                        logo = _a.sent();
                        _a.label = 3;
                    case 3: return [3 /*break*/, 5];
                    case 4:
                        e_2 = _a.sent();
                        this.errorLabel = e_2.message; //conversion to Error type
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    FormMemberEditComponent.prototype.checkFileType = function (text) {
        var splitFileName = text.split('.');
        var extensFile = splitFileName[splitFileName.length - 1];
        var extens = extensFile.toLowerCase();
        return extens == 'pdf' ? 'document' : 'image';
    };
    FormMemberEditComponent.prototype.onFileSelected = function (event, typeImg, idx) {
        return __awaiter(this, void 0, void 0, function () {
            var reader, fileMaxSize_2, e_3;
            var _this_1 = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (typeImg == 'bast') {
                            this.loadingBAST[idx] = true;
                        }
                        else if (typeImg == 'suratKuasa') {
                            this.loadingSuratKuasa[idx] = true;
                        }
                        reader = new FileReader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        this.errorFile = false;
                        fileMaxSize_2 = 2064000;
                        Array.from(event.target.files).forEach(function (file) {
                            if (file.size > fileMaxSize_2) {
                                _this_1.errorFile = 'Maximum File Upload is 2MB';
                            }
                        });
                        this.selectedFile = event.target.files[0];
                        reader.onload = function (event) {
                            _this_1.selFile = event.target.result;
                        };
                        if (event.target.files[0]) {
                            reader.readAsDataURL(event.target.files[0]);
                        }
                        if (!this.selectedFile) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.productService.upload(this.selectedFile, this, 'image', function (result) {
                                var dataImage = {
                                    "url": result.base_url + result.pic_big_path,
                                    "updated_date": Date.now()
                                };
                                if (typeImg == 'bast') {
                                    _this_1.updateSingle.foto_bast[idx] = __assign({}, _this_1.updateSingle.foto_bast[idx], dataImage);
                                    _this_1.loadingBAST[idx] = false;
                                }
                                else if (typeImg == 'suratKuasa') {
                                    _this_1.updateSingle.foto_surat_kuasa[idx] = __assign({}, _this_1.updateSingle.foto_surat_kuasa[idx], dataImage);
                                    _this_1.loadingSuratKuasa[idx] = false;
                                }
                            })];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3: return [3 /*break*/, 5];
                    case 4:
                        e_3 = _a.sent();
                        this.errorLabel = e_3.message; //conversion to Error type
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    FormMemberEditComponent.prototype.addMoreUpload = function (typeImg) {
        var dummyImg = {
            "url": "",
            "updated_date": Date.now(),
            "desc": ""
        };
        if (typeImg == 'bast') {
            // if(!this.isArray(this.updateSingle.foto_bast)) this.updateSingle.foto_bast = [];
            this.updateSingle.foto_bast.push(dummyImg);
            this.loadingBAST.push(false);
        }
        else if (typeImg == 'suratKuasa') {
            // if(!this.isArray(this.updateSingle.foto_surat_kuasa)) this.updateSingle.foto_surat_kuasa = [];
            this.updateSingle.foto_surat_kuasa.push(dummyImg);
            this.loadingSuratKuasa.push(false);
        }
    };
    FormMemberEditComponent.prototype.deleteImage = function (type, idx, url) {
        var _this_1 = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: "Delete",
            text: "Are you sure want to delete this ?",
            imageUrl: url,
            imageHeight: 250,
            imageAlt: type,
            showCancelButton: true,
            focusConfirm: false,
        }).then(function (res) {
            if (res.isConfirmed) {
                if (type == "bast") {
                    _this_1.updateSingle.foto_bast.splice(idx, 1);
                    _this_1.loadingBAST.splice(idx, 1);
                    // console.log("BAST DELETE", this.updateSingle.foto_bast,this.allBast);
                }
                else if (type = "suratKuasa") {
                    _this_1.updateSingle.foto_surat_kuasa.splice(idx, 1);
                    _this_1.loadingSuratKuasa.splice(idx, 1);
                    // console.log("SURAT KUASA DELETE", this.updateSingle.foto_surat_kuasa,this.allSuratKuasa);
                }
            }
        });
    };
    FormMemberEditComponent.prototype.deletePhoto = function (type, url) {
        var _this_1 = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: "Delete",
            text: "Are you sure want to delete this ?",
            imageUrl: url,
            imageHeight: 250,
            imageAlt: type,
            showCancelButton: true,
            focusConfirm: false,
        }).then(function (res) {
            if (res.isConfirmed) {
                if (type == "foto_ktp_pemilik") {
                    _this_1.updateSingle.foto_ktp_pemilik = '-';
                    _this_1.loadingKTPPemilik = false;
                    // console.log("KTP Pemilik DELETE", this.updateSingle.foto_ktp_pemilik);
                }
                else if (type == "foto_npwp_pemilik") {
                    _this_1.updateSingle.foto_npwp_pemilik = '-';
                    _this_1.loadingNPWPPemilik = false;
                    // console.log("NPWP Pemilik DELETE", this.updateSingle.foto_npwp_pemilik);
                }
                else if (type == "foto_ktp_penerima") {
                    _this_1.updateSingle.foto_ktp_penerima = '-';
                    _this_1.loadingKTPPenerima = false;
                    // console.log("KTP Penerima DELETE", this.updateSingle.foto_ktp_penerima);
                }
                else if (type == "foto_npwp_penerima") {
                    _this_1.updateSingle.foto_npwp_penerima = '-';
                    _this_1.loadingNPWPPenerima = false;
                    // console.log("NPWP Penerima DELETE", this.updateSingle.foto_npwp_penerima);
                }
            }
        });
    };
    FormMemberEditComponent.prototype.convertDate = function (time) {
        var date = new Date(time);
        var dateString = date.toString();
        var retDate = dateString.split("GMT");
        return retDate[0];
    };
    FormMemberEditComponent.prototype.checkLoading = function (fieldName, fieldValue) {
        var valueImage;
        switch (fieldName) {
            case 'ktpPemilik':
                valueImage = 'foto_ktp_pemilik';
                this.loadingKTPPemilik = fieldValue;
                break;
            case 'npwpPemilik':
                valueImage = 'foto_npwp_pemilik';
                this.loadingNPWPPemilik = fieldValue;
                break;
            case 'ktpPenerima':
                valueImage = 'foto_ktp_penerima';
                this.loadingKTPPenerima = fieldValue;
                break;
            case 'npwpPenerima':
                valueImage = 'foto_npwp_penerima';
                this.loadingNPWPPenerima = fieldValue;
                break;
            case 'bast':
                valueImage = 'foto_bast';
                this.loadingBAST = fieldValue;
                break;
            case 'suratKuasa':
                valueImage = 'foto_surat_kuasa';
                this.loadingSuratKuasa = fieldValue;
        }
        return valueImage;
    };
    FormMemberEditComponent.prototype.selectEvent = function (item, region) {
        // do something with selected item
        if (region == "province") {
            this.updateSingle.provinsi_rumah = item.province;
            this.province_code = item.province_code;
        }
        else if (region == "city") {
            this.updateSingle.kota_rumah = item.city;
            this.city_code = item.city_code;
        }
        else if (region == "subdistrict") {
            this.updateSingle.kecamatan_rumah = item.subdistrict;
            this.subdistrict_code = item.subdistrict_code;
        }
        else if (region == "village") {
            this.updateSingle.kelurahan_rumah = item.village;
            this.village_code = item.village_code;
        }
    };
    FormMemberEditComponent.prototype.inputRegion = function (val, region) {
        return __awaiter(this, void 0, void 0, function () {
            var _this_1 = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: 
                    //prevent calling API twice or more
                    return [4 /*yield*/, clearTimeout(this.inputTimeOut)];
                    case 1:
                        //prevent calling API twice or more
                        _a.sent();
                        this.isInputComplete = false;
                        //set loading indicator
                        if (region == "province") {
                            this.loadingProvince = true;
                            if (this.programType != "reguler") {
                                this.updateSingle.kota_rumah = "";
                                this.updateSingle.kecamatan_rumah = "";
                                this.updateSingle.kelurahan_rumah = "";
                                delete this.province_code;
                                delete this.city_code;
                                delete this.subdistrict_code;
                            }
                        }
                        else if (region == "city") {
                            this.loadingCity = true;
                            if (this.programType != "reguler") {
                                this.updateSingle.kecamatan_rumah = "";
                                this.updateSingle.kelurahan_rumah = "";
                                delete this.city_code;
                                delete this.subdistrict_code;
                            }
                        }
                        else if (region == "subdistrict") {
                            this.loadingSubDistrict = true;
                            if (this.programType != "reguler") {
                                delete this.subdistrict_code;
                                this.updateSingle.kelurahan_rumah = "";
                            }
                        }
                        else if (region == "village") {
                            this.loadingVillage = true;
                        }
                        this.inputTimeOut = setTimeout(function () {
                            _this_1.isInputComplete = true;
                            if (_this_1.isInputComplete) {
                                _this_1.filterRegion(val, region);
                            }
                        }, 300);
                        return [2 /*return*/];
                }
            });
        });
    };
    FormMemberEditComponent.prototype.filterRegion = function (search, region) {
        var _this_1 = this;
        var payload = {
            "region": region,
            "search": search
        };
        if (region == "city") {
            payload.province_code = this.province_code;
        }
        else if (region == "subdistrict") {
            payload.city_code = this.city_code;
        }
        else if (region == "village") {
            payload.subdistrict_code = this.subdistrict_code;
        }
        // Set val to the value of the searchbar
        if (search.length > 0) {
            this.memberService.searchRegion(payload).then(function (result) {
                _this_1.loadingProvince = false;
                _this_1.loadingCity = false;
                _this_1.loadingSubDistrict = false;
                _this_1.loadingVillage = false;
                if (result && result.length > 0) {
                    if (region == "province") {
                        _this_1.dataProvince = result;
                    }
                    else if (region == "city") {
                        _this_1.dataCity = result;
                    }
                    else if (region == "subdistrict") {
                        _this_1.dataSubDistrict = result;
                    }
                    else if (region == "village") {
                        _this_1.dataVillage = result;
                    }
                }
            }, function (err) {
                console.log(err);
                _this_1.loadingProvince = false;
                _this_1.loadingCity = false;
                _this_1.loadingSubDistrict = false;
                _this_1.loadingVillage = false;
                _this_1.dataProvince = [];
                _this_1.dataCity = [];
                _this_1.dataSubDistrict = [];
                _this_1.dataVillage = [];
            });
        }
        else {
            this.loadingProvince = false;
            this.loadingCity = false;
            this.loadingSubDistrict = false;
            this.loadingVillage = false;
            this.dataProvince = [];
            this.dataCity = [];
            this.dataSubDistrict = [];
            this.dataVillage = [];
        }
    };
    FormMemberEditComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"] },
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__["ProductService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], FormMemberEditComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], FormMemberEditComponent.prototype, "back", void 0);
    FormMemberEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-form-member-edit',
            template: __webpack_require__(/*! raw-loader!./form-member.edit.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/form-member/edit/form-member.edit.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_2__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./form-member.edit.component.scss */ "./src/app/layout/modules/form-member/edit/form-member.edit.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"], _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__["ProductService"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]])
    ], FormMemberEditComponent);
    return FormMemberEditComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/form-member/form-member-routing.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/layout/modules/form-member/form-member-routing.module.ts ***!
  \**************************************************************************/
/*! exports provided: DataMemberRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataMemberRoutingModule", function() { return DataMemberRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _form_member_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./form-member.component */ "./src/app/layout/modules/form-member/form-member.component.ts");
/* harmony import */ var _add_form_member_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/form-member.add.component */ "./src/app/layout/modules/form-member/add/form-member.add.component.ts");
/* harmony import */ var _add_bulk_form_member_add_bulk_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./add-bulk/form-member.add-bulk.component */ "./src/app/layout/modules/form-member/add-bulk/form-member.add-bulk.component.ts");
/* harmony import */ var _detail_form_member_detail_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./detail/form-member.detail.component */ "./src/app/layout/modules/form-member/detail/form-member.detail.component.ts");
/* harmony import */ var _list_order_list_order_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./list-order/list-order.component */ "./src/app/layout/modules/form-member/list-order/list-order.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '', component: _form_member_component__WEBPACK_IMPORTED_MODULE_2__["FormMemberComponent"],
    },
    {
        path: 'add', component: _add_form_member_add_component__WEBPACK_IMPORTED_MODULE_3__["FormMemberAddComponent"]
    },
    {
        path: 'add-bulk', component: _add_bulk_form_member_add_bulk_component__WEBPACK_IMPORTED_MODULE_4__["FormMemberAddBulkComponent"]
    },
    {
        path: 'detail', component: _detail_form_member_detail_component__WEBPACK_IMPORTED_MODULE_5__["FormMemberDetailComponent"]
    },
    {
        path: 'list-order', component: _list_order_list_order_component__WEBPACK_IMPORTED_MODULE_6__["ListOrderComponent"]
    },
];
var DataMemberRoutingModule = /** @class */ (function () {
    function DataMemberRoutingModule() {
    }
    DataMemberRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], DataMemberRoutingModule);
    return DataMemberRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/form-member/form-member.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/layout/modules/form-member/form-member.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "iframe {\n  width: 0px;\n  height: 0px;\n  border: 0px medium none;\n  visibility: hidden;\n}\n\n.error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 26px;\n  padding-top: 10px;\n}\n\n.error-message .text-message {\n  background: #E74C3C;\n  padding: 20px;\n  margin: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZm9ybS1tZW1iZXIvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxmb3JtLW1lbWJlclxcZm9ybS1tZW1iZXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2Zvcm0tbWVtYmVyL2Zvcm0tbWVtYmVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksVUFBQTtFQUNBLFdBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0FDQ0o7O0FERUE7RUFDSSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ0NKOztBRENJO0VBQ0ksbUJBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtBQ0NSIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZm9ybS1tZW1iZXIvZm9ybS1tZW1iZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpZnJhbWV7XHJcbiAgICB3aWR0aDowcHg7XHJcbiAgICBoZWlnaHQ6IDBweDtcclxuICAgIGJvcmRlcjowcHggbWVkaXVtIG5vbmU7XHJcbiAgICB2aXNpYmlsaXR5OiBoaWRkZW47XHJcbn1cclxuXHJcbi5lcnJvci1tZXNzYWdlIHtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgaGVpZ2h0OjEwMHZoO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOjI2cHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcclxuXHJcbiAgICAudGV4dC1tZXNzYWdlIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjRTc0QzNDO1xyXG4gICAgICAgIHBhZGRpbmc6IDIwcHg7XHJcbiAgICAgICAgbWFyZ2luOiAwIDEwcHg7XHJcbiAgICB9XHJcbn0iLCJpZnJhbWUge1xuICB3aWR0aDogMHB4O1xuICBoZWlnaHQ6IDBweDtcbiAgYm9yZGVyOiAwcHggbWVkaXVtIG5vbmU7XG4gIHZpc2liaWxpdHk6IGhpZGRlbjtcbn1cblxuLmVycm9yLW1lc3NhZ2Uge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDI2cHg7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xufVxuLmVycm9yLW1lc3NhZ2UgLnRleHQtbWVzc2FnZSB7XG4gIGJhY2tncm91bmQ6ICNFNzRDM0M7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIG1hcmdpbjogMCAxMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/form-member/form-member.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/layout/modules/form-member/form-member.component.ts ***!
  \*********************************************************************/
/*! exports provided: FormMemberComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormMemberComponent", function() { return FormMemberComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_5___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






// import { Http, Response, Headers, RequestOptions, RequestMethod, RequestOptionsArgs, URLSearchParams, ResponseContentType } from '@angular/http';
// import { Observable } from 'rxjs';
// import { map } from "rxjs/operators";
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';
// import 'rxjs/Rx' ;
var FormMemberComponent = /** @class */ (function () {
    // private options = new RequestOptions(
    //   { headers: new Headers({ 'Content-Type': 'application/json' }) });
    function FormMemberComponent(memberService, sanitizer) {
        this.memberService = memberService;
        this.sanitizer = sanitizer;
        this.mci_project = false;
        this.Members = [];
        this.row_id = "_id";
        this.tableFormat = {
            title: 'Form Member',
            label_headers: [
                { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'username' },
                { label: 'Data Complete', visible: true, type: 'data_complete', data_row_name: 'additional_info' },
                { label: 'Nama Toko', visible: true, type: 'form_nama_toko', data_row_name: 'input_form_data' },
                { label: 'Nama Distributor', visible: true, type: 'form_nama_distributor', data_row_name: 'input_form_data' },
                { label: 'Alamat Toko', visible: true, type: 'form_alamat_toko', data_row_name: 'input_form_data' },
                { label: 'Nama Pemilik', visible: true, type: 'form_nama_pemilik', data_row_name: 'input_form_data' },
                { label: 'No. KTP Pemilik', visible: true, type: 'form_ktp_pemilik', data_row_name: 'input_form_data' },
                { label: 'Foto KTP Pemilik', visible: true, type: 'form_foto_ktp_pemilik', data_row_name: 'input_form_data' },
                { label: 'No. NPWP Pemilik', visible: true, type: 'form_npwp_pemilik', data_row_name: 'input_form_data' },
                { label: 'Foto NPWP Pemilik', visible: true, type: 'form_foto_npwp_pemilik', data_row_name: 'input_form_data' },
                { label: 'No. Telp Pemilik', visible: true, type: 'form_telp_pemilik', data_row_name: 'input_form_data' },
                { label: 'No. WA Pemilik', visible: true, type: 'form_wa_pemilik', data_row_name: 'input_form_data' },
                { label: 'Alamat Pengiriman', visible: true, type: 'form_alamat_penerima', data_row_name: 'input_form_data' },
                { label: 'Hadiah Dikuasakan', visible: true, type: 'form_hadiah_dikuasakan', data_row_name: 'input_form_data' },
                { label: 'Nama penerima', visible: true, type: 'form_nama_penerima', data_row_name: 'input_form_data' },
                { label: 'No. KTP Penerima', visible: true, type: 'form_ktp_penerima', data_row_name: 'input_form_data' },
                { label: 'Foto KTP Penerima', visible: true, type: 'form_foto_ktp_penerima', data_row_name: 'input_form_data' },
                { label: 'No. NPWP Penerima', visible: true, type: 'form_npwp_penerima', data_row_name: 'input_form_data' },
                { label: 'Foto NPWP Penerima', visible: true, type: 'form_foto_npwp_penerima', data_row_name: 'input_form_data' },
                { label: 'No. WA Penerima', visible: true, type: 'form_wa_penerima', data_row_name: 'input_form_data' },
                { label: 'No. Gopay Penerima', visible: true, type: 'form_gopay_penerima', data_row_name: 'input_form_data' },
                { label: 'Group', visible: true, type: 'form_group', data_row_name: 'input_form_data' },
                { label: 'Program/Cluster', visible: true, type: 'form_cluster', data_row_name: 'input_form_data' },
                { label: 'Created Date', visible: true, type: 'date', data_row_name: 'created_date' },
                { label: 'Updated Date', visible: true, type: 'date', data_row_name: 'updated_date' },
            ],
            row_primary_key: '_id',
            formOptions: {
                addForm: true,
                bulkUpdate: true,
                row_id: this.row_id,
                this: this,
                result_var_name: 'Members',
                detail_function: [this, 'callDetail'],
                setCompleteMember: true
            },
            show_checkbox_options: true
        };
        this.tableFormat2 = {
            title: 'Form Member',
            label_headers: [
                { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'username' },
                { label: 'Data Complete', visible: true, type: 'data_complete', data_row_name: 'additional_info' },
                { label: 'ID Bisnis', visible: true, type: 'form_business_id', data_row_name: 'input_form_data' },
                { label: 'Nama Pelanggan', visible: true, type: 'form_nama_pemilik', data_row_name: 'input_form_data' },
                { label: 'No. Telp Pelanggan', visible: true, type: 'form_telp_pemilik', data_row_name: 'input_form_data' },
                // {label: 'No. WA Pelanggan',     visible: true, type: 'form_wa_pemilik', data_row_name: 'input_form_data'},
                { label: 'Email Pelanggan', visible: true, type: 'form_email_pelanggan', data_row_name: 'input_form_data' },
                { label: 'Alamat Pengiriman', visible: true, type: 'form_alamat_penerima', data_row_name: 'input_form_data' },
                // {label: 'Nama penerima',     visible: true, type: 'form_nama_penerima', data_row_name: 'input_form_data'},
                // {label: 'No. WA Penerima',     visible: true, type: 'form_wa_penerima', data_row_name: 'input_form_data'},
                { label: 'Program', visible: true, type: 'form_group', data_row_name: 'input_form_data' },
                { label: 'Created Date', visible: true, type: 'date', data_row_name: 'created_date' },
                { label: 'Updated Date', visible: true, type: 'date', data_row_name: 'updated_date' },
            ],
            row_primary_key: '_id',
            formOptions: {
                addForm: true,
                bulkUpdate: true,
                row_id: this.row_id,
                this: this,
                result_var_name: 'Members',
                detail_function: [this, 'callDetail'],
                setCompleteMember: true
            },
            show_checkbox_options: true
        };
        this.tableFormat3 = {
            title: 'Form Member',
            label_headers: [
                { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'username' },
                { label: 'Data Complete', visible: true, type: 'data_complete', data_row_name: 'additional_info' },
                { label: 'Nama Program', visible: true, type: 'form_cluster', data_row_name: 'input_form_data' },
                { label: 'Nama Entitas', visible: true, type: 'form_nama_toko', data_row_name: 'input_form_data' },
                // {label: 'Nama Distributor', visible: true, type: 'form_nama_distributor', data_row_name: 'input_form_data'},
                { label: 'Alamat', visible: true, type: 'form_alamat_toko', data_row_name: 'input_form_data' },
                { label: 'Nama PIC', visible: true, type: 'form_nama_pemilik', data_row_name: 'input_form_data' },
                { label: 'No. KTP PIC', visible: true, type: 'form_ktp_pemilik', data_row_name: 'input_form_data' },
                { label: 'Foto KTP PIC', visible: true, type: 'form_foto_ktp_pemilik', data_row_name: 'input_form_data' },
                { label: 'No. NPWP PIC', visible: true, type: 'form_npwp_pemilik', data_row_name: 'input_form_data' },
                { label: 'Foto NPWP PIC', visible: true, type: 'form_foto_npwp_pemilik', data_row_name: 'input_form_data' },
                { label: 'No. Telp PIC', visible: true, type: 'form_telp_pemilik', data_row_name: 'input_form_data' },
                { label: 'No. WA PIC', visible: true, type: 'form_wa_pemilik', data_row_name: 'input_form_data' },
                { label: 'Alamat Pengiriman', visible: true, type: 'form_alamat_penerima', data_row_name: 'input_form_data' },
                { label: 'Hadiah Dikuasakan', visible: true, type: 'form_hadiah_dikuasakan', data_row_name: 'input_form_data' },
                { label: 'Nama penerima', visible: true, type: 'form_nama_penerima', data_row_name: 'input_form_data' },
                { label: 'Jabatan', visible: true, type: 'form_group', data_row_name: 'input_form_data' },
                { label: 'No. KTP Penerima', visible: true, type: 'form_ktp_penerima', data_row_name: 'input_form_data' },
                { label: 'Foto KTP Penerima', visible: true, type: 'form_foto_ktp_penerima', data_row_name: 'input_form_data' },
                { label: 'No. NPWP Penerima', visible: true, type: 'form_npwp_penerima', data_row_name: 'input_form_data' },
                { label: 'Foto NPWP Penerima', visible: true, type: 'form_foto_npwp_penerima', data_row_name: 'input_form_data' },
                { label: 'No. WA Penerima', visible: true, type: 'form_wa_penerima', data_row_name: 'input_form_data' },
                { label: 'No. E-Wallet Penerima', visible: true, type: 'form_gopay_penerima', data_row_name: 'input_form_data' },
                { label: 'Created Date', visible: true, type: 'date', data_row_name: 'created_date' },
                { label: 'Updated Date', visible: true, type: 'date', data_row_name: 'updated_date' },
            ],
            row_primary_key: '_id',
            formOptions: {
                addForm: true,
                bulkUpdate: true,
                row_id: this.row_id,
                this: this,
                result_var_name: 'Members',
                detail_function: [this, 'callDetail'],
                setCompleteMember: true
            },
            show_checkbox_options: true
        };
        this.contentList = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_5__;
        this.form_input = {};
        this.errorLabel = false;
        this.errorMessage = false;
        this.memberDetail = false;
        this.currentPage = 1;
        this.programType = "";
        this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
    }
    FormMemberComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    FormMemberComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var program, _this, result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        program = localStorage.getItem('programName');
                        _this = this;
                        this.contentList.filter(function (element) {
                            if (element.appLabel == program) {
                                if (element.type == "reguler") {
                                    _this.mci_project = true;
                                }
                                else if (element.type == "custom_kontraktual") {
                                    _this.programType = "custom_kontraktual";
                                }
                                else {
                                    _this.mci_project = false;
                                }
                            }
                        });
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        // let params = {
                        //   'type':'member'
                        // }
                        this.service = this.memberService;
                        return [4 /*yield*/, this.memberService.getAutoformMember(this.currentPage)];
                    case 2:
                        result = _a.sent();
                        this.totalPage = result.total_page;
                        this.Members = result.values;
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    FormMemberComponent.prototype.doPageChange = function (currentPage) {
        console.log('currentPage: ', currentPage);
        this.currentPage = currentPage;
    };
    FormMemberComponent.prototype.callDetail = function (data, rowData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                try {
                    // console.warn("id search",rowData.id_pel);
                    // console.warn("members", this.Members);
                    // this.service    = this.memberService;
                    // let result: any = await this.memberService.detailMember(_id);
                    // this.memberDetail = result.result[0];
                    this.memberDetail = this.Members.find(function (member) { return member.username == rowData.username; });
                    // console.warn("Member detail",this.memberDetail);
                }
                catch (e) {
                    this.errorLabel = (e.message); //conversion to Error type
                }
                return [2 /*return*/];
            });
        });
    };
    FormMemberComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.memberDetail = false;
                obj.firstLoad();
                return [2 /*return*/];
            });
        });
    };
    FormMemberComponent.prototype.onDownload = function (downloadLint) {
        var srcDownload = downloadLint;
        //let srcDownload = "http://localhost:8888/f3/assets/csv/members_report.csv";
        this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl(srcDownload);
        //console.log(this.srcDownload);
        this.getDownloadFileLint();
    };
    FormMemberComponent.prototype.getDownloadFileLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, downloadLint, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        result = void 0;
                        this.service = this.memberService;
                        return [4 /*yield*/, this.memberService.getDownloadFileLint()];
                    case 1:
                        result = _a.sent();
                        downloadLint = result.result;
                        //console.log(downloadLint);
                        //To running other subfunction with together automatically
                        this.onDownload(downloadLint);
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // public async pagination() {
    //   try
    //   {
    //     let result: any;
    //     this.service = this.memberService;
    //     result = await this.memberService.getDownloadFileLint();
    //     //console.log(result.result);
    //     let downloadLint = result.result;
    //     //console.log(downloadLint);
    //     //To running other subfunction with together automatically
    //     this.onDownload(downloadLint);
    //   }
    //   catch (e) {
    //     this.errorLabel = ((<Error>e).message);//conversion to Error type
    //   }  
    // }
    FormMemberComponent.prototype.completeSelectedMembers = function (obj, members) {
        return __awaiter(this, void 0, void 0, function () {
            var _this_1 = this;
            return __generator(this, function (_a) {
                console.log("members", members);
                if (members.length > 0) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                        title: 'Confirmation',
                        text: 'Apakah anda yakin ingin memproses complete semua member yang dipilih?',
                        // icon: 'success',
                        confirmButtonText: 'Process',
                        cancelButtonText: "Cancel",
                        showCancelButton: true
                    }).then(function (result) { return __awaiter(_this_1, void 0, void 0, function () {
                        var memberIds_1, payload, e_3;
                        var _this_1 = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!result.isConfirmed) return [3 /*break*/, 6];
                                    _a.label = 1;
                                case 1:
                                    _a.trys.push([1, 4, , 5]);
                                    memberIds_1 = [];
                                    return [4 /*yield*/, members.forEach(function (member) { return __awaiter(_this_1, void 0, void 0, function () {
                                            return __generator(this, function (_a) {
                                                console.log("member", member);
                                                console.log("member._id", member.username);
                                                memberIds_1.push(member.username);
                                                return [2 /*return*/];
                                            });
                                        }); })];
                                case 2:
                                    _a.sent();
                                    console.log("memberIds", memberIds_1);
                                    payload = {
                                        "member_id": memberIds_1,
                                        "status": "ya"
                                    };
                                    return [4 /*yield*/, obj.memberService.setCompleteMember(payload)];
                                case 3:
                                    _a.sent();
                                    return [3 /*break*/, 5];
                                case 4:
                                    e_3 = _a.sent();
                                    console.log(e_3);
                                    return [3 /*break*/, 5];
                                case 5:
                                    // console.log("member ids", memberIds);
                                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                                        title: 'Success',
                                        text: 'Semua member terpilih telah complete',
                                        icon: 'success',
                                        confirmButtonText: 'Ok',
                                        showCancelButton: false
                                    }).then(function (result) { return __awaiter(_this_1, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            if (result.isConfirmed) {
                                                obj.firstLoad();
                                            }
                                            return [2 /*return*/];
                                        });
                                    }); });
                                    _a.label = 6;
                                case 6: return [2 /*return*/];
                            }
                        });
                    }); });
                }
                return [2 /*return*/];
            });
        });
    };
    FormMemberComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"] },
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"] }
    ]; };
    FormMemberComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-form-member',
            template: __webpack_require__(/*! raw-loader!./form-member.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/form-member/form-member.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./form-member.component.scss */ "./src/app/layout/modules/form-member/form-member.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"]])
    ], FormMemberComponent);
    return FormMemberComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/form-member/form-member.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/layout/modules/form-member/form-member.module.ts ***!
  \******************************************************************/
/*! exports provided: FormMemberModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormMemberModule", function() { return FormMemberModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _form_member_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./form-member.component */ "./src/app/layout/modules/form-member/form-member.component.ts");
/* harmony import */ var _add_form_member_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/form-member.add.component */ "./src/app/layout/modules/form-member/add/form-member.add.component.ts");
/* harmony import */ var _add_bulk_form_member_add_bulk_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./add-bulk/form-member.add-bulk.component */ "./src/app/layout/modules/form-member/add-bulk/form-member.add-bulk.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _form_member_routing_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./form-member-routing.module */ "./src/app/layout/modules/form-member/form-member-routing.module.ts");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var _detail_form_member_detail_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./detail/form-member.detail.component */ "./src/app/layout/modules/form-member/detail/form-member.detail.component.ts");
/* harmony import */ var _edit_form_member_edit_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./edit/form-member.edit.component */ "./src/app/layout/modules/form-member/edit/form-member.edit.component.ts");
/* harmony import */ var _show_password_show_password_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./show-password/show-password.component */ "./src/app/layout/modules/form-member/show-password/show-password.component.ts");
/* harmony import */ var _additional_document_additional_document_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./additional-document/additional-document.component */ "./src/app/layout/modules/form-member/additional-document/additional-document.component.ts");
/* harmony import */ var _additional_document_edit_additional_document_edit_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./additional-document/edit/additional-document.edit.component */ "./src/app/layout/modules/form-member/additional-document/edit/additional-document.edit.component.ts");
/* harmony import */ var _redeem_login_redeem_login_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./redeem-login/redeem-login.component */ "./src/app/layout/modules/form-member/redeem-login/redeem-login.component.ts");
/* harmony import */ var _list_order_list_order_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./list-order/list-order.component */ "./src/app/layout/modules/form-member/list-order/list-order.component.ts");
/* harmony import */ var _list_order_edit_list_order_edit_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./list-order/edit/list-order.edit.component */ "./src/app/layout/modules/form-member/list-order/edit/list-order.edit.component.ts");
/* harmony import */ var angular_ng_autocomplete__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! angular-ng-autocomplete */ "./node_modules/angular-ng-autocomplete/fesm5/angular-ng-autocomplete.js");
/* harmony import */ var ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ng2-pdf-viewer */ "./node_modules/ng2-pdf-viewer/fesm5/ng2-pdf-viewer.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





















var FormMemberModule = /** @class */ (function () {
    function FormMemberModule() {
    }
    FormMemberModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _form_member_routing_module__WEBPACK_IMPORTED_MODULE_9__["DataMemberRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_5__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_8__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_10__["BsComponentModule"],
                angular_ng_autocomplete__WEBPACK_IMPORTED_MODULE_19__["AutocompleteLibModule"],
                ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_20__["PdfViewerModule"]
            ],
            declarations: [
                _form_member_component__WEBPACK_IMPORTED_MODULE_2__["FormMemberComponent"],
                _add_form_member_add_component__WEBPACK_IMPORTED_MODULE_3__["FormMemberAddComponent"],
                _add_bulk_form_member_add_bulk_component__WEBPACK_IMPORTED_MODULE_4__["FormMemberAddBulkComponent"],
                _detail_form_member_detail_component__WEBPACK_IMPORTED_MODULE_11__["FormMemberDetailComponent"],
                _edit_form_member_edit_component__WEBPACK_IMPORTED_MODULE_12__["FormMemberEditComponent"],
                _show_password_show_password_component__WEBPACK_IMPORTED_MODULE_13__["ShowPasswordComponent"],
                _additional_document_additional_document_component__WEBPACK_IMPORTED_MODULE_14__["AdditonalDocumentComponent"],
                _additional_document_edit_additional_document_edit_component__WEBPACK_IMPORTED_MODULE_15__["EditAdditonalDocumentComponent"],
                _redeem_login_redeem_login_component__WEBPACK_IMPORTED_MODULE_16__["RedeemLoginComponent"],
                _list_order_list_order_component__WEBPACK_IMPORTED_MODULE_17__["ListOrderComponent"],
                _list_order_edit_list_order_edit_component__WEBPACK_IMPORTED_MODULE_18__["EditListOrderComponent"]
            ],
        })
    ], FormMemberModule);
    return FormMemberModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/form-member/list-order/edit/list-order.edit.component.scss":
/*!*******************************************************************************************!*\
  !*** ./src/app/layout/modules/form-member/list-order/edit/list-order.edit.component.scss ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n}\n.card-detail > .card-header .back_button {\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 7px;\n  background-color: #3498db;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n.card-detail > .card-header .save_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button.true {\n  background-color: #e67e22;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .member-detail .image {\n  overflow: hidden;\n}\n.card-detail .member-detail .image > div {\n  width: 33.3333%;\n  padding: 5px;\n  float: left;\n}\n.card-detail .member-detail .image .card-header {\n  background-color: #f0f0f0;\n}\n.card-detail .member-detail .image h3 {\n  font-size: 12px;\n  margin-bottom: 0px;\n  color: #555;\n}\n.card-detail .member-detail .image .card-content {\n  overflow: hidden;\n  width: 100%;\n  height: auto;\n}\n.card-detail .member-detail .image .card-content img {\n  max-width: 100%;\n  width: 100%;\n}\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n.member-detail-image-container {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.member-detail-image-container .img-upload {\n  display: flex;\n  justify-content: space-around;\n  align-items: center;\n  margin: 20px 10px;\n}\n.member-detail-image-container .img-upload .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.member-detail-image-container .form-group.uploaded {\n  position: relative;\n  margin-bottom: 0px;\n  height: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading {\n  height: 100%;\n  width: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading .sk-fading-circle {\n  top: 50%;\n}\nbutton.btn_upload, button.add-more-image, button.btn_delete {\n  background-color: #3498db;\n  color: #fff;\n  padding: 5px 15px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 4px;\n  border: 0;\n  cursor: pointer;\n}\nbutton.btn_delete {\n  background-color: salmon;\n}\n.add-more-container {\n  display: flex;\n  width: 100%;\n  align-items: center;\n  justify-content: center;\n}\n.label-bold {\n  font-weight: 600;\n}\n.rounded-btn {\n  margin-top: 20px;\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  background: #56a4ff;\n}\n.rounded-btn-danger {\n  margin-top: 20px;\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  color: #555;\n  margin-left: 10px;\n}\n.rounded-btn:hover {\n  background: #0a7bff;\n  outline: none;\n}\n.member-detail-image-container {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.member-detail-image-container .img-upload {\n  display: flex;\n  justify-content: space-around;\n  align-items: center;\n  margin: 20px 10px;\n}\n.member-detail-image-container .img-upload .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.member-detail-image-container .form-group.uploaded {\n  position: relative;\n  margin-bottom: 0px;\n  height: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading {\n  height: 100%;\n  width: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading .sk-fading-circle {\n  top: 50%;\n}\nbutton.btn_upload, button.add-more-image, button.btn_delete {\n  background-color: #3498db;\n  color: #fff;\n  padding: 5px 15px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 4px;\n  border: 0;\n  cursor: pointer;\n}\nbutton.btn-change-password {\n  width: 100%;\n}\nbutton.btn_delete {\n  background-color: salmon;\n}\n.add-more-container {\n  display: flex;\n  width: 100%;\n  align-items: center;\n  justify-content: center;\n}\n.label-bold {\n  font-weight: 600;\n}\n.error-msg-upload {\n  margin-top: 10px;\n  padding-left: 0px !important;\n  padding-right: 0px !important;\n}\n.container-two-button {\n  display: flex;\n}\n.file-selected {\n  color: red;\n}\n.btn-add-bulk {\n  margin-bottom: 20px;\n}\n.btn-upload {\n  margin-right: 10px;\n}\n.member-detail-image-container {\n  background: #f0f0f0;\n  font-weight: normal;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.member-detail-image-container img {\n  width: 100%;\n}\n.member-detail-image-container a {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.member-detail-image-container a .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.member-detail-image-container .btn-download-container {\n  margin-top: 5px;\n  margin-bottom: 5px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.member-detail-image-container .img-upload {\n  display: flex;\n  justify-content: space-around;\n  align-items: center;\n  margin: 20px 10px;\n}\n.member-detail-image-container .img-upload .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\nbutton.btn-download-img {\n  margin-top: 10px;\n  background-color: #3498db;\n  color: #fff;\n  padding: 5px 15px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 4px;\n  border: 0;\n  cursor: pointer;\n}\n.edit_button {\n  position: absolute;\n  top: 7px;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n.edit_button i {\n  font-weight: bold;\n}\n.btn-right {\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-left: 10px;\n}\n.error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 26px;\n  padding-top: 10px;\n}\n.error-message .text-message {\n  background: #E74C3C;\n  padding: 20px;\n  margin: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZm9ybS1tZW1iZXIvbGlzdC1vcmRlci9lZGl0L0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcZm9ybS1tZW1iZXJcXGxpc3Qtb3JkZXJcXGVkaXRcXGxpc3Qtb3JkZXIuZWRpdC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZm9ybS1tZW1iZXIvbGlzdC1vcmRlci9lZGl0L2xpc3Qtb3JkZXIuZWRpdC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLGtCQUFBO0FDQVI7QURDUTtFQUVJLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSw2QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEseUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0FDRFo7QURFWTtFQUNJLGlCQUFBO0FDQWhCO0FER1E7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUtBLFdBQUE7QUNOWjtBREdZO0VBQ0ksaUJBQUE7QUNEaEI7QURLUTtFQUNJLHlCQUFBO0FDSFo7QURNSTtFQUNJLGFBQUE7QUNKUjtBRE1RO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ0paO0FEUUk7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQ05SO0FEVVE7RUFDSSxnQkFBQTtBQ1JaO0FEVVE7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDUlo7QURVUTtFQUNJLGdCQUFBO0FDUlo7QURTWTtFQUNJLGVBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQ1BoQjtBRFNZO0VBQ0kseUJBQUE7QUNQaEI7QURTWTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUNQaEI7QURTWTtFQUNJLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUNQaEI7QURRZ0I7RUFDSSxlQUFBO0VBQ0EsV0FBQTtBQ05wQjtBRFlRO0VBQ0ksc0JBQUE7QUNWWjtBRFdZO0VBQ0ksWUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBQ0EsMEJBQUE7QUNUaEI7QURnQkE7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDYko7QURjSTtFQUNJLGFBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7QUNaUjtBRGNRO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7S0FBQSxtQkFBQTtBQ1paO0FEZ0JJO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUNkUjtBRGVRO0VBQ0ksWUFBQTtFQUVBLFdBQUE7QUNkWjtBRGVZO0VBRUksUUFBQTtBQ2RoQjtBRHFCQTtFQUNJLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxlQUFBO0FDbEJKO0FEcUJBO0VBQ0ksd0JBQUE7QUNsQko7QURxQkE7RUFDSSxhQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUNsQko7QURxQkE7RUFDSSxnQkFBQTtBQ2xCSjtBRHFCQTtFQUNJLGdCQUFBO0VBRUEsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FDbEJKO0FEdUJBO0VBQ0ksZ0JBQUE7RUFFQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0FDcEJKO0FEc0JBO0VBQ0ksbUJBQUE7RUFFQSxhQUFBO0FDcEJKO0FEdUJBO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ3BCSjtBRHFCSTtFQUNJLGFBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7QUNuQlI7QURxQlE7RUFDSSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtLQUFBLG1CQUFBO0FDbkJaO0FEdUJJO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUNyQlI7QURzQlE7RUFDSSxZQUFBO0VBRUEsV0FBQTtBQ3JCWjtBRHNCWTtFQUVJLFFBQUE7QUNyQmhCO0FENEJBO0VBQ0kseUJBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGVBQUE7QUN6Qko7QUQ0QkE7RUFDSSxXQUFBO0FDekJKO0FENEJBO0VBQ0ksd0JBQUE7QUN6Qko7QUQ0QkE7RUFDSSxhQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUN6Qko7QUQ0QkE7RUFDSSxnQkFBQTtBQ3pCSjtBRDRCQTtFQUNJLGdCQUFBO0VBQ0EsNEJBQUE7RUFDQSw2QkFBQTtBQ3pCSjtBRDRCQTtFQUNJLGFBQUE7QUN6Qko7QUQ0QkE7RUFDSSxVQUFBO0FDekJKO0FENEJBO0VBQ0ksbUJBQUE7QUN6Qko7QUQ0QkE7RUFDSSxrQkFBQTtBQ3pCSjtBRDRCQTtFQUNJLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUN6Qko7QUQwQkk7RUFDSSxXQUFBO0FDeEJSO0FEMEJJO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUN4QlI7QUR5QlE7RUFDSSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtLQUFBLG1CQUFBO0FDdkJaO0FEMEJJO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUN4QlI7QUQwQkk7RUFDSSxhQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDeEJSO0FEMEJRO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7S0FBQSxtQkFBQTtBQ3hCWjtBRDZCQTtFQUNJLGdCQUFBO0VBQ0EseUJBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGVBQUE7QUMxQko7QUQ2QkE7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFFQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFJQSxXQUFBO0FDOUJKO0FEMkJJO0VBQ0ksaUJBQUE7QUN6QlI7QUQ2QkE7RUFDSSxzQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDMUJKO0FENkJBO0VBQ0ksaUJBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUMxQko7QUQ0Qkk7RUFDSSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0FDMUJSIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZm9ybS1tZW1iZXIvbGlzdC1vcmRlci9lZGl0L2xpc3Qtb3JkZXIuZWRpdC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jYXJkLWRldGFpbHtcclxuICAgID4uY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIC5iYWNrX2J1dHRvbntcclxuICAgICAgICAgICAgLy8gcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAtNHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogLTEwcHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAuc2F2ZV9idXR0b257XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgdG9wOiA3cHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByaWdodDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnNhdmVfYnV0dG9uLnRydWV7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNlNjdlMjI7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmNhcmQtY29udGVudHtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgXHJcbiAgICAgICAgPi5jb2wtbWQtMTJ7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgXHJcbiAgICBoMXtcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgIH1cclxuICAgIC5tZW1iZXItZGV0YWlse1xyXG4gICAgICAgXHJcbiAgICAgICAgbGFiZWx7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxhYmVsK2RpdntcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6bm9ybWE7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgY29sb3I6ICM2NjY7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5pbWFnZXtcclxuICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgICAgPmRpdntcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAzMy4zMzMzJTtcclxuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgICAgICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC5jYXJkLWhlYWRlcntcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaDN7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICAgICAgICAgICAgaW1ne1xyXG4gICAgICAgICAgICAgICAgICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgIFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuY2FyZC1oZWFkZXJ7IFxyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoMntcclxuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46IDBweDtcclxuICAgICAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIge1xyXG4gICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgIGZvbnQtd2VpZ2h0OiBub3JtYTtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBjb2xvcjogIzY2NjtcclxuICAgIC5pbWctdXBsb2FkIHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luOjIwcHggMTBweDtcclxuXHJcbiAgICAgICAgLm1lbWJlci1kZXRhaWwtaW1hZ2Uge1xyXG4gICAgICAgICAgICB3aWR0aDoyMDBweDtcclxuICAgICAgICAgICAgbWF4LWhlaWdodDogMjAwcHg7XHJcbiAgICAgICAgICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5mb3JtLWdyb3VwLnVwbG9hZGVkIHtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAubG9hZGluZyB7XHJcbiAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgLy8gYmFja2dyb3VuZC1jb2xvcjogIzAwMDtcclxuICAgICAgICAgICAgd2lkdGg6MTAwJTtcclxuICAgICAgICAgICAgLnNrLWZhZGluZy1jaXJjbGUge1xyXG4gICAgICAgICAgICAgICAgLy8gYmFja2dyb3VuZC1jb2xvcjpyZWQ7XHJcbiAgICAgICAgICAgICAgICB0b3A6IDUwJTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmJ1dHRvbi5idG5fdXBsb2FkLCBidXR0b24uYWRkLW1vcmUtaW1hZ2UsIGJ1dHRvbi5idG5fZGVsZXRlIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIHBhZGRpbmc6IDVweCAxNXB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICAgIGJvcmRlcjowO1xyXG4gICAgY3Vyc29yOnBvaW50ZXI7XHJcbn1cclxuXHJcbmJ1dHRvbi5idG5fZGVsZXRle1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogc2FsbW9uO1xyXG59XHJcblxyXG4uYWRkLW1vcmUtY29udGFpbmVyIHtcclxuICAgIGRpc3BsYXk6ZmxleDtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBhbGlnbi1pdGVtczpjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG5cclxuLmxhYmVsLWJvbGQge1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxufVxyXG5cclxuLnJvdW5kZWQtYnRuIHtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDcwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDQwcHg7XHJcbiAgICBwYWRkaW5nOiAwIDQwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjNTZhNGZmO1xyXG4gICAgLy8gbWFyZ2luLWxlZnQ6IDQwJTtcclxuICAgIC8vIG1hcmdpbi1yaWdodDogNTAlO1xyXG59XHJcblxyXG4ucm91bmRlZC1idG4tZGFuZ2VyIHtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDcwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDQwcHg7XHJcbiAgICBwYWRkaW5nOiAwIDQwcHg7XHJcbiAgICBjb2xvcjogIzU1NTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG59XHJcbi5yb3VuZGVkLWJ0bjpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kOiBkYXJrZW4oIzU2QTRGRiwgMTUlKTtcclxuICAgIC8vIGJvcmRlcjogMnB4IHNvbGlkIGJsdWU7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG59XHJcblxyXG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIge1xyXG4gICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgIGZvbnQtd2VpZ2h0OiBub3JtYTtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBjb2xvcjogIzY2NjtcclxuICAgIC5pbWctdXBsb2FkIHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luOjIwcHggMTBweDtcclxuXHJcbiAgICAgICAgLm1lbWJlci1kZXRhaWwtaW1hZ2Uge1xyXG4gICAgICAgICAgICB3aWR0aDoyMDBweDtcclxuICAgICAgICAgICAgbWF4LWhlaWdodDogMjAwcHg7XHJcbiAgICAgICAgICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5mb3JtLWdyb3VwLnVwbG9hZGVkIHtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAubG9hZGluZyB7XHJcbiAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgLy8gYmFja2dyb3VuZC1jb2xvcjogIzAwMDtcclxuICAgICAgICAgICAgd2lkdGg6MTAwJTtcclxuICAgICAgICAgICAgLnNrLWZhZGluZy1jaXJjbGUge1xyXG4gICAgICAgICAgICAgICAgLy8gYmFja2dyb3VuZC1jb2xvcjpyZWQ7XHJcbiAgICAgICAgICAgICAgICB0b3A6IDUwJTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmJ1dHRvbi5idG5fdXBsb2FkLCBidXR0b24uYWRkLW1vcmUtaW1hZ2UsIGJ1dHRvbi5idG5fZGVsZXRlIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIHBhZGRpbmc6IDVweCAxNXB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICAgIGJvcmRlcjowO1xyXG4gICAgY3Vyc29yOnBvaW50ZXI7XHJcbn1cclxuXHJcbmJ1dHRvbi5idG4tY2hhbmdlLXBhc3N3b3JkIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5idXR0b24uYnRuX2RlbGV0ZXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHNhbG1vbjtcclxufVxyXG5cclxuLmFkZC1tb3JlLWNvbnRhaW5lciB7XHJcbiAgICBkaXNwbGF5OmZsZXg7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgYWxpZ24taXRlbXM6Y2VudGVyO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuXHJcbi5sYWJlbC1ib2xkIHtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbn1cclxuXHJcbi5lcnJvci1tc2ctdXBsb2FkIHtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDBweCAhaW1wb3J0YW50O1xyXG4gICAgcGFkZGluZy1yaWdodDogMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5jb250YWluZXItdHdvLWJ1dHRvbiB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG59XHJcblxyXG4uZmlsZS1zZWxlY3RlZCB7XHJcbiAgICBjb2xvcjogcmVkO1xyXG59XHJcblxyXG4uYnRuLWFkZC1idWxrIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbn1cclxuXHJcbi5idG4tdXBsb2FkIHtcclxuICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxufVxyXG5cclxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIHtcclxuICAgIGJhY2tncm91bmQ6ICNmMGYwZjA7XHJcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgbWluLWhlaWdodDogMjdweDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGNvbG9yOiAjNjY2O1xyXG4gICAgaW1nIHtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgIH1cclxuICAgIGEge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAubWVtYmVyLWRldGFpbC1pbWFnZSB7XHJcbiAgICAgICAgICAgIHdpZHRoOjIwMHB4O1xyXG4gICAgICAgICAgICBtYXgtaGVpZ2h0OiAyMDBweDtcclxuICAgICAgICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuYnRuLWRvd25sb2FkLWNvbnRhaW5lciB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDo1cHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTo1cHg7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgfVxyXG4gICAgLmltZy11cGxvYWQge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW46MjBweCAxMHB4O1xyXG5cclxuICAgICAgICAubWVtYmVyLWRldGFpbC1pbWFnZSB7XHJcbiAgICAgICAgICAgIHdpZHRoOjIwMHB4O1xyXG4gICAgICAgICAgICBtYXgtaGVpZ2h0OiAyMDBweDtcclxuICAgICAgICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbmJ1dHRvbi5idG4tZG93bmxvYWQtaW1nIHtcclxuICAgIG1hcmdpbi10b3A6MTBweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIHBhZGRpbmc6IDVweCAxNXB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICAgIGJvcmRlcjowO1xyXG4gICAgY3Vyc29yOnBvaW50ZXI7XHJcbn1cclxuXHJcbi5lZGl0X2J1dHRvbntcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogN3B4O1xyXG4gICAgLy8gYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgaXtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIH1cclxuICAgIHJpZ2h0OiAxMHB4O1xyXG59XHJcbi5idG4tcmlnaHQge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbn1cclxuXHJcbi5lcnJvci1tZXNzYWdlIHtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgaGVpZ2h0OjEwMHZoO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOjI2cHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcclxuXHJcbiAgICAudGV4dC1tZXNzYWdlIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjRTc0QzNDO1xyXG4gICAgICAgIHBhZGRpbmc6IDIwcHg7XHJcbiAgICAgICAgbWFyZ2luOiAwIDEwcHg7XHJcbiAgICB9XHJcbn0iLCIuY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIHtcbiAgbWFyZ2luLXRvcDogLTRweDtcbiAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgY29sb3I6ICM1NTU7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuc2F2ZV9idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogN3B4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHJpZ2h0OiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLnNhdmVfYnV0dG9uLnRydWUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTY3ZTIyO1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQge1xuICBwYWRkaW5nOiAxNXB4O1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQgPiAuY29sLW1kLTEyIHtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDBweDtcbn1cbi5jYXJkLWRldGFpbCBoMSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjNTU1O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBsYWJlbCB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwgKyBkaXYge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWE7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlIHtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2UgPiBkaXYge1xuICB3aWR0aDogMzMuMzMzMyU7XG4gIHBhZGRpbmc6IDVweDtcbiAgZmxvYXQ6IGxlZnQ7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlIGgzIHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIGNvbG9yOiAjNTU1O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSAuY2FyZC1jb250ZW50IHtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogYXV0bztcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2UgLmNhcmQtY29udGVudCBpbWcge1xuICBtYXgtd2lkdGg6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIGgyIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGxpbmUtaGVpZ2h0OiB1bnNldDtcbiAgaGVpZ2h0OiB1bnNldDtcbiAgbWFyZ2luOiAwcHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuXG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWE7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmltZy11cGxvYWQge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luOiAyMHB4IDEwcHg7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmltZy11cGxvYWQgLm1lbWJlci1kZXRhaWwtaW1hZ2Uge1xuICB3aWR0aDogMjAwcHg7XG4gIG1heC1oZWlnaHQ6IDIwMHB4O1xuICBvYmplY3QtZml0OiBjb250YWluO1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5mb3JtLWdyb3VwLnVwbG9hZGVkIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuZm9ybS1ncm91cC51cGxvYWRlZCAubG9hZGluZyB7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLmxvYWRpbmcgLnNrLWZhZGluZy1jaXJjbGUge1xuICB0b3A6IDUwJTtcbn1cblxuYnV0dG9uLmJ0bl91cGxvYWQsIGJ1dHRvbi5hZGQtbW9yZS1pbWFnZSwgYnV0dG9uLmJ0bl9kZWxldGUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogI2ZmZjtcbiAgcGFkZGluZzogNXB4IDE1cHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIGJvcmRlcjogMDtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG5idXR0b24uYnRuX2RlbGV0ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6IHNhbG1vbjtcbn1cblxuLmFkZC1tb3JlLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHdpZHRoOiAxMDAlO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmxhYmVsLWJvbGQge1xuICBmb250LXdlaWdodDogNjAwO1xufVxuXG4ucm91bmRlZC1idG4ge1xuICBtYXJnaW4tdG9wOiAyMHB4O1xuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDcwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDcwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBsaW5lLWhlaWdodDogNDBweDtcbiAgcGFkZGluZzogMCA0MHB4O1xuICBiYWNrZ3JvdW5kOiAjNTZhNGZmO1xufVxuXG4ucm91bmRlZC1idG4tZGFuZ2VyIHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA3MHB4O1xuICBib3JkZXItcmFkaXVzOiA3MHB4O1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XG4gIHBhZGRpbmc6IDAgNDBweDtcbiAgY29sb3I6ICM1NTU7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuXG4ucm91bmRlZC1idG46aG92ZXIge1xuICBiYWNrZ3JvdW5kOiAjMGE3YmZmO1xuICBvdXRsaW5lOiBub25lO1xufVxuXG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWE7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmltZy11cGxvYWQge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luOiAyMHB4IDEwcHg7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmltZy11cGxvYWQgLm1lbWJlci1kZXRhaWwtaW1hZ2Uge1xuICB3aWR0aDogMjAwcHg7XG4gIG1heC1oZWlnaHQ6IDIwMHB4O1xuICBvYmplY3QtZml0OiBjb250YWluO1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5mb3JtLWdyb3VwLnVwbG9hZGVkIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuZm9ybS1ncm91cC51cGxvYWRlZCAubG9hZGluZyB7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLmxvYWRpbmcgLnNrLWZhZGluZy1jaXJjbGUge1xuICB0b3A6IDUwJTtcbn1cblxuYnV0dG9uLmJ0bl91cGxvYWQsIGJ1dHRvbi5hZGQtbW9yZS1pbWFnZSwgYnV0dG9uLmJ0bl9kZWxldGUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogI2ZmZjtcbiAgcGFkZGluZzogNXB4IDE1cHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIGJvcmRlcjogMDtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG5idXR0b24uYnRuLWNoYW5nZS1wYXNzd29yZCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG5idXR0b24uYnRuX2RlbGV0ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6IHNhbG1vbjtcbn1cblxuLmFkZC1tb3JlLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHdpZHRoOiAxMDAlO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmxhYmVsLWJvbGQge1xuICBmb250LXdlaWdodDogNjAwO1xufVxuXG4uZXJyb3ItbXNnLXVwbG9hZCB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIHBhZGRpbmctbGVmdDogMHB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctcmlnaHQ6IDBweCAhaW1wb3J0YW50O1xufVxuXG4uY29udGFpbmVyLXR3by1idXR0b24ge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuXG4uZmlsZS1zZWxlY3RlZCB7XG4gIGNvbG9yOiByZWQ7XG59XG5cbi5idG4tYWRkLWJ1bGsge1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuXG4uYnRuLXVwbG9hZCB7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cblxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIHtcbiAgYmFja2dyb3VuZDogI2YwZjBmMDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgbWluLWhlaWdodDogMjdweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzY2Njtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciBpbWcge1xuICB3aWR0aDogMTAwJTtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciBhIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgYSAubWVtYmVyLWRldGFpbC1pbWFnZSB7XG4gIHdpZHRoOiAyMDBweDtcbiAgbWF4LWhlaWdodDogMjAwcHg7XG4gIG9iamVjdC1maXQ6IGNvbnRhaW47XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmJ0bi1kb3dubG9hZC1jb250YWluZXIge1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmltZy11cGxvYWQge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luOiAyMHB4IDEwcHg7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmltZy11cGxvYWQgLm1lbWJlci1kZXRhaWwtaW1hZ2Uge1xuICB3aWR0aDogMjAwcHg7XG4gIG1heC1oZWlnaHQ6IDIwMHB4O1xuICBvYmplY3QtZml0OiBjb250YWluO1xufVxuXG5idXR0b24uYnRuLWRvd25sb2FkLWltZyB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiAjZmZmO1xuICBwYWRkaW5nOiA1cHggMTVweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgYm9yZGVyOiAwO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5lZGl0X2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA3cHg7XG4gIGNvbG9yOiAjZmZmO1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcmlnaHQ6IDEwcHg7XG59XG4uZWRpdF9idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uYnRuLXJpZ2h0IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcbiAgY29sb3I6ICNmZmY7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cblxuLmVycm9yLW1lc3NhZ2Uge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDI2cHg7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xufVxuLmVycm9yLW1lc3NhZ2UgLnRleHQtbWVzc2FnZSB7XG4gIGJhY2tncm91bmQ6ICNFNzRDM0M7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIG1hcmdpbjogMCAxMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/form-member/list-order/edit/list-order.edit.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/layout/modules/form-member/list-order/edit/list-order.edit.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: EditListOrderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditListOrderComponent", function() { return EditListOrderComponent; });
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
// import { AdditonalDocumentComponent } from '../list-order.component';






// import { basename } from 'path';
// import { ProductService } from '../../../../services/product/product.service';
var EditListOrderComponent = /** @class */ (function () {
    function EditListOrderComponent(memberService, orderHistoryService, productService, zone) {
        this.memberService = memberService;
        this.orderHistoryService = orderHistoryService;
        this.productService = productService;
        this.zone = zone;
        this.errorLabel = false;
        this.new_form = {};
        this.loading = false;
        this.errorFile = false;
        this.cancel = false;
        // loadingKTPPemilik: boolean = false;
        // loadingNPWPPemilik: boolean = false;
        // loadingKTPPenerima: boolean = false;
        // loadingNPWPPenerima: boolean = false;
        this.loadingBAST = [];
        this.loadingSuratKuasa = [];
        this.loadingFotoBAST = false;
        this.loadingFotoSuratKuasa = false;
        this.loadingFotoOthers = false;
        this.loadingFotoReceipt = false;
        this.loadingOthers = [];
        this.loadingReceipt = [];
    }
    EditListOrderComponent.prototype.ngOnInit = function () {
        this.memberDetail = this.detail;
        this.order = this.detailOrder;
        this.firstLoad();
    };
    EditListOrderComponent.prototype.isArray = function (curVar) {
        return Array.isArray(curVar) ? true : false;
    };
    EditListOrderComponent.prototype.assignArrayBoolean = function (curVar, varCont) {
        if (this.isArray(curVar)) {
            curVar.forEach(function (element) {
                varCont.push(false);
            });
        }
    };
    EditListOrderComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.loadingOthers = [];
                this.loadingReceipt = [];
                if (this.order.additional_info && this.order.additional_info.others && this.order.additional_info.others.length > 0) {
                    this.order.additional_info.others.forEach(function (item) { return _this.loadingOthers.push(false); });
                }
                if (this.order.additional_info && this.order.additional_info.receipt && this.order.additional_info.receipt.length > 0) {
                    this.order.additional_info.receipt.forEach(function (item) { return _this.loadingReceipt.push(false); });
                }
                return [2 /*return*/];
            });
        });
    };
    EditListOrderComponent.prototype.backToDetail = function () {
        this.back[0][this.back[1]]();
    };
    EditListOrderComponent.prototype.validURL = function (str) {
        return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(str);
    };
    EditListOrderComponent.prototype.openUpload = function (type, index) {
        console.log("index upload", index, type);
        var upFile = index || index == 0 ? document.getElementById(type + index) : document.getElementById(type);
        console.warn("upfile", upFile);
        upFile.click();
    };
    EditListOrderComponent.prototype.saveThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var arrAddBAST, arrAddSK, arrDeleteBAST, arrDeleteSK, i, found, j, i, i, found, j, i, payloadRemove, payloadAdd, e_1, message;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        arrAddBAST = [];
                        arrAddSK = [];
                        arrDeleteBAST = [];
                        arrDeleteSK = [];
                        //validate BAST
                        for (i = 0; i < this.detail.additional_info.bast.length; i++) {
                            found = false;
                            for (j = 0; j < this.updateSingle.bast.length; j++) {
                                if (this.updateSingle.bast[j].id == this.detail.additional_info.bast[i].id) {
                                    found = true;
                                    break;
                                }
                            }
                            if (!found) {
                                arrDeleteBAST.push(this.detail.additional_info.bast[i].id);
                            }
                        }
                        for (i = 0; i < this.updateSingle.bast.length; i++) {
                            if (this.updateSingle.bast[i].id == null && this.updateSingle.bast[i].pic_file_name) {
                                arrAddBAST.push(this.updateSingle.bast[i].pic_file_name);
                            }
                        }
                        // Validate Surat Kuasa
                        for (i = 0; i < this.detail.additional_info.surat_kuasa.length; i++) {
                            found = false;
                            for (j = 0; j < this.updateSingle.surat_kuasa.length; j++) {
                                if (this.updateSingle.surat_kuasa[j].id == this.detail.additional_info.surat_kuasa[i].id) {
                                    found = true;
                                    break;
                                }
                            }
                            if (!found) {
                                arrDeleteSK.push(this.detail.additional_info.surat_kuasa[i].id);
                            }
                        }
                        for (i = 0; i < this.updateSingle.surat_kuasa.length; i++) {
                            if (this.updateSingle.surat_kuasa[i].id == null && this.updateSingle.surat_kuasa[i].pic_file_name) {
                                arrAddSK.push(this.updateSingle.surat_kuasa[i].pic_file_name);
                            }
                        }
                        payloadRemove = {
                            username: this.detail.username,
                            bast: arrDeleteBAST,
                            surat_kuasa: arrDeleteSK
                        };
                        payloadAdd = {
                            username: this.detail.username,
                            bast: arrAddBAST,
                            surat_kuasa: arrAddSK
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 6, , 7]);
                        if (!(payloadAdd.bast.length > 0 || payloadAdd.surat_kuasa.length > 0)) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.memberService.addBastSk(payloadAdd)];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        if (!(payloadRemove.bast.length > 0 || payloadRemove.surat_kuasa.length > 0)) return [3 /*break*/, 5];
                        return [4 /*yield*/, this.memberService.deleteBastSk(payloadRemove)];
                    case 4:
                        _a.sent();
                        _a.label = 5;
                    case 5:
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                            title: 'Success',
                            text: 'Edit data pelanggan berhasil',
                            icon: 'success',
                            confirmButtonText: 'Ok',
                        }).then(function (result) {
                            if (result.isConfirmed) {
                                _this.backToDetail();
                            }
                        });
                        return [3 /*break*/, 7];
                    case 6:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 7];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    EditListOrderComponent.prototype.checkFileType = function (text) {
        var splitFileName = text.split('.');
        var extensFile = splitFileName[splitFileName.length - 1];
        var extens = extensFile.toLowerCase();
        return extens == 'pdf' ? 'document' : 'image';
    };
    // async oldsaveThis() {
    //   this.new_form = {
    //     username: this.detail.username
    //   }
    //   if(this.updateSingle.bast && this.updateSingle.bast.length > 0){
    //     let currentBast = this.updateSingle.bast.filter((element : any) => {
    //       if(element.url && this.validURL(element.url)){
    //         return element;
    //       }
    //     });
    //     this.updateSingle.bast = currentBast;
    //   }
    //   if(this.updateSingle.surat_kuasa && this.updateSingle.surat_kuasa.length > 0){
    //     let currentSuratKuasa = this.updateSingle.surat_kuasa.filter((element:any) => {
    //       if(element.url && this.validURL(element.url)){
    //         return element;
    //       }
    //     });
    //     this.updateSingle.surat_kuasa = currentSuratKuasa;
    //   }
    //   for(let prop in this.updateSingle){
    //     if(this.updateSingle[prop] != this.detail[prop] && this.updateSingle[prop] != ""){
    //         this.new_form[prop] = this.updateSingle[prop];
    //     }
    //     if(prop == 'bast' || prop == 'surat_kuasa'){
    //       this.new_form[prop] = this.updateSingle[prop];
    //     }
    //   }
    //   console.warn("new form", this.new_form);
    //   console.warn("updateSingle", this.updateSingle);
    //   console.warn("detail", this.detail);
    //   try {
    //     await this.memberService.updateBASTandSK(new_form);
    //     this.detail = JSON.parse(JSON.stringify(this.updateSingle));
    //     //  Swal.fire('Success', 'Edit data pelanggan berhasil', 'success');
    //     Swal.fire({
    //       title: 'Success',
    //       text: 'Edit data pelanggan berhasil',
    //       icon: 'success',
    //       confirmButtonText: 'Ok',
    //     }).then((result) => {
    //       if(result.isConfirmed){
    //         this.backToDetail();
    //       }
    //     });
    //   } catch (e) {
    //     this.errorLabel = ((<Error>e).message);//conversion to Error type
    //     alert("Error");
    //   }
    // }
    EditListOrderComponent.prototype.onFileSelected = function (event, typeImg, idx) {
        return __awaiter(this, void 0, void 0, function () {
            var fileValue, payload, option, id, receipt_id, reader, fileMaxSize_1, result, e_2, message;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        payload = null;
                        option = null;
                        id = null;
                        receipt_id = null;
                        if (typeImg == 'bast') {
                            this.loadingFotoBAST = true;
                            fileValue = 'bast';
                        }
                        else if (typeImg == 'suratKuasa') {
                            this.loadingFotoSuratKuasa = true;
                            fileValue = 'surat_kuasa';
                        }
                        else if (typeImg == 'others') {
                            if (idx || idx == 0) {
                                this.loadingOthers[idx] = true;
                                id = this.order.additional_info.others[idx].id;
                                option = 'edit';
                            }
                            else {
                                this.loadingFotoOthers = true;
                            }
                            fileValue = 'others';
                        }
                        else if (typeImg == 'receipt') {
                            if (idx || idx == 0) {
                                this.loadingReceipt[idx] = true;
                                receipt_id = this.order.additional_info.receipt[idx].receipt_id;
                                option = 'edit';
                            }
                            else {
                                this.loadingFotoReceipt = true;
                            }
                            fileValue = 'receipt';
                        }
                        reader = new FileReader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 5, , 6]);
                        this.errorFile = false;
                        fileMaxSize_1 = 2064000;
                        Array.from(event.target.files).forEach(function (file) {
                            if (file.size > fileMaxSize_1) {
                                _this.errorFile = 'Maximum File Upload is 2MB';
                            }
                        });
                        this.selectedFile = event.target.files[0];
                        reader.onload = function (event) {
                            _this.selFile = event.target.result;
                        };
                        if (event.target.files[0]) {
                            reader.readAsDataURL(event.target.files[0]);
                        }
                        if (typeImg == 'receipt') {
                            payload = {
                                type: 'application/form-data',
                                option: option,
                                receipt_id: receipt_id
                            };
                        }
                        else {
                            payload = {
                                type: 'application/form-data',
                                option: option,
                                id: id
                            };
                        }
                        // let payload = {
                        //   type : 'application/form-data',
                        //   option,
                        //   id
                        // }
                        console.warn("payload edit", payload);
                        if (!this.selectedFile) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.orderHistoryService.uploadAdditionalImage(this.selectedFile, this, payload, this.order.order_id, fileValue)];
                    case 2:
                        result = _a.sent();
                        if (typeImg == 'bast') {
                            this.loadingFotoBAST = false;
                        }
                        else if (typeImg == 'suratKuasa') {
                            this.loadingFotoSuratKuasa = false;
                        }
                        else if (typeImg == 'others') {
                            if (idx || idx == 0) {
                                this.loadingOthers[idx] = false;
                            }
                            else {
                                this.loadingFotoOthers = false;
                            }
                        }
                        else if (typeImg == 'receipt') {
                            if (idx || idx == 0) {
                                this.loadingReceipt[idx] = false;
                            }
                            else {
                                this.loadingFotoReceipt = false;
                            }
                        }
                        if (!result) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.getCurrentOrderHistory()];
                    case 3:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 4: return [3 /*break*/, 6];
                    case 5:
                        e_2 = _a.sent();
                        this.errorLabel = e_2.message; //conversion to Error type
                        console.log;
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    EditListOrderComponent.prototype.getCurrentOrderHistory = function () {
        return __awaiter(this, void 0, void 0, function () {
            var query, result, e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        query = {
                            search: {
                                order_id: this.order.order_id
                            },
                            limit_per_page: 1,
                            current_page: 1
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.orderHistoryService.searchAllReportSalesOrder(query)];
                    case 2:
                        result = _a.sent();
                        //  this.loadingOthers.forEach((element,idx) => {
                        //    document.getElementById('others'+idx)['value'] ='';
                        //  });
                        this.order = result.values[0];
                        document.getElementById("bast")['value'] = '';
                        document.getElementById("suratKuasa")['value'] = '';
                        document.getElementById("others")['value'] = '';
                        document.getElementById("receipt")['value'] = '';
                        this.firstLoad();
                        return [3 /*break*/, 4];
                    case 3:
                        e_3 = _a.sent();
                        this.errorLabel = (e_3.message); //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    EditListOrderComponent.prototype.addMoreUpload = function (typeImg) {
        var dummyImg = {
            created_by: null,
            created_date: null,
            id: null,
            pic_file_name: null,
            updated_by: null,
            updated_date: null
        };
        if (typeImg == 'bast') {
            this.updateSingle.bast.push(dummyImg);
            this.loadingBAST.push(false);
        }
        else if (typeImg == 'suratKuasa') {
            this.updateSingle.surat_kuasa.push(dummyImg);
            this.loadingSuratKuasa.push(false);
        }
    };
    EditListOrderComponent.prototype.deleteImage = function (type, id, url, idx) {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: "Delete",
            text: "Apakah anda yakin ingin menghapus gambar ini ?",
            imageUrl: url,
            imageHeight: 250,
            imageAlt: type,
            showCancelButton: true,
            focusConfirm: false,
        }).then(function (res) { return __awaiter(_this, void 0, void 0, function () {
            var payload, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!res.isConfirmed) return [3 /*break*/, 3];
                        if (!(type == "others")) return [3 /*break*/, 3];
                        this.loadingOthers[idx] = true;
                        payload = {
                            type: 'application/form-data',
                            option: 'delete',
                            id: id
                        };
                        return [4 /*yield*/, this.orderHistoryService.uploadAdditionalImage(null, this, payload, this.order.order_id, type)];
                    case 1:
                        result = _a.sent();
                        if (!result) return [3 /*break*/, 3];
                        // Swal.fire({
                        //   title: 'Success',
                        //   text: 'Hapus gambar berhasil',
                        //   icon: 'success',
                        //   confirmButtonText: 'Ok',
                        // }).then(async (result) => {
                        //   if(result.isConfirmed){
                        //   }
                        // });
                        return [4 /*yield*/, this.getCurrentOrderHistory()];
                    case 2:
                        // Swal.fire({
                        //   title: 'Success',
                        //   text: 'Hapus gambar berhasil',
                        //   icon: 'success',
                        //   confirmButtonText: 'Ok',
                        // }).then(async (result) => {
                        //   if(result.isConfirmed){
                        //   }
                        // });
                        _a.sent();
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        }); });
    };
    EditListOrderComponent.prototype.deleteReceipt = function (type, receipt_id, url, idx) {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: "Delete",
            text: "Apakah anda yakin ingin menghapus gambar ini ?",
            imageUrl: url,
            imageHeight: 250,
            imageAlt: type,
            showCancelButton: true,
            focusConfirm: false,
        }).then(function (res) { return __awaiter(_this, void 0, void 0, function () {
            var payload, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!res.isConfirmed) return [3 /*break*/, 3];
                        if (!(type == "receipt")) return [3 /*break*/, 3];
                        this.loadingReceipt[idx] = true;
                        console.warn("receipt id", receipt_id);
                        payload = {
                            type: 'application/form-data',
                            option: 'delete',
                            receipt_id: receipt_id
                        };
                        return [4 /*yield*/, this.orderHistoryService.uploadAdditionalImage(null, this, payload, this.order.order_id, type)];
                    case 1:
                        result = _a.sent();
                        if (!result) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.getCurrentOrderHistory()];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        }); });
    };
    EditListOrderComponent.prototype.convertDate = function (time) {
        var date = new Date(time);
        var dateString = date.toString();
        var retDate = dateString.split("GMT");
        return retDate[0];
    };
    EditListOrderComponent.prototype.checkLoading = function (fieldName, fieldValue) {
        var valueImage;
        switch (fieldName) {
            case 'bast':
                valueImage = 'bast';
                this.loadingBAST = fieldValue;
                break;
            case 'suratKuasa':
                valueImage = 'surat_kuasa';
                this.loadingSuratKuasa = fieldValue;
        }
        return valueImage;
    };
    EditListOrderComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"] },
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_5__["OrderhistoryService"] },
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__["ProductService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], EditListOrderComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], EditListOrderComponent.prototype, "back", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], EditListOrderComponent.prototype, "detailOrder", void 0);
    EditListOrderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list-order-edit',
            template: __webpack_require__(/*! raw-loader!./list-order.edit.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/form-member/list-order/edit/list-order.edit.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_2__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./list-order.edit.component.scss */ "./src/app/layout/modules/form-member/list-order/edit/list-order.edit.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"],
            _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_5__["OrderhistoryService"],
            _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__["ProductService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]])
    ], EditListOrderComponent);
    return EditListOrderComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/form-member/list-order/list-order.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/layout/modules/form-member/list-order/list-order.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n}\n.card-detail > .card-header .back_button {\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 7px;\n  background-color: #3498db;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n.card-detail > .card-header .save_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button.true {\n  background-color: #e67e22;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .member-detail .image {\n  overflow: hidden;\n}\n.card-detail .member-detail .image > div {\n  width: 33.3333%;\n  padding: 5px;\n  float: left;\n}\n.card-detail .member-detail .image .card-header {\n  background-color: #f0f0f0;\n}\n.card-detail .member-detail .image h3 {\n  font-size: 12px;\n  margin-bottom: 0px;\n  color: #555;\n}\n.card-detail .member-detail .image .card-content {\n  overflow: hidden;\n  width: 100%;\n  height: auto;\n}\n.card-detail .member-detail .image .card-content img {\n  max-width: 100%;\n  width: 100%;\n}\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n.rounded-btn {\n  margin-top: 20px;\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  background: #56a4ff;\n}\n.rounded-btn-danger {\n  margin-top: 20px;\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  color: #555;\n  margin-left: 10px;\n}\n.rounded-btn:hover {\n  background: #0a7bff;\n  outline: none;\n}\n.member-detail-image-container {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.member-detail-image-container .img-upload {\n  display: flex;\n  justify-content: space-around;\n  align-items: center;\n  margin: 20px 10px;\n}\n.member-detail-image-container .img-upload .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.member-detail-image-container .form-group.uploaded {\n  position: relative;\n  margin-bottom: 0px;\n  height: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading {\n  height: 100%;\n  width: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading .sk-fading-circle {\n  top: 50%;\n}\nbutton.btn_upload, button.add-more-image, button.btn_delete {\n  background-color: #3498db;\n  color: #fff;\n  padding: 5px 15px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 4px;\n  border: 0;\n  cursor: pointer;\n}\nbutton.btn-change-password {\n  width: 100%;\n}\nbutton.btn_delete {\n  background-color: salmon;\n}\n.add-more-container {\n  display: flex;\n  width: 100%;\n  align-items: center;\n  justify-content: center;\n}\n.label-bold {\n  font-weight: 600;\n}\n.error-msg-upload {\n  margin-top: 10px;\n  padding-left: 0px !important;\n  padding-right: 0px !important;\n}\n.container-two-button {\n  display: flex;\n}\n.file-selected {\n  color: red;\n}\n.btn-add-bulk {\n  margin-bottom: 20px;\n}\n.btn-upload {\n  margin-right: 10px;\n}\n.member-detail-image-container {\n  background: #f0f0f0;\n  font-weight: normal;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.member-detail-image-container img {\n  width: 100%;\n}\n.member-detail-image-container a {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.member-detail-image-container a .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.member-detail-image-container .btn-download-container {\n  margin-top: 5px;\n  margin-bottom: 5px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.member-detail-image-container .img-upload {\n  display: flex;\n  justify-content: space-around;\n  align-items: center;\n  margin: 20px 10px;\n}\n.member-detail-image-container .img-upload .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\nbutton.btn-download-img {\n  margin-top: 10px;\n  background-color: #3498db;\n  color: #fff;\n  padding: 5px 15px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 4px;\n  border: 0;\n  cursor: pointer;\n}\n.edit_button {\n  position: absolute;\n  top: 7px;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n.edit_button i {\n  font-weight: bold;\n}\n.btn-right {\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-left: 10px;\n}\n.error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 26px;\n  padding-top: 10px;\n}\n.error-message .text-message {\n  background: #E74C3C;\n  padding: 20px;\n  margin: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZm9ybS1tZW1iZXIvbGlzdC1vcmRlci9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXGZvcm0tbWVtYmVyXFxsaXN0LW9yZGVyXFxsaXN0LW9yZGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9mb3JtLW1lbWJlci9saXN0LW9yZGVyL2xpc3Qtb3JkZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxrQkFBQTtBQ0FSO0FEQ1E7RUFFSSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0RaO0FERVk7RUFDSSxpQkFBQTtBQ0FoQjtBREdRO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0EseUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFLQSxXQUFBO0FDTlo7QURHWTtFQUNJLGlCQUFBO0FDRGhCO0FES1E7RUFDSSx5QkFBQTtBQ0haO0FETUk7RUFDSSxhQUFBO0FDSlI7QURNUTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7QUNKWjtBRFFJO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNOUjtBRFVRO0VBQ0ksZ0JBQUE7QUNSWjtBRFVRO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ1JaO0FEVVE7RUFDSSxnQkFBQTtBQ1JaO0FEU1k7RUFDSSxlQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUNQaEI7QURTWTtFQUNJLHlCQUFBO0FDUGhCO0FEU1k7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FDUGhCO0FEU1k7RUFDSSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FDUGhCO0FEUWdCO0VBQ0ksZUFBQTtFQUNBLFdBQUE7QUNOcEI7QURZUTtFQUNJLHNCQUFBO0FDVlo7QURXWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDVGhCO0FEZ0JBO0VBQ0ksZ0JBQUE7RUFFQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7QUNiSjtBRGtCQTtFQUNJLGdCQUFBO0VBRUEsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtBQ2ZKO0FEaUJBO0VBQ0ksbUJBQUE7RUFFQSxhQUFBO0FDZko7QURrQkE7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDZko7QURnQkk7RUFDSSxhQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDZFI7QURnQlE7RUFDSSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtLQUFBLG1CQUFBO0FDZFo7QURrQkk7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQ2hCUjtBRGlCUTtFQUNJLFlBQUE7RUFFQSxXQUFBO0FDaEJaO0FEaUJZO0VBRUksUUFBQTtBQ2hCaEI7QUR1QkE7RUFDSSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtBQ3BCSjtBRHVCQTtFQUNJLFdBQUE7QUNwQko7QUR1QkE7RUFDSSx3QkFBQTtBQ3BCSjtBRHVCQTtFQUNJLGFBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQ3BCSjtBRHVCQTtFQUNJLGdCQUFBO0FDcEJKO0FEdUJBO0VBQ0ksZ0JBQUE7RUFDQSw0QkFBQTtFQUNBLDZCQUFBO0FDcEJKO0FEdUJBO0VBQ0ksYUFBQTtBQ3BCSjtBRHVCQTtFQUNJLFVBQUE7QUNwQko7QUR1QkE7RUFDSSxtQkFBQTtBQ3BCSjtBRHVCQTtFQUNJLGtCQUFBO0FDcEJKO0FEdUJBO0VBQ0ksbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ3BCSjtBRHFCSTtFQUNJLFdBQUE7QUNuQlI7QURxQkk7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQ25CUjtBRG9CUTtFQUNJLFlBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0tBQUEsbUJBQUE7QUNsQlo7QURxQkk7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQ25CUjtBRHFCSTtFQUNJLGFBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7QUNuQlI7QURxQlE7RUFDSSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtLQUFBLG1CQUFBO0FDbkJaO0FEd0JBO0VBQ0ksZ0JBQUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtBQ3JCSjtBRHdCQTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUVBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUlBLFdBQUE7QUN6Qko7QURzQkk7RUFDSSxpQkFBQTtBQ3BCUjtBRHdCQTtFQUNJLHNCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSwwQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7QUNyQko7QUR3QkE7RUFDSSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ3JCSjtBRHVCSTtFQUNJLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7QUNyQlIiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9mb3JtLW1lbWJlci9saXN0LW9yZGVyL2xpc3Qtb3JkZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2FyZC1kZXRhaWx7XHJcbiAgICA+LmNhcmQtaGVhZGVye1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAuYmFja19idXR0b257XHJcbiAgICAgICAgICAgIC8vIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogLTRweDtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLnNhdmVfYnV0dG9ue1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIHRvcDogN3B4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgIFxyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zYXZlX2J1dHRvbi50cnVle1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTY3ZTIyO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgICAgIFxyXG4gICAgICAgID4uY29sLW1kLTEye1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIFxyXG4gICAgaDF7XHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICB9XHJcbiAgICAubWVtYmVyLWRldGFpbHtcclxuICAgICAgIFxyXG4gICAgICAgIGxhYmVse1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYWJlbCtkaXZ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmMGYwZjA7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0Om5vcm1hO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgICAgICAgICBtaW4taGVpZ2h0OiAyN3B4O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNjY2O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuaW1hZ2V7XHJcbiAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICAgID5kaXZ7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMzMuMzMzMyU7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICAgICAgICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAuY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGgze1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLmNhcmQtY29udGVudHtcclxuICAgICAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgICAgICAgICAgIGltZ3tcclxuICAgICAgICAgICAgICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICBcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmNhcmQtaGVhZGVyeyBcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaDJ7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLnJvdW5kZWQtYnRuIHtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDcwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDQwcHg7XHJcbiAgICBwYWRkaW5nOiAwIDQwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjNTZhNGZmO1xyXG4gICAgLy8gbWFyZ2luLWxlZnQ6IDQwJTtcclxuICAgIC8vIG1hcmdpbi1yaWdodDogNTAlO1xyXG59XHJcblxyXG4ucm91bmRlZC1idG4tZGFuZ2VyIHtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDcwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDQwcHg7XHJcbiAgICBwYWRkaW5nOiAwIDQwcHg7XHJcbiAgICBjb2xvcjogIzU1NTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG59XHJcbi5yb3VuZGVkLWJ0bjpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kOiBkYXJrZW4oIzU2QTRGRiwgMTUlKTtcclxuICAgIC8vIGJvcmRlcjogMnB4IHNvbGlkIGJsdWU7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG59XHJcblxyXG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIge1xyXG4gICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgIGZvbnQtd2VpZ2h0OiBub3JtYTtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBjb2xvcjogIzY2NjtcclxuICAgIC5pbWctdXBsb2FkIHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luOjIwcHggMTBweDtcclxuXHJcbiAgICAgICAgLm1lbWJlci1kZXRhaWwtaW1hZ2Uge1xyXG4gICAgICAgICAgICB3aWR0aDoyMDBweDtcclxuICAgICAgICAgICAgbWF4LWhlaWdodDogMjAwcHg7XHJcbiAgICAgICAgICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5mb3JtLWdyb3VwLnVwbG9hZGVkIHtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAubG9hZGluZyB7XHJcbiAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgLy8gYmFja2dyb3VuZC1jb2xvcjogIzAwMDtcclxuICAgICAgICAgICAgd2lkdGg6MTAwJTtcclxuICAgICAgICAgICAgLnNrLWZhZGluZy1jaXJjbGUge1xyXG4gICAgICAgICAgICAgICAgLy8gYmFja2dyb3VuZC1jb2xvcjpyZWQ7XHJcbiAgICAgICAgICAgICAgICB0b3A6IDUwJTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmJ1dHRvbi5idG5fdXBsb2FkLCBidXR0b24uYWRkLW1vcmUtaW1hZ2UsIGJ1dHRvbi5idG5fZGVsZXRlIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIHBhZGRpbmc6IDVweCAxNXB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICAgIGJvcmRlcjowO1xyXG4gICAgY3Vyc29yOnBvaW50ZXI7XHJcbn1cclxuXHJcbmJ1dHRvbi5idG4tY2hhbmdlLXBhc3N3b3JkIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5idXR0b24uYnRuX2RlbGV0ZXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHNhbG1vbjtcclxufVxyXG5cclxuLmFkZC1tb3JlLWNvbnRhaW5lciB7XHJcbiAgICBkaXNwbGF5OmZsZXg7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgYWxpZ24taXRlbXM6Y2VudGVyO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuXHJcbi5sYWJlbC1ib2xkIHtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbn1cclxuXHJcbi5lcnJvci1tc2ctdXBsb2FkIHtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDBweCAhaW1wb3J0YW50O1xyXG4gICAgcGFkZGluZy1yaWdodDogMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5jb250YWluZXItdHdvLWJ1dHRvbiB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG59XHJcblxyXG4uZmlsZS1zZWxlY3RlZCB7XHJcbiAgICBjb2xvcjogcmVkO1xyXG59XHJcblxyXG4uYnRuLWFkZC1idWxrIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbn1cclxuXHJcbi5idG4tdXBsb2FkIHtcclxuICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxufVxyXG5cclxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIHtcclxuICAgIGJhY2tncm91bmQ6ICNmMGYwZjA7XHJcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgbWluLWhlaWdodDogMjdweDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGNvbG9yOiAjNjY2O1xyXG4gICAgaW1nIHtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgIH1cclxuICAgIGEge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAubWVtYmVyLWRldGFpbC1pbWFnZSB7XHJcbiAgICAgICAgICAgIHdpZHRoOjIwMHB4O1xyXG4gICAgICAgICAgICBtYXgtaGVpZ2h0OiAyMDBweDtcclxuICAgICAgICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuYnRuLWRvd25sb2FkLWNvbnRhaW5lciB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDo1cHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTo1cHg7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgfVxyXG4gICAgLmltZy11cGxvYWQge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW46MjBweCAxMHB4O1xyXG5cclxuICAgICAgICAubWVtYmVyLWRldGFpbC1pbWFnZSB7XHJcbiAgICAgICAgICAgIHdpZHRoOjIwMHB4O1xyXG4gICAgICAgICAgICBtYXgtaGVpZ2h0OiAyMDBweDtcclxuICAgICAgICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbmJ1dHRvbi5idG4tZG93bmxvYWQtaW1nIHtcclxuICAgIG1hcmdpbi10b3A6MTBweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIHBhZGRpbmc6IDVweCAxNXB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICAgIGJvcmRlcjowO1xyXG4gICAgY3Vyc29yOnBvaW50ZXI7XHJcbn1cclxuXHJcbi5lZGl0X2J1dHRvbntcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogN3B4O1xyXG4gICAgLy8gYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgaXtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIH1cclxuICAgIHJpZ2h0OiAxMHB4O1xyXG59XHJcbi5idG4tcmlnaHQge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbn1cclxuXHJcbi5lcnJvci1tZXNzYWdlIHtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgaGVpZ2h0OjEwMHZoO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOjI2cHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcclxuXHJcbiAgICAudGV4dC1tZXNzYWdlIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjRTc0QzNDO1xyXG4gICAgICAgIHBhZGRpbmc6IDIwcHg7XHJcbiAgICAgICAgbWFyZ2luOiAwIDEwcHg7XHJcbiAgICB9XHJcbn0iLCIuY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIHtcbiAgbWFyZ2luLXRvcDogLTRweDtcbiAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgY29sb3I6ICM1NTU7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuc2F2ZV9idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogN3B4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHJpZ2h0OiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLnNhdmVfYnV0dG9uLnRydWUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTY3ZTIyO1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQge1xuICBwYWRkaW5nOiAxNXB4O1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQgPiAuY29sLW1kLTEyIHtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDBweDtcbn1cbi5jYXJkLWRldGFpbCBoMSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjNTU1O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBsYWJlbCB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwgKyBkaXYge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWE7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlIHtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2UgPiBkaXYge1xuICB3aWR0aDogMzMuMzMzMyU7XG4gIHBhZGRpbmc6IDVweDtcbiAgZmxvYXQ6IGxlZnQ7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlIGgzIHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIGNvbG9yOiAjNTU1O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSAuY2FyZC1jb250ZW50IHtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogYXV0bztcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2UgLmNhcmQtY29udGVudCBpbWcge1xuICBtYXgtd2lkdGg6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIGgyIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGxpbmUtaGVpZ2h0OiB1bnNldDtcbiAgaGVpZ2h0OiB1bnNldDtcbiAgbWFyZ2luOiAwcHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuXG4ucm91bmRlZC1idG4ge1xuICBtYXJnaW4tdG9wOiAyMHB4O1xuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDcwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDcwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBsaW5lLWhlaWdodDogNDBweDtcbiAgcGFkZGluZzogMCA0MHB4O1xuICBiYWNrZ3JvdW5kOiAjNTZhNGZmO1xufVxuXG4ucm91bmRlZC1idG4tZGFuZ2VyIHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA3MHB4O1xuICBib3JkZXItcmFkaXVzOiA3MHB4O1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XG4gIHBhZGRpbmc6IDAgNDBweDtcbiAgY29sb3I6ICM1NTU7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuXG4ucm91bmRlZC1idG46aG92ZXIge1xuICBiYWNrZ3JvdW5kOiAjMGE3YmZmO1xuICBvdXRsaW5lOiBub25lO1xufVxuXG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWE7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmltZy11cGxvYWQge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luOiAyMHB4IDEwcHg7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmltZy11cGxvYWQgLm1lbWJlci1kZXRhaWwtaW1hZ2Uge1xuICB3aWR0aDogMjAwcHg7XG4gIG1heC1oZWlnaHQ6IDIwMHB4O1xuICBvYmplY3QtZml0OiBjb250YWluO1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5mb3JtLWdyb3VwLnVwbG9hZGVkIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuZm9ybS1ncm91cC51cGxvYWRlZCAubG9hZGluZyB7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLmxvYWRpbmcgLnNrLWZhZGluZy1jaXJjbGUge1xuICB0b3A6IDUwJTtcbn1cblxuYnV0dG9uLmJ0bl91cGxvYWQsIGJ1dHRvbi5hZGQtbW9yZS1pbWFnZSwgYnV0dG9uLmJ0bl9kZWxldGUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogI2ZmZjtcbiAgcGFkZGluZzogNXB4IDE1cHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIGJvcmRlcjogMDtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG5idXR0b24uYnRuLWNoYW5nZS1wYXNzd29yZCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG5idXR0b24uYnRuX2RlbGV0ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6IHNhbG1vbjtcbn1cblxuLmFkZC1tb3JlLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHdpZHRoOiAxMDAlO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmxhYmVsLWJvbGQge1xuICBmb250LXdlaWdodDogNjAwO1xufVxuXG4uZXJyb3ItbXNnLXVwbG9hZCB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIHBhZGRpbmctbGVmdDogMHB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctcmlnaHQ6IDBweCAhaW1wb3J0YW50O1xufVxuXG4uY29udGFpbmVyLXR3by1idXR0b24ge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuXG4uZmlsZS1zZWxlY3RlZCB7XG4gIGNvbG9yOiByZWQ7XG59XG5cbi5idG4tYWRkLWJ1bGsge1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuXG4uYnRuLXVwbG9hZCB7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cblxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIHtcbiAgYmFja2dyb3VuZDogI2YwZjBmMDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgbWluLWhlaWdodDogMjdweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzY2Njtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciBpbWcge1xuICB3aWR0aDogMTAwJTtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciBhIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgYSAubWVtYmVyLWRldGFpbC1pbWFnZSB7XG4gIHdpZHRoOiAyMDBweDtcbiAgbWF4LWhlaWdodDogMjAwcHg7XG4gIG9iamVjdC1maXQ6IGNvbnRhaW47XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmJ0bi1kb3dubG9hZC1jb250YWluZXIge1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmltZy11cGxvYWQge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luOiAyMHB4IDEwcHg7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmltZy11cGxvYWQgLm1lbWJlci1kZXRhaWwtaW1hZ2Uge1xuICB3aWR0aDogMjAwcHg7XG4gIG1heC1oZWlnaHQ6IDIwMHB4O1xuICBvYmplY3QtZml0OiBjb250YWluO1xufVxuXG5idXR0b24uYnRuLWRvd25sb2FkLWltZyB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiAjZmZmO1xuICBwYWRkaW5nOiA1cHggMTVweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgYm9yZGVyOiAwO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5lZGl0X2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA3cHg7XG4gIGNvbG9yOiAjZmZmO1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcmlnaHQ6IDEwcHg7XG59XG4uZWRpdF9idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uYnRuLXJpZ2h0IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcbiAgY29sb3I6ICNmZmY7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cblxuLmVycm9yLW1lc3NhZ2Uge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDI2cHg7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xufVxuLmVycm9yLW1lc3NhZ2UgLnRleHQtbWVzc2FnZSB7XG4gIGJhY2tncm91bmQ6ICNFNzRDM0M7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIG1hcmdpbjogMCAxMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/form-member/list-order/list-order.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/layout/modules/form-member/list-order/list-order.component.ts ***!
  \*******************************************************************************/
/*! exports provided: ListOrderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListOrderComponent", function() { return ListOrderComponent; });
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_5___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






// import Swal from 'sweetalert2';
// import { basename } from 'path';
// import { ProductService } from '../../../../services/product/product.service';
var ListOrderComponent = /** @class */ (function () {
    function ListOrderComponent(memberService, productService, zone, orderHistoryService) {
        this.memberService = memberService;
        this.productService = productService;
        this.zone = zone;
        this.orderHistoryService = orderHistoryService;
        this.contentList = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_5__;
        this.errorLabel = false;
        this.errorMessage = false;
        this.edit = false;
        this.selectedFile = null;
        this.cancel = false;
        this.progressBar = 0;
        this.errorFile = false;
        this.mci_project = false;
        this.programType = "";
        this.allBast = [];
        this.allSuratKuasa = [];
        this.loading = false;
        this.loadingBAST = [];
        this.loadingSuratKuasa = [];
        this.password = "";
    }
    ListOrderComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    ListOrderComponent.prototype.isArray = function (curVar) {
        return Array.isArray(curVar) ? true : false;
    };
    ListOrderComponent.prototype.assignArrayBoolean = function (curVar, varCont) {
        if (this.isArray(curVar)) {
            curVar.forEach(function (element) {
                varCont.push(false);
            });
        }
    };
    ListOrderComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var program, _this, query, result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        program = localStorage.getItem('programName');
                        _this = this;
                        this.contentList.filter(function (element) {
                            if (element.appLabel == program) {
                                if (element.type == "reguler") {
                                    _this.mci_project = true;
                                }
                                else if (element.type == "custom_kontraktual") {
                                    _this.programType = "custom_kontraktual";
                                }
                                else {
                                    _this.mci_project = false;
                                }
                            }
                        });
                        this.memberDetail = this.detail;
                        query = {
                            search: {
                                username: this.memberDetail.username,
                                status: {
                                    in: ["PROCESSED", "COMPLETED"]
                                }
                            },
                            limit_per_page: 0
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.orderHistoryService.searchAllReportSalesOrder(query)];
                    case 2:
                        result = _a.sent();
                        this.listOrder = result.values;
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ListOrderComponent.prototype.backToDetail = function () {
        // console.log("Edit member",this.back);
        this.back[0][this.back[1]]();
        // this.back[0]();
    };
    ListOrderComponent.prototype.validURL = function (str) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return pattern.test(str);
    };
    ListOrderComponent.prototype.editThis = function (order) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.edit = !this.edit;
                if (this.edit == false) {
                    try {
                        // const result : any= await this.memberService.getFormMemberByID(this.detail.username);
                        // if(result.length > 0){
                        //   if(result[0].username == this.detail.username){
                        //     this.detail = result[0];
                        //     this.firstLoad();
                        //   }
                        // }
                        this.firstLoad();
                    }
                    catch (e) {
                        this.errorLabel = (e.message); //conversion to Error type
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                    }
                }
                else {
                    this.detailOrder = order;
                }
                return [2 /*return*/];
            });
        });
    };
    ListOrderComponent.prototype.downloadImage = function (url) {
        var filename;
        filename = this.lastPathURL(url);
        fetch(url).then(function (t) {
            return t.blob().then(function (b) {
                var a = document.createElement("a");
                a.href = URL.createObjectURL(b);
                a.setAttribute("download", filename);
                a.click();
            });
        });
    };
    ListOrderComponent.prototype.checkFileType = function (text) {
        var splitFileName = text.split('.');
        var extensFile = splitFileName[splitFileName.length - 1];
        var extens = extensFile.toLowerCase();
        return extens == 'pdf' ? 'document' : 'image';
    };
    ListOrderComponent.prototype.lastPathURL = function (url) {
        var resURL = url.split("/");
        console.log("resURL", resURL);
        var lastNumber = resURL.length - 1;
        return resURL[lastNumber];
    };
    ListOrderComponent.prototype.convertDate = function (time) {
        var date = new Date(time);
        var dateString = date.toString();
        var retDate = dateString.split("GMT");
        return retDate[0];
    };
    ListOrderComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"] },
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__["ProductService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] },
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_4__["OrderhistoryService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], ListOrderComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], ListOrderComponent.prototype, "back", void 0);
    ListOrderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list-order',
            template: __webpack_require__(/*! raw-loader!./list-order.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/form-member/list-order/list-order.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_2__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./list-order.component.scss */ "./src/app/layout/modules/form-member/list-order/list-order.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"],
            _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__["ProductService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"],
            _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_4__["OrderhistoryService"]])
    ], ListOrderComponent);
    return ListOrderComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/form-member/redeem-login/redeem-login.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/layout/modules/form-member/redeem-login/redeem-login.component.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n}\n.card-detail > .card-header .back_button {\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 7px;\n  background-color: #3498db;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n.card-detail > .card-header .save_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button.true {\n  background-color: #e67e22;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .member-detail .image {\n  overflow: hidden;\n}\n.card-detail .member-detail .image > div {\n  width: 33.3333%;\n  padding: 5px;\n  float: left;\n}\n.card-detail .member-detail .image .card-header {\n  background-color: #f0f0f0;\n}\n.card-detail .member-detail .image h3 {\n  font-size: 12px;\n  margin-bottom: 0px;\n  color: #555;\n}\n.card-detail .member-detail .image .card-content {\n  overflow: hidden;\n  width: 100%;\n  height: auto;\n}\n.card-detail .member-detail .image .card-content img {\n  max-width: 100%;\n  width: 100%;\n}\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n.rounded-btn {\n  margin-top: 20px;\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  background: #56a4ff;\n}\n.rounded-btn-danger {\n  margin-top: 20px;\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  color: #555;\n  margin-left: 10px;\n}\n.rounded-btn:hover {\n  background: #0a7bff;\n  outline: none;\n}\n.member-detail-image-container {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.member-detail-image-container .img-upload {\n  display: flex;\n  justify-content: space-around;\n  align-items: center;\n  margin: 20px 10px;\n}\n.member-detail-image-container .img-upload .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.member-detail-image-container .form-group.uploaded {\n  position: relative;\n  margin-bottom: 0px;\n  height: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading {\n  height: 100%;\n  width: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading .sk-fading-circle {\n  top: 50%;\n}\nbutton.btn_upload, button.add-more-image, button.btn_delete {\n  background-color: #3498db;\n  color: #fff;\n  padding: 5px 15px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 4px;\n  border: 0;\n  cursor: pointer;\n}\nbutton.btn-change-password {\n  width: 100%;\n}\nbutton.btn_delete {\n  background-color: salmon;\n}\n.add-more-container {\n  display: flex;\n  width: 100%;\n  align-items: center;\n  justify-content: center;\n}\n.label-bold {\n  font-weight: 600;\n}\n.error-msg-upload {\n  margin-top: 10px;\n  padding-left: 0px !important;\n  padding-right: 0px !important;\n}\n.container-two-button {\n  display: flex;\n}\n.file-selected {\n  color: red;\n}\n.btn-add-bulk {\n  margin-bottom: 20px;\n}\n.btn-upload {\n  margin-right: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZm9ybS1tZW1iZXIvcmVkZWVtLWxvZ2luL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcZm9ybS1tZW1iZXJcXHJlZGVlbS1sb2dpblxccmVkZWVtLWxvZ2luLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9mb3JtLW1lbWJlci9yZWRlZW0tbG9naW4vcmVkZWVtLWxvZ2luLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0ksa0JBQUE7QUNBUjtBRENRO0VBRUksZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLDZCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSx5QkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7QUNEWjtBREVZO0VBQ0ksaUJBQUE7QUNBaEI7QURHUTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSwwQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBS0EsV0FBQTtBQ05aO0FER1k7RUFDSSxpQkFBQTtBQ0RoQjtBREtRO0VBQ0kseUJBQUE7QUNIWjtBRE1JO0VBQ0ksYUFBQTtBQ0pSO0FETVE7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0FDSlo7QURRSTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDTlI7QURVUTtFQUNJLGdCQUFBO0FDUlo7QURVUTtFQUNJLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUNSWjtBRFVRO0VBQ0ksZ0JBQUE7QUNSWjtBRFNZO0VBQ0ksZUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FDUGhCO0FEU1k7RUFDSSx5QkFBQTtBQ1BoQjtBRFNZO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQ1BoQjtBRFNZO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQ1BoQjtBRFFnQjtFQUNJLGVBQUE7RUFDQSxXQUFBO0FDTnBCO0FEWVE7RUFDSSxzQkFBQTtBQ1ZaO0FEV1k7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSwwQkFBQTtBQ1RoQjtBRGdCQTtFQUNJLGdCQUFBO0VBRUEsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FDYko7QURrQkE7RUFDSSxnQkFBQTtFQUVBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7QUNmSjtBRGlCQTtFQUNJLG1CQUFBO0VBRUEsYUFBQTtBQ2ZKO0FEa0JBO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ2ZKO0FEZ0JJO0VBQ0ksYUFBQTtFQUNBLDZCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtBQ2RSO0FEZ0JRO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7S0FBQSxtQkFBQTtBQ2RaO0FEa0JJO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUNoQlI7QURpQlE7RUFDSSxZQUFBO0VBRUEsV0FBQTtBQ2hCWjtBRGlCWTtFQUVJLFFBQUE7QUNoQmhCO0FEdUJBO0VBQ0kseUJBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGVBQUE7QUNwQko7QUR1QkE7RUFDSSxXQUFBO0FDcEJKO0FEdUJBO0VBQ0ksd0JBQUE7QUNwQko7QUR1QkE7RUFDSSxhQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUNwQko7QUR1QkE7RUFDSSxnQkFBQTtBQ3BCSjtBRHVCQTtFQUNJLGdCQUFBO0VBQ0EsNEJBQUE7RUFDQSw2QkFBQTtBQ3BCSjtBRHVCQTtFQUNJLGFBQUE7QUNwQko7QUR1QkE7RUFDSSxVQUFBO0FDcEJKO0FEdUJBO0VBQ0ksbUJBQUE7QUNwQko7QUR1QkE7RUFDSSxrQkFBQTtBQ3BCSiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2Zvcm0tbWVtYmVyL3JlZGVlbS1sb2dpbi9yZWRlZW0tbG9naW4uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2FyZC1kZXRhaWx7XHJcbiAgICA+LmNhcmQtaGVhZGVye1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAuYmFja19idXR0b257XHJcbiAgICAgICAgICAgIC8vIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogLTRweDtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLnNhdmVfYnV0dG9ue1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIHRvcDogN3B4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgIFxyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zYXZlX2J1dHRvbi50cnVle1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTY3ZTIyO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgICAgIFxyXG4gICAgICAgID4uY29sLW1kLTEye1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIFxyXG4gICAgaDF7XHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICB9XHJcbiAgICAubWVtYmVyLWRldGFpbHtcclxuICAgICAgIFxyXG4gICAgICAgIGxhYmVse1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYWJlbCtkaXZ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmMGYwZjA7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0Om5vcm1hO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgICAgICAgICBtaW4taGVpZ2h0OiAyN3B4O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNjY2O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuaW1hZ2V7XHJcbiAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICAgID5kaXZ7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMzMuMzMzMyU7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICAgICAgICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAuY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGgze1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLmNhcmQtY29udGVudHtcclxuICAgICAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgICAgICAgICAgIGltZ3tcclxuICAgICAgICAgICAgICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICBcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmNhcmQtaGVhZGVyeyBcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaDJ7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLnJvdW5kZWQtYnRuIHtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDcwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDQwcHg7XHJcbiAgICBwYWRkaW5nOiAwIDQwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjNTZhNGZmO1xyXG4gICAgLy8gbWFyZ2luLWxlZnQ6IDQwJTtcclxuICAgIC8vIG1hcmdpbi1yaWdodDogNTAlO1xyXG59XHJcblxyXG4ucm91bmRlZC1idG4tZGFuZ2VyIHtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDcwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDQwcHg7XHJcbiAgICBwYWRkaW5nOiAwIDQwcHg7XHJcbiAgICBjb2xvcjogIzU1NTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG59XHJcbi5yb3VuZGVkLWJ0bjpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kOiBkYXJrZW4oIzU2QTRGRiwgMTUlKTtcclxuICAgIC8vIGJvcmRlcjogMnB4IHNvbGlkIGJsdWU7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG59XHJcblxyXG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIge1xyXG4gICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuICAgIGZvbnQtd2VpZ2h0OiBub3JtYTtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBjb2xvcjogIzY2NjtcclxuICAgIC5pbWctdXBsb2FkIHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luOjIwcHggMTBweDtcclxuXHJcbiAgICAgICAgLm1lbWJlci1kZXRhaWwtaW1hZ2Uge1xyXG4gICAgICAgICAgICB3aWR0aDoyMDBweDtcclxuICAgICAgICAgICAgbWF4LWhlaWdodDogMjAwcHg7XHJcbiAgICAgICAgICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5mb3JtLWdyb3VwLnVwbG9hZGVkIHtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAubG9hZGluZyB7XHJcbiAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgLy8gYmFja2dyb3VuZC1jb2xvcjogIzAwMDtcclxuICAgICAgICAgICAgd2lkdGg6MTAwJTtcclxuICAgICAgICAgICAgLnNrLWZhZGluZy1jaXJjbGUge1xyXG4gICAgICAgICAgICAgICAgLy8gYmFja2dyb3VuZC1jb2xvcjpyZWQ7XHJcbiAgICAgICAgICAgICAgICB0b3A6IDUwJTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmJ1dHRvbi5idG5fdXBsb2FkLCBidXR0b24uYWRkLW1vcmUtaW1hZ2UsIGJ1dHRvbi5idG5fZGVsZXRlIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIHBhZGRpbmc6IDVweCAxNXB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICAgIGJvcmRlcjowO1xyXG4gICAgY3Vyc29yOnBvaW50ZXI7XHJcbn1cclxuXHJcbmJ1dHRvbi5idG4tY2hhbmdlLXBhc3N3b3JkIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5idXR0b24uYnRuX2RlbGV0ZXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHNhbG1vbjtcclxufVxyXG5cclxuLmFkZC1tb3JlLWNvbnRhaW5lciB7XHJcbiAgICBkaXNwbGF5OmZsZXg7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgYWxpZ24taXRlbXM6Y2VudGVyO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuXHJcbi5sYWJlbC1ib2xkIHtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbn1cclxuXHJcbi5lcnJvci1tc2ctdXBsb2FkIHtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDBweCAhaW1wb3J0YW50O1xyXG4gICAgcGFkZGluZy1yaWdodDogMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5jb250YWluZXItdHdvLWJ1dHRvbiB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG59XHJcblxyXG4uZmlsZS1zZWxlY3RlZCB7XHJcbiAgICBjb2xvcjogcmVkO1xyXG59XHJcblxyXG4uYnRuLWFkZC1idWxrIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbn1cclxuXHJcbi5idG4tdXBsb2FkIHtcclxuICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxufSIsIi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24ge1xuICBtYXJnaW4tdG9wOiAtNHB4O1xuICBtYXJnaW4tbGVmdDogLTEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBjb2xvcjogIzU1NTtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA3cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiAjZmZmO1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcmlnaHQ6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLnNhdmVfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuc2F2ZV9idXR0b24udHJ1ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlNjdlMjI7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCB7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCA+IC5jb2wtbWQtMTIge1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuLmNhcmQtZGV0YWlsIGgxIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM1NTU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGxhYmVsIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBsYWJlbCArIGRpdiB7XG4gIGJhY2tncm91bmQ6ICNmMGYwZjA7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYTtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgbWluLWhlaWdodDogMjdweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzY2Njtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2Uge1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSA+IGRpdiB7XG4gIHdpZHRoOiAzMy4zMzMzJTtcbiAgcGFkZGluZzogNXB4O1xuICBmbG9hdDogbGVmdDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2UgLmNhcmQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2UgaDMge1xuICBmb250LXNpemU6IDEycHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlIC5jYXJkLWNvbnRlbnQge1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSAuY2FyZC1jb250ZW50IGltZyB7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuY2FyZC1oZWFkZXIgaDIge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbGluZS1oZWlnaHQ6IHVuc2V0O1xuICBoZWlnaHQ6IHVuc2V0O1xuICBtYXJnaW46IDBweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG5cbi5yb3VuZGVkLWJ0biB7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNzBweDtcbiAgYm9yZGVyLXJhZGl1czogNzBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE4cHg7XG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xuICBwYWRkaW5nOiAwIDQwcHg7XG4gIGJhY2tncm91bmQ6ICM1NmE0ZmY7XG59XG5cbi5yb3VuZGVkLWJ0bi1kYW5nZXIge1xuICBtYXJnaW4tdG9wOiAyMHB4O1xuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDcwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDcwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBsaW5lLWhlaWdodDogNDBweDtcbiAgcGFkZGluZzogMCA0MHB4O1xuICBjb2xvcjogIzU1NTtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG59XG5cbi5yb3VuZGVkLWJ0bjpob3ZlciB7XG4gIGJhY2tncm91bmQ6ICMwYTdiZmY7XG4gIG91dGxpbmU6IG5vbmU7XG59XG5cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciB7XG4gIGJhY2tncm91bmQ6ICNmMGYwZjA7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYTtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgbWluLWhlaWdodDogMjdweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzY2Njtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuaW1nLXVwbG9hZCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW46IDIwcHggMTBweDtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuaW1nLXVwbG9hZCAubWVtYmVyLWRldGFpbC1pbWFnZSB7XG4gIHdpZHRoOiAyMDBweDtcbiAgbWF4LWhlaWdodDogMjAwcHg7XG4gIG9iamVjdC1maXQ6IGNvbnRhaW47XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmZvcm0tZ3JvdXAudXBsb2FkZWQge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC5sb2FkaW5nIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuZm9ybS1ncm91cC51cGxvYWRlZCAubG9hZGluZyAuc2stZmFkaW5nLWNpcmNsZSB7XG4gIHRvcDogNTAlO1xufVxuXG5idXR0b24uYnRuX3VwbG9hZCwgYnV0dG9uLmFkZC1tb3JlLWltYWdlLCBidXR0b24uYnRuX2RlbGV0ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiAjZmZmO1xuICBwYWRkaW5nOiA1cHggMTVweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgYm9yZGVyOiAwO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbmJ1dHRvbi5idG4tY2hhbmdlLXBhc3N3b3JkIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbmJ1dHRvbi5idG5fZGVsZXRlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogc2FsbW9uO1xufVxuXG4uYWRkLW1vcmUtY29udGFpbmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgd2lkdGg6IDEwMCU7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4ubGFiZWwtYm9sZCB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG5cbi5lcnJvci1tc2ctdXBsb2FkIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgcGFkZGluZy1sZWZ0OiAwcHggIWltcG9ydGFudDtcbiAgcGFkZGluZy1yaWdodDogMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5jb250YWluZXItdHdvLWJ1dHRvbiB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG5cbi5maWxlLXNlbGVjdGVkIHtcbiAgY29sb3I6IHJlZDtcbn1cblxuLmJ0bi1hZGQtYnVsayB7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG5cbi5idG4tdXBsb2FkIHtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/form-member/redeem-login/redeem-login.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/layout/modules/form-member/redeem-login/redeem-login.component.ts ***!
  \***********************************************************************************/
/*! exports provided: RedeemLoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RedeemLoginComponent", function() { return RedeemLoginComponent; });
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






// import { ProductService } from '../../../../services/product/product.service';
var RedeemLoginComponent = /** @class */ (function () {
    function RedeemLoginComponent(memberService, productService, zone) {
        this.memberService = memberService;
        this.productService = productService;
        this.zone = zone;
        this.errorLabel = false;
        this.prodOnUpload = false;
        this.showUploadButton = false;
        this.selectedFile = null;
        this.cancel = false;
        this.progressBar = 0;
        this.errorFile = false;
        this.loading = false;
        this.activationStatus = [
            { label: 'ACTIVATED', value: 'ACTIVATED' },
            { label: 'INACTIVED', value: 'INACTIVED' }
        ];
        this.memberStatus = [
            { label: 'ACTIVE', value: 'ACTIVE' },
            { label: 'INACTIVE', value: 'INACTIVE' }
        ];
        this.loadingKTPPemilik = false;
        this.loadingNPWPPemilik = false;
        this.loadingKTPPenerima = false;
        this.loadingNPWPPenerima = false;
        this.loadingBAST = [];
        this.loadingSuratKuasa = [];
        this.password = "";
        this.showPasswordPage = false;
        this.dataLogin = {
            username: "",
            password: "",
        };
    }
    RedeemLoginComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    RedeemLoginComponent.prototype.isArray = function (curVar) {
        return Array.isArray(curVar) ? true : false;
    };
    RedeemLoginComponent.prototype.assignArrayBoolean = function (curVar, varCont) {
        if (this.isArray(curVar)) {
            curVar.forEach(function (element) {
                varCont.push(false);
            });
        }
    };
    RedeemLoginComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.updateSingle = JSON.parse(JSON.stringify(this.detail.input_form_data));
                this.updateSingle.foto_bast = this.isArray(this.updateSingle.foto_bast) ? this.updateSingle.foto_bast : [];
                this.updateSingle.foto_surat_kuasa = this.isArray(this.updateSingle.foto_surat_kuasa) ? this.updateSingle.foto_surat_kuasa : [];
                this.detail.previous_member_id = this.detail.member_id;
                this.activationStatus.forEach(function (element, index, products_ids) {
                    if (element.value == _this.detail.activation_status) {
                        _this.activationStatus[index].selected = 1;
                    }
                });
                this.detail.previous_member_id = this.detail.member_id;
                this.memberStatus.forEach(function (element, index, products_ids) {
                    if (element.value == _this.detail.member_status) {
                        _this.memberStatus[index].selected = 1;
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    RedeemLoginComponent.prototype.backToDetail = function () {
        // console.log("Edit member",this.back);
        this.back[0][this.back[1]]();
        // this.back[0]();
    };
    RedeemLoginComponent.prototype.validURL = function (str) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return !!pattern.test(str);
    };
    RedeemLoginComponent.prototype.openUpload = function (type, index) {
        var upFile = document.getElementById(type + index);
        upFile.click();
    };
    RedeemLoginComponent.prototype.redeemLogin = function () {
        return __awaiter(this, void 0, void 0, function () {
            var dataLogin, result, webLink, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        dataLogin = {
                            username: this.updateSingle.id_pel,
                            password: this.password
                        };
                        if (!dataLogin) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.memberService.loginRedeemMember(dataLogin)];
                    case 1:
                        result = _a.sent();
                        if (result && result.token) {
                            console.warn("result login", result);
                            webLink = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["getLink"])();
                            window.open(webLink + "?param=" + encodeURIComponent(result.token), '_blank');
                        }
                        _a.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        console.warn("error login", e_1);
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    RedeemLoginComponent.prototype.saveNewPassword = function (idCustomer) {
        return __awaiter(this, void 0, void 0, function () {
            var dataLogin, result, e_2, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        console.warn("id customer", idCustomer);
                        dataLogin = {
                            username: [idCustomer]
                        };
                        return [4 /*yield*/, this.memberService.resetPasswordChangePassword(dataLogin)];
                    case 1:
                        result = _a.sent();
                        if (result && result.data) {
                            this.dataLogin.username = result.data.username;
                            this.dataLogin.password = result.data.password;
                        }
                        this.detail = JSON.parse(JSON.stringify(this.updateSingle));
                        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                            title: 'Success',
                            text: 'Password berhasil disimpan',
                            icon: 'success',
                            confirmButtonText: 'Ok',
                        }).then(function (result) {
                            if (result.isConfirmed) {
                                // this.showThisPassword();
                                // this.backToDetail();
                            }
                        });
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        console.warn("error", e_2);
                        this.errorLabel = (e_2.message); //conversion to Error type
                        message = this.errorLabel;
                        console.warn("message", this.errorLabel);
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    RedeemLoginComponent.prototype.onFileSelected = function (event) {
        var _this = this;
        this.errorFile = false;
        var fileMaxSize = 3000000; // let say 3Mb
        // var reader = new FileReader();
        // reader.readAsDataURL(event.target.files[0]); //
        Array.from(event.target.files).forEach(function (file) {
            if (file.size > fileMaxSize) {
                _this.errorFile = "maximum file is 3Mb";
            }
        });
        this.selectedFile = event.target.files[0];
        console.log('file', this.selectedFile);
    };
    RedeemLoginComponent.prototype.cancelThis = function () {
        this.cancel = !this.cancel;
    };
    RedeemLoginComponent.prototype.actionShowUploadButton = function () {
        this.showUploadButton = !this.showUploadButton;
    };
    RedeemLoginComponent.prototype.convertDate = function (time) {
        var date = new Date(time);
        var dateString = date.toString();
        var retDate = dateString.split("GMT");
        return retDate[0];
    };
    RedeemLoginComponent.prototype.updateDataBulk = function () {
        return __awaiter(this, void 0, void 0, function () {
            var payload, e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.cancel = false;
                        payload = {
                            type: 'application/form-dataLogin',
                        };
                        if (!this.selectedFile) return [3 /*break*/, 2];
                        console.log('file', this.selectedFile, this);
                        return [4 /*yield*/, this.memberService.registerBulkMember(this.selectedFile, this, payload)];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        this.firstLoad();
                        return [3 /*break*/, 4];
                    case 3:
                        e_3 = _a.sent();
                        this.errorLabel = (e_3.message); //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    RedeemLoginComponent.prototype.updateProgressBar = function (value) {
        this.progressBar = value;
    };
    RedeemLoginComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"] },
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__["ProductService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], RedeemLoginComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], RedeemLoginComponent.prototype, "back", void 0);
    RedeemLoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-redeem-login',
            template: __webpack_require__(/*! raw-loader!./redeem-login.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/form-member/redeem-login/redeem-login.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_2__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./redeem-login.component.scss */ "./src/app/layout/modules/form-member/redeem-login/redeem-login.component.scss")]
        }),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root',
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"], _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__["ProductService"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]])
    ], RedeemLoginComponent);
    return RedeemLoginComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/form-member/show-password/show-password.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/layout/modules/form-member/show-password/show-password.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n}\n.card-detail > .card-header .back_button {\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 7px;\n  background-color: #3498db;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n.card-detail > .card-header .save_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button.true {\n  background-color: #e67e22;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .member-detail .image {\n  overflow: hidden;\n}\n.card-detail .member-detail .image > div {\n  width: 33.3333%;\n  padding: 5px;\n  float: left;\n}\n.card-detail .member-detail .image .card-header {\n  background-color: #f0f0f0;\n}\n.card-detail .member-detail .image h3 {\n  font-size: 12px;\n  margin-bottom: 0px;\n  color: #555;\n}\n.card-detail .member-detail .image .card-content {\n  overflow: hidden;\n  width: 100%;\n  height: auto;\n}\n.card-detail .member-detail .image .card-content img {\n  max-width: 100%;\n  width: 100%;\n}\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n.member-detail-image-container {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.member-detail-image-container .img-upload {\n  display: flex;\n  justify-content: space-around;\n  align-items: center;\n  margin: 20px 10px;\n}\n.member-detail-image-container .img-upload .member-detail-image {\n  width: 200px;\n  max-height: 200px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.member-detail-image-container .form-group.uploaded {\n  position: relative;\n  margin-bottom: 0px;\n  height: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading {\n  height: 100%;\n  width: 100%;\n}\n.member-detail-image-container .form-group.uploaded .loading .sk-fading-circle {\n  top: 50%;\n}\nbutton.btn_upload, button.add-more-image, button.btn_delete {\n  background-color: #3498db;\n  color: #fff;\n  padding: 5px 15px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 4px;\n  border: 0;\n  cursor: pointer;\n}\nbutton.btn-change-password {\n  width: 100%;\n}\nbutton.btn_delete {\n  background-color: salmon;\n}\n.add-more-container {\n  display: flex;\n  width: 100%;\n  align-items: center;\n  justify-content: center;\n}\n.label-bold {\n  font-weight: 600;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZm9ybS1tZW1iZXIvc2hvdy1wYXNzd29yZC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXGZvcm0tbWVtYmVyXFxzaG93LXBhc3N3b3JkXFxzaG93LXBhc3N3b3JkLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9mb3JtLW1lbWJlci9zaG93LXBhc3N3b3JkL3Nob3ctcGFzc3dvcmQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxrQkFBQTtBQ0FSO0FEQ1E7RUFFSSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0RaO0FERVk7RUFDSSxpQkFBQTtBQ0FoQjtBREdRO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0EseUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFLQSxXQUFBO0FDTlo7QURHWTtFQUNJLGlCQUFBO0FDRGhCO0FES1E7RUFDSSx5QkFBQTtBQ0haO0FETUk7RUFDSSxhQUFBO0FDSlI7QURNUTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7QUNKWjtBRFFJO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNOUjtBRFVRO0VBQ0ksZ0JBQUE7QUNSWjtBRFVRO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ1JaO0FEVVE7RUFDSSxnQkFBQTtBQ1JaO0FEU1k7RUFDSSxlQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUNQaEI7QURTWTtFQUNJLHlCQUFBO0FDUGhCO0FEU1k7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FDUGhCO0FEU1k7RUFDSSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FDUGhCO0FEUWdCO0VBQ0ksZUFBQTtFQUNBLFdBQUE7QUNOcEI7QURZUTtFQUNJLHNCQUFBO0FDVlo7QURXWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDVGhCO0FEZ0JBO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ2JKO0FEY0k7RUFDSSxhQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDWlI7QURjUTtFQUNJLFlBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0tBQUEsbUJBQUE7QUNaWjtBRGdCSTtFQUNJLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FDZFI7QURlUTtFQUNJLFlBQUE7RUFFQSxXQUFBO0FDZFo7QURlWTtFQUVJLFFBQUE7QUNkaEI7QURxQkE7RUFDSSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtBQ2xCSjtBRHFCQTtFQUNJLFdBQUE7QUNsQko7QURxQkE7RUFDSSx3QkFBQTtBQ2xCSjtBRHFCQTtFQUNJLGFBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQ2xCSjtBRHFCQTtFQUNJLGdCQUFBO0FDbEJKIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZm9ybS1tZW1iZXIvc2hvdy1wYXNzd29yZC9zaG93LXBhc3N3b3JkLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNhcmQtZGV0YWlse1xyXG4gICAgPi5jYXJkLWhlYWRlcntcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgLmJhY2tfYnV0dG9ue1xyXG4gICAgICAgICAgICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC00cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtMTBweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zYXZlX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDdweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICBcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJpZ2h0OiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuc2F2ZV9idXR0b24udHJ1ZXtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2U2N2UyMjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICBcclxuICAgICAgICA+LmNvbC1tZC0xMntcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBcclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLm1lbWJlci1kZXRhaWx7XHJcbiAgICAgICBcclxuICAgICAgICBsYWJlbHtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWwrZGl2e1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDpub3JtYTtcclxuICAgICAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgICAgICAgICAgbWluLWhlaWdodDogMjdweDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICBjb2xvcjogIzY2NjtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmltYWdle1xyXG4gICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICA+ZGl2e1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDMzLjMzMzMlO1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICAgICAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLmNhcmQtaGVhZGVye1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBoM3tcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgICAgICAgICAgICBpbWd7XHJcbiAgICAgICAgICAgICAgICAgICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5jYXJkLWhlYWRlcnsgXHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGgye1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgbWluLWhlaWdodDogMjdweDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGNvbG9yOiAjNjY2O1xyXG4gICAgLmltZy11cGxvYWQge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW46MjBweCAxMHB4O1xyXG5cclxuICAgICAgICAubWVtYmVyLWRldGFpbC1pbWFnZSB7XHJcbiAgICAgICAgICAgIHdpZHRoOjIwMHB4O1xyXG4gICAgICAgICAgICBtYXgtaGVpZ2h0OiAyMDBweDtcclxuICAgICAgICAgICAgb2JqZWN0LWZpdDogY29udGFpbjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLmZvcm0tZ3JvdXAudXBsb2FkZWQge1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIC5sb2FkaW5nIHtcclxuICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwO1xyXG4gICAgICAgICAgICB3aWR0aDoxMDAlO1xyXG4gICAgICAgICAgICAuc2stZmFkaW5nLWNpcmNsZSB7XHJcbiAgICAgICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOnJlZDtcclxuICAgICAgICAgICAgICAgIHRvcDogNTAlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxyXG5cclxuYnV0dG9uLmJ0bl91cGxvYWQsIGJ1dHRvbi5hZGQtbW9yZS1pbWFnZSwgYnV0dG9uLmJ0bl9kZWxldGUge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgcGFkZGluZzogNXB4IDE1cHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gICAgYm9yZGVyOjA7XHJcbiAgICBjdXJzb3I6cG9pbnRlcjtcclxufVxyXG5cclxuYnV0dG9uLmJ0bi1jaGFuZ2UtcGFzc3dvcmQge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbmJ1dHRvbi5idG5fZGVsZXRle1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogc2FsbW9uO1xyXG59XHJcblxyXG4uYWRkLW1vcmUtY29udGFpbmVyIHtcclxuICAgIGRpc3BsYXk6ZmxleDtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBhbGlnbi1pdGVtczpjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG5cclxuLmxhYmVsLWJvbGQge1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxufVxyXG4iLCIuY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIHtcbiAgbWFyZ2luLXRvcDogLTRweDtcbiAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgY29sb3I6ICM1NTU7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuc2F2ZV9idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogN3B4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHJpZ2h0OiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLnNhdmVfYnV0dG9uLnRydWUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTY3ZTIyO1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQge1xuICBwYWRkaW5nOiAxNXB4O1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQgPiAuY29sLW1kLTEyIHtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDBweDtcbn1cbi5jYXJkLWRldGFpbCBoMSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjNTU1O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBsYWJlbCB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwgKyBkaXYge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWE7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlIHtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2UgPiBkaXYge1xuICB3aWR0aDogMzMuMzMzMyU7XG4gIHBhZGRpbmc6IDVweDtcbiAgZmxvYXQ6IGxlZnQ7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlIGgzIHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIGNvbG9yOiAjNTU1O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSAuY2FyZC1jb250ZW50IHtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogYXV0bztcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2UgLmNhcmQtY29udGVudCBpbWcge1xuICBtYXgtd2lkdGg6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIGgyIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGxpbmUtaGVpZ2h0OiB1bnNldDtcbiAgaGVpZ2h0OiB1bnNldDtcbiAgbWFyZ2luOiAwcHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuXG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWE7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmltZy11cGxvYWQge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luOiAyMHB4IDEwcHg7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmltZy11cGxvYWQgLm1lbWJlci1kZXRhaWwtaW1hZ2Uge1xuICB3aWR0aDogMjAwcHg7XG4gIG1heC1oZWlnaHQ6IDIwMHB4O1xuICBvYmplY3QtZml0OiBjb250YWluO1xufVxuLm1lbWJlci1kZXRhaWwtaW1hZ2UtY29udGFpbmVyIC5mb3JtLWdyb3VwLnVwbG9hZGVkIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5tZW1iZXItZGV0YWlsLWltYWdlLWNvbnRhaW5lciAuZm9ybS1ncm91cC51cGxvYWRlZCAubG9hZGluZyB7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubWVtYmVyLWRldGFpbC1pbWFnZS1jb250YWluZXIgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLmxvYWRpbmcgLnNrLWZhZGluZy1jaXJjbGUge1xuICB0b3A6IDUwJTtcbn1cblxuYnV0dG9uLmJ0bl91cGxvYWQsIGJ1dHRvbi5hZGQtbW9yZS1pbWFnZSwgYnV0dG9uLmJ0bl9kZWxldGUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogI2ZmZjtcbiAgcGFkZGluZzogNXB4IDE1cHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIGJvcmRlcjogMDtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG5idXR0b24uYnRuLWNoYW5nZS1wYXNzd29yZCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG5idXR0b24uYnRuX2RlbGV0ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6IHNhbG1vbjtcbn1cblxuLmFkZC1tb3JlLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHdpZHRoOiAxMDAlO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmxhYmVsLWJvbGQge1xuICBmb250LXdlaWdodDogNjAwO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/form-member/show-password/show-password.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/layout/modules/form-member/show-password/show-password.component.ts ***!
  \*************************************************************************************/
/*! exports provided: ShowPasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShowPasswordComponent", function() { return ShowPasswordComponent; });
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




// import { ProductService } from '../../../../services/product/product.service';
var ShowPasswordComponent = /** @class */ (function () {
    function ShowPasswordComponent(memberService, productService, zone) {
        this.memberService = memberService;
        this.productService = productService;
        this.zone = zone;
        this.errorLabel = false;
        this.loading = false;
        this.activationStatus = [
            { label: 'ACTIVATED', value: 'ACTIVATED' },
            { label: 'INACTIVED', value: 'INACTIVED' }
        ];
        this.memberStatus = [
            { label: 'ACTIVE', value: 'ACTIVE' },
            { label: 'INACTIVE', value: 'INACTIVE' }
        ];
        this.errorFile = false;
    }
    ShowPasswordComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    ShowPasswordComponent.prototype.isArray = function (curVar) {
        return Array.isArray(curVar) ? true : false;
    };
    ShowPasswordComponent.prototype.assignArrayBoolean = function (curVar, varCont) {
        if (this.isArray(curVar)) {
            curVar.forEach(function (element) {
                varCont.push(false);
            });
        }
    };
    ShowPasswordComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.updateSingle = JSON.parse(JSON.stringify(this.detail));
                // console.warn("this detail???", this.detail)
                this.detail.previous_member_id = this.detail.member_id;
                return [2 /*return*/];
            });
        });
    };
    ShowPasswordComponent.prototype.backToDetail = function () {
        this.back[0][this.back[1]]();
    };
    ShowPasswordComponent.prototype.validURL = function (str) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return !!pattern.test(str);
    };
    ShowPasswordComponent.prototype.openUpload = function (type, index) {
        var upFile = document.getElementById(type + index);
        upFile.click();
    };
    ShowPasswordComponent.prototype.convertDate = function (time) {
        var date = new Date(time);
        var dateString = date.toString();
        var retDate = dateString.split("GMT");
        return retDate[0];
    };
    ShowPasswordComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"] },
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__["ProductService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], ShowPasswordComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Object)
    ], ShowPasswordComponent.prototype, "back", void 0);
    ShowPasswordComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-show-password',
            template: __webpack_require__(/*! raw-loader!./show-password.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/form-member/show-password/show-password.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_2__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./show-password.component.scss */ "./src/app/layout/modules/form-member/show-password/show-password.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_3__["MemberService"], _services_product_product_service__WEBPACK_IMPORTED_MODULE_0__["ProductService"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]])
    ], ShowPasswordComponent);
    return ShowPasswordComponent;
}());



/***/ })

}]);
//# sourceMappingURL=modules-form-member-form-member-module.js.map