(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["reset-password-reset-password-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/reset-password/reset-password.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/reset-password/reset-password.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">\r\n<div class=\"container-all\">\r\n    <div class=\"header\">\r\n        <div class=\"logo\">\r\n            <img src=\"/assets/images/locard_icon_circle.png\">\r\n            <span class=\"title\">LOCARD</span>\r\n        </div>\r\n    </div>\r\n    <div class=\"button-back\" >\r\n      \r\n            <i class=\"material-icons\"  (click)=\"backToLogin()\" style=\"font-size:60px; font-weight: bold;\" >\r\n                keyboard_arrow_left\r\n                </i>\r\n    </div>\r\n    <div class=\"body\">\r\n            <div class=\"col-md-4\">\r\n                <div class=\"card container-form\">\r\n                    <div class=\"content\" *ngIf=\"step == 1\">\r\n                        <div class=\"title-content\">Forgot Your Password ?</div>\r\n                        <label class=\"body-content\">Enter your email address and we will send a message along with a link to return to your account.</label>\r\n                        <div class=\"email\">\r\n                        <mat-list >\r\n                            <mat-list-item >\r\n                                <mat-form-field appearance=\"outline\" class=\"full\">\r\n                                    <mat-label>Email</mat-label>\r\n                                    <input matInput placeholder=\"Email\" (click)=\"errorMessage = false\" [formControl]=\"email\" required [(ngModel)]=\"form1.email\"\r\n                                        >\r\n                                   \r\n                                </mat-form-field>\r\n                            </mat-list-item>\r\n                        </mat-list>\r\n                        <div class=\"notif error-message\" *ngIf=\"errorMessage\">\r\n                            *{{errorMessage}}\r\n                        </div>\r\n                    </div>\r\n                    \r\n                        <button class=\"common-button blue-fill full\" (click)=\"sendEmail()\"><span style=\"font-size:20px;\">Send</span ></button>\r\n                    </div>\r\n                    <div class=\"content\" *ngIf=\"step == 2\">\r\n                        <div class=\"title-content\"></div>\r\n                        <div class=\"body-content\">Silahkan cek email untuk kode verifikasi</div>\r\n                        <div class=\"email\">\r\n                        <mat-list >\r\n                            <mat-list-item >\r\n                                <mat-form-field appearance=\"outline\" class=\"full\">\r\n                                    <mat-label>Kode Verifikasi</mat-label>\r\n                                    <input matInput placeholder=\"code\" (click)=\"errorMessage = false\" required [(ngModel)]=\"form2.auth_code\">\r\n                                </mat-form-field>\r\n                            </mat-list-item>\r\n                        </mat-list>\r\n                        <div class=\"notif error-message\" *ngIf=\"errorMessage\">\r\n                            *{{errorMessage}}\r\n                        </div>\r\n                        <div class=\"button-container\">\r\n                        <button class=\"common-button blue-fill\" (click)=\"submitCode()\">Submit</button>\r\n                        <button class=\"common-button\" [disabled]=\"!buttonActive\" [class.blue-fill]= \"buttonActive\" (click)=\"sendEmail()\">Resend <span *ngIf=\"!buttonActive\">({{timeLeft}})</span></button>\r\n                    </div>\r\n                    </div>\r\n                    </div>\r\n                    <div class=\"content\" *ngIf=\"step == 3\">\r\n                        <div class=\"title-content\"></div>\r\n                        <div>New Password</div>\r\n                        <mat-list-item>\r\n                            <mat-form-field  appearance=\"outline\" class=\"full\">\r\n                                \r\n                                <input matInput [type]=\"hide ? 'password' : 'text'\"\r\n                                    [(ngModel)]=\"form3.password\" (click)=\"errorMessage = false\">\r\n                                <button mat-icon-button matSuffix (click)=\"hide = !hide\" [attr.aria-label]=\"'Hide password'\"\r\n                                    [attr.aria-pressed]=\"hide\" style=\"background:transparent; border: none;\">\r\n                                    <mat-icon >{{hide ? 'visibility_off' : 'visibility'}}</mat-icon>\r\n                                    \r\n                                </button>\r\n                            </mat-form-field>\r\n                        </mat-list-item>\r\n                    \r\n                        \r\n                        <div >Re Type Password</div>\r\n                        <mat-list-item>\r\n                            <mat-form-field  appearance=\"outline\" class=\"full\">\r\n                                <input matInput [type]=\"hide ? 'password' : 'text'\"\r\n                                    [(ngModel)]=\"form3.repeat_password\" (click)=\"errorMessage = false\">\r\n                                <button mat-icon-button matSuffix (click)=\"hide = !hide\" [attr.aria-label]=\"'Hide password'\"\r\n                                    [attr.aria-pressed]=\"hide\" style=\"background:transparent; border: none;\">\r\n                                    <mat-icon >{{hide ? 'visibility_off' : 'visibility'}}</mat-icon>\r\n                                    \r\n                                </button>\r\n                            </mat-form-field>\r\n                        </mat-list-item>\r\n\r\n                        <button class=\"common-button blue-fill\" (click)=\"changePassword()\">Save </button>\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n        \r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/reset-password/reset-password-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/reset-password/reset-password-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: ResetPasswordRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResetPasswordRoutingModule", function() { return ResetPasswordRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _reset_password_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./reset-password.component */ "./src/app/reset-password/reset-password.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _reset_password_component__WEBPACK_IMPORTED_MODULE_2__["ResetPasswordComponent"]
    },
];
var ResetPasswordRoutingModule = /** @class */ (function () {
    function ResetPasswordRoutingModule() {
    }
    ResetPasswordRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ResetPasswordRoutingModule);
    return ResetPasswordRoutingModule;
}());



/***/ }),

/***/ "./src/app/reset-password/reset-password.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/reset-password/reset-password.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  font-family: \"Poppins\", sans-serif;\n  transition: ease all 0.3s;\n}\n\n::ng-deep .mat-list-base .mat-list-item .mat-list-item-content, .mat-list-base .mat-list-option .mat-list-item-content {\n  padding: 0px !important;\n}\n\n.full {\n  width: 100%;\n}\n\n.container-all .button-back {\n  top: 3vh;\n  left: 5vw;\n  position: absolute;\n}\n\n.container-all .header {\n  margin-top: 5vh;\n}\n\n.container-all .header .logo {\n  display: flex;\n  justify-content: center;\n}\n\n.container-all .header .logo img {\n  width: 60px;\n  height: auto;\n}\n\n.container-all .header .logo .title {\n  margin-left: 15px;\n  font-weight: bold;\n  color: #085685;\n  font-size: 40px;\n}\n\n.container-all .body {\n  margin-top: 10vh;\n  display: flex;\n  justify-content: center;\n  align-self: center;\n}\n\n.container-all .body .container-form {\n  height: 400px;\n}\n\n.container-all .body .container-form .content {\n  display: flex;\n  justify-content: center;\n  align-content: center;\n  flex-direction: column;\n  vertical-align: middle;\n  padding: 10% 15%;\n}\n\n.container-all .body .container-form .content .title-content {\n  text-align: center;\n  font-weight: bold;\n  font-size: 20px;\n}\n\n.container-all .body .container-form .content .body-content {\n  text-align: left;\n  margin-top: 30px;\n  font-size: 15px;\n}\n\n.container-all .body .container-form .content .email {\n  margin-top: 20px;\n  margin-bottom: 20px;\n}\n\n.container-all .body .container-form .content .button-container {\n  display: flex;\n  justify-content: space-between;\n  margin-top: 20px;\n}\n\n.container-all .body .container-form .content .button-container button {\n  width: 47%;\n  border: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVzZXQtcGFzc3dvcmQvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxccmVzZXQtcGFzc3dvcmRcXHJlc2V0LXBhc3N3b3JkLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9yZXNldC1wYXNzd29yZC9yZXNldC1wYXNzd29yZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNJLGtDQUFBO0VBQ0EseUJBQUE7QUNBSjs7QURFRTtFQUNFLHVCQUFBO0FDQ0o7O0FETUE7RUFDSSxXQUFBO0FDSEo7O0FETUk7RUFDSSxRQUFBO0VBQ0EsU0FBQTtFQUNBLGtCQUFBO0FDSFI7O0FEUUk7RUFDSSxlQUFBO0FDTlI7O0FET1E7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7QUNMWjs7QURNWTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FDSmhCOztBRE1ZO0VBQ0ksaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FDSmhCOztBRFFJO0VBQ0ksZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtBQ05SOztBRE9RO0VBQ0ksYUFBQTtBQ0xaOztBRE1ZO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0VBQ0EscUJBQUE7RUFDQSxzQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7QUNKaEI7O0FES2dCO0VBQ0ksa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUNIcEI7O0FES2dCO0VBQ0ksZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUNIcEI7O0FES2dCO0VBQ0ksZ0JBQUE7RUFDQSxtQkFBQTtBQ0hwQjs7QURLZ0I7RUFDSSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxnQkFBQTtBQ0hwQjs7QURJb0I7RUFDSSxVQUFBO0VBQ0EsWUFBQTtBQ0Z4QiIsImZpbGUiOiJzcmMvYXBwL3Jlc2V0LXBhc3N3b3JkL3Jlc2V0LXBhc3N3b3JkLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbioge1xyXG4gICAgZm9udC1mYW1pbHk6IFwiUG9wcGluc1wiLCBzYW5zLXNlcmlmO1xyXG4gICAgdHJhbnNpdGlvbjogZWFzZSBhbGwgLjNzO1xyXG4gIH1cclxuICA6Om5nLWRlZXAgLm1hdC1saXN0LWJhc2UgLm1hdC1saXN0LWl0ZW0gLm1hdC1saXN0LWl0ZW0tY29udGVudCwgLm1hdC1saXN0LWJhc2UgLm1hdC1saXN0LW9wdGlvbiAubWF0LWxpc3QtaXRlbS1jb250ZW50e1xyXG4gICAgcGFkZGluZyA6IDBweCAhaW1wb3J0YW50O1xyXG4gIH1cclxuLy8gICA6Om5nLWRlZXAgLm1hdC1mb3JtLWZpZWxkLWFwcGVhcmFuY2Utb3V0bGluZSAubWF0LWZvcm0tZmllbGQtaW5maXgge1xyXG4vLyAgICAgLy8gcGFkZGluZy10b3A6IDNweCAhaW1wb3J0YW50O1xyXG4gIFxyXG4vLyAgIH1cclxuXHJcbi5mdWxsIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcbi5jb250YWluZXItYWxsIHtcclxuICAgIC5idXR0b24tYmFjayB7XHJcbiAgICAgICAgdG9wIDozdmg7XHJcbiAgICAgICAgbGVmdDogNXZ3O1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAuY29udGFpbmVyLWJ1dHRvbi1iYWNrIHtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmhlYWRlciB7XHJcbiAgICAgICAgbWFyZ2luLXRvcCA6IDV2aDtcclxuICAgICAgICAubG9nbyB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBpbWcge1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDYwcHg7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLnRpdGxlIHtcclxuICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgICAgICBjb2xvciA6ICMwODU2ODU7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDQwcHg7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuYm9keSB7XHJcbiAgICAgICAgbWFyZ2luLXRvcCA6IDEwdmg7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1zZWxmOiBjZW50ZXI7XHJcbiAgICAgICAgLmNvbnRhaW5lci1mb3JtIHtcclxuICAgICAgICAgICAgaGVpZ2h0OiA0MDBweDtcclxuICAgICAgICAgICAgLmNvbnRlbnQge1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgYWxpZ24tY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICAgICAgICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nIDogMTAlIDE1JTtcclxuICAgICAgICAgICAgICAgIC50aXRsZS1jb250ZW50IHtcclxuICAgICAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAgMjBweDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIC5ib2R5LWNvbnRlbnQge1xyXG4gICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogMzBweDtcclxuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAuZW1haWwge1xyXG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIC5idXR0b24tY29udGFpbmVyIHtcclxuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wIDogMjBweDtcclxuICAgICAgICAgICAgICAgICAgICBidXR0b24ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogNDclO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBib3JkZXIgOiBub25lO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0iLCIqIHtcbiAgZm9udC1mYW1pbHk6IFwiUG9wcGluc1wiLCBzYW5zLXNlcmlmO1xuICB0cmFuc2l0aW9uOiBlYXNlIGFsbCAwLjNzO1xufVxuXG46Om5nLWRlZXAgLm1hdC1saXN0LWJhc2UgLm1hdC1saXN0LWl0ZW0gLm1hdC1saXN0LWl0ZW0tY29udGVudCwgLm1hdC1saXN0LWJhc2UgLm1hdC1saXN0LW9wdGlvbiAubWF0LWxpc3QtaXRlbS1jb250ZW50IHtcbiAgcGFkZGluZzogMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5mdWxsIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5jb250YWluZXItYWxsIC5idXR0b24tYmFjayB7XG4gIHRvcDogM3ZoO1xuICBsZWZ0OiA1dnc7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cbi5jb250YWluZXItYWxsIC5oZWFkZXIge1xuICBtYXJnaW4tdG9wOiA1dmg7XG59XG4uY29udGFpbmVyLWFsbCAuaGVhZGVyIC5sb2dvIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG4uY29udGFpbmVyLWFsbCAuaGVhZGVyIC5sb2dvIGltZyB7XG4gIHdpZHRoOiA2MHB4O1xuICBoZWlnaHQ6IGF1dG87XG59XG4uY29udGFpbmVyLWFsbCAuaGVhZGVyIC5sb2dvIC50aXRsZSB7XG4gIG1hcmdpbi1sZWZ0OiAxNXB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICMwODU2ODU7XG4gIGZvbnQtc2l6ZTogNDBweDtcbn1cbi5jb250YWluZXItYWxsIC5ib2R5IHtcbiAgbWFyZ2luLXRvcDogMTB2aDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLXNlbGY6IGNlbnRlcjtcbn1cbi5jb250YWluZXItYWxsIC5ib2R5IC5jb250YWluZXItZm9ybSB7XG4gIGhlaWdodDogNDAwcHg7XG59XG4uY29udGFpbmVyLWFsbCAuYm9keSAuY29udGFpbmVyLWZvcm0gLmNvbnRlbnQge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24tY29udGVudDogY2VudGVyO1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICBwYWRkaW5nOiAxMCUgMTUlO1xufVxuLmNvbnRhaW5lci1hbGwgLmJvZHkgLmNvbnRhaW5lci1mb3JtIC5jb250ZW50IC50aXRsZS1jb250ZW50IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAyMHB4O1xufVxuLmNvbnRhaW5lci1hbGwgLmJvZHkgLmNvbnRhaW5lci1mb3JtIC5jb250ZW50IC5ib2R5LWNvbnRlbnQge1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBtYXJnaW4tdG9wOiAzMHB4O1xuICBmb250LXNpemU6IDE1cHg7XG59XG4uY29udGFpbmVyLWFsbCAuYm9keSAuY29udGFpbmVyLWZvcm0gLmNvbnRlbnQgLmVtYWlsIHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cbi5jb250YWluZXItYWxsIC5ib2R5IC5jb250YWluZXItZm9ybSAuY29udGVudCAuYnV0dG9uLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgbWFyZ2luLXRvcDogMjBweDtcbn1cbi5jb250YWluZXItYWxsIC5ib2R5IC5jb250YWluZXItZm9ybSAuY29udGVudCAuYnV0dG9uLWNvbnRhaW5lciBidXR0b24ge1xuICB3aWR0aDogNDclO1xuICBib3JkZXI6IG5vbmU7XG59Il19 */"

/***/ }),

/***/ "./src/app/reset-password/reset-password.component.ts":
/*!************************************************************!*\
  !*** ./src/app/reset-password/reset-password.component.ts ***!
  \************************************************************/
/*! exports provided: ResetPasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResetPasswordComponent", function() { return ResetPasswordComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var ResetPasswordComponent = /** @class */ (function () {
    function ResetPasswordComponent(memberService, router) {
        this.memberService = memberService;
        this.router = router;
        this.step = 1;
        this.hide = true;
        this.email = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]);
        this.timeLeft = 60;
        this.buttonActive = false;
        this.form1 = {
            email: ''
        };
        this.form2 = {
            email: '',
            auth_code: ''
        };
        this.form3 = {
            password: '',
            repeat_password: ''
        };
    }
    ResetPasswordComponent.prototype.getErrorMessage = function () {
        return this.email.hasError('required') ? 'You must enter a value' :
            this.email.hasError('email') ? 'Not a valid email' :
                '';
    };
    ResetPasswordComponent.prototype.ngOnInit = function () {
    };
    ResetPasswordComponent.prototype.sendEmail = function () {
        return __awaiter(this, void 0, void 0, function () {
            var data, res, e_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = this.form1;
                        console.log("test", data);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.memberService.resetPasswordRequest(data)];
                    case 2:
                        res = _a.sent();
                        if (res.result) {
                            alert('request send');
                            this.step = 2;
                            clearInterval(this.interval);
                            this.timeLeft = 60;
                            this.buttonActive = false;
                            this.startTimer();
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        setTimeout(function () {
                            _this.errorMessage = (e_1.message);
                        }, 500);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ResetPasswordComponent.prototype.backToLogin = function () {
        if (localStorage)
            localStorage.removeItem('tokenlogin');
        this.router.navigate(['/login-merchant']);
    };
    ResetPasswordComponent.prototype.startTimer = function () {
        var _this = this;
        this.interval = setInterval(function () {
            if (_this.timeLeft > 0) {
                _this.timeLeft--;
            }
            else {
                _this.buttonActive = true;
            }
        }, 1000);
    };
    ResetPasswordComponent.prototype.submitCode = function () {
        return __awaiter(this, void 0, void 0, function () {
            var data, res, e_2;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.form2.email = this.form1.email;
                        data = this.form2;
                        console.log("test2", data);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.memberService.resetPasswordCodeVerification(data)];
                    case 2:
                        res = _a.sent();
                        if (res.result) {
                            alert('code send');
                            localStorage.setItem('tokenlogin', res.result.token);
                            this.step = 3;
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_2 = _a.sent();
                        setTimeout(function () {
                            _this.errorMessage = (e_2.message);
                        }, 500);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ResetPasswordComponent.prototype.changePassword = function () {
        return __awaiter(this, void 0, void 0, function () {
            var data;
            var _this = this;
            return __generator(this, function (_a) {
                data = this.form3;
                console.log("test2", data);
                try {
                    // let res = await this.memberService.resetPasswordChangePassword(data);
                    // console.log("selesai")
                    // if (res.result) {
                    //   alert('Password Changed');
                    //   localStorage.removeItem('tokenlogin');
                    //   this.router.navigate(['/login-merchant'])
                    // }
                }
                catch (e) {
                    setTimeout(function () {
                        _this.errorMessage = (e.message);
                    }, 500);
                }
                return [2 /*return*/];
            });
        });
    };
    ResetPasswordComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    ResetPasswordComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-reset-password',
            template: __webpack_require__(/*! raw-loader!./reset-password.component.html */ "./node_modules/raw-loader/index.js!./src/app/reset-password/reset-password.component.html"),
            styles: [__webpack_require__(/*! ./reset-password.component.scss */ "./src/app/reset-password/reset-password.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], ResetPasswordComponent);
    return ResetPasswordComponent;
}());



/***/ }),

/***/ "./src/app/reset-password/reset-password.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/reset-password/reset-password.module.ts ***!
  \*********************************************************/
/*! exports provided: ResetPasswordModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResetPasswordModule", function() { return ResetPasswordModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _reset_password_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./reset-password.component */ "./src/app/reset-password/reset-password.component.ts");
/* harmony import */ var _reset_password_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./reset-password-routing.module */ "./src/app/reset-password/reset-password-routing.module.ts");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/list */ "./node_modules/@angular/material/esm5/list.es5.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var ResetPasswordModule = /** @class */ (function () {
    function ResetPasswordModule() {
    }
    ResetPasswordModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_reset_password_routing_module__WEBPACK_IMPORTED_MODULE_3__["ResetPasswordRoutingModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatFormFieldModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__["NgbModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_5__["MatInputModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__["MatIconModule"],
                _angular_material_list__WEBPACK_IMPORTED_MODULE_7__["MatListModule"],],
            declarations: [_reset_password_component__WEBPACK_IMPORTED_MODULE_2__["ResetPasswordComponent"]]
        })
    ], ResetPasswordModule);
    return ResetPasswordModule;
}());



/***/ })

}]);
//# sourceMappingURL=reset-password-reset-password-module.js.map