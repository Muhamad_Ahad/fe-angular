(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["administrator-administrator-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/administrator/administrator.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/administrator/administrator.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n<app-nested-sidebar (collapsedEvent)=\"receiveCollapsed($event)\"></app-nested-sidebar>\r\n<section class=\"main-container\" [ngClass]=\"{collapsed: collapedSideBar}\">\r\n    <router-outlet></router-outlet>\r\n</section>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/components/nested-sidebar/nested-sidebar.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/components/nested-sidebar/nested-sidebar.component.html ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"sidebar\" [ngClass]=\"{sidebarPushRight: isActive, collapsed: collapsed}\" xmlns=\"\">\r\n  <div class=\"list-group\">\r\n\r\n    <div class=\"nested-menu\">\r\n        <p class=\"parent-menu\">Member Management</p>\r\n    </div>\r\n    <div class=\"nested-menu child-menu\">\r\n        <li id=\"new-line\" class=\"new-line\" *ngFor=\"let menu of sideBarMenu['Member']\">\r\n          <a [ngClass]=\"router.url == menu.routerLink ? 'list-group-item router-link-active' : 'list-group-item'\" (click) = \"handleChangePage(menu.routerLink)\">\r\n            <span>{{ menu.label | translate }}</span>\r\n          </a>\r\n            <!-- <a routerLink=\"{{menu.routerLink}}\" [routerLinkActive]=\"['router-link-active']\"\r\n                class=\"list-group-item\">\r\n                <span>{{ menu.label | translate }}</span>\r\n            </a> -->\r\n        </li>\r\n        <li *ngIf=\"mci_project == true\" id=\"new-line\" class=\"new-line\">\r\n          <a [ngClass]=\"router.url == '/administrator/order-bulk' ? 'list-group-item router-link-active' : 'list-group-item'\" (click) = \"handleChangePage('/administrator/order-bulk')\">\r\n            <span>Order Bulky</span>\r\n          </a>\r\n          <!-- <a routerLink=\"/administrator/order-bulk\" [routerLinkActive]=\"['router-link-active']\"\r\n              class=\"list-group-item\">\r\n              <span>Bulky Order</span>\r\n          </a> -->\r\n        </li>\r\n    </div>\r\n\r\n    <div class=\"nested-menu\">\r\n      <p class=\"parent-menu\">Product Management</p>\r\n    </div>\r\n    <div class=\"nested-menu child-menu\">\r\n      <a (click)=\"addExpandClass('Products')\" class=\"list-group-item\">\r\n        <span class=\"new-line\"><i [ngClass]=\"showMenu === 'Products'? 'fas fa-caret-down' : 'fas fa-caret-right'\" ></i>{{ ' Products' | translate }}</span>\r\n      </a>\r\n      <li class=\"nested\" [class.expand]=\"showMenu === 'Products'\">\r\n          <ul class=\"submenu\">\r\n              <li id=\"new-line\" class=\"new-line\" *ngFor=\"let menu of sideBarMenu['Products']\">\r\n                  <!-- <a routerLink=\"{{menu.routerLink}}\" [routerLinkActive]=\"['router-link-active']\"\r\n                      class=\"list-group-item\">\r\n                      <span>{{menu.label}}</span>\r\n                  </a> -->\r\n                  <a [ngClass]=\"router.url == menu.routerLink ? 'list-group-item router-link-active' : 'list-group-item'\" (click) = \"handleChangePage(menu.routerLink)\">\r\n                    <span>{{ menu.label | translate }}</span>\r\n                  </a>\r\n              </li>\r\n          </ul>  \r\n      </li>\r\n      <a (click)=\"addExpandClass('Evoucher')\" class=\"list-group-item\">\r\n        <span class=\"new-line\"><i [ngClass]=\"showMenu === 'Evoucher'? 'fas fa-caret-down' : 'fas fa-caret-right'\" ></i>{{ ' E-Voucher' | translate }}</span>\r\n      </a>\r\n      <li class=\"nested\" [class.expand]=\"showMenu === 'Evoucher'\">\r\n          <ul class=\"submenu\">\r\n              <li id=\"new-line\" class=\"new-line\" *ngFor=\"let menu of sideBarMenu['Evoucher']\">\r\n                <a [ngClass]=\"router.url == menu.routerLink ? 'list-group-item router-link-active' : 'list-group-item'\" (click) = \"handleChangePage(menu.routerLink)\">\r\n                  <span>{{ menu.label | translate }}</span>\r\n                </a>\r\n                  <!-- <a routerLink=\"{{menu.routerLink}}\" [routerLinkActive]=\"['router-link-active']\"\r\n                      class=\"list-group-item\">\r\n                      <span>{{ menu.label | translate }}</span>\r\n                  </a> -->\r\n              </li>\r\n          </ul>\r\n      </li>\r\n      <a (click)=\"addExpandClass('Category')\" class=\"list-group-item\">\r\n        <span class=\"new-line\"><i [ngClass]=\"showMenu === 'Category'? 'fas fa-caret-down' : 'fas fa-caret-right'\" ></i>{{ ' Category' | translate }}</span>\r\n      </a>\r\n      <li class=\"nested\" [class.expand]=\"showMenu === 'Category'\">\r\n          <ul class=\"submenu\">\r\n              <li id=\"new-line\" class=\"new-line\" *ngFor=\"let menu of sideBarMenu['Category']\">\r\n                  <!-- <a routerLink=\"{{menu.routerLink}}\" [routerLinkActive]=\"['router-link-active']\"\r\n                      class=\"list-group-item\">\r\n                      <span>{{ menu.label | translate }}</span>\r\n                  </a> -->\r\n                  <a [ngClass]=\"router.url == menu.routerLink ? 'list-group-item router-link-active' : 'list-group-item'\" (click) = \"handleChangePage(menu.routerLink)\">\r\n                    <span>{{ menu.label | translate }}</span>\r\n                  </a>\r\n              </li>\r\n          </ul>\r\n      </li>\r\n      <!-- Top Up E-Wallet -->\r\n      <a *ngIf=\"mci_project == false\" (click)=\"addExpandClass('E-Wallet')\" class=\"list-group-item\">\r\n        <span class=\"new-line\"><i [ngClass]=\"showMenu === 'E-Wallet'? 'fas fa-caret-down' : 'fas fa-caret-right'\" ></i>{{ ' E-Wallet' | translate }}</span>\r\n      </a>\r\n\r\n      <li *ngIf=\"mci_project == false\" class=\"nested\" [class.expand]=\"showMenu === 'E-Wallet'\">\r\n          <ul class=\"submenu\">\r\n              <li id=\"new-line\" class=\"new-line\" *ngFor=\"let menu of sideBarMenu['E-Wallet']\">\r\n                  <!-- <a routerLink=\"{{menu.routerLink}}\" [routerLinkActive]=\"['router-link-active']\"\r\n                      class=\"list-group-item\">\r\n                      <span>{{menu.label}}</span>\r\n                  </a> -->\r\n                  <a [ngClass]=\"router.url == menu.routerLink ? 'list-group-item router-link-active' : 'list-group-item'\" (click) = \"handleChangePage(menu.routerLink)\">\r\n                    <span>{{ menu.label | translate }}</span>\r\n                  </a>\r\n              </li>\r\n          </ul>\r\n      </li>\r\n      <!-- SHIPPING -->\r\n      <a (click)=\"addExpandClass('Shipping')\" class=\"list-group-item\">\r\n        <span class=\"new-line\"><i [ngClass]=\"showMenu === 'Shipping'? 'fas fa-caret-down' : 'fas fa-caret-right'\" ></i>{{ ' Shipping' | translate }}</span>\r\n      </a>\r\n      <li class=\"nested\" [class.expand]=\"showMenu === 'Shipping'\">\r\n          <ul class=\"submenu\">\r\n              <li id=\"new-line\" class=\"new-line\" *ngFor=\"let menu of sideBarMenu['Shipping']\">\r\n                  <!-- <a routerLink=\"{{menu.routerLink}}\" [routerLinkActive]=\"['router-link-active']\"\r\n                      class=\"list-group-item\">\r\n                      <span>{{ menu.label | translate }}</span>\r\n                  </a> -->\r\n                  <a [ngClass]=\"router.url == menu.routerLink ? 'list-group-item router-link-active' : 'list-group-item'\" (click) = \"handleChangePage(menu.routerLink)\">\r\n                    <span>{{ menu.label | translate }}</span>\r\n                  </a>\r\n              </li>\r\n          </ul>\r\n      </li>\r\n    </div>\r\n\r\n\r\n    <div class=\"nested-menu\">\r\n      <p class=\"parent-menu\">Sales Management</p>\r\n    </div>\r\n    <div class=\"nested-menu child-menu\">\r\n      <a (click)=\"addExpandClass('Order History')\" class=\"list-group-item\">\r\n          <span class=\"new-line\"><i [ngClass]=\"showMenu === 'Order History'? 'fas fa-caret-down' : 'fas fa-caret-right'\" ></i>{{ ' Invoice History' | translate }}</span>\r\n      </a>\r\n      <li class=\"nested\" [class.expand]=\"showMenu === 'Order History'\">\r\n          <ul class=\"submenu\">\r\n              <li id=\"new-line\" class=\"new-line\" *ngFor=\"let menu of sideBarMenu['Order History']\">\r\n                  <!-- <a routerLink=\"{{menu.routerLink}}\" [routerLinkActive]=\"['router-link-active']\"\r\n                      class=\"list-group-item\">\r\n                      <div class=\"circle-custom\">\r\n                      </div>\r\n                      <span>{{ menu.label | translate }}</span>\r\n                  </a> -->\r\n                  <a [ngClass]=\"router.url == menu.routerLink ? 'list-group-item router-link-active' : 'list-group-item'\" (click) = \"handleChangePage(menu.routerLink)\">\r\n                    <span>{{ menu.label | translate }}</span>\r\n                  </a>\r\n              </li>\r\n          </ul>\r\n      </li>\r\n\r\n      <a *ngIf=\"mci_project == true\" (click)=\"addExpandClass('PO Number')\" class=\"list-group-item\">\r\n        <span class=\"new-line\"><i [ngClass]=\"showMenu === 'PO Number'? 'fas fa-caret-down' : 'fas fa-caret-right'\" ></i>{{ ' PO Number' | translate }}</span>\r\n      </a>\r\n      <li *ngIf=\"mci_project == true\" class=\"nested\" [class.expand]=\"showMenu === 'PO Number'\">\r\n          <ul class=\"submenu\">\r\n              <li id=\"new-line\" class=\"new-line\" *ngFor=\"let menu of sideBarMenu['PO Number']\">\r\n                  <!-- <a routerLink=\"{{menu.routerLink}}\" [routerLinkActive]=\"['router-link-active']\"\r\n                      class=\"list-group-item\">\r\n                      <div class=\"circle-custom\">\r\n                      </div>\r\n                      <span>{{ menu.label | translate }}</span>\r\n                  </a> -->\r\n                  <a [ngClass]=\"router.url == menu.routerLink ? 'list-group-item router-link-active' : 'list-group-item'\" (click) = \"handleChangePage(menu.routerLink)\">\r\n                    <span>{{ menu.label | translate }}</span>\r\n                  </a>\r\n              </li>\r\n          </ul>\r\n      </li>\r\n\r\n      <!-- <a routerLink=\"/administrator/salesorderreport\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\r\n        <span class=\"new-line\">{{ 'Sales Order' | translate }}</span>\r\n      </a> -->\r\n      <a *ngIf=\"mci_project == false\" (click)=\"addExpandClass('Sales Redemption')\" class=\"list-group-item\">\r\n        <span class=\"new-line\"><i [ngClass]=\"showMenu === 'Sales Redemption'? 'fas fa-caret-down' : 'fas fa-caret-right'\" ></i>{{ ' Sales Redemption' | translate }}</span>\r\n      </a>\r\n\r\n      <li *ngIf=\"mci_project == false\" class=\"nested\" [class.expand]=\"showMenu === 'Sales Redemption'\">\r\n          <ul class=\"submenu\">\r\n              <li id=\"new-line\" class=\"new-line\" *ngFor=\"let menu of sideBarMenu['Sales Redemption']\">\r\n                  <!-- <a routerLink=\"{{menu.routerLink}}\" [routerLinkActive]=\"['router-link-active']\"\r\n                      class=\"list-group-item\">\r\n                      <span>{{menu.label}}</span>\r\n                  </a> -->\r\n                  <a [ngClass]=\"router.url == menu.routerLink ? 'list-group-item router-link-active' : 'list-group-item'\" (click) = \"handleChangePage(menu.routerLink)\">\r\n                    <span>{{ menu.label | translate }}</span>\r\n                  </a>\r\n              </li>\r\n          </ul>\r\n      </li>\r\n\r\n      <a *ngIf=\"mci_project == true\" (click)=\"addExpandClass('Sales Redemption MCI')\" class=\"list-group-item\">\r\n        <span class=\"new-line\"><i [ngClass]=\"showMenu === 'Sales Redemption MCI'? 'fas fa-caret-down' : 'fas fa-caret-right'\" ></i>{{ ' Sales Redemption' | translate }}</span>\r\n      </a>\r\n\r\n      <li *ngIf=\"mci_project == true\" class=\"nested\" [class.expand]=\"showMenu === 'Sales Redemption MCI'\">\r\n          <ul class=\"submenu\">\r\n              <li id=\"new-line\" class=\"new-line\" *ngFor=\"let menu of sideBarMenu['Sales Redemption MCI']\">\r\n                  <!-- <a routerLink=\"{{menu.routerLink}}\" [routerLinkActive]=\"['router-link-active']\"\r\n                      class=\"list-group-item\">\r\n                      <span>{{menu.label}}</span>\r\n                  </a> -->\r\n                  <a [ngClass]=\"router.url == menu.routerLink ? 'list-group-item router-link-active' : 'list-group-item'\" (click) = \"handleChangePage(menu.routerLink)\">\r\n                    <span>{{ menu.label | translate }}</span>\r\n                  </a>\r\n              </li>\r\n          </ul>\r\n      </li>\r\n    </div>\r\n\r\n    <div class=\"nested-menu\">\r\n      <p class=\"parent-menu\">Point Management</p>\r\n    </div>\r\n    <div class=\"nested-menu child-menu\">\r\n      <!-- <a routerLink=\"/administrator/pointstransactionreport\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\r\n        <span class=\"new-line\">{{ 'Points Approval' | translate }}</span>\r\n      </a> -->\r\n      <a [ngClass]=\"router.url == '/administrator/pointstransactionreport' ? 'list-group-item router-link-active' : 'list-group-item'\" (click) = \"handleChangePage('/administrator/pointstransactionreport')\">\r\n        <span>{{ 'Points Approval' | translate }}</span>\r\n      </a>\r\n      <!-- <a routerLink=\"/administrator/pointstransactionapproval\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\r\n        <span class=\"new-line\">{{ 'Points Transaction Report' | translate }}</span>\r\n      </a> -->\r\n      <!-- <a routerLink=\"/administrator/pointsmodels\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\r\n        <span class=\"new-line\">{{ 'Transaction Summary' | translate }}</span>\r\n      </a> -->\r\n      <a [ngClass]=\"router.url == '/administrator/pointsmodels' ? 'list-group-item router-link-active' : 'list-group-item'\" (click) = \"handleChangePage('/administrator/pointsmodels')\">\r\n        <span>{{ 'Transaction Summary' | translate }}</span>\r\n      </a>\r\n      <!-- Point Movement -->\r\n      <!-- <a (click)=\"addExpandClass('Point Movement')\" class=\"list-group-item\">\r\n          <span class=\"new-line\"><i [ngClass]=\"showMenu === 'Point Movement'? 'fas fa-caret-down' : 'fas fa-caret-right'\" ></i>{{ ' Point Movement' | translate }}</span>\r\n      </a>\r\n      <li class=\"nested\" [class.expand]=\"showMenu === 'Point Movement'\">\r\n          <ul class=\"submenu\">\r\n              <li class=\"new-line\" *ngFor=\"let menu of sideBarMenu['Point Movement']\">\r\n                  <a routerLink=\"{{menu.routerLink}}\" [routerLinkActive]=\"['router-link-active']\"\r\n                      class=\"list-group-item\">\r\n                      <span>{{ menu.label | translate }}</span>\r\n                  </a>\r\n              </li>\r\n\r\n          </ul>\r\n      </li> -->\r\n    </div>\r\n\r\n    <!-- <div class=\"nested-menu\">\r\n      <p class=\"parent-menu\">Finance</p>\r\n    </div>\r\n    <div class=\"nested-menu child-menu\">\r\n      <li class=\"new-line\">\r\n        <a routerLink=\"/administrator/paymentreport\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\r\n          <span class=\"new-line\">{{ 'Payment' | translate }}</span>\r\n        </a>\r\n    \r\n        <a routerLink=\"/administrator/escrowreport\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\r\n          <span class=\"new-line\">{{ 'Escrow Report' | translate }}</span>\r\n        </a>\r\n      </li>\r\n    </div> -->\r\n\r\n    <div class=\"nested-menu\">\r\n      <p class=\"parent-menu\">Report Management</p>\r\n    </div>\r\n    <div class=\"nested-menu child-menu\">\r\n        <li id=\"new-line\" class=\"new-line\" *ngFor=\"let menu of sideBarMenu['Reportnew']\">\r\n            <!-- <a routerLink=\"{{menu.routerLink}}\" [routerLinkActive]=\"['router-link-active']\"\r\n                class=\"list-group-item\">\r\n                <span>{{ menu.label | translate }}</span>\r\n            </a> -->\r\n            <a [ngClass]=\"router.url == menu.routerLink ? 'list-group-item router-link-active' : 'list-group-item'\" (click) = \"handleChangePage(menu.routerLink)\">\r\n              <span>{{ menu.label | translate }}</span>\r\n            </a>\r\n        </li>\r\n    </div>\r\n\r\n  </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/components/sidebar/sidebar.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/components/sidebar/sidebar.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"sidebar\" [ngClass]=\"{sidebarPushRight: isActive, collapsed: collapsed}\" xmlns=\"\">\r\n    <div class=\"list-group\">\r\n\r\n        <div class=\"nested-menu\">\r\n            <a (click)=\"addExpandClass('Order History')\" class=\"list-group-item\">\r\n                <!-- <i class=\"icon-order_history_merchant\" style=\"font-size: 40px;\"> </i> -->\r\n                <span class=\"new-line\">{{ 'Invoice History' | translate }}</span>\r\n            </a>\r\n            <li class=\"nested\" [class.expand]=\"showMenu === 'Order History'\">\r\n                <ul class=\"submenu\">\r\n                    <li class=\"new-line\" *ngFor=\"let menu of sideBarMenu['Order History']\">\r\n                        <a routerLink=\"{{menu.routerLink}}\" [routerLinkActive]=\"['router-link-active']\"\r\n                            class=\"list-group-item\">\r\n                            <div class=\"circle-custom\">\r\n                            </div>\r\n                            <span>{{ menu.label | translate }}</span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n        </div>\r\n        \r\n        <!-- <a [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\" (click)=\"orderhistory()\">\r\n            <i class=\"icon-order_history_merchant\" style=\"font-size: 40px;\"></i>\r\n            <span class=\"new-line\">{{ 'Order History' | translate }}</span>\r\n        </a> -->\r\n        <div class=\"nested-menu\">\r\n            <a (click)=\"addExpandClass('Member')\" style=\"display: block;\" class=\"list-group-item\">\r\n                <!-- <i class=\"icon-member\" style=\"font-size: 45px;\"></i> -->\r\n                <span>{{ 'Member' | translate }}</span>\r\n            </a>\r\n\r\n\r\n            <li class=\"nested\" [class.expand]=\"showMenu === 'Member'\">\r\n                <ul class=\"submenu\">\r\n                    <li class=\"new-line\" *ngFor=\"let menu of sideBarMenu['Member']\">\r\n                        <a routerLink=\"{{menu.routerLink}}\" [routerLinkActive]=\"['router-link-active']\"\r\n                            class=\"list-group-item\">\r\n                            <span>{{ menu.label | translate }}</span>\r\n                        </a>\r\n                    </li>\r\n\r\n                </ul>\r\n            </li>\r\n\r\n        </div>\r\n         <!-- Shipping Report -->\r\n        <a routerLink=\"/administrator/shippingreport\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\r\n            <!-- <i class=\"icon-merchant\" style=\"font-size: 40px;\"></i> -->\r\n            <span class=\"new-line\">{{ 'Shipping' | translate }}</span>\r\n        </a>\r\n        <a routerLink=\"/administrator/escrowreport\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\r\n            <!-- <i class=\"icon-merchant\" style=\"font-size: 40px;\"></i> -->\r\n            <span class=\"new-line\">{{ 'Escrow Report' | translate }}</span>\r\n        </a>\r\n             <!--End of Shipping Report -->\r\n               <!-- Sales Order Report -->\r\n        <a routerLink=\"/administrator/salesorderreport\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\r\n            <!-- <i class=\"icon-merchant\" style=\"font-size: 40px;\"></i> -->\r\n            <span class=\"new-line\">{{ 'Sales Order' | translate }}</span>\r\n        </a>\r\n        \r\n             <!--End of Sales Order Report -->\r\n                  <!-- Payment Report -->\r\n        <a routerLink=\"/administrator/paymentreport\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\r\n            <!-- <i class=\"icon-merchant\" style=\"font-size: 40px;\"></i> -->\r\n            <span class=\"new-line\">{{ 'Payment' | translate }}</span>\r\n        </a>\r\n        <!--End of Payment Report -->\r\n\r\n        <!-- Stock Movement Report -->\r\n        <div class=\"nested-menu\">\r\n            <a (click)=\"addExpandClass('Sales Redemption')\" class=\"list-group-item\">\r\n                <!-- <i class=\"icon-product\" style=\"font-size: 40px;\"></i> -->\r\n                <span class=\"new-line\">{{ 'Sales Redemption' | translate }}</span>\r\n            </a>\r\n\r\n            <li class=\"nested\" [class.expand]=\"showMenu === 'Sales Redemption'\">\r\n                <ul class=\"submenu\">\r\n                    <li class=\"new-line\" *ngFor=\"let menu of sideBarMenu['Sales Redemption']\">\r\n                        <a routerLink=\"{{menu.routerLink}}\" [routerLinkActive]=\"['router-link-active']\"\r\n                            class=\"list-group-item\">\r\n                            <span>{{menu.label}}</span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n        </div>\r\n        <!--End of Stock Movement Report -->\r\n\r\n        <!-- PRODUCTS -->\r\n        <div class=\"nested-menu\">\r\n            <a (click)=\"addExpandClass('Products')\" class=\"list-group-item\">\r\n                <!-- <i class=\"icon-product\" style=\"font-size: 40px;\"></i> -->\r\n                <span class=\"new-line\">{{ 'Products' | translate }}</span>\r\n            </a>\r\n\r\n            <li class=\"nested\" [class.expand]=\"showMenu === 'Products'\">\r\n                <ul class=\"submenu\">\r\n                    <li class=\"new-line\" *ngFor=\"let menu of sideBarMenu['Products']\">\r\n                        <a routerLink=\"{{menu.routerLink}}\" [routerLinkActive]=\"['router-link-active']\"\r\n                            class=\"list-group-item\">\r\n                            <span>{{menu.label}}</span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n        </div>\r\n        <!-- END OF PRODUCTS -->\r\n        <!-- MERCHANTS -->\r\n        <!-- <a routerLink=\"/administrator/merchant\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\r\n            <i class=\"icon-merchant\" style=\"font-size: 40px;\"></i>\r\n            <span class=\"new-line\">{{ 'Merchants' | translate }}</span>\r\n        </a> -->\r\n        <!-- END OF MERCHANTS -->\r\n        <!-- OUTLETS -->\r\n        <!-- <a routerLink=\"/administrator/outletsadmin\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\r\n            <i class=\"icon-outlets\" style=\"font-size: 40px;\"></i>\r\n            <span class=\"new-line\">{{ 'Outlets' | translate }}</span>\r\n        </a> -->\r\n        <!-- END OF OUTLETS -->\r\n\r\n        <div class=\"nested-menu\">\r\n            <a (click)=\"addExpandClass('Evoucher')\" class=\"list-group-item\">\r\n            \r\n                <!-- <i class=\"icon-e-voucher\" style=\"font-size: 40px;\"></i> -->\r\n                <span class=\"new-line\">{{ 'E-Voucher' | translate }}</span>\r\n            </a>\r\n            <li class=\"nested\" [class.expand]=\"showMenu === 'Evoucher'\">\r\n                <ul class=\"submenu\">\r\n                    <li class=\"new-line\" *ngFor=\"let menu of sideBarMenu['Evoucher']\">\r\n                        <a routerLink=\"{{menu.routerLink}}\" [routerLinkActive]=\"['router-link-active']\"\r\n                            class=\"list-group-item\">\r\n                            <span>{{ menu.label | translate }}</span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n        </div>\r\n        <div class=\"nested-menu\">\r\n            <a (click)=\"addExpandClass('Loyalty')\" class=\"list-group-item\">\r\n           \r\n                <!-- <i class=\"icon-loyalty\" style=\"font-size: 40px;\"></i> -->\r\n                <span class=\"new-line\">{{ 'SIG Point report' | translate }}</span>\r\n            </a>\r\n            <li class=\"nested\" [class.expand]=\"showMenu === 'Loyalty'\">\r\n                <ul class=\"submenu\">\r\n                    <li class=\"new-line\" *ngFor=\"let menu of sideBarMenu['Loyalty']\">\r\n                        <a routerLink=\"{{menu.routerLink}}\" [routerLinkActive]=\"['router-link-active']\"\r\n                            class=\"list-group-item\">\r\n                            <span>{{ menu.label | translate }}</span>\r\n                        </a>\r\n                    </li>\r\n\r\n                </ul>\r\n            </li>\r\n        </div>\r\n\r\n        <div class=\"nested-menu\">\r\n            <a (click)=\"addExpandClass('Point Movement')\" class=\"list-group-item\">\r\n           \r\n                <!-- <i class=\"icon-loyalty\" style=\"font-size: 40px;\"></i> -->\r\n                <span class=\"new-line\">{{ 'Point Movement' | translate }}</span>\r\n            </a>\r\n            <li class=\"nested\" [class.expand]=\"showMenu === 'Point Movement'\">\r\n                <ul class=\"submenu\">\r\n                    <li class=\"new-line\" *ngFor=\"let menu of sideBarMenu['Point Movement']\">\r\n                        <a routerLink=\"{{menu.routerLink}}\" [routerLinkActive]=\"['router-link-active']\"\r\n                            class=\"list-group-item\">\r\n                            <span>{{ menu.label | translate }}</span>\r\n                        </a>\r\n                    </li>\r\n\r\n                </ul>\r\n            </li>\r\n        </div>\r\n\r\n        \r\n\r\n        <!-- <div class=\"nested-menu\">\r\n            <a (click)=\"addExpandClass('Payment')\" class=\"list-group-item\"\r\n                routerLink=\"/administrator/paymentgatewayadmin\">\r\n               \r\n                <i class=\"icon-payment-gateway\" style=\"font-size: 40px;\"></i>\r\n                <span class=\"new-line\">{{ 'Payment Gateway' | translate }}</span>\r\n            </a>\r\n        </div>\r\n\r\n     \r\n\r\n        <div class=\"nested-menu\">\r\n            <a (click)=\"addExpandClass('Notification')\" class=\"list-group-item\">\r\n                <i class=\"icon-notification\" style=\"font-size: 40px;\"></i>\r\n                <span>{{ 'Notification' | translate }}</span>\r\n            </a>\r\n            <li class=\"nested\" [class.expand]=\"showMenu === 'Notification'\">\r\n                <ul class=\"submenu\">\r\n                    <li class=\"new-line\" *ngFor=\"let menu of sideBarMenu['Notification']\">\r\n                        <a routerLink=\"{{menu.routerLink}}\" [routerLinkActive]=\"['router-link-active']\"\r\n                            class=\"list-group-item\">\r\n                            <span>{{ menu.label | translate }}</span>\r\n                        </a>\r\n                    </li>\r\n\r\n                </ul>\r\n            </li>\r\n        </div>\r\n\r\n        <a routerLink=\"/administrator/promotion\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\r\n            <i class=\"icon-promotion\" style=\"font-size: 40px;\"></i>\r\n            <span class=\"new-line\">{{ 'Promotion' | translate }}</span>\r\n        </a> -->\r\n\r\n        <!-- <a routerLink=\"/administrator/packagebundle\" [routerLinkActive]=\"['router-link-active']\"\r\n            class=\"list-group-item\">\r\n         \r\n            <i class=\"icon-package-bundle\" style=\"font-size: 40px;\"></i>\r\n            <span class=\"new-line\">{{ 'Package Bundle' | translate }}</span>\r\n        </a>\r\n\r\n\r\n        <a routerLink=\"/administrator/versioning\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\r\n         \r\n            <i class=\"icon-versioning\" style=\"font-size: 40px;\"></i>\r\n            <span class=\"new-line\">{{ 'Versioning' | translate }}</span>\r\n        </a>\r\n\r\n        <a routerLink=\"/administrator/courier-service-page\" [routerLinkActive]=\"['router-link-active']\"\r\n            class=\"list-group-item\">\r\n          \r\n            <i class=\"icon-courier-service\" style=\"font-size: 40px;\"></i>\r\n            <span class=\"new-line\">{{ 'Courier Service' | translate }}</span>\r\n        </a>\r\n        <a routerLink=\"/administrator/points-transaction\" [routerLinkActive]=\"['router-link-active']\"\r\n            class=\"list-group-item\">\r\n     \r\n            <i class=\"icon-point-transaction-rp\" style=\"font-size: 40px;\"></i>\r\n            <span class=\"new-line\">{{ 'Points Transaction' | translate }}</span>\r\n        </a>\r\n        <a routerLink=\"/administrator/escrow-transaction\" [routerLinkActive]=\"['router-link-active']\"\r\n            class=\"list-group-item\">\r\n      \r\n            <i class=\"icon-escrow-rp\" style=\"font-size: 40px;\"></i>\r\n            <span class=\"new-line\">{{ 'Escrow Transaction' | translate }}</span>\r\n        </a>\r\n        <a routerLink=\"/administrator/promocode\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\r\n            <i class=\"icon-promo-code\" style=\"font-size: 40px;\"></i>\r\n            <span class=\"new-line\">{{ 'Promo Code' | translate }}</span>\r\n        </a>\r\n        <a routerLink=\"/administrator/promousage\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\r\n            <i class=\"icon-promo-code\" style=\"font-size: 40px;\"></i>\r\n            <span class=\"new-line\">{{ 'Promo Usage' | translate }}</span>\r\n        </a>\r\n        <a routerLink=\"/administrator/promostatistic\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\r\n            <i class=\"icon-promo-code\" style=\"font-size: 40px;\"></i>\r\n            <span class=\"new-line\">{{ 'Promo Statistic' | translate }}</span>\r\n        </a>\r\n        <a routerLink=\"/administrator/articleadmin\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\r\n            <i class=\"icon-articles\" style=\"font-size: 40px;\"></i>\r\n            <span class=\"new-line\">{{ 'Articles' | translate }}</span>\r\n        </a>\r\n        <a routerLink=\"/administrator/categoryadmin\" [routerLinkActive]=\"['router-link-active']\"\r\n            class=\"list-group-item\">\r\n            <i><img src=\"assets/images/category.png\" style=\"width: 45px;\"></i>\r\n            <span class=\"new-line\">{{ 'Category' | translate }}</span>\r\n        </a>\r\n\r\n        <a routerLink=\"/administrator/emailadmin\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\r\n            <i class=\"icon-email-history\" style=\"font-size: 40px;\"></i>\r\n            <span class=\"new-line\">{{ 'Email History' | translate }}</span>\r\n        </a>\r\n\r\n\r\n        <a routerLink=\"/administrator/shoppingcartadmin\" [routerLinkActive]=\"['router-link-active']\"\r\n            class=\"list-group-item\">\r\n            <i class=\"icon-shopping-cart\" style=\"font-size: 40px;\"></i>\r\n            <span class=\"new-line\">{{ 'Shopping Cart' | translate }}</span>\r\n        </a>\r\n        <a routerLink=\"/administrator/mediaadmin\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\r\n            <i class=\"icon-media\" style=\"font-size: 40px;\"></i>\r\n            <span class=\"new-line\">{{ 'Media' | translate }}</span>\r\n        </a>\r\n        <a routerLink=\"/administrator/banneradmin\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\r\n            <i class=\"icon-banner\" style=\"font-size: 40px;\"></i>\r\n            <span class=\"new-line\">{{ 'Banner' | translate }}</span>\r\n        </a>\r\n\r\n        <div class=\"nested-menu\">\r\n            <a (click)=\"addExpandClass('Report')\" class=\"list-group-item\">\r\n                <i class=\"icon-report\" style=\"font-size: 40px;\"></i>\r\n                <span>{{ 'Report' | translate }}</span>\r\n            </a>\r\n            <li class=\"nested\" [class.expand]=\"showMenu === 'Report'\">\r\n                <ul class=\"submenu\">\r\n                    <li class=\"new-line\" *ngFor=\"let menu of sideBarMenu['Report']\">\r\n                        <a routerLink=\"{{menu.routerLink}}\" [routerLinkActive]=\"['router-link-active']\"\r\n                            class=\"list-group-item\">\r\n                            <span>{{ menu.label | translate }}</span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n        </div>\r\n\r\n        <a (click)=\"rltAndLtr()\" class=\"list-group-item\">\r\n            <i><img src=\"assets/images/RTL,LTR.svg\" style=\"width: 45px;\"></i>\r\n            <span>\r\n                RTL/LTR\r\n            </span>\r\n        </a> -->\r\n        <!-- \r\n        <a routerLink=\"/login/out\" (click)=\"onLoggedout()\" [routerLinkActive]=\"['router-link-active']\"\r\n            class=\"list-group-item\">\r\n            <i><img src=\"assets/images/logout.png\"></i>\r\n            Logout\r\n        </a> -->\r\n    </div>\r\n</nav>"

/***/ }),

/***/ "./src/app/layout/administrator/administrator-route-path.ts":
/*!******************************************************************!*\
  !*** ./src/app/layout/administrator/administrator-route-path.ts ***!
  \******************************************************************/
/*! exports provided: administratorRoutePath */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "administratorRoutePath", function() { return administratorRoutePath; });
var administratorRoutePath = [
    { path: 'frontpage', loadChildren: '../modules/frontpage/frontpage.module#FrontpageModule', label: 'frontpage' },
    { path: 'pagebaru', loadChildren: '../modules/page-baru/pagebaru.module#PagebaruModule', label: 'Page baru' },
    { path: 'articleadmin', loadChildren: '../modules/article/article.module#ArticleModule', label: 'article' },
    { path: 'categoryadmin', loadChildren: '../modules/category/category.module#CategoryModule', label: 'category' },
    { path: 'dashboard', loadChildren: '../modules/dashboard/dashboard.module#DashboardModule', label: 'dashboard' },
    // { path: 'charts', loadChildren: '../modules/charts/charts.module#ChartsModule' , label: 'chart'},
    { path: 'emailadmin', loadChildren: '../modules/email/email.module#EmailModule', label: 'emailadmin' },
    // { path: 'tables', loadChildren: '../modules/tables/tables.module#TablesModule' , label: 'table'},
    /** Members Part */
    { path: 'membersummary', loadChildren: '../modules/admin_members/member-summary/membersummary.module#MemberSummaryModule', label: 'member' },
    { path: 'allmembersadmin', loadChildren: '../modules/admin_members/allmember/allmember.module#AllMemberModule', label: 'member' },
    { path: 'memberadmin', loadChildren: '../modules/member/member.module#MemberModule', label: 'merchant' },
    { path: 'datamember', loadChildren: '../modules/data-member/data-member.module#DataMemberModule', label: '' },
    { path: 'formmember', loadChildren: '../modules/form-member/form-member.module#FormMemberModule', label: '' },
    { path: 'order-bulk', loadChildren: '../modules/order-bulk/order-bulk.module#OrderBulkModule', label: '' },
    { path: 'merchantadmin', loadChildren: '../modules/merchant/merchant.module#MerchantModule', label: '' },
    { path: 'guestadmin', loadChildren: '../modules/guest/guest.module#GuestModule', label: '' },
    { path: 'membershipadmin', loadChildren: '../modules/membership/membership.module#MembershipModule', label: '' },
    { path: 'outletsadmin', loadChildren: '../modules/outlets/outlets.module#OutletsModule', label: '' },
    { path: 'pointsgroup', loadChildren: '../modules/points.group/points-group.module#PointsGroupModule', label: '' },
    { path: 'pointscampaign', loadChildren: '../modules/points.campaign/pointscampaign.module#PointsCampaignModule', label: '' },
    { path: 'promocampaign', loadChildren: '../modules/promo-campaign/promo-campaign.module#PromoCampaignModule', label: '' },
    { path: 'pointsmodels', loadChildren: '../modules/points.models/pointsmodels.module#PointsModelsModule', label: '' },
    // Point Movement
    { path: 'pointstransactionreport', loadChildren: '../modules/points-transaction-report/points-transaction-report.module#PointsTransactionReportModule', label: '' },
    { path: 'pointstransactionapproval', loadChildren: '../modules/points-transaction-approval/points-transaction-approval.module#PointsTransactionApprovalModule', label: '' },
    { path: 'pointmovement', loadChildren: '../modules/point-movement-product/point-movement-product.module#PointMovementProductModule', label: '' },
    { path: 'pointmovementorder', loadChildren: '../modules/point-movement-order/point-movement-order.module#PointMovementOrderModule', label: '' },
    // End of Point Movement
    { path: 'productadmin', loadChildren: '../modules/product/product.module#ProductModule', label: '' },
    { path: 'vouchers', loadChildren: '../modules/product-voucher/voucher.module#VouchersModule', label: '' },
    { path: 'category', loadChildren: '../modules/category/category.module#CategoryModule', label: '' },
    { path: 'evoucherstockreport', loadChildren: '../modules/evoucherstockreport/evoucher-stock-report/evoucher-stock-report.module#EvoucherStockReportModule', label: '' },
    { path: 'evouchersummaryreport', loadChildren: '../modules/product.evoucher/evoucher.module#EvoucherModule', label: '' },
    { path: 'evoucheradmin/detail', loadChildren: '../modules/product.evoucher/evoucher.module#EvoucherModule', label: '' },
    /** Order History part */
    { path: 'order-history-summary', loadChildren: '../modules/admin-portal/admin-order-history/order-history-summary/orderhistorysummary.module#OrderHistorySummaryModule', label: 'Order History summary' },
    { path: 'order-amount-history-summary', loadChildren: '../modules/admin-amount-order-history/order-amount-history-summary/orderamounthistorysummary.module#OrderAmountHistorySummaryModule', label: 'Order Amount History summary' },
    { path: 'orderhistoryadmin', loadChildren: '../merchant-portal/order-histories/order-histories.component', label: '' },
    { path: 'orderhistorysummaryadmin', loadChildren: '../modules/orderhistorysummary/orderhistorysummary.module#OrderhistorysummaryModule', label: '' },
    { path: 'orderhistoryallhistoryadmin', loadChildren: '../modules/orderhistoryallhistory/orderhistoryallhistory.module#OrderhistoryallhistoryModule', label: '' },
    { path: 'po-bulk-update', loadChildren: '../modules/po-bulk-update/po-bulk-update.module#POBulkUpdateModule', label: '' },
    { path: 'po-report', loadChildren: '../modules/po-report/po-report.module#POReportModule', label: '' },
    { path: 'orderhistoryreturnadmin', loadChildren: '../modules/orderhistoryreturn/orderhistoryreturn.module#OrderhistoryreturnModule', label: '' },
    { path: 'orderhistorycompleteadmin', loadChildren: '../modules/orderhistorycomplete/orderhistorycomplete.module#OrderhistorycompleteModule', label: '' },
    { path: 'orderhistorysuccessadmin', loadChildren: '../modules/orderhistorysuccess/orderhistorysuccess.module#OrderhistorysuccessModule', label: '' },
    { path: 'orderhistorypendingadmin', loadChildren: '../modules/orderhistorypending/orderhistorypending.module#OrderhistorypendingModule', label: '' },
    { path: 'orderhistorycanceladmin', loadChildren: '../modules/orderhistorycancel/orderhistorycancel.module#OrderhistorycancelModule', label: '' },
    { path: 'orderhistoryfailedadmin', loadChildren: '../modules/orderhistoryfailed/orderhistoryfailed.module#OrderhistoryfailedModule', label: '' },
    /** REPORT PART */
    { path: 'cart-abandonment-summary', loadChildren: '../modules/admin-report/cartabandonment/cartabandonmentsummary.module#CartAbandonmentSummaryModule', label: 'Cart Abandonment Summary ' },
    { path: 'activity-report', loadChildren: '../modules/admin-report/activity-report/activity-report-summary.module#ActivityReportSummaryModule', label: 'Activity Report Summary ' },
    { path: 'product-activity-report', loadChildren: '../modules/admin-report/product-activity-report/product-activity-report-summary.module#ProductActivityReportSummaryModule', label: 'Product Activity Report Summary ' },
    { path: 'member-demography-report', loadChildren: '../modules/admin-report/member-demography-report/member-demography-report.module#MemberDemographyReportModule', label: 'Member Demography Report Summary ' },
    // { path: 'orderhistoryerroradmin', loadChildren: '../modules/orderhistoryerror/orderhistoryerror.module#OrderhistoryerrorModule' , label: ''},
    { path: 'orderhistorywaitingadmin', loadChildren: '../modules/orderhistorywaiting/orderhistorywaiting.module#OrderhistorywaitingModule', label: '' },
    { path: 'shoppingcartadmin', loadChildren: '../modules/shoppingcart/shoppingcart.module#ShoppingcartModule', label: '' },
    { path: 'mediaadmin', loadChildren: '../modules/media/media.module#MediaModule', label: '' },
    { path: 'notificationsadmin', loadChildren: '../modules/notification/notification.module#NotificationModule', label: '' },
    { path: 'notificationbroadcastadmin', loadChildren: '../modules/notification.broadcast/broadcast.module#BroadcastModule', label: '' },
    { path: 'notificationsettingadmin', loadChildren: '../modules/notification.setting/notificationsetting.module#NotificationsettingModule', label: '' },
    { path: 'paymentgatewayadmin', loadChildren: '../modules/paymentgateway/paymentgateway.module#PaymentGatewayModule', label: '' },
    { path: 'banneradmin', loadChildren: '../modules/banner/banner.module#BannerModule', label: '' },
    { path: 'reportadmin', loadChildren: '../modules/report/report.module#ReportModule', label: '' },
    { path: 'notificationgroupadmin', loadChildren: '../modules/notificationgroup/notificationgroup.module#NotificationGroupModule', label: '' },
    { path: 'notificationmessageadmin', loadChildren: '../modules/notificationmessage/notificationmessage.module#NotificationMessageModule', label: '' },
    { path: 'forms', loadChildren: '../modules/admin-portal/form/form.module#FormModule', label: '' },
    { path: 'bs-element', loadChildren: '../modules/bs-element/bs-element.module#BsElementModule', label: '' },
    { path: 'grid', loadChildren: '../modules/grid/grid.module#GridModule', label: '' },
    { path: 'components', loadChildren: '../modules/bs-component/bs-component.module#BsComponentModule', label: '' },
    // { path: 'blank-page', loadChildren: '../modules/blank-page/blank-page.module#BlankPageModule' },
    { path: 'promocode', loadChildren: '../modules/code-page/code-page.module#CodePageModule' },
    { path: 'courier-service-page', loadChildren: '../modules/courier-service-page/courier-service-page.module#CourierServicePageModule', label: 'courier-service-page' },
    { path: 'points-transaction', loadChildren: '../modules/points-transaction/points-transaction.module#PointstransactionModule', label: 'points-transaction-history' },
    { path: 'escrow-transaction', loadChildren: '../modules/escrow-transaction/escrow-transaction.module#EscrowTransactionModule', label: 'escrow-transaction-history' },
    { path: 'promotion', loadChildren: '../modules/promotion/promotion.module#PromotionModule', label: 'Promotion' },
    { path: 'reporterror', loadChildren: '../modules/reporterror/reporterror.module#ReporterrorModule', label: 'Reporterror' },
    // { path: 'localstorage', loadChildren: '../modules/local-storage/local-storage.module#LocalstorageModule' , label: 'localstorage'},
    { path: 'promodeals', loadChildren: '../modules/promodeals/promodeals.module#PromodealsModule', label: 'localstorage' },
    { path: 'packagebundle', loadChildren: '../modules/packagebundle/packagebundle.module#PackagebundleModule', label: 'packagebundle' },
    { path: 'versioning', loadChildren: '../modules/versioning/versioning.module#VersioningModule', label: 'versioning' },
    { path: 'merchant', loadChildren: '../modules/merchant/merchant.module#MerchantModule', label: 'merchant' },
    { path: 'shippingreport', loadChildren: '../modules/admin-shipping/admin-shipping.module#AdminShippingModule', label: 'admin-shipping' },
    { path: 'deliveryprocess', loadChildren: '../modules/delivery-process/delivery-process.module#DeliveryProcessModule', label: 'delivery-process' },
    { path: 'packinglist', loadChildren: '../modules/packinglist/packinglist.module#PackingListModule', label: 'packinglist' },
    { path: 'shippingbulkprocess', loadChildren: '../modules/shipping-bulk-process/shipping-bulk-process.module#ShippingBulkProcessModule', label: '' },
    { path: 'salesorderreport', loadChildren: '../modules/admin-salesorder/admin-salesorder.module#AdminSalesorderModule', label: 'admin-salesorder' },
    { path: 'paymentreport', loadChildren: '../modules/admin-payment/admin-payment.module#AdminPaymentModule', label: 'admin-payment' },
    { path: 'escrowreport', loadChildren: '../modules/admin-escrow-report/admin-escrow-report.module#AdminEscrowReportModule', label: 'admin-escrow-report' },
    { path: 'merchantescrowreport', loadChildren: '../modules/admin-escrow-by-merchant/admin-escrow-by-merchant.module#AdminEscrowByMerchantModule', label: 'admin-escrow-by-merchant' },
    //balance all merchant
    { path: 'allbalancemerchant', loadChildren: '../modules/admin-all-balance/admin-all-balance.module#AdminAllBalanceModule', label: 'admin-all-balance' },
    //SALES ORDER MERCHANT
    { path: 'merchantsalesorder', loadChildren: '../modules/admin-merchant-sales-order/admin-merchant-sales-order.module#AdminMerchantSalesOrderModule', label: 'admin-all-balance' },
    { path: 'promousage', loadChildren: '../modules/admin-promo-usage/admin-promo-usage.module#AdminPromoUsageModule', label: 'admin-promo-usage' },
    { path: 'promostatistic', loadChildren: '../modules/admin-promo-statistic/admin-promo-statistic.module#AdminPromoStatisticModule', label: 'admin-promo-statistic' },
    { path: 'salesredemption', loadChildren: '../modules/admin-stock/admin-stock.module#AdminStockModule', label: 'admin-stock' },
    { path: 'salesredemptionbast', loadChildren: '../modules/admin-bast/admin-bast.module#AdminBastModule', label: 'admin-bast' },
    // { path: 'bast', loadChildren: '../modules/admin-stock/bast/bast.module#BastModule', label: 'bast' },
    { path: 'crr', loadChildren: '../modules/admin-crr/crr.module#CrrModule', label: 'Crr' },
    { path: 'cnr', loadChildren: '../modules/admin-cnr/cnr.module#CnrModule', label: 'Cnr' },
    { path: 'salesorder', loadChildren: '../modules/salesorder/salesorder.module#SalesorderModule', label: 'salesorder' },
    { path: 'evouchersalesreport', loadChildren: '../modules/evouchersalesreport/evoucher-sales-report/evoucher-sales-report.module#EvoucherSalesReportModule', label: 'evouchersalesreport' },
    { path: 'ewalletsalesreport', loadChildren: '../modules/ewalletsalesreport/ewallet-sales-report.module#EwalletSalesReportModule', label: 'ewallet-sales-report' },
    { path: 'evouchersales', loadChildren: '../modules/evouchersales/evoucher-sales.module#EvoucherSalesModule', label: 'evoucher-sales' },
    { path: 'financeinvoicereport', loadChildren: '../modules/finance-invoice-report/finance-invoice-report.module#FinanceInvoiceReportModule', label: 'finance-invoice-report' },
];


/***/ }),

/***/ "./src/app/layout/administrator/administrator-routing.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/layout/administrator/administrator-routing.module.ts ***!
  \**********************************************************************/
/*! exports provided: AdministratorRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdministratorRoutingModule", function() { return AdministratorRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _administrator_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./administrator.component */ "./src/app/layout/administrator/administrator.component.ts");
/* harmony import */ var _administrator_route_path__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./administrator-route-path */ "./src/app/layout/administrator/administrator-route-path.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    { path: '', component: _administrator_component__WEBPACK_IMPORTED_MODULE_2__["AdministratorComponent"], children: _administrator_route_path__WEBPACK_IMPORTED_MODULE_3__["administratorRoutePath"] },
];
var AdministratorRoutingModule = /** @class */ (function () {
    function AdministratorRoutingModule() {
    }
    AdministratorRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AdministratorRoutingModule);
    return AdministratorRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/administrator/administrator.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/layout/administrator/administrator.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  transition: margin-left 0.2s ease-in-out;\n  font-family: \"Poppins\", sans-serif;\n}\n\n.main-container {\n  top: 0px;\n  margin-left: 200px;\n  padding: 40px 15px 0;\n  -ms-overflow-x: hidden;\n  overflow-x: hidden;\n  overflow-y: scroll;\n  overflow: hidden;\n  background-color: #252B37;\n  height: 100%;\n}\n\n.collapsed {\n  margin-left: 100px;\n}\n\n@media screen and (max-width: 992px) {\n  .main-container {\n    margin-left: 0px;\n    padding-top: 40px;\n    margin-top: -10vw;\n  }\n}\n\n@media print {\n  .main-container {\n    margin-top: 0px !important;\n    margin-left: 0px !important;\n  }\n\n  app-header, app-nested-sidebar {\n    display: none;\n  }\n}\n\n.multiselect-dropdown .dropdown-btn {\n  background-color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L2FkbWluaXN0cmF0b3IvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxhZG1pbmlzdHJhdG9yXFxhZG1pbmlzdHJhdG9yLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvYWRtaW5pc3RyYXRvci9hZG1pbmlzdHJhdG9yLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBS0ksd0NBQUE7RUFDQSxrQ0FBQTtBQ0NKOztBREdBO0VBQ0ksUUFBQTtFQUNBLGtCQUFBO0VBQ0Esb0JBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtBQ0FKOztBREVBO0VBQ0ksa0JBQUE7QUNDSjs7QURDQTtFQUNJO0lBQ0ksZ0JBQUE7SUFDQSxpQkFBQTtJQUNBLGlCQUFBO0VDRU47QUFDRjs7QURBQTtFQUNJO0lBQ0ksMEJBQUE7SUFDQSwyQkFBQTtFQ0VOOztFRENFO0lBQ0ksYUFBQTtFQ0VOO0FBQ0Y7O0FEQ0E7RUFDSSx1QkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L2FkbWluaXN0cmF0b3IvYWRtaW5pc3RyYXRvci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIioge1xyXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiBtYXJnaW4tbGVmdCAwLjJzIGVhc2UtaW4tb3V0O1xyXG4gICAgLW1vei10cmFuc2l0aW9uOiBtYXJnaW4tbGVmdCAwLjJzIGVhc2UtaW4tb3V0O1xyXG4gICAgLW1zLXRyYW5zaXRpb246IG1hcmdpbi1sZWZ0IDAuMnMgZWFzZS1pbi1vdXQ7XHJcbiAgICAtby10cmFuc2l0aW9uOiBtYXJnaW4tbGVmdCAwLjJzIGVhc2UtaW4tb3V0O1xyXG4gICAgdHJhbnNpdGlvbjogbWFyZ2luLWxlZnQgMC4ycyBlYXNlLWluLW91dDtcclxuICAgIGZvbnQtZmFtaWx5OiAnUG9wcGlucycsIHNhbnMtc2VyaWY7XHJcbiAgICAvLyBmb250LXdlaWdodDogMjAwO1xyXG59XHJcblxyXG4ubWFpbi1jb250YWluZXIge1xyXG4gICAgdG9wOiAwcHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMjAwcHg7XHJcbiAgICBwYWRkaW5nOiA0MHB4IDE1cHggMDtcclxuICAgIC1tcy1vdmVyZmxvdy14OiBoaWRkZW47XHJcbiAgICBvdmVyZmxvdy14OiBoaWRkZW47XHJcbiAgICBvdmVyZmxvdy15OiBzY3JvbGw7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzI1MkIzNztcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uY29sbGFwc2VkIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMDBweDtcclxufVxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTJweCkge1xyXG4gICAgLm1haW4tY29udGFpbmVyIHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMHB4O1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiA0MHB4O1xyXG4gICAgICAgIG1hcmdpbi10b3A6IC0xMHZ3O1xyXG4gICAgfVxyXG59XHJcbkBtZWRpYSBwcmludCB7XHJcbiAgICAubWFpbi1jb250YWluZXIge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDBweCAhaW1wb3J0YW50O1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAwcHggIWltcG9ydGFudDtcclxuICAgIH1cclxuXHJcbiAgICBhcHAtaGVhZGVyICwgYXBwLW5lc3RlZC1zaWRlYmFye1xyXG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5tdWx0aXNlbGVjdC1kcm9wZG93biAuZHJvcGRvd24tYnRuIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG5cclxuXHJcbiIsIioge1xuICAtd2Via2l0LXRyYW5zaXRpb246IG1hcmdpbi1sZWZ0IDAuMnMgZWFzZS1pbi1vdXQ7XG4gIC1tb3otdHJhbnNpdGlvbjogbWFyZ2luLWxlZnQgMC4ycyBlYXNlLWluLW91dDtcbiAgLW1zLXRyYW5zaXRpb246IG1hcmdpbi1sZWZ0IDAuMnMgZWFzZS1pbi1vdXQ7XG4gIC1vLXRyYW5zaXRpb246IG1hcmdpbi1sZWZ0IDAuMnMgZWFzZS1pbi1vdXQ7XG4gIHRyYW5zaXRpb246IG1hcmdpbi1sZWZ0IDAuMnMgZWFzZS1pbi1vdXQ7XG4gIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnNcIiwgc2Fucy1zZXJpZjtcbn1cblxuLm1haW4tY29udGFpbmVyIHtcbiAgdG9wOiAwcHg7XG4gIG1hcmdpbi1sZWZ0OiAyMDBweDtcbiAgcGFkZGluZzogNDBweCAxNXB4IDA7XG4gIC1tcy1vdmVyZmxvdy14OiBoaWRkZW47XG4gIG92ZXJmbG93LXg6IGhpZGRlbjtcbiAgb3ZlcmZsb3cteTogc2Nyb2xsO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjUyQjM3O1xuICBoZWlnaHQ6IDEwMCU7XG59XG5cbi5jb2xsYXBzZWQge1xuICBtYXJnaW4tbGVmdDogMTAwcHg7XG59XG5cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MnB4KSB7XG4gIC5tYWluLWNvbnRhaW5lciB7XG4gICAgbWFyZ2luLWxlZnQ6IDBweDtcbiAgICBwYWRkaW5nLXRvcDogNDBweDtcbiAgICBtYXJnaW4tdG9wOiAtMTB2dztcbiAgfVxufVxuQG1lZGlhIHByaW50IHtcbiAgLm1haW4tY29udGFpbmVyIHtcbiAgICBtYXJnaW4tdG9wOiAwcHggIWltcG9ydGFudDtcbiAgICBtYXJnaW4tbGVmdDogMHB4ICFpbXBvcnRhbnQ7XG4gIH1cblxuICBhcHAtaGVhZGVyLCBhcHAtbmVzdGVkLXNpZGViYXIge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cbn1cbi5tdWx0aXNlbGVjdC1kcm9wZG93biAuZHJvcGRvd24tYnRuIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/administrator/administrator.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/layout/administrator/administrator.component.ts ***!
  \*****************************************************************/
/*! exports provided: AdministratorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdministratorComponent", function() { return AdministratorComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/observerable/permission-observer */ "./src/app/services/observerable/permission-observer.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AdministratorComponent = /** @class */ (function () {
    function AdministratorComponent(route, castPermission) {
        var _this = this;
        this.route = route;
        this.castPermission = castPermission;
        this.castPermission.currentPermission.subscribe(function (permissionVal) {
            if (permissionVal != 'admin') {
                _this.route.navigateByUrl("/");
            }
        });
    }
    AdministratorComponent.prototype.ngOnInit = function () { };
    AdministratorComponent.prototype.receiveCollapsed = function ($event) {
        this.collapedSideBar = $event;
    };
    AdministratorComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_2__["PermissionObserver"] }
    ]; };
    AdministratorComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-administrator',
            template: __webpack_require__(/*! raw-loader!./administrator.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/administrator/administrator.component.html"),
            styles: [__webpack_require__(/*! ./administrator.component.scss */ "./src/app/layout/administrator/administrator.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_2__["PermissionObserver"]])
    ], AdministratorComponent);
    return AdministratorComponent;
}());



/***/ }),

/***/ "./src/app/layout/administrator/administrator.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/layout/administrator/administrator.module.ts ***!
  \**************************************************************/
/*! exports provided: AdministratorModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdministratorModule", function() { return AdministratorModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _administrator_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./administrator-routing.module */ "./src/app/layout/administrator/administrator-routing.module.ts");
/* harmony import */ var _administrator_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./administrator.component */ "./src/app/layout/administrator/administrator.component.ts");
/* harmony import */ var _components_sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/sidebar/sidebar.component */ "./src/app/layout/components/sidebar/sidebar.component.ts");
/* harmony import */ var _components_nested_sidebar_nested_sidebar_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/nested-sidebar/nested-sidebar.component */ "./src/app/layout/components/nested-sidebar/nested-sidebar.component.ts");
/* harmony import */ var _components_header_header_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/header/header.module */ "./src/app/layout/components/header/header.module.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _modules_bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../modules/bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






// import { HeaderComponent } from '../components/header/header.component';






var AdministratorModule = /** @class */ (function () {
    function AdministratorModule() {
    }
    AdministratorModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_administrator_component__WEBPACK_IMPORTED_MODULE_3__["AdministratorComponent"], _components_sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_4__["SidebarComponent"], _components_nested_sidebar_nested_sidebar_component__WEBPACK_IMPORTED_MODULE_5__["NestedSidebarComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _administrator_routing_module__WEBPACK_IMPORTED_MODULE_2__["AdministratorRoutingModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__["NgbDropdownModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_9__["FormBuilderTableModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_10__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__["NgbModule"],
                // FormBuilderTableModule,
                _modules_bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_11__["BsComponentModule"],
                _components_header_header_module__WEBPACK_IMPORTED_MODULE_6__["HeaderModule"],
            ]
        })
    ], AdministratorModule);
    return AdministratorModule;
}());



/***/ }),

/***/ "./src/app/layout/components/nested-sidebar/nested-sidebar.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/layout/components/nested-sidebar/nested-sidebar.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".new-line {\n  white-space: pre-wrap;\n  list-style-type: none;\n}\n\n.sidebar {\n  font-size: small;\n  box-shadow: 0 4px 1px 0 rgba(0, 0, 0, 0.01), 0 3px 5px 0 rgba(0, 0, 0, 0.19);\n  border-radius: 0;\n  position: fixed;\n  z-index: 9999;\n  left: 235px;\n  width: 200px;\n  height: 100%;\n  margin-left: -235px;\n  margin-bottom: 48px;\n  border: none;\n  border-radius: 0;\n  overflow-y: auto;\n  background-color: #2b3240;\n  padding-bottom: 40px;\n  padding-top: 22px;\n  white-space: nowrap;\n  transition: all 0.2s ease-in-out;\n  text-align: center;\n  margin-top: 19px;\n}\n\n.sidebar .list-group {\n  padding-bottom: 20px;\n}\n\n.sidebar .list-group a.list-group-item {\n  background: #2b3240;\n  border: 0;\n  border-radius: 0;\n  color: #ffffff;\n  text-decoration: none;\n  padding-top: 15px;\n  padding-bottom: 15px;\n  padding-right: 5px;\n  padding-left: 10px;\n  text-align: left;\n  vertical-align: 10px;\n}\n\n.sidebar .list-group a.list-group-item > i {\n  display: block;\n  font-family: \"fontello\";\n  font-style: normal;\n}\n\n.sidebar .list-group a.list-group-item .fa {\n  margin-right: 10px;\n}\n\n.sidebar .list-group a.list-group-item > p {\n  margin-top: 5px;\n}\n\n.sidebar .list-group a.list-group-item > p:active {\n  color: #56a4ff;\n}\n\n.sidebar .list-group a:hover {\n  background: #3b4558;\n  color: #56a4ff;\n  border-left: 2px #56a4ff solid;\n}\n\n.sidebar .list-group div.list-group-item {\n  background: #2b3240;\n  border: 0;\n  border-radius: 0;\n  color: #ffffff;\n  text-decoration: none;\n  padding-top: 15px;\n  padding-bottom: 15px;\n  padding-right: 5px;\n  padding-left: 10px;\n  text-align: left;\n  vertical-align: 10px;\n}\n\n.sidebar .list-group div.list-group-item > i {\n  display: block;\n  font-family: \"fontello\";\n  font-style: normal;\n}\n\n.sidebar .list-group div.list-group-item .fa {\n  margin-right: 10px;\n}\n\n.sidebar .list-group div.list-group-item > p {\n  margin-top: 5px;\n}\n\n.sidebar .list-group div.list-group-item > p:active {\n  color: #56a4ff;\n}\n\n.sidebar .list-group div.list-group-item:hover {\n  background: #3b4558;\n  color: #56a4ff;\n  border-left: 2px #56a4ff solid;\n}\n\n.sidebar .list-group a.router-link-active {\n  background: #3b4558;\n  color: #56a4ff;\n  border-left: 2px #56a4ff solid;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n}\n\n.sidebar .list-group .header-fields {\n  padding-top: 10px;\n}\n\n.sidebar .list-group .header-fields > .list-group-item:first-child {\n  border-top: 1px solid rgba(255, 255, 255, 0.2);\n}\n\n.sidebar .list-group img {\n  width: 35px;\n}\n\n.sidebar p {\n  margin-bottom: 0px;\n  margin-top: 0px;\n}\n\n.sidebar .sidebar-dropdown *:focus {\n  border-radius: none;\n  border: none;\n}\n\n.sidebar .sidebar-dropdown .panel-title {\n  font-size: 1rem;\n  height: 50px;\n  margin-bottom: 0;\n}\n\n.sidebar .sidebar-dropdown .panel-title a {\n  color: #999;\n  text-decoration: none;\n  font-weight: 400;\n  background: #2b3240;\n}\n\n.sidebar .sidebar-dropdown .panel-title a span {\n  position: relative;\n  display: block;\n  padding: 0.75rem 1.5rem;\n  padding-top: 1rem;\n}\n\n.sidebar .sidebar-dropdown .panel-title a:hover,\n.sidebar .sidebar-dropdown .panel-title a:focus {\n  color: #fff;\n  outline: none;\n  outline-offset: -2px;\n}\n\n.sidebar .sidebar-dropdown .panel-title:hover {\n  background: #212631;\n}\n\n.sidebar .sidebar-dropdown .panel-collapse {\n  border-radius: 0;\n  border: none;\n}\n\n.sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item {\n  border-radius: 0;\n  background-color: #2b3240;\n  border: 0 solid transparent;\n}\n\n.sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item a {\n  color: #999;\n}\n\n.sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item a:hover {\n  color: #fff;\n}\n\n.sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item:hover {\n  background: #212631;\n}\n\n.sidebar .list-group > a.list-group-item .fa {\n  display: block;\n  clear: both;\n  font-size: 36px;\n  margin-right: 0px;\n}\n\n.sidebar .nested-menu .list-group-item {\n  cursor: pointer;\n}\n\n.sidebar .nested-menu .parent-menu {\n  color: white;\n  text-transform: uppercase;\n  font-size: 11px;\n  text-align: left;\n  padding-left: 10px;\n}\n\n.sidebar .nested-menu i img {\n  width: 45px;\n  margin-bottom: 5px;\n}\n\n.sidebar .nested-menu > a.list-group-item .fa {\n  display: block;\n  clear: both;\n  font-size: 36px;\n  margin-right: 0px;\n}\n\n.sidebar .nested-menu .nested {\n  list-style-type: none;\n}\n\n.sidebar .nested-menu ul.submenu {\n  display: none;\n  height: 0;\n  margin: 0;\n  padding: 0;\n  background: #212631;\n}\n\n.sidebar .nested-menu .expand ul.submenu {\n  display: block;\n  height: auto;\n  background: #212631;\n}\n\n.sidebar .nested-menu .expand ul.submenu a.list-group-item {\n  border-bottom: 1px solid rgba(255, 255, 255, 0.1);\n  padding-left: 20px;\n}\n\n.sidebar .nested-menu .expand ul.submenu a.list-group-item .circle-custom {\n  width: 1px;\n  border-radius: 50%;\n  background-color: #247de5;\n}\n\n.sidebar .nested-menu .expand ul.submenu a.list-group-item span {\n  margin-bottom: 5px;\n  color: #9ea6b7;\n  padding: 0px;\n  list-style: circle;\n}\n\n.sidebar .nested-menu .expand ul.submenu a.list-group-item.router-link-active {\n  background: #3b4558;\n  color: #56a4ff;\n  border-left: 2px #56a4ff solid;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n}\n\n.sidebar .nested-menu .expand ul.submenu a.list-group-item.router-link-active span {\n  color: #56a4ff;\n}\n\n.sidebar .nested-menu .expand ul.submenu a.list-group-item:hover p {\n  color: white;\n}\n\n.sidebar .nested-menu .expand ul.submenu li p {\n  color: #627190;\n  padding: 10px;\n  display: block;\n}\n\n.sidebar .nested-menu .expand ul.submenu p {\n  color: #ffffff;\n}\n\n.sidebar .child-menu {\n  margin-left: 10px;\n  margin-bottom: 20px;\n}\n\n@media (max-width: 992px) {\n  /* smartphones, iPhone, portrait 480x320 phones */\n  .sidebar {\n    top: 38px;\n    left: 0px;\n    padding-bottom: 3em;\n  }\n}\n\n@media (max-width: 600px) {\n  /* smartphones, iPhone, portrait 480x320 phones */\n  .sidebar {\n    margin-top: -3%;\n    top: 68px;\n    left: 0px;\n    height: 97%;\n    width: 25%;\n    padding-bottom: 3em;\n    position: fixed;\n  }\n  .sidebar .list-group > a.list-group-item .fa {\n    font-size: 40px;\n  }\n  .sidebar .list-group img {\n    width: 40px;\n  }\n  .sidebar .nested-menu > a.list-group-item .fa {\n    font-size: 40px;\n  }\n}\n\n@media print {\n  .sidebar {\n    display: none !important;\n  }\n}\n\n::-webkit-scrollbar {\n  width: 8px;\n}\n\n::-webkit-scrollbar-track {\n  -webkit-box-shadow: inset 0 0 0px white;\n  border-radius: 3px;\n}\n\n::-webkit-scrollbar-thumb {\n  border-radius: 3px;\n  -webkit-box-shadow: inset 0 0 3px white;\n}\n\n.toggle-button {\n  position: fixed;\n  width: 236px;\n  cursor: pointer;\n  padding: 12px;\n  bottom: 0;\n  color: #999;\n  background: #212529;\n  border-top: 1px solid #999;\n  transition: all 0.2s ease-in-out;\n}\n\n.toggle-button i {\n  font-size: 23px;\n}\n\n.toggle-button:hover {\n  background: #212631;\n  color: #56a4ff;\n}\n\n.collapsed {\n  width: 50px;\n}\n\n.collapsed span {\n  display: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L2NvbXBvbmVudHMvbmVzdGVkLXNpZGViYXIvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxjb21wb25lbnRzXFxuZXN0ZWQtc2lkZWJhclxcbmVzdGVkLXNpZGViYXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9jb21wb25lbnRzL25lc3RlZC1zaWRlYmFyL25lc3RlZC1zaWRlYmFyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0UscUJBQUE7RUFDQSxxQkFBQTtBQ0FGOztBREdBO0VBQ0UsZ0JBQUE7RUFDQSw0RUFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5QkFwQndCO0VBcUJ4QixvQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFLQSxnQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUNBRjs7QURFRTtFQUNFLG9CQUFBO0FDQUo7O0FERUk7RUFDRSxtQkFwQ29CO0VBcUNwQixTQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EscUJBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQWFBLGdCQUFBO0VBQ0Esb0JBQUE7QUNaTjs7QURBTTtFQUNFLGNBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0FDRVI7O0FERU07RUFDRSxrQkFBQTtBQ0FSOztBRE9JO0VBQ0UsZUFBQTtBQ0xOOztBRFFJO0VBQ0UsY0FBQTtBQ05OOztBRFNJO0VBQ0UsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsOEJBQUE7QUNQTjs7QURVSTtFQUNFLG1CQTVFb0I7RUE2RXBCLFNBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxxQkFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBYUEsZ0JBQUE7RUFDQSxvQkFBQTtBQ3BCTjs7QURRTTtFQUNFLGNBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0FDTlI7O0FEVU07RUFDRSxrQkFBQTtBQ1JSOztBRGVJO0VBQ0UsZUFBQTtBQ2JOOztBRGdCSTtFQUNFLGNBQUE7QUNkTjs7QURpQkk7RUFDRSxtQkFBQTtFQUNBLGNBQUE7RUFDQSw4QkFBQTtBQ2ZOOztBRGtCSTtFQUNFLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLDhCQUFBO0VBQ0EsNEVBQUE7QUNoQk47O0FEbUJJO0VBQ0UsaUJBQUE7QUNqQk47O0FEbUJNO0VBQ0UsOENBQUE7QUNqQlI7O0FEcUJJO0VBQ0UsV0FBQTtBQ25CTjs7QUR1QkU7RUFDRSxrQkFBQTtFQUNBLGVBQUE7QUNyQko7O0FEeUJJO0VBQ0UsbUJBQUE7RUFDQSxZQUFBO0FDdkJOOztBRDBCSTtFQUNFLGVBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUN4Qk47O0FEMEJNO0VBQ0UsV0FBQTtFQUNBLHFCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkEzSmtCO0FDbUkxQjs7QUQwQlE7RUFDRSxrQkFBQTtFQUNBLGNBQUE7RUFDQSx1QkFBQTtFQUNBLGlCQUFBO0FDeEJWOztBRDRCTTs7RUFFRSxXQUFBO0VBQ0EsYUFBQTtFQUNBLG9CQUFBO0FDMUJSOztBRDhCSTtFQUNFLG1CQUFBO0FDNUJOOztBRCtCSTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtBQzdCTjs7QURnQ1E7RUFDRSxnQkFBQTtFQUNBLHlCQXhMZ0I7RUF5TGhCLDJCQUFBO0FDOUJWOztBRGdDVTtFQUNFLFdBQUE7QUM5Qlo7O0FEaUNVO0VBQ0UsV0FBQTtBQy9CWjs7QURtQ1E7RUFDRSxtQkFBQTtBQ2pDVjs7QUR5Q007RUFDRSxjQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ3ZDUjs7QUQ2Q0k7RUFDRSxlQUFBO0FDM0NOOztBRDhDSTtFQUNFLFlBQUE7RUFDQSx5QkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDNUNOOztBRGdETTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtBQzlDUjs7QURtRE07RUFDRSxjQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ2pEUjs7QURxREk7RUFDRSxxQkFBQTtBQ25ETjs7QURzREk7RUFDRSxhQUFBO0VBQ0EsU0FBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsbUJBQUE7QUNwRE47O0FEd0RNO0VBQ0UsY0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQ3REUjs7QUR3RFE7RUFFRSxpREFBQTtFQUVBLGtCQUFBO0FDeERWOztBRHlEVTtFQUNFLFVBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0FDdkRaOztBRHlEVTtFQUNFLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQ3ZEWjs7QUQyRFE7RUFDRSxtQkFBQTtFQUNBLGNBQUE7RUFDQSw4QkFBQTtFQUNBLDRFQUFBO0FDekRWOztBRDBEVTtFQUNFLGNBQUE7QUN4RFo7O0FENkRVO0VBQ0UsWUFBQTtBQzNEWjs7QURnRVU7RUFDRSxjQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7QUM5RFo7O0FEa0VRO0VBQ0UsY0FBQTtBQ2hFVjs7QURzRUU7RUFDRSxpQkFBQTtFQUNBLG1CQUFBO0FDcEVKOztBRHlFQTtFQUNFLGlEQUFBO0VBQ0E7SUFDRSxTQUFBO0lBQ0EsU0FBQTtJQUNBLG1CQUFBO0VDdEVGO0FBQ0Y7O0FEd0VBO0VBQ0UsaURBQUE7RUFDQTtJQUNFLGVBQUE7SUFDQSxTQUFBO0lBQ0EsU0FBQTtJQUNBLFdBQUE7SUFDQSxVQUFBO0lBQ0EsbUJBQUE7SUFDQSxlQUFBO0VDdEVGO0VEMEVNO0lBQ0UsZUFBQTtFQ3hFUjtFRDRFSTtJQUNFLFdBQUE7RUMxRU47RURnRk07SUFDRSxlQUFBO0VDOUVSO0FBQ0Y7O0FEZ0dBO0VBQ0U7SUFDRSx3QkFBQTtFQzlGRjtBQUNGOztBRGlHQTtFQUNFLFVBQUE7QUMvRkY7O0FEa0dBO0VBQ0UsdUNBQUE7RUFDQSxrQkFBQTtBQy9GRjs7QURrR0E7RUFDRSxrQkFBQTtFQUNBLHVDQUFBO0FDL0ZGOztBRGtHQTtFQUNFLGVBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGFBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBV0EsMEJBQUE7RUFLQSxnQ0FBQTtBQ3pHRjs7QUQyRkU7RUFDRSxlQUFBO0FDekZKOztBRDRGRTtFQUNFLG1CQUFBO0VBQ0EsY0FBQTtBQzFGSjs7QURxR0E7RUFDRSxXQUFBO0FDbEdGOztBRG9HRTtFQUNFLGFBQUE7QUNsR0oiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvY29tcG9uZW50cy9uZXN0ZWQtc2lkZWJhci9uZXN0ZWQtc2lkZWJhci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiR0b3BuYXYtYmFja2dyb3VuZC1jb2xvcjogIzJiMzI0MDtcclxuLm5ldy1saW5lIHtcclxuICB3aGl0ZS1zcGFjZTogcHJlLXdyYXA7XHJcbiAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xyXG59XHJcblxyXG4uc2lkZWJhciB7XHJcbiAgZm9udC1zaXplOiBzbWFsbDtcclxuICBib3gtc2hhZG93OiAwIDRweCAxcHggMCByZ2JhKDAsIDAsIDAsIDAuMDEpLCAwIDNweCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMTkpO1xyXG4gIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIHotaW5kZXg6IDk5OTk7XHJcbiAgbGVmdDogMjM1cHg7XHJcbiAgd2lkdGg6IDIwMHB4O1xyXG4gIGhlaWdodDogMTAwJTtcclxuICBtYXJnaW4tbGVmdDogLTIzNXB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDQ4cHg7XHJcbiAgYm9yZGVyOiBub25lO1xyXG4gIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgb3ZlcmZsb3cteTogYXV0bztcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAkdG9wbmF2LWJhY2tncm91bmQtY29sb3I7XHJcbiAgcGFkZGluZy1ib3R0b206IDQwcHg7XHJcbiAgcGFkZGluZy10b3A6IDIycHg7XHJcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xyXG4gIC1tb3otdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XHJcbiAgLW1zLXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xyXG4gIC1vLXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBtYXJnaW4tdG9wOiAxOXB4O1xyXG5cclxuICAubGlzdC1ncm91cCB7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcclxuXHJcbiAgICBhLmxpc3QtZ3JvdXAtaXRlbSB7XHJcbiAgICAgIGJhY2tncm91bmQ6ICR0b3BuYXYtYmFja2dyb3VuZC1jb2xvcjtcclxuICAgICAgYm9yZGVyOiAwO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICBwYWRkaW5nLXRvcDogMTVweDtcclxuICAgICAgcGFkZGluZy1ib3R0b206IDE1cHg7XHJcbiAgICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcclxuICAgICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xyXG5cclxuICAgICAgPiBpIHtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICBmb250LWZhbWlseTogXCJmb250ZWxsb1wiO1xyXG4gICAgICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICAgICAgfVxyXG5cclxuICAgICAgLy9wYWRkaW5nLWxlZnQ6IDRweDtcclxuICAgICAgLmZhIHtcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgIHZlcnRpY2FsLWFsaWduOiAxMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIGEubGlzdC1ncm91cC1pdGVtID4gcCB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDVweDtcclxuICAgIH1cclxuXHJcbiAgICBhLmxpc3QtZ3JvdXAtaXRlbSA+IHA6YWN0aXZlIHtcclxuICAgICAgY29sb3I6ICM1NmE0ZmY7XHJcbiAgICB9XHJcblxyXG4gICAgYTpob3ZlciB7XHJcbiAgICAgIGJhY2tncm91bmQ6IGxpZ2h0ZW4oJHRvcG5hdi1iYWNrZ3JvdW5kLWNvbG9yLCA4JSk7XHJcbiAgICAgIGNvbG9yOiAjNTZhNGZmO1xyXG4gICAgICBib3JkZXItbGVmdDogMnB4ICM1NmE0ZmYgc29saWQ7XHJcbiAgICB9XHJcblxyXG4gICAgZGl2Lmxpc3QtZ3JvdXAtaXRlbSB7XHJcbiAgICAgIGJhY2tncm91bmQ6ICR0b3BuYXYtYmFja2dyb3VuZC1jb2xvcjtcclxuICAgICAgYm9yZGVyOiAwO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICBwYWRkaW5nLXRvcDogMTVweDtcclxuICAgICAgcGFkZGluZy1ib3R0b206IDE1cHg7XHJcbiAgICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcclxuICAgICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xyXG5cclxuICAgICAgPiBpIHtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICBmb250LWZhbWlseTogXCJmb250ZWxsb1wiO1xyXG4gICAgICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICAgICAgfVxyXG5cclxuICAgICAgLy9wYWRkaW5nLWxlZnQ6IDRweDtcclxuICAgICAgLmZhIHtcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgIHZlcnRpY2FsLWFsaWduOiAxMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIGRpdi5saXN0LWdyb3VwLWl0ZW0gPiBwIHtcclxuICAgICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgfVxyXG5cclxuICAgIGRpdi5saXN0LWdyb3VwLWl0ZW0gPiBwOmFjdGl2ZSB7XHJcbiAgICAgIGNvbG9yOiAjNTZhNGZmO1xyXG4gICAgfVxyXG5cclxuICAgIGRpdi5saXN0LWdyb3VwLWl0ZW06aG92ZXIge1xyXG4gICAgICBiYWNrZ3JvdW5kOiBsaWdodGVuKCR0b3BuYXYtYmFja2dyb3VuZC1jb2xvciwgOCUpO1xyXG4gICAgICBjb2xvcjogIzU2YTRmZjtcclxuICAgICAgYm9yZGVyLWxlZnQ6IDJweCAjNTZhNGZmIHNvbGlkO1xyXG4gICAgfVxyXG5cclxuICAgIGEucm91dGVyLWxpbmstYWN0aXZlIHtcclxuICAgICAgYmFja2dyb3VuZDogbGlnaHRlbigkdG9wbmF2LWJhY2tncm91bmQtY29sb3IsIDglKTtcclxuICAgICAgY29sb3I6ICM1NmE0ZmY7XHJcbiAgICAgIGJvcmRlci1sZWZ0OiAycHggIzU2YTRmZiBzb2xpZDtcclxuICAgICAgYm94LXNoYWRvdzogMCA0cHggOHB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDZweCAyMHB4IDAgcmdiYSgwLCAwLCAwLCAwLjE5KTtcclxuICAgIH1cclxuXHJcbiAgICAuaGVhZGVyLWZpZWxkcyB7XHJcbiAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xyXG5cclxuICAgICAgPiAubGlzdC1ncm91cC1pdGVtOmZpcnN0LWNoaWxkIHtcclxuICAgICAgICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjIpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaW1nIHtcclxuICAgICAgd2lkdGg6IDM1cHg7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgIG1hcmdpbi10b3A6IDBweDtcclxuICB9XHJcblxyXG4gIC5zaWRlYmFyLWRyb3Bkb3duIHtcclxuICAgICo6Zm9jdXMge1xyXG4gICAgICBib3JkZXItcmFkaXVzOiBub25lO1xyXG4gICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICB9XHJcblxyXG4gICAgLnBhbmVsLXRpdGxlIHtcclxuICAgICAgZm9udC1zaXplOiAxcmVtO1xyXG4gICAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDA7XHJcblxyXG4gICAgICBhIHtcclxuICAgICAgICBjb2xvcjogIzk5OTtcclxuICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAkdG9wbmF2LWJhY2tncm91bmQtY29sb3I7XHJcblxyXG4gICAgICAgIHNwYW4ge1xyXG4gICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICBwYWRkaW5nOiAwLjc1cmVtIDEuNXJlbTtcclxuICAgICAgICAgIHBhZGRpbmctdG9wOiAxcmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgYTpob3ZlcixcclxuICAgICAgYTpmb2N1cyB7XHJcbiAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgb3V0bGluZTogbm9uZTtcclxuICAgICAgICBvdXRsaW5lLW9mZnNldDogLTJweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5wYW5lbC10aXRsZTpob3ZlciB7XHJcbiAgICAgIGJhY2tncm91bmQ6IGRhcmtlbigkdG9wbmF2LWJhY2tncm91bmQtY29sb3IsIDUlKTtcclxuICAgIH1cclxuXHJcbiAgICAucGFuZWwtY29sbGFwc2Uge1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgICBib3JkZXI6IG5vbmU7XHJcblxyXG4gICAgICAucGFuZWwtYm9keSB7XHJcbiAgICAgICAgLmxpc3QtZ3JvdXAtaXRlbSB7XHJcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJHRvcG5hdi1iYWNrZ3JvdW5kLWNvbG9yO1xyXG4gICAgICAgICAgYm9yZGVyOiAwIHNvbGlkIHRyYW5zcGFyZW50O1xyXG5cclxuICAgICAgICAgIGEge1xyXG4gICAgICAgICAgICBjb2xvcjogIzk5OTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBhOmhvdmVyIHtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAubGlzdC1ncm91cC1pdGVtOmhvdmVyIHtcclxuICAgICAgICAgIGJhY2tncm91bmQ6IGRhcmtlbigkdG9wbmF2LWJhY2tncm91bmQtY29sb3IsIDUlKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5saXN0LWdyb3VwIHtcclxuICAgID4gYS5saXN0LWdyb3VwLWl0ZW0ge1xyXG4gICAgICAuZmEge1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIGNsZWFyOiBib3RoO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDBweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLm5lc3RlZC1tZW51IHtcclxuICAgIC5saXN0LWdyb3VwLWl0ZW0ge1xyXG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgLnBhcmVudC1tZW51IHtcclxuICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICBmb250LXNpemU6IDExcHg7XHJcbiAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgIHBhZGRpbmctbGVmdDogMTBweDtcclxuICAgIH1cclxuXHJcbiAgICBpIHtcclxuICAgICAgaW1nIHtcclxuICAgICAgICB3aWR0aDogNDVweDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICA+IGEubGlzdC1ncm91cC1pdGVtIHtcclxuICAgICAgLmZhIHtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICBjbGVhcjogYm90aDtcclxuICAgICAgICBmb250LXNpemU6IDM2cHg7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAwcHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAubmVzdGVkIHtcclxuICAgICAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xyXG4gICAgfVxyXG5cclxuICAgIHVsLnN1Ym1lbnUge1xyXG4gICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgICBoZWlnaHQ6IDA7XHJcbiAgICAgIG1hcmdpbjogMDtcclxuICAgICAgcGFkZGluZzogMDtcclxuICAgICAgYmFja2dyb3VuZDogZGFya2VuKCR0b3BuYXYtYmFja2dyb3VuZC1jb2xvciwgNSUpO1xyXG4gICAgfVxyXG5cclxuICAgICYgLmV4cGFuZCB7XHJcbiAgICAgIHVsLnN1Ym1lbnUge1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgICBiYWNrZ3JvdW5kOiBkYXJrZW4oJHRvcG5hdi1iYWNrZ3JvdW5kLWNvbG9yLCA1JSk7XHJcblxyXG4gICAgICAgIGEubGlzdC1ncm91cC1pdGVtIHtcclxuICAgICAgICAgIC8vIGJhY2tncm91bmQ6ICMzOTQ0NTg7XHJcbiAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjEpO1xyXG4gICAgICAgICAgLy8gYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4xKTtcclxuICAgICAgICAgIHBhZGRpbmctbGVmdDogMjBweDtcclxuICAgICAgICAgIC5jaXJjbGUtY3VzdG9tIHtcclxuICAgICAgICAgICAgd2lkdGg6IDFweDtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjQ3ZGU1O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgc3BhbiB7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxuICAgICAgICAgICAgY29sb3I6ICM5ZWE2Yjc7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweDtcclxuICAgICAgICAgICAgbGlzdC1zdHlsZTogY2lyY2xlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgICBhLmxpc3QtZ3JvdXAtaXRlbS5yb3V0ZXItbGluay1hY3RpdmUge1xyXG4gICAgICAgICAgYmFja2dyb3VuZDogIzNiNDU1ODtcclxuICAgICAgICAgIGNvbG9yOiAjNTZhNGZmO1xyXG4gICAgICAgICAgYm9yZGVyLWxlZnQ6IDJweCAjNTZhNGZmIHNvbGlkO1xyXG4gICAgICAgICAgYm94LXNoYWRvdzogMCA0cHggOHB4IDAgcmdiKDAgMCAwIC8gMjAlKSwgMCA2cHggMjBweCAwIHJnYigwIDAgMCAvIDE5JSk7XHJcbiAgICAgICAgICBzcGFuIHtcclxuICAgICAgICAgICAgY29sb3I6ICM1NmE0ZmY7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBhLmxpc3QtZ3JvdXAtaXRlbTpob3ZlciB7XHJcbiAgICAgICAgICBwIHtcclxuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGkge1xyXG4gICAgICAgICAgcCB7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNjI3MTkwO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHAge1xyXG4gICAgICAgICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAuY2hpbGQtbWVudSB7XHJcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcblxyXG4gICAgXHJcbiAgfVxyXG59XHJcbkBtZWRpYSAobWF4LXdpZHRoOiA5OTJweCkge1xyXG4gIC8qIHNtYXJ0cGhvbmVzLCBpUGhvbmUsIHBvcnRyYWl0IDQ4MHgzMjAgcGhvbmVzICovXHJcbiAgLnNpZGViYXIge1xyXG4gICAgdG9wOiAzOHB4O1xyXG4gICAgbGVmdDogMHB4O1xyXG4gICAgcGFkZGluZy1ib3R0b206IDNlbTtcclxuICB9XHJcbn1cclxuQG1lZGlhIChtYXgtd2lkdGg6IDYwMHB4KSB7XHJcbiAgLyogc21hcnRwaG9uZXMsIGlQaG9uZSwgcG9ydHJhaXQgNDgweDMyMCBwaG9uZXMgKi9cclxuICAuc2lkZWJhciB7XHJcbiAgICBtYXJnaW4tdG9wOiAtMyU7XHJcbiAgICB0b3A6IDY4cHg7XHJcbiAgICBsZWZ0OiAwcHg7XHJcbiAgICBoZWlnaHQ6IDk3JTtcclxuICAgIHdpZHRoOiAyNSU7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogM2VtO1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG5cclxuICAgIC5saXN0LWdyb3VwIHtcclxuICAgICAgPiBhLmxpc3QtZ3JvdXAtaXRlbSB7XHJcbiAgICAgICAgLmZhIHtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogNDBweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGltZyB7XHJcbiAgICAgICAgd2lkdGg6IDQwcHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAubmVzdGVkLW1lbnUge1xyXG4gICAgICA+IGEubGlzdC1ncm91cC1pdGVtIHtcclxuICAgICAgICAuZmEge1xyXG4gICAgICAgICAgZm9udC1zaXplOiA0MHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLy9AbWVkaWEgKG1pbi13aWR0aDogOTYxcHgpIHtcclxuLy8gICAgLyogdGFibGV0LCBsYW5kc2NhcGUgaVBhZCwgbG8tcmVzIGxhcHRvcHMgYW5kcyBkZXNrdG9wcyAqL1xyXG4vL31cclxuLy9cclxuLy9AbWVkaWEgKG1pbi13aWR0aDogMTAyNXB4KSB7XHJcbi8vICAgIC8qIGJpZyBsYW5kc2NhcGUgdGFibGV0cywgbGFwdG9wcywgYW5kIGRlc2t0b3BzICovXHJcbi8vfVxyXG4vL1xyXG4vL0BtZWRpYSAobWluLXdpZHRoOiAxMjgxcHgpIHtcclxuLy8gICAgLyogaGktcmVzIGxhcHRvcHMgYW5kIGRlc2t0b3BzICovXHJcbi8vfVxyXG5cclxuQG1lZGlhIHByaW50IHtcclxuICAuc2lkZWJhciB7XHJcbiAgICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG59XHJcblxyXG46Oi13ZWJraXQtc2Nyb2xsYmFyIHtcclxuICB3aWR0aDogOHB4O1xyXG59XHJcblxyXG46Oi13ZWJraXQtc2Nyb2xsYmFyLXRyYWNrIHtcclxuICAtd2Via2l0LWJveC1zaGFkb3c6IGluc2V0IDAgMCAwcHggcmdiYSgyNTUsIDI1NSwgMjU1LCAxKTtcclxuICBib3JkZXItcmFkaXVzOiAzcHg7XHJcbn1cclxuXHJcbjo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xyXG4gIGJvcmRlci1yYWRpdXM6IDNweDtcclxuICAtd2Via2l0LWJveC1zaGFkb3c6IGluc2V0IDAgMCAzcHggcmdiYSgyNTUsIDI1NSwgMjU1LCAxKTtcclxufVxyXG5cclxuLnRvZ2dsZS1idXR0b24ge1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICB3aWR0aDogMjM2cHg7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIHBhZGRpbmc6IDEycHg7XHJcbiAgYm90dG9tOiAwO1xyXG4gIGNvbG9yOiAjOTk5O1xyXG4gIGJhY2tncm91bmQ6ICMyMTI1Mjk7XHJcblxyXG4gIGkge1xyXG4gICAgZm9udC1zaXplOiAyM3B4O1xyXG4gIH1cclxuXHJcbiAgJjpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kOiBkYXJrZW4oJHRvcG5hdi1iYWNrZ3JvdW5kLWNvbG9yLCA1JSk7XHJcbiAgICBjb2xvcjogIzU2YTRmZjtcclxuICB9XHJcblxyXG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjOTk5O1xyXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XHJcbiAgLW1vei10cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcclxuICAtbXMtdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XHJcbiAgLW8tdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XHJcbn1cclxuXHJcbi5jb2xsYXBzZWQge1xyXG4gIHdpZHRoOiA1MHB4O1xyXG5cclxuICBzcGFuIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgfVxyXG59IiwiLm5ldy1saW5lIHtcbiAgd2hpdGUtc3BhY2U6IHByZS13cmFwO1xuICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XG59XG5cbi5zaWRlYmFyIHtcbiAgZm9udC1zaXplOiBzbWFsbDtcbiAgYm94LXNoYWRvdzogMCA0cHggMXB4IDAgcmdiYSgwLCAwLCAwLCAwLjAxKSwgMCAzcHggNXB4IDAgcmdiYSgwLCAwLCAwLCAwLjE5KTtcbiAgYm9yZGVyLXJhZGl1czogMDtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB6LWluZGV4OiA5OTk5O1xuICBsZWZ0OiAyMzVweDtcbiAgd2lkdGg6IDIwMHB4O1xuICBoZWlnaHQ6IDEwMCU7XG4gIG1hcmdpbi1sZWZ0OiAtMjM1cHg7XG4gIG1hcmdpbi1ib3R0b206IDQ4cHg7XG4gIGJvcmRlcjogbm9uZTtcbiAgYm9yZGVyLXJhZGl1czogMDtcbiAgb3ZlcmZsb3cteTogYXV0bztcbiAgYmFja2dyb3VuZC1jb2xvcjogIzJiMzI0MDtcbiAgcGFkZGluZy1ib3R0b206IDQwcHg7XG4gIHBhZGRpbmctdG9wOiAyMnB4O1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xuICAtbW96LXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xuICAtbXMtdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XG4gIC1vLXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xuICB0cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tdG9wOiAxOXB4O1xufVxuLnNpZGViYXIgLmxpc3QtZ3JvdXAge1xuICBwYWRkaW5nLWJvdHRvbTogMjBweDtcbn1cbi5zaWRlYmFyIC5saXN0LWdyb3VwIGEubGlzdC1ncm91cC1pdGVtIHtcbiAgYmFja2dyb3VuZDogIzJiMzI0MDtcbiAgYm9yZGVyOiAwO1xuICBib3JkZXItcmFkaXVzOiAwO1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBwYWRkaW5nLXRvcDogMTVweDtcbiAgcGFkZGluZy1ib3R0b206IDE1cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICB2ZXJ0aWNhbC1hbGlnbjogMTBweDtcbn1cbi5zaWRlYmFyIC5saXN0LWdyb3VwIGEubGlzdC1ncm91cC1pdGVtID4gaSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LWZhbWlseTogXCJmb250ZWxsb1wiO1xuICBmb250LXN0eWxlOiBub3JtYWw7XG59XG4uc2lkZWJhciAubGlzdC1ncm91cCBhLmxpc3QtZ3JvdXAtaXRlbSAuZmEge1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG4uc2lkZWJhciAubGlzdC1ncm91cCBhLmxpc3QtZ3JvdXAtaXRlbSA+IHAge1xuICBtYXJnaW4tdG9wOiA1cHg7XG59XG4uc2lkZWJhciAubGlzdC1ncm91cCBhLmxpc3QtZ3JvdXAtaXRlbSA+IHA6YWN0aXZlIHtcbiAgY29sb3I6ICM1NmE0ZmY7XG59XG4uc2lkZWJhciAubGlzdC1ncm91cCBhOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogIzNiNDU1ODtcbiAgY29sb3I6ICM1NmE0ZmY7XG4gIGJvcmRlci1sZWZ0OiAycHggIzU2YTRmZiBzb2xpZDtcbn1cbi5zaWRlYmFyIC5saXN0LWdyb3VwIGRpdi5saXN0LWdyb3VwLWl0ZW0ge1xuICBiYWNrZ3JvdW5kOiAjMmIzMjQwO1xuICBib3JkZXI6IDA7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIHBhZGRpbmctdG9wOiAxNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTVweDtcbiAgcGFkZGluZy1yaWdodDogNXB4O1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIHZlcnRpY2FsLWFsaWduOiAxMHB4O1xufVxuLnNpZGViYXIgLmxpc3QtZ3JvdXAgZGl2Lmxpc3QtZ3JvdXAtaXRlbSA+IGkge1xuICBkaXNwbGF5OiBibG9jaztcbiAgZm9udC1mYW1pbHk6IFwiZm9udGVsbG9cIjtcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xufVxuLnNpZGViYXIgLmxpc3QtZ3JvdXAgZGl2Lmxpc3QtZ3JvdXAtaXRlbSAuZmEge1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG4uc2lkZWJhciAubGlzdC1ncm91cCBkaXYubGlzdC1ncm91cC1pdGVtID4gcCB7XG4gIG1hcmdpbi10b3A6IDVweDtcbn1cbi5zaWRlYmFyIC5saXN0LWdyb3VwIGRpdi5saXN0LWdyb3VwLWl0ZW0gPiBwOmFjdGl2ZSB7XG4gIGNvbG9yOiAjNTZhNGZmO1xufVxuLnNpZGViYXIgLmxpc3QtZ3JvdXAgZGl2Lmxpc3QtZ3JvdXAtaXRlbTpob3ZlciB7XG4gIGJhY2tncm91bmQ6ICMzYjQ1NTg7XG4gIGNvbG9yOiAjNTZhNGZmO1xuICBib3JkZXItbGVmdDogMnB4ICM1NmE0ZmYgc29saWQ7XG59XG4uc2lkZWJhciAubGlzdC1ncm91cCBhLnJvdXRlci1saW5rLWFjdGl2ZSB7XG4gIGJhY2tncm91bmQ6ICMzYjQ1NTg7XG4gIGNvbG9yOiAjNTZhNGZmO1xuICBib3JkZXItbGVmdDogMnB4ICM1NmE0ZmYgc29saWQ7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKSwgMCA2cHggMjBweCAwIHJnYmEoMCwgMCwgMCwgMC4xOSk7XG59XG4uc2lkZWJhciAubGlzdC1ncm91cCAuaGVhZGVyLWZpZWxkcyB7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xufVxuLnNpZGViYXIgLmxpc3QtZ3JvdXAgLmhlYWRlci1maWVsZHMgPiAubGlzdC1ncm91cC1pdGVtOmZpcnN0LWNoaWxkIHtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4yKTtcbn1cbi5zaWRlYmFyIC5saXN0LWdyb3VwIGltZyB7XG4gIHdpZHRoOiAzNXB4O1xufVxuLnNpZGViYXIgcCB7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgbWFyZ2luLXRvcDogMHB4O1xufVxuLnNpZGViYXIgLnNpZGViYXItZHJvcGRvd24gKjpmb2N1cyB7XG4gIGJvcmRlci1yYWRpdXM6IG5vbmU7XG4gIGJvcmRlcjogbm9uZTtcbn1cbi5zaWRlYmFyIC5zaWRlYmFyLWRyb3Bkb3duIC5wYW5lbC10aXRsZSB7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgaGVpZ2h0OiA1MHB4O1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxuLnNpZGViYXIgLnNpZGViYXItZHJvcGRvd24gLnBhbmVsLXRpdGxlIGEge1xuICBjb2xvcjogIzk5OTtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBmb250LXdlaWdodDogNDAwO1xuICBiYWNrZ3JvdW5kOiAjMmIzMjQwO1xufVxuLnNpZGViYXIgLnNpZGViYXItZHJvcGRvd24gLnBhbmVsLXRpdGxlIGEgc3BhbiB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBhZGRpbmc6IDAuNzVyZW0gMS41cmVtO1xuICBwYWRkaW5nLXRvcDogMXJlbTtcbn1cbi5zaWRlYmFyIC5zaWRlYmFyLWRyb3Bkb3duIC5wYW5lbC10aXRsZSBhOmhvdmVyLFxuLnNpZGViYXIgLnNpZGViYXItZHJvcGRvd24gLnBhbmVsLXRpdGxlIGE6Zm9jdXMge1xuICBjb2xvcjogI2ZmZjtcbiAgb3V0bGluZTogbm9uZTtcbiAgb3V0bGluZS1vZmZzZXQ6IC0ycHg7XG59XG4uc2lkZWJhciAuc2lkZWJhci1kcm9wZG93biAucGFuZWwtdGl0bGU6aG92ZXIge1xuICBiYWNrZ3JvdW5kOiAjMjEyNjMxO1xufVxuLnNpZGViYXIgLnNpZGViYXItZHJvcGRvd24gLnBhbmVsLWNvbGxhcHNlIHtcbiAgYm9yZGVyLXJhZGl1czogMDtcbiAgYm9yZGVyOiBub25lO1xufVxuLnNpZGViYXIgLnNpZGViYXItZHJvcGRvd24gLnBhbmVsLWNvbGxhcHNlIC5wYW5lbC1ib2R5IC5saXN0LWdyb3VwLWl0ZW0ge1xuICBib3JkZXItcmFkaXVzOiAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMmIzMjQwO1xuICBib3JkZXI6IDAgc29saWQgdHJhbnNwYXJlbnQ7XG59XG4uc2lkZWJhciAuc2lkZWJhci1kcm9wZG93biAucGFuZWwtY29sbGFwc2UgLnBhbmVsLWJvZHkgLmxpc3QtZ3JvdXAtaXRlbSBhIHtcbiAgY29sb3I6ICM5OTk7XG59XG4uc2lkZWJhciAuc2lkZWJhci1kcm9wZG93biAucGFuZWwtY29sbGFwc2UgLnBhbmVsLWJvZHkgLmxpc3QtZ3JvdXAtaXRlbSBhOmhvdmVyIHtcbiAgY29sb3I6ICNmZmY7XG59XG4uc2lkZWJhciAuc2lkZWJhci1kcm9wZG93biAucGFuZWwtY29sbGFwc2UgLnBhbmVsLWJvZHkgLmxpc3QtZ3JvdXAtaXRlbTpob3ZlciB7XG4gIGJhY2tncm91bmQ6ICMyMTI2MzE7XG59XG4uc2lkZWJhciAubGlzdC1ncm91cCA+IGEubGlzdC1ncm91cC1pdGVtIC5mYSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBjbGVhcjogYm90aDtcbiAgZm9udC1zaXplOiAzNnB4O1xuICBtYXJnaW4tcmlnaHQ6IDBweDtcbn1cbi5zaWRlYmFyIC5uZXN0ZWQtbWVudSAubGlzdC1ncm91cC1pdGVtIHtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51IC5wYXJlbnQtbWVudSB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC1zaXplOiAxMXB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG59XG4uc2lkZWJhciAubmVzdGVkLW1lbnUgaSBpbWcge1xuICB3aWR0aDogNDVweDtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51ID4gYS5saXN0LWdyb3VwLWl0ZW0gLmZhIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGNsZWFyOiBib3RoO1xuICBmb250LXNpemU6IDM2cHg7XG4gIG1hcmdpbi1yaWdodDogMHB4O1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51IC5uZXN0ZWQge1xuICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XG59XG4uc2lkZWJhciAubmVzdGVkLW1lbnUgdWwuc3VibWVudSB7XG4gIGRpc3BsYXk6IG5vbmU7XG4gIGhlaWdodDogMDtcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAwO1xuICBiYWNrZ3JvdW5kOiAjMjEyNjMxO1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51IC5leHBhbmQgdWwuc3VibWVudSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBoZWlnaHQ6IGF1dG87XG4gIGJhY2tncm91bmQ6ICMyMTI2MzE7XG59XG4uc2lkZWJhciAubmVzdGVkLW1lbnUgLmV4cGFuZCB1bC5zdWJtZW51IGEubGlzdC1ncm91cC1pdGVtIHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4xKTtcbiAgcGFkZGluZy1sZWZ0OiAyMHB4O1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51IC5leHBhbmQgdWwuc3VibWVudSBhLmxpc3QtZ3JvdXAtaXRlbSAuY2lyY2xlLWN1c3RvbSB7XG4gIHdpZHRoOiAxcHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzI0N2RlNTtcbn1cbi5zaWRlYmFyIC5uZXN0ZWQtbWVudSAuZXhwYW5kIHVsLnN1Ym1lbnUgYS5saXN0LWdyb3VwLWl0ZW0gc3BhbiB7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgY29sb3I6ICM5ZWE2Yjc7XG4gIHBhZGRpbmc6IDBweDtcbiAgbGlzdC1zdHlsZTogY2lyY2xlO1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51IC5leHBhbmQgdWwuc3VibWVudSBhLmxpc3QtZ3JvdXAtaXRlbS5yb3V0ZXItbGluay1hY3RpdmUge1xuICBiYWNrZ3JvdW5kOiAjM2I0NTU4O1xuICBjb2xvcjogIzU2YTRmZjtcbiAgYm9yZGVyLWxlZnQ6IDJweCAjNTZhNGZmIHNvbGlkO1xuICBib3gtc2hhZG93OiAwIDRweCA4cHggMCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgNnB4IDIwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTkpO1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51IC5leHBhbmQgdWwuc3VibWVudSBhLmxpc3QtZ3JvdXAtaXRlbS5yb3V0ZXItbGluay1hY3RpdmUgc3BhbiB7XG4gIGNvbG9yOiAjNTZhNGZmO1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51IC5leHBhbmQgdWwuc3VibWVudSBhLmxpc3QtZ3JvdXAtaXRlbTpob3ZlciBwIHtcbiAgY29sb3I6IHdoaXRlO1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51IC5leHBhbmQgdWwuc3VibWVudSBsaSBwIHtcbiAgY29sb3I6ICM2MjcxOTA7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuLnNpZGViYXIgLm5lc3RlZC1tZW51IC5leHBhbmQgdWwuc3VibWVudSBwIHtcbiAgY29sb3I6ICNmZmZmZmY7XG59XG4uc2lkZWJhciAuY2hpbGQtbWVudSB7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuXG5AbWVkaWEgKG1heC13aWR0aDogOTkycHgpIHtcbiAgLyogc21hcnRwaG9uZXMsIGlQaG9uZSwgcG9ydHJhaXQgNDgweDMyMCBwaG9uZXMgKi9cbiAgLnNpZGViYXIge1xuICAgIHRvcDogMzhweDtcbiAgICBsZWZ0OiAwcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDNlbTtcbiAgfVxufVxuQG1lZGlhIChtYXgtd2lkdGg6IDYwMHB4KSB7XG4gIC8qIHNtYXJ0cGhvbmVzLCBpUGhvbmUsIHBvcnRyYWl0IDQ4MHgzMjAgcGhvbmVzICovXG4gIC5zaWRlYmFyIHtcbiAgICBtYXJnaW4tdG9wOiAtMyU7XG4gICAgdG9wOiA2OHB4O1xuICAgIGxlZnQ6IDBweDtcbiAgICBoZWlnaHQ6IDk3JTtcbiAgICB3aWR0aDogMjUlO1xuICAgIHBhZGRpbmctYm90dG9tOiAzZW07XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICB9XG4gIC5zaWRlYmFyIC5saXN0LWdyb3VwID4gYS5saXN0LWdyb3VwLWl0ZW0gLmZhIHtcbiAgICBmb250LXNpemU6IDQwcHg7XG4gIH1cbiAgLnNpZGViYXIgLmxpc3QtZ3JvdXAgaW1nIHtcbiAgICB3aWR0aDogNDBweDtcbiAgfVxuICAuc2lkZWJhciAubmVzdGVkLW1lbnUgPiBhLmxpc3QtZ3JvdXAtaXRlbSAuZmEge1xuICAgIGZvbnQtc2l6ZTogNDBweDtcbiAgfVxufVxuQG1lZGlhIHByaW50IHtcbiAgLnNpZGViYXIge1xuICAgIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcbiAgfVxufVxuOjotd2Via2l0LXNjcm9sbGJhciB7XG4gIHdpZHRoOiA4cHg7XG59XG5cbjo6LXdlYmtpdC1zY3JvbGxiYXItdHJhY2sge1xuICAtd2Via2l0LWJveC1zaGFkb3c6IGluc2V0IDAgMCAwcHggd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDNweDtcbn1cblxuOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XG4gIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiBpbnNldCAwIDAgM3B4IHdoaXRlO1xufVxuXG4udG9nZ2xlLWJ1dHRvbiB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgd2lkdGg6IDIzNnB4O1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIHBhZGRpbmc6IDEycHg7XG4gIGJvdHRvbTogMDtcbiAgY29sb3I6ICM5OTk7XG4gIGJhY2tncm91bmQ6ICMyMTI1Mjk7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjOTk5O1xuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xuICAtbW96LXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xuICAtbXMtdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XG4gIC1vLXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xuICB0cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcbn1cbi50b2dnbGUtYnV0dG9uIGkge1xuICBmb250LXNpemU6IDIzcHg7XG59XG4udG9nZ2xlLWJ1dHRvbjpob3ZlciB7XG4gIGJhY2tncm91bmQ6ICMyMTI2MzE7XG4gIGNvbG9yOiAjNTZhNGZmO1xufVxuXG4uY29sbGFwc2VkIHtcbiAgd2lkdGg6IDUwcHg7XG59XG4uY29sbGFwc2VkIHNwYW4ge1xuICBkaXNwbGF5OiBub25lO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/components/nested-sidebar/nested-sidebar.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/layout/components/nested-sidebar/nested-sidebar.component.ts ***!
  \******************************************************************************/
/*! exports provided: NestedSidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NestedSidebarComponent", function() { return NestedSidebarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _nested_sidebarmenu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./nested-sidebarmenu */ "./src/app/layout/components/nested-sidebar/nested-sidebarmenu.ts");
/* harmony import */ var _services_storage_service_module_storage_service_module_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/storage-service-module/storage-service-module.service */ "./src/app/services/storage-service-module/storage-service-module.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _services_program_name_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/program-name.service */ "./src/app/services/program-name.service.ts");
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_7___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var NestedSidebarComponent = /** @class */ (function () {
    function NestedSidebarComponent(translate, router, lStorage, activatedRoute, programNameService) {
        // let $secretKey = localStorage.getToken();
        // let encodedStr = JSON.stringify({data:'here'})
        // let AesCrypto = crypto.AES.encrypt(encodedStr, $secretKey);
        var _this_1 = this;
        this.translate = translate;
        this.router = router;
        this.lStorage = lStorage;
        this.activatedRoute = activatedRoute;
        this.programNameService = programNameService;
        this.isActive = false;
        this.collapsed = false;
        this.showMenu = '';
        this.pushRightClass = 'push-right';
        this.sideBarMenu = _nested_sidebarmenu__WEBPACK_IMPORTED_MODULE_3__["sidebarmenu"];
        this.mci_project = false;
        this.contentProgram = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_7__;
        this.contentList = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_7__;
        this.collapsedEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        // let result = AesCrypto.toString();
        // let decrypt = crypto.AES.decrypt(result, $secretKey );
        // console.log("CRYPTO", result,decrypt.toString(crypto.enc.Utf8)) ;
        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de']);
        this.translate.setDefaultLang('en');
        var browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de/) ? browserLang : 'en');
        this.router.events.subscribe(function (val) {
            if (val instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"] &&
                window.innerWidth <= 992 &&
                _this_1.isToggled()) {
                _this_1.toggleSidebar();
            }
        });
        var program = localStorage.getItem('programName');
        var _this = this;
        this.contentList.filter(function (element) {
            if (element.appLabel == program) {
                if (element.type == "reguler") {
                    _this.mci_project = true;
                }
                else {
                    _this.mci_project = false;
                }
            }
        });
    }
    NestedSidebarComponent.prototype.ngOnInit = function () {
        this.paramAppLabel = this.activatedRoute.snapshot.queryParamMap.get('app_label');
        if (!this.paramAppLabel)
            this.paramAppLabel = localStorage.getItem('programName');
    };
    NestedSidebarComponent.prototype.getDetailProgram = function (value) {
        var detailProgram = this.contentProgram.find(function (element) { return element.appLabel == value; });
        return detailProgram && detailProgram.title ? detailProgram.title : "";
    };
    NestedSidebarComponent.prototype.handleChangePage = function (value) {
        var _this_1 = this;
        var programName = localStorage.getItem('programName');
        if (programName && programName != this.paramAppLabel) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                text: "Tab lain sedang mengakses program " + this.getDetailProgram(programName) + ".",
                icon: 'warning',
                confirmButtonText: "Tetap di " + this.getDetailProgram(this.paramAppLabel),
                cancelButtonText: "Pindah ke " + this.getDetailProgram(programName),
                cancelButtonColor: '#3086d6',
                showCancelButton: true,
            }).then(function (result) {
                if (result.isConfirmed) {
                    _this_1.programNameService.setData(_this_1.paramAppLabel);
                    _this_1.router.navigate([value]);
                }
                else {
                    _this_1.programNameService.setData(programName);
                    _this_1.paramAppLabel = localStorage.getItem('programName');
                    _this_1.router.navigate([value]);
                }
            });
        }
        else if (this.paramAppLabel && !programName) {
            this.programNameService.setData(this.paramAppLabel);
            this.router.navigate([value]);
        }
        else {
            this.router.navigate([value]);
        }
    };
    NestedSidebarComponent.prototype.eventCalled = function () {
        this.isActive = !this.isActive;
    };
    NestedSidebarComponent.prototype.addExpandClass = function (element) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        }
        else {
            this.showMenu = element;
        }
    };
    NestedSidebarComponent.prototype.toggleCollapsed = function () {
        this.collapsed = !this.collapsed;
        this.collapsedEvent.emit(this.collapsed);
    };
    NestedSidebarComponent.prototype.isToggled = function () {
        var dom = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    };
    NestedSidebarComponent.prototype.toggleSidebar = function () {
        var dom = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    };
    NestedSidebarComponent.prototype.rltAndLtr = function () {
        var dom = document.querySelector('body');
        dom.classList.toggle('rtl');
    };
    NestedSidebarComponent.prototype.changeLang = function (language) {
        this.translate.use(language);
    };
    NestedSidebarComponent.prototype.onLoggedout = function () {
        localStorage.removeItem('isLoggedin');
        localStorage.removeItem('tokenlogin');
        localStorage.removeItem('devmode');
    };
    NestedSidebarComponent.prototype.orderhistory = function () {
        // this.router.navigate(["merchant-portal/order-histories"])
    };
    NestedSidebarComponent.ctorParameters = function () { return [
        { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _services_storage_service_module_storage_service_module_service__WEBPACK_IMPORTED_MODULE_4__["LocalStorageService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"] },
        { type: _services_program_name_service__WEBPACK_IMPORTED_MODULE_6__["ProgramNameService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], NestedSidebarComponent.prototype, "collapsedEvent", void 0);
    NestedSidebarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-nested-sidebar',
            template: __webpack_require__(/*! raw-loader!./nested-sidebar.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/components/nested-sidebar/nested-sidebar.component.html"),
            styles: [__webpack_require__(/*! ./nested-sidebar.component.scss */ "./src/app/layout/components/nested-sidebar/nested-sidebar.component.scss")]
        }),
        __metadata("design:paramtypes", [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _services_storage_service_module_storage_service_module_service__WEBPACK_IMPORTED_MODULE_4__["LocalStorageService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _services_program_name_service__WEBPACK_IMPORTED_MODULE_6__["ProgramNameService"]])
    ], NestedSidebarComponent);
    return NestedSidebarComponent;
}());



/***/ }),

/***/ "./src/app/layout/components/nested-sidebar/nested-sidebarmenu.ts":
/*!************************************************************************!*\
  !*** ./src/app/layout/components/nested-sidebar/nested-sidebarmenu.ts ***!
  \************************************************************************/
/*! exports provided: sidebarmenu */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sidebarmenu", function() { return sidebarmenu; });
var sidebarmenu = {
    "Order History": [
        // {label: "Summary", routerLink:'/administrator/order-history-summary'},
        { label: "All History", routerLink: '/administrator/orderhistoryallhistoryadmin' },
        { label: "Processed", routerLink: '/administrator/orderhistorysuccessadmin' },
        // {label: "Pending", routerLink:'/administrator/orderhistorypendingadmin'},
        { label: "Cancel", routerLink: '/administrator/orderhistorycanceladmin' },
        // {label: "Failed", routerLink:'/administrator/orderhistoryfailedadmin'},
        { label: "Waiting", routerLink: '/administrator/orderhistorywaitingadmin' },
        { label: "Completed", routerLink: '/administrator/orderhistorycompleteadmin' },
        { label: "Return", routerLink: '/administrator/orderhistoryreturnadmin' },
    ],
    "PO Number": [
        { label: "Update", routerLink: '/administrator/po-bulk-update' },
        { label: "Report", routerLink: '/administrator/po-report' },
    ],
    "Sales Redemption": [
        { label: "Report", routerLink: '/administrator/salesredemption' },
        { label: "Generate BAST", routerLink: '/administrator/salesredemptionbast' }
    ],
    "Sales Redemption MCI": [
        { label: "Report", routerLink: '/administrator/salesredemption' },
    ],
    "Member": [
        // {label: "Summary", routerLink:'/administrator/membersummary'},
        // {label: "All Members", routerLink:'/administrator/allmembersadmin'},
        // {label: "Member Registration", routerLink:'/administrator/datamember'},
        { label: "Report", routerLink: '/administrator/datamember' },
        { label: "Form Member", routerLink: '/administrator/formmember' },
    ],
    "Reportnew": [
        { label: "Order Report", routerLink: '/administrator/pointmovement' },
        { label: "Finance Invoice Report", routerLink: '/administrator/financeinvoicereport' },
    ],
    "Products": [
        // {label: "Upload Product CSV", routerLink:'/administrator/testproductadmin'},
        { label: "Add", routerLink: '/administrator/productadmin/add' },
        { label: "Product", routerLink: '/administrator/productadmin' },
        { label: "Evoucher", routerLink: '/administrator/vouchers' },
    ],
    "Category": [
        { label: "Add", routerLink: '/administrator/category/add' },
        { label: "Category", routerLink: '/administrator/categoryadmin' },
    ],
    "Payment": [
        { label: "Payment Gateway", routerLink: '/administrator/paymentgatewayadmin' }
    ],
    "Loyalty": [
        // {label: "Points Model Group", routerLink:'/administrator/pointsgroup'},
        { label: "Points Models Setup", routerLink: '/administrator/pointsmodels' },
    ],
    "Point Movement": [
        { label: "Based on Product", routerLink: '/administrator/pointmovement' },
    ],
    "Notification": [
        { label: "Notification Group", routerLink: '/administrator/notificationgroupadmin' },
        { label: "Notification Setting", routerLink: '/administrator/notificationsettingadmin' },
        { label: "Notification Broadcast", routerLink: '/administrator/notificationbroadcastadmin' },
        { label: "Notification Message", routerLink: '/administrator/notificationmessageadmin' },
    ],
    "Evoucher": [
        { label: "Evoucher Stock Report", routerLink: '/administrator/evoucherstockreport' },
        { label: "Evoucher Summary Report", routerLink: '/administrator/evouchersummaryreport' },
        { label: "Sales Report", routerLink: '/administrator/evouchersales' },
    ],
    "Shipping": [
        // {label: "Report", routerLink:'/administrator/shippingreport'},
        // {label: "Process Shipping", routerLink:'/administrator/shippingprocess'},
        { label: "Process Shipping", routerLink: '/administrator/deliveryprocess' },
        { label: "Packing List", routerLink: '/administrator/packinglist' },
        { label: "Update Shipping Status", routerLink: '/administrator/shippingbulkprocess' }
        // {label: "Report", routerLink:'/administrator/evouchersalesreport'},
        // {label: "Shipping Processing", routerLink:'/administrator/evoucheradmin'},
    ],
    "E-Wallet": [
        { label: "Sales Report", routerLink: '/administrator/ewalletsalesreport' },
    ],
    "Report": [
        { label: "Sales Summary report", routerLink: '/administrator/order-history-summary' },
        { label: "Member Demography", routerLink: '/administrator/member-demography-report' },
        { label: "Sales Order", routerLink: '/administrator/salesorder' },
        { label: "Traffic Activity", routerLink: '/administrator/activity-report' },
        { label: "Product Activity", routerLink: '/administrator/product-activity-report' },
        { label: "Report", routerLink: '/administrator/reportadmin' },
        { label: "Report Error", routerLink: '/administrator/reporterror' },
        { label: "CRR", routerLink: '/administrator/crr' },
        { label: "CNR", routerLink: '/administrator/cnr' },
    ],
    "Product": [
        { label: "Product", routerLink: '/administrator/product' },
        { label: "Evoucher", routerLink: '/administrator/evoucher' }
    ]
};


/***/ }),

/***/ "./src/app/layout/components/sidebar/sidebar.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/layout/components/sidebar/sidebar.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".new-line {\n  white-space: pre-wrap;\n}\n\n.sidebar {\n  font-size: small;\n  box-shadow: 0 4px 1px 0 rgba(0, 0, 0, 0.01), 0 3px 5px 0 rgba(0, 0, 0, 0.19);\n  border-radius: 0;\n  position: fixed;\n  z-index: 1000;\n  top: 60px;\n  left: 235px;\n  width: 150px;\n  height: 100%;\n  margin-left: -235px;\n  margin-bottom: 48px;\n  border: none;\n  border-radius: 0;\n  overflow-y: auto;\n  background-color: #2b3240;\n  padding-bottom: 40px;\n  padding-top: 70px;\n  white-space: nowrap;\n  transition: all 0.2s ease-in-out;\n  text-align: center;\n  margin-top: 23px;\n}\n\n.sidebar .list-group {\n  padding-bottom: 20px;\n}\n\n.sidebar .list-group a.list-group-item {\n  background: #2b3240;\n  border: 0;\n  border-radius: 0;\n  color: #ffffff;\n  text-decoration: none;\n  padding-top: 15px;\n  padding-bottom: 15px;\n  padding-right: 5px;\n  padding-left: 5px;\n  text-align: left;\n  vertical-align: 10px;\n}\n\n.sidebar .list-group a.list-group-item > i {\n  display: block;\n  font-family: \"fontello\";\n  font-style: normal;\n}\n\n.sidebar .list-group a.list-group-item .fa {\n  margin-right: 10px;\n}\n\n.sidebar .list-group a.list-group-item > p {\n  margin-top: 5px;\n}\n\n.sidebar .list-group a.list-group-item > p:active {\n  color: #56a4ff;\n}\n\n.sidebar .list-group a:hover {\n  background: #3b4558;\n  color: #56a4ff;\n  border-left: 2px #56a4ff solid;\n}\n\n.sidebar .list-group a.router-link-active {\n  background: #3b4558;\n  color: #56a4ff;\n  border-left: 2px #56a4ff solid;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n}\n\n.sidebar .list-group .header-fields {\n  padding-top: 10px;\n}\n\n.sidebar .list-group .header-fields > .list-group-item:first-child {\n  border-top: 1px solid rgba(255, 255, 255, 0.2);\n}\n\n.sidebar .list-group img {\n  width: 35px;\n}\n\n.sidebar p {\n  margin-bottom: 0px;\n  margin-top: 0px;\n}\n\n.sidebar .sidebar-dropdown *:focus {\n  border-radius: none;\n  border: none;\n}\n\n.sidebar .sidebar-dropdown .panel-title {\n  font-size: 1rem;\n  height: 50px;\n  margin-bottom: 0;\n}\n\n.sidebar .sidebar-dropdown .panel-title a {\n  color: #999;\n  text-decoration: none;\n  font-weight: 400;\n  background: #2b3240;\n}\n\n.sidebar .sidebar-dropdown .panel-title a span {\n  position: relative;\n  display: block;\n  padding: 0.75rem 1.5rem;\n  padding-top: 1rem;\n}\n\n.sidebar .sidebar-dropdown .panel-title a:hover,\n.sidebar .sidebar-dropdown .panel-title a:focus {\n  color: #fff;\n  outline: none;\n  outline-offset: -2px;\n}\n\n.sidebar .sidebar-dropdown .panel-title:hover {\n  background: #212631;\n}\n\n.sidebar .sidebar-dropdown .panel-collapse {\n  border-radius: 0;\n  border: none;\n}\n\n.sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item {\n  border-radius: 0;\n  background-color: #2b3240;\n  border: 0 solid transparent;\n}\n\n.sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item a {\n  color: #999;\n}\n\n.sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item a:hover {\n  color: #fff;\n}\n\n.sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item:hover {\n  background: #212631;\n}\n\n.sidebar .list-group > a.list-group-item .fa {\n  display: block;\n  clear: both;\n  font-size: 36px;\n  margin-right: 0px;\n}\n\n.sidebar .nested-menu .list-group-item {\n  cursor: pointer;\n}\n\n.sidebar .nested-menu i img {\n  width: 45px;\n  margin-bottom: 5px;\n}\n\n.sidebar .nested-menu > a.list-group-item .fa {\n  display: block;\n  clear: both;\n  font-size: 36px;\n  margin-right: 0px;\n}\n\n.sidebar .nested-menu .nested {\n  list-style-type: none;\n}\n\n.sidebar .nested-menu ul.submenu {\n  display: none;\n  height: 0;\n  margin: 0;\n  padding: 0;\n  background: #212631;\n}\n\n.sidebar .nested-menu .expand ul.submenu {\n  display: block;\n  height: auto;\n  background: #212631;\n}\n\n.sidebar .nested-menu .expand ul.submenu a.list-group-item {\n  background: #394458;\n  border-bottom: 1px solid #2b3240;\n  border-top: 1px solid rgba(255, 255, 255, 0.1);\n}\n\n.sidebar .nested-menu .expand ul.submenu a.list-group-item .circle-custom {\n  width: 1px;\n  border-radius: 50%;\n  background-color: #247de5;\n}\n\n.sidebar .nested-menu .expand ul.submenu a.list-group-item span {\n  margin-bottom: 5px;\n  color: #9ea6b7;\n  padding: 0px;\n  list-style: circle;\n}\n\n.sidebar .nested-menu .expand ul.submenu a.list-group-item.router-link-active {\n  background: #247de5;\n}\n\n.sidebar .nested-menu .expand ul.submenu a.list-group-item.router-link-active span {\n  color: white;\n}\n\n.sidebar .nested-menu .expand ul.submenu a.list-group-item:hover p {\n  color: white;\n}\n\n.sidebar .nested-menu .expand ul.submenu li p {\n  color: #627190;\n  padding: 10px;\n  display: block;\n}\n\n@media (max-width: 992px) {\n  /* smartphones, iPhone, portrait 480x320 phones */\n  .sidebar {\n    top: 60px;\n    left: 0px;\n    padding-bottom: 3em;\n  }\n}\n\n@media (max-width: 600px) {\n  /* smartphones, iPhone, portrait 480x320 phones */\n  .sidebar {\n    margin-top: -3%;\n    top: 8em;\n    left: 0px;\n    height: 97%;\n    width: 25%;\n    padding-bottom: 3em;\n    position: fixed;\n  }\n  .sidebar .list-group > a.list-group-item .fa {\n    font-size: 40px;\n  }\n  .sidebar .list-group img {\n    width: 40px;\n  }\n  .sidebar .nested-menu > a.list-group-item .fa {\n    font-size: 40px;\n  }\n}\n\n@media print {\n  .sidebar {\n    display: none !important;\n  }\n}\n\n::-webkit-scrollbar {\n  width: 8px;\n}\n\n::-webkit-scrollbar-track {\n  -webkit-box-shadow: inset 0 0 0px white;\n  border-radius: 3px;\n}\n\n::-webkit-scrollbar-thumb {\n  border-radius: 3px;\n  -webkit-box-shadow: inset 0 0 3px white;\n}\n\n.toggle-button {\n  position: fixed;\n  width: 236px;\n  cursor: pointer;\n  padding: 12px;\n  bottom: 0;\n  color: #999;\n  background: #212529;\n  border-top: 1px solid #999;\n  transition: all 0.2s ease-in-out;\n}\n\n.toggle-button i {\n  font-size: 23px;\n}\n\n.toggle-button:hover {\n  background: #212631;\n  color: #56a4ff;\n}\n\n.collapsed {\n  width: 50px;\n}\n\n.collapsed span {\n  display: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L2NvbXBvbmVudHMvc2lkZWJhci9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXGNvbXBvbmVudHNcXHNpZGViYXJcXHNpZGViYXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9jb21wb25lbnRzL3NpZGViYXIvc2lkZWJhci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFjQTtFQUNFLHFCQUFBO0FDYkY7O0FEZ0JBO0VBQ0UsZ0JBQUE7RUFDQSw0RUFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGFBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EseUJBckJ3QjtFQXdCeEIsb0JBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBS0EsZ0NBQUE7RUFDQSxrQkFBQTtFQUVBLGdCQUFBO0FDaEJGOztBRG1CRTtFQUNFLG9CQUFBO0FDakJKOztBRG1CSTtFQUNFLG1CQXpDb0I7RUEwQ3BCLFNBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxxQkFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBYUEsZ0JBQUE7RUFDQSxvQkFBQTtBQzdCTjs7QURpQk07RUFDRSxjQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtBQ2ZSOztBRG1CTTtFQUNFLGtCQUFBO0FDakJSOztBRHdCSTtFQUNFLGVBQUE7QUN0Qk47O0FEeUJJO0VBQ0UsY0FBQTtBQ3ZCTjs7QUQwQkk7RUFDRSxtQkFBQTtFQUNBLGNBQUE7RUFDQSw4QkFBQTtBQ3hCTjs7QUQyQkk7RUFDRSxtQkFBQTtFQUNBLGNBQUE7RUFDQSw4QkFBQTtFQUNBLDRFQUFBO0FDekJOOztBRDRCSTtFQUNFLGlCQUFBO0FDMUJOOztBRDRCTTtFQUNFLDhDQUFBO0FDMUJSOztBRDhCSTtFQUNFLFdBQUE7QUM1Qk47O0FEZ0NFO0VBQ0Usa0JBQUE7RUFDQSxlQUFBO0FDOUJKOztBRGtDSTtFQUNFLG1CQUFBO0VBQ0EsWUFBQTtBQ2hDTjs7QURtQ0k7RUFDRSxlQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0FDakNOOztBRG1DTTtFQUNFLFdBQUE7RUFDQSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBeEhrQjtBQ3VGMUI7O0FEbUNRO0VBQ0Usa0JBQUE7RUFDQSxjQUFBO0VBQ0EsdUJBQUE7RUFDQSxpQkFBQTtBQ2pDVjs7QURxQ007O0VBRUUsV0FBQTtFQUNBLGFBQUE7RUFDQSxvQkFBQTtBQ25DUjs7QUR1Q0k7RUFDRSxtQkFBQTtBQ3JDTjs7QUR3Q0k7RUFDRSxnQkFBQTtFQUNBLFlBQUE7QUN0Q047O0FEeUNRO0VBQ0UsZ0JBQUE7RUFDQSx5QkFySmdCO0VBc0poQiwyQkFBQTtBQ3ZDVjs7QUR5Q1U7RUFDRSxXQUFBO0FDdkNaOztBRDBDVTtFQUNFLFdBQUE7QUN4Q1o7O0FENENRO0VBQ0UsbUJBQUE7QUMxQ1Y7O0FEa0RNO0VBQ0UsY0FBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUNoRFI7O0FEc0RJO0VBQ0UsZUFBQTtBQ3BETjs7QUR3RE07RUFDRSxXQUFBO0VBQ0Esa0JBQUE7QUN0RFI7O0FEMkRNO0VBQ0UsY0FBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUN6RFI7O0FENkRJO0VBQ0UscUJBQUE7QUMzRE47O0FEOERJO0VBQ0UsYUFBQTtFQUNBLFNBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQUFBO0FDNUROOztBRGdFTTtFQUNFLGNBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUM5RFI7O0FEZ0VRO0VBQ0UsbUJBQUE7RUFDQSxnQ0FBQTtFQUNBLDhDQUFBO0FDOURWOztBRCtEVTtFQUNFLFVBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0FDN0RaOztBRCtEVTtFQUNFLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQzdEWjs7QURpRVE7RUFDRSxtQkFBQTtBQy9EVjs7QURnRVU7RUFDRSxZQUFBO0FDOURaOztBRG1FVTtFQUNFLFlBQUE7QUNqRVo7O0FEc0VVO0VBQ0UsY0FBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0FDcEVaOztBRDJFQTtFQUNFLGlEQUFBO0VBQ0E7SUFDRSxTQUFBO0lBQ0EsU0FBQTtJQUNBLG1CQUFBO0VDeEVGO0FBQ0Y7O0FEMEVBO0VBQ0UsaURBQUE7RUFDQTtJQUNFLGVBQUE7SUFDQSxRQUFBO0lBQ0EsU0FBQTtJQUNBLFdBQUE7SUFDQSxVQUFBO0lBQ0EsbUJBQUE7SUFDQSxlQUFBO0VDeEVGO0VENEVNO0lBQ0UsZUFBQTtFQzFFUjtFRDhFSTtJQUNFLFdBQUE7RUM1RU47RURrRk07SUFDRSxlQUFBO0VDaEZSO0FBQ0Y7O0FEa0dBO0VBQ0U7SUFDRSx3QkFBQTtFQ2hHRjtBQUNGOztBRG1HQTtFQUNFLFVBQUE7QUNqR0Y7O0FEb0dBO0VBQ0UsdUNBQUE7RUFDQSxrQkFBQTtBQ2pHRjs7QURvR0E7RUFDRSxrQkFBQTtFQUNBLHVDQUFBO0FDakdGOztBRG9HQTtFQUNFLGVBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGFBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBV0EsMEJBQUE7RUFLQSxnQ0FBQTtBQzNHRjs7QUQ2RkU7RUFDRSxlQUFBO0FDM0ZKOztBRDhGRTtFQUNFLG1CQUFBO0VBQ0EsY0FBQTtBQzVGSjs7QUR1R0E7RUFDRSxXQUFBO0FDcEdGOztBRHNHRTtFQUNFLGFBQUE7QUNwR0oiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvY29tcG9uZW50cy9zaWRlYmFyL3NpZGViYXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBAZm9udC1mYWNlIHtcclxuLy8gICBmb250LWZhbWlseTogJ2ZvbnRlbGxvJztcclxuLy8gICBzcmM6IHVybCgnLi9mb250L2ZvbnRlbGxvLmVvdD83NTE3Nzc2NCcpO1xyXG4vLyAgIHNyYzogdXJsKCcuL2ZvbnQvZm9udGVsbG8uZW90Pzc1MTc3NzY0I2llZml4JykgZm9ybWF0KCdlbWJlZGRlZC1vcGVudHlwZScpLFxyXG4vLyAgICAgICAgdXJsKCcuL2ZvbnQvZm9udGVsbG8ud29mZjI/NzUxNzc3NjQnKSBmb3JtYXQoJ3dvZmYyJyksXHJcbi8vICAgICAgICB1cmwoJy4vZm9udC9mb250ZWxsby53b2ZmPzc1MTc3NzY0JykgZm9ybWF0KCd3b2ZmJyksXHJcbi8vICAgICAgICB1cmwoJy4vZm9udC9mb250ZWxsby50dGY/NzUxNzc3NjQnKSBmb3JtYXQoJ3RydWV0eXBlJyksXHJcbi8vICAgICAgICB1cmwoJy4vZm9udC9mb250ZWxsby5zdmc/NzUxNzc3NjQjZm9udGVsbG8nKSBmb3JtYXQoJ3N2ZycpO1xyXG4vLyAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbi8vICAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4vLyB9XHJcblxyXG4kdG9wbmF2LWJhY2tncm91bmQtY29sb3I6ICMyYjMyNDA7XHJcbi8vJHRvcG5hdi1iYWNrZ3JvdW5kLWNvbG9yOiAjMjUyQzM5O1xyXG4ubmV3LWxpbmUge1xyXG4gIHdoaXRlLXNwYWNlOiBwcmUtd3JhcDtcclxufVxyXG5cclxuLnNpZGViYXIge1xyXG4gIGZvbnQtc2l6ZTogc21hbGw7XHJcbiAgYm94LXNoYWRvdzogMCA0cHggMXB4IDAgcmdiYSgwLCAwLCAwLCAwLjAxKSwgMCAzcHggNXB4IDAgcmdiYSgwLCAwLCAwLCAwLjE5KTtcclxuICBib3JkZXItcmFkaXVzOiAwO1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICB6LWluZGV4OiAxMDAwO1xyXG4gIHRvcDogNjBweDtcclxuICBsZWZ0OiAyMzVweDtcclxuICB3aWR0aDogMTUwcHg7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIG1hcmdpbi1sZWZ0OiAtMjM1cHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogNDhweDtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgYm9yZGVyLXJhZGl1czogMDtcclxuICBvdmVyZmxvdy15OiBhdXRvO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICR0b3BuYXYtYmFja2dyb3VuZC1jb2xvcjtcclxuICAvL2JvdHRvbTogMDtcclxuICAvLyBvdmVyZmxvdy14OiBoaWRkZW47XHJcbiAgcGFkZGluZy1ib3R0b206IDQwcHg7XHJcbiAgcGFkZGluZy10b3A6IDcwcHg7XHJcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xyXG4gIC1tb3otdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XHJcbiAgLW1zLXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xyXG4gIC1vLXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAvL2J1YXQgZGV2bW9kZVxyXG4gIG1hcmdpbi10b3A6IDIzcHg7XHJcblxyXG4gIC8vIGJvcmRlci10b3A6IDFweCBzb2xpZCByZ2JhKDI1NSwyNTUsMjU1LDAuMyk7XHJcbiAgLmxpc3QtZ3JvdXAge1xyXG4gICAgcGFkZGluZy1ib3R0b206IDIwcHg7XHJcblxyXG4gICAgYS5saXN0LWdyb3VwLWl0ZW0ge1xyXG4gICAgICBiYWNrZ3JvdW5kOiAkdG9wbmF2LWJhY2tncm91bmQtY29sb3I7XHJcbiAgICAgIGJvcmRlcjogMDtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMDtcclxuICAgICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgcGFkZGluZy10b3A6IDE1cHg7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiAxNXB4O1xyXG4gICAgICBwYWRkaW5nLXJpZ2h0OiA1cHg7XHJcbiAgICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG5cclxuICAgICAgPiBpIHtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICBmb250LWZhbWlseTogXCJmb250ZWxsb1wiO1xyXG4gICAgICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICAgICAgfVxyXG5cclxuICAgICAgLy9wYWRkaW5nLWxlZnQ6IDRweDtcclxuICAgICAgLmZhIHtcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgIHZlcnRpY2FsLWFsaWduOiAxMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIGEubGlzdC1ncm91cC1pdGVtID4gcCB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDVweDtcclxuICAgIH1cclxuXHJcbiAgICBhLmxpc3QtZ3JvdXAtaXRlbSA+IHA6YWN0aXZlIHtcclxuICAgICAgY29sb3I6ICM1NmE0ZmY7XHJcbiAgICB9XHJcblxyXG4gICAgYTpob3ZlciB7XHJcbiAgICAgIGJhY2tncm91bmQ6IGxpZ2h0ZW4oJHRvcG5hdi1iYWNrZ3JvdW5kLWNvbG9yLCA4JSk7XHJcbiAgICAgIGNvbG9yOiAjNTZhNGZmO1xyXG4gICAgICBib3JkZXItbGVmdDogMnB4ICM1NmE0ZmYgc29saWQ7XHJcbiAgICB9XHJcblxyXG4gICAgYS5yb3V0ZXItbGluay1hY3RpdmUge1xyXG4gICAgICBiYWNrZ3JvdW5kOiBsaWdodGVuKCR0b3BuYXYtYmFja2dyb3VuZC1jb2xvciwgOCUpO1xyXG4gICAgICBjb2xvcjogIzU2YTRmZjtcclxuICAgICAgYm9yZGVyLWxlZnQ6IDJweCAjNTZhNGZmIHNvbGlkO1xyXG4gICAgICBib3gtc2hhZG93OiAwIDRweCA4cHggMCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgNnB4IDIwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTkpO1xyXG4gICAgfVxyXG5cclxuICAgIC5oZWFkZXItZmllbGRzIHtcclxuICAgICAgcGFkZGluZy10b3A6IDEwcHg7XHJcblxyXG4gICAgICA+IC5saXN0LWdyb3VwLWl0ZW06Zmlyc3QtY2hpbGQge1xyXG4gICAgICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMik7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpbWcge1xyXG4gICAgICB3aWR0aDogMzVweDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHAge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgbWFyZ2luLXRvcDogMHB4O1xyXG4gIH1cclxuXHJcbiAgLnNpZGViYXItZHJvcGRvd24ge1xyXG4gICAgKjpmb2N1cyB7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IG5vbmU7XHJcbiAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgIH1cclxuXHJcbiAgICAucGFuZWwtdGl0bGUge1xyXG4gICAgICBmb250LXNpemU6IDFyZW07XHJcbiAgICAgIGhlaWdodDogNTBweDtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMDtcclxuXHJcbiAgICAgIGEge1xyXG4gICAgICAgIGNvbG9yOiAjOTk5O1xyXG4gICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICR0b3BuYXYtYmFja2dyb3VuZC1jb2xvcjtcclxuXHJcbiAgICAgICAgc3BhbiB7XHJcbiAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgIHBhZGRpbmc6IDAuNzVyZW0gMS41cmVtO1xyXG4gICAgICAgICAgcGFkZGluZy10b3A6IDFyZW07XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICBhOmhvdmVyLFxyXG4gICAgICBhOmZvY3VzIHtcclxuICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgICAgIG91dGxpbmUtb2Zmc2V0OiAtMnB4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLnBhbmVsLXRpdGxlOmhvdmVyIHtcclxuICAgICAgYmFja2dyb3VuZDogZGFya2VuKCR0b3BuYXYtYmFja2dyb3VuZC1jb2xvciwgNSUpO1xyXG4gICAgfVxyXG5cclxuICAgIC5wYW5lbC1jb2xsYXBzZSB7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgICAgIGJvcmRlcjogbm9uZTtcclxuXHJcbiAgICAgIC5wYW5lbC1ib2R5IHtcclxuICAgICAgICAubGlzdC1ncm91cC1pdGVtIHtcclxuICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkdG9wbmF2LWJhY2tncm91bmQtY29sb3I7XHJcbiAgICAgICAgICBib3JkZXI6IDAgc29saWQgdHJhbnNwYXJlbnQ7XHJcblxyXG4gICAgICAgICAgYSB7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjOTk5O1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIGE6aG92ZXIge1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5saXN0LWdyb3VwLWl0ZW06aG92ZXIge1xyXG4gICAgICAgICAgYmFja2dyb3VuZDogZGFya2VuKCR0b3BuYXYtYmFja2dyb3VuZC1jb2xvciwgNSUpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLmxpc3QtZ3JvdXAge1xyXG4gICAgPiBhLmxpc3QtZ3JvdXAtaXRlbSB7XHJcbiAgICAgIC5mYSB7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgY2xlYXI6IGJvdGg7XHJcbiAgICAgICAgZm9udC1zaXplOiAzNnB4O1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogMHB4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAubmVzdGVkLW1lbnUge1xyXG4gICAgLmxpc3QtZ3JvdXAtaXRlbSB7XHJcbiAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIH1cclxuXHJcbiAgICBpIHtcclxuICAgICAgaW1nIHtcclxuICAgICAgICB3aWR0aDogNDVweDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICA+IGEubGlzdC1ncm91cC1pdGVtIHtcclxuICAgICAgLmZhIHtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICBjbGVhcjogYm90aDtcclxuICAgICAgICBmb250LXNpemU6IDM2cHg7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAwcHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAubmVzdGVkIHtcclxuICAgICAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xyXG4gICAgfVxyXG5cclxuICAgIHVsLnN1Ym1lbnUge1xyXG4gICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgICBoZWlnaHQ6IDA7XHJcbiAgICAgIG1hcmdpbjogMDtcclxuICAgICAgcGFkZGluZzogMDtcclxuICAgICAgYmFja2dyb3VuZDogZGFya2VuKCR0b3BuYXYtYmFja2dyb3VuZC1jb2xvciwgNSUpO1xyXG4gICAgfVxyXG5cclxuICAgICYgLmV4cGFuZCB7XHJcbiAgICAgIHVsLnN1Ym1lbnUge1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgICBiYWNrZ3JvdW5kOiBkYXJrZW4oJHRvcG5hdi1iYWNrZ3JvdW5kLWNvbG9yLCA1JSk7XHJcblxyXG4gICAgICAgIGEubGlzdC1ncm91cC1pdGVtIHtcclxuICAgICAgICAgIGJhY2tncm91bmQ6ICMzOTQ0NTg7XHJcbiAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgJHRvcG5hdi1iYWNrZ3JvdW5kLWNvbG9yO1xyXG4gICAgICAgICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4xKTtcclxuICAgICAgICAgIC5jaXJjbGUtY3VzdG9tIHtcclxuICAgICAgICAgICAgd2lkdGg6IDFweDtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjQ3ZGU1O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgc3BhbiB7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxuICAgICAgICAgICAgY29sb3I6ICM5ZWE2Yjc7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweDtcclxuICAgICAgICAgICAgbGlzdC1zdHlsZTogY2lyY2xlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgYS5saXN0LWdyb3VwLWl0ZW0ucm91dGVyLWxpbmstYWN0aXZlIHtcclxuICAgICAgICAgIGJhY2tncm91bmQ6ICMyNDdkZTU7XHJcbiAgICAgICAgICBzcGFuIHtcclxuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgYS5saXN0LWdyb3VwLWl0ZW06aG92ZXIge1xyXG4gICAgICAgICAgcCB7XHJcbiAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxpIHtcclxuICAgICAgICAgIHAge1xyXG4gICAgICAgICAgICBjb2xvcjogIzYyNzE5MDtcclxuICAgICAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbkBtZWRpYSAobWF4LXdpZHRoOiA5OTJweCkge1xyXG4gIC8qIHNtYXJ0cGhvbmVzLCBpUGhvbmUsIHBvcnRyYWl0IDQ4MHgzMjAgcGhvbmVzICovXHJcbiAgLnNpZGViYXIge1xyXG4gICAgdG9wOiA2MHB4O1xyXG4gICAgbGVmdDogMHB4O1xyXG4gICAgcGFkZGluZy1ib3R0b206IDNlbTtcclxuICB9XHJcbn1cclxuQG1lZGlhIChtYXgtd2lkdGg6IDYwMHB4KSB7XHJcbiAgLyogc21hcnRwaG9uZXMsIGlQaG9uZSwgcG9ydHJhaXQgNDgweDMyMCBwaG9uZXMgKi9cclxuICAuc2lkZWJhciB7XHJcbiAgICBtYXJnaW4tdG9wOiAtMyU7XHJcbiAgICB0b3A6IDhlbTtcclxuICAgIGxlZnQ6IDBweDtcclxuICAgIGhlaWdodDogOTclO1xyXG4gICAgd2lkdGg6IDI1JTtcclxuICAgIHBhZGRpbmctYm90dG9tOiAzZW07XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcblxyXG4gICAgLmxpc3QtZ3JvdXAge1xyXG4gICAgICA+IGEubGlzdC1ncm91cC1pdGVtIHtcclxuICAgICAgICAuZmEge1xyXG4gICAgICAgICAgZm9udC1zaXplOiA0MHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgaW1nIHtcclxuICAgICAgICB3aWR0aDogNDBweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5uZXN0ZWQtbWVudSB7XHJcbiAgICAgID4gYS5saXN0LWdyb3VwLWl0ZW0ge1xyXG4gICAgICAgIC5mYSB7XHJcbiAgICAgICAgICBmb250LXNpemU6IDQwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4vL0BtZWRpYSAobWluLXdpZHRoOiA5NjFweCkge1xyXG4vLyAgICAvKiB0YWJsZXQsIGxhbmRzY2FwZSBpUGFkLCBsby1yZXMgbGFwdG9wcyBhbmRzIGRlc2t0b3BzICovXHJcbi8vfVxyXG4vL1xyXG4vL0BtZWRpYSAobWluLXdpZHRoOiAxMDI1cHgpIHtcclxuLy8gICAgLyogYmlnIGxhbmRzY2FwZSB0YWJsZXRzLCBsYXB0b3BzLCBhbmQgZGVza3RvcHMgKi9cclxuLy99XHJcbi8vXHJcbi8vQG1lZGlhIChtaW4td2lkdGg6IDEyODFweCkge1xyXG4vLyAgICAvKiBoaS1yZXMgbGFwdG9wcyBhbmQgZGVza3RvcHMgKi9cclxuLy99XHJcblxyXG5AbWVkaWEgcHJpbnQge1xyXG4gIC5zaWRlYmFyIHtcclxuICAgIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcclxuICB9XHJcbn1cclxuXHJcbjo6LXdlYmtpdC1zY3JvbGxiYXIge1xyXG4gIHdpZHRoOiA4cHg7XHJcbn1cclxuXHJcbjo6LXdlYmtpdC1zY3JvbGxiYXItdHJhY2sge1xyXG4gIC13ZWJraXQtYm94LXNoYWRvdzogaW5zZXQgMCAwIDBweCByZ2JhKDI1NSwgMjU1LCAyNTUsIDEpO1xyXG4gIGJvcmRlci1yYWRpdXM6IDNweDtcclxufVxyXG5cclxuOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XHJcbiAgYm9yZGVyLXJhZGl1czogM3B4O1xyXG4gIC13ZWJraXQtYm94LXNoYWRvdzogaW5zZXQgMCAwIDNweCByZ2JhKDI1NSwgMjU1LCAyNTUsIDEpO1xyXG59XHJcblxyXG4udG9nZ2xlLWJ1dHRvbiB7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIHdpZHRoOiAyMzZweDtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgcGFkZGluZzogMTJweDtcclxuICBib3R0b206IDA7XHJcbiAgY29sb3I6ICM5OTk7XHJcbiAgYmFja2dyb3VuZDogIzIxMjUyOTtcclxuXHJcbiAgaSB7XHJcbiAgICBmb250LXNpemU6IDIzcHg7XHJcbiAgfVxyXG5cclxuICAmOmhvdmVyIHtcclxuICAgIGJhY2tncm91bmQ6IGRhcmtlbigkdG9wbmF2LWJhY2tncm91bmQtY29sb3IsIDUlKTtcclxuICAgIGNvbG9yOiAjNTZhNGZmO1xyXG4gIH1cclxuXHJcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICM5OTk7XHJcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcclxuICAtbW96LXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xyXG4gIC1tcy10cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcclxuICAtby10cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcclxuICB0cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcclxufVxyXG5cclxuLmNvbGxhcHNlZCB7XHJcbiAgd2lkdGg6IDUwcHg7XHJcblxyXG4gIHNwYW4ge1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxuICB9XHJcbn1cclxuIiwiLm5ldy1saW5lIHtcbiAgd2hpdGUtc3BhY2U6IHByZS13cmFwO1xufVxuXG4uc2lkZWJhciB7XG4gIGZvbnQtc2l6ZTogc21hbGw7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDFweCAwIHJnYmEoMCwgMCwgMCwgMC4wMSksIDAgM3B4IDVweCAwIHJnYmEoMCwgMCwgMCwgMC4xOSk7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgei1pbmRleDogMTAwMDtcbiAgdG9wOiA2MHB4O1xuICBsZWZ0OiAyMzVweDtcbiAgd2lkdGg6IDE1MHB4O1xuICBoZWlnaHQ6IDEwMCU7XG4gIG1hcmdpbi1sZWZ0OiAtMjM1cHg7XG4gIG1hcmdpbi1ib3R0b206IDQ4cHg7XG4gIGJvcmRlcjogbm9uZTtcbiAgYm9yZGVyLXJhZGl1czogMDtcbiAgb3ZlcmZsb3cteTogYXV0bztcbiAgYmFja2dyb3VuZC1jb2xvcjogIzJiMzI0MDtcbiAgcGFkZGluZy1ib3R0b206IDQwcHg7XG4gIHBhZGRpbmctdG9wOiA3MHB4O1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xuICAtbW96LXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xuICAtbXMtdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XG4gIC1vLXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xuICB0cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tdG9wOiAyM3B4O1xufVxuLnNpZGViYXIgLmxpc3QtZ3JvdXAge1xuICBwYWRkaW5nLWJvdHRvbTogMjBweDtcbn1cbi5zaWRlYmFyIC5saXN0LWdyb3VwIGEubGlzdC1ncm91cC1pdGVtIHtcbiAgYmFja2dyb3VuZDogIzJiMzI0MDtcbiAgYm9yZGVyOiAwO1xuICBib3JkZXItcmFkaXVzOiAwO1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBwYWRkaW5nLXRvcDogMTVweDtcbiAgcGFkZGluZy1ib3R0b206IDE1cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgcGFkZGluZy1sZWZ0OiA1cHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIHZlcnRpY2FsLWFsaWduOiAxMHB4O1xufVxuLnNpZGViYXIgLmxpc3QtZ3JvdXAgYS5saXN0LWdyb3VwLWl0ZW0gPiBpIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtZmFtaWx5OiBcImZvbnRlbGxvXCI7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbn1cbi5zaWRlYmFyIC5saXN0LWdyb3VwIGEubGlzdC1ncm91cC1pdGVtIC5mYSB7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cbi5zaWRlYmFyIC5saXN0LWdyb3VwIGEubGlzdC1ncm91cC1pdGVtID4gcCB7XG4gIG1hcmdpbi10b3A6IDVweDtcbn1cbi5zaWRlYmFyIC5saXN0LWdyb3VwIGEubGlzdC1ncm91cC1pdGVtID4gcDphY3RpdmUge1xuICBjb2xvcjogIzU2YTRmZjtcbn1cbi5zaWRlYmFyIC5saXN0LWdyb3VwIGE6aG92ZXIge1xuICBiYWNrZ3JvdW5kOiAjM2I0NTU4O1xuICBjb2xvcjogIzU2YTRmZjtcbiAgYm9yZGVyLWxlZnQ6IDJweCAjNTZhNGZmIHNvbGlkO1xufVxuLnNpZGViYXIgLmxpc3QtZ3JvdXAgYS5yb3V0ZXItbGluay1hY3RpdmUge1xuICBiYWNrZ3JvdW5kOiAjM2I0NTU4O1xuICBjb2xvcjogIzU2YTRmZjtcbiAgYm9yZGVyLWxlZnQ6IDJweCAjNTZhNGZmIHNvbGlkO1xuICBib3gtc2hhZG93OiAwIDRweCA4cHggMCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgNnB4IDIwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTkpO1xufVxuLnNpZGViYXIgLmxpc3QtZ3JvdXAgLmhlYWRlci1maWVsZHMge1xuICBwYWRkaW5nLXRvcDogMTBweDtcbn1cbi5zaWRlYmFyIC5saXN0LWdyb3VwIC5oZWFkZXItZmllbGRzID4gLmxpc3QtZ3JvdXAtaXRlbTpmaXJzdC1jaGlsZCB7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMik7XG59XG4uc2lkZWJhciAubGlzdC1ncm91cCBpbWcge1xuICB3aWR0aDogMzVweDtcbn1cbi5zaWRlYmFyIHAge1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIG1hcmdpbi10b3A6IDBweDtcbn1cbi5zaWRlYmFyIC5zaWRlYmFyLWRyb3Bkb3duICo6Zm9jdXMge1xuICBib3JkZXItcmFkaXVzOiBub25lO1xuICBib3JkZXI6IG5vbmU7XG59XG4uc2lkZWJhciAuc2lkZWJhci1kcm9wZG93biAucGFuZWwtdGl0bGUge1xuICBmb250LXNpemU6IDFyZW07XG4gIGhlaWdodDogNTBweDtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbn1cbi5zaWRlYmFyIC5zaWRlYmFyLWRyb3Bkb3duIC5wYW5lbC10aXRsZSBhIHtcbiAgY29sb3I6ICM5OTk7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgYmFja2dyb3VuZDogIzJiMzI0MDtcbn1cbi5zaWRlYmFyIC5zaWRlYmFyLWRyb3Bkb3duIC5wYW5lbC10aXRsZSBhIHNwYW4ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwYWRkaW5nOiAwLjc1cmVtIDEuNXJlbTtcbiAgcGFkZGluZy10b3A6IDFyZW07XG59XG4uc2lkZWJhciAuc2lkZWJhci1kcm9wZG93biAucGFuZWwtdGl0bGUgYTpob3Zlcixcbi5zaWRlYmFyIC5zaWRlYmFyLWRyb3Bkb3duIC5wYW5lbC10aXRsZSBhOmZvY3VzIHtcbiAgY29sb3I6ICNmZmY7XG4gIG91dGxpbmU6IG5vbmU7XG4gIG91dGxpbmUtb2Zmc2V0OiAtMnB4O1xufVxuLnNpZGViYXIgLnNpZGViYXItZHJvcGRvd24gLnBhbmVsLXRpdGxlOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogIzIxMjYzMTtcbn1cbi5zaWRlYmFyIC5zaWRlYmFyLWRyb3Bkb3duIC5wYW5lbC1jb2xsYXBzZSB7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIGJvcmRlcjogbm9uZTtcbn1cbi5zaWRlYmFyIC5zaWRlYmFyLWRyb3Bkb3duIC5wYW5lbC1jb2xsYXBzZSAucGFuZWwtYm9keSAubGlzdC1ncm91cC1pdGVtIHtcbiAgYm9yZGVyLXJhZGl1czogMDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzJiMzI0MDtcbiAgYm9yZGVyOiAwIHNvbGlkIHRyYW5zcGFyZW50O1xufVxuLnNpZGViYXIgLnNpZGViYXItZHJvcGRvd24gLnBhbmVsLWNvbGxhcHNlIC5wYW5lbC1ib2R5IC5saXN0LWdyb3VwLWl0ZW0gYSB7XG4gIGNvbG9yOiAjOTk5O1xufVxuLnNpZGViYXIgLnNpZGViYXItZHJvcGRvd24gLnBhbmVsLWNvbGxhcHNlIC5wYW5lbC1ib2R5IC5saXN0LWdyb3VwLWl0ZW0gYTpob3ZlciB7XG4gIGNvbG9yOiAjZmZmO1xufVxuLnNpZGViYXIgLnNpZGViYXItZHJvcGRvd24gLnBhbmVsLWNvbGxhcHNlIC5wYW5lbC1ib2R5IC5saXN0LWdyb3VwLWl0ZW06aG92ZXIge1xuICBiYWNrZ3JvdW5kOiAjMjEyNjMxO1xufVxuLnNpZGViYXIgLmxpc3QtZ3JvdXAgPiBhLmxpc3QtZ3JvdXAtaXRlbSAuZmEge1xuICBkaXNwbGF5OiBibG9jaztcbiAgY2xlYXI6IGJvdGg7XG4gIGZvbnQtc2l6ZTogMzZweDtcbiAgbWFyZ2luLXJpZ2h0OiAwcHg7XG59XG4uc2lkZWJhciAubmVzdGVkLW1lbnUgLmxpc3QtZ3JvdXAtaXRlbSB7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5zaWRlYmFyIC5uZXN0ZWQtbWVudSBpIGltZyB7XG4gIHdpZHRoOiA0NXB4O1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG59XG4uc2lkZWJhciAubmVzdGVkLW1lbnUgPiBhLmxpc3QtZ3JvdXAtaXRlbSAuZmEge1xuICBkaXNwbGF5OiBibG9jaztcbiAgY2xlYXI6IGJvdGg7XG4gIGZvbnQtc2l6ZTogMzZweDtcbiAgbWFyZ2luLXJpZ2h0OiAwcHg7XG59XG4uc2lkZWJhciAubmVzdGVkLW1lbnUgLm5lc3RlZCB7XG4gIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcbn1cbi5zaWRlYmFyIC5uZXN0ZWQtbWVudSB1bC5zdWJtZW51IHtcbiAgZGlzcGxheTogbm9uZTtcbiAgaGVpZ2h0OiAwO1xuICBtYXJnaW46IDA7XG4gIHBhZGRpbmc6IDA7XG4gIGJhY2tncm91bmQ6ICMyMTI2MzE7XG59XG4uc2lkZWJhciAubmVzdGVkLW1lbnUgLmV4cGFuZCB1bC5zdWJtZW51IHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGhlaWdodDogYXV0bztcbiAgYmFja2dyb3VuZDogIzIxMjYzMTtcbn1cbi5zaWRlYmFyIC5uZXN0ZWQtbWVudSAuZXhwYW5kIHVsLnN1Ym1lbnUgYS5saXN0LWdyb3VwLWl0ZW0ge1xuICBiYWNrZ3JvdW5kOiAjMzk0NDU4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzJiMzI0MDtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4xKTtcbn1cbi5zaWRlYmFyIC5uZXN0ZWQtbWVudSAuZXhwYW5kIHVsLnN1Ym1lbnUgYS5saXN0LWdyb3VwLWl0ZW0gLmNpcmNsZS1jdXN0b20ge1xuICB3aWR0aDogMXB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyNDdkZTU7XG59XG4uc2lkZWJhciAubmVzdGVkLW1lbnUgLmV4cGFuZCB1bC5zdWJtZW51IGEubGlzdC1ncm91cC1pdGVtIHNwYW4ge1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gIGNvbG9yOiAjOWVhNmI3O1xuICBwYWRkaW5nOiAwcHg7XG4gIGxpc3Qtc3R5bGU6IGNpcmNsZTtcbn1cbi5zaWRlYmFyIC5uZXN0ZWQtbWVudSAuZXhwYW5kIHVsLnN1Ym1lbnUgYS5saXN0LWdyb3VwLWl0ZW0ucm91dGVyLWxpbmstYWN0aXZlIHtcbiAgYmFja2dyb3VuZDogIzI0N2RlNTtcbn1cbi5zaWRlYmFyIC5uZXN0ZWQtbWVudSAuZXhwYW5kIHVsLnN1Ym1lbnUgYS5saXN0LWdyb3VwLWl0ZW0ucm91dGVyLWxpbmstYWN0aXZlIHNwYW4ge1xuICBjb2xvcjogd2hpdGU7XG59XG4uc2lkZWJhciAubmVzdGVkLW1lbnUgLmV4cGFuZCB1bC5zdWJtZW51IGEubGlzdC1ncm91cC1pdGVtOmhvdmVyIHAge1xuICBjb2xvcjogd2hpdGU7XG59XG4uc2lkZWJhciAubmVzdGVkLW1lbnUgLmV4cGFuZCB1bC5zdWJtZW51IGxpIHAge1xuICBjb2xvcjogIzYyNzE5MDtcbiAgcGFkZGluZzogMTBweDtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbkBtZWRpYSAobWF4LXdpZHRoOiA5OTJweCkge1xuICAvKiBzbWFydHBob25lcywgaVBob25lLCBwb3J0cmFpdCA0ODB4MzIwIHBob25lcyAqL1xuICAuc2lkZWJhciB7XG4gICAgdG9wOiA2MHB4O1xuICAgIGxlZnQ6IDBweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogM2VtO1xuICB9XG59XG5AbWVkaWEgKG1heC13aWR0aDogNjAwcHgpIHtcbiAgLyogc21hcnRwaG9uZXMsIGlQaG9uZSwgcG9ydHJhaXQgNDgweDMyMCBwaG9uZXMgKi9cbiAgLnNpZGViYXIge1xuICAgIG1hcmdpbi10b3A6IC0zJTtcbiAgICB0b3A6IDhlbTtcbiAgICBsZWZ0OiAwcHg7XG4gICAgaGVpZ2h0OiA5NyU7XG4gICAgd2lkdGg6IDI1JTtcbiAgICBwYWRkaW5nLWJvdHRvbTogM2VtO1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgfVxuICAuc2lkZWJhciAubGlzdC1ncm91cCA+IGEubGlzdC1ncm91cC1pdGVtIC5mYSB7XG4gICAgZm9udC1zaXplOiA0MHB4O1xuICB9XG4gIC5zaWRlYmFyIC5saXN0LWdyb3VwIGltZyB7XG4gICAgd2lkdGg6IDQwcHg7XG4gIH1cbiAgLnNpZGViYXIgLm5lc3RlZC1tZW51ID4gYS5saXN0LWdyb3VwLWl0ZW0gLmZhIHtcbiAgICBmb250LXNpemU6IDQwcHg7XG4gIH1cbn1cbkBtZWRpYSBwcmludCB7XG4gIC5zaWRlYmFyIHtcbiAgICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XG4gIH1cbn1cbjo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICB3aWR0aDogOHB4O1xufVxuXG46Oi13ZWJraXQtc2Nyb2xsYmFyLXRyYWNrIHtcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiBpbnNldCAwIDAgMHB4IHdoaXRlO1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG59XG5cbjo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG4gIC13ZWJraXQtYm94LXNoYWRvdzogaW5zZXQgMCAwIDNweCB3aGl0ZTtcbn1cblxuLnRvZ2dsZS1idXR0b24ge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHdpZHRoOiAyMzZweDtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBwYWRkaW5nOiAxMnB4O1xuICBib3R0b206IDA7XG4gIGNvbG9yOiAjOTk5O1xuICBiYWNrZ3JvdW5kOiAjMjEyNTI5O1xuICBib3JkZXItdG9wOiAxcHggc29saWQgIzk5OTtcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcbiAgLW1vei10cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcbiAgLW1zLXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xuICAtby10cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XG59XG4udG9nZ2xlLWJ1dHRvbiBpIHtcbiAgZm9udC1zaXplOiAyM3B4O1xufVxuLnRvZ2dsZS1idXR0b246aG92ZXIge1xuICBiYWNrZ3JvdW5kOiAjMjEyNjMxO1xuICBjb2xvcjogIzU2YTRmZjtcbn1cblxuLmNvbGxhcHNlZCB7XG4gIHdpZHRoOiA1MHB4O1xufVxuLmNvbGxhcHNlZCBzcGFuIHtcbiAgZGlzcGxheTogbm9uZTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/components/sidebar/sidebar.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/layout/components/sidebar/sidebar.component.ts ***!
  \****************************************************************/
/*! exports provided: SidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarComponent", function() { return SidebarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _sidebarmenu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sidebarmenu */ "./src/app/layout/components/sidebar/sidebarmenu.ts");
/* harmony import */ var _services_storage_service_module_storage_service_module_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/storage-service-module/storage-service-module.service */ "./src/app/services/storage-service-module/storage-service-module.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SidebarComponent = /** @class */ (function () {
    function SidebarComponent(translate, router, lStorage) {
        // let $secretKey = localStorage.getToken();
        // let encodedStr = JSON.stringify({data:'here'})
        // let AesCrypto = crypto.AES.encrypt(encodedStr, $secretKey);
        var _this = this;
        this.translate = translate;
        this.router = router;
        this.lStorage = lStorage;
        this.isActive = false;
        this.collapsed = false;
        this.showMenu = '';
        this.pushRightClass = 'push-right';
        this.sideBarMenu = _sidebarmenu__WEBPACK_IMPORTED_MODULE_3__["sidebarmenu"];
        this.collapsedEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        // let result = AesCrypto.toString();
        // let decrypt = crypto.AES.decrypt(result, $secretKey );
        // console.log("CRYPTO", result,decrypt.toString(crypto.enc.Utf8)) ;
        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de']);
        this.translate.setDefaultLang('en');
        var browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de/) ? browserLang : 'en');
        this.router.events.subscribe(function (val) {
            if (val instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"] &&
                window.innerWidth <= 2000 &&
                _this.isToggled()) {
                _this.toggleSidebar();
            }
        });
    }
    SidebarComponent.prototype.eventCalled = function () {
        this.isActive = !this.isActive;
    };
    SidebarComponent.prototype.addExpandClass = function (element) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        }
        else {
            this.showMenu = element;
        }
    };
    SidebarComponent.prototype.toggleCollapsed = function () {
        this.collapsed = !this.collapsed;
        this.collapsedEvent.emit(this.collapsed);
    };
    SidebarComponent.prototype.isToggled = function () {
        var dom = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    };
    SidebarComponent.prototype.toggleSidebar = function () {
        var dom = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    };
    SidebarComponent.prototype.rltAndLtr = function () {
        var dom = document.querySelector('body');
        dom.classList.toggle('rtl');
    };
    SidebarComponent.prototype.changeLang = function (language) {
        this.translate.use(language);
    };
    SidebarComponent.prototype.onLoggedout = function () {
        localStorage.removeItem('isLoggedin');
        localStorage.removeItem('tokenlogin');
        localStorage.removeItem('devmode');
    };
    SidebarComponent.prototype.orderhistory = function () {
        // this.router.navigate(["merchant-portal/order-histories"])
    };
    SidebarComponent.ctorParameters = function () { return [
        { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _services_storage_service_module_storage_service_module_service__WEBPACK_IMPORTED_MODULE_4__["LocalStorageService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], SidebarComponent.prototype, "collapsedEvent", void 0);
    SidebarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sidebar',
            template: __webpack_require__(/*! raw-loader!./sidebar.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/components/sidebar/sidebar.component.html"),
            styles: [__webpack_require__(/*! ./sidebar.component.scss */ "./src/app/layout/components/sidebar/sidebar.component.scss")]
        }),
        __metadata("design:paramtypes", [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _services_storage_service_module_storage_service_module_service__WEBPACK_IMPORTED_MODULE_4__["LocalStorageService"]])
    ], SidebarComponent);
    return SidebarComponent;
}());



/***/ }),

/***/ "./src/app/layout/components/sidebar/sidebarmenu.ts":
/*!**********************************************************!*\
  !*** ./src/app/layout/components/sidebar/sidebarmenu.ts ***!
  \**********************************************************/
/*! exports provided: sidebarmenu */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sidebarmenu", function() { return sidebarmenu; });
var sidebarmenu = {
    "Order History": [
        { label: "Summary", routerLink: '/administrator/order-history-summary' },
        { label: "All History", routerLink: '/administrator/orderhistoryallhistoryadmin' },
        { label: "Success", routerLink: '/administrator/orderhistorysuccessadmin' },
        // {label: "Pending", routerLink:'/administrator/orderhistorypendingadmin'},
        { label: "Cancel", routerLink: '/administrator/orderhistorycanceladmin' },
        // {label: "Failed", routerLink:'/administrator/orderhistoryfailedadmin'},
        { label: "Waiting", routerLink: '/administrator/orderhistorywaitingadmin' },
    ],
    "Sales Redemption": [
        { label: "Report", routerLink: '/administrator/salesredemption' },
        { label: "Generate BAST", routerLink: '/administrator/salesredemptionbast' }
    ],
    "Member": [
        // {label: "Summary", routerLink:'/administrator/membersummary'},
        // {label: "All Members", routerLink:'/administrator/allmembersadmin'},
        { label: "Member Point", routerLink: '/administrator/memberadmin' },
        { label: "Data Member", routerLink: '/administrator/datamember' },
    ],
    "Products": [
        // {label: "Upload Product CSV", routerLink:'/administrator/testproductadmin'},
        { label: "Add", routerLink: '/administrator/productadmin/add' },
        { label: "Product", routerLink: '/administrator/productadmin' },
        { label: "Evoucher", routerLink: '/administrator/vouchers' },
    ],
    "Payment": [
        { label: "Payment Gateway", routerLink: '/administrator/paymentgatewayadmin' }
    ],
    "Loyalty": [
        // {label: "Points Model Group", routerLink:'/administrator/pointsgroup'},
        { label: "Points Models Setup", routerLink: '/administrator/pointsmodels' },
    ],
    "Point Movement": [
        { label: "Based on Product", routerLink: '/administrator/pointmovement' },
        { label: "Based on Order", routerLink: '/administrator/pointmovementorder' },
    ],
    "Notification": [
        { label: "Notification Group", routerLink: '/administrator/notificationgroupadmin' },
        { label: "Notification Setting", routerLink: '/administrator/notificationsettingadmin' },
        { label: "Notification Broadcast", routerLink: '/administrator/notificationbroadcastadmin' },
        { label: "Notification Message", routerLink: '/administrator/notificationmessageadmin' },
    ],
    "Evoucher": [
        // {label: "Evoucher Generated", routerLink:'/administrator/evoucheradmin'},
        { label: "Evoucher Sales Report", routerLink: '/administrator/evouchersalesreport' },
        { label: "Generated Voucher List", routerLink: '/administrator/evoucheradmin' },
    ],
    "Report": [
        { label: "Sales Summary report", routerLink: '/administrator/order-history-summary' },
        { label: "Member Demography", routerLink: '/administrator/member-demography-report' },
        { label: "Sales Order", routerLink: '/administrator/salesorder' },
        { label: "Traffic Activity", routerLink: '/administrator/activity-report' },
        { label: "Product Activity", routerLink: '/administrator/product-activity-report' },
        { label: "Report", routerLink: '/administrator/reportadmin' },
        { label: "Report Error", routerLink: '/administrator/reporterror' },
        { label: "CRR", routerLink: '/administrator/crr' },
        { label: "CNR", routerLink: '/administrator/cnr' },
    ],
    "Product": [
        { label: "Product", routerLink: '/administrator/product' },
        { label: "Evoucher", routerLink: '/administrator/evoucher' }
    ]
};


/***/ })

}]);
//# sourceMappingURL=administrator-administrator-module.js.map