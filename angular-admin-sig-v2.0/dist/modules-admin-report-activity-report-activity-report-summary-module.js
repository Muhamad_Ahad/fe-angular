(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-admin-report-activity-report-activity-report-summary-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-report/activity-report/activity-report-summary.component.html":
/*!******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/admin-report/activity-report/activity-report-summary.component.html ***!
  \******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n    <app-page-header [heading]=\"'Traffic Activity Report'\" [icon]=\"'fa-table'\">\r\n    </app-page-header>\r\n    <div class=\"summary-report row\">\r\n        <div class=\"col-md-6\">\r\n            <div class=\"row-per-card\">\r\n                <div class=\"card \" *ngIf=\"analitycsData.eachPlatformToday.barChartLabels.length\">\r\n                    <div class=\"card-header\">\r\n                        Today Platform Activity\r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                        <canvas baseChart [datasets]=\"analitycsData.eachPlatformToday.barChartData\"\r\n                            [labels]=\"analitycsData.eachPlatformToday.barChartLabels\" [options]=\"barChartOptions\"\r\n                            [legend]=\"barChartLegend\" [chartType]=\"'pie'\" (chartHover)=\"chartHovered($event)\"\r\n                            (chartClick)=\"chartClicked($event)\">\r\n                        </canvas>\r\n                    </div>\r\n                    <!-- <div class=\"card-footer\">\r\n                            <button class=\"btn btn-info btn-sm\" (click)=\"onUpdateCart(analitycsData.eachPlatformToday)\">Refresh</button>\r\n                        </div> -->\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n            <div class=\"row-per-card\">\r\n                <div class=\"card \" *ngIf=\"analitycsData.eachPlatformToday.barChartLabels.length\">\r\n                    <div class=\"card-header\">\r\n                        Today Platform Activity\r\n                    </div>\r\n                    <div class=\"card-body emirate-daily\">\r\n                            <div class=\"card-body emirate-daily\">\r\n                                    <h1>Today Active Users : {{data.total_member}}</h1>\r\n                                </div>\r\n                        <table>\r\n                            <tr>\r\n                                <th>Platform</th>\r\n                                <th>Users amount</th>\r\n                            </tr>\r\n                            <tr *ngFor=\"let dt of analitycsData.eachPlatformToday.barChartLabels;let i=index;\">\r\n                                <td>{{dt}}</td>\r\n                                <td>{{data.user_active_each_platform_today[dt]}}</td>\r\n\r\n                            </tr>\r\n                        </table>\r\n                    </div>\r\n                    <!-- <div class=\"card-footer\">\r\n                                        <button class=\"btn btn-info btn-sm\" (click)=\"onUpdateCart(analitycsData.daily)\">Refresh</button>\r\n                                    </div> -->\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n        \r\n        <div class=\"col-md-6\">\r\n            <div class=\"row-per-card\">\r\n                <div class=\"card \" *ngIf=\"analitycsData.eachPlatformToday.barChartLabels.length\">\r\n                    \r\n                    <div class=\"card-header\">\r\n                        Today Pages Activity\r\n                    </div>\r\n                    <div class=\"card-body emirate-daily\">\r\n                        <table>\r\n                            <tr>\r\n                                <th>Pages</th>\r\n                                <th>Views</th>\r\n                            </tr>\r\n                            <tr *ngFor=\"let dt of analitycsData.eachPageToday.barChartLabels;let i=index;\">\r\n                                <td>{{dt}}</td>\r\n                                <td>{{data.user_active_each_page_today[dt]}}</td>\r\n\r\n                            </tr>\r\n                        </table>\r\n                    </div>\r\n                    <!-- <div class=\"card-footer\">\r\n                                        <button class=\"btn btn-info btn-sm\" (click)=\"onUpdateCart(analitycsData.daily)\">Refresh</button>\r\n                                    </div> -->\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n        \r\n    </div>\r\n\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/layout/modules/admin-report/activity-report/activity-report-summary-routing.module.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-report/activity-report/activity-report-summary-routing.module.ts ***!
  \*******************************************************************************************************/
/*! exports provided: ActivityReportSummaryRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActivityReportSummaryRoutingModule", function() { return ActivityReportSummaryRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _activity_report_summary_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./activity-report-summary.component */ "./src/app/layout/modules/admin-report/activity-report/activity-report-summary.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '', component: _activity_report_summary_component__WEBPACK_IMPORTED_MODULE_2__["ActivityReportSummaryComponent"],
    },
];
var ActivityReportSummaryRoutingModule = /** @class */ (function () {
    function ActivityReportSummaryRoutingModule() {
    }
    ActivityReportSummaryRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ActivityReportSummaryRoutingModule);
    return ActivityReportSummaryRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-report/activity-report/activity-report-summary.component.scss":
/*!****************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-report/activity-report/activity-report-summary.component.scss ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".summary-report .dark-header {\n  background: #34495e;\n  font-weight: bold;\n  color: white;\n}\n.summary-report .card-body.emirate-daily {\n  max-height: 340px;\n  overflow: auto;\n}\n.summary-report .row-per-card {\n  margin-bottom: 20px;\n}\n.summary-report table {\n  border-collapse: collapse;\n  border: 1px solid #ccc;\n  width: 100%;\n  text-align: center;\n}\n.summary-report table th {\n  background: #333;\n  color: white;\n  font-size: 12px;\n  font-weight: normal;\n}\n.summary-report table tr:nth-child(odd) td {\n  background-color: #f0f0f0;\n}\n.summary-report table td, .summary-report table th {\n  border: 1px solid #ccc;\n  padding: 4px 15px;\n}\n.summary-report table td {\n  font-size: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tcmVwb3J0L2FjdGl2aXR5LXJlcG9ydC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXGFkbWluLXJlcG9ydFxcYWN0aXZpdHktcmVwb3J0XFxhY3Rpdml0eS1yZXBvcnQtc3VtbWFyeS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tcmVwb3J0L2FjdGl2aXR5LXJlcG9ydC9hY3Rpdml0eS1yZXBvcnQtc3VtbWFyeS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0FDQVI7QURHSTtFQUNJLGlCQUFBO0VBQ0EsY0FBQTtBQ0RSO0FER0k7RUFDSSxtQkFBQTtBQ0RSO0FER0k7RUFDSSx5QkFBQTtFQUNBLHNCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FDRFI7QURFUTtFQUNJLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtBQ0FaO0FERVE7RUFDSSx5QkFBQTtBQ0FaO0FERVE7RUFDSSxzQkFBQTtFQUNBLGlCQUFBO0FDQVo7QURFUTtFQUNJLGVBQUE7QUNBWiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2FkbWluLXJlcG9ydC9hY3Rpdml0eS1yZXBvcnQvYWN0aXZpdHktcmVwb3J0LXN1bW1hcnkuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc3VtbWFyeS1yZXBvcnR7XHJcbiAgICAuZGFyay1oZWFkZXJ7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzM0NDk1ZTtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5jYXJkLWJvZHkuZW1pcmF0ZS1kYWlseXtcclxuICAgICAgICBtYXgtaGVpZ2h0OiAzNDBweDtcclxuICAgICAgICBvdmVyZmxvdzogYXV0bztcclxuICAgIH1cclxuICAgIC5yb3ctcGVyLWNhcmR7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICAgIH1cclxuICAgIHRhYmxle1xyXG4gICAgICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgdGh7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICMzMzM7XHJcbiAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0cjpudGgtY2hpbGQob2RkKSB0ZHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGQsIHRoe1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiA0cHggMTVweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGR7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxufSIsIi5zdW1tYXJ5LXJlcG9ydCAuZGFyay1oZWFkZXIge1xuICBiYWNrZ3JvdW5kOiAjMzQ0OTVlO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6IHdoaXRlO1xufVxuLnN1bW1hcnktcmVwb3J0IC5jYXJkLWJvZHkuZW1pcmF0ZS1kYWlseSB7XG4gIG1heC1oZWlnaHQ6IDM0MHB4O1xuICBvdmVyZmxvdzogYXV0bztcbn1cbi5zdW1tYXJ5LXJlcG9ydCAucm93LXBlci1jYXJkIHtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cbi5zdW1tYXJ5LXJlcG9ydCB0YWJsZSB7XG4gIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIHdpZHRoOiAxMDAlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uc3VtbWFyeS1yZXBvcnQgdGFibGUgdGgge1xuICBiYWNrZ3JvdW5kOiAjMzMzO1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbn1cbi5zdW1tYXJ5LXJlcG9ydCB0YWJsZSB0cjpudGgtY2hpbGQob2RkKSB0ZCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XG59XG4uc3VtbWFyeS1yZXBvcnQgdGFibGUgdGQsIC5zdW1tYXJ5LXJlcG9ydCB0YWJsZSB0aCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIHBhZGRpbmc6IDRweCAxNXB4O1xufVxuLnN1bW1hcnktcmVwb3J0IHRhYmxlIHRkIHtcbiAgZm9udC1zaXplOiAxMnB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/admin-report/activity-report/activity-report-summary.component.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-report/activity-report/activity-report-summary.component.ts ***!
  \**************************************************************************************************/
/*! exports provided: ActivityReportSummaryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActivityReportSummaryComponent", function() { return ActivityReportSummaryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _services_activity_activity_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/activity/activity.service */ "./src/app/services/activity/activity.service.ts");
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var ActivityReportSummaryComponent = /** @class */ (function () {
    function ActivityReportSummaryComponent(activityReport, sanitizer, activityService) {
        this.activityReport = activityReport;
        this.sanitizer = sanitizer;
        this.activityService = activityService;
        this.data = [];
        this.barChartOptions = {
            scaleShowVerticalLines: false,
            responsive: true,
            scales: {
                yAxes: [{
                        ticks: {
                            callback: this.currencyFormatter,
                        }
                    }]
            },
        };
        this.barChartLabels = [];
        this.barChartType = 'bar';
        this.barChartLegend = true;
        this.barChartData = [
            { data: [], label: 'user active each platform today' },
        ];
        this.optO = { fill: false, borderWidth: 1, };
        this.analitycsData = {
            eachPlatformToday: {
                barChartData: [__assign({ data: [], label: 'each platform' }, this.optO)],
                barChartLabels: []
            },
            eachPageToday: {
                barChartData: [__assign({ data: [], label: 'each page' }, this.optO)],
                barChartLabels: []
            },
        };
        this.service = activityReport;
        this.activityService.addLog({ page: this.title });
    }
    ActivityReportSummaryComponent.prototype.onUpdateCart = function (data) {
        var clonedData = JSON.parse(JSON.stringify(data.barChartData));
        var clone = JSON.parse(JSON.stringify(this.barChartData));
        clone[0].data = [];
        data.barChartLabels.forEach(function () { clone[0].data.push(Math.round(Math.random() * 100)); });
        data.barChartData = clone;
        setTimeout(function () {
            data.barChartData = clonedData;
        }, 500);
    };
    ActivityReportSummaryComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var currentDate;
            return __generator(this, function (_a) {
                currentDate = new Date();
                console.log(this.barChartLabels);
                this.firstLoad();
                return [2 /*return*/];
            });
        });
    };
    ActivityReportSummaryComponent.prototype.generateChartData = function (dataValues) {
        var barChartLabels = [];
        var newData = [];
        var allData;
        allData = dataValues;
        var total = 0;
        for (var data in allData) {
            // console.log('allData data', allData[data])
            barChartLabels.push(data);
            total += parseInt(allData[data]);
            newData.push(allData[data]);
        }
        allData = null; //clearing memory
        // console.log("new Data", newData)
        return [barChartLabels, newData, total];
    };
    ActivityReportSummaryComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, arEachPlatformToday, clone, arEachPageToday, clone, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.activityReport;
                        return [4 /*yield*/, this.activityReport.getActivityReport()];
                    case 1:
                        result = _a.sent();
                        this.data = result.result;
                        if (this.data.user_active_each_platform_today) {
                            arEachPlatformToday = this.generateChartData(this.data.user_active_each_platform_today);
                            clone = JSON.parse(JSON.stringify(this.analitycsData.eachPlatformToday.barChartData));
                            clone[0].data = arEachPlatformToday[1];
                            this.analitycsData.eachPlatformToday.barChartLabels = arEachPlatformToday[0];
                            this.barChartLabels = arEachPlatformToday[0];
                            this.analitycsData.eachPlatformToday.barChartData = clone;
                        }
                        if (this.data.user_active_each_page_today) {
                            arEachPageToday = this.generateChartData(this.data.user_active_each_page_today);
                            clone = JSON.parse(JSON.stringify(this.analitycsData.eachPageToday.barChartData));
                            clone[0].data = arEachPageToday[1];
                            this.analitycsData.eachPageToday.barChartLabels = arEachPageToday[0];
                            this.barChartLabels = arEachPageToday[0];
                            this.analitycsData.eachPageToday.barChartData = clone;
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.log("this e result", e_1);
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ActivityReportSummaryComponent.prototype.calculateTheAverageValue = function (_value) {
        var howMany = Object.keys(_value).length;
        var sum = 0;
        Reflect.ownKeys(_value).forEach(function (key) {
            sum = sum + _value[key];
        });
        var avg = sum / howMany;
        return avg;
    };
    ActivityReportSummaryComponent.prototype.currencyFormatter = function (value, index, values) {
        // add comma as thousand separator
        return value + '%';
    };
    ActivityReportSummaryComponent.ctorParameters = function () { return [
        { type: _services_activity_activity_service__WEBPACK_IMPORTED_MODULE_3__["ActivityService"] },
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"] },
        { type: _services_activity_activity_service__WEBPACK_IMPORTED_MODULE_3__["ActivityService"] }
    ]; };
    ActivityReportSummaryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-activity-report-summary',
            template: __webpack_require__(/*! raw-loader!./activity-report-summary.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-report/activity-report/activity-report-summary.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./activity-report-summary.component.scss */ "./src/app/layout/modules/admin-report/activity-report/activity-report-summary.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_activity_activity_service__WEBPACK_IMPORTED_MODULE_3__["ActivityService"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"],
            _services_activity_activity_service__WEBPACK_IMPORTED_MODULE_3__["ActivityService"]])
    ], ActivityReportSummaryComponent);
    return ActivityReportSummaryComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-report/activity-report/activity-report-summary.module.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-report/activity-report/activity-report-summary.module.ts ***!
  \***********************************************************************************************/
/*! exports provided: ActivityReportSummaryModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActivityReportSummaryModule", function() { return ActivityReportSummaryModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _activity_report_summary_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./activity-report-summary.component */ "./src/app/layout/modules/admin-report/activity-report/activity-report-summary.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _activity_report_summary_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./activity-report-summary-routing.module */ "./src/app/layout/modules/admin-report/activity-report/activity-report-summary-routing.module.ts");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/index.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(ng2_charts__WEBPACK_IMPORTED_MODULE_9__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var ActivityReportSummaryModule = /** @class */ (function () {
    function ActivityReportSummaryModule() {
    }
    ActivityReportSummaryModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _activity_report_summary_routing_module__WEBPACK_IMPORTED_MODULE_7__["ActivityReportSummaryRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_3__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__["BsComponentModule"],
                ng2_charts__WEBPACK_IMPORTED_MODULE_9__["ChartsModule"]
            ],
            declarations: [
                _activity_report_summary_component__WEBPACK_IMPORTED_MODULE_2__["ActivityReportSummaryComponent"]
            ]
        })
    ], ActivityReportSummaryModule);
    return ActivityReportSummaryModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-admin-report-activity-report-activity-report-summary-module.js.map