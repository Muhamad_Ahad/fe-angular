(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-admin-report-cartabandonment-cartabandonmentsummary-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-report/cartabandonment/CartAbandonmentsummary.component.html":
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/admin-report/cartabandonment/CartAbandonmentsummary.component.html ***!
  \*****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n    <app-page-header [heading]=\"'Shopping cart (order) Abandonment Rate'\" [icon]=\"'fa-table'\" [topBarMenu]=\"topBarMenu\">\r\n    </app-page-header>\r\n    <div class=\"summary-report row\">\r\n        <div class=\"col-md-6\">\r\n            <div class=\"row-per-card\">\r\n                <div class=\"card \" *ngIf=\"analitycsData.daily.barChartLabels.length\">\r\n                    <div class=\"card-header\">\r\n                        Daily Rate\r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                        <canvas baseChart [datasets]=\"analitycsData.daily.barChartData\"\r\n                            [labels]=\"analitycsData.daily.barChartLabels\" [options]=\"barChartOptions\"\r\n                            [legend]=\"barChartLegend\" [chartType]=\"'line'\" (chartHover)=\"chartHovered($event)\"\r\n                            (chartClick)=\"chartClicked($event)\">\r\n                        </canvas>\r\n                    </div>\r\n                    <!-- <div class=\"card-footer\">\r\n                            <button class=\"btn btn-info btn-sm\" (click)=\"onUpdateCart(analitycsData.daily)\">Refresh</button>\r\n                        </div> -->\r\n                </div>\r\n            </div>\r\n\r\n\r\n\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n            <div class=\"row-per-card\">\r\n                <div class=\"card \" *ngIf=\"analitycsData.daily.barChartLabels.length\">\r\n                    <div class=\"card-header\">\r\n                        Daily Abandonment Rate\r\n                    </div>\r\n                    <div class=\"card-body emirate-daily\">\r\n                        <table>\r\n                            <tr>\r\n                                <th>Date</th>\r\n                                <th>Order Value</th>\r\n                                <th>Paid Value</th>\r\n                                <th>Rate</th>\r\n                            </tr>\r\n                            <tr *ngFor=\"let dt of analitycsData.daily.barChartLabels;let i=index;\">\r\n                                <td>{{dt}}</td>\r\n                                <td>{{data.order_history_by_the_day.value[dt]}}</td>\r\n                                <td>{{data.order_history_by_the_day_paid.value[dt]}}</td>\r\n                                <td>{{analitycsData.daily.barChartData[0].data[i] }}%</td>\r\n\r\n                            </tr>\r\n                        </table>\r\n                    </div>\r\n                    <!-- <div class=\"card-footer\">\r\n                                        <button class=\"btn btn-info btn-sm\" (click)=\"onUpdateCart(analitycsData.daily)\">Refresh</button>\r\n                                    </div> -->\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n\r\n        <div class=\"col-md-6\">\r\n            <div class=\"row-per-card\" style=\"margin-bottom: 20px; text-align: center\">\r\n                <div class=\"card \" *ngIf=\"analitycsData.monthly.barChartLabels.length\">\r\n                    <div class=\"card-header\">\r\n                        monthly Abandonment Rate\r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                        <canvas baseChart [datasets]=\"analitycsData.monthly.barChartData\"\r\n                            [labels]=\"analitycsData.monthly.barChartLabels\" [options]=\"barChartOptions\"\r\n                            [legend]=\"barChartLegend\" [chartType]=\"'line'\" (chartHover)=\"chartHovered($event)\"\r\n                            (chartClick)=\"chartClicked($event)\">\r\n                        </canvas>\r\n                    </div>\r\n                    <!-- <div class=\"card-footer\">\r\n                                          <button class=\"btn btn-info btn-sm\" (click)=\"onUpdateCart(analitycsData.monthly)\">Refresh</button>\r\n                                      </div> -->\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n                <div class=\"row-per-card\">\r\n                    <div class=\"card \" *ngIf=\"analitycsData.monthly.barChartLabels.length\">\r\n                        <div class=\"card-header\">\r\n                            Monthly Abandonment Rate\r\n                        </div>\r\n                        <div class=\"card-body emirate-daily\">\r\n                            <table>\r\n                                <tr>\r\n                                    <th>Date</th>\r\n                                    <th>Order Value</th>\r\n                                    <th>Paid Value</th>\r\n                                    <th>Rate</th>\r\n                                </tr>\r\n                                <tr *ngFor=\"let dt of analitycsData.monthly.barChartLabels;let i=index;\">\r\n                                    <td>{{dt}}</td>\r\n                                    <td>{{data.order_history_by_month.value[dt]}}</td>\r\n                                    <td>{{data.order_history_by_month_paid.value[dt]}}</td>\r\n                                    <td>{{analitycsData.monthly.barChartData[0].data[i] }}%</td>\r\n    \r\n                                </tr>\r\n                            </table>\r\n                        </div>\r\n                        <!-- <div class=\"card-footer\">\r\n                                            <button class=\"btn btn-info btn-sm\" (click)=\"onUpdateCart(analitycsData.daily)\">Refresh</button>\r\n                                        </div> -->\r\n                    </div>\r\n                </div>\r\n    \r\n            </div>\r\n    \r\n        <div class=\"col-md-6\">\r\n            <div class=\"row-per-card\">\r\n                <div class=\"card \" *ngIf=\"analitycsData.yearly.barChartLabels\">\r\n                    <div class=\"card-header\">\r\n                        Yearly Rate\r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                        <canvas baseChart [datasets]=\"analitycsData.yearly.barChartData\"\r\n                            [labels]=\"analitycsData.yearly.barChartLabels\" [options]=\"barChartOptions\"\r\n                            [legend]=\"barChartLegend\" [chartType]=\"'bar'\" (chartHover)=\"chartHovered($event)\"\r\n                            (chartClick)=\"chartClicked($event)\">\r\n                        </canvas>\r\n                    </div>\r\n                    <!-- <div class=\"card-footer\">\r\n                                                    <button class=\"btn btn-info btn-sm\" (click)=\"onUpdateCart(analitycsData.hourly)\">Refresh</button>\r\n                                                </div> -->\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n                <div class=\"row-per-card\">\r\n                    <div class=\"card \" *ngIf=\"analitycsData.yearly.barChartLabels.length\">\r\n                        <div class=\"card-header\">\r\n                            Yearly Abandonment Rate\r\n                        </div>\r\n                        <div class=\"card-body emirate-daily\">\r\n                            <table>\r\n                                <tr>\r\n                                    <th>Date</th>\r\n                                    <th>Order Value</th>\r\n                                    <th>Paid Value</th>\r\n                                    <th>Rate</th>\r\n                                </tr>\r\n                                <tr *ngFor=\"let dt of analitycsData.yearly.barChartLabels;let i=index;\">\r\n                                    <td>{{dt}}</td>\r\n                                    <td>{{data.order_history_yearly.value[dt]}}</td>\r\n                                    <td>{{data.order_history_yearly_paid.value[dt]}}</td>\r\n                                    <td>{{analitycsData.yearly.barChartData[0].data[i] }}%</td>\r\n    \r\n                                </tr>\r\n                            </table>\r\n                        </div>\r\n                        <!-- <div class=\"card-footer\">\r\n                                            <button class=\"btn btn-info btn-sm\" (click)=\"onUpdateCart(analitycsData.daily)\">Refresh</button>\r\n                                        </div> -->\r\n                    </div>\r\n                </div>\r\n    \r\n            </div>\r\n        <div class=\"col-md-6\">\r\n\r\n            <div class=\"row\" style=\"margin-bottom: 20px; text-align: center\">\r\n                <div class=\"col-md-12\">\r\n                    <div class=\"row-per-card\">\r\n                        <div class=\"card \" *ngIf=\"analitycsData.hourly.barChartLabels\">\r\n                            <div class=\"card-header\">\r\n                                Hourly Abandonment Rate\r\n                            </div>\r\n                            <div class=\"card-body\">\r\n                                <canvas baseChart [datasets]=\"analitycsData.hourly.barChartData\"\r\n                                    [labels]=\"analitycsData.hourly.barChartLabels\" [options]=\"barChartOptions\"\r\n                                    [legend]=\"barChartLegend\" [chartType]=\"'line'\" (chartHover)=\"chartHovered($event)\"\r\n                                    (chartClick)=\"chartClicked($event)\">\r\n                                </canvas>\r\n                            </div>\r\n                            <!-- <div class=\"card-footer\">\r\n                                                        <button class=\"btn btn-info btn-sm\" (click)=\"onUpdateCart(analitycsData.hourly)\">Refresh</button>\r\n                                                    </div> -->\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"col-md-6\">\r\n                <div class=\"row-per-card\">\r\n                    <div class=\"card \" *ngIf=\"analitycsData.hourly.barChartLabels.length\">\r\n                        <div class=\"card-header\">\r\n                            Hourly Abandonment Rate\r\n                        </div>\r\n                        <div class=\"card-body emirate-daily\">\r\n                            <table>\r\n                                <tr>\r\n                                    <th>Date</th>\r\n                                    <th>Order Value</th>\r\n                                    <th>Paid Value</th>\r\n                                    <th>Rate</th>\r\n                                </tr>\r\n                                <tr *ngFor=\"let dt of analitycsData.hourly.barChartLabels;let i=index;\">\r\n                                    <td>{{dt}}</td>\r\n                                    <td>{{data.order_history_hourly.value[dt]}}</td>\r\n                                    <td>{{data.order_history_hourly_paid.value[dt]}}</td>\r\n                                    <td>{{analitycsData.hourly.barChartData[0].data[i] }}%</td>\r\n    \r\n                                </tr>\r\n                            </table>\r\n                        </div>\r\n                        <!-- <div class=\"card-footer\">\r\n                                            <button class=\"btn btn-info btn-sm\" (click)=\"onUpdateCart(analitycsData.daily)\">Refresh</button>\r\n                                        </div> -->\r\n                    </div>\r\n                </div>\r\n    \r\n            </div>\r\n\r\n\r\n\r\n\r\n    </div>\r\n\r\n\r\n\r\n\r\n</div>"

/***/ }),

/***/ "./src/app/layout/modules/admin-report/cartabandonment/CartAbandonmentsummary.component.scss":
/*!***************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-report/cartabandonment/CartAbandonmentsummary.component.scss ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".summary-report .dark-header {\n  background: #34495e;\n  font-weight: bold;\n  color: white;\n}\n.summary-report .card-body.emirate-daily {\n  max-height: 340px;\n  overflow: auto;\n}\n.summary-report .row-per-card {\n  margin-bottom: 20px;\n}\n.summary-report table {\n  border-collapse: collapse;\n  border: 1px solid #ccc;\n  width: 100%;\n  text-align: center;\n}\n.summary-report table th {\n  font-size: 12px;\n  font-weight: normal;\n}\n.summary-report table tr:nth-child(odd) td {\n  background-color: #f0f0f0;\n}\n.summary-report table td, .summary-report table th {\n  border: 1px solid #ccc;\n  padding: 4px 15px;\n}\n.summary-report table td {\n  font-size: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tcmVwb3J0L2NhcnRhYmFuZG9ubWVudC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXGFkbWluLXJlcG9ydFxcY2FydGFiYW5kb25tZW50XFxDYXJ0QWJhbmRvbm1lbnRzdW1tYXJ5LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9hZG1pbi1yZXBvcnQvY2FydGFiYW5kb25tZW50L0NhcnRBYmFuZG9ubWVudHN1bW1hcnkuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtBQ0FSO0FER0k7RUFDSSxpQkFBQTtFQUNBLGNBQUE7QUNEUjtBREdJO0VBQ0ksbUJBQUE7QUNEUjtBREdJO0VBQ0kseUJBQUE7RUFDQSxzQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQ0RSO0FERVE7RUFHSSxlQUFBO0VBQ0EsbUJBQUE7QUNGWjtBRElRO0VBQ0kseUJBQUE7QUNGWjtBRElRO0VBQ0ksc0JBQUE7RUFDQSxpQkFBQTtBQ0ZaO0FESVE7RUFDSSxlQUFBO0FDRloiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9hZG1pbi1yZXBvcnQvY2FydGFiYW5kb25tZW50L0NhcnRBYmFuZG9ubWVudHN1bW1hcnkuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc3VtbWFyeS1yZXBvcnR7XHJcbiAgICAuZGFyay1oZWFkZXJ7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzM0NDk1ZTtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5jYXJkLWJvZHkuZW1pcmF0ZS1kYWlseXtcclxuICAgICAgICBtYXgtaGVpZ2h0OiAzNDBweDtcclxuICAgICAgICBvdmVyZmxvdzogYXV0bztcclxuICAgIH1cclxuICAgIC5yb3ctcGVyLWNhcmR7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICAgIH1cclxuICAgIHRhYmxle1xyXG4gICAgICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgdGh7XHJcbiAgICAgICAgICAgIC8vIGJhY2tncm91bmQ6ICMzMzM7XHJcbiAgICAgICAgICAgIC8vIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0cjpudGgtY2hpbGQob2RkKSB0ZHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGQsIHRoe1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiA0cHggMTVweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGR7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxufSIsIi5zdW1tYXJ5LXJlcG9ydCAuZGFyay1oZWFkZXIge1xuICBiYWNrZ3JvdW5kOiAjMzQ0OTVlO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6IHdoaXRlO1xufVxuLnN1bW1hcnktcmVwb3J0IC5jYXJkLWJvZHkuZW1pcmF0ZS1kYWlseSB7XG4gIG1heC1oZWlnaHQ6IDM0MHB4O1xuICBvdmVyZmxvdzogYXV0bztcbn1cbi5zdW1tYXJ5LXJlcG9ydCAucm93LXBlci1jYXJkIHtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cbi5zdW1tYXJ5LXJlcG9ydCB0YWJsZSB7XG4gIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIHdpZHRoOiAxMDAlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uc3VtbWFyeS1yZXBvcnQgdGFibGUgdGgge1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG59XG4uc3VtbWFyeS1yZXBvcnQgdGFibGUgdHI6bnRoLWNoaWxkKG9kZCkgdGQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmMGYwO1xufVxuLnN1bW1hcnktcmVwb3J0IHRhYmxlIHRkLCAuc3VtbWFyeS1yZXBvcnQgdGFibGUgdGgge1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBwYWRkaW5nOiA0cHggMTVweDtcbn1cbi5zdW1tYXJ5LXJlcG9ydCB0YWJsZSB0ZCB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/admin-report/cartabandonment/cartabandonmentsummary-routing.module.ts":
/*!******************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-report/cartabandonment/cartabandonmentsummary-routing.module.ts ***!
  \******************************************************************************************************/
/*! exports provided: CartAbandonmentSummaryRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartAbandonmentSummaryRoutingModule", function() { return CartAbandonmentSummaryRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _cartabandonmentsummary_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./cartabandonmentsummary.component */ "./src/app/layout/modules/admin-report/cartabandonment/cartabandonmentsummary.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '', component: _cartabandonmentsummary_component__WEBPACK_IMPORTED_MODULE_2__["CartAbandonmentSummaryComponent"],
    },
];
var CartAbandonmentSummaryRoutingModule = /** @class */ (function () {
    function CartAbandonmentSummaryRoutingModule() {
    }
    CartAbandonmentSummaryRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], CartAbandonmentSummaryRoutingModule);
    return CartAbandonmentSummaryRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-report/cartabandonment/cartabandonmentsummary.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-report/cartabandonment/cartabandonmentsummary.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: CartAbandonmentSummaryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartAbandonmentSummaryComponent", function() { return CartAbandonmentSummaryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _services_activity_activity_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/activity/activity.service */ "./src/app/services/activity/activity.service.ts");
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var CartAbandonmentSummaryComponent = /** @class */ (function () {
    function CartAbandonmentSummaryComponent(orderhistoryService, sanitizer, activityService) {
        this.orderhistoryService = orderhistoryService;
        this.sanitizer = sanitizer;
        this.activityService = activityService;
        this.data = [];
        this.topBarMenu = [
            { label: "Number of Sales", routerLink: '/administrator/order-history-summary' },
            { label: "Amount of Sales", routerLink: '/administrator/order-amount-history-summary' },
            { label: "Abondonment Rate", routerLink: '/administrator/cart-abandonment-summary', active: true }
        ];
        this.avgData = {
            monthly: {
                all: 0,
                pending: 0,
                paid: 0,
                checkout: 0,
                waiting: 0,
                cancel: 0
            },
            daily: {
                all: 0,
                pending: 0,
                paid: 0,
                checkout: 0,
                waiting: 0,
                cancel: 0
            },
            yearly: {
                all: 0,
                pending: 0,
                paid: 0,
                checkout: 0,
                waiting: 0,
                cancel: 0
            },
            hourly: {
                all: 0, pending: 0, paid: 0, checkout: 0, waiting: 0, cancel: 0
            }
        };
        this.barChartOptions = {
            scaleShowVerticalLines: false,
            responsive: true,
            scales: {
                yAxes: [{
                        ticks: {
                            callback: this.currencyFormatter,
                        }
                    }]
            },
        };
        this.barChartLabels = [];
        this.barChartType = 'bar';
        this.barChartLegend = true;
        this.barChartData = [
            { data: [], label: 'Abandonment Rate' },
        ];
        this.optO = { fill: false, borderWidth: 1, };
        this.analitycsData = {
            monthly: {
                barChartData: [__assign({ data: [], label: 'Abandonment Rate' }, this.optO)],
                barChartLabels: []
            },
            daily: {
                barChartData: [__assign({ data: [], label: 'Abandonment Rate' }, this.optO)],
                barChartLabels: []
            },
            hourly: {
                barChartData: [__assign({ data: [], label: 'Abandonment Rate' }, this.optO)],
                barChartLabels: []
            },
            yearly: {
                barChartData: [__assign({ data: [], label: 'Abandonment Rate' }, this.optO)],
                barChartLabels: []
            }
        };
        this.service = orderhistoryService;
        this.activityService.addLog({ page: this.title });
    }
    CartAbandonmentSummaryComponent.prototype.onUpdateCart = function (data) {
        var clonedData = JSON.parse(JSON.stringify(data.barChartData));
        var clone = JSON.parse(JSON.stringify(this.barChartData));
        clone[0].data = [];
        data.barChartLabels.forEach(function () { clone[0].data.push(Math.round(Math.random() * 100)); });
        data.barChartData = clone;
        setTimeout(function () {
            data.barChartData = clonedData;
        }, 500);
    };
    CartAbandonmentSummaryComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var currentDate;
            return __generator(this, function (_a) {
                currentDate = new Date();
                console.log(this.barChartLabels);
                this.firstLoad();
                return [2 /*return*/];
            });
        });
    };
    CartAbandonmentSummaryComponent.prototype.generateAbandonmentChartData = function (PaidTransaction, AllOrder) {
        var barChartLabels = [];
        var newData = [];
        var total = 0;
        // console.log(PaidTransaction);
        // PaidTransaction.forEach((val , i) => {
        //   console.log("VAL I", val, i);
        // });
        for (var data in PaidTransaction) {
            var rate = !AllOrder[data] || AllOrder[data] <= 0 ? 0 : ((1 - (PaidTransaction[data] / AllOrder[data])) * 100);
            if (rate == 0) {
                continue;
            }
            var floatRate = parseFloat(rate.toFixed(2));
            barChartLabels.push(data);
            total += floatRate;
            newData.push(floatRate);
        }
        console.log("new Data", newData);
        return [barChartLabels, newData, total];
    };
    CartAbandonmentSummaryComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, cvrtdDataMonthly, clone, cvrtdDataDaily, clone, cvrtdDataHourly, clone, cvrtdDataYearly, clone, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.orderhistoryService;
                        return [4 /*yield*/, this.orderhistoryService.getOrderHistorySummaryByDate('today')];
                    case 1:
                        result = _a.sent();
                        this.data = result.result;
                        this.data.shopping_cart_abandonment_rate = this.data.current_order_history.total == 0 ? 0 :
                            (1 - (this.data.current_order_history_paid.total / this.data.current_order_history.total)) * 100;
                        if (this.data.order_history_by_month) {
                            cvrtdDataMonthly = this.generateAbandonmentChartData(this.data.order_history_by_month_paid.value, this.data.order_history_by_month.value);
                            this.avgData.monthly.all = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_month.value));
                            this.avgData.monthly.paid = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_month_paid.value));
                            clone = JSON.parse(JSON.stringify(this.analitycsData.monthly.barChartData));
                            clone[0].data = cvrtdDataMonthly[1];
                            console.log("cvrtdDataMonthly clone", clone);
                            this.barChartLabels = cvrtdDataMonthly[0];
                            this.analitycsData.monthly.barChartLabels = cvrtdDataMonthly[0];
                            // this.barChartLabels = cvrtdDataMonthly[0]
                            // this.analitycsData.monthly.barChartLabels = cvrtdDataMonthly[0]
                            // this.barChartData   = clone;
                            this.analitycsData.monthly.barChartData = clone;
                        }
                        if (this.data.order_history_by_the_day) {
                            console.log("cvrtdDataDaily");
                            cvrtdDataDaily = this.generateAbandonmentChartData(this.data.order_history_by_the_day_paid.value, this.data.order_history_by_the_day.value);
                            this.avgData.daily.all = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_the_day.value));
                            this.avgData.daily.paid = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_the_day_paid.value));
                            clone = JSON.parse(JSON.stringify(this.analitycsData.daily.barChartData));
                            clone[0].data = cvrtdDataDaily[1];
                            this.analitycsData.daily.barChartLabels = cvrtdDataDaily[0];
                            this.barChartLabels = cvrtdDataDaily[0];
                            // this.barChartData   = clone;
                            this.analitycsData.daily.barChartData = clone;
                        }
                        if (this.data.order_history_hourly) {
                            cvrtdDataHourly = this.generateAbandonmentChartData(this.data.order_history_hourly_paid.value, this.data.order_history_hourly.value);
                            this.avgData.hourly.all = Math.floor(this.calculateTheAverageValue(this.data.order_history_hourly.value));
                            this.avgData.hourly.paid = Math.floor(this.calculateTheAverageValue(this.data.order_history_hourly_paid.value));
                            clone = this.analitycsData.hourly.barChartData;
                            SETUP_DATA_FOR_HOURLY: {
                                clone[0].data = cvrtdDataHourly[1];
                            }
                            this.analitycsData.hourly.barChartLabels = cvrtdDataHourly[0];
                            this.barChartLabels = cvrtdDataHourly[0];
                            // this.barChartData   = clone;
                            this.analitycsData.hourly.barChartData = clone;
                        }
                        if (this.data.order_history_yearly) {
                            cvrtdDataYearly = this.generateAbandonmentChartData(this.data.order_history_yearly_paid.value, this.data.order_history_yearly.value);
                            this.avgData.yearly.all = Math.floor(this.calculateTheAverageValue(this.data.order_history_yearly.value));
                            this.avgData.yearly.paid = Math.floor(this.calculateTheAverageValue(this.data.order_history_yearly_paid.value));
                            clone = this.analitycsData.yearly.barChartData;
                            SETUP_DATA_FOR_HOURLY: {
                                clone[0].data = cvrtdDataYearly[1];
                            }
                            this.analitycsData.yearly.barChartLabels = cvrtdDataYearly[0];
                            this.barChartLabels = cvrtdDataYearly[0];
                            this.analitycsData.yearly.barChartData = clone;
                            console.log("his.analitycsData yearly", this.analitycsData.yearly);
                            TOP_MEMBERS_SETUP: {
                                this.topMembers = this.data.top_users.value;
                            }
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.log("this e result", e_1);
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    CartAbandonmentSummaryComponent.prototype.calculateTheAverageValue = function (_value) {
        var howMany = Object.keys(_value).length;
        var sum = 0;
        Reflect.ownKeys(_value).forEach(function (key) {
            sum = sum + _value[key];
        });
        var avg = sum / howMany;
        return avg;
    };
    CartAbandonmentSummaryComponent.prototype.currencyFormatter = function (value, index, values) {
        // add comma as thousand separator
        return value + '%';
    };
    CartAbandonmentSummaryComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_3__["OrderhistoryService"] },
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"] },
        { type: _services_activity_activity_service__WEBPACK_IMPORTED_MODULE_4__["ActivityService"] }
    ]; };
    CartAbandonmentSummaryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-CartAbandonmentsummary-CartAbandonment',
            template: __webpack_require__(/*! raw-loader!./CartAbandonmentsummary.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-report/cartabandonment/CartAbandonmentsummary.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./CartAbandonmentsummary.component.scss */ "./src/app/layout/modules/admin-report/cartabandonment/CartAbandonmentsummary.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_3__["OrderhistoryService"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"],
            _services_activity_activity_service__WEBPACK_IMPORTED_MODULE_4__["ActivityService"]])
    ], CartAbandonmentSummaryComponent);
    return CartAbandonmentSummaryComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-report/cartabandonment/cartabandonmentsummary.module.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-report/cartabandonment/cartabandonmentsummary.module.ts ***!
  \**********************************************************************************************/
/*! exports provided: CartAbandonmentSummaryModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartAbandonmentSummaryModule", function() { return CartAbandonmentSummaryModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _cartabandonmentsummary_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./cartabandonmentsummary.component */ "./src/app/layout/modules/admin-report/cartabandonment/cartabandonmentsummary.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _cartabandonmentsummary_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./cartabandonmentsummary-routing.module */ "./src/app/layout/modules/admin-report/cartabandonment/cartabandonmentsummary-routing.module.ts");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/index.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(ng2_charts__WEBPACK_IMPORTED_MODULE_9__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var CartAbandonmentSummaryModule = /** @class */ (function () {
    function CartAbandonmentSummaryModule() {
    }
    CartAbandonmentSummaryModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _cartabandonmentsummary_routing_module__WEBPACK_IMPORTED_MODULE_7__["CartAbandonmentSummaryRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_3__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__["BsComponentModule"],
                ng2_charts__WEBPACK_IMPORTED_MODULE_9__["ChartsModule"]
            ],
            declarations: [
                _cartabandonmentsummary_component__WEBPACK_IMPORTED_MODULE_2__["CartAbandonmentSummaryComponent"]
            ]
        })
    ], CartAbandonmentSummaryModule);
    return CartAbandonmentSummaryModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-admin-report-cartabandonment-cartabandonmentsummary-module.js.map