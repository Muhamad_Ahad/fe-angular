(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-admin-salesorder-admin-salesorder-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-salesorder/admin-salesorder.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/admin-salesorder/admin-salesorder.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div >\r\n    <app-page-header [heading]=\"'&nbsp;'\" [icon]=\"'fa-table'\"></app-page-header>\r\n\r\n    <div class=\"container-swapper-option\">\r\n      <span style=\"margin-right: 10px;\">View  </span> \r\n      <div class=\"button-container\">\r\n    <button class=\"btn toolbar {{swaper}}\" (click)=\"swapClick(true)\">\r\n      <span class=\"fa fa-grip-horizontal\">Based On Product</span>\r\n    </button>\r\n    <button class=\"btn toolbar {{!swaper}}\" (click)=\"swapClick(false)\">\r\n      <span class=\"fa fa-table\">Based On Order_id</span>\r\n    </button>\r\n  </div>\r\n  </div>\r\n    <div *ngIf=\"Pointstransaction && swaper\">\r\n          <app-form-builder-table\r\n          [searchCallback]= \"[service, 'searchSalesOrderReportint',this]\"\r\n          [tableFormat]=\"tableFormat\"\r\n          [table_data]=\"Pointstransaction\"\r\n          [total_page]=\"totalPage\"\r\n        >\r\n  \r\n          </app-form-builder-table>\r\n    </div>\r\n    <div *ngIf=\"Pointstransaction  && !swaper\">\r\n      <app-form-builder-table\r\n      [searchCallback]= \"[service, 'searchSalesOrderReport2int',this]\"\r\n      [tableFormat]=\"tableFormat2\"\r\n      [table_data]=\"Pointstransaction\"\r\n      [total_page]=\"totalPage\"\r\n    >\r\n\r\n      </app-form-builder-table>\r\n</div>\r\n        <div *ngIf=\"pointstransactionDetail\">\r\n              <!-- <app-pointstransaction-detail [back]=\"[this,backToHere]\" [detail]=\"pointstransactionDetail\"></app-pointstransaction-detail> -->\r\n        </div>\r\n  \r\n    </div>\r\n  "

/***/ }),

/***/ "./src/app/layout/modules/admin-salesorder/admin-salesorder-routing.module.ts":
/*!************************************************************************************!*\
  !*** ./src/app/layout/modules/admin-salesorder/admin-salesorder-routing.module.ts ***!
  \************************************************************************************/
/*! exports provided: AdminSalesorderRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminSalesorderRoutingModule", function() { return AdminSalesorderRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _admin_salesorder_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./admin-salesorder.component */ "./src/app/layout/modules/admin-salesorder/admin-salesorder.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '', component: _admin_salesorder_component__WEBPACK_IMPORTED_MODULE_2__["AdminSalesorderComponent"]
    }
];
var AdminSalesorderRoutingModule = /** @class */ (function () {
    function AdminSalesorderRoutingModule() {
    }
    AdminSalesorderRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AdminSalesorderRoutingModule);
    return AdminSalesorderRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-salesorder/admin-salesorder.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/layout/modules/admin-salesorder/admin-salesorder.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container-swapper-option {\n  display: flex;\n  justify-content: center;\n  flex-direction: row;\n  align-items: center;\n}\n.container-swapper-option .button-container {\n  border: 2px solid #ddd;\n  border-radius: 8px;\n}\n.container-swapper-option .button-container button {\n  color: #ddd;\n  background: white;\n  font-size: 14px;\n  border-radius: 8px;\n}\n.container-swapper-option .button-container button.true {\n  background: #2480fb;\n  border-radius: 8px;\n  color: #ddd;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tc2FsZXNvcmRlci9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXGFkbWluLXNhbGVzb3JkZXJcXGFkbWluLXNhbGVzb3JkZXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2FkbWluLXNhbGVzb3JkZXIvYWRtaW4tc2FsZXNvcmRlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUNDSjtBREFJO0VBRUUsc0JBQUE7RUFDQSxrQkFBQTtBQ0NOO0FEQU07RUFDRSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUNFUjtBREFNO0VBRUUsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUNDUiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2FkbWluLXNhbGVzb3JkZXIvYWRtaW4tc2FsZXNvcmRlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250YWluZXItc3dhcHBlci1vcHRpb24ge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAuYnV0dG9uLWNvbnRhaW5lciB7XHJcblxyXG4gICAgICBib3JkZXI6IDJweCBzb2xpZCAjZGRkO1xyXG4gICAgICBib3JkZXItcmFkaXVzOjhweDtcclxuICAgICAgYnV0dG9uIHtcclxuICAgICAgICBjb2xvcjogI2RkZDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgICB9XHJcbiAgICAgIGJ1dHRvbi50cnVlIHtcclxuICAgICAgIFxyXG4gICAgICAgIGJhY2tncm91bmQ6ICMyNDgwZmI7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgICAgIGNvbG9yOiAjZGRkO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfSIsIi5jb250YWluZXItc3dhcHBlci1vcHRpb24ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5jb250YWluZXItc3dhcHBlci1vcHRpb24gLmJ1dHRvbi1jb250YWluZXIge1xuICBib3JkZXI6IDJweCBzb2xpZCAjZGRkO1xuICBib3JkZXItcmFkaXVzOiA4cHg7XG59XG4uY29udGFpbmVyLXN3YXBwZXItb3B0aW9uIC5idXR0b24tY29udGFpbmVyIGJ1dHRvbiB7XG4gIGNvbG9yOiAjZGRkO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBib3JkZXItcmFkaXVzOiA4cHg7XG59XG4uY29udGFpbmVyLXN3YXBwZXItb3B0aW9uIC5idXR0b24tY29udGFpbmVyIGJ1dHRvbi50cnVlIHtcbiAgYmFja2dyb3VuZDogIzI0ODBmYjtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xuICBjb2xvcjogI2RkZDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/admin-salesorder/admin-salesorder.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/layout/modules/admin-salesorder/admin-salesorder.component.ts ***!
  \*******************************************************************************/
/*! exports provided: AdminSalesorderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminSalesorderComponent", function() { return AdminSalesorderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var AdminSalesorderComponent = /** @class */ (function () {
    function AdminSalesorderComponent(OrderhistoryService, router) {
        this.OrderhistoryService = OrderhistoryService;
        this.router = router;
        this.swaper = true;
        this.Pointstransaction = [];
        this.tableFormat = {
            title: 'Sales Order Report Page (Product)',
            label_headers: [
                { label: 'Tgl. Order', visible: true, type: 'date', data_row_name: 'order_date' },
                { label: 'No Order', visible: true, type: 'string', data_row_name: 'invoice_id' },
                { label: 'Member ID', visible: true, type: 'string', data_row_name: 'member_email' },
                { label: 'Name', visible: true, type: 'string', data_row_name: 'member_name' },
                { label: 'Item Code', visible: true, type: 'string', data_row_name: 'product_code' },
                { label: 'Product Name', visible: true, type: 'string', data_row_name: 'product_name' },
                { label: 'Category', visible: true, type: 'string', data_row_name: 'categories' },
                { label: 'Merchant', visible: true, type: 'string', data_row_name: 'merchant_username' },
                { label: 'Qty', visible: true, type: 'string', data_row_name: 'quantity' },
                { label: 'Unit Price', visible: true, type: 'string', data_row_name: 'fixed_price' },
                { label: 'Total Order', visible: true, type: 'string', data_row_name: 'total_product_price' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: 'invoice_id',
                this: this,
                result_var_name: 'Pointstransaction',
                detail_function: [this, 'callDetail']
            }
        };
        this.tableFormat2 = {
            title: 'Sales Order Report Page (Order ID)',
            label_headers: [
                { label: 'Order Date', visible: true, type: 'date', data_row_name: 'order_date' },
                { label: 'No Order', visible: true, type: 'string', data_row_name: 'invoice_id' },
                { label: 'Member ID', visible: true, type: 'string', data_row_name: 'member_email' },
                { label: 'Member Name', visible: true, type: 'string', data_row_name: 'member_name' },
                { label: 'Total Order', visible: true, type: 'string', data_row_name: 'total_price' },
                { label: 'Total Shipping', visible: true, type: 'string', data_row_name: 'total_shipping' },
                { label: 'Lo Point', visible: true, type: 'string', data_row_name: 'lo_points' },
                { label: 'Unique Amount', visible: true, type: 'string', data_row_name: 'unique_amount' },
                { label: 'Total Order Summary', visible: true, type: 'string', data_row_name: 'sum_total' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: 'invoice_id',
                this: this,
                result_var_name: 'Pointstransaction',
                detail_function: [this, 'callDetail']
            }
        };
        this.form_input = {};
        this.errorLabel = false;
        this.totalPage = 0;
        this.pointstransactionDetail = false;
    }
    AdminSalesorderComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    AdminSalesorderComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var params, result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        params = {
                            search: { based_on: "products" },
                            current_page: 1,
                            limit_per_page: 50
                        };
                        this.service = this.OrderhistoryService;
                        result = {};
                        if (!(this.swaper == true)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.OrderhistoryService.getSalesOrderReportint('product')];
                    case 1:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, this.OrderhistoryService.getSalesOrderReportint('order_id')];
                    case 3:
                        result = _a.sent();
                        _a.label = 4;
                    case 4:
                        this.totalPage = result.result.total_page;
                        this.Pointstransaction = result.result.values;
                        return [3 /*break*/, 6];
                    case 5:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    AdminSalesorderComponent.prototype.swapClick = function (bool) {
        this.swaper = bool;
        this.firstLoad();
    };
    AdminSalesorderComponent.prototype.callDetail = function (orderhistory_id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.router.navigate(['administrator/orderhistoryallhistoryadmin/edit'], { queryParams: { id: orderhistory_id } });
                return [2 /*return*/];
            });
        });
    };
    AdminSalesorderComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.pointstransactionDetail = false;
                return [2 /*return*/];
            });
        });
    };
    AdminSalesorderComponent.prototype.addMonths = function (date, months) {
        date.setMonth(date.getMonth() + months);
        return date;
    };
    AdminSalesorderComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_1__["OrderhistoryService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    AdminSalesorderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-admin-salesorder',
            template: __webpack_require__(/*! raw-loader!./admin-salesorder.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-salesorder/admin-salesorder.component.html"),
            styles: [__webpack_require__(/*! ./admin-salesorder.component.scss */ "./src/app/layout/modules/admin-salesorder/admin-salesorder.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_1__["OrderhistoryService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AdminSalesorderComponent);
    return AdminSalesorderComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-salesorder/admin-salesorder.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/layout/modules/admin-salesorder/admin-salesorder.module.ts ***!
  \****************************************************************************/
/*! exports provided: AdminSalesorderModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminSalesorderModule", function() { return AdminSalesorderModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _admin_salesorder_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./admin-salesorder-routing.module */ "./src/app/layout/modules/admin-salesorder/admin-salesorder-routing.module.ts");
/* harmony import */ var _admin_salesorder_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./admin-salesorder.component */ "./src/app/layout/modules/admin-salesorder/admin-salesorder.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var AdminSalesorderModule = /** @class */ (function () {
    function AdminSalesorderModule() {
    }
    AdminSalesorderModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _admin_salesorder_routing_module__WEBPACK_IMPORTED_MODULE_2__["AdminSalesorderRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__["BsComponentModule"]
            ],
            declarations: [_admin_salesorder_component__WEBPACK_IMPORTED_MODULE_3__["AdminSalesorderComponent"]],
        })
    ], AdminSalesorderModule);
    return AdminSalesorderModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-admin-salesorder-admin-salesorder-module.js.map