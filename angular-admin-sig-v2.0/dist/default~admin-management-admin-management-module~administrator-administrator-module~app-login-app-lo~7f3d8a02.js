(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~admin-management-admin-management-module~administrator-administrator-module~app-login-app-lo~7f3d8a02"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/components/header/header.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/components/header/header.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ng-template #content let-modal>\r\n    <div class=\"modal-header\">\r\n        <h4 class=\"modal-title\" id=\"modal-basic-title\">Enter Password</h4>\r\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\r\n    </div>\r\n    <div class=\"modal-body\">\r\n        <form (submit)=\"onSubmit()\">\r\n            <div class=\"form-group\">\r\n                <label for=\"dateOfBirth\">Password</label>\r\n                <div class=\"input-group\">\r\n                    <input [(ngModel)]=\"password\" id=\"dateOfBirth\" type=\"password\" class=\"form-control\"\r\n                        placeholder=\"Password\" name=\"dp\">\r\n                </div>\r\n            </div>\r\n            <button type=\"submit\" class=\"btn btn-outline-dark\">Submit</button>\r\n        </form>\r\n    </div>\r\n</ng-template>\r\n\r\n\r\n<hr>\r\n\r\n<pre>{{closeResult}}</pre>\r\n\r\n<nav id=\"navbar-menu\" class=\"navbar navbar-expand-lg fixed-top\">\r\n    <button class=\"navbar-toggler\" type=\"button\" (click)=\"toggleSidebar()\">\r\n        <i class=\"fa fa-bars text-muted\" aria-hidden=\"true\"></i>\r\n    </button>\r\n    <ul id=\"navbarright\" class=\"navbar-nav ml-auto\" style=\"flex-direction: row;\">\r\n        <li class=\"datetimecontainer navbar-toggler\">\r\n            <a class=\"time-custom \">\r\n                <img src=\"assets/images/clock.svg\" style=\"width: 10px;\">\r\n                {{ timeCurrent }}\r\n            </a>\r\n            <a class=\"date-custom \">\r\n                <img src=\"assets/images/calendar icon.svg\" style=\"width: 10px;\">\r\n                {{ dateCurrent }}\r\n            </a>\r\n        </li>\r\n        <!-- RESPONSIVE -->\r\n        <!-- <li class=\"nav-item navbar-toggler\" style=\"list-style-type: none;\" id=\"administrator\">\r\n            <a href=\"javascript:void(0)\" class=\"nav-link\">\r\n                <span>Administrator</span>\r\n            </a>\r\n        </li> -->\r\n        <li class=\"nav-item navbar-toggler\" style=\"list-style-type: none;\" id=\"administrator\">\r\n            <a href=\"/app-login\" class=\"nav-link\">\r\n                <!-- <img src=\"assets/images/pikachu.png\"> -->\r\n                <span>Choose Program</span>\r\n            </a>\r\n        </li>\r\n        <a class=\"nav-link logout resp navbar-toggler\" routerLink=\"/login\" (click)=\"onLoggedout()\">\r\n            <img src=\"assets/images/logout.png\"> {{ 'Log Out' | translate }}\r\n        </a>\r\n    </ul>\r\n    <!-- CLOSE-RESPONSIVE -->\r\n    <div class=\"collapse navbar-collapse\">\r\n        <div class=\"form-inline\">\r\n            <datalist class=\"search-menu\" id=\"browsers\">\r\n                <option *ngFor=\"let dt of routePath\" [value]=\"dt.path\">{{dt.label}}</option>\r\n            </datalist>\r\n        </div>\r\n        <a class=\"admin-portal\">\r\n            <img src=\"assets/images/admin1.png\"> {{ portalName | translate }}\r\n        </a>\r\n        <ul class=\"navbar-nav ml-auto\">\r\n            <!-- NON-RESPONSIVE -->\r\n            <a class=\"time-custom\">\r\n                <img src=\"assets/images/clock.svg\" style=\"width: 20px;\">{{ time | date: 'hh:mm:ss a'   }}\r\n            </a>\r\n            <a class=\"date-custom\">\r\n                <img src=\"assets/images/calendar icon.svg\" style=\"width: 20px;\">{{ dateCurrent }}\r\n            </a>\r\n            <li class=\"nav-item dropdown\" id=\"administrator\">\r\n                <a href=\"javascript:void(0)\" class=\"nav-link\" Toggle>\r\n                    <span>Administrator</span>\r\n                </a>\r\n            </li>\r\n            <li class=\"nav-item dropdown\" id=\"administrator\">\r\n                <a href=\"/app-login\" class=\"nav-link\" Toggle>\r\n                    <!-- <img src=\"assets/images/pikachu.png\"> -->\r\n                    <span>Choose Program</span>\r\n                </a>\r\n            </li>\r\n            <a class=\"logout\" routerLink=\"/login\" (click)=\"onLoggedout()\">\r\n                <img src=\"assets/images/logout.png\"> {{ 'Log Out' | translate }}\r\n            </a>\r\n            <!-- CLOSE-NON-RESPONSIVE -->\r\n        </ul>\r\n    </div>\r\n</nav>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/router-master/router-master.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/router-master/router-master.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/layout/components/header/header.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/layout/components/header/header.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host nav.navbar.navbar-expand-lg.fixed-top:after {\n  content: \"\";\n  position: absolute;\n  width: 0;\n  height: 0;\n  border-style: solid;\n  border-width: 0 0 62px 106px;\n  border-color: transparent transparent #2b3240 transparent;\n  left: -106px;\n}\n:host .navbar {\n  background-color: #2b3240;\n  box-shadow: 0 4px 1px 0 rgba(0, 0, 0, 0.01), 0 3px 5px 0 rgba(0, 0, 0, 0.19);\n  margin-top: 23px;\n  left: unset;\n  right: 0px;\n  width: 100%;\n  position: fixed;\n}\n:host .navbar .navbar-brand > img {\n  width: 55px;\n  padding-right: 15px;\n  padding-bottom: 5px;\n}\n:host .navbar .navbar-brand {\n  color: #fff;\n}\n:host .navbar .nav-item > a {\n  color: #999;\n}\n:host .navbar .nav-item > a:hover {\n  color: #56a4ff;\n}\n:host .navbar .nav-link > img {\n  width: 30px;\n  padding-right: 5px;\n  padding-bottom: 5px;\n}\n:host .navbar .nav-link > span {\n  color: #56a4ff;\n}\n:host .navbar .bg-circle {\n  background-color: white;\n  border-radius: 50%;\n  width: 40px;\n  height: 40px;\n}\n:host .navbar .bg-circle > img {\n  width: 28px;\n  margin: 6px 0 5px 6px;\n}\n:host .navbar .logout {\n  color: #627190;\n  cursor: pointer;\n  margin-top: 10px;\n  text-decoration: none;\n}\n:host .navbar .logout:hover {\n  color: #56a4ff;\n}\n:host .navbar .time-custom,\n:host .navbar .date-custom {\n  margin: 10px 10px 0px;\n  text-decoration: none;\n  color: white;\n  border-right: #434c5c solid 1px;\n  padding-right: 20px;\n}\n:host .navbar .time-custom img,\n:host .navbar .date-custom img {\n  margin-right: 10px;\n}\n:host .navbar .admin-portal {\n  text-decoration: none;\n  color: white;\n  float: left;\n}\n:host .navbar .admin-portal img {\n  width: 50px;\n}\n:host .navbar .logout > img {\n  width: 20px;\n}\n:host .navbar #administrator > a {\n  padding-right: 20px;\n}\n:host .navbar .dropdown-item {\n  margin: 8px auto;\n  font-size: 15px;\n}\n:host .messages {\n  width: 300px;\n}\n:host .messages .media {\n  border-bottom: 1px solid #ddd;\n  padding: 5px 10px;\n}\n:host .messages .media:last-child {\n  border-bottom: none;\n}\n:host .messages .media-body h5 {\n  font-size: 13px;\n  font-weight: 600;\n}\n:host .messages .media-body .small {\n  margin: 0;\n}\n:host .messages .media-body .last {\n  font-size: 12px;\n  margin: 0;\n}\n.resp {\n  display: none;\n}\n.switch {\n  position: relative;\n  display: inline-block;\n  width: 60px;\n  height: 34px;\n}\n.switch-capsule {\n  text-align: center;\n}\n/* Hide default HTML checkbox */\n.switch input {\n  opacity: 0;\n  width: 0;\n  height: 0;\n}\n/* The slider */\n.slider {\n  position: absolute;\n  cursor: pointer;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background-color: #ccc;\n  transition: 0.4s;\n}\n.slider:before {\n  position: absolute;\n  content: \"\";\n  height: 26px;\n  width: 26px;\n  left: 4px;\n  bottom: 4px;\n  background-color: white;\n  transition: 0.4s;\n}\ninput:checked + .slider {\n  background-color: #2196f3;\n}\ninput:focus + .slider {\n  box-shadow: 0 0 1px #2196f3;\n}\ninput:checked + .slider:before {\n  -webkit-transform: translateX(26px);\n  transform: translateX(26px);\n}\n/* Rounded sliders */\n.slider.round {\n  border-radius: 34px;\n}\n.slider.round:before {\n  border-radius: 50%;\n}\n@media screen and (max-width: 600px) {\n  :host .navbar .navbar-brand {\n    margin-left: 50%;\n  }\n  :host .navbar #navbarright {\n    justify-content: center;\n    align-items: center;\n  }\n  :host .navbar #navbarright .logout {\n    font-size: 0pt;\n  }\n  :host .navbar #navbarright .logout img {\n    width: 25px;\n  }\n  :host .navbar #navbarright .time-custom,\n:host .navbar #navbarright .date-custom {\n    margin: 10px 10px 0px;\n    text-decoration: none;\n    color: white;\n    border-right: #434c5c solid 1px;\n    padding-right: 20px;\n  }\n  :host .navbar #navbarright .time-custom img,\n:host .navbar #navbarright .date-custom img {\n    display: none;\n    padding: 0;\n  }\n  :host .navbar .nav-link > span {\n    font-size: 15px;\n  }\n  :host .navbar #administrator > a {\n    margin-left: -14px;\n    margin-right: -20px;\n  }\n  :host .navbar .dropdown-menu {\n    top: 10vw !important;\n    left: 15vw !important;\n  }\n\n  .resp {\n    display: inline;\n  }\n\n  .navbar .navbar-expand-lg .fixed-top {\n    left: 0px !important;\n  }\n}\n@media screen and (max-width: 991px) {\n  :host #navbar-menu {\n    left: 0 !important;\n  }\n  :host #navbar-menu .navbar-brand {\n    margin-left: 50%;\n  }\n  :host #navbar-menu #administrator > a {\n    padding-right: 20px;\n    font-size: 1rem;\n  }\n  :host #navbar-menu .logout {\n    margin-top: 0px;\n    display: flex;\n    font-size: 1rem;\n    justify-content: center;\n    align-items: center;\n  }\n  :host #navbar-menu .logout img {\n    width: 25px;\n  }\n  :host #navbar-menu .admin-portal {\n    text-decoration: none;\n    color: white;\n    float: left;\n  }\n  :host #navbar-menu .admin-portal img {\n    width: 50px;\n  }\n  :host #navbar-menu .datetimecontainer {\n    display: flex;\n    flex-direction: column;\n  }\n  :host #navbar-menu .datetimecontainer .time-custom,\n:host #navbar-menu .datetimecontainer .date-custom {\n    margin: 3px 0px 0px;\n    text-decoration: none;\n    font-size: 12px;\n    color: white;\n    border-right: #434c5c solid 1px;\n  }\n  :host #navbar-menu .datetimecontainer .time-custom img,\n:host #navbar-menu .datetimecontainer .date-custom img {\n    margin-right: 10px;\n  }\n\n  .resp {\n    display: inline;\n  }\n\n  .navbar .navbar-expand-lg .fixed-top {\n    left: 0px !important;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L2NvbXBvbmVudHMvaGVhZGVyL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcY29tcG9uZW50c1xcaGVhZGVyXFxoZWFkZXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9jb21wb25lbnRzL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBR0U7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLG1CQUFBO0VBQ0EsNEJBQUE7RUFDQSx5REFBQTtFQUNBLFlBQUE7QUNGSjtBREtFO0VBQ0UseUJBZnNCO0VBZ0J0Qiw0RUFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtBQ0hKO0FES0k7RUFDRSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQ0hOO0FETUk7RUFDRSxXQUFBO0FDSk47QURPSTtFQUNFLFdBQUE7QUNMTjtBRE9NO0VBQ0UsY0FBQTtBQ0xSO0FEU0k7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQ1BOO0FEVUk7RUFDRSxjQUFBO0FDUk47QURXSTtFQUNFLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQ1ROO0FEWUk7RUFDRSxXQUFBO0VBQ0EscUJBQUE7QUNWTjtBRGFJO0VBQ0UsY0FBQTtFQUNBLGVBQUE7RUFNQSxnQkFBQTtFQUNBLHFCQUFBO0FDaEJOO0FEV007RUFDRSxjQUFBO0FDVFI7QURnQkk7O0VBRUUscUJBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7RUFDQSwrQkFBQTtFQUNBLG1CQUFBO0FDZE47QURlTTs7RUFDRSxrQkFBQTtBQ1pSO0FEZ0JJO0VBQ0UscUJBQUE7RUFDQSxZQUFBO0VBSUEsV0FBQTtBQ2pCTjtBRGNNO0VBQ0UsV0FBQTtBQ1pSO0FEaUJJO0VBQ0UsV0FBQTtBQ2ZOO0FEa0JJO0VBQ0UsbUJBQUE7QUNoQk47QURtQkk7RUFDRSxnQkFBQTtFQUNBLGVBQUE7QUNqQk47QURxQkU7RUFDRSxZQUFBO0FDbkJKO0FEcUJJO0VBQ0UsNkJBQUE7RUFDQSxpQkFBQTtBQ25CTjtBRHFCTTtFQUNFLG1CQUFBO0FDbkJSO0FEd0JNO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0FDdEJSO0FEeUJNO0VBQ0UsU0FBQTtBQ3ZCUjtBRDBCTTtFQUNFLGVBQUE7RUFDQSxTQUFBO0FDeEJSO0FEOEJBO0VBQ0UsYUFBQTtBQzNCRjtBRCtCQTtFQUNFLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQzVCRjtBRCtCQTtFQUNFLGtCQUFBO0FDNUJGO0FEK0JBLCtCQUFBO0FBQ0E7RUFDRSxVQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7QUM1QkY7QUQrQkEsZUFBQTtBQUNBO0VBQ0Usa0JBQUE7RUFDQSxlQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLHNCQUFBO0VBRUEsZ0JBQUE7QUM1QkY7QUQrQkE7RUFDRSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0VBQ0EsdUJBQUE7RUFFQSxnQkFBQTtBQzVCRjtBRCtCQTtFQUNFLHlCQUFBO0FDNUJGO0FEK0JBO0VBQ0UsMkJBQUE7QUM1QkY7QUQrQkE7RUFDRSxtQ0FBQTtFQUVBLDJCQUFBO0FDNUJGO0FEK0JBLG9CQUFBO0FBQ0E7RUFDRSxtQkFBQTtBQzVCRjtBRCtCQTtFQUNFLGtCQUFBO0FDNUJGO0FEK0JBO0VBR007SUFDRSxnQkFBQTtFQzlCTjtFRGdDSTtJQUNFLHVCQUFBO0lBQ0EsbUJBQUE7RUM5Qk47RUQrQk07SUFDRSxjQUFBO0VDN0JSO0VEOEJRO0lBQ0UsV0FBQTtFQzVCVjtFRCtCTTs7SUFFRSxxQkFBQTtJQUNBLHFCQUFBO0lBQ0EsWUFBQTtJQUNBLCtCQUFBO0lBQ0EsbUJBQUE7RUM3QlI7RUQ4QlE7O0lBQ0UsYUFBQTtJQUNBLFVBQUE7RUMzQlY7RUQrQkk7SUFDRSxlQUFBO0VDN0JOO0VEZ0NJO0lBQ0Usa0JBQUE7SUFDQSxtQkFBQTtFQzlCTjtFRGlDSTtJQUNFLG9CQUFBO0lBQ0EscUJBQUE7RUMvQk47O0VEb0NBO0lBQ0UsZUFBQTtFQ2pDRjs7RURtQ0E7SUFDRSxvQkFBQTtFQ2hDRjtBQUNGO0FEbUNBO0VBRUk7SUFDRSxrQkFBQTtFQ2xDSjtFRG9DSTtJQUNFLGdCQUFBO0VDbENOO0VEcUNJO0lBQ0UsbUJBQUE7SUFFQSxlQUFBO0VDcENOO0VEc0NJO0lBQ0UsZUFBQTtJQUNBLGFBQUE7SUFDQSxlQUFBO0lBQ0EsdUJBQUE7SUFDQSxtQkFBQTtFQ3BDTjtFRHFDTTtJQUNFLFdBQUE7RUNuQ1I7RURzQ0k7SUFDRSxxQkFBQTtJQUNBLFlBQUE7SUFJQSxXQUFBO0VDdkNOO0VEb0NNO0lBQ0UsV0FBQTtFQ2xDUjtFRHNDSTtJQUNFLGFBQUE7SUFDQSxzQkFBQTtFQ3BDTjtFRHNDTTs7SUFFRSxtQkFBQTtJQUVBLHFCQUFBO0lBQ0EsZUFBQTtJQUNBLFlBQUE7SUFDQSwrQkFBQTtFQ3JDUjtFRHVDUTs7SUFDRSxrQkFBQTtFQ3BDVjs7RUQwQ0E7SUFDRSxlQUFBO0VDdkNGOztFRHlDQTtJQUNFLG9CQUFBO0VDdENGO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvY29tcG9uZW50cy9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiJHRvcG5hdi1iYWNrZ3JvdW5kLWNvbG9yOiAjMmIzMjQwO1xyXG5cclxuOmhvc3Qge1xyXG4gIG5hdi5uYXZiYXIubmF2YmFyLWV4cGFuZC1sZy5maXhlZC10b3A6YWZ0ZXIge1xyXG4gICAgY29udGVudDogXCJcIjtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHdpZHRoOiAwO1xyXG4gICAgaGVpZ2h0OiAwO1xyXG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcclxuICAgIGJvcmRlci13aWR0aDogMCAwIDYycHggMTA2cHg7XHJcbiAgICBib3JkZXItY29sb3I6IHRyYW5zcGFyZW50IHRyYW5zcGFyZW50ICMyYjMyNDAgdHJhbnNwYXJlbnQ7XHJcbiAgICBsZWZ0OiAtMTA2cHg7XHJcbiAgfVxyXG5cclxuICAubmF2YmFyIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICR0b3BuYXYtYmFja2dyb3VuZC1jb2xvcjtcclxuICAgIGJveC1zaGFkb3c6IDAgNHB4IDFweCAwIHJnYmEoMCwgMCwgMCwgMC4wMSksIDAgM3B4IDVweCAwIHJnYmEoMCwgMCwgMCwgMC4xOSk7XHJcbiAgICBtYXJnaW4tdG9wOiAyM3B4O1xyXG4gICAgbGVmdDogdW5zZXQ7XHJcbiAgICByaWdodDogMHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcblxyXG4gICAgLm5hdmJhci1icmFuZCA+IGltZyB7XHJcbiAgICAgIHdpZHRoOiA1NXB4O1xyXG4gICAgICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xyXG4gICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5uYXZiYXItYnJhbmQge1xyXG4gICAgICBjb2xvcjogI2ZmZjtcclxuICAgIH1cclxuXHJcbiAgICAubmF2LWl0ZW0gPiBhIHtcclxuICAgICAgY29sb3I6ICM5OTk7XHJcblxyXG4gICAgICAmOmhvdmVyIHtcclxuICAgICAgICBjb2xvcjogIzU2YTRmZjtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5uYXYtbGluayA+IGltZyB7XHJcbiAgICAgIHdpZHRoOiAzMHB4O1xyXG4gICAgICBwYWRkaW5nLXJpZ2h0OiA1cHg7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICB9XHJcblxyXG4gICAgLm5hdi1saW5rID4gc3BhbiB7XHJcbiAgICAgIGNvbG9yOiAjNTZhNGZmO1xyXG4gICAgfVxyXG5cclxuICAgIC5iZy1jaXJjbGUge1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICB3aWR0aDogNDBweDtcclxuICAgICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5iZy1jaXJjbGUgPiBpbWcge1xyXG4gICAgICB3aWR0aDogMjhweDtcclxuICAgICAgbWFyZ2luOiA2cHggMCA1cHggNnB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5sb2dvdXQge1xyXG4gICAgICBjb2xvcjogIzYyNzE5MDtcclxuICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG5cclxuICAgICAgJjpob3ZlciB7XHJcbiAgICAgICAgY29sb3I6ICM1NmE0ZmY7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgIH1cclxuXHJcbiAgICAudGltZS1jdXN0b20sXHJcbiAgICAuZGF0ZS1jdXN0b20ge1xyXG4gICAgICBtYXJnaW46IDEwcHggMTBweCAwcHg7XHJcbiAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICBib3JkZXItcmlnaHQ6ICM0MzRjNWMgc29saWQgMXB4O1xyXG4gICAgICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xyXG4gICAgICBpbWcge1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5hZG1pbi1wb3J0YWwge1xyXG4gICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgaW1nIHtcclxuICAgICAgICB3aWR0aDogNTBweDtcclxuICAgICAgfVxyXG4gICAgICBmbG9hdDogbGVmdDtcclxuICAgIH1cclxuXHJcbiAgICAubG9nb3V0ID4gaW1nIHtcclxuICAgICAgd2lkdGg6IDIwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgI2FkbWluaXN0cmF0b3IgPiBhIHtcclxuICAgICAgcGFkZGluZy1yaWdodDogMjBweDtcclxuICAgIH1cclxuXHJcbiAgICAuZHJvcGRvd24taXRlbSB7XHJcbiAgICAgIG1hcmdpbjogOHB4IGF1dG87XHJcbiAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5tZXNzYWdlcyB7XHJcbiAgICB3aWR0aDogMzAwcHg7XHJcblxyXG4gICAgLm1lZGlhIHtcclxuICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkZGQ7XHJcbiAgICAgIHBhZGRpbmc6IDVweCAxMHB4O1xyXG5cclxuICAgICAgJjpsYXN0LWNoaWxkIHtcclxuICAgICAgICBib3JkZXItYm90dG9tOiBub25lO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLm1lZGlhLWJvZHkge1xyXG4gICAgICBoNSB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5zbWFsbCB7XHJcbiAgICAgICAgbWFyZ2luOiAwO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAubGFzdCB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgIG1hcmdpbjogMDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLnJlc3Age1xyXG4gIGRpc3BsYXk6IG5vbmU7XHJcbiAgLy8gcG9zaXRpb246IGZpeGVkO1xyXG59XHJcblxyXG4uc3dpdGNoIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIHdpZHRoOiA2MHB4O1xyXG4gIGhlaWdodDogMzRweDtcclxufVxyXG5cclxuLnN3aXRjaC1jYXBzdWxlIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi8qIEhpZGUgZGVmYXVsdCBIVE1MIGNoZWNrYm94ICovXHJcbi5zd2l0Y2ggaW5wdXQge1xyXG4gIG9wYWNpdHk6IDA7XHJcbiAgd2lkdGg6IDA7XHJcbiAgaGVpZ2h0OiAwO1xyXG59XHJcblxyXG4vKiBUaGUgc2xpZGVyICovXHJcbi5zbGlkZXIge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgdG9wOiAwO1xyXG4gIGxlZnQ6IDA7XHJcbiAgcmlnaHQ6IDA7XHJcbiAgYm90dG9tOiAwO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNjY2M7XHJcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiAwLjRzO1xyXG4gIHRyYW5zaXRpb246IDAuNHM7XHJcbn1cclxuXHJcbi5zbGlkZXI6YmVmb3JlIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgY29udGVudDogXCJcIjtcclxuICBoZWlnaHQ6IDI2cHg7XHJcbiAgd2lkdGg6IDI2cHg7XHJcbiAgbGVmdDogNHB4O1xyXG4gIGJvdHRvbTogNHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogMC40cztcclxuICB0cmFuc2l0aW9uOiAwLjRzO1xyXG59XHJcblxyXG5pbnB1dDpjaGVja2VkICsgLnNsaWRlciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzIxOTZmMztcclxufVxyXG5cclxuaW5wdXQ6Zm9jdXMgKyAuc2xpZGVyIHtcclxuICBib3gtc2hhZG93OiAwIDAgMXB4ICMyMTk2ZjM7XHJcbn1cclxuXHJcbmlucHV0OmNoZWNrZWQgKyAuc2xpZGVyOmJlZm9yZSB7XHJcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVgoMjZweCk7XHJcbiAgLW1zLXRyYW5zZm9ybTogdHJhbnNsYXRlWCgyNnB4KTtcclxuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoMjZweCk7XHJcbn1cclxuXHJcbi8qIFJvdW5kZWQgc2xpZGVycyAqL1xyXG4uc2xpZGVyLnJvdW5kIHtcclxuICBib3JkZXItcmFkaXVzOiAzNHB4O1xyXG59XHJcblxyXG4uc2xpZGVyLnJvdW5kOmJlZm9yZSB7XHJcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA2MDBweCkge1xyXG4gIDpob3N0IHtcclxuICAgIC5uYXZiYXIge1xyXG4gICAgICAubmF2YmFyLWJyYW5kIHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogNTAlO1xyXG4gICAgICB9XHJcbiAgICAgICNuYXZiYXJyaWdodCB7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAubG9nb3V0IHtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMHB0O1xyXG4gICAgICAgICAgaW1nIHtcclxuICAgICAgICAgICAgd2lkdGg6IDI1cHg7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC50aW1lLWN1c3RvbSxcclxuICAgICAgICAuZGF0ZS1jdXN0b20ge1xyXG4gICAgICAgICAgbWFyZ2luOiAxMHB4IDEwcHggMHB4O1xyXG4gICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgYm9yZGVyLXJpZ2h0OiAjNDM0YzVjIHNvbGlkIDFweDtcclxuICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDIwcHg7XHJcbiAgICAgICAgICBpbWcge1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgICAgICAgICBwYWRkaW5nOjA7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIC5uYXYtbGluayA+IHNwYW4ge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgICAgfVxyXG5cclxuICAgICAgI2FkbWluaXN0cmF0b3IgPiBhIHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogLTE0cHg7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAtMjBweDtcclxuICAgICAgfVxyXG5cclxuICAgICAgLmRyb3Bkb3duLW1lbnUge1xyXG4gICAgICAgIHRvcDogMTB2dyAhaW1wb3J0YW50O1xyXG4gICAgICAgIGxlZnQ6IDE1dncgIWltcG9ydGFudDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLnJlc3Age1xyXG4gICAgZGlzcGxheTogaW5saW5lO1xyXG4gIH1cclxuICAubmF2YmFyIC5uYXZiYXItZXhwYW5kLWxnIC5maXhlZC10b3Age1xyXG4gICAgbGVmdDogMHB4ICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTFweCkge1xyXG4gIDpob3N0IHtcclxuICAgICNuYXZiYXItbWVudSB7XHJcbiAgICAgIGxlZnQ6IDAgIWltcG9ydGFudDtcclxuICAgICAgLy8gcG9zaXRpb246IGZpeGVkO1xyXG4gICAgICAubmF2YmFyLWJyYW5kIHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogNTAlO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAjYWRtaW5pc3RyYXRvciA+IGEge1xyXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDIwcHg7XHJcbiAgICAgICAgLy8gbWFyZ2luLWxlZnQ6IDV2dztcclxuICAgICAgICBmb250LXNpemU6IDFyZW07XHJcbiAgICAgIH1cclxuICAgICAgLmxvZ291dCB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMHB4O1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZm9udC1zaXplOiAxcmVtO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgaW1nIHtcclxuICAgICAgICAgIHdpZHRoOiAyNXB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAuYWRtaW4tcG9ydGFsIHtcclxuICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIGltZyB7XHJcbiAgICAgICAgICB3aWR0aDogNTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgIH1cclxuICAgICAgLmRhdGV0aW1lY29udGFpbmVyIHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcblxyXG4gICAgICAgIC50aW1lLWN1c3RvbSxcclxuICAgICAgICAuZGF0ZS1jdXN0b20ge1xyXG4gICAgICAgICAgbWFyZ2luOiAzcHggMHB4IDBweDtcclxuXHJcbiAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICBib3JkZXItcmlnaHQ6ICM0MzRjNWMgc29saWQgMXB4O1xyXG5cclxuICAgICAgICAgIGltZyB7XHJcbiAgICAgICAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgLnJlc3Age1xyXG4gICAgZGlzcGxheTogaW5saW5lO1xyXG4gIH1cclxuICAubmF2YmFyIC5uYXZiYXItZXhwYW5kLWxnIC5maXhlZC10b3Age1xyXG4gICAgbGVmdDogMHB4ICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG59XHJcbiIsIjpob3N0IG5hdi5uYXZiYXIubmF2YmFyLWV4cGFuZC1sZy5maXhlZC10b3A6YWZ0ZXIge1xuICBjb250ZW50OiBcIlwiO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHdpZHRoOiAwO1xuICBoZWlnaHQ6IDA7XG4gIGJvcmRlci1zdHlsZTogc29saWQ7XG4gIGJvcmRlci13aWR0aDogMCAwIDYycHggMTA2cHg7XG4gIGJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQgdHJhbnNwYXJlbnQgIzJiMzI0MCB0cmFuc3BhcmVudDtcbiAgbGVmdDogLTEwNnB4O1xufVxuOmhvc3QgLm5hdmJhciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyYjMyNDA7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDFweCAwIHJnYmEoMCwgMCwgMCwgMC4wMSksIDAgM3B4IDVweCAwIHJnYmEoMCwgMCwgMCwgMC4xOSk7XG4gIG1hcmdpbi10b3A6IDIzcHg7XG4gIGxlZnQ6IHVuc2V0O1xuICByaWdodDogMHB4O1xuICB3aWR0aDogMTAwJTtcbiAgcG9zaXRpb246IGZpeGVkO1xufVxuOmhvc3QgLm5hdmJhciAubmF2YmFyLWJyYW5kID4gaW1nIHtcbiAgd2lkdGg6IDU1cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE1cHg7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG59XG46aG9zdCAubmF2YmFyIC5uYXZiYXItYnJhbmQge1xuICBjb2xvcjogI2ZmZjtcbn1cbjpob3N0IC5uYXZiYXIgLm5hdi1pdGVtID4gYSB7XG4gIGNvbG9yOiAjOTk5O1xufVxuOmhvc3QgLm5hdmJhciAubmF2LWl0ZW0gPiBhOmhvdmVyIHtcbiAgY29sb3I6ICM1NmE0ZmY7XG59XG46aG9zdCAubmF2YmFyIC5uYXYtbGluayA+IGltZyB7XG4gIHdpZHRoOiAzMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG59XG46aG9zdCAubmF2YmFyIC5uYXYtbGluayA+IHNwYW4ge1xuICBjb2xvcjogIzU2YTRmZjtcbn1cbjpob3N0IC5uYXZiYXIgLmJnLWNpcmNsZSB7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIHdpZHRoOiA0MHB4O1xuICBoZWlnaHQ6IDQwcHg7XG59XG46aG9zdCAubmF2YmFyIC5iZy1jaXJjbGUgPiBpbWcge1xuICB3aWR0aDogMjhweDtcbiAgbWFyZ2luOiA2cHggMCA1cHggNnB4O1xufVxuOmhvc3QgLm5hdmJhciAubG9nb3V0IHtcbiAgY29sb3I6ICM2MjcxOTA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuOmhvc3QgLm5hdmJhciAubG9nb3V0OmhvdmVyIHtcbiAgY29sb3I6ICM1NmE0ZmY7XG59XG46aG9zdCAubmF2YmFyIC50aW1lLWN1c3RvbSxcbjpob3N0IC5uYXZiYXIgLmRhdGUtY3VzdG9tIHtcbiAgbWFyZ2luOiAxMHB4IDEwcHggMHB4O1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLXJpZ2h0OiAjNDM0YzVjIHNvbGlkIDFweDtcbiAgcGFkZGluZy1yaWdodDogMjBweDtcbn1cbjpob3N0IC5uYXZiYXIgLnRpbWUtY3VzdG9tIGltZyxcbjpob3N0IC5uYXZiYXIgLmRhdGUtY3VzdG9tIGltZyB7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cbjpob3N0IC5uYXZiYXIgLmFkbWluLXBvcnRhbCB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgY29sb3I6IHdoaXRlO1xuICBmbG9hdDogbGVmdDtcbn1cbjpob3N0IC5uYXZiYXIgLmFkbWluLXBvcnRhbCBpbWcge1xuICB3aWR0aDogNTBweDtcbn1cbjpob3N0IC5uYXZiYXIgLmxvZ291dCA+IGltZyB7XG4gIHdpZHRoOiAyMHB4O1xufVxuOmhvc3QgLm5hdmJhciAjYWRtaW5pc3RyYXRvciA+IGEge1xuICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xufVxuOmhvc3QgLm5hdmJhciAuZHJvcGRvd24taXRlbSB7XG4gIG1hcmdpbjogOHB4IGF1dG87XG4gIGZvbnQtc2l6ZTogMTVweDtcbn1cbjpob3N0IC5tZXNzYWdlcyB7XG4gIHdpZHRoOiAzMDBweDtcbn1cbjpob3N0IC5tZXNzYWdlcyAubWVkaWEge1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2RkZDtcbiAgcGFkZGluZzogNXB4IDEwcHg7XG59XG46aG9zdCAubWVzc2FnZXMgLm1lZGlhOmxhc3QtY2hpbGQge1xuICBib3JkZXItYm90dG9tOiBub25lO1xufVxuOmhvc3QgLm1lc3NhZ2VzIC5tZWRpYS1ib2R5IGg1IHtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBmb250LXdlaWdodDogNjAwO1xufVxuOmhvc3QgLm1lc3NhZ2VzIC5tZWRpYS1ib2R5IC5zbWFsbCB7XG4gIG1hcmdpbjogMDtcbn1cbjpob3N0IC5tZXNzYWdlcyAubWVkaWEtYm9keSAubGFzdCB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgbWFyZ2luOiAwO1xufVxuXG4ucmVzcCB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5cbi5zd2l0Y2gge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgd2lkdGg6IDYwcHg7XG4gIGhlaWdodDogMzRweDtcbn1cblxuLnN3aXRjaC1jYXBzdWxlIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4vKiBIaWRlIGRlZmF1bHQgSFRNTCBjaGVja2JveCAqL1xuLnN3aXRjaCBpbnB1dCB7XG4gIG9wYWNpdHk6IDA7XG4gIHdpZHRoOiAwO1xuICBoZWlnaHQ6IDA7XG59XG5cbi8qIFRoZSBzbGlkZXIgKi9cbi5zbGlkZXIge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgYm90dG9tOiAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjY2NjO1xuICAtd2Via2l0LXRyYW5zaXRpb246IDAuNHM7XG4gIHRyYW5zaXRpb246IDAuNHM7XG59XG5cbi5zbGlkZXI6YmVmb3JlIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBjb250ZW50OiBcIlwiO1xuICBoZWlnaHQ6IDI2cHg7XG4gIHdpZHRoOiAyNnB4O1xuICBsZWZ0OiA0cHg7XG4gIGJvdHRvbTogNHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiAwLjRzO1xuICB0cmFuc2l0aW9uOiAwLjRzO1xufVxuXG5pbnB1dDpjaGVja2VkICsgLnNsaWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyMTk2ZjM7XG59XG5cbmlucHV0OmZvY3VzICsgLnNsaWRlciB7XG4gIGJveC1zaGFkb3c6IDAgMCAxcHggIzIxOTZmMztcbn1cblxuaW5wdXQ6Y2hlY2tlZCArIC5zbGlkZXI6YmVmb3JlIHtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVgoMjZweCk7XG4gIC1tcy10cmFuc2Zvcm06IHRyYW5zbGF0ZVgoMjZweCk7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgyNnB4KTtcbn1cblxuLyogUm91bmRlZCBzbGlkZXJzICovXG4uc2xpZGVyLnJvdW5kIHtcbiAgYm9yZGVyLXJhZGl1czogMzRweDtcbn1cblxuLnNsaWRlci5yb3VuZDpiZWZvcmUge1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG59XG5cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDYwMHB4KSB7XG4gIDpob3N0IC5uYXZiYXIgLm5hdmJhci1icmFuZCB7XG4gICAgbWFyZ2luLWxlZnQ6IDUwJTtcbiAgfVxuICA6aG9zdCAubmF2YmFyICNuYXZiYXJyaWdodCB7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxuICA6aG9zdCAubmF2YmFyICNuYXZiYXJyaWdodCAubG9nb3V0IHtcbiAgICBmb250LXNpemU6IDBwdDtcbiAgfVxuICA6aG9zdCAubmF2YmFyICNuYXZiYXJyaWdodCAubG9nb3V0IGltZyB7XG4gICAgd2lkdGg6IDI1cHg7XG4gIH1cbiAgOmhvc3QgLm5hdmJhciAjbmF2YmFycmlnaHQgLnRpbWUtY3VzdG9tLFxuOmhvc3QgLm5hdmJhciAjbmF2YmFycmlnaHQgLmRhdGUtY3VzdG9tIHtcbiAgICBtYXJnaW46IDEwcHggMTBweCAwcHg7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBib3JkZXItcmlnaHQ6ICM0MzRjNWMgc29saWQgMXB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG4gIH1cbiAgOmhvc3QgLm5hdmJhciAjbmF2YmFycmlnaHQgLnRpbWUtY3VzdG9tIGltZyxcbjpob3N0IC5uYXZiYXIgI25hdmJhcnJpZ2h0IC5kYXRlLWN1c3RvbSBpbWcge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgcGFkZGluZzogMDtcbiAgfVxuICA6aG9zdCAubmF2YmFyIC5uYXYtbGluayA+IHNwYW4ge1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgfVxuICA6aG9zdCAubmF2YmFyICNhZG1pbmlzdHJhdG9yID4gYSB7XG4gICAgbWFyZ2luLWxlZnQ6IC0xNHB4O1xuICAgIG1hcmdpbi1yaWdodDogLTIwcHg7XG4gIH1cbiAgOmhvc3QgLm5hdmJhciAuZHJvcGRvd24tbWVudSB7XG4gICAgdG9wOiAxMHZ3ICFpbXBvcnRhbnQ7XG4gICAgbGVmdDogMTV2dyAhaW1wb3J0YW50O1xuICB9XG5cbiAgLnJlc3Age1xuICAgIGRpc3BsYXk6IGlubGluZTtcbiAgfVxuXG4gIC5uYXZiYXIgLm5hdmJhci1leHBhbmQtbGcgLmZpeGVkLXRvcCB7XG4gICAgbGVmdDogMHB4ICFpbXBvcnRhbnQ7XG4gIH1cbn1cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MXB4KSB7XG4gIDpob3N0ICNuYXZiYXItbWVudSB7XG4gICAgbGVmdDogMCAhaW1wb3J0YW50O1xuICB9XG4gIDpob3N0ICNuYXZiYXItbWVudSAubmF2YmFyLWJyYW5kIHtcbiAgICBtYXJnaW4tbGVmdDogNTAlO1xuICB9XG4gIDpob3N0ICNuYXZiYXItbWVudSAjYWRtaW5pc3RyYXRvciA+IGEge1xuICAgIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG4gICAgZm9udC1zaXplOiAxcmVtO1xuICB9XG4gIDpob3N0ICNuYXZiYXItbWVudSAubG9nb3V0IHtcbiAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxuICA6aG9zdCAjbmF2YmFyLW1lbnUgLmxvZ291dCBpbWcge1xuICAgIHdpZHRoOiAyNXB4O1xuICB9XG4gIDpob3N0ICNuYXZiYXItbWVudSAuYWRtaW4tcG9ydGFsIHtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGZsb2F0OiBsZWZ0O1xuICB9XG4gIDpob3N0ICNuYXZiYXItbWVudSAuYWRtaW4tcG9ydGFsIGltZyB7XG4gICAgd2lkdGg6IDUwcHg7XG4gIH1cbiAgOmhvc3QgI25hdmJhci1tZW51IC5kYXRldGltZWNvbnRhaW5lciB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICB9XG4gIDpob3N0ICNuYXZiYXItbWVudSAuZGF0ZXRpbWVjb250YWluZXIgLnRpbWUtY3VzdG9tLFxuOmhvc3QgI25hdmJhci1tZW51IC5kYXRldGltZWNvbnRhaW5lciAuZGF0ZS1jdXN0b20ge1xuICAgIG1hcmdpbjogM3B4IDBweCAwcHg7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgYm9yZGVyLXJpZ2h0OiAjNDM0YzVjIHNvbGlkIDFweDtcbiAgfVxuICA6aG9zdCAjbmF2YmFyLW1lbnUgLmRhdGV0aW1lY29udGFpbmVyIC50aW1lLWN1c3RvbSBpbWcsXG46aG9zdCAjbmF2YmFyLW1lbnUgLmRhdGV0aW1lY29udGFpbmVyIC5kYXRlLWN1c3RvbSBpbWcge1xuICAgIG1hcmdpbi1yaWdodDogMTBweDtcbiAgfVxuXG4gIC5yZXNwIHtcbiAgICBkaXNwbGF5OiBpbmxpbmU7XG4gIH1cblxuICAubmF2YmFyIC5uYXZiYXItZXhwYW5kLWxnIC5maXhlZC10b3Age1xuICAgIGxlZnQ6IDBweCAhaW1wb3J0YW50O1xuICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/components/header/header.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/layout/components/header/header.component.ts ***!
  \**************************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _routing_path__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../routing.path */ "./src/app/layout/routing.path.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _services_program_name_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/program-name.service */ "./src/app/services/program-name.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(translate, router, modalService, programNameService) {
        var _this = this;
        this.translate = translate;
        this.router = router;
        this.modalService = modalService;
        this.programNameService = programNameService;
        this.pushRightClass = 'push-right';
        this.dataSearch = '';
        this.developmentMode = 'production';
        // time: Date;
        this.time = new Date();
        this.routePath = _routing_path__WEBPACK_IMPORTED_MODULE_3__["routePath"];
        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
        this.translate.setDefaultLang('en');
        var browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');
        this.router.events.subscribe(function (val) {
            if (val instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"] &&
                window.innerWidth <= 992 &&
                _this.isToggled()) {
                _this.toggleSidebar();
            }
        });
    }
    HeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dateTimeCurrent = Date();
        var clock = new Date();
        var arrayDate = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        var splittedDate = this.dateTimeCurrent.split(' ');
        // switch (splittedDate[0]) {
        //     case 'Mon':
        //         splittedDate[0] = arrayDate[0];
        //         break;
        //     case 'Tue':
        //         splittedDate[0] = arrayDate[1];
        //         break;
        //     case 'Wed':
        //         splittedDate[0] = arrayDate[2];
        //         break;    
        //     case 'Thu':
        //         splittedDate[0] = arrayDate[3];
        //         break; 
        //     case 'Fri':
        //         splittedDate[0] = arrayDate[4];
        //         break;
        //     case 'Sat':
        //         splittedDate[0] = arrayDate[5];
        //         break;
        //     case 'Sun':
        //         splittedDate[0] = arrayDate[6];
        //         break;      
        //     default:
        //         break;
        // }
        this.dateCurrent = splittedDate[0] + ', ' + splittedDate[2] + ' ' + splittedDate[1] + ' ' + splittedDate[3];
        var res = (splittedDate[4]).split(':');
        this.timeCurrent = setInterval(function () {
            _this.time = new Date();
        }, 1000);
        console.log(this.timeCurrent);
        // this.timeCurrent = setInterval(() => {
        //     clock = new Date(clock.setSeconds(clock.getSeconds()+ 1));
        //     this.time = clock;
        // })
        // console.log(this.timeCurrent)
        var program = localStorage.getItem('programName');
        try {
            this.programNameService.currentData().subscribe(function (value) {
                _this.portalName = value;
            });
            this.programNameService.setData(program);
        }
        catch (error) {
            console.log("error", error);
        }
    };
    // clock(){
    //     var today = new Date();
    //     var hours = today.getHours();
    //     var minutes = today.getMinutes();
    //     var second = today.getSeconds();
    //     console.log("result", today)
    // }
    HeaderComponent.prototype.onSubmit = function () {
        console.log(this.password);
        if (this.password == "devmode1234") {
            localStorage.setItem('devmode', 'active');
            this.devModeLoggedout();
            window.location.href = "/login";
        }
        else {
            alert("wrong password");
        }
    };
    HeaderComponent.prototype.isToggled = function () {
        var dom = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    };
    HeaderComponent.prototype.toggleSidebar = function () {
        var dom = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    };
    HeaderComponent.prototype.rltAndLtr = function () {
        var dom = document.querySelector('body');
        dom.classList.toggle('rtl');
    };
    HeaderComponent.prototype.goHome = function () {
        this.router.navigate(['administrator/orderhistoryallhistoryadmin']);
    };
    HeaderComponent.prototype.onLoggedout = function () {
        localStorage.removeItem('isLoggedin');
        localStorage.removeItem('tokenlogin');
        localStorage.removeItem('devmode');
        // window.location.reload();
        window.location.href = "/login";
    };
    HeaderComponent.prototype.devModeLoggedout = function () {
        localStorage.removeItem('isLoggedin');
        localStorage.removeItem('tokenlogin');
    };
    HeaderComponent.prototype.changeLang = function (language) {
        this.translate.use(language);
    };
    HeaderComponent.prototype.devmodeon = function (content) {
        if (localStorage.getItem('devmode') == 'active') {
            localStorage.removeItem('devmode');
            this.onLoggedout();
            window.location.href = "/login";
        }
        else {
            // this.open(content);
            localStorage.setItem('devmode', 'active');
            this.devModeLoggedout();
            window.location.href = "/login";
        }
    };
    // devmodeoff(){
    //     localStorage.removeItem('devmode');
    //     this.onLoggedout();
    //     window.location.href = "/login";
    // }
    HeaderComponent.prototype.onSearch = function () {
        // console.log('routepath', routePath);    
    };
    HeaderComponent.prototype.onDataChanged = function () {
        console.log('changed', this.dataSearch);
        this.router.navigateByUrl(this.dataSearch);
        this.dataSearch = '';
    };
    HeaderComponent.prototype.open = function (content) {
        var _this = this;
        if (localStorage.getItem('devmode') == 'active') {
            localStorage.removeItem('devmode');
            this.onLoggedout();
            window.location.href = "/login";
        }
        else {
            this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(function (result) {
                _this.closeResult = "Closed with: " + result;
            }, function (reason) {
                _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
            });
        }
    };
    HeaderComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["ModalDismissReasons"].ESC) {
            var element = document.getElementById("checkbox");
            element.checked = false;
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["ModalDismissReasons"].BACKDROP_CLICK) {
            var element = document.getElementById("checkbox");
            element.checked = false;
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    HeaderComponent.ctorParameters = function () { return [
        { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"] },
        { type: _services_program_name_service__WEBPACK_IMPORTED_MODULE_5__["ProgramNameService"] }
    ]; };
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! raw-loader!./header.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/components/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/layout/components/header/header.component.scss")]
        }),
        __metadata("design:paramtypes", [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"], _services_program_name_service__WEBPACK_IMPORTED_MODULE_5__["ProgramNameService"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/layout/components/header/header.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/layout/components/header/header.module.ts ***!
  \***********************************************************/
/*! exports provided: HeaderModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderModule", function() { return HeaderModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _header_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./header.component */ "./src/app/layout/components/header/header.component.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


// import { BrowserModule } from '@angular/platform-browser';




var HeaderModule = /** @class */ (function () {
    function HeaderModule() {
    }
    HeaderModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModule"],
            ],
            declarations: [_header_component__WEBPACK_IMPORTED_MODULE_4__["HeaderComponent"]],
            exports: [_header_component__WEBPACK_IMPORTED_MODULE_4__["HeaderComponent"]],
            bootstrap: [_header_component__WEBPACK_IMPORTED_MODULE_4__["HeaderComponent"]],
        })
    ], HeaderModule);
    return HeaderModule;
}());



/***/ }),

/***/ "./src/app/layout/router-master/router-master.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/layout/router-master/router-master.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9yb3V0ZXItbWFzdGVyL3JvdXRlci1tYXN0ZXIuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/layout/router-master/router-master.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/layout/router-master/router-master.component.ts ***!
  \*****************************************************************/
/*! exports provided: RouterMasterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RouterMasterComponent", function() { return RouterMasterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/observerable/permission-observer */ "./src/app/services/observerable/permission-observer.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RouterMasterComponent = /** @class */ (function () {
    function RouterMasterComponent(router, castPermission) {
        this.router = router;
        this.castPermission = castPermission;
    }
    RouterMasterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.castPermission.currentPermission.subscribe(function (permissionVal) {
            // if(permissionVal == 'merchant'){
            //     this.router.navigate(['/merchant-portal/homepage']);
            //     return true;
            // }
            if (permissionVal == 'admin') {
                // this.router.navigate(['/app-login']);
                // this.router.navigate(['/administrator/order-history-summary']);
            }
            else {
                _this.router.navigate(['/login/out']);
            }
        });
        var isLoggedin = localStorage.getItem('isLoggedin');
        if (isLoggedin == 'true') {
            this.router.navigateByUrl('/app-login');
        }
    };
    RouterMasterComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_2__["PermissionObserver"] }
    ]; };
    RouterMasterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-router-master',
            template: __webpack_require__(/*! raw-loader!./router-master.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/router-master/router-master.component.html"),
            styles: [__webpack_require__(/*! ./router-master.component.scss */ "./src/app/layout/router-master/router-master.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_2__["PermissionObserver"]])
    ], RouterMasterComponent);
    return RouterMasterComponent;
}());



/***/ }),

/***/ "./src/app/layout/routing.path.ts":
/*!****************************************!*\
  !*** ./src/app/layout/routing.path.ts ***!
  \****************************************/
/*! exports provided: routePath */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routePath", function() { return routePath; });
/* harmony import */ var _router_master_router_master_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./router-master/router-master.component */ "./src/app/layout/router-master/router-master.component.ts");

var routePath = [
    { path: '', component: _router_master_router_master_component__WEBPACK_IMPORTED_MODULE_0__["RouterMasterComponent"], label: "router master component" },
    { path: 'administrator', loadChildren: './administrator/administrator.module#AdministratorModule', label: 'Administrator' },
    { path: 'merchant-portal', loadChildren: './merchant-portal/merchant-portal.module#MerchantPortalModule', label: "Merchant portal" },
    { path: 'admin-management', loadChildren: './admin-management/admin-management.module#AdminManagementModule', label: 'Admin Management' },
];


/***/ }),

/***/ "./src/app/services/observerable/permission-observer.ts":
/*!**************************************************************!*\
  !*** ./src/app/services/observerable/permission-observer.ts ***!
  \**************************************************************/
/*! exports provided: PermissionObserver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PermissionObserver", function() { return PermissionObserver; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PermissionObserver = /** @class */ (function () {
    function PermissionObserver() {
        this.currentPermission = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"]({});
        this.castCurrentPermission = this.currentPermission.asObservable();
        this.api_url = '';
    }
    PermissionObserver.prototype.updateCurrentPermission = function (data) {
        this.currentPermission.next(data);
    };
    PermissionObserver = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root',
        }),
        __metadata("design:paramtypes", [])
    ], PermissionObserver);
    return PermissionObserver;
}());



/***/ })

}]);
//# sourceMappingURL=default~admin-management-admin-management-module~administrator-administrator-module~app-login-app-lo~7f3d8a02.js.map