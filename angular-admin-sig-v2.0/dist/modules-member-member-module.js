(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-member-member-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/member/add/member.add.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/member/add/member.add.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n\r\n    <app-page-header [heading]=\"'Add New Member'\" [icon]=\"'fa-table'\"></app-page-header>\r\n\r\n    <div>\r\n        <div class=\"card card-detail\">\r\n            <div class=\"card-header\">\r\n                <button class=\"btn save_button\" (click)=\"formSubmitAddMember()\" type=\"submit\"\r\n                    [routerLinkActive]=\"['router-link-active']\"><i class=\"fa fa-fw fa-save\"\r\n                        style=\"transform: translate(-3px, -1px)\"></i>Submit</button>\r\n                <button class=\"btn back_button\" (click)=\"backTo()\"><i class=\"fa fa-fw fa-angle-left\"></i>Back</button>\r\n            </div>\r\n\r\n            <div class=\"card-content\">\r\n                <div class=\"member-detail\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-8\">\r\n                            <div class=\"card mb-3\">\r\n                                <div class=\"card-header\">\r\n                                    <h2> Member Details </h2>\r\n                                </div>\r\n                                <div class=\"card-content\">\r\n                                    <div class=\"col-md-12\">\r\n\r\n                                        <label>Member ID</label>\r\n                                        <form-input name=\"id\" [type]=\"'text'\" [(ngModel)]=\"form.id\" [placeholder]=\"'Member ID'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Full Name</label>\r\n                                        <form-input name=\"full_name\" [(ngModel)]=\"form.full_name\" [type]=\"'text'\" [placeholder]=\"'Full Name'\"\r\n                                            autofocus required>\r\n                                        </form-input>\r\n\r\n                                        <!-- <div class=\"form-group\">\r\n                                            <label for=\"dob\">Date Of Birth</label>\r\n                                            <input type=\"text\" name=\"dob\" [(ngModel)]=\"dob\" type=\"date\"\r\n                                                class=\"form-control\">\r\n                                        </div> -->\r\n\r\n                                        <label>Date of Birth</label>\r\n                                        <div class=\"input-group datepicker-input\" format=\"DD-MM-YYYY\">\r\n                                            <input class=\"form-control\" placeholder=\"yyyy-mm-dd\" name=\"dob\"\r\n                                                [(ngModel)]=\"form.dob\" ngbDatepicker #d=\"ngbDatepicker\">\r\n                                            <button class=\"input-group-addon\" (click)=\"d.toggle()\" type=\"button\">\r\n                                                <i class=\"fa fa-calendar\"></i>\r\n                                            </button>\r\n                                        </div>\r\n\r\n\r\n                                        <div class=\"form-group\">\r\n                                            <label>Gender</label>\r\n                                            <select class=\"form-control\" name=\"gender\" [(ngModel)]=\"form.gender\">\r\n                                                <option *ngFor=\"let g of gender_type\" [ngValue]=\"g.value\">{{g.label}}\r\n                                                </option>\r\n                                            </select>\r\n                                        </div>\r\n\r\n                                        <div class=\"form-group\">\r\n                                            <label>Member Status</label>\r\n                                            <select class=\"form-control\" name=\"member_status\"\r\n                                                [(ngModel)]=\"form.member_status\">\r\n                                                <option *ngFor=\"let m of member_status_type\" [ngValue]=\"m.value\">\r\n                                                    {{m.label}}</option>\r\n                                            </select>\r\n                                        </div>\r\n\r\n                                        <label>Address</label>\r\n                                        <form-input name=\"maddress1\" [(ngModel)]=\"form.maddress1\" [type]=\"'textarea'\" [placeholder]=\"'Address'\"\r\n                                            autofocus required>\r\n                                        </form-input>\r\n\r\n                                        <label>City</label>\r\n                                        <form-input name=\"mcity\" [(ngModel)]=\"form.mcity\" [type]=\"'text'\" [placeholder]=\"'City'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Postcode</label>\r\n                                        <form-input name=\"mpostcode\" [(ngModel)]=\"form.mpostcode\" [type]=\"'number'\" [placeholder]=\"'Postcode'\"\r\n                                            autofocus required>\r\n                                        </form-input>\r\n\r\n                                        <label>State</label>\r\n                                        <form-input name=\"mstate\" [(ngModel)]=\"form.mstate\" [type]=\"'text'\" [placeholder]=\"'State'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Country</label>\r\n                                        <form-input name=\"mcountry\" [(ngModel)]=\"form.mcountry\" [type]=\"'text'\" [placeholder]=\"'Country'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"col-md-4\">\r\n                            <div class=\"card mb-3\">\r\n                                <div class=\"card-header\">\r\n                                    <h2>Member &amp; Status Member</h2>\r\n                                </div>\r\n                                <div class=\"card-content\">\r\n                                    <div class=\"col-md-12\">\r\n\r\n                                        <label>Password</label>\r\n                                        <form-input name=\"password\" [type]=\"'password'\" [placeholder]=\"'Password'\"\r\n                                            [(ngModel)]=\"form.password\" autofocus required>\r\n                                        </form-input>\r\n\r\n                                        <label>Email</label>\r\n                                        <form-input name=\"email\" [(ngModel)]=\"form.email\" [type]=\"'text'\" [placeholder]=\"'Email'\" autofocus\r\n                                            required>\r\n                                        </form-input>\r\n\r\n                                        <label>Cell Phone</label>\r\n                                        <form-input name=\"cell_phone\" [type]=\"'number'\" [(ngModel)]=\"form.cell_phone\" [placeholder]=\"'Cell Phone'\"\r\n                                            autofocus required>\r\n                                        </form-input>\r\n\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <!-- <form #form=\"ngForm\" (ngSubmit)=\"formSubmitAddMember(form.value)\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-6\">\r\n                        <label>Full Name</label>\r\n                        <form-input name=\"full_name\" [type]=\"'text'\" [placeholder]=\"'Full Name'\" [(ngModel)]=\"full_name\"\r\n                            required></form-input>\r\n\r\n                        <label>Email</label>\r\n                        <form-input name=\"email\" [placeholder]=\"'Email'\" [type]=\"'text'\" [(ngModel)]=\"email\" required>\r\n                        </form-input>\r\n\r\n                        <label>Password</label>\r\n                        <form-input name=\"password\" [placeholder]=\"'Password'\" [type]=\"'password'\"\r\n                            [(ngModel)]=\"password\" required></form-input>\r\n\r\n                    \r\n                        <label>Cell Phone</label>\r\n                        <form-input name=\"cell_phone\" [placeholder]=\"'Cell Phone'\" [type]=\"'number'\"\r\n                            [(ngModel)]=\"cell_phone\" required></form-input>\r\n\r\n                        <label>Date of Birth</label>\r\n                        <div class=\"form-group\">\r\n                            <div class=\"input-group datepicker-input\">\r\n                                <input class=\"form-control\" placeholder=\"yyyy-mm-dd\" name=\"dob\" [(ngModel)]=\"dob\"\r\n                                    ngbDatepicker #d=\"ngbDatepicker\">\r\n                                <button class=\"input-group-addon\" (click)=\"d.toggle()\" type=\"button\">\r\n                                    <span class=\"fa fa-calendar\"></span>\r\n                                </button>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <label>Date of Birth</label>\r\n                        <div class=\"form-group\">\r\n                            <div class=\"input-group datepicker-input\">\r\n                                <input class=\"form-control\" placeholder=\"yyyy-mm-dd\" name=\"dob\" [(ngModel)]=\"dob\"\r\n                                    ngbDatepicker #d=\"ngbDatepicker\">\r\n                                <button class=\"input-group-addon\" (click)=\"d.toggle()\" type=\"button\">\r\n                                    <span class=\"fa fa-calendar\"></span>\r\n                                </button>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <label>City</label>\r\n                        <form-input name=\"mcity\" [placeholder]=\"'City'\" [type]=\"'text'\" [(ngModel)]=\"mcity\" required>\r\n                        </form-input>\r\n                    </div>\r\n\r\n                    <div class=\"col-md-6\">\r\n                        <label>State</label>\r\n                        <form-input name=\"mstate\" [placeholder]=\"'State'\" [type]=\"'text'\" [(ngModel)]=\"mstate\" required>\r\n                        </form-input>\r\n\r\n                        <label>Country</label>\r\n                        <form-input name=\"mcountry\" [placeholder]=\"'Country'\" [type]=\"'text'\" [(ngModel)]=\"mcountry\"\r\n                            required></form-input>\r\n\r\n                        <label>Address</label>\r\n                        <form-input name=\"maddress1\" [placeholder]=\"'Address'\" [type]=\"'textarea'\"\r\n                            [(ngModel)]=\"maddress1\" required></form-input>\r\n\r\n                        <label>Post Code</label>\r\n                        <form-input name=\"mpostcode\" [placeholder]=\"'Post Code'\" [type]=\"'number'\"\r\n                            [(ngModel)]=\"mpostcode\" required></form-input>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label>Gender</label>\r\n                            <select class=\"form-control\" name=\"gender\" [(ngModel)]=\"gender\">\r\n                                <option *ngFor=\"let d of gender_type\" value=\"{{d.value}}\">{{d.label}}</option>\r\n                            </select>\r\n                        </div>\r\n\r\n\r\n                        <label>Marital Status</label>\r\n                        <form-select name=\"marital_status\" [(ngModel)]=\"marital_status\" [data]=\"marital_status_type\">\r\n                        </form-select>\r\n\r\n                        <label>Member Type</label>\r\n                        <form-select name=\"type\" [(ngModel)]=\"type\" [data]=\"member_type\"></form-select>\r\n                    </div>\r\n                </div>\r\n\r\n            </form> -->\r\n        </div>\r\n    </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/member/detail/member-point-history/member-point-history.component.html":
/*!*********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/member/detail/member-point-history/member-point-history.component.html ***!
  \*********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div >\r\n    <!-- <app-page-header [heading]=\"'&nbsp;'\" [icon]=\"'fa-table'\"></app-page-header> -->\r\n    <div *ngIf=\"Pointstransaction\">\r\n          <app-form-builder-table\r\n          [searchCallback]= \"[service, 'searchPointReportLint',this]\"\r\n          [tableFormat]=\"tableFormat\"\r\n          [table_data]=\"Pointstransaction\"\r\n          [total_page]=\"totalPage\"\r\n        >\r\n  \r\n          </app-form-builder-table>\r\n    </div>\r\n        <div *ngIf=\"pointstransactionDetail\">\r\n              <!-- <app-pointstransaction-detail [back]=\"[this,backToHere]\" [detail]=\"pointstransactionDetail\"></app-pointstransaction-detail> -->\r\n        </div>\r\n  \r\n    </div>\r\n  "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/member/detail/member.detail.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/member/detail/member.detail.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!edit\" class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToTable()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n        <h1>{{detail.username}}</h1>\r\n        <button class=\"btn edit_button\" (click)=\"editThis()\"><i class=\"fa fa-fw fa-pencil-alt\"></i> edit</button>\r\n    </div>\r\n    <div class=\"card-header\">\r\n        <a class=\"btn edit_button\"\r\n        (click)=\"pointHistory()\"\r\n         >Point History</a>\r\n    </div>\r\n  \r\n    <div class=\"card-content\">\r\n        <div class=\"member-detail\">\r\n    \r\n            <div class=\"row\">\r\n             <div class=\"col-md-4\">\r\n                 <div class=\"card mb-3\">\r\n                     <div class=\"card-header\"><h2>Data Pemilik</h2></div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">             \r\n                             <label>ID Customer</label>\r\n                             <div name=\"member_id\">{{detail.username}}</div>\r\n\r\n                             <label>Nama Pemilik</label>\r\n                             <div name=\"full_name\">{{detail.full_name}}</div>\r\n\r\n                             <!-- <label>Member Status</label>\r\n                             <div name=\"member_status\">{{detail.permission}}</div> -->\r\n\r\n                             <label>No. Telp Pemilik</label>\r\n                             <div name=\"cell_phone\">{{detail.cell_phone}}</div>\r\n\r\n                             <!-- <label>Address</label>\r\n                             <div name=\"address\">{{detail.address}}</div>\r\n\r\n                             <label>City </label>\r\n                             <div name=\"mcity\">{{detail.mcity}}</div>\r\n\r\n                             <label>PostCode </label>\r\n                             <div name=\"mpostcode\">{{detail.mpostcode}}</div>\r\n\r\n                             <label>State </label>\r\n                             <div name=\"mstate\">{{detail.mstate}}</div>\r\n\r\n                             <label>Country </label>\r\n                             <div name=\"mcountry\">{{detail.mcountry}}</div> -->\r\n\r\n                         </div>\r\n                       \r\n                      </div>\r\n                 </div>\r\n                </div>\r\n                <div  *ngFor=\"let address of detail.buyer_detail; let i=index\" class=\" col-md-4\" >\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Alamat Pengiriman {{i+1}}</h2> <span *ngIf=\"address.use_as == 'main_address'\"> (ALAMAT UTAMA)</span> </div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n                                <label>id</label>\r\n                                <div name=\"email\">{{address.id}}</div>\r\n                                <label>Nama Penerima </label>\r\n                                <div name=\"email\">{{address.name}}</div>\r\n                               <label>Alamat Pengiriman </label>\r\n                               <div name=\"email\">{{address.address}}</div>\r\n                               <label>No. Telp Penerima<div style=\"font-size: 10px;\">(Jika hadiah berupa Saldo Gopay, isi dengan No. Gopay Penerima)</div></label>\r\n                               <div name=\"email\">{{address.cell_phone}}</div>\r\n                               <!-- <label>Provinsi</label>\r\n                               <div name=\"email\">{{address.province_name}}</div>\r\n                               <label>Kota/Kabupaten </label>\r\n                               <div name=\"email\">{{address.city_name}}</div>\r\n                               <label>Kecamatan</label>\r\n                               <div name=\"email\">{{address.subdistrict_name}}</div>\r\n                               <label>Desa/Kelurahan</label>\r\n                               <div name=\"email\">{{address.village_name}}</div>                      \r\n                               <label>Kode Pos</label>\r\n                               <div name=\"email\">{{address.postal_code}}</div>                  -->\r\n                               <label>Use as main address</label>\r\n                               <div name=\"email\">{{address.use_as}}</div>           \r\n                            </div>\r\n                         </div>\r\n                    </div>\r\n                    \r\n                   </div>\r\n                <!-- <div class=\" col-md-4\" >\r\n                 <div class=\"card mb-3\">\r\n                     <div class=\"card-header\"><h2>Member &amp; Status Member</h2> </div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                             \r\n                            <label>Email </label>\r\n                            <div name=\"email\">{{detail.email}}</div>\r\n\r\n                            <label>Cell Phone </label>\r\n                            <div name=\"cell_phone\">{{detail.cell_phone}}</div>\r\n\r\n                            <label>Registration Date </label>\r\n                            <div name=\"registration_date\">{{detail.created_date}}</div>\r\n\r\n                            <label>Point Balance </label>\r\n                            <div name=\"point_balance\">{{detail.point_balance}}</div>\r\n\r\n                            <label>Activation Code </label>\r\n                            <div name=\"activation_code\">{{detail.activation_code}}</div>\r\n\r\n                            <label>Activation Status </label>\r\n                            <div name=\"activation_status\">{{detail.activation_status}}</div>\r\n                             \r\n                         </div>\r\n                       \r\n                      </div>\r\n                 </div>\r\n                 \r\n                </div> -->\r\n            </div>\r\n            <div class=\"row\">\r\n                <div class=\"col-md-12 bg-delete\"><button class=\"btn delete-button float-right btn-danger\" (click)=\"delete()\">delete this</button></div>\r\n            </div>\r\n            \r\n         </div>\r\n    </div>\r\n</div>\r\n<div *ngIf=\"edit\">\r\n    <app-member-edit [back]=\"[this, 'editThis']\" [detail]=\"detail\"></app-member-edit>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/member/edit/member.edit.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/member/edit/member.edit.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToDetail()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button><h1>{{detail.member_id}}</h1>\r\n        <button class=\"btn save_button {{loading}}\" type=\"submit\" (click)=\"saveThis()\"><i class=\"fa fa-fw fa-save\"></i> <span  *ngIf=\"!loading\">Save</span> <span *ngIf=\"loading\">loading</span></button>\r\n    </div>\r\n    <div class=\"card-content\">\r\n        <div class=\"member-detail\">\r\n    \r\n            <div class=\"row\">\r\n                <div class=\" col-md-4\" >\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Data Pemilik</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n                                <label>ID Customer</label>\r\n                                <form-input  name=\"member_id\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Member ID'\"  [(ngModel)]=\"detail.username\" autofocus required></form-input>\r\n                \r\n                                <label>Nama Pemilik</label>\r\n                                <form-input name=\"full_name\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Full Name'\"  [(ngModel)]=\"detail.full_name\" autofocus required></form-input>\r\n\r\n                                <label>No. Telp Pemilik</label>\r\n                                <form-input name=\"cell_phone\" [disabled]=\"true\" [type]=\"'text'\" [placeholder]=\"'Cell Phone'\"  [(ngModel)]=\"detail.cell_phone\" autofocus required></form-input>\r\n\r\n                                <!-- <label>Password</label>\r\n                                <form-input name=\"password\" [(ngModel)]=\"detail.password\" autofocus required></form-input> -->\r\n\r\n                                <!-- <label>Member Status</label>\r\n                                <div name=\"member_status\"> {{member_status}}\r\n                                    <form-select name=\"member_status\" [(ngModel)]=\"detail.member_status\" [data]=\"memberStatus\" required></form-select> \r\n                                </div> \r\n\r\n                                <label>Address</label>\r\n                                <form-input name=\"maddress1\" [type]=\"'textarea'\" [placeholder]=\"'Address'\"  [(ngModel)]=\"detail.maddress1\" autofocus required></form-input>\r\n\r\n                                <label>City </label>\r\n                                <form-input name=\"mcity\" [type]=\"'text'\" [placeholder]=\"'City'\"  [(ngModel)]=\"detail.mcity\" autofocus required></form-input>\r\n\r\n                                <label>Post Code </label>\r\n                                <form-input name=\"mpostcode\" [type]=\"'text'\" [placeholder]=\"'Post Code'\"  [(ngModel)]=\"detail.post_code\" autofocus required></form-input>\r\n\r\n                                <label>State </label>\r\n                                <form-input name=\"mstate\" [type]=\"'text'\" [placeholder]=\"'State'\"  [(ngModel)]=\"detail.mstate\" autofocus required></form-input>\r\n\r\n                                <label>Country </label>\r\n                                <form-input name=\"mcountry\" [type]=\"'text'\" [placeholder]=\"'Country'\"  [(ngModel)]=\"detail.mcountry\" autofocus required></form-input> -->\r\n                            </div>\r\n                        \r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div  *ngFor=\"let address of detail.buyer_detail; let i=index\" class=\" col-md-4\" >\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Alamat Pengiriman</h2> </div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n\r\n                                <label>Nama Penerima </label>\r\n                                <form-input name=\"email\" [type]=\"'text'\" [placeholder]=\"'Nama Penerima'\"  [(ngModel)]=\"address.name\" autofocus required></form-input>\r\n                                \r\n                                <label>Alamat Penerima</label>\r\n                                <form-input name=\"email\" [type]=\"'text'\" [placeholder]=\"'Alamat Penerima'\"  [(ngModel)]=\"address.address\" autofocus required></form-input>\r\n\r\n                                <label>No. Telp Penerima<div style=\"font-size: 10px;\">(Jika hadiah berupa Saldo Gopay, isi dengan No. Gopay Penerima)</div></label>\r\n                                <form-input name=\"cell_phone\" [type]=\"'text'\" [placeholder]=\"'Cell Phone Penerima'\"  [(ngModel)]=\"address.cell_phone\" autofocus required></form-input>\r\n                                \r\n                                <!-- <label>Provinsi </label>\r\n                                <form-input name=\"cell_phone\" [type]=\"'text'\" [placeholder]=\"'Provinsi'\"  [(ngModel)]=\"address.province_name\" autofocus required></form-input>\r\n                                \r\n                                <label>Kota/Kabupaten</label>\r\n                                <form-input name=\"cell_phone\" [type]=\"'text'\" [placeholder]=\"'Kota'\"  [(ngModel)]=\"address.city_name\" autofocus required></form-input>\r\n                                <label>Kecamatan </label>\r\n                                <form-input name=\"cell_phone\" [type]=\"'text'\" [placeholder]=\"'Kecamatan'\"  [(ngModel)]=\"address.subdistrict_name\" autofocus required></form-input>\r\n                                \r\n                                <label>Desa/Kelurahan</label>\r\n                                <form-input name=\"cell_phone\" [type]=\"'text'\" [placeholder]=\"'Desa/Kelurahan'\"  [(ngModel)]=\"address.village_name\" autofocus required></form-input>\r\n                                <label>Kode Pos</label>\r\n                                <form-input name=\"cell_phone\" [type]=\"'text'\" [placeholder]=\"'Kode Pos'\"  [(ngModel)]=\"address.postal_code\" autofocus required></form-input> -->\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n     \r\n         </div>\r\n    </div>\r\n    \r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/member/member.component.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/member/member.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n       \r\n  <app-page-header [heading]=\"'Member'\" [icon]=\"'fa-table'\"></app-page-header>\r\n  \r\n  <!-- <button (click)=\"getDownloadFileLint()\"><i class=\"fa fa-file-pdf-o\"></i>Download</button> -->\r\n  <!-- <iframe [src]=\"srcDownload\"></iframe> -->\r\n  <div *ngIf=\"Members&&memberDetail==false\">\r\n        <app-form-builder-table \r\n        [tableFormat] = \"tableFormat\" \r\n        [table_data]  = \"Members\" \r\n        [searchCallback]= \"[service, 'searchMemberLint',this]\" \r\n        [total_page]=\"totalPage\"\r\n        >\r\n            \r\n        </app-form-builder-table>\r\n  </div>\r\n  <div *ngIf=\"memberDetail\">\r\n    <app-member-detail [back]=\"[this,backToHere]\" [detail]=\"memberDetail\"></app-member-detail>\r\n</div>\r\n \r\n</div>\r\n\r\n "

/***/ }),

/***/ "./src/app/layout/modules/member/add/member.add.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/layout/modules/member/add/member.add.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg-delete {\n  background-color: #ffe3f3;\n  padding: 10px;\n}\n\n.ng-pristine {\n  color: white !important;\n}\n\n.ng-pristine {\n  color: black !important;\n  width: 100%;\n  border-radius: 5px;\n  padding: 6px 0px;\n}\n\nlabel {\n  color: black;\n}\n\n.card-detail > .card-header {\n  position: relative;\n  min-height: 48px;\n}\n\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 10px;\n  background-color: #3498db;\n  color: white;\n  height: 30px;\n  align-items: center;\n  display: flex;\n  right: 10px;\n  padding-top: 0px;\n  padding-right: 10px;\n  padding-bottom: 0;\n  padding-left: 10px;\n  font-weight: bold;\n}\n\n.card-detail .card-content {\n  padding: 15px;\n}\n\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail form-select[name=status] .form-group {\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: normal;\n  padding: 0px;\n  border: 1px solid #f8f9fa;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n\n.rounded-btn {\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  background: #56a4ff;\n  margin-left: 40%;\n  margin-right: 50%;\n}\n\n.rounded-btn:hover {\n  background: #0a7bff;\n  outline: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvbWVtYmVyL2FkZC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXG1lbWJlclxcYWRkXFxtZW1iZXIuYWRkLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9tZW1iZXIvYWRkL21lbWJlci5hZGQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx5QkFBQTtFQUNBLGFBQUE7QUNDSjs7QURFQTtFQUNJLHVCQUFBO0FDQ0o7O0FERUE7RUFDSSx1QkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FDQ0o7O0FERUE7RUFDSSxZQUFBO0FDQ0o7O0FERUk7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0FDQ1I7O0FEQVE7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSw2QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEseUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0FDQ1o7O0FEQVk7RUFDSSxpQkFBQTtBQ0VoQjs7QURDUTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBRUEsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQ0FaOztBREdJO0VBQ0ksYUFBQTtBQ0RSOztBREVRO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ0FaOztBRElJO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNGUjs7QURLUTtFQUNJLGtCQUFBO0FDSFo7O0FES1E7RUFDSSxnQkFBQTtBQ0haOztBREtRO0VBQ0ksbUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ0haOztBRE9RO0VBQ0ksc0JBQUE7QUNMWjs7QURNWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDSmhCOztBRFVBO0VBRVEsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0osZ0JBQUE7RUFDQSxpQkFBQTtBQ1BKOztBRFNBO0VBQ1EsbUJBQUE7RUFFQSxhQUFBO0FDUFIiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9tZW1iZXIvYWRkL21lbWJlci5hZGQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmctZGVsZXRle1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTNmMztcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbn1cclxuXHJcbi5uZy1wcmlzdGluZXtcclxuICAgIGNvbG9yIDogd2hpdGUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLm5nLXByaXN0aW5le1xyXG4gICAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIHBhZGRpbmc6IDZweCAwcHg7XHJcbn1cclxuXHJcbmxhYmVse1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG59XHJcbi5jYXJkLWRldGFpbHtcclxuICAgID4uY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIG1pbi1oZWlnaHQ6IDQ4cHg7XHJcbiAgICAgICAgLmJhY2tfYnV0dG9ue1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC00cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtMTBweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zYXZlX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDEwcHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XHJcbiAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICByaWdodDogMTBweDtcclxuICAgICAgICAgICAgLy8gbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogMDtcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAgPi5jb2wtbWQtMTJ7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLm1lbWJlci1kZXRhaWx7XHJcbiAgICAgICAgZm9ybS1zZWxlY3RbbmFtZT1zdGF0dXNdIC5mb3JtLWdyb3VwIHtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYWJlbHtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWwrZGl2e1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDpub3JtYWw7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweDtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2Y4ZjlmYTtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgICAgICAgICAgbWluLWhlaWdodDogMjdweDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICBjb2xvcjogIzY2NjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIFxyXG4gICAgICAgIC5jYXJkLWhlYWRlcnsgXHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGgye1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuLnJvdW5kZWQtYnRuIHtcclxuICAgICAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDcwcHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNzBweDtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiA0MHB4O1xyXG4gICAgICAgIHBhZGRpbmc6IDAgNDBweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjNTZhNGZmO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDQwJTtcclxuICAgIG1hcmdpbi1yaWdodDogNTAlO1xyXG4gICAgfVxyXG4ucm91bmRlZC1idG46aG92ZXIge1xyXG4gICAgICAgIGJhY2tncm91bmQ6IGRhcmtlbigjNTZBNEZGLCAxNSUpO1xyXG4gICAgICAgIC8vIGJvcmRlcjogMnB4IHNvbGlkIGJsdWU7XHJcbiAgICAgICAgb3V0bGluZTogbm9uZTtcclxuICAgIH1cclxuLy8gLmRhdGVwaWNrZXItaW5wdXR7XHJcbi8vICAgICBwb3NpdGlvbjogYWJzb2x1dGU7ICAgIFxyXG4vLyAgICAgfVxyXG4vLyAuZm9ybS1jb250cm9se1xyXG4vLyAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4vLyAgICAgdG9wOiAxMHB4O1xyXG4vLyAgICAgbGVmdDogMHB4O1xyXG4vLyB9XHJcbiIsIi5iZy1kZWxldGUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlM2YzO1xuICBwYWRkaW5nOiAxMHB4O1xufVxuXG4ubmctcHJpc3RpbmUge1xuICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbn1cblxuLm5nLXByaXN0aW5lIHtcbiAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIHBhZGRpbmc6IDZweCAwcHg7XG59XG5cbmxhYmVsIHtcbiAgY29sb3I6IGJsYWNrO1xufVxuXG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1pbi1oZWlnaHQ6IDQ4cHg7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tdG9wOiAtNHB4O1xuICBtYXJnaW4tbGVmdDogLTEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBjb2xvcjogIzU1NTtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAxMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogd2hpdGU7XG4gIGhlaWdodDogMzBweDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgZGlzcGxheTogZmxleDtcbiAgcmlnaHQ6IDEwcHg7XG4gIHBhZGRpbmctdG9wOiAwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAwO1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQge1xuICBwYWRkaW5nOiAxNXB4O1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQgPiAuY29sLW1kLTEyIHtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDBweDtcbn1cbi5jYXJkLWRldGFpbCBoMSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjNTU1O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBmb3JtLXNlbGVjdFtuYW1lPXN0YXR1c10gLmZvcm0tZ3JvdXAge1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGxhYmVsICsgZGl2IHtcbiAgYmFja2dyb3VuZDogI2YwZjBmMDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgcGFkZGluZzogMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjZjhmOWZhO1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtaW4taGVpZ2h0OiAyN3B4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNjY2O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIGgyIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGxpbmUtaGVpZ2h0OiB1bnNldDtcbiAgaGVpZ2h0OiB1bnNldDtcbiAgbWFyZ2luOiAwcHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuXG4ucm91bmRlZC1idG4ge1xuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDcwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDcwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBsaW5lLWhlaWdodDogNDBweDtcbiAgcGFkZGluZzogMCA0MHB4O1xuICBiYWNrZ3JvdW5kOiAjNTZhNGZmO1xuICBtYXJnaW4tbGVmdDogNDAlO1xuICBtYXJnaW4tcmlnaHQ6IDUwJTtcbn1cblxuLnJvdW5kZWQtYnRuOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogIzBhN2JmZjtcbiAgb3V0bGluZTogbm9uZTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/member/add/member.add.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/layout/modules/member/add/member.add.component.ts ***!
  \*******************************************************************/
/*! exports provided: MemberAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberAddComponent", function() { return MemberAddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var MemberAddComponent = /** @class */ (function () {
    function MemberAddComponent(memberService) {
        this.memberService = memberService;
        this.name = "";
        this.Members = [];
        this.form = {
            username: "",
            id: "",
            full_name: "",
            email: "",
            password: "",
            cell_phone: "",
            card_number: "",
            dob: "",
            mcity: "",
            mcountry: "",
            maddress1: "",
            mpostcode: "",
            gender: "",
            member_status: "",
            marital_status: "",
            address2: "",
        };
        this.errorLabel = false;
        this.member_status_type = [
            { label: 'Superuser', value: 'superuser' },
            { label: 'Member', value: 'member', selected: 1 },
            { label: 'Marketing', value: 'marketing' },
            { label: 'Admin', value: 'admin' },
            { label: 'Merchant', value: 'merchant' }
        ];
        this.gender_type = [
            { label: 'Famele', value: 'female' },
            { label: 'Male', value: 'male' }
        ];
        this.marital_status_type = [
            { label: 'Married', value: 'married' },
            { label: 'Unmarried', value: 'unmarried' }
        ];
        // let form_add_test     : any = [
        //   { label:"Member ID",  type: "text",  value: "", data_binding: 'id'  },
        //   { label:"Full Name",  type: "text",  value: "", data_binding: 'full_name'  },
        //   { label:"Email",  type: "text",  value: "", data_binding: 'email'  },
        //   { label:"Password",  type: "password",  value: "", data_binding: 'password'  },
        //   { label:"Cell Phone",  type: "text",  value: "", data_binding: 'cell_phone'  },
        //   { label:"Card Number ",  type: "text",  value: "", data_binding: 'card_number' },
        //   { label:"Date of Birth",  type: "text",  value: "", data_binding: 'dob' },
        //   { label:"City",  type: "text",  value: "", data_binding: 'mcity' },
        //   { label:"State",  type: "text",  value: "", data_binding: 'mstate' },
        //   { label:"Country",  type: "text",  value: "", data_binding: 'mcountry' },
        //   { label:"Address",  type: "textarea",  value: "", data_binding: 'maddress1' },
        //   { label:"Post Code",  type: "text",  value: "", data_binding: 'mpostcode' },
        //   { label:"Gender",  type: "text",  value: "", data_binding: 'gender' },
        //   { label:"Member Status",  type: "text",  value: "", data_binding: 'member_status' },
        //   { label:"Marital Status",  type: "text",  value: "", data_binding: 'marital_status' },
        //   // { label:"type ",  type: "textarea",  value: [{label:"label", value:"label", selected:1},{label:"label1", value:"label2"}], data_binding: 'address2' },
        //   { label:"address 2 ",  type: "textarea",  value: "", data_binding: 'address2' },
        // ];
    }
    MemberAddComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    MemberAddComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    MemberAddComponent.prototype.formSubmitAddMember = function () {
        // console.log(JSON.stringify(form));
        // console.log(form.password)
        var form_add = this.form;
        if (!form_add.password || form_add.password.length < 8) {
            alert("password must be longer than 8 character");
        }
        else {
            try {
                this.service = this.memberService;
                var dob = void 0;
                var dataForm = void 0;
                if (form_add.dob) {
                    var month = form_add.dob.month.toString().padStart(2, '0');
                    var day = form_add.dob.day.toString().padStart(2, '0');
                    dob = form_add.dob.year + "-" + month + "-" + day;
                    dataForm = __assign({}, form_add, { dob: dob });
                }
                else {
                    dataForm = form_add;
                }
                console.log(dataForm);
                var result = this.memberService.addNewMember(dataForm);
                this.Members = result.result;
                alert("Member Has Been Added!");
            }
            catch (e) {
                this.errorLabel = (e.message); //conversion to Error type
            }
        }
    };
    MemberAddComponent.prototype.backTo = function () {
        window.history.back();
    };
    MemberAddComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"] }
    ]; };
    MemberAddComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-member-add',
            template: __webpack_require__(/*! raw-loader!./member.add.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/member/add/member.add.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./member.add.component.scss */ "./src/app/layout/modules/member/add/member.add.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"]])
    ], MemberAddComponent);
    return MemberAddComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/member/detail/member-point-history/member-point-history.component.scss":
/*!*******************************************************************************************************!*\
  !*** ./src/app/layout/modules/member/detail/member-point-history/member-point-history.component.scss ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL21lbWJlci9kZXRhaWwvbWVtYmVyLXBvaW50LWhpc3RvcnkvbWVtYmVyLXBvaW50LWhpc3RvcnkuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/layout/modules/member/detail/member-point-history/member-point-history.component.ts":
/*!*****************************************************************************************************!*\
  !*** ./src/app/layout/modules/member/detail/member-point-history/member-point-history.component.ts ***!
  \*****************************************************************************************************/
/*! exports provided: MemberPointHistoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberPointHistoryComponent", function() { return MemberPointHistoryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_points_transaction_points_transaction_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../services/points-transaction/points-transaction.service */ "./src/app/services/points-transaction/points-transaction.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var MemberPointHistoryComponent = /** @class */ (function () {
    function MemberPointHistoryComponent(PointService, _Activatedroute, route) {
        this.PointService = PointService;
        this._Activatedroute = _Activatedroute;
        this.route = route;
        this.Pointstransaction = [];
        this.tableFormat = {
            title: 'Point History Report ',
            label_headers: [
                { label: 'Updated Date', visible: true, type: 'string', data_row_name: 'updated_date' },
                { label: 'ID Customer', visible: true, type: 'string', data_row_name: 'username' },
                // {label: 'Reference No', visible: true, type: 'string', data_row_name: 'reference_no'},
                { label: 'Nama Pemilik', visible: true, type: 'string', data_row_name: 'nama_pemilik' },
                { label: 'Prev Points Balance', visible: true, type: 'string', data_row_name: 'prev_points_balance' },
                { label: 'Points', visible: true, type: 'string', data_row_name: 'points' },
                { label: 'Current Points Balance', visible: true, type: 'string', data_row_name: 'current_points_balance' },
                { label: 'Description', visible: true, type: 'string', data_row_name: 'description' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'Pointstransaction',
                detail_function: []
            }
        };
        this.form_input = {};
        this.errorLabel = false;
        this.totalPage = 0;
        this.pointstransactionDetail = false;
    }
    MemberPointHistoryComponent.prototype.ngOnInit = function () {
        this.id = this._Activatedroute.snapshot.params['id'];
        this.firstLoad();
    };
    MemberPointHistoryComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.route.queryParams.subscribe(function (params) { return __awaiter(_this, void 0, void 0, function () {
                    var id, params1, result, e_1;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                this.tableFormat.title += '( user : ' + params.id + ' )';
                                id = params.id;
                                _a.label = 1;
                            case 1:
                                _a.trys.push([1, 3, , 4]);
                                params1 = {
                                    search: { start_date: this.addMonths(new Date(), -6), end_date: new Date(), member_email: id, },
                                    order_by: {},
                                    current_page: 1,
                                    limit_per_page: 50
                                };
                                // {"search":{"start_date":"2019-01-10T08:19:13.799Z","end_date":"2020-08-13T08:19:13.799Z","member_email":"sri.zetka@gmail.com"},"order_by":{},"limit_per_page":null,"current_page":1,"download":false}
                                this.service = this.PointService;
                                return [4 /*yield*/, this.PointService.getPointReportLint(params1)];
                            case 2:
                                result = _a.sent();
                                this.totalPage = result.result.total_page;
                                this.Pointstransaction = result.result.values;
                                return [3 /*break*/, 4];
                            case 3:
                                e_1 = _a.sent();
                                this.errorLabel = (e_1.message); //conversion to Error type
                                return [3 /*break*/, 4];
                            case 4: return [2 /*return*/];
                        }
                    });
                }); });
                console.log(' admin payment', this.id);
                return [2 /*return*/];
            });
        });
    };
    // public async callDetail(pointstransaction_id){
    //   try{
    //     let result: any;
    //     this.service    = this.OrderhistoryService;
    //     result          = await this.OrderhistoryService.detailPointstransaction(pointstransaction_id);
    //     console.log(result);
    //     this.pointstransactionDetail = result.result[0];
    //   } catch (e) {
    //     this.errorLabel = ((<Error>e).message);//conversion to Error type
    //   }
    // }
    MemberPointHistoryComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.pointstransactionDetail = false;
                return [2 /*return*/];
            });
        });
    };
    MemberPointHistoryComponent.prototype.addMonths = function (date, months) {
        date.setMonth(date.getMonth() + months);
        return date;
    };
    MemberPointHistoryComponent.ctorParameters = function () { return [
        { type: _services_points_transaction_points_transaction_service__WEBPACK_IMPORTED_MODULE_2__["PointsTransactionService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"] }
    ]; };
    MemberPointHistoryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-member-point-history',
            template: __webpack_require__(/*! raw-loader!./member-point-history.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/member/detail/member-point-history/member-point-history.component.html"),
            styles: [__webpack_require__(/*! ./member-point-history.component.scss */ "./src/app/layout/modules/member/detail/member-point-history/member-point-history.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_points_transaction_points_transaction_service__WEBPACK_IMPORTED_MODULE_2__["PointsTransactionService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], MemberPointHistoryComponent);
    return MemberPointHistoryComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/member/detail/member.detail.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/layout/modules/member/detail/member.detail.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg-delete {\n  background-color: #ffe3f3;\n  padding: 10px;\n}\n\n.card-detail > .card-header {\n  position: relative;\n}\n\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .edit_button {\n  position: absolute;\n  top: 7px;\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n\n.card-detail > .card-header .edit_button i {\n  font-weight: bold;\n}\n\n.card-detail .card-content {\n  padding: 15px;\n}\n\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail form-select[name=status] .form-group {\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvbWVtYmVyL2RldGFpbC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXG1lbWJlclxcZGV0YWlsXFxtZW1iZXIuZGV0YWlsLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9tZW1iZXIvZGV0YWlsL21lbWJlci5kZXRhaWwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx5QkFBQTtFQUNBLGFBQUE7QUNDSjs7QURFSTtFQUNJLGtCQUFBO0FDQ1I7O0FEQVE7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSw2QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEseUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0FDQ1o7O0FEQVk7RUFDSSxpQkFBQTtBQ0VoQjs7QURDUTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLHNCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSwwQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBS0EsV0FBQTtBQ0paOztBRENZO0VBQ0ksaUJBQUE7QUNDaEI7O0FESUk7RUFDSSxhQUFBO0FDRlI7O0FER1E7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0FDRFo7O0FEZ0JJO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNkUjs7QURpQlE7RUFDSSxrQkFBQTtBQ2ZaOztBRGlCUTtFQUNJLGdCQUFBO0FDZlo7O0FEaUJRO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ2ZaOztBRGtCUTtFQUNJLHNCQUFBO0FDaEJaOztBRGlCWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDZmhCIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvbWVtYmVyL2RldGFpbC9tZW1iZXIuZGV0YWlsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJnLWRlbGV0ZXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmUzZjM7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG59XHJcbi5jYXJkLWRldGFpbHtcclxuICAgID4uY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIC5iYWNrX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAtNHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogLTEwcHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAuZWRpdF9idXR0b257XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgdG9wOiA3cHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByaWdodDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAgPi5jb2wtbWQtMTJ7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLy8gLmltYWdle1xyXG4gICAgLy8gICAgIGgze1xyXG4gICAgLy8gICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAvLyAgICAgfVxyXG4gICAgLy8gICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAvLyAgICAgICAgIGltZ3tcclxuICAgIC8vICAgICAgICAgICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgIC8vICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG5cclxuICAgIC8vICAgICAgICAgfVxyXG4gICAgLy8gICAgIH1cclxuICAgIC8vIH1cclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLm1lbWJlci1kZXRhaWx7XHJcbiAgICAgICAgZm9ybS1zZWxlY3RbbmFtZT1zdGF0dXNdIC5mb3JtLWdyb3VwIHtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYWJlbHtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWwrZGl2e1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDpub3JtYTtcclxuICAgICAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgICAgICAgICAgbWluLWhlaWdodDogMjdweDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICBjb2xvcjogIzY2NjtcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgLmNhcmQtaGVhZGVyeyBcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaDJ7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufSIsIi5iZy1kZWxldGUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlM2YzO1xuICBwYWRkaW5nOiAxMHB4O1xufVxuXG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tdG9wOiAtNHB4O1xuICBtYXJnaW4tbGVmdDogLTEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBjb2xvcjogIzU1NTtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5lZGl0X2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA3cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG4gIGNvbG9yOiAjZmZmO1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcmlnaHQ6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmVkaXRfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogMTVweDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50ID4gLmNvbC1tZC0xMiB7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgaDEge1xuICBmb250LXNpemU6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzU1NTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgZm9ybS1zZWxlY3RbbmFtZT1zdGF0dXNdIC5mb3JtLWdyb3VwIHtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGxhYmVsIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBsYWJlbCArIGRpdiB7XG4gIGJhY2tncm91bmQ6ICNmMGYwZjA7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYTtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgbWluLWhlaWdodDogMjdweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzY2Njtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciBoMiB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBsaW5lLWhlaWdodDogdW5zZXQ7XG4gIGhlaWdodDogdW5zZXQ7XG4gIG1hcmdpbjogMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/member/detail/member.detail.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/layout/modules/member/detail/member.detail.component.ts ***!
  \*************************************************************************/
/*! exports provided: MemberDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberDetailComponent", function() { return MemberDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var MemberDetailComponent = /** @class */ (function () {
    function MemberDetailComponent(memberService, router, route) {
        this.memberService = memberService;
        this.router = router;
        this.route = route;
        this.edit = false;
        this.errorLabel = false;
    }
    MemberDetailComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    MemberDetailComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    MemberDetailComponent.prototype.editThis = function () {
        // console.log(this.edit );
        this.edit = !this.edit;
        // console.log(this.edit );
    };
    MemberDetailComponent.prototype.backToTable = function () {
        // console.log(this.back);
        this.back[1](this.back[0]);
    };
    MemberDetailComponent.prototype.delete = function () {
        return __awaiter(this, void 0, void 0, function () {
            var delResult, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.memberService.deleteMember(this.detail)];
                    case 1:
                        delResult = _a.sent();
                        console.log(delResult);
                        if (delResult.error == false) {
                            console.log(this.back[0]);
                            this.back[0].memberDetail = false;
                            this.back[0].firstLoad();
                            // delete this.back[0].prodDetail;
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        throw new TypeError(error_1.error.error);
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MemberDetailComponent.prototype.catch = function (e) {
        this.errorLabel = (e.message); //conversion to Error type
    };
    MemberDetailComponent.prototype.pointHistory = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.log('here');
                // this.router.navigate(['/member-point-history']);
                this.router.navigate(['administrator/memberadmin/point-history'], { queryParams: { id: this.detail.email } });
                return [2 /*return*/];
            });
        });
    };
    MemberDetailComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], MemberDetailComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], MemberDetailComponent.prototype, "back", void 0);
    MemberDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-member-detail',
            template: __webpack_require__(/*! raw-loader!./member.detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/member/detail/member.detail.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./member.detail.component.scss */ "./src/app/layout/modules/member/detail/member.detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], MemberDetailComponent);
    return MemberDetailComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/member/edit/member.edit.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/layout/modules/member/edit/member.edit.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n}\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 7px;\n  background-color: #3498db;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n.card-detail > .card-header .save_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .save_button.true {\n  background-color: #e67e22;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .member-detail .image {\n  overflow: hidden;\n}\n.card-detail .member-detail .image > div {\n  width: 33.3333%;\n  padding: 5px;\n  float: left;\n}\n.card-detail .member-detail .image .card-header {\n  background-color: #f0f0f0;\n}\n.card-detail .member-detail .image h3 {\n  font-size: 12px;\n  margin-bottom: 0px;\n  color: #555;\n}\n.card-detail .member-detail .image .card-content {\n  overflow: hidden;\n  width: 100%;\n  height: auto;\n}\n.card-detail .member-detail .image .card-content img {\n  max-width: 100%;\n  width: 100%;\n}\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvbWVtYmVyL2VkaXQvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxtZW1iZXJcXGVkaXRcXG1lbWJlci5lZGl0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9tZW1iZXIvZWRpdC9tZW1iZXIuZWRpdC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLGtCQUFBO0FDQVI7QURDUTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLDZCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSx5QkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7QUNBWjtBRENZO0VBQ0ksaUJBQUE7QUNDaEI7QURFUTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSwwQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBS0EsV0FBQTtBQ0xaO0FERVk7RUFDSSxpQkFBQTtBQ0FoQjtBRElRO0VBQ0kseUJBQUE7QUNGWjtBREtJO0VBQ0ksYUFBQTtBQ0hSO0FES1E7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0FDSFo7QURPSTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDTFI7QURTUTtFQUNJLGdCQUFBO0FDUFo7QURTUTtFQUNJLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUNQWjtBRFNRO0VBQ0ksZ0JBQUE7QUNQWjtBRFFZO0VBQ0ksZUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FDTmhCO0FEUVk7RUFDSSx5QkFBQTtBQ05oQjtBRFFZO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQ05oQjtBRFFZO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQ05oQjtBRE9nQjtFQUNJLGVBQUE7RUFDQSxXQUFBO0FDTHBCO0FEV1E7RUFDSSxzQkFBQTtBQ1RaO0FEVVk7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSwwQkFBQTtBQ1JoQiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL21lbWJlci9lZGl0L21lbWJlci5lZGl0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNhcmQtZGV0YWlse1xyXG4gICAgPi5jYXJkLWhlYWRlcntcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgLmJhY2tfYnV0dG9ue1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC00cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtMTBweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zYXZlX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDdweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICBcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJpZ2h0OiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuc2F2ZV9idXR0b24udHJ1ZXtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2U2N2UyMjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICBcclxuICAgICAgICA+LmNvbC1tZC0xMntcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBcclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLm1lbWJlci1kZXRhaWx7XHJcbiAgICAgICBcclxuICAgICAgICBsYWJlbHtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWwrZGl2e1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDpub3JtYTtcclxuICAgICAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgICAgICAgICAgbWluLWhlaWdodDogMjdweDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICBjb2xvcjogIzY2NjtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmltYWdle1xyXG4gICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICA+ZGl2e1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDMzLjMzMzMlO1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICAgICAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLmNhcmQtaGVhZGVye1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBoM3tcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgICAgICAgICAgICBpbWd7XHJcbiAgICAgICAgICAgICAgICAgICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5jYXJkLWhlYWRlcnsgXHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGgye1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0iLCIuY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tdG9wOiAtNHB4O1xuICBtYXJnaW4tbGVmdDogLTEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBjb2xvcjogIzU1NTtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5zYXZlX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA3cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XG4gIGNvbG9yOiAjZmZmO1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcmlnaHQ6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLnNhdmVfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuc2F2ZV9idXR0b24udHJ1ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlNjdlMjI7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCB7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCA+IC5jb2wtbWQtMTIge1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuLmNhcmQtZGV0YWlsIGgxIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM1NTU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGxhYmVsIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBsYWJlbCArIGRpdiB7XG4gIGJhY2tncm91bmQ6ICNmMGYwZjA7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYTtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgbWluLWhlaWdodDogMjdweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzY2Njtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2Uge1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSA+IGRpdiB7XG4gIHdpZHRoOiAzMy4zMzMzJTtcbiAgcGFkZGluZzogNXB4O1xuICBmbG9hdDogbGVmdDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2UgLmNhcmQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuaW1hZ2UgaDMge1xuICBmb250LXNpemU6IDEycHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmltYWdlIC5jYXJkLWNvbnRlbnQge1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5pbWFnZSAuY2FyZC1jb250ZW50IGltZyB7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuY2FyZC1oZWFkZXIgaDIge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbGluZS1oZWlnaHQ6IHVuc2V0O1xuICBoZWlnaHQ6IHVuc2V0O1xuICBtYXJnaW46IDBweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/member/edit/member.edit.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/layout/modules/member/edit/member.edit.component.ts ***!
  \*********************************************************************/
/*! exports provided: MemberEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberEditComponent", function() { return MemberEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var MemberEditComponent = /** @class */ (function () {
    function MemberEditComponent(memberService) {
        this.memberService = memberService;
        this.errorLabel = false;
        this.loading = false;
        this.activationStatus = [
            { label: 'ACTIVATED', value: 'ACTIVATED' },
            { label: 'INACTIVED', value: 'INACTIVED' }
        ];
        this.memberStatus = [
            { label: 'ACTIVE', value: 'ACTIVE' },
            { label: 'INACTIVE', value: 'INACTIVE' }
        ];
    }
    MemberEditComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    MemberEditComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                console.log(this.detail);
                this.detail.previous_member_id = this.detail.member_id;
                this.activationStatus.forEach(function (element, index, products_ids) {
                    if (element.value == _this.detail.activation_status) {
                        _this.activationStatus[index].selected = 1;
                    }
                });
                this.detail.previous_member_id = this.detail.member_id;
                this.memberStatus.forEach(function (element, index, products_ids) {
                    if (element.value == _this.detail.member_status) {
                        _this.memberStatus[index].selected = 1;
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    MemberEditComponent.prototype.backToDetail = function () {
        // console.log(this.back);
        this.back[0][this.back[1]]();
    };
    MemberEditComponent.prototype.saveThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var details, new_form, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("data", this.detail);
                        details = this.detail.buyer_detail[0];
                        new_form = {
                            username: this.detail.username,
                            address: {
                                id: details.id,
                                name: details.name,
                                address: details.address,
                                cell_phone: details.cell_phone,
                                province_name: details.province_name,
                                city_name: details.city_name,
                                subdistrict_name: details.subdistrict_name,
                                postal_code: details.postal_code,
                                email: details.email,
                                use_as: details.use_as,
                                village_name: details.village_name
                            }
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        // this.loading = !this.loading;
                        return [4 /*yield*/, this.memberService.updateMemberAddress(new_form)];
                    case 2:
                        // this.loading = !this.loading;
                        _a.sent();
                        alert("Edit berhasil");
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        alert("Error");
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MemberEditComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], MemberEditComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], MemberEditComponent.prototype, "back", void 0);
    MemberEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-member-edit',
            template: __webpack_require__(/*! raw-loader!./member.edit.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/member/edit/member.edit.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./member.edit.component.scss */ "./src/app/layout/modules/member/edit/member.edit.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"]])
    ], MemberEditComponent);
    return MemberEditComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/member/member-routing.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/layout/modules/member/member-routing.module.ts ***!
  \****************************************************************/
/*! exports provided: MemberRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberRoutingModule", function() { return MemberRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _member_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./member.component */ "./src/app/layout/modules/member/member.component.ts");
/* harmony import */ var _add_member_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/member.add.component */ "./src/app/layout/modules/member/add/member.add.component.ts");
/* harmony import */ var _detail_member_detail_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./detail/member.detail.component */ "./src/app/layout/modules/member/detail/member.detail.component.ts");
/* harmony import */ var _detail_member_point_history_member_point_history_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./detail/member-point-history/member-point-history.component */ "./src/app/layout/modules/member/detail/member-point-history/member-point-history.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '', component: _member_component__WEBPACK_IMPORTED_MODULE_2__["MemberComponent"],
    },
    {
        path: 'add', component: _add_member_add_component__WEBPACK_IMPORTED_MODULE_3__["MemberAddComponent"]
    },
    {
        path: 'detail', component: _detail_member_detail_component__WEBPACK_IMPORTED_MODULE_4__["MemberDetailComponent"]
    },
    // { path: 'detail', loadChildren: () => import(`./detail/member.detail-routing.module`).then(m => m.MemberDetailRoutingModule) },
    {
        path: 'point-history', component: _detail_member_point_history_member_point_history_component__WEBPACK_IMPORTED_MODULE_5__["MemberPointHistoryComponent"]
    }
];
var MemberRoutingModule = /** @class */ (function () {
    function MemberRoutingModule() {
    }
    MemberRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], MemberRoutingModule);
    return MemberRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/member/member.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/layout/modules/member/member.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "iframe {\n  width: 0px;\n  height: 0px;\n  border: 0px medium none;\n  visibility: hidden;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvbWVtYmVyL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcbWVtYmVyXFxtZW1iZXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL21lbWJlci9tZW1iZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxVQUFBO0VBQ0EsV0FBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL21lbWJlci9tZW1iZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpZnJhbWV7XHJcbiAgICB3aWR0aDowcHg7XHJcbiAgICBoZWlnaHQ6IDBweDtcclxuICAgIGJvcmRlcjowcHggbWVkaXVtIG5vbmU7XHJcbiAgICB2aXNpYmlsaXR5OiBoaWRkZW47XHJcbn0iLCJpZnJhbWUge1xuICB3aWR0aDogMHB4O1xuICBoZWlnaHQ6IDBweDtcbiAgYm9yZGVyOiAwcHggbWVkaXVtIG5vbmU7XG4gIHZpc2liaWxpdHk6IGhpZGRlbjtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/member/member.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/layout/modules/member/member.component.ts ***!
  \***********************************************************/
/*! exports provided: MemberComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberComponent", function() { return MemberComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/member/member.service */ "./src/app/services/member/member.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




// import { Http, Response, Headers, RequestOptions, RequestMethod, RequestOptionsArgs, URLSearchParams, ResponseContentType } from '@angular/http';
// import { Observable } from 'rxjs';
// import { map } from "rxjs/operators";
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';
// import 'rxjs/Rx' ;
var MemberComponent = /** @class */ (function () {
    // private options = new RequestOptions(
    //   { headers: new Headers({ 'Content-Type': 'application/json' }) });
    function MemberComponent(memberService, sanitizer) {
        this.memberService = memberService;
        this.sanitizer = sanitizer;
        this.Members = [];
        this.row_id = "_id";
        this.tableFormat = {
            title: 'Active Members Detail',
            label_headers: [
                { label: 'ID Customer', visible: true, type: 'string', data_row_name: 'username' },
                { label: 'Name', visible: true, type: 'string', data_row_name: 'full_name' },
                // {label: 'Email',     visible: true, type: 'string', data_row_name: 'email'},
                // {label: 'Birthdate',     visible: true, type: 'date', data_row_name: 'dob'},
                { label: 'Phone Number', visible: true, type: 'string', data_row_name: 'cell_phone' },
                // {label: 'Gender',     visible: true, type: 'string', data_row_name: 'gender'},
                // {label: 'Address',     visible: true, type: 'string', data_row_name: 'address'},
                // {label: 'Membership',     visible: true, type: 'string', data_row_name: 'membership'},
                { label: 'Points', visible: true, type: 'string', data_row_name: 'points' },
                { label: 'Join Date', visible: true, type: 'date', data_row_name: 'created_date' },
            ],
            row_primary_key: '_id',
            formOptions: {
                addForm: true,
                row_id: this.row_id,
                this: this,
                result_var_name: 'Members',
                detail_function: [this, 'callDetail']
            }
        };
        this.form_input = {};
        this.errorLabel = false;
        this.memberDetail = false;
        this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
    }
    MemberComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    MemberComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        // let params = {
                        //   'type':'member'
                        // }
                        this.service = this.memberService;
                        return [4 /*yield*/, this.memberService.getMemberLint()];
                    case 1:
                        result = _a.sent();
                        this.totalPage = result.result.total_page;
                        this.Members = result.result.values;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MemberComponent.prototype.callDetail = function (_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.memberService;
                        return [4 /*yield*/, this.memberService.detailMember(_id)];
                    case 1:
                        result = _a.sent();
                        this.memberDetail = result.result[0];
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MemberComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.memberDetail = false;
                return [2 /*return*/];
            });
        });
    };
    MemberComponent.prototype.onDownload = function (downloadLint) {
        var srcDownload = downloadLint;
        //let srcDownload = "http://localhost:8888/f3/assets/csv/members_report.csv";
        this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl(srcDownload);
        //console.log(this.srcDownload);
        this.getDownloadFileLint();
    };
    MemberComponent.prototype.getDownloadFileLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, downloadLint, e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        result = void 0;
                        this.service = this.memberService;
                        return [4 /*yield*/, this.memberService.getDownloadFileLint()];
                    case 1:
                        result = _a.sent();
                        downloadLint = result.result;
                        //console.log(downloadLint);
                        //To running other subfunction with together automatically
                        this.onDownload(downloadLint);
                        return [3 /*break*/, 3];
                    case 2:
                        e_3 = _a.sent();
                        this.errorLabel = (e_3.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MemberComponent.ctorParameters = function () { return [
        { type: _services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"] },
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"] }
    ]; };
    MemberComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-member',
            template: __webpack_require__(/*! raw-loader!./member.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/member/member.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./member.component.scss */ "./src/app/layout/modules/member/member.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_member_member_service__WEBPACK_IMPORTED_MODULE_2__["MemberService"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"]])
    ], MemberComponent);
    return MemberComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/member/member.module.ts":
/*!********************************************************!*\
  !*** ./src/app/layout/modules/member/member.module.ts ***!
  \********************************************************/
/*! exports provided: MemberModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberModule", function() { return MemberModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _member_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./member.component */ "./src/app/layout/modules/member/member.component.ts");
/* harmony import */ var _add_member_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/member.add.component */ "./src/app/layout/modules/member/add/member.add.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _member_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./member-routing.module */ "./src/app/layout/modules/member/member-routing.module.ts");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var _detail_member_detail_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./detail/member.detail.component */ "./src/app/layout/modules/member/detail/member.detail.component.ts");
/* harmony import */ var _edit_member_edit_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./edit/member.edit.component */ "./src/app/layout/modules/member/edit/member.edit.component.ts");
/* harmony import */ var _detail_member_point_history_member_point_history_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./detail/member-point-history/member-point-history.component */ "./src/app/layout/modules/member/detail/member-point-history/member-point-history.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var MemberModule = /** @class */ (function () {
    function MemberModule() {
    }
    MemberModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _member_routing_module__WEBPACK_IMPORTED_MODULE_8__["MemberRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_7__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_9__["BsComponentModule"]
            ],
            declarations: [
                _member_component__WEBPACK_IMPORTED_MODULE_2__["MemberComponent"], _add_member_add_component__WEBPACK_IMPORTED_MODULE_3__["MemberAddComponent"], _detail_member_detail_component__WEBPACK_IMPORTED_MODULE_10__["MemberDetailComponent"], _edit_member_edit_component__WEBPACK_IMPORTED_MODULE_11__["MemberEditComponent"], _detail_member_point_history_member_point_history_component__WEBPACK_IMPORTED_MODULE_12__["MemberPointHistoryComponent"]
            ],
        })
    ], MemberModule);
    return MemberModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-member-member-module.js.map