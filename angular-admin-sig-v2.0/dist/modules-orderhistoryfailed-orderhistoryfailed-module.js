(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-orderhistoryfailed-orderhistoryfailed-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/orderhistoryfailed/detail/orderhistoryfailed.detail.component.html":
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/orderhistoryfailed/detail/orderhistoryfailed.detail.component.html ***!
  \*****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n    <app-page-header [heading]=\"'Order Detail for '+ orderhistoryDetail.order_id\" [icon]=\"'fa-table'\"></app-page-header>\r\n    <div *ngIf=\"orderhistoryDetail\">\r\n        <app-orderhistoryfailed-edit [back]=\"[this, 'editThis']\" [detail]=\"orderhistoryDetail\">\r\n        </app-orderhistoryfailed-edit>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/orderhistoryfailed/edit/orderhistoryfailed.edit.component.html":
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/orderhistoryfailed/edit/orderhistoryfailed.edit.component.html ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToDetail()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n        <button class=\"btn save_button {{loading}}\" type=\"submit\" (click)=\"saveThis()\"><i class=\"fa fa-fw fa-save\"></i>\r\n            <span *ngIf=\"!loading\">Save</span>\r\n            <span *ngIf=\"loading\">Loading</span>\r\n        </button>\r\n    </div>\r\n    <div class=\"card-content\">\r\n        <div class=\"orderhistoryallhistory-detail\">\r\n\r\n            <div class=\"row\">\r\n                <div *ngIf=\"errorLabel!==false\">\r\n                    {{errorLabel}}\r\n                </div>\r\n                <div class=\" col-md-8\">\r\n                    <div class=\"card mb-3\">\r\n\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n                                <div class=\"form-group head-one\">\r\n                                    <label class=\"order-id\"><strong> {{detail.order_id}}</strong> <br />\r\n                                        <span class=\"buyer-name\"><strong>\r\n                                                {{detail.buyer_detail.name}}</strong></span></label>\r\n                                </div>\r\n                                <div class=\"row\">\r\n                                    <div class=\"form-group col-md-6\">\r\n                                        <label>Order at : <strong>{{detail.created_date}}</strong></label>\r\n                                    </div>\r\n\r\n                                    <div class=\"form-group col-md-6\">\r\n                                        <label>User ID : {{detail._id}} </label>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"product-group\">\r\n                                    <table>\r\n                                        <tr>\r\n                                            <th>Product Name</th>\r\n                                            <th>qty</th>\r\n                                            <th>@price</th>\r\n                                            <th>price</th>\r\n                                        </tr>\r\n                                        <tr *ngFor=\"let product of detail.products\">\r\n                                            <td>{{product.product_name}} <br />\r\n                                                <em><strong>note</strong>: {{product.note_to_merchant}}</em></td>\r\n                                            <td class=\"qty\">{{product.quantity}}x</td>\r\n                                            <td class=\"price\">{{product.price | currency: 'Rp '}}</td>\r\n                                            <td class=\"price\">{{product.fixed_price | currency: 'Rp '}}</td>\r\n                                        </tr>\r\n                                        <tr *ngFor=\"let bd of detail.additionalBillings\">\r\n                                            <td>{{bd.product_name}}</td>\r\n                                            <td></td>\r\n                                            <td></td>\r\n                                            <td class=\"price\">{{bd.value  | currency: 'Rp '}}</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td colspan=\"3\">Total Price</td>\r\n                                            <td class=\"price\">{{detail.billings.sum_total| currency: 'Rp '}}</td>\r\n                                        </tr>\r\n                                    </table>\r\n\r\n                                </div>\r\n                                <div class=\"buyer-detail\">\r\n                                    <div class=\"form-group\">\r\n                                        <label id=\"buyer-detail\">Buyer Detail</label>\r\n                                        <div class=\"buyer-name form-control-plaintext\">{{detail.buyer_detail.name}}\r\n                                        </div>\r\n                                        <div name=\"address form-control-plaintext\">{{detail.buyer_detail.address}}</div>\r\n                                        <div name=\"address form-control-plaintext\">{{detail.buyer_detail.cell_phone}}\r\n                                        </div>\r\n                                        <div name=\"address form-control-plaintext\">{{detail.buyer_detail.email}}</div>\r\n                                    </div>\r\n\r\n                                </div>\r\n\r\n\r\n                            </div>\r\n                            <div *ngIf=\"errorLabel!==false\">\r\n                                {{errorLabel}}\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\">\r\n                            <h2>Payment Info</h2>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                            <label>Transaction Status</label>\r\n                            <div *ngIf=\"!merchantMode\" name=\"transaction_status\">\r\n                                <select name=\"transaction_status\" [(ngModel)]=\"detail.status\">\r\n                                    <option *ngFor=\"let t of transactionStatus\" [ngValue]=\"t.value\">{{t.label}}\r\n                                    </option>\r\n                                </select>\r\n                            </div>\r\n                            <div *ngIf=\"merchantMode\" name=\"transaction_status\">\r\n                                <div class=\"\">\r\n                                    {{detail.status}}\r\n                                </div>\r\n                            </div>\r\n                            <br />\r\n                            <table>\r\n                                <tr>\r\n                                    <th>Service</th>\r\n                                    <th>Via</th>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>{{detail.payment_detail.service}}</td>\r\n                                    <td>{{detail.payment_detail.payment_via}}</td>\r\n                                </tr>\r\n                            </table>\r\n\r\n                            <div *ngIf=\"detail.status.toLowerCase() != 'paid'\" class=\"form-group\">\r\n                                <label>Expired Payment date</label>\r\n                                <div name=\"payment_expire_date\">{{detail.payment_expire_date.in_string}}</div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\">\r\n                            <h2>Setup Shipping Information </h2>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\" *ngIf=\"!detail.shipping_info\">\r\n                                no shipping info\r\n                            </div>\r\n                            <div class=\"col-md-12\" *ngIf=\"detail.shipping_info\">\r\n                                <div class=\"form-group\">\r\n                                    <label>delivery services</label>\r\n                                    <select class=\"form-control\" [(ngModel)]=\"detail.shipping_services\">\r\n\r\n                                        <option *ngFor=\"let ss of shippingServices\" [ngValue]=\"ss.value\">\r\n                                            {{ss.label}}</option>\r\n                                    </select>\r\n                                </div>\r\n\r\n\r\n                                <div class=\"form-group\" *ngIf=\"detail.shipping_services!='no-services'\">\r\n                                    <div class=\"shipping-status\" *ngFor=\"let sSL of shippingStatusList; let i = index\">\r\n                                        <!-- <input type=\"checkbox\" [(ngModel)]=\"sSL.checked\" id=\"{{sSL.id}}\" /> -->\r\n                                        <div class=\"radio-button checked-{{sSL.checked}} hovered-{{sSL.hovered}}\"\r\n                                            (click)=\"changeshippingStatusList(i)\"\r\n                                            (mouseenter)=\"hoverShippingStatusList(i,true)\"\r\n                                            (mouseleave)=\"hoverShippingStatusList(i, false)\">\r\n                                        </div>\r\n                                        <label (click)=\"changeshippingStatusList(i, true)\"\r\n                                            (mouseenter)=\"hoverShippingStatusList(i,true)\"\r\n                                            (mouseleave)=\"hoverShippingStatusList(i, false)\">\r\n                                            {{sSL.label}}<br />\r\n                                            <span class=\"shipping-date\"\r\n                                                *ngIf=\"sSL.created_date\">{{sSL.created_date}}</span>\r\n                                            <div *ngIf=\"sSL.value=='on delivery'\" class=\"awb-list\">\r\n                                                <input\r\n                                                    *ngIf=\"detail.shipping_services!='no-services' && shippingStatusList[2].checked == true\"\r\n                                                    name=\"awb_receipt\" class=\"form-control awb\" [type]=\"'text'\"\r\n                                                    [placeholder]=\"'Put AWB / receipt Number'\"\r\n                                                    [(ngModel)]=\"detail.awb_receipt\" autofocus required />\r\n                                                <input\r\n                                                    *ngIf=\"detail.shipping_services=='no-services' || shippingStatusList[2].checked == false \"\r\n                                                    class=\"form-control awb\" disabled name=\"unnamed\"\r\n                                                    [placeholder]=\"'Put AWB / receipt Number'\"\r\n                                                    title=\"You can add AWB after choosing the services\" />\r\n                                            </div>\r\n                                        </label>\r\n\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"form-group\" *ngIf=\"detail.shipping_services=='no-services'\">\r\n                                    <div class=\"shipping-status\" *ngFor=\"let sSL of shippingStatusList; let i = index\">\r\n                                        <!-- <input type=\"checkbox\" [(ngModel)]=\"sSL.checked\" id=\"{{sSL.id}}\" /> -->\r\n                                        <div class=\"radio-button checked-false hovered-false\">\r\n                                        </div>\r\n                                        <label>\r\n                                            {{sSL.label}}<br />\r\n                                            <span class=\"shipping-date\"\r\n                                                *ngIf=\"sSL.created_date\">{{sSL.created_date}}</span>\r\n                                        </label>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n                    <!-- <div class=\"card mb-3\">\r\n                     \r\n                     <div class=\"card-header\"><h2>Member &amp; Status Member</h2> </div>\r\n                     <div class=\"card-content\">\r\n                         <div class=\"col-md-12\">\r\n                             \r\n                            <label>Email </label>\r\n                            <form-input name=\"email\" [type]=\"'text'\" [placeholder]=\"'Email'\"  [(ngModel)]=\"detail.email\" autofocus required></form-input>\r\n\r\n                            <label>Cell Phone </label>\r\n                            <form-input name=\"cell_phone\" [type]=\"'text'\" [placeholder]=\"'Cell Phone'\"  [(ngModel)]=\"detail.cell_phone\" autofocus required></form-input>\r\n                            \r\n                             \r\n                         </div>\r\n                       \r\n                      </div>\r\n                 </div>  -->\r\n\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n    </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/orderhistoryfailed/orderhistoryfailed.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/orderhistoryfailed/orderhistoryfailed.component.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n  <app-page-header [heading]=\"'Order History Failed'\" [icon]=\"'fa-table'\"></app-page-header>\r\n  <div *ngIf=\"Orderhistoryfailed&&orderhistoryfailedDetail==false\">\r\n        <app-form-builder-table \r\n        [searchCallback]=\"[service, 'searchOrderhistoryfailedLint',this]\" \r\n        [tableFormat]=\"tableFormat\"\r\n        [table_data]=\"Orderhistoryfailed\"\r\n        [total_page]=\"totalPage\">\r\n      \r\n      </app-form-builder-table>\r\n  </div>\r\n      <!-- <div *ngIf=\"orderhistoryfailedDetail\">\r\n            <app-orderhistoryfailed-detail [back]=\"[this,backToHere]\" [detail]=\"orderhistoryfailedDetail\"></app-orderhistoryfailed-detail>\r\n      </div> -->\r\n\r\n  </div>\r\n\r\n"

/***/ }),

/***/ "./src/app/layout/modules/orderhistoryfailed/detail/orderhistoryfailed.detail.component.scss":
/*!***************************************************************************************************!*\
  !*** ./src/app/layout/modules/orderhistoryfailed/detail/orderhistoryfailed.detail.component.scss ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n  min-height: 48px;\n}\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -5px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .edit_button {\n  position: absolute;\n  top: 7px;\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 16px;\n}\n.card-detail > .card-header .edit_button i {\n  font-weight: bold;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .orderhistoryfailed-detail label {\n  margin-top: 10px;\n}\n.card-detail .orderhistoryfailed-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .orderhistoryfailed-detail .card-header {\n  background-color: #555;\n}\n.card-detail .orderhistoryfailed-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\ntable {\n  border-collapse: collapse;\n  border: 1px solid #ccc;\n  width: 100%;\n}\ntable thead {\n  background-color: #555;\n}\ntable thead th {\n  color: white;\n  padding: 10px;\n  border: 1px solid #ccc;\n}\ntable tbody td {\n  padding: 10px;\n  border: 1px solid #ccc;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvb3JkZXJoaXN0b3J5ZmFpbGVkL2RldGFpbC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXG9yZGVyaGlzdG9yeWZhaWxlZFxcZGV0YWlsXFxvcmRlcmhpc3RvcnlmYWlsZWQuZGV0YWlsLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9vcmRlcmhpc3RvcnlmYWlsZWQvZGV0YWlsL29yZGVyaGlzdG9yeWZhaWxlZC5kZXRhaWwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUk7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0FDRFI7QURFUTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLDZCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSx5QkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7QUNEWjtBREVZO0VBQ0ksaUJBQUE7QUNBaEI7QURHUTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLHNCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSwwQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBS0EsV0FBQTtBQ05aO0FER1k7RUFDSSxpQkFBQTtBQ0RoQjtBRE1JO0VBQ0ksYUFBQTtBQ0pSO0FES1E7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0FDSFo7QURrQkk7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQ2hCUjtBRG1CUTtFQUNJLGdCQUFBO0FDakJaO0FEbUJRO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ2pCWjtBRG9CUTtFQUNJLHNCQUFBO0FDbEJaO0FEbUJZO0VBQ0ksWUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBQ0EsMEJBQUE7QUNqQmhCO0FEdUJBO0VBQ0kseUJBQUE7RUFDQSxzQkFBQTtFQUNBLFdBQUE7QUNwQko7QURxQkk7RUFDSSxzQkFwR2dCO0FDaUZ4QjtBRG9CUTtFQUNJLFlBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7QUNsQlo7QUR3QlE7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7QUN0QloiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9vcmRlcmhpc3RvcnlmYWlsZWQvZGV0YWlsL29yZGVyaGlzdG9yeWZhaWxlZC5kZXRhaWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIkYmFja2dyb3VuZENvbG9ySGVhZGVyOiAjNTU1O1xyXG4uY2FyZC1kZXRhaWx7XHJcbiAgICA+LmNhcmQtaGVhZGVye1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBtaW4taGVpZ2h0OiA0OHB4O1xyXG4gICAgICAgIC5iYWNrX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAtNHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogLTVweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5lZGl0X2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDdweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICBcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJpZ2h0OiAxNnB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgICAgICA+LmNvbC1tZC0xMntcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAvLyAuaW1hZ2V7XHJcbiAgICAvLyAgICAgaDN7XHJcbiAgICAvLyAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIC8vICAgICB9XHJcbiAgICAvLyAgICAgLmNhcmQtY29udGVudHtcclxuICAgIC8vICAgICAgICAgaW1ne1xyXG4gICAgLy8gICAgICAgICAgICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgLy8gICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcblxyXG4gICAgLy8gICAgICAgICB9XHJcbiAgICAvLyAgICAgfVxyXG4gICAgLy8gfVxyXG4gICAgaDF7XHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICB9XHJcbiAgICAub3JkZXJoaXN0b3J5ZmFpbGVkLWRldGFpbHtcclxuICAgICAgICBsYWJlbHtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWwrZGl2e1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDpub3JtYTtcclxuICAgICAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgICAgICAgICAgbWluLWhlaWdodDogMjdweDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICBjb2xvcjogIzY2NjtcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgLmNhcmQtaGVhZGVyeyBcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgaDJ7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG50YWJsZXtcclxuICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIHRoZWFke1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRiYWNrZ3JvdW5kQ29sb3JIZWFkZXI7XHJcbiAgICAgICAgdGh7XHJcbiAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgcGFkZGluZzoxMHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG5cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgdGJvZHl7XHJcbiAgICAgICAgdGR7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6MTBweDtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufSIsIi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWluLWhlaWdodDogNDhweDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi10b3A6IC00cHg7XG4gIG1hcmdpbi1sZWZ0OiAtNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgY29sb3I6ICM1NTU7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuZWRpdF9idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogN3B4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHJpZ2h0OiAxNnB4O1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5lZGl0X2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCB7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG4uY2FyZC1kZXRhaWwgLmNhcmQtY29udGVudCA+IC5jb2wtbWQtMTIge1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuLmNhcmQtZGV0YWlsIGgxIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM1NTU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlmYWlsZWQtZGV0YWlsIGxhYmVsIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5ZmFpbGVkLWRldGFpbCBsYWJlbCArIGRpdiB7XG4gIGJhY2tncm91bmQ6ICNmMGYwZjA7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYTtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgbWluLWhlaWdodDogMjdweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzY2Njtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5ZmFpbGVkLWRldGFpbCAuY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xufVxuLmNhcmQtZGV0YWlsIC5vcmRlcmhpc3RvcnlmYWlsZWQtZGV0YWlsIC5jYXJkLWhlYWRlciBoMiB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBsaW5lLWhlaWdodDogdW5zZXQ7XG4gIGhlaWdodDogdW5zZXQ7XG4gIG1hcmdpbjogMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cblxudGFibGUge1xuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICB3aWR0aDogMTAwJTtcbn1cbnRhYmxlIHRoZWFkIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcbn1cbnRhYmxlIHRoZWFkIHRoIHtcbiAgY29sb3I6IHdoaXRlO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xufVxudGFibGUgdGJvZHkgdGQge1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/orderhistoryfailed/detail/orderhistoryfailed.detail.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/layout/modules/orderhistoryfailed/detail/orderhistoryfailed.detail.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: OrderhistoryfailedDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderhistoryfailedDetailComponent", function() { return OrderhistoryfailedDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var OrderhistoryfailedDetailComponent = /** @class */ (function () {
    function OrderhistoryfailedDetailComponent(orderhistoryService, route, router) {
        this.orderhistoryService = orderhistoryService;
        this.route = route;
        this.router = router;
    }
    OrderhistoryfailedDetailComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    OrderhistoryfailedDetailComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.route.queryParams.subscribe(function (params) { return __awaiter(_this, void 0, void 0, function () {
                    var historyID;
                    return __generator(this, function (_a) {
                        historyID = params.id;
                        this.loadHistoryDetail(historyID);
                        return [2 /*return*/];
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    OrderhistoryfailedDetailComponent.prototype.loadHistoryDetail = function (historyID) {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.orderhistoryService;
                        return [4 /*yield*/, this.orderhistoryService.detailOrderhistory(historyID)];
                    case 1:
                        result = _a.sent();
                        // console.log("result", result);
                        this.orderhistoryDetail = result.result[0];
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    OrderhistoryfailedDetailComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    OrderhistoryfailedDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-orderhistoryfailed-detail',
            template: __webpack_require__(/*! raw-loader!./orderhistoryfailed.detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/orderhistoryfailed/detail/orderhistoryfailed.detail.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./orderhistoryfailed.detail.component.scss */ "./src/app/layout/modules/orderhistoryfailed/detail/orderhistoryfailed.detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], OrderhistoryfailedDetailComponent);
    return OrderhistoryfailedDetailComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/orderhistoryfailed/edit/orderhistoryfailed.edit.component.scss":
/*!***********************************************************************************************!*\
  !*** ./src/app/layout/modules/orderhistoryfailed/edit/orderhistoryfailed.edit.component.scss ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-detail > .card-header {\n  position: relative;\n  min-height: 48px;\n}\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -5px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n.card-detail > .card-header .edit_button {\n  position: absolute;\n  top: 7px;\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 16px;\n}\n.card-detail > .card-header .edit_button i {\n  font-weight: bold;\n}\n.card-detail .card-content {\n  padding: 15px;\n}\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n.card-detail .orderhistoryfailed-detail label {\n  margin-top: 10px;\n}\n.card-detail .orderhistoryfailed-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n.card-detail .orderhistoryfailed-detail .card-header {\n  background-color: #555;\n}\n.card-detail .orderhistoryfailed-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\ntable {\n  border-collapse: collapse;\n  border: 1px solid #ccc;\n  width: 100%;\n}\ntable thead {\n  background-color: #555;\n}\ntable thead th {\n  color: white;\n  padding: 10px;\n  border: 1px solid #ccc;\n}\ntable tbody td {\n  padding: 10px;\n  border: 1px solid #ccc;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvb3JkZXJoaXN0b3J5ZmFpbGVkL2VkaXQvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxvcmRlcmhpc3RvcnlmYWlsZWRcXGVkaXRcXG9yZGVyaGlzdG9yeWZhaWxlZC5lZGl0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9vcmRlcmhpc3RvcnlmYWlsZWQvZWRpdC9vcmRlcmhpc3RvcnlmYWlsZWQuZWRpdC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFHSTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7QUNGUjtBRElRO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ0haO0FES1k7RUFDSSxpQkFBQTtBQ0hoQjtBRE9RO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0Esc0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFNQSxXQUFBO0FDWFo7QURPWTtFQUNJLGlCQUFBO0FDTGhCO0FEWUk7RUFDSSxhQUFBO0FDVlI7QURZUTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7QUNWWjtBRDBCSTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDeEJSO0FENEJRO0VBQ0ksZ0JBQUE7QUMxQlo7QUQ2QlE7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDM0JaO0FEOEJRO0VBQ0ksc0JBQUE7QUM1Qlo7QUQ4Qlk7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSwwQkFBQTtBQzVCaEI7QURtQ0E7RUFDSSx5QkFBQTtFQUNBLHNCQUFBO0VBQ0EsV0FBQTtBQ2hDSjtBRGtDSTtFQUNJLHNCQWpIZ0I7QUNpRnhCO0FEa0NRO0VBQ0ksWUFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtBQ2hDWjtBRHNDUTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtBQ3BDWiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL29yZGVyaGlzdG9yeWZhaWxlZC9lZGl0L29yZGVyaGlzdG9yeWZhaWxlZC5lZGl0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiJGJhY2tncm91bmRDb2xvckhlYWRlcjogIzU1NTtcclxuXHJcbi5jYXJkLWRldGFpbCB7XHJcbiAgICA+LmNhcmQtaGVhZGVyIHtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgbWluLWhlaWdodDogNDhweDtcclxuXHJcbiAgICAgICAgLmJhY2tfYnV0dG9uIHtcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAtNHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogLTVweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcblxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuXHJcbiAgICAgICAgICAgIGkge1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5lZGl0X2J1dHRvbiB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgdG9wOiA3cHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcblxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblxyXG4gICAgICAgICAgICBpIHtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByaWdodDogMTZweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLmNhcmQtY29udGVudCB7XHJcbiAgICAgICAgcGFkZGluZzogMTVweDtcclxuXHJcbiAgICAgICAgPi5jb2wtbWQtMTIge1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvLyAuaW1hZ2V7XHJcbiAgICAvLyAgICAgaDN7XHJcbiAgICAvLyAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIC8vICAgICB9XHJcbiAgICAvLyAgICAgLmNhcmQtY29udGVudHtcclxuICAgIC8vICAgICAgICAgaW1ne1xyXG4gICAgLy8gICAgICAgICAgICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgLy8gICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcblxyXG4gICAgLy8gICAgICAgICB9XHJcbiAgICAvLyAgICAgfVxyXG4gICAgLy8gfVxyXG4gICAgaDEge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5vcmRlcmhpc3RvcnlmYWlsZWQtZGV0YWlsIHtcclxuICAgICAgICBsYWJlbCB7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsYWJlbCtkaXYge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogbm9ybWE7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgY29sb3I6ICM2NjY7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuY2FyZC1oZWFkZXIge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG5cclxuICAgICAgICAgICAgaDIge1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcblxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG50YWJsZSB7XHJcbiAgICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgIHdpZHRoOiAxMDAlO1xyXG5cclxuICAgIHRoZWFkIHtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkYmFja2dyb3VuZENvbG9ySGVhZGVyO1xyXG5cclxuICAgICAgICB0aCB7XHJcbiAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHRib2R5IHtcclxuICAgICAgICB0ZCB7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcblxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn0iLCIuY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1pbi1oZWlnaHQ6IDQ4cHg7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tdG9wOiAtNHB4O1xuICBtYXJnaW4tbGVmdDogLTVweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGNvbG9yOiAjNTU1O1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmVkaXRfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDdweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcbiAgY29sb3I6ICNmZmY7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICByaWdodDogMTZweDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuZWRpdF9idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQge1xuICBwYWRkaW5nOiAxNXB4O1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQgPiAuY29sLW1kLTEyIHtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDBweDtcbn1cbi5jYXJkLWRldGFpbCBoMSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjNTU1O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5ZmFpbGVkLWRldGFpbCBsYWJlbCB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm9yZGVyaGlzdG9yeWZhaWxlZC1kZXRhaWwgbGFiZWwgKyBkaXYge1xuICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICBmb250LXdlaWdodDogbm9ybWE7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIG1pbi1oZWlnaHQ6IDI3cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2NjY7XG59XG4uY2FyZC1kZXRhaWwgLm9yZGVyaGlzdG9yeWZhaWxlZC1kZXRhaWwgLmNhcmQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcbn1cbi5jYXJkLWRldGFpbCAub3JkZXJoaXN0b3J5ZmFpbGVkLWRldGFpbCAuY2FyZC1oZWFkZXIgaDIge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbGluZS1oZWlnaHQ6IHVuc2V0O1xuICBoZWlnaHQ6IHVuc2V0O1xuICBtYXJnaW46IDBweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG5cbnRhYmxlIHtcbiAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgd2lkdGg6IDEwMCU7XG59XG50YWJsZSB0aGVhZCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG59XG50YWJsZSB0aGVhZCB0aCB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbn1cbnRhYmxlIHRib2R5IHRkIHtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/orderhistoryfailed/edit/orderhistoryfailed.edit.component.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/layout/modules/orderhistoryfailed/edit/orderhistoryfailed.edit.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: OrderhistoryfailedEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderhistoryfailedEditComponent", function() { return OrderhistoryfailedEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _object_interface_common_function__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../object-interface/common.function */ "./src/app/object-interface/common.function.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var OrderhistoryfailedEditComponent = /** @class */ (function () {
    function OrderhistoryfailedEditComponent(orderhistoryService) {
        this.orderhistoryService = orderhistoryService;
        this.loading = false;
        this.done = false;
        this.shippingStatusList = [
            { label: 'on checking and processing', value: 'on process', id: 'on-process', checked: true, hovered: false },
            { label: 'Warehouse Packaging', value: 'on packaging', id: 'warehouse-packaging', hovered: false },
            { label: 'On Delivery process', value: 'on delivery', id: 'on-delivery', hovered: false },
            { label: 'Delivered', value: 'delivered', id: 'delivered', hovered: false }
        ];
        this.shippingServices = [
            { label: 'no services', value: 'no-services', id: 'noshipping' },
            // {label: 'JNE', value: 'JNE', id:'jne'},
            { label: 'SAP', value: 'SAP', id: 'sap' },
            { label: 'Si Cepat', value: 'Si Cepat', id: 'sicepat' },
        ];
        this.transactionStatus = [
            { label: "PENDING", value: "PENDING" },
            { label: "CANCEL", value: "CANCEL" },
            { label: "ORDER", value: "ORDER" },
            { label: "CHECKOUT", value: "CHECKOUT" },
            { label: "PAID", value: "PAID" },
            { label: "ERROR", value: "ERROR" },
            { label: "WAITING", value: "WAITING" }
        ];
        this.errorLabel = false;
        this.merchantMode = false;
    }
    OrderhistoryfailedEditComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    OrderhistoryfailedEditComponent.prototype.isShippingValueExists = function (value) {
        for (var _i = 0, _a = this.detail.shipping_info; _i < _a.length; _i++) {
            var element = _a[_i];
            console.log("value", value, element.label);
            if (element.label.trim().toLowerCase() == value.trim().toLowerCase()) {
                return [true, element.created_date];
            }
        }
        return false;
    };
    OrderhistoryfailedEditComponent.prototype.ngOnChange = function () {
        console.log("HELLO");
    };
    OrderhistoryfailedEditComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var billings, additionalBillings, prop, _a, e_1;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        if (this.detail.billings) {
                            billings = this.detail.billings;
                            additionalBillings = [];
                            if (billings.discount) {
                                additionalBillings.push({
                                    product_name: billings.discount.product_name,
                                    value: billings.discount.value
                                });
                            }
                            if (this.detail.payment_detail.payment_via != 'manual banking') {
                                if (billings.unique_amount) {
                                    delete billings.unique_amount;
                                }
                            }
                            for (prop in billings) {
                                console.log("PROP", prop);
                                if (prop == 'shipping_fee' || prop == 'unique_amount') {
                                    additionalBillings.push({
                                        product_name: prop,
                                        value: billings[prop]
                                    });
                                }
                            }
                            console.log(this.detail.billings);
                            this.detail.additionalBillings = additionalBillings;
                        }
                        if (this.detail.merchant) {
                            this.merchantMode = true;
                        }
                        if (!this.detail.shipping_services) {
                            this.detail.shipping_services = 'no-services';
                            // console.log("Result", shippingServices)
                        }
                        if (this.detail.status) {
                            this.detail.status = this.detail.status.trim().toUpperCase();
                        }
                        if (this.detail.shipping_info) {
                            console.log('this.detail.shipping_info', this.detail.shipping_info);
                            this.shippingStatusList.forEach(function (element, index) {
                                var check = _this.isShippingValueExists(element.value);
                                if (check) {
                                    _this.shippingStatusList[index].checked = check[0];
                                    _this.shippingStatusList[index].created_date = check[1];
                                }
                                else {
                                    _this.shippingStatusList[index].checked = false;
                                }
                            });
                        }
                        _a = this.detail;
                        return [4 /*yield*/, this.detail.status];
                    case 1:
                        _a.previous_status = _b.sent();
                        this.transactionStatus.forEach(function (element, index) {
                            if (element.value == _this.detail.transaction_status) {
                                _this.transactionStatus[index].selected = 1;
                            }
                        });
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _b.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    OrderhistoryfailedEditComponent.prototype.backToDetail = function () {
        if (this.back[0][this.back[1]] && !Object(_object_interface_common_function__WEBPACK_IMPORTED_MODULE_3__["isFunction"])(this.back[0])) {
            this.back[0][this.back[1]];
        }
        else if (Object(_object_interface_common_function__WEBPACK_IMPORTED_MODULE_3__["isFunction"])(this.back[0])) {
            this.back[0]();
        }
        else {
            window.history.back();
        }
    };
    OrderhistoryfailedEditComponent.prototype.changeshippingStatusList = function (index) {
        for (var n = 0; n <= index; n++) {
            //  console.log("HOVERED",  this.shippingStatusList[n].hovered)
            this.shippingStatusList[n].checked = true;
        }
        for (var n = index + 1; n < this.shippingStatusList.length; n++) {
            this.shippingStatusList[n].checked = false;
        }
    };
    OrderhistoryfailedEditComponent.prototype.hoverShippingStatusList = function (index, objectHover) {
        // console.log("HOVERED", index)
        for (var n = 0; n <= index; n++) {
            //  console.log("HOVERED",  this.shippingStatusList[n].hovered)
            this.shippingStatusList[n].hovered = true;
        }
        for (var n = index + 1; n < this.shippingStatusList.length; n++) {
            this.shippingStatusList[n].hovered = false;
        }
    };
    OrderhistoryfailedEditComponent.prototype.convertShippingStatusToShippingInfo = function () {
        var newData = [];
        this.shippingStatusList.forEach(function (element, index) {
            if (element.checked) {
                newData.push({ value: element.value });
            }
        });
        return newData;
    };
    OrderhistoryfailedEditComponent.prototype.saveThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var frm, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.loading = !this.loading;
                        frm = JSON.parse(JSON.stringify(this.detail));
                        frm.shipping_info = this.convertShippingStatusToShippingInfo();
                        // this.detail.shipping_status = this.valu
                        delete frm.buyer_detail;
                        delete frm.payment_expire_date;
                        delete frm.products;
                        return [4 /*yield*/, this.orderhistoryService.updateOrderHistoryAllHistory(frm)];
                    case 1:
                        _a.sent();
                        this.loading = !this.loading;
                        this.done = true;
                        alert("Edit submitted");
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    OrderhistoryfailedEditComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], OrderhistoryfailedEditComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], OrderhistoryfailedEditComponent.prototype, "back", void 0);
    OrderhistoryfailedEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-orderhistoryfailed-edit',
            template: __webpack_require__(/*! raw-loader!./orderhistoryfailed.edit.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/orderhistoryfailed/edit/orderhistoryfailed.edit.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./orderhistoryfailed.edit.component.scss */ "./src/app/layout/modules/orderhistoryfailed/edit/orderhistoryfailed.edit.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"]])
    ], OrderhistoryfailedEditComponent);
    return OrderhistoryfailedEditComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/orderhistoryfailed/orderhistoryfailed-routing.module.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/layout/modules/orderhistoryfailed/orderhistoryfailed-routing.module.ts ***!
  \****************************************************************************************/
/*! exports provided: OrderhistoryfailedRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderhistoryfailedRoutingModule", function() { return OrderhistoryfailedRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _orderhistoryfailed_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./orderhistoryfailed.component */ "./src/app/layout/modules/orderhistoryfailed/orderhistoryfailed.component.ts");
/* harmony import */ var _detail_orderhistoryfailed_detail_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./detail/orderhistoryfailed.detail.component */ "./src/app/layout/modules/orderhistoryfailed/detail/orderhistoryfailed.detail.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// import { OrderhistorysummaryDetailComponent } from './detail/orderhistorysummary.detail.component';
// import { OrderhistorysummaryEditComponent } from './edit/orderhistorysummary.edit.component';
var routes = [
    {
        path: '', component: _orderhistoryfailed_component__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryfailedComponent"]
    },
    // {
    //   path:'detail', component: OrderhistorysuccessDetailComponent
    // },
    // {
    //   path:'edit', component: OrderhistorysummaryEditComponent
    // }
    {
        path: 'edit', component: _detail_orderhistoryfailed_detail_component__WEBPACK_IMPORTED_MODULE_3__["OrderhistoryfailedDetailComponent"]
    }
];
var OrderhistoryfailedRoutingModule = /** @class */ (function () {
    function OrderhistoryfailedRoutingModule() {
    }
    OrderhistoryfailedRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], OrderhistoryfailedRoutingModule);
    return OrderhistoryfailedRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/orderhistoryfailed/orderhistoryfailed.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/layout/modules/orderhistoryfailed/orderhistoryfailed.component.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL29yZGVyaGlzdG9yeWZhaWxlZC9vcmRlcmhpc3RvcnlmYWlsZWQuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/layout/modules/orderhistoryfailed/orderhistoryfailed.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/layout/modules/orderhistoryfailed/orderhistoryfailed.component.ts ***!
  \***********************************************************************************/
/*! exports provided: OrderhistoryfailedComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderhistoryfailedComponent", function() { return OrderhistoryfailedComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var OrderhistoryfailedComponent = /** @class */ (function () {
    function OrderhistoryfailedComponent(orderhistoryService, router) {
        this.orderhistoryService = orderhistoryService;
        this.router = router;
        this.Orderhistoryfailed = [];
        this.tableFormat = {
            title: 'Order History Failed Detail Page',
            label_headers: [
                { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
                { label: 'Order date', visible: true, type: 'date', data_row_name: 'created_date' },
                { label: 'Expired date', visible: true, type: 'string', data_row_name: 'payment_expire_date' },
                { label: 'Merchant Name', visible: true, type: 'string', data_row_name: 'merchant_name' },
                { label: 'User Id', visible: true, type: 'string', data_row_name: 'user_id' },
                // unfinished
                { label: 'User Name', visible: true, type: 'string', data_row_name: 'user_name' },
                // unfinished
                { label: 'User Email', visible: true, type: 'string', data_row_name: 'email' },
                { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
                // unfinished
                { label: 'Total Price', visible: true, type: 'string', data_row_name: 'total_price' }
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'Orderhistoryfailed',
                detail_function: [this, 'callDetail']
            },
            show_checkbox_options: true
        };
        this.form_input = false;
        this.errorLabel = false;
        this.orderhistoryfailedDetail = false;
    }
    // ngOnInit() {
    //   this.orderhistoryService.getOrderhistory().subscribe(data=>{
    //     let result: any = data;
    //     console.log(result.result);
    //     this.Orderhistory = result.result;
    //     //display and convert shoppingcart_id in foreach because type data is ObjectID
    //     this.Orderhistory.forEach((data, index)=>{this.Orderhistory[index].shoppingcart_id = this.Orderhistory[index].shoppingcart_id.$oid})
    //   });
    // }
    OrderhistoryfailedComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    OrderhistoryfailedComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.orderhistoryService;
                        return [4 /*yield*/, this.orderhistoryService.getOrderhistoryfailedLint()];
                    case 1:
                        result = _a.sent();
                        this.totalPage = result.result.total_page;
                        console.log(result);
                        this.Orderhistoryfailed = result.result.values;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    OrderhistoryfailedComponent.prototype.callDetail = function (orderhistory_id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.router.navigate(['administrator/orderhistoryfailedamin/edit'], { queryParams: { id: orderhistory_id } });
                return [2 /*return*/];
            });
        });
    };
    OrderhistoryfailedComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.orderhistoryfailedDetail = false;
                return [2 /*return*/];
            });
        });
    };
    OrderhistoryfailedComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    OrderhistoryfailedComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-orderhistoryfailed',
            template: __webpack_require__(/*! raw-loader!./orderhistoryfailed.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/orderhistoryfailed/orderhistoryfailed.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./orderhistoryfailed.component.scss */ "./src/app/layout/modules/orderhistoryfailed/orderhistoryfailed.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], OrderhistoryfailedComponent);
    return OrderhistoryfailedComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/orderhistoryfailed/orderhistoryfailed.module.ts":
/*!********************************************************************************!*\
  !*** ./src/app/layout/modules/orderhistoryfailed/orderhistoryfailed.module.ts ***!
  \********************************************************************************/
/*! exports provided: OrderhistoryfailedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderhistoryfailedModule", function() { return OrderhistoryfailedModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _orderhistoryfailed_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./orderhistoryfailed-routing.module */ "./src/app/layout/modules/orderhistoryfailed/orderhistoryfailed-routing.module.ts");
/* harmony import */ var _orderhistoryfailed_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./orderhistoryfailed.component */ "./src/app/layout/modules/orderhistoryfailed/orderhistoryfailed.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var _detail_orderhistoryfailed_detail_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./detail/orderhistoryfailed.detail.component */ "./src/app/layout/modules/orderhistoryfailed/detail/orderhistoryfailed.detail.component.ts");
/* harmony import */ var _edit_orderhistoryfailed_edit_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./edit/orderhistoryfailed.edit.component */ "./src/app/layout/modules/orderhistoryfailed/edit/orderhistoryfailed.edit.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var OrderhistoryfailedModule = /** @class */ (function () {
    function OrderhistoryfailedModule() {
    }
    OrderhistoryfailedModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _orderhistoryfailed_routing_module__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryfailedRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__["BsComponentModule"]
            ],
            declarations: [_orderhistoryfailed_component__WEBPACK_IMPORTED_MODULE_3__["OrderhistoryfailedComponent"], _detail_orderhistoryfailed_detail_component__WEBPACK_IMPORTED_MODULE_9__["OrderhistoryfailedDetailComponent"], _edit_orderhistoryfailed_edit_component__WEBPACK_IMPORTED_MODULE_10__["OrderhistoryfailedEditComponent"]]
        })
    ], OrderhistoryfailedModule);
    return OrderhistoryfailedModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-orderhistoryfailed-orderhistoryfailed-module.js.map