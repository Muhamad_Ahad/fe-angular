(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-product-voucher-voucher-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/product-voucher/add/product.add.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/product-voucher/add/product.add.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n  <h3 class=\"heading\">Product Management</h3>\r\n  <!-- <app-page-header [heading]=\"'Product Management'\" [icon]=\"'fa-table'\"></app-page-header> -->\r\n  <div class=\"add-product\">\r\n      <div class=\"e-wrapper\">\r\n          <app-merchant-add-new-product type=\"e-voucher\"></app-merchant-add-new-product>\r\n      </div>\r\n  </div>\r\n</div>\r\n\r\n<!-- <div [@routerTransition]>\r\n  <app-page-header [heading]=\"'Add New Product'\" [icon]=\"'fa-table'\"></app-page-header>\r\n  <div>\r\n    <div class=\"card card-detail\">\r\n      <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToDetail()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n        <button class=\"btn save_button {{loading}}\" type=\"submit\" (click)=\"saveThis()\">\r\n          <i class=\"fa fa-fw fa-save\"></i>\r\n          <span *ngIf=\"!loading\">Save</span> <span *ngIf=\"loading\">loading</span>\r\n        </button>\r\n      </div>\r\n\r\n      <div class=\"card-content\">\r\n        <div class=\"product-detail\">\r\n          <div class=\"row\">\r\n            <div class=\" col-md-8\">\r\n              <div class=\"card mb-3\">\r\n                <div class=\"card-header\">\r\n                  <h2> Product Details </h2>\r\n                </div>\r\n                <div class=\"card-content\">\r\n                  <div class=\"col-md-12\">\r\n\r\n                    <label>Type</label>\r\n                    <form-select name=\"type\" [disabled]=\"false\" [(ngModel)]=\"data.type\" \r\n                      [data]=\"productType\" autofocus required>                      \r\n                    </form-select>\r\n\r\n                    <label>Merchant Group</label>\r\n                    <form-select name=\"merchant_group\" [disabled]=\"false\" [(ngModel)]=\"data.merchant_group\"\r\n                      [data]=\"merchant_group\" autofocus required>\r\n                    </form-select>\r\n\r\n                    <label>Choose Merchant </label>\r\n                    <form-select name=\"merchant_username\" [disabled]=\"false\" [(ngModel)]=\"data.merchant_username\"\r\n                      [data]=\"merchantList\" autofocus required>\r\n                    </form-select>\r\n\r\n                    <label>Product Code</label>\r\n                    <form-input name=\"product_code\" [type]=\"'text'\" [placeholder]=\"'Product Code'\"\r\n                      [(ngModel)]=\"data.product_code\" autofocus required></form-input>\r\n\r\n                    <label>Product Name</label>\r\n                    <form-input name=\"product_name\" [type]=\"'text'\" [placeholder]=\"'Product Name'\"\r\n                      [(ngModel)]=\"data.product_name\" autofocus required></form-input>\r\n\r\n                    <label>Price</label>\r\n                    <form-input name=\"price\" [type]=\"'number'\" [placeholder]=\"'Price'\" [(ngModel)]=\"data.price\"\r\n                      autofocus required></form-input>\r\n\r\n                    <label>Fixed Price</label>\r\n                    <form-input name=\"fixed_price\" [type]=\"'number'\" [placeholder]=\"'Fixed Price'\"\r\n                      [(ngModel)]=\"data.fixed_price\" autofocus required></form-input>\r\n\r\n                    <label>Category</label>\r\n                      <form-select name=\"category\" [disabled]=\"false\" [(ngModel)]=\"category\"\r\n                        [data]=\"categoryProduct\" autofocus required>\r\n                      </form-select>\r\n\r\n                    <label>Hashtags</label>\r\n                    <form-input name=\"hashtags\" [type]=\"'text'\" [placeholder]=\"'Hashtags'\" [(ngModel)]=\"data.hashtags\"\r\n                      autofocus required></form-input>\r\n\r\n\r\n\r\n                    <label>Description</label>\r\n                    <ckeditor [editor]=\"Editor\" [(ngModel)]=\"data.description\"></ckeditor>\r\n\r\n\r\n                    <label>TNC</label>\r\n                    <ckeditor [editor]=\"Editor\" [(ngModel)]=\"data.tnc\"></ckeditor>\r\n\r\n                    <div *ngIf=\"data.type === 'product'\" class=\"condition-wrapper\">\r\n\r\n                      <label>Quantity</label>\r\n                      <form-input name=\"quantity\" [type]=\"'number'\" [placeholder]=\"'Quantity'\" [(ngModel)]=\"data.qty\"\r\n                        autofocus required></form-input>\r\n\r\n                    </div>\r\n\r\n                    <label>Minimal Order</label>\r\n                    <form-input name=\"order\" [type]=\"'text'\" [(ngModel)]=\"data.min_order\" autofocus required>\r\n                    </form-input>\r\n\r\n                  </div>\r\n                </div>\r\n              </div>\r\n\r\n            </div>\r\n            <div class=\" col-md-4\">\r\n              <div class=\"card mb-9\">\r\n                <div class=\"card-header\">\r\n                  <h2>Others</h2>\r\n                </div>\r\n                <div class=\"card-content\">\r\n                  <div class=\"col-md-12\">\r\n\r\n                    <div *ngIf=\"data.type == 'evoucher'\" class=\"evoucher-condition-wrapper\">\r\n\r\n                      <label>Redeem Type</label>\r\n                      <form-select name=\"redeem_type\" [disabled]=\"false\" [(ngModel)]=\"data.redeem_type\"\r\n                        [data]=\"redeem_type\" autofocus required>\r\n                      </form-select>\r\n                    </div>\r\n\r\n                    <div *ngIf=\"data.type == 'product'\" class=\"product-condition-wrapper\">\r\n\r\n                      <label>Condition</label>\r\n                      <form-select width=\"50%\" name=\"condition\" [disabled]=\"false\" [(ngModel)]=\"data.condition\"\r\n                       [data]=\"condition\" autofocus required>\r\n                      </form-select>\r\n\r\n                      <label>Weight</label>\r\n                      <form-input name=\"weight\" [type]=\"'number'\" [(ngModel)]=\"data.weight\" autofocus required>\r\n                      </form-input>\r\n\r\n                      <label>Dimension Length</label>\r\n                      <form-input name=\"length\" [type]=\"'number'\" [placeholder]=\"'length'\"\r\n                        [(ngModel)]=\"data.dimensions.length\" autofocus required></form-input>\r\n\r\n                      <label>Dimension Width</label>\r\n                      <form-input name=\"width\" [type]=\"'number'\" [(ngModel)]=\"data.dimensions.width\" autofocus required>\r\n                      </form-input>\r\n\r\n                      <label>Dimension Height</label>\r\n                      <form-input name=\"height\" [type]=\"'number'\" [(ngModel)]=\"data.dimensions.height\" autofocus\r\n                        required></form-input>\r\n\r\n\r\n\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n              </div>\r\n              <div class=\"card mb-3\" id=\"img\">\r\n                <div class=\"card-header\">\r\n                  <h2>Images</h2>\r\n                </div>\r\n                <div class=\"card-content\">\r\n                  <div class=\"  image\">\r\n                    <div class=\"\">\r\n                      <div class=\"card\">\r\n                        <div class=\"card-header\">\r\n                          <h3>Big </h3>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                          <img *ngIf=\"data.pic_big_path\" src=\"{{data.base_url+data.pic_big_path}}\" />\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"\">\r\n                      <div class=\"card\">\r\n                        <div class=\"card-header\">\r\n                          <h3>Medium </h3>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                          <img *ngIf=\"data.pic_medium_path\" src=\"{{data.base_url+data.pic_medium_path}}\" />\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"\">\r\n                      <div class=\"card\">\r\n                        <div class=\"card-header\">\r\n                          <h3>Small </h3>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                          <img *ngIf=\"data.pic_small_path\" src=\"{{data.base_url+data.pic_small_path}}\" />\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n\r\n\r\n                  <div class=\"col-md-3 col-sm-3\">\r\n                    <div class=\"pb-framer\">\r\n                      <div class=\"progressbar {{cancel?'terminated':''}}\" [ngStyle]=\"{'width.%': progressBar}\">\r\n                      </div>\r\n                      <span *ngIf=\"progressBar <58&&progressBar>0\"> {{cancel?'terminated':progressBar+'%'}} </span>\r\n                      <span class=\"clr-white\" *ngIf=\"progressBar>58\"> {{cancel?'terminated':progressBar+'%'}} </span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"file-upload\">\r\n                    <div *ngIf=\"errorFile\" class=\"error_label\"><mark>Error: {{errorFile}}</mark></div>\r\n                    <img *ngIf=\"data.pic_big_path\" [src]=\"data.base_url+data.pic_big_path\" height=\"200\"> <br />\r\n                    <input type=\"file\" (change)=\"onFileSelected($event)\" accept=\"image/*\">\r\n                    <br> <br>\r\n                    <button class=\"btn rounded-btn\" (click)=\"onUpload()\">Upload</button>\r\n                    <button class=\"btn rounded-btn-danger\" (click)=\"cancelThis()\">Cancel</button>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div> -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/product-voucher/detail/product.detail.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/product-voucher/detail/product.detail.component.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <div [@routerTransition]>\r\n    <app-page-header [heading]=\"'Order Detail for'\" [icon]=\"'fa-table'\"></app-page-header> -->\r\n    <div *ngIf=\"this.productDetail\">\r\n        <app-merchant-edit-product-product></app-merchant-edit-product-product>\r\n    </div>\r\n\r\n<!-- <div *ngIf=\"!edit\" class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToTable()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n        <h1>{{detail.product_name}}</h1>\r\n        <button class=\"btn edit_button\" (click)=\"editThis()\"><i class=\"fa fa-fw fa-pencil-alt\"></i> edit</button>\r\n    </div>\r\n    <div class=\"card-content\">\r\n        <div class=\"product-detail\">\r\n\r\n            <div class=\"row\">\r\n                <div class=\" col-md-8\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\">\r\n                            <h2> Product Details </h2>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n                                <label>Product Name</label>\r\n                                <div name=\"product_name\">{{detail.product_name}}</div>\r\n\r\n                                <label>Product Code</label>\r\n                                <div name=\"product_code\">{{detail.product_code}}</div>\r\n\r\n                                <label>Description</label>\r\n                                <div name=\"description\">{{detail.description}}</div>\r\n\r\n                                <label>Terms and Conditions</label>\r\n                                <div name=\"tnc\">{{detail.tnc}}</div>\r\n -->\r\n                                <!-- <label>Point Price</label>\r\n                                <div name=\"point_price\">{{detail.point_price}}</div> -->\r\n<!-- \r\n                                <label>Price</label>\r\n                                <div name=\"price\">{{detail.price}}</div>\r\n\r\n                                 <label>Fixed Price</label>\r\n                                <div name=\"fixed_price\">{{detail.fixed_price}}</div>\r\n -->\r\n                                <!-- <label>Discount Value</label>\r\n                                <div name=\"discount_value\">{{detail.discount_value}}</div> -->\r\n                                \r\n                                <!-- <label>Earned Points</label>\r\n                                <div name=\"earned_points\">{{detail.earned_points}}</div> -->\r\n<!-- \r\n                                <label>Quantity</label>\r\n                                <div name=\"qty\">{{detail.qty}}</div>\r\n\r\n                                <label>Type</label>\r\n                                <div name=\"type\">{{detail.type}}</div>\r\n\r\n                                \r\n\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\" col-md-4\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\">\r\n                            <h2>Merchant &amp; status</h2>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n                                \r\n                                <label>Status</label>\r\n                                <div name=\"status\">{{detail.active}}</div>\r\n\r\n                                <label>Category</label>\r\n                                <div name=\"status\">{{detail.category}}</div>\r\n\r\n                                <label>Hashtags</label>\r\n                                <div name=\"hashtags\">{{detail.hashtags}}</div>\r\n\r\n                                <label *ngIf=\"detail.merchant_name\">Merchant Name</label>\r\n                                <div *ngIf=\"detail.merchant_name\" name=\"owner_name\">{{detail.merchant_name}}</div>\r\n\r\n                                <label>Favorite</label>\r\n                                <div name=\"favorite\">{{detail.favorite}}</div>\r\n\r\n                                <label>Process ID</label>\r\n                                <div name=\"process_id\">{{detail.process_id}}</div>\r\n\r\n                             \r\n\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\">\r\n                            <h2>Images</h2>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"  image\">\r\n                                <div class=\"\">\r\n                                    <div class=\"card\">\r\n                                        <div class=\"card-header\">\r\n                                            <h3>Big </h3>\r\n                                        </div>\r\n                                        <div class=\"card-content\">\r\n                                            <img src=\"{{detail.base_url+detail.pic_big_path}}\" />\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"\">\r\n                                    <div class=\"card\">\r\n                                        <div class=\"card-header\">\r\n                                            <h3>Medium </h3>\r\n                                        </div>\r\n                                        <div class=\"card-content\">\r\n                                            <img src=\"{{detail.base_url+detail.pic_medium_path}}\" />\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"\">\r\n                                    <div class=\"card\">\r\n                                        <div class=\"card-header\">\r\n                                            <h3>Small </h3>\r\n                                        </div>\r\n                                        <div class=\"card-content\">\r\n                                            <img src=\"{{detail.base_url+detail.pic_small_path}}\" />\r\n                                        </div>\r\n                                    </div>\r\n                                </div> -->\r\n                                <!-- {{form.value | json}}  -->\r\n                            <!-- </div>\r\n\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-12\"><button class=\"btn delete-button float-right btn-danger\"\r\n                                (click)=\"deleteThis()\">delete</button></div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n    </div>\r\n</div>\r\n<div *ngIf=\"edit\">\r\n    <app-product-edit [back]=\"[this, 'editThis']\" [detail]=\"detail\"></app-product-edit>\r\n</div> -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/product-voucher/edit/product.edit.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/product-voucher/edit/product.edit.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n    <app-page-header [heading]=\"'Product Detail'\" [icon]=\"'fa-table'\"></app-page-header>\r\n    <div class=\"edit-product\">\r\n        <div class=\"e-wrapper\">\r\n            <app-merchant-edit-product-product></app-merchant-edit-product-product>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/product-voucher/voucher.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/product-voucher/voucher.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  <div [@routerTransition]>\r\n           \r\n            <!-- <app-page-header [heading]=\"'Product'\" [icon]=\"'fa-table'\"></app-page-header>\r\n            <h3 style=\"color: white\">File Upload</h3>\r\n            <div class=\"col-md-3 col-sm-3\">\r\n                  <div class=\"pb-framer\">\r\n                              <div class=\"progressbar {{cancel?'terminated':''}}\" [ngStyle]=\"{'width.%': progressBar}\">\r\n                  \r\n                              </div>\r\n                              <span *ngIf=\"progressBar<58&&progressBar>0\"> {{cancel?'terminated':progressBar+'%'}} </span>\r\n                              <span class=\"clr-white\" *ngIf=\"progressBar>58\"> {{cancel?'terminated':progressBar+'%'}} </span>\r\n                        \r\n                        </div>\r\n            \r\n            </div>\r\n            <div class=\"col-md-3 col-sm-3\">\r\n                  <div *ngIf=\"errorFile\" class=\"error_label\"><mark>Error: {{errorFile}}</mark></div>                       \r\n            </div>\r\n            <input class=\"custom-content\" title=\" \" type=\"file\" style=\"color: white;\" (change)=\"onFileSelected($event)\" accept=\".csv,.xlsx,.xls\" multiple/>\r\n            <button class=\"btn rounded-btn\" (click)=\"onUpload()\" >Upload</button>\r\n            <button class=\"btn rounded-btn-danger\" (click)=\"cancelThis()\" >Cancel</button>\r\n            <br><br> -->\r\n            <!-- this section belongs to product item list -->\r\n            <div *ngIf=\"Products&&prodDetail==false&&!errorMessage\">\r\n                  <app-form-builder-table\r\n                  [table_data]  = \"Products\" \r\n                  [searchCallback]= \"[service, 'searchVouchersReportLint',this]\" \r\n                  [tableFormat]=\"tableFormat\"\r\n                  [total_page]=\"totalPage\"\r\n                  >\r\n                      \r\n                  </app-form-builder-table>\r\n            </div>\r\n            <div *ngIf=\"errorMessage\" class=\"error-message\">\r\n                  <div class=\"text-message\">\r\n                        <i class=\"fas fa-ban\"></i>\r\n                        {{errorMessage}}\r\n                  </div>\r\n            </div>\r\n            <!-- <div *ngIf=\"prodDetail\">\r\n                  <app-product-detail [back]=\"[this,backToHere]\" [detail]=\"prodDetail\"></app-product-detail>\r\n            </div> -->\r\n\r\n           \r\n          </div>\r\n          \r\n"

/***/ }),

/***/ "./src/app/layout/modules/product-voucher/add/product.add.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/layout/modules/product-voucher/add/product.add.component.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".add-product {\n  border: 1px solid rgba(0, 0, 0, 0.125);\n  border-radius: 0.25rem;\n  background-color: white;\n  margin: 20px;\n}\n.add-product .e-wrapper {\n  position: relative;\n}\n.heading {\n  color: white;\n  font-weight: bold;\n  letter-spacing: 0.8px;\n  margin: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcHJvZHVjdC12b3VjaGVyL2FkZC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXHByb2R1Y3Qtdm91Y2hlclxcYWRkXFxwcm9kdWN0LmFkZC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcHJvZHVjdC12b3VjaGVyL2FkZC9wcm9kdWN0LmFkZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHNDQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7QUNDSjtBREFJO0VBQ0ksa0JBQUE7QUNFUjtBREVBO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9wcm9kdWN0LXZvdWNoZXIvYWRkL3Byb2R1Y3QuYWRkLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmFkZC1wcm9kdWN0e1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEyNSk7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwLjI1cmVtO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICBtYXJnaW46IDIwcHg7XHJcbiAgICAuZS13cmFwcGVye1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIH1cclxufVxyXG5cclxuLmhlYWRpbmcge1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogLjhweDtcclxuICAgIG1hcmdpbjogMjBweDtcclxufVxyXG5cclxuLy8gLmNhcmQtaGVhZGVye1xyXG4vLyAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4vLyAgICAgbWluLWhlaWdodDogNDhweDtcclxuXHJcbi8vICAgICAuYmFja19idXR0b257XHJcbi8vICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4vLyAgICAgICAgIG1hcmdpbi10b3A6IC00cHg7XHJcbi8vICAgICAgICAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xyXG4vLyAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4vLyAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4vLyAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICBcclxuLy8gICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xyXG4vLyAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbi8vICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbi8vICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuLy8gICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4vLyAgICAgICAgIGl7XHJcbi8vICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4vLyAgICAgICAgIH1cclxuLy8gICAgIH1cclxuLy8gICAgIC5zYXZlX2J1dHRvbntcclxuLy8gICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbi8vICAgICAgICAgdG9wOiA3cHg7XHJcbi8vICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuLy8gICAgICAgICBjb2xvcjogI2ZmZjtcclxuLy8gICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgXHJcbi8vICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XHJcbi8vICAgICAgICAgZGlzcGxheTogZmxleDtcclxuLy8gICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuLy8gICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgXHJcbi8vICAgICAgICAgaXtcclxuLy8gICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbi8vICAgICAgICAgfVxyXG4vLyAgICAgICAgIHJpZ2h0OiAxMHB4O1xyXG4vLyAgICAgfVxyXG4vLyAgICAgLnNhdmVfYnV0dG9uLnRydWV7XHJcbi8vICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2U2N2UyMjtcclxuLy8gICAgIH1cclxuLy8gfVxyXG4vLyAucGItZnJhbWVye1xyXG4vLyAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4vLyAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4vLyAgICAgaGVpZ2h0OiAyNXB4O1xyXG4vLyAgICAgY29sb3I6ICNlNjdlMjI7XHJcbi8vICAgICAucHJvZ3Jlc3NiYXJ7XHJcbi8vICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuLy8gICAgICAgICBiYWNrZ3JvdW5kOiAjMzQ5OGRiO1xyXG4vLyAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuLy8gICAgICAgICBoZWlnaHQ6IDI1cHg7XHJcbi8vICAgICAgICAgei1pbmRleDogLTE7XHJcbi8vICAgICB9XHJcbi8vICAgICAudGVybWluYXRlZC5wcm9ncmVzc2JhcntcclxuLy8gICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XHJcbi8vICAgICB9XHJcbi8vICAgICAuY2xyLXdoaXRle1xyXG4vLyAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuLy8gICAgIH1cclxuLy8gfVxyXG5cclxuLy8gbGFiZWx7XHJcbi8vICAgICBjb2xvcjpibGFjaztcclxuLy8gfVxyXG5cclxuLy8gLm5nLXByaXN0aW5le1xyXG4vLyAgICAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XHJcbi8vIH1cclxuXHJcbi8vIC5yb3VuZGVkLWJ0bi1zdWJtaXQge1xyXG4vLyAgICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4vLyAgICAgYm9yZGVyLXJhZGl1czogNzBweDtcclxuLy8gICAgIGNvbG9yOiB3aGl0ZTtcclxuLy8gICAgIGZvbnQtc2l6ZTogMThweDtcclxuLy8gICAgIGxpbmUtaGVpZ2h0OiA0MHB4O1xyXG4vLyAgICAgcGFkZGluZzogMCA0MHB4O1xyXG4vLyAgICAgYmFja2dyb3VuZDogIzU2YTRmZjtcclxuLy8gbWFyZ2luLWxlZnQ6IDQwJTtcclxuLy8gbWFyZ2luLXJpZ2h0OiA1MCU7XHJcbi8vIH1cclxuLy8gLnJvdW5kZWQtYnRuLXN1Ym1pdDpob3ZlciB7XHJcbi8vICAgICBiYWNrZ3JvdW5kOiBkYXJrZW4oIzU2QTRGRiwgMTUlKTtcclxuLy8gICAgIC8vIGJvcmRlcjogMnB4IHNvbGlkIGJsdWU7XHJcbi8vICAgICBvdXRsaW5lOiBub25lO1xyXG4vLyB9XHJcblxyXG4vLyAuYXV0by1uYW1le1xyXG4vLyAgICAgd2lkdGg6IDE1MiUgIWltcG9ydGFudDtcclxuLy8gfVxyXG5cclxuLy8gLm5nLXByaXN0aW5le1xyXG4vLyAgICAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7XHJcbi8vICAgICB3aWR0aDogMTAwJTtcclxuLy8gICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuLy8gICAgIHBhZGRpbmc6IDZweCAxMnB4O1xyXG4vLyB9XHJcblxyXG4vLyAucm91bmRlZC1idG4ge1xyXG4vLyAgICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4vLyAgICAgYm9yZGVyLXJhZGl1czogMzBweDtcclxuLy8gICAgIGNvbG9yOiB3aGl0ZTtcclxuLy8gICAgIGZvbnQtc2l6ZTogMThweDtcclxuLy8gICAgIGxpbmUtaGVpZ2h0OiA0MHB4O1xyXG4vLyAgICAgcGFkZGluZzogMCAxNXB4O1xyXG4vLyAgICAgYmFja2dyb3VuZDogIzU2YTRmZjtcclxuLy8gfVxyXG4vLyAucm91bmRlZC1idG46aG92ZXIge1xyXG4vLyAgICAgYmFja2dyb3VuZDogZGFya2VuKCM1NkE0RkYsIDE1JSk7XHJcbi8vICAgICAvLyBib3JkZXI6IDJweCBzb2xpZCBibHVlO1xyXG4vLyAgICAgb3V0bGluZTogbm9uZTtcclxuLy8gfVxyXG5cclxuLy8gLnJvdW5kZWQtYnRuLWRhbmdlciB7XHJcbi8vICAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbi8vICAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4vLyAgICAgY29sb3I6IHdoaXRlO1xyXG4vLyAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4vLyAgICAgbGluZS1oZWlnaHQ6IDQwcHg7XHJcbi8vICAgICBwYWRkaW5nOiAwIDE1cHg7XHJcbi8vICAgICBiYWNrZ3JvdW5kOiAjZGMzNTQ1O1xyXG4vLyB9XHJcbi8vIC5yb3VuZGVkLWJ0bi1kYW5nZXI6aG92ZXIge1xyXG4vLyAgICAgYmFja2dyb3VuZDogZGFya2VuKCNkYzM1NDUsIDE1JSk7XHJcbi8vICAgICAvLyBib3JkZXI6IDJweCBzb2xpZCBibHVlO1xyXG4vLyAgICAgb3V0bGluZTogbm9uZTtcclxuLy8gfVxyXG5cclxuLy8gLmNhcmQtZGV0YWlse1xyXG4vLyAgICAgPi5jYXJkLWhlYWRlcntcclxuLy8gICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbi8vICAgICB9XHJcbi8vICAgICAuY2FyZC1jb250ZW50e1xyXG4vLyAgICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICBcclxuLy8gICAgICAgICA+LmNvbC1tZC0xMntcclxuLy8gICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbi8vICAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuLy8gICAgICAgICB9XHJcbi8vICAgICB9XHJcbiAgICBcclxuLy8gICAgIGgxe1xyXG4vLyAgICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuLy8gICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuLy8gICAgICAgICBjb2xvcjogIzU1NTtcclxuLy8gICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbi8vICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4vLyAgICAgfVxyXG4vLyAgICAgLnByb2R1Y3QtZGV0YWlse1xyXG4gICAgICAgXHJcbi8vICAgICAgICAgbGFiZWx7XHJcbi8vICAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbi8vICAgICAgICAgfVxyXG4vLyAgICAgICAgIGxhYmVsK2RpdntcclxuLy8gICAgICAgICAgICAgYmFja2dyb3VuZDogI2YwZjBmMDtcclxuLy8gICAgICAgICAgICAgZm9udC13ZWlnaHQ6bm9ybWE7XHJcbi8vICAgICAgICAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbi8vICAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbi8vICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbi8vICAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDI3cHg7XHJcbi8vICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuLy8gICAgICAgICAgICAgY29sb3I6ICM2NjY7XHJcbi8vICAgICAgICAgfVxyXG4vLyAgICAgICAgIC5pbWFnZXtcclxuLy8gICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuLy8gICAgICAgICAgICAgPmRpdntcclxuLy8gICAgICAgICAgICAgICAgIHdpZHRoOiAzMy4zMzMzJTtcclxuLy8gICAgICAgICAgICAgICAgIHBhZGRpbmc6IDVweDtcclxuLy8gICAgICAgICAgICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4vLyAgICAgICAgICAgICB9XHJcbi8vICAgICAgICAgICAgIC5jYXJkLWhlYWRlcntcclxuLy8gICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XHJcbi8vICAgICAgICAgICAgIH1cclxuLy8gICAgICAgICAgICAgaDN7XHJcbi8vICAgICAgICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbi8vICAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbi8vICAgICAgICAgICAgICAgICBjb2xvcjogIzU1NTtcclxuLy8gICAgICAgICAgICAgfVxyXG4vLyAgICAgICAgICAgICAuY2FyZC1jb250ZW50e1xyXG4vLyAgICAgICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuLy8gICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4vLyAgICAgICAgICAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4vLyAgICAgICAgICAgICAgICAgaW1ne1xyXG4vLyAgICAgICAgICAgICAgICAgICAgIG1heC13aWR0aDogMTAwJTtcclxuLy8gICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgIFxyXG4vLyAgICAgICAgICAgICAgICAgfVxyXG4vLyAgICAgICAgICAgICB9XHJcbi8vICAgICAgICAgfVxyXG5cclxuLy8gICAgICAgICAuY2FyZC1oZWFkZXJ7IFxyXG4vLyAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG4vLyAgICAgICAgICAgICBoMntcclxuLy8gICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuLy8gICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuLy8gICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiB1bnNldDtcclxuLy8gICAgICAgICAgICAgICAgIGhlaWdodDogdW5zZXQ7XHJcbi8vICAgICAgICAgICAgICAgICBtYXJnaW46IDBweDtcclxuLy8gICAgICAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgICAgIFxyXG4vLyAgICAgICAgICAgICB9XHJcbi8vICAgICAgICAgfVxyXG4vLyAgICAgfVxyXG4vLyB9XHJcblxyXG4vLyAuZmlsZS11cGxvYWR7XHJcbi8vICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbi8vIH1cclxuXHJcbi8vICNpbWd7XHJcbi8vICAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4vLyB9IiwiLmFkZC1wcm9kdWN0IHtcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEyNSk7XG4gIGJvcmRlci1yYWRpdXM6IDAuMjVyZW07XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBtYXJnaW46IDIwcHg7XG59XG4uYWRkLXByb2R1Y3QgLmUtd3JhcHBlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmhlYWRpbmcge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBsZXR0ZXItc3BhY2luZzogMC44cHg7XG4gIG1hcmdpbjogMjBweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/product-voucher/add/product.add.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/layout/modules/product-voucher/add/product.add.component.ts ***!
  \*****************************************************************************/
/*! exports provided: ProductAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductAddComponent", function() { return ProductAddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ckeditor/ckeditor5-build-classic */ "./node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var ProductAddComponent = /** @class */ (function () {
    function ProductAddComponent(productService, router) {
        this.productService = productService;
        this.router = router;
        this.name = "";
        this.categoryProduct = [];
        this.productType = [];
        this.merchantList = [];
        this.merchant_group = [];
        this.condition = [
            { label: "NEW", value: "new" },
            { label: "SECOND", value: "second" },
        ];
        this.redeem_type = [
            { label: "Voucher Link", value: "voucher_link" },
            { label: "Voucher Code", value: "voucher_code" }
        ];
        // condition:any;
        this.Products = [];
        this.loading = false;
        /* public productStatus = [
          {label:"ACTIVE", value:"ACTIVE", selected:1}
         ,{label:"INACTIVE", value:"INACTIVE"}
       ]; */
        this.toggleDelete = 0;
        this.selectedFile = null;
        this.progressBar = 0;
        this.cancel = false;
        this.errorFile = false;
        this.errorLabel = false;
        this.newData = "JUmp start";
        this.data = {
            merchant_username: '',
            type: '',
            category: [],
            price: 0,
            fixed_price: 0,
            tnc: '',
            description: '',
            qty: 0,
            product_name: '',
            product_code: '',
            condition: '',
            weight: '',
            dimensions: {
                length: 0,
                width: 0,
                height: 0
            },
            // dimension_length: '',
            // dimension_width: '',
            // dimension_height: '',
            min_order: '',
        };
        this.Editor = _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_3___default.a;
    }
    ProductAddComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    ProductAddComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.getCategory();
                // this.getProductType();
                this.getMerchant();
                this.getKeyname();
                console.log(this.merchant_group);
                return [2 /*return*/];
            });
        });
    };
    ProductAddComponent.prototype.getCategory = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.productService.getCategoryLint()];
                    case 1:
                        _a.category = _b.sent();
                        this.category.result.forEach(function (element) {
                            _this.categoryProduct.push({ label: element.name, value: element.name });
                        });
                        this.category = this.categoryProduct[0].value;
                        return [2 /*return*/];
                }
            });
        });
    };
    ProductAddComponent.prototype.getKeyname = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.productService.configGroup()];
                    case 1:
                        _a.keyname = _b.sent();
                        // console.log("PERBEDAAN MERCHANT GROUP/ PRODUCTCONFIG", this.keyname)l
                        this.keyname.result.merchant_group.forEach(function (element) {
                            _this.merchant_group.push({ label: element.name, value: element.name });
                        });
                        this.data.merchant_group = this.merchant_group[0].value;
                        return [2 /*return*/];
                }
            });
        });
    };
    // async redeemGroup(){
    //   this.keyname = await this.productService.redeemGroup();
    // }
    ProductAddComponent.prototype.getProductType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.productService.configuration()];
                    case 1:
                        _a.product_type = _b.sent();
                        console.log("KELUAR GA:", this.product_type);
                        this.product_type.result.product_type.forEach(function (element) {
                            _this.productType.push({ label: element, value: element });
                        });
                        this.data.type = this.productType[0].value;
                        return [2 /*return*/];
                }
            });
        });
    };
    ProductAddComponent.prototype.getRedeemType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.productService.configuration()];
                    case 1:
                        _a.product_type = _b.sent();
                        // console.log("KELUAR GA:", this.product_type);
                        this.product_type.result.evoucher_redeem.forEach(function (element) {
                            _this.productType.push({ label: element, value: element });
                        });
                        this.data.type = this.productType[0].value;
                        return [2 /*return*/];
                }
            });
        });
    };
    ProductAddComponent.prototype.getConditionType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.productService.configuration()];
                    case 1:
                        _a.product_type = _b.sent();
                        this.product_type.result.product_condition.forEach(function (element) {
                            _this.productType.push({ label: element, value: element });
                            console.log("DISINI", _this.productType);
                        });
                        this.data.type = this.productType[0].value;
                        return [2 /*return*/];
                }
            });
        });
    };
    ProductAddComponent.prototype.getMerchant = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.productService.getMerchantList()];
                    case 1:
                        _a.merchant_username = _b.sent();
                        this.merchant_username.result.forEach(function (element) {
                            _this.merchantList.push({ label: element.merchant_name, value: element.merchant_username });
                        });
                        this.data.merchant_username = this.merchantList[0].value;
                        return [2 /*return*/];
                }
            });
        });
    };
    ProductAddComponent.prototype.selectCategory = function (event) {
        this.category = event.target.value;
        this.data.category = [event.target.value];
    };
    ProductAddComponent.prototype.saveThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        // console.log(this.dimensions)
                        this.data.price = parseInt(this.data.price);
                        this.data.fixed_price = parseInt(this.data.fixed_price);
                        this.data.min_order = parseInt(this.data.min_order);
                        if (this.data.type == 'product') {
                            this.data.weight = parseFloat(this.data.weight);
                            this.data.dimensions.length = parseFloat(this.data.dimensions.length);
                            this.data.dimensions.width = parseFloat(this.data.dimensions.width);
                            this.data.dimensions.height = parseFloat(this.data.dimensions.height);
                            delete this.data.merchant_group;
                        }
                        else {
                            delete this.data.weight;
                            delete this.data.dimensions;
                            delete this.data.condition;
                        }
                        this.data.qty = parseInt(this.data.qty);
                        this.service = this.productService;
                        return [4 /*yield*/, this.productService.addProductsLint(this.data)];
                    case 1:
                        result = _a.sent();
                        this.Products = result.result;
                        alert("Product has been added!");
                        this.router.navigate(['administrator/productadmin/edit'], { queryParams: { id: this.data._id } });
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ProductAddComponent.prototype.onFileSelected = function (event) {
        var _this = this;
        try {
            this.errorFile = false;
            var fileMaxSize_1 = 3000000; // let say 3Mb
            Array.from(event.target.files).forEach(function (file) {
                if (file.size > fileMaxSize_1) {
                    _this.errorFile = "Maximum File Upload is 3MB";
                }
            });
            this.selectedFile = event.target.files[0];
        }
        catch (e) {
            this.errorLabel = (e.message); //conversion to Error type
        }
    };
    ProductAddComponent.prototype.onSelectFile = function (event) {
        var _this = this;
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(event.target.files[0]); // read file as data url
            reader.onload = function (event) {
                var _event = event;
                _this.url = _event.target.result;
            };
        }
    };
    ProductAddComponent.prototype.updateProgressBar = function (value) {
        this.progressBar = value;
    };
    ProductAddComponent.prototype.cancelThis = function () {
        this.cancel = !this.cancel;
    };
    ProductAddComponent.prototype.onUpload = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, product, e_2;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.cancel = false;
                        result = void 0;
                        if (!this.selectedFile) return [3 /*break*/, 2];
                        console.log("result", result);
                        return [4 /*yield*/, this.productService.upload(this.selectedFile, this, 'image', function (result) { _this.callAfterUpload(result); })];
                    case 1:
                        result = _a.sent();
                        _a.label = 2;
                    case 2:
                        if (result) {
                            product = this.detail;
                            this.detail = __assign({}, product, result);
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: 
                    //   this.onUpload.
                    return [2 /*return*/, false];
                }
            });
        });
    };
    ProductAddComponent.prototype.callAfterUpload = function (result) {
        this.data = __assign({}, this.data, result);
    };
    ProductAddComponent.prototype.backToDetail = function () {
        window.history.back();
        // this.back[0][this.back[1]]();
    };
    ProductAddComponent.ctorParameters = function () { return [
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ProductAddComponent.prototype, "back", void 0);
    ProductAddComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product-add',
            template: __webpack_require__(/*! raw-loader!./product.add.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/product-voucher/add/product.add.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./product.add.component.scss */ "./src/app/layout/modules/product-voucher/add/product.add.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_product_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], ProductAddComponent);
    return ProductAddComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/product-voucher/detail/product.detail.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/layout/modules/product-voucher/detail/product.detail.component.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg-delete {\n  background-color: #ffe3f3;\n  padding: 10px;\n}\n\n.card-detail > .card-header {\n  position: relative;\n}\n\n.card-detail > .card-header .back_button {\n  position: absolute;\n  top: 8px;\n  left: 15px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .edit_button {\n  position: absolute;\n  top: 8px;\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  right: 10px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.card-detail > .card-header .edit_button i {\n  font-weight: bold;\n}\n\n.card-detail .card-content {\n  padding: 15px;\n}\n\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n\n.card-detail .product-detail form-select[name=status] .form-group {\n  margin-bottom: 0px;\n}\n\n.card-detail .product-detail label {\n  margin-top: 10px;\n}\n\n.card-detail .product-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n\n.card-detail .product-detail .image {\n  overflow: hidden;\n}\n\n.card-detail .product-detail .image > div {\n  width: 33.3333%;\n  padding: 5px;\n  float: left;\n}\n\n.card-detail .product-detail .image .card-header {\n  background-color: #f0f0f0;\n}\n\n.card-detail .product-detail .image h3 {\n  font-size: 12px;\n  margin-bottom: 0px;\n  color: #555;\n}\n\n.card-detail .product-detail .image .card-content {\n  overflow: hidden;\n  width: 100%;\n  height: auto;\n}\n\n.card-detail .product-detail .image .card-content img {\n  max-width: 100%;\n  width: 100%;\n}\n\n.card-detail .product-detail .card-header {\n  background-color: #555;\n}\n\n.card-detail .product-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcHJvZHVjdC12b3VjaGVyL2RldGFpbC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXHByb2R1Y3Qtdm91Y2hlclxcZGV0YWlsXFxwcm9kdWN0LmRldGFpbC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcHJvZHVjdC12b3VjaGVyL2RldGFpbC9wcm9kdWN0LmRldGFpbC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHlCQUFBO0VBQ0EsYUFBQTtBQ0NKOztBREdJO0VBQ0ksa0JBQUE7QUNBUjs7QURDUTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7RUFDQSw2QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEseUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0FDQVo7O0FEQ1k7RUFDSSxpQkFBQTtBQ0NoQjs7QURFUTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLHNCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQ0FaOztBREVZO0VBQ0ksaUJBQUE7QUNBaEI7O0FES0k7RUFDSSxhQUFBO0FDSFI7O0FESVE7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0FDRlo7O0FETUk7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQ0pSOztBRE9RO0VBQ0ksa0JBQUE7QUNMWjs7QURPUTtFQUNJLGdCQUFBO0FDTFo7O0FET1E7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDTFo7O0FEUVE7RUFDSSxnQkFBQTtBQ05aOztBRE9ZO0VBQ0ksZUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FDTGhCOztBRE9ZO0VBQ0kseUJBQUE7QUNMaEI7O0FET1k7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FDTGhCOztBRE9ZO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQ0xoQjs7QURNZ0I7RUFDSSxlQUFBO0VBQ0EsV0FBQTtBQ0pwQjs7QURVUTtFQUNJLHNCQUFBO0FDUlo7O0FEU1k7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSwwQkFBQTtBQ1BoQiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL3Byb2R1Y3Qtdm91Y2hlci9kZXRhaWwvcHJvZHVjdC5kZXRhaWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmctZGVsZXRle1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTNmMztcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbn1cclxuXHJcbi5jYXJkLWRldGFpbHtcclxuICAgID4uY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIC5iYWNrX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDhweDtcclxuICAgICAgICAgICAgbGVmdDogMTVweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5lZGl0X2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDhweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4IDBweCAxMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICBcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmNhcmQtY29udGVudHtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgID4uY29sLW1kLTEye1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIFxyXG4gICAgaDF7XHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICB9XHJcbiAgICAucHJvZHVjdC1kZXRhaWx7XHJcbiAgICAgICAgZm9ybS1zZWxlY3RbbmFtZT1zdGF0dXNdIC5mb3JtLWdyb3VwIHtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYWJlbHtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWwrZGl2e1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDpub3JtYTtcclxuICAgICAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgICAgICAgICAgbWluLWhlaWdodDogMjdweDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICBjb2xvcjogIzY2NjtcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgLmltYWdle1xyXG4gICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICA+ZGl2e1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDMzLjMzMzMlO1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICAgICAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLmNhcmQtaGVhZGVye1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBoM3tcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgICAgICAgICAgICBpbWd7XHJcbiAgICAgICAgICAgICAgICAgICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5jYXJkLWhlYWRlcnsgXHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGgye1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0iLCIuYmctZGVsZXRlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTNmMztcbiAgcGFkZGluZzogMTBweDtcbn1cblxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA4cHg7XG4gIGxlZnQ6IDE1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBjb2xvcjogIzU1NTtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuYmFja19idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5lZGl0X2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA4cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG4gIGNvbG9yOiAjZmZmO1xuICBoZWlnaHQ6IDMwcHg7XG4gIHJpZ2h0OiAxMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmVkaXRfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogMTVweDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50ID4gLmNvbC1tZC0xMiB7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgaDEge1xuICBmb250LXNpemU6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzU1NTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLnByb2R1Y3QtZGV0YWlsIGZvcm0tc2VsZWN0W25hbWU9c3RhdHVzXSAuZm9ybS1ncm91cCB7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5jYXJkLWRldGFpbCAucHJvZHVjdC1kZXRhaWwgbGFiZWwge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5wcm9kdWN0LWRldGFpbCBsYWJlbCArIGRpdiB7XG4gIGJhY2tncm91bmQ6ICNmMGYwZjA7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYTtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgbWluLWhlaWdodDogMjdweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzY2Njtcbn1cbi5jYXJkLWRldGFpbCAucHJvZHVjdC1kZXRhaWwgLmltYWdlIHtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbi5jYXJkLWRldGFpbCAucHJvZHVjdC1kZXRhaWwgLmltYWdlID4gZGl2IHtcbiAgd2lkdGg6IDMzLjMzMzMlO1xuICBwYWRkaW5nOiA1cHg7XG4gIGZsb2F0OiBsZWZ0O1xufVxuLmNhcmQtZGV0YWlsIC5wcm9kdWN0LWRldGFpbCAuaW1hZ2UgLmNhcmQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjBmMDtcbn1cbi5jYXJkLWRldGFpbCAucHJvZHVjdC1kZXRhaWwgLmltYWdlIGgzIHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIGNvbG9yOiAjNTU1O1xufVxuLmNhcmQtZGV0YWlsIC5wcm9kdWN0LWRldGFpbCAuaW1hZ2UgLmNhcmQtY29udGVudCB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IGF1dG87XG59XG4uY2FyZC1kZXRhaWwgLnByb2R1Y3QtZGV0YWlsIC5pbWFnZSAuY2FyZC1jb250ZW50IGltZyB7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG4uY2FyZC1kZXRhaWwgLnByb2R1Y3QtZGV0YWlsIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLnByb2R1Y3QtZGV0YWlsIC5jYXJkLWhlYWRlciBoMiB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBsaW5lLWhlaWdodDogdW5zZXQ7XG4gIGhlaWdodDogdW5zZXQ7XG4gIG1hcmdpbjogMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/product-voucher/detail/product.detail.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/layout/modules/product-voucher/detail/product.detail.component.ts ***!
  \***********************************************************************************/
/*! exports provided: ProductDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailComponent", function() { return ProductDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var ProductDetailComponent = /** @class */ (function () {
    function ProductDetailComponent(productService, route, router) {
        this.productService = productService;
        this.route = route;
        this.router = router;
        this.edit = false;
        this.errorLabel = false;
    }
    ProductDetailComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    ProductDetailComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.route.queryParams.subscribe(function (params) { return __awaiter(_this, void 0, void 0, function () {
                    var historyID;
                    return __generator(this, function (_a) {
                        historyID = params.id;
                        this.loadDetail(historyID);
                        return [2 /*return*/];
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    ProductDetailComponent.prototype.loadDetail = function (historyID) {
        return __awaiter(this, void 0, void 0, function () {
            var result, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.productService;
                        return [4 /*yield*/, this.productService.detailProducts(historyID)];
                    case 1:
                        result = _a.sent();
                        this.productDetail = result.result[0];
                        console.log(this.productDetail);
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ProductDetailComponent.ctorParameters = function () { return [
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ProductDetailComponent.prototype, "back", void 0);
    ProductDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product-detail',
            template: __webpack_require__(/*! raw-loader!./product.detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/product-voucher/detail/product.detail.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./product.detail.component.scss */ "./src/app/layout/modules/product-voucher/detail/product.detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_product_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], ProductDetailComponent);
    return ProductDetailComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/product-voucher/edit/product.edit.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/layout/modules/product-voucher/edit/product.edit.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".edit-product {\n  border: 1px solid rgba(0, 0, 0, 0.125);\n  border-radius: 0.25rem;\n  background-color: white;\n}\n.edit-product .e-wrapper {\n  position: relative;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcHJvZHVjdC12b3VjaGVyL2VkaXQvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxwcm9kdWN0LXZvdWNoZXJcXGVkaXRcXHByb2R1Y3QuZWRpdC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcHJvZHVjdC12b3VjaGVyL2VkaXQvcHJvZHVjdC5lZGl0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksc0NBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0FDQ0o7QURBSTtFQUNJLGtCQUFBO0FDRVIiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9wcm9kdWN0LXZvdWNoZXIvZWRpdC9wcm9kdWN0LmVkaXQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZWRpdC1wcm9kdWN0e1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEyNSk7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwLjI1cmVtO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICAuZS13cmFwcGVye1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIH1cclxufSIsIi5lZGl0LXByb2R1Y3Qge1xuICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMTI1KTtcbiAgYm9yZGVyLXJhZGl1czogMC4yNXJlbTtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG4uZWRpdC1wcm9kdWN0IC5lLXdyYXBwZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/product-voucher/edit/product.edit.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/layout/modules/product-voucher/edit/product.edit.component.ts ***!
  \*******************************************************************************/
/*! exports provided: ProductEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductEditComponent", function() { return ProductEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ckeditor/ckeditor5-build-classic */ "./node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var ProductEditComponent = /** @class */ (function () {
    // listedData: any[];
    function ProductEditComponent(productService, modalService) {
        this.productService = productService;
        this.modalService = modalService;
        this.selectedFile = null;
        this.loading = false;
        this.categoryProduct = [];
        this.editStatus = [
            { label: "ACTIVE", value: 1 },
            { label: "INACTIVE", value: 0 }
        ];
        this.errorLabel = false;
        this.cancel = false;
        this.errorFile = false;
        this.product_code = [];
        this.progressBar = 0;
        this.product_sku = [];
        this.Editor = _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_3___default.a;
        this.productData = {};
    }
    ProductEditComponent.prototype.ngOnInit = function () {
        this.getCategory();
        this.firstLoad();
        // this.setStatus()
    };
    ProductEditComponent.prototype.getCategory = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.productService.getCategoryLint()];
                    case 1:
                        _a.category = _b.sent();
                        this.category.result.forEach(function (element) {
                            _this.categoryProduct.push({ label: element.name, value: element.name });
                        });
                        this.category = this.categoryProduct[0].value;
                        return [2 /*return*/];
                }
            });
        });
    };
    ProductEditComponent.prototype.selectCategory = function (event) {
        this.category = event.target.value;
        this.productData.category = [event.target.value];
    };
    ProductEditComponent.prototype.onUpload = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.cancel = false;
                        result = void 0;
                        if (!this.selectedFile) return [3 /*break*/, 2];
                        console.log("result", result);
                        return [4 /*yield*/, this.productService.upload(this.selectedFile, this, 'image', function (result) { _this.callAfterUpload(result); })];
                    case 1:
                        result = _a.sent();
                        _a.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ProductEditComponent.prototype.callAfterUpload = function (result) {
        this.productData = __assign({}, this.productData, result);
        console.log("THE RESULT", result);
        console.log("this.productData", this.productData);
    };
    ProductEditComponent.prototype.setStatus = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.productService.statusEdit];
                    case 1:
                        _a.service = _b.sent();
                        this.productData.previous_status = this.productData.status;
                        this.editStatus.forEach(function (element, index) {
                            if (element.value == _this.productData.active) {
                                _this.editStatus[index].selected = 1;
                            }
                            if (element.value == _this.productData.active) {
                                _this.editStatus[index].selected = 0;
                            }
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ProductEditComponent.prototype.cancelThis = function () {
        this.cancel = !this.cancel;
    };
    ProductEditComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                try {
                    this.detail.previous_product_code = this.detail.product_code;
                    if (this.detail._id)
                        delete this.detail._id;
                    this.categoryProduct.forEach(function (element, index, products_ids) {
                        if (element.value == _this.detail.status) {
                            _this.categoryProduct[index].selected = 1;
                        }
                    });
                    console.log("SKU", this.detail.product_sku);
                    Object.keys(this.detail).forEach(function (key) {
                        _this.productData[key] = _this.detail[key];
                    });
                    console.log("product data : ", this.productData);
                }
                catch (e) {
                    this.errorLabel = (e.message); //conversion to Error type
                }
                return [2 /*return*/];
            });
        });
    };
    ProductEditComponent.prototype.removeSKU = function (sku) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.log("sku : ", sku);
                return [2 /*return*/];
            });
        });
    };
    ProductEditComponent.prototype.backToDetail = function () {
        history.back();
    };
    ProductEditComponent.prototype.saveThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var prevProductCode, result, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.productData.min_order = parseInt(this.productData.min_order);
                        if (this.productData.type == 'Product') {
                            this.productData.dimensions.length = parseInt(this.productData.dimensions.length);
                            this.productData.dimensions.width = parseInt(this.productData.dimensions.width);
                            this.productData.dimensions.height = parseInt(this.productData.dimensions.height);
                        }
                        this.productData.price = parseInt(this.productData.price);
                        this.productData.fixed_price = parseInt(this.productData.fixed_price);
                        this.productData.qty = parseInt(this.productData.qty);
                        prevProductCode = this.productData.previous_product_code;
                        delete this.productData.previous_product_code;
                        delete this.productData.status;
                        console.log("RESPON", this.productData);
                        if (this.productData.active)
                            this.productData.active = parseInt(this.productData.active);
                        this.loading = !this.loading;
                        return [4 /*yield*/, this.productService.updateProduct(this.productData, prevProductCode)];
                    case 1:
                        result = _a.sent();
                        if (result) {
                            alert("Saved!");
                        }
                        // console.log("this.detail", this.detail)
                        this.loading = !this.loading;
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        alert(this.errorLabel = (e_2.message)); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    // async saveSKU(sku){
    //   var item = this.detail.product_sku;
    //   var push = item.push(sku);
    //   console.log("Berhasil ga", push)
    // }
    //  
    ProductEditComponent.prototype.onFileSelected = function (event) {
        var _this = this;
        this.errorFile = false;
        var fileMaxSize = 3000000; // let say 3Mb
        Array.from(event.target.files).forEach(function (file) {
            if (file.size > fileMaxSize) {
                _this.errorFile = "maximum file is 3Mb";
            }
        });
        this.selectedFile = event.target.files[0];
    };
    ProductEditComponent.ctorParameters = function () { return [
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ProductEditComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ProductEditComponent.prototype, "back", void 0);
    ProductEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product-edit',
            template: __webpack_require__(/*! raw-loader!./product.edit.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/product-voucher/edit/product.edit.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./product.edit.component.scss */ "./src/app/layout/modules/product-voucher/edit/product.edit.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_product_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"]])
    ], ProductEditComponent);
    return ProductEditComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/product-voucher/voucher-routing.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/layout/modules/product-voucher/voucher-routing.module.ts ***!
  \**************************************************************************/
/*! exports provided: VouchersRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VouchersRoutingModule", function() { return VouchersRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _add_product_add_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./add/product.add.component */ "./src/app/layout/modules/product-voucher/add/product.add.component.ts");
/* harmony import */ var _edit_product_edit_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./edit/product.edit.component */ "./src/app/layout/modules/product-voucher/edit/product.edit.component.ts");
/* harmony import */ var _voucher_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./voucher.component */ "./src/app/layout/modules/product-voucher/voucher.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    {
        path: '', component: _voucher_component__WEBPACK_IMPORTED_MODULE_4__["VouchersComponent"]
    },
    {
        path: 'add', component: _add_product_add_component__WEBPACK_IMPORTED_MODULE_2__["ProductAddComponent"]
    },
    // {
    //   path:'detail', component: ProductDetailComponent
    // },
    {
        path: 'edit', component: _edit_product_edit_component__WEBPACK_IMPORTED_MODULE_3__["ProductEditComponent"]
    }
];
var VouchersRoutingModule = /** @class */ (function () {
    function VouchersRoutingModule() {
    }
    VouchersRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], VouchersRoutingModule);
    return VouchersRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/product-voucher/voucher.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/layout/modules/product-voucher/voucher.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".pb-framer {\n  text-align: center;\n  position: relative;\n  height: 25px;\n  color: #e67e22;\n}\n.pb-framer .progressbar {\n  overflow: hidden;\n  background: #3498db;\n  position: absolute;\n  height: 25px;\n  z-index: -1;\n}\n.pb-framer .terminated.progressbar {\n  background-color: red;\n}\n.pb-framer .clr-white {\n  color: white;\n}\nh1 {\n  color: white;\n}\n#approve {\n  border: 1px solid #4b5872;\n  display: inline-block;\n  margin-right: 15px;\n  font-weight: 400;\n  text-align: center;\n  white-space: nowrap;\n  vertical-align: middle;\n  padding: 0.375rem 0.75rem;\n  font-size: 1rem;\n  line-height: 1.5;\n  border-radius: 0.25rem;\n}\n.fa {\n  margin-right: 5px;\n}\n.error_label {\n  color: white;\n}\n.custom-content::-webkit-file-upload-button {\n  visibility: hidden;\n}\n.custom-content::before {\n  content: \"Choose File\";\n  margin-right: 5px;\n  display: inline-block;\n  border: 1px solid #999;\n  border-radius: 5px;\n  padding: 8px;\n  outline: none;\n  white-space: nowrap;\n  -webkit-user-select: none;\n  cursor: pointer;\n  font-weight: 700;\n  font-size: 10pt;\n  color: white;\n  letter-spacing: 0.5px;\n}\nmark {\n  background-color: #dc3545;\n  color: white;\n}\n.rounded-btn {\n  border-radius: 30px;\n  color: white;\n  font-size: 15px;\n  line-height: 40px;\n  padding: 0 15px;\n  background: #56a4ff;\n}\n.rounded-btn:hover {\n  background: #0a7bff;\n  outline: none;\n}\n.rounded-btn-danger {\n  border-radius: 30px;\n  color: white;\n  font-size: 15px;\n  line-height: 40px;\n  padding: 0 15px;\n  background: #dc3545;\n}\n.rounded-btn-danger:hover {\n  background: #a71d2a;\n  outline: none;\n}\n@media screen and (max-width: 600px) {\n  .rounded-btn {\n    border-radius: 30px;\n    color: white;\n    font-size: 15px;\n    line-height: 30px;\n    padding: 0 12px;\n    background: #56a4ff;\n    margin-left: 25%;\n    margin-top: 10px;\n    margin-right: 10px;\n  }\n\n  .rounded-btn:hover {\n    background: #0a7bff;\n    outline: none;\n  }\n\n  .rounded-btn-danger {\n    border-radius: 30px;\n    color: white;\n    font-size: 15px;\n    line-height: 30px;\n    padding: 0 12px;\n    background: #dc3545;\n    margin-right: 25%;\n    margin-top: 10px;\n  }\n\n  .rounded-btn-danger:hover {\n    background: #a71d2a;\n    outline: none;\n  }\n}\n.error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 26px;\n  padding-top: 40px;\n}\n.error-message .text-message {\n  background: #E74C3C;\n  padding: 20px;\n  margin: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcHJvZHVjdC12b3VjaGVyL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xccHJvZHVjdC12b3VjaGVyXFx2b3VjaGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9wcm9kdWN0LXZvdWNoZXIvdm91Y2hlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtBQ0NGO0FEQUU7RUFDRSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQ0VKO0FEQUU7RUFDRSxxQkFBQTtBQ0VKO0FEQUU7RUFDRSxZQUFBO0FDRUo7QURFQTtFQUNFLFlBQUE7QUNDRjtBREVBO0VBQ0UseUJBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSx5QkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLHNCQUFBO0FDQ0Y7QURDQTtFQUNFLGlCQUFBO0FDRUY7QURFQTtFQUNFLFlBQUE7QUNDRjtBREVBO0VBQ0ksa0JBQUE7QUNDSjtBRENBO0VBQ0Usc0JBQUE7RUFDQSxpQkFBQTtFQUNBLHFCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EscUJBQUE7QUNFRjtBRENBO0VBQ0UseUJBQUE7RUFDQSxZQUFBO0FDRUY7QURDQTtFQUVFLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtBQ0VGO0FEQUE7RUFDRSxtQkFBQTtFQUVBLGFBQUE7QUNFRjtBRENBO0VBRUUsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FDRUY7QURBQTtFQUNFLG1CQUFBO0VBRUEsYUFBQTtBQ0VGO0FEQ0E7RUFDRTtJQUVFLG1CQUFBO0lBQ0EsWUFBQTtJQUNBLGVBQUE7SUFDQSxpQkFBQTtJQUNBLGVBQUE7SUFDQSxtQkFBQTtJQUNBLGdCQUFBO0lBQ0EsZ0JBQUE7SUFDQSxrQkFBQTtFQ0VGOztFREFBO0lBQ0UsbUJBQUE7SUFFQSxhQUFBO0VDRUY7O0VEQ0E7SUFFRSxtQkFBQTtJQUNBLFlBQUE7SUFDQSxlQUFBO0lBQ0EsaUJBQUE7SUFDQSxlQUFBO0lBQ0EsbUJBQUE7SUFDQSxpQkFBQTtJQUNBLGdCQUFBO0VDRUY7O0VEQ0E7SUFDRSxtQkFBQTtJQUVBLGFBQUE7RUNDRjtBQUNGO0FERUE7RUFDRSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ0FGO0FERUU7RUFDSSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0FDQU4iLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9wcm9kdWN0LXZvdWNoZXIvdm91Y2hlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5wYi1mcmFtZXIge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgaGVpZ2h0OiAyNXB4O1xyXG4gIGNvbG9yOiAjZTY3ZTIyO1xyXG4gIC5wcm9ncmVzc2JhciB7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgYmFja2dyb3VuZDogIzM0OThkYjtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGhlaWdodDogMjVweDtcclxuICAgIHotaW5kZXg6IC0xO1xyXG4gIH1cclxuICAudGVybWluYXRlZC5wcm9ncmVzc2JhciB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XHJcbiAgfVxyXG4gIC5jbHItd2hpdGUge1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gIH1cclxufVxyXG5cclxuaDEge1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuI2FwcHJvdmUge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICM0YjU4NzI7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIG1hcmdpbi1yaWdodDogMTVweDtcclxuICBmb250LXdlaWdodDogNDAwO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbiAgcGFkZGluZzogMC4zNzVyZW0gMC43NXJlbTtcclxuICBmb250LXNpemU6IDFyZW07XHJcbiAgbGluZS1oZWlnaHQ6IDEuNTtcclxuICBib3JkZXItcmFkaXVzOiAwLjI1cmVtO1xyXG59XHJcbi5mYSB7XHJcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbiAgLy8gLy9jb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi5lcnJvcl9sYWJlbCB7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4uY3VzdG9tLWNvbnRlbnQ6Oi13ZWJraXQtZmlsZS11cGxvYWQtYnV0dG9uIHtcclxuICAgIHZpc2liaWxpdHk6IGhpZGRlbjtcclxufVxyXG4uY3VzdG9tLWNvbnRlbnQ6OmJlZm9yZSB7XHJcbiAgY29udGVudDogXCJDaG9vc2UgRmlsZVwiO1xyXG4gIG1hcmdpbi1yaWdodDogNXB4O1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICBib3JkZXI6IDFweCBzb2xpZCAjOTk5O1xyXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICBwYWRkaW5nOiA4cHg7XHJcbiAgb3V0bGluZTogbm9uZTtcclxuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gIC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgZm9udC1zaXplOiAxMHB0O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBsZXR0ZXItc3BhY2luZzogMC41cHg7XHJcbn1cclxuXHJcbm1hcmsge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNkYzM1NDU7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4ucm91bmRlZC1idG4ge1xyXG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogMzBweDtcclxuICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XHJcbiAgcGFkZGluZzogMCAxNXB4O1xyXG4gIGJhY2tncm91bmQ6ICM1NmE0ZmY7XHJcbn1cclxuLnJvdW5kZWQtYnRuOmhvdmVyIHtcclxuICBiYWNrZ3JvdW5kOiBkYXJrZW4oIzU2YTRmZiwgMTUlKTtcclxuICAvLyBib3JkZXI6IDJweCBzb2xpZCBibHVlO1xyXG4gIG91dGxpbmU6IG5vbmU7XHJcbn1cclxuXHJcbi5yb3VuZGVkLWJ0bi1kYW5nZXIge1xyXG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogMzBweDtcclxuICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XHJcbiAgcGFkZGluZzogMCAxNXB4O1xyXG4gIGJhY2tncm91bmQ6ICNkYzM1NDU7XHJcbn1cclxuLnJvdW5kZWQtYnRuLWRhbmdlcjpob3ZlciB7XHJcbiAgYmFja2dyb3VuZDogZGFya2VuKCNkYzM1NDUsIDE1JSk7XHJcbiAgLy8gYm9yZGVyOiAycHggc29saWQgYmx1ZTtcclxuICBvdXRsaW5lOiBub25lO1xyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA2MDBweCkge1xyXG4gIC5yb3VuZGVkLWJ0biB7XHJcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDMwcHg7XHJcbiAgICBwYWRkaW5nOiAwIDEycHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjNTZhNGZmO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDI1JTtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgfVxyXG4gIC5yb3VuZGVkLWJ0bjpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kOiBkYXJrZW4oIzU2YTRmZiwgMTUlKTtcclxuICAgIC8vIGJvcmRlcjogMnB4IHNvbGlkIGJsdWU7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gIH1cclxuXHJcbiAgLnJvdW5kZWQtYnRuLWRhbmdlciB7XHJcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDMwcHg7XHJcbiAgICBwYWRkaW5nOiAwIDEycHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZGMzNTQ1O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAyNSU7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gIH1cclxuXHJcbiAgLnJvdW5kZWQtYnRuLWRhbmdlcjpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kOiBkYXJrZW4oI2RjMzU0NSwgMTUlKTtcclxuICAgIC8vIGJvcmRlcjogMnB4IHNvbGlkIGJsdWU7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gIH1cclxufVxyXG5cclxuLmVycm9yLW1lc3NhZ2Uge1xyXG4gIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gIGhlaWdodDoxMDB2aDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgZm9udC1zaXplOjI2cHg7XHJcbiAgcGFkZGluZy10b3A6IDQwcHg7XHJcblxyXG4gIC50ZXh0LW1lc3NhZ2Uge1xyXG4gICAgICBiYWNrZ3JvdW5kOiAjRTc0QzNDO1xyXG4gICAgICBwYWRkaW5nOiAyMHB4O1xyXG4gICAgICBtYXJnaW46IDAgMTBweDtcclxuICB9XHJcbn0iLCIucGItZnJhbWVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGhlaWdodDogMjVweDtcbiAgY29sb3I6ICNlNjdlMjI7XG59XG4ucGItZnJhbWVyIC5wcm9ncmVzc2JhciB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGJhY2tncm91bmQ6ICMzNDk4ZGI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgaGVpZ2h0OiAyNXB4O1xuICB6LWluZGV4OiAtMTtcbn1cbi5wYi1mcmFtZXIgLnRlcm1pbmF0ZWQucHJvZ3Jlc3NiYXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XG59XG4ucGItZnJhbWVyIC5jbHItd2hpdGUge1xuICBjb2xvcjogd2hpdGU7XG59XG5cbmgxIHtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4jYXBwcm92ZSB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICM0YjU4NzI7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xuICBmb250LXdlaWdodDogNDAwO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gIHBhZGRpbmc6IDAuMzc1cmVtIDAuNzVyZW07XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgbGluZS1oZWlnaHQ6IDEuNTtcbiAgYm9yZGVyLXJhZGl1czogMC4yNXJlbTtcbn1cblxuLmZhIHtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5lcnJvcl9sYWJlbCB7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmN1c3RvbS1jb250ZW50Ojotd2Via2l0LWZpbGUtdXBsb2FkLWJ1dHRvbiB7XG4gIHZpc2liaWxpdHk6IGhpZGRlbjtcbn1cblxuLmN1c3RvbS1jb250ZW50OjpiZWZvcmUge1xuICBjb250ZW50OiBcIkNob29zZSBGaWxlXCI7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGJvcmRlcjogMXB4IHNvbGlkICM5OTk7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgcGFkZGluZzogOHB4O1xuICBvdXRsaW5lOiBub25lO1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAtd2Via2l0LXVzZXItc2VsZWN0OiBub25lO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIGZvbnQtc2l6ZTogMTBwdDtcbiAgY29sb3I6IHdoaXRlO1xuICBsZXR0ZXItc3BhY2luZzogMC41cHg7XG59XG5cbm1hcmsge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGMzNTQ1O1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5yb3VuZGVkLWJ0biB7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogMzBweDtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xuICBwYWRkaW5nOiAwIDE1cHg7XG4gIGJhY2tncm91bmQ6ICM1NmE0ZmY7XG59XG5cbi5yb3VuZGVkLWJ0bjpob3ZlciB7XG4gIGJhY2tncm91bmQ6ICMwYTdiZmY7XG4gIG91dGxpbmU6IG5vbmU7XG59XG5cbi5yb3VuZGVkLWJ0bi1kYW5nZXIge1xuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBsaW5lLWhlaWdodDogNDBweDtcbiAgcGFkZGluZzogMCAxNXB4O1xuICBiYWNrZ3JvdW5kOiAjZGMzNTQ1O1xufVxuXG4ucm91bmRlZC1idG4tZGFuZ2VyOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogI2E3MWQyYTtcbiAgb3V0bGluZTogbm9uZTtcbn1cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNjAwcHgpIHtcbiAgLnJvdW5kZWQtYnRuIHtcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICAgIGxpbmUtaGVpZ2h0OiAzMHB4O1xuICAgIHBhZGRpbmc6IDAgMTJweDtcbiAgICBiYWNrZ3JvdW5kOiAjNTZhNGZmO1xuICAgIG1hcmdpbi1sZWZ0OiAyNSU7XG4gICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gIH1cblxuICAucm91bmRlZC1idG46aG92ZXIge1xuICAgIGJhY2tncm91bmQ6ICMwYTdiZmY7XG4gICAgb3V0bGluZTogbm9uZTtcbiAgfVxuXG4gIC5yb3VuZGVkLWJ0bi1kYW5nZXIge1xuICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgbGluZS1oZWlnaHQ6IDMwcHg7XG4gICAgcGFkZGluZzogMCAxMnB4O1xuICAgIGJhY2tncm91bmQ6ICNkYzM1NDU7XG4gICAgbWFyZ2luLXJpZ2h0OiAyNSU7XG4gICAgbWFyZ2luLXRvcDogMTBweDtcbiAgfVxuXG4gIC5yb3VuZGVkLWJ0bi1kYW5nZXI6aG92ZXIge1xuICAgIGJhY2tncm91bmQ6ICNhNzFkMmE7XG4gICAgb3V0bGluZTogbm9uZTtcbiAgfVxufVxuLmVycm9yLW1lc3NhZ2Uge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDI2cHg7XG4gIHBhZGRpbmctdG9wOiA0MHB4O1xufVxuLmVycm9yLW1lc3NhZ2UgLnRleHQtbWVzc2FnZSB7XG4gIGJhY2tncm91bmQ6ICNFNzRDM0M7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIG1hcmdpbjogMCAxMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/product-voucher/voucher.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/layout/modules/product-voucher/voucher.component.ts ***!
  \*********************************************************************/
/*! exports provided: VouchersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VouchersComponent", function() { return VouchersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var VouchersComponent = /** @class */ (function () {
    function VouchersComponent(productService, router) {
        var _this = this;
        this.productService = productService;
        this.router = router;
        // this section belongs to product item list
        this.Products = [];
        this.tableFormat = {
            title: 'E-Voucher Data',
            label_headers: [
                { label: 'E-Voucher Name', visible: true, type: 'string', data_row_name: 'product_name' },
                { label: 'E-Voucher Code', visible: true, type: 'string', data_row_name: 'product_code' },
                // {label: 'Type', visible: true, type: 'string', data_row_name: 'type'},
                { label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_qty' },
                { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
                { label: 'Created Date', visible: true, type: 'string', data_row_name: 'created_date' },
                { label: 'Updated Date', visible: true, type: 'string', data_row_name: 'updated_date' },
            ],
            row_primary_key: 'product_code',
            formOptions: {
                row_id: 'product_code',
                addForm: true,
                this: this,
                result_var_name: 'Products',
                detail_function: [this, 'callDetail'],
                customButtons: [
                    { label: 'Approve', func: function (f) { _this.approval(f); } },
                ]
            },
            show_checkbox_options: true
        };
        this.form_input = {};
        this.errorLabel = false;
        this.errorMessage = false;
        this.totalPage = 0;
        this.page = 1;
        this.pageSize = 50;
        this.prodDetail = false;
        this.selectedFile = null;
        this.progressBar = 0;
        this.cancel = false;
        this.errorFile = false;
    }
    VouchersComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    VouchersComponent.prototype.approval = function (listedData) {
        return __awaiter(this, void 0, void 0, function () {
            var listOfData, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // console.log("masuk coy")
                        if (listedData == undefined || listedData.length == 0) {
                            return [2 /*return*/, false];
                        }
                        listOfData = [];
                        //
                        listedData.forEach(function (element, index) {
                            listOfData.push(element._id);
                        });
                        listedData = [];
                        if (listOfData.length == 0)
                            return [2 /*return*/, false];
                        this.service = this.productService;
                        return [4 /*yield*/, this.productService.approvedProducts({ product_ids: listOfData })];
                    case 1:
                        result = _a.sent();
                        if (result.result.status == 'success') {
                            alert("the Current Data Below has been approved");
                            this.firstLoad();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    VouchersComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        result = void 0;
                        this.service = this.productService;
                        return [4 /*yield*/, this.productService.getEvoucherReport()];
                    case 1:
                        result = _a.sent();
                        this.totalPage = result.total_page;
                        this.Products = result.values;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    VouchersComponent.prototype.callDetail = function (_id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.router.navigate(['administrator/productadmin/edit'], { queryParams: { product_code: _id } });
                return [2 /*return*/];
            });
        });
    };
    VouchersComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.prodDetail = false;
                return [2 /*return*/];
            });
        });
    };
    VouchersComponent.prototype.onFileSelected = function (event) {
        var _this = this;
        this.errorFile = false;
        var fileMaxSize = 3000000; // let say 3Mb
        Array.from(event.target.files).forEach(function (file) {
            if (file.size > fileMaxSize) {
                _this.errorFile = "maximum file is 3Mb";
            }
        });
        this.selectedFile = event.target.files[0];
    };
    VouchersComponent.prototype.updateProgressBar = function (value) {
        this.progressBar = value;
    };
    VouchersComponent.prototype.cancelThis = function () {
        this.cancel = !this.cancel;
    };
    VouchersComponent.prototype.callAfterUpload = function (result) {
        console.log("THE RESULT", result);
    };
    VouchersComponent.prototype.onUpload = function () {
        return __awaiter(this, void 0, void 0, function () {
            var e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.cancel = false;
                        if (!this.selectedFile) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.productService.uploadFile(this.selectedFile, this, 'product', this.callAfterUpload)];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        alert("Upload Success");
                        return [3 /*break*/, 4];
                    case 3:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4:
                        if (this.selectedFile) { }
                        return [2 /*return*/];
                }
            });
        });
    };
    VouchersComponent.ctorParameters = function () { return [
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    VouchersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product',
            template: __webpack_require__(/*! raw-loader!./voucher.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/product-voucher/voucher.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./voucher.component.scss */ "./src/app/layout/modules/product-voucher/voucher.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_product_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], VouchersComponent);
    return VouchersComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/product-voucher/voucher.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/layout/modules/product-voucher/voucher.module.ts ***!
  \******************************************************************/
/*! exports provided: VouchersModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VouchersModule", function() { return VouchersModule; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _add_product_add_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add/product.add.component */ "./src/app/layout/modules/product-voucher/add/product.add.component.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _ckeditor_ckeditor5_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ckeditor/ckeditor5-angular */ "./node_modules/@ckeditor/ckeditor5-angular/fesm5/ckeditor-ckeditor5-angular.js");
/* harmony import */ var _detail_product_detail_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./detail/product.detail.component */ "./src/app/layout/modules/product-voucher/detail/product.detail.component.ts");
/* harmony import */ var _edit_product_edit_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./edit/product.edit.component */ "./src/app/layout/modules/product-voucher/edit/product.edit.component.ts");
/* harmony import */ var _merchant_portal_separated_modules_merchant_products_products_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../merchant-portal/separated-modules/merchant-products/products.module */ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products.module.ts");
/* harmony import */ var _voucher_routing_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./voucher-routing.module */ "./src/app/layout/modules/product-voucher/voucher-routing.module.ts");
/* harmony import */ var _voucher_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./voucher.component */ "./src/app/layout/modules/product-voucher/voucher.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








// import { NgxEditorModule } from 'ngx-editor';






var VouchersModule = /** @class */ (function () {
    function VouchersModule() {
    }
    VouchersModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _voucher_routing_module__WEBPACK_IMPORTED_MODULE_12__["VouchersRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_3__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_5__["FormBuilderTableModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                // NgxEditorModule,
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClientModule"],
                _ckeditor_ckeditor5_angular__WEBPACK_IMPORTED_MODULE_8__["CKEditorModule"],
                _merchant_portal_separated_modules_merchant_products_products_module__WEBPACK_IMPORTED_MODULE_11__["ProductsModule"]
            ],
            declarations: [_voucher_component__WEBPACK_IMPORTED_MODULE_13__["VouchersComponent"], _add_product_add_component__WEBPACK_IMPORTED_MODULE_6__["ProductAddComponent"],
                _detail_product_detail_component__WEBPACK_IMPORTED_MODULE_9__["ProductDetailComponent"],
                _edit_product_edit_component__WEBPACK_IMPORTED_MODULE_10__["ProductEditComponent"]
            ]
        })
    ], VouchersModule);
    return VouchersModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-product-voucher-voucher-module.js.map