import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenMerchantComponent } from './open-merchant.component';

describe('OpenMerchantComponent', () => {
  let component: OpenMerchantComponent;
  let fixture: ComponentFixture<OpenMerchantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenMerchantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenMerchantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
