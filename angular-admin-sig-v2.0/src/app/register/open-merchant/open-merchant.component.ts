import { MerchantService } from '../../services/merchant/merchant.service';
import { Component, OnInit, Input } from '@angular/core';
import { Routes, Router, ActivatedRoute } from '@angular/router';
import { MemberService } from '../../services/member/member.service';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { LoginService } from '../../services/login/login.service';
import { ReadVarExpr } from '@angular/compiler';
import { FormControl, FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { CourierServiceService } from '../../services/courier/courier.service';
import { CountryService } from '../../services/country/country.service';
import { PaymentGatewayService } from '../../services/paymentgateway/paymentgateway.service';
@Component({
	selector: 'app-open-merchant',
	templateUrl: './open-merchant.component.html',
	styleUrls: ['./open-merchant.component.scss']
})

export class OpenMerchantComponent implements OnInit {

	// @Input() public detail: any;
	Editor = ClassicEditor;
	config = {
		toolbar: ['heading', '|', 'bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote'],
		heading: {
			options: [
				{ model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
				{ model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
				{ model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
			]
		}
	};
	service: any;
	errorLabel: any = false;
	autoFill = true;
	edit: boolean = false;

	progressBar: any;
	public merchant_group: any = [];
	public available_courier: any = [];
	success = false;
	complete = false;
	bgUrl = '';
	countryList = [];
	showLoading = false;
	showLoading1 = false;
	showLoading2 = false;
	openCourier = false;
	form: any = {
		cell_phone: '',
		dob: '',
		email: '',
		full_name: '',
		gender: '',
		merchant_name: '',
		merchant_username: '',
		description: '',
		image_owner_url: '',
		background_image: '',
		mcountry: 'Indonesia',
		mcity: '',
		mcity_code: '',
		mstate: '',
		mprovince: '',
		mprovince_code: '',
		mdistrict: '',
		mpostal_code: '',
		mvillage: '',
		mvillage_code: '',
		region_code: '',
		region_name: '',
		msubdistrict: '',
		msubdistrict_code: '',
		address: '',
		maddress: '',
		contact_person: '',
		email_address: '',
		work_phone: '',
		handphone: '',
		fax_number: '',
		active: 0,
		_id: '',
		slogan: '',
		available_courier: undefined
	};


	formEditable: any = {};
	courierServices: any = [];
	regionList = [];
	provinceList = [];
	cityList = [];
	subDistrictList = [];
	villageList = [];
	merchantGroup: any = [];
	errorFile;
	selFile;
	selectedFile = null;
	selectedFile2 = null;
	selectedFile3 = null;
	selectedFile4 = null;
	cancel: boolean = false;
	hasToko: boolean = false;
	bukaTokoBtn: boolean = true;
	upload: boolean = false;
	addMerchantDetail: boolean = false;
	storeInfoBtn: boolean = true;
	storeProfile: boolean = false;
	open: boolean = false;
	openStore: boolean = false;
	keyname: any = [];
	activeCheckbox = false;
	checkBox = [];
	errorMessage;
	province_code: any;
	city_code: any;
	subdistrict_code: any;
	village_code: any;
	formcourier: boolean;
	errorLabel2: any;

	constructor(
		private memberService: MemberService,
		private countryService: CountryService,
		private courierService: CourierServiceService,
		private paymentGatewayService: PaymentGatewayService,
		public loginService: LoginService,

		private router: Router,
		private route: ActivatedRoute,
		private merchantService: MerchantService
	) { }

	ngOnInit() {
		this.firstLoad();

	}

	getCountryList() {
		let n = this.countryService.getCountryList();
		let r = [];
		n.forEach((element) => {
			r.push({ value: element, name: element })
		})
		this.countryList = r;
		this.form.mcountry = 'Indonesia'
	}

	valueCheck() {
		console.log("ke value check");
		if (this.form.mprovince_code && this.form.mcity_code && this.form.msubdistrict_code && this.form.mvillage_code)
			this.complete = true
		else
			this.complete = false;
	}

	editableForm(label, val?: boolean) {
		this.formEditable[label] = val;
	}
	goToAdd() {
		// this.router.navigate(['/merchant-portal/add'])
		this.router.navigate(['/login-merchant'])
	}
	goToDashboard() {
		this.router.navigate(['/merchant-portal'])
	}
	async firstLoad() {
		let member = await this.memberService.getMemberDetail();

		if (member.result) {
			let data = member.result;
			this.form = { ...this.form, ...data };

		}

		if (member.result.permission == 'merchant') {
			this.success = true;
			let member = await this.merchantService.getDetail();
			let data = member.result;

			this.form = { ...this.form, ...data };
		}


		if (this.form.available_courier && this.form.available_courier.length) {
			this.form.available_courier.forEach((value) => {
				this.setCourierService(value);
			});
		}

		// await this.getBankList();

	}

	async getCourierSupported() {
		let form = {
			province_code: this.form.mprovince_code,
			city_code: this.form.mcity_code,
			subdistrict_code: this.form.msubdistrict_code,
			village_code: this.form.mvillage_code
		}
		try {
			console.log("ini form ", form)
			let result = await this.courierService.getCourier(form);
			
			if (result) {
				
				this.courierServices = result.result;
				this.courierServices.forEach((el, i) => {
					Object.assign(el.courier, { value: false });
				});
				console.log("get courier", this.courierService)
				this.showLoading=false;
			}
		} catch (e) {
			setTimeout(() => {
				this.errorMessage = ((<Error>e).message)
			}, 500)
		}


	}



	async seeAvailableCourier() {
		this.showLoading = true;
		let result = await this.getCourierSupported();
		this.formcourier = true;
	}
	setCourierService(value): boolean {
		for (let n in this.courierServices) {

			if (value == this.courierServices[n].courier_code) {
				this.courierServices[n].value = true;
				return true;
			}
		}
		return false;
	}

	getValueCheckbox() {
		let getData = [];
		this.courierServices.forEach((el, i) => {
			if (el.value == true) {
				getData.push(el);
			}
		});
		return getData;
	}


	async saveProfile() {
		this.errorMessage = '';
		this.form.contact_person = this.form.contact_person.toString();
		this.form.mpostal_code = this.form.mpostal_code.toString();
		let data: any = JSON.parse(JSON.stringify(this.form));
		data.email_address = data.email;
		try {
			if (this.form.withdrawal_bank_name && this.form.withdrawal_bank_name != "none") {
				data.withdrawal_bank_name = JSON.parse(this.form.withdrawal_bank_name)
			}
			else {
				data.withdrawal_bank_name = '';
			}
			data.available_courier = this.getValueCheckbox();
			if (this.form.permission == 'member') {
				const valid = this.formValidation();
				console.log("ini data", data);
				let resultMerchant = await this.merchantService.addMerchant(data);
				this.success = true;
				localStorage.removeItem('isLoggedin');
				localStorage.removeItem('tokenlogin');
				// this.firstLoad();
			} else {
				let result = await this.memberService.updateProfile(data);
			}
		}
		catch (e) {
			setTimeout(() => {
				this.errorMessage = ((<Error>e).message)
			}, 500)
		}

	}
	async autoLogin(login_data) {
		console.log(login_data);

		let result = await this.loginService.memberLogin(login_data);
		console.log('here')
		if (!result.error) {
			console.log(result);
			localStorage.setItem('isLoggedin', 'true');
			localStorage.setItem('tokenlogin', result.result.token);

			// this.router.navigate(['/merchant-portal/profile'])
			this.router.navigate(['/register/open-merchant'])

		}
	}


	nameInput($event) {
		if (this.autoFill == true) {
			let fill = this.form.merchant_name.toLowerCase().replace(/[\s\?\*]/g, '-');
			this.form.merchant_username = fill;
		}
	}

	formValidation(): boolean {
		console.log(this.form);
		return true;
	}

	openUploadLegalDocument() {
		this.upload = true;
		this.bukaTokoBtn = false;
	}

	openAddMerchantDetail() {
		this.upload = false;
		this.addMerchantDetail = true;
	}

	openStoreInfo() {
		this.openStore = true;
		this.storeInfoBtn = false;
	}

	openStoreProfile() {
		this.openStore = false;
		this.storeProfile = true;
	}

	getVillageList($event) {
		let subdistrict_code = this.subdistrict_code;
		let result = $event.target.value;
		console.log('ini biang kerok', $event)
		if (result.length > 2) {
			this.memberService.getVillage(result, 'origin', subdistrict_code).then((result) => {
				this.villageList = result.result;
				console.log('INI village', this.villageList);
				this.onVillageChange();
			})
		}
	}

	onVillageChange() {
		console.log('ON village Changed', this.form.mvillage, this.villageList);
		this.villageList.forEach((element) => {
			if (this.form.mvillage.toLowerCase() == element.village.toLowerCase()) {
				this.form.mvillage_code = this.village_code = element.village_code;
				this.valueCheck();
			}
		});
		console.log("kode village", this.form.mvillage_code);
	}

	getSubDistrictList($event) {
		let city_code = this.city_code;
		let result = $event.target.value;
		console.log('ini biang kerok', $event)
		if (result.length > 2) {
			this.memberService.getSubDistrict(result, 'origin', city_code).then((result) => {
				this.subDistrictList = result.result;
				console.log('INI Subdistrict', this.subDistrictList);
				this.onSubDistrictChange();
			})
		}
	}

	onSubDistrictChange() {
		console.log('ON Subdistrict Changed', this.form.msubdistrict, this.subDistrictList);
		this.subDistrictList.forEach((element) => {
			if (this.form.msubdistrict.toLowerCase() == element.subdistrict.toLowerCase()) {
				this.subdistrict_code = this.form.msubdistrict_code = element.subdistrict_code;
			}
		});
		console.log("kode subdistrict", this.city_code);
	}

	getCityList($event) {
		console.log('sekarang di city list')
		let province_code = this.province_code;
		let city = $event.target.value;
		console.log()
		if (city.length > 2) {
			this.memberService.getCity(city, 'origin', province_code).then((result) => {
				this.cityList = result.result;
				console.log('INI City', this.cityList);
				this.onCityChange();
			})
		}
	}

	onCityChange() {
		console.log('ON City Changed', this.form.mcity, this.cityList);
		this.cityList.forEach((element) => {
			if (this.form.mcity.toLowerCase() == element.city.toLowerCase()) {
				this.city_code = this.form.mcity_code = element.city_code;
			}
		});
		console.log("kode kota", this.city_code);
	}


	getProvinceList($event) {
		let result = $event.target.value;
		console.log('ini biang kerok', $event.target.value)
		if (result.length > 2)
			this.memberService.getProvince(result, 'origin').then((result) => {
				this.provinceList = result.result;
				console.log('INI PROVINCE', this.provinceList)
				this.onProvinceChange();
			})

	}

	onProvinceChange() {
		console.log('ON province Changed', this.form.mprovince, this.provinceList);
		this.provinceList.forEach((element) => {
			if (this.form.mprovince.toLowerCase() == element.province.toLowerCase()) {
				this.form.mprovince_code = this.province_code = element.province_code;
			}
		});
		console.log(this.form.mprovince_code);
	}


	getRegionList($event) {
		let result = $event.target.value;

		if (result.length > 2)
			this.memberService.getRegion(result, 'origin').then((result) => {
				this.regionList = result.result;
				console.log('hasil reg', this.regionList);
				this.onRegionChange()
			});
	}

	onRegionChange() {
		console.log('ON Region Changed', this.form.region_name, this.regionList);
		this.regionList.forEach((element) => {
			if (this.form.region_name.toLowerCase() == element.region_name.toLowerCase()) {
				this.form.region_code = element.region_code;
			}
		});
		console.log(this.form.region_code);
	}

	async onFileSelected(event, img) {
		var reader = new FileReader()
		try {
			this.errorFile = this.errorLabel =this.errorLabel2 =false;
			this.errorMessage = undefined;
			let fileMaxSize = 3000000; // let say 3Mb
			Array.from(event.target.files).forEach((file: any) => {
				if (file.size > fileMaxSize) {
					this.errorFile = 'Maximum File Upload is 3MB';
				}
			});
			this.selectedFile = event.target.files[0];

			reader.onload = (event: any) => {
				this.selFile = event.target.result ;
			}
			if(event.target.files[0]){
				reader.readAsDataURL(event.target.files[0]);
			  }
			

			if (this.selectedFile && this.errorFile == false) {
				switch (img) {
					case 'image_owner_url':
						this.showLoading1 = true;
						console.log("ini2", img)
						break;
					case 'background_image':

						this.showLoading2 = true;
						console.log("ini3", img)
						break;
				}
				const logo = await this.merchantService.upload(this.selectedFile, this, 'image', (result) => {
					console.log("hasil", result)
					this.callImage(result, img);
				});
			}
			if(this.errorFile != false){
			switch (img) {
				case 'image_owner_url':
					this.errorLabel = this.errorFile;
					this.errorLabel2 = false
					break;
				case 'background_image':	
					this.errorLabel2 = this.errorFile;
					this.errorLabel = false
					break;
			}
			console.log(this.errorLabel,this.errorLabel2)
			console.log("test lah", this.callImage)
		}
		} catch (e) {
			this.errorLabel = (<Error>e).message;
		}
	}
	allfalse() {
		this.showLoading = this.showLoading2 = this.showLoading1
			= false;
	}
	cancelThis() {
		this.cancel = !this.cancel;
	}

	callImage(result, img) {
		console.log('result upload logo ', result);
		this.form[img] = result.base_url + result.pic_big_path;
		console.log('This detail', this.form);
		this.allfalse();
		// this.detail = {...this.detail, ...result}
	}


}
