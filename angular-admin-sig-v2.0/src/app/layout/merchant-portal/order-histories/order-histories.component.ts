import { Component, OnInit } from '@angular/core';
import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TableFormat, orderHistoriesTableFormat, salesOrderTableFormat } from '../../../object-interface/common.object';
import { getHost } from '../../../../environments/environment';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { MemberService } from '../../../services/member/member.service';
@Component({
  selector: 'app-order-histories',
  templateUrl: './order-histories.component.html',
  styleUrls: ['./order-histories.component.scss']
})
export class OrderHistoriesComponent implements OnInit {

  onSearchActive: boolean = false;
  showLoading: boolean = false;
  searchCallback: any;
  allData: any = [];
  prodType: any;
  form_input: any = {};
  errorLabel: any = false;
  allMemberDetail: any = false;
  service: any;
  swaper: any[] = [
    { name: 'All Order', val: true, value: 'all order' },
    { name: 'Incoming', val: false, value: 'on process', values: 'process' },
    { name: 'Packaging', val: false, value: 'on packaging', values: 'packaging' },
    { name: 'On Delivery', val: false, value: 'on delivery', values: 'delivery' },
    { name: 'Delivered', val: false, value: 'delivered', values: 'delivered' },
    { name: 'Canceled', val: false, value: 'cancel', values: 'cancel' }
  ]
  filterBy = [
    { name: 'Order ID', value: 'order_id' },
    { name: 'Buyer Name', value: 'buyer_name' },
    { name: 'Product Name', value: 'product_name' },
    { name: 'No. Resi', value: 'no_resi' }
  ]
  sortBy = [
    { name: 'Newest', value: 'newest' },
    { name: 'Highest Transactions', value: 'highest transactions' },
    { name: 'Lowest Transactions', value: 'lowest transactions' }
  ]
  srcDownload: any;
  toggler: any = {};
  totalPage: any;
  pageNumbering: any = [];
  currentPage = 1;
  pageLimits = '20';
  pagesLimiter = [];
  total_page: 0;
  orderBy: string = "newest";
  filter: string = "order_id";
  checkBox = [];
  activeCheckbox = false;
  filterResult: any = {};
  orderByResult: any = { order_date: -1 };
  getDataCheck = [];
  totalValuePending;
  currentPermission;

  constructor(private orderhistoryService: OrderhistoryService, private router: Router, private route: ActivatedRoute, public memberService: MemberService) {
    this.service = this.orderhistoryService;
  }

  public async callDetail(order_id) {
    // this.openDetail(order_id)
  }

  private openDetail(order_id) {
    // this.router.navigate(['merchant-portal/order-detail/edit'],  {queryParams: {id: order_id }})
  }

  //Shipping Status Button Swapping Function
  async swapClick(index) {
    if (this.form_input.transaction_status) {
      delete this.form_input.transaction_status;
    }

    this.swaper.forEach((e) => {
      e.val = false
    })
    this.swaper[index].val = true;

    let status = this.swaper[index].values
    let params;

    console.log("Status", status)
    if (status == "all order") {
      this.firstLoad();
    } else if (status == "process") {
      let process = {
        transaction_status: 'PAID',
        shipping_status: 'process'
      }
      Object.assign(this.form_input, process)
    } else if (status == "cancel") {
      let process = {
        transaction_status: 'CANCEL',
      }
      Object.assign(this.form_input, process)
    } else {
      let swaper = {
        shipping_status: status,
        payment_status: 'PAID',
      }


      Object.assign(this.form_input, swaper)
    }
    this.valuechange({}, false, false);
  }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    let member
    member = await this.memberService.getMemberDetail();
    this.currentPermission = member.result.permission;
    if (this.currentPermission == 'admin') {
      try {
        let admin = {
          search: { status: "paid", merchant_username: "loca" },
          limit_per_page: 10,
          page: 1,
          order_by: this.orderByResult,
        }
        let result = await this.orderhistoryService.searchOrderhistorysummaryLint(admin)

      } catch (e) {
        this.errorLabel = ((<Error>e).message);//conversion to Error type
      }
    }
    if (this.currentPermission == 'merchant') {

      try {
        let filter = {
          search: {},
          order_by: this.orderByResult,
          limit_per_page: this.pageLimits,
          current_page: this.currentPage,
          download: false
        }
        let filter_pending = {
          search: {
            shipping_status: "process",
            transaction_status: "PAID"
          },
          order_by: this.orderByResult,
          limit_per_page: this.pageLimits,
          current_page: this.currentPage,
          download: false
        }


        this.showLoading = true;
        let result = await this.orderhistoryService.getOrderHistoryMerchantFilter(filter);
        let resultPending = await this.orderhistoryService.getOrderHistoryMerchantFilter(filter_pending);

        this.totalValuePending = resultPending.result.total_all_values;

        this.showLoading = false;
        this.allData = result.result.values;
        this.totalPage = result.result.total_page
        console.log(this.allData)



        this.buildPagesNumbers(this.totalPage)

        let productStatus = {
          product_status: false
        }

        this.allData.forEach((el, i) => {
          Object.assign(this.allData[i], productStatus)
        });

        // console.log(this.totalPage);
        // console.log(this.allData);
        // console.log(result);

      } catch (e) {
        this.errorLabel = ((<Error>e).message);//conversion to Error type
      }
    }
  }

  type(array) {
    let res = [];
    this.allData.forEach((el, i) => {
      el.product_list.forEach((element) => {
        res.push(element.type)
      })
      // Object.assign(this.allData[i], this.prodType)

    });
    // console.log("hasil",res[0])

    return res[array]
  }


  //When Page Changed
  async onChangePage(pageNumber) {
    if (pageNumber <= 0) {
      pageNumber = 1;
    }
    if (pageNumber >= this.total_page) {
      pageNumber = this.total_page;
    }

    this.currentPage = pageNumber;
    console.log(pageNumber, this.currentPage, this.total_page);

    this.valuechange({}, false, false);
  }

  //Order by Function
  async orderBySelected() {
    let params;
    if (this.orderBy == "highest transactions") {
      this.orderByResult = {
        total_price: -1
      }
      this.valuechange({}, false, false)
    }
    else if (this.orderBy == "lowest transactions") {
      this.orderByResult = {
        total_price: 1
      }
      this.valuechange({}, false, false)
    }
    else if (this.orderBy == "newest") {
      this.orderByResult = {
        order_date: -1
      }
      this.valuechange({}, false, false)
    }
  }

  //Start of Pagination
  pagination(c, m) {
    var current = c,
      last = m,
      delta = 2,
      left = current - delta,
      right = current + delta + 1,
      range = [],
      rangeWithDots = [],
      l;

    for (let i = 1; i <= last; i++) {
      if (i == 1 || i == last || i >= left && i < right) {
        range.push(i);
      }
    }

    for (let i of range) {
      if (l) {
        if (i - l === 2) {
          rangeWithDots.push(l + 1);
        } else if (i - l !== 1) {
          rangeWithDots.push('...');
        }
      }
      rangeWithDots.push(i);
      l = i;
    }

    return rangeWithDots;
  }

  buildPagesNumbers(totalPage) {
    this.pageNumbering = [];
    this.total_page = totalPage;
    this.pageNumbering = this.pagination(this.currentPage, totalPage);

  }

  //End of Pagination

  //Search by text function
  async searchText($event) {
    let result = $event.target.value
    console.log("Search", result);
    if (result.length > 2) {
      let params
      if (this.filter == "order_id") {
        let order_id = {
          order_id: result,
          get_detail: true
        }
        Object.assign(this.form_input, order_id)
      }
      else if (this.filter == "product_name") {
        let product_name = {
          product_name: result,
          get_detail: true
        }
        Object.assign(this.form_input, product_name)
      }
      else if (this.filter == "buyer_name") {
        let buyer_name = {
          buyer_name: result,
          get_detail: true
        }
        Object.assign(this.form_input, buyer_name)
      }
      else if (this.filter == "no_resi") {
        let no_resi = {
          no_resi: result,
          get_detail: true
        }
        Object.assign(this.form_input, no_resi)
      }
      this.valuechange({}, false, false);
    }
    else if (!result) {
      this.form_input = {};
      this.valuechange({}, false, false);
    }
  }

  //Get the detail Function
  getDetail(idDetail) {
    console.log(idDetail)
    this.router.navigate(['merchant-portal/order-detail/edit'],
      {
        queryParams: {
          id: idDetail
        }
      })
  }

  //When value change
  valuechange(event, input_name, download?: boolean) {

    if (this.onSearchActive == false) {
      this.onSearchActive = true;
      let myVar = setTimeout(async () => {
        if (this.form_input) {
          let clearFormInput = this.form_input
          let searchDate = {};

          if (clearFormInput.order_date) {
            if (clearFormInput.order_date.to) {
              let order_date_from_to = {
                order_date_from: clearFormInput.order_date.from,
                order_date_to: clearFormInput.order_date.to
              }
              Object.assign(clearFormInput, order_date_from_to)
            }
            else {
              let order_date_from = {
                order_date_from: clearFormInput.order_date.from,
              }
              Object.assign(clearFormInput, order_date_from)
            }

            delete clearFormInput.order_date
          }

          this.showLoading = true;
          console.log("clearFormInput", clearFormInput)
          let filter = {
            search: clearFormInput,
            order_by: this.orderByResult,
            limit_per_page: this.pageLimits,
            current_page: this.currentPage,
            download: false
          }
          // console.log(filter)

          // let result = await searchCallback[0][searchCallback[1]](filter);
          let result = await this.orderhistoryService.getOrderHistoryMerchantFilter(filter);
          this.allData = result.result.values;
          this.buildPagesNumbers(result.result.total_page)
        } else {
          this.firstLoad();
        }
        this.onSearchActive = false;
        this.showLoading = false;

      }, 2000);
    }

  }

  //Start of DatePicker Function

  datePickerOnDateSelection(date: NgbDate, formName) {
    let _this: any = this.make_This(formName);

    if (!_this.fromDate && !_this.toDate) {
      _this.fromDate = date;
    }
    else if (_this.fromDate && !_this.toDate && this.datePickerDateAfter(date, _this.fromDate)) {
      _this.toDate = date;
    }
    else {
      _this.toDate = null;
      _this.fromDate = date;
    }

    _this.from = (_this.fromDate != undefined && _this.fromDate != null) ? this.convertToDateString(_this.fromDate) : ''
    _this.to = (_this.toDate != undefined && _this.toDate != null) ? this.convertToDateString(_this.toDate) : ''
  }

  typeof(object) {
    return typeof object;
  }

  make_This(formName) {
    if (typeof formName == 'object') {
      return formName;
    }

    if (this.form_input[formName] == undefined || this.form_input[formName] == '') {
      this.form_input[formName] = {};
    }
    return this.form_input[formName];

  }

  dateToggler(formName) {
    this.toggler[formName] = (typeof this.toggler == undefined) ? true : !this.toggler[formName];
    if (this.toggler[formName] == false && this.form_input[formName] !== '') {
      this.valuechange({}, false, false);
    }
  }

  dateClear(event, dataInput, deleteInput) {
    this.form_input[dataInput] = '';
    this.valuechange(event, deleteInput);
  }

  datePickerOnDateIsInside(date: NgbDate, formName) {
    let _this: any = this.make_This(formName)

    return this.datePickerDateAfter(date, _this.fromDate) && this.datePickerDateBefore(date, _this.toDate);
  }

  datePickerOnDateIsRange(date: NgbDate, formName) {
    let _this: any = this.make_This(formName)

    return this.datePickerDateEquals(date, _this.fromDate) || this.datePickerDateEquals(date, _this.toDate) || this.datePickerOnDateIsInside(date, _this) || this.datePickerOnDateIsHovered(date, _this);
  }

  datePickerOnDateIsHovered(date: NgbDate, formName) {
    let _this: any = this.make_This(formName)
    return _this.fromDate && !_this.toDate && _this.hoveredDate && this.datePickerDateAfter(date, _this.fromDate) && this.datePickerDateBefore(date, _this.hoveredDate);
  }

  datePickerDateAfter(datePrev: NgbDate, dateAfter: NgbDate) {
    if (datePrev == undefined || dateAfter == undefined) return false

    let iFromDate = new Date(datePrev.year + '-' + datePrev.month + '-' + datePrev.day)
    let iToDate = new Date(dateAfter.year + '-' + dateAfter.month + '-' + dateAfter.day)
    if (iToDate.getTime() < iFromDate.getTime()) {
      return true
    }
    return false
  }

  convertToDateString(datePrev: NgbDate) {
    console.log(datePrev)
    return datePrev.year.toString().padStart(2, "0") + '-' + datePrev.month.toString().padStart(2, "0") + '-' + datePrev.day.toString().padStart(2, "0")
  }
  datePickerDateEquals(datePrev: NgbDate, dateAfter: NgbDate) {
    if (datePrev == undefined || dateAfter == undefined) return false

    let iFromDate = new Date(datePrev.year + '-' + datePrev.month + '-' + datePrev.day)
    let iToDate = new Date(dateAfter.year + '-' + dateAfter.month + '-' + dateAfter.day)
    if (iToDate.getTime() == iFromDate.getTime()) {
      return true
    }
    return false
  }

  datePickerDateBefore(datePrev: NgbDate, dateAfter: NgbDate) {
    if (datePrev == undefined || dateAfter == undefined) return false

    let iFromDate = new Date(datePrev.year + '-' + datePrev.month + '-' + datePrev.day)
    let iToDate = new Date(dateAfter.year + '-' + dateAfter.month + '-' + dateAfter.day)
    if (iToDate.getTime() > iFromDate.getTime()) {
      return true
    }
    return false
  }

  //End of DatePicker Function

  //Start of Checkbox Function
  onChecked() {
    setTimeout(() => {
      let getData = [];
      this.checkBox.forEach((el, i) => {
        var data = this.allData[i];
        console.log(data)
        if (!this.activeCheckbox) {
          getData.push(data.booking_id)
        }
        else {
          var index = getData.indexOf(data.booking_id);
          if (index !== -1) getData.splice(index, 1);
        }
      })
      this.getDataCheck = getData;
      console.log(getData)
    }, 100);
  }

  checkedAll() {
    setTimeout(() => {
      let getData = [];
      this.allData.forEach((e, i) => {
        console.log("result", e)
        if (this.activeCheckbox) {
          this.checkBox[i] = this.activeCheckbox
          getData.push(e.order_id)
          console.log("result", e.order_id)
        }
        else {
          this.checkBox = [];
        }
      })
      this.getDataCheck = getData;
      console.log(getData)
    }, 100);
  }

  printLabel() {
    console.log("test")
    this.router.navigate(['merchant-portal/order-histories/shipping-label'], { queryParams: { data: this.getDataCheck } })
    // window.print();

  }

  clickToDownloadReport() {
    //reportTable ID

    let dataType = 'application/vnd.ms-excel';
    let tableSelect = document.getElementById('reportTable');
    let tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

    // Specify file name
    let filename = 'excel_data.xls';
    let downloadLink = document.createElement("a");
    document.body.appendChild(downloadLink);

    if (navigator.msSaveOrOpenBlob) {
      var blob = new Blob(['\ufeff', tableHTML], {
        type: dataType
      });
      navigator.msSaveOrOpenBlob(blob, filename);
    } else {
      // Create a link to the file
      downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

      // Setting the file name
      downloadLink.download = filename;

      //triggering the function
      downloadLink.click();
    }
  }

  //see more detail function
  setSeeMore(index) {


    this.allData[index].product_status = true;
    console.log(this.allData)
  }
}
