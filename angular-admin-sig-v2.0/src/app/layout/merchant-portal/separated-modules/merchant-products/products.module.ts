import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MerchantEditProductComponent } from './products/merchant-edit-product/merchant-edit-product.component';
import { FormsModule } from '@angular/forms';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { NgbDatepickerModule, NgbModule, NgbPagination } from '@ng-bootstrap/ng-bootstrap';
import { MerchantAddNewProductComponent } from './products/merchant-add-new-product/merchant-add-new-product.component';
// import { EvoucherDetailComponent } from '../../../modules/product.evoucher/detail/evoucher.detail.component';
import { EvoucherListComponent } from './products/evoucher-list/evoucher-list.component';
import { FormBuilderTableModule } from '../../../../component-libs/form-builder-table/form-builder-table.module';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MerchantDetailProductComponent } from './products/merchant-detail-product/merchant-detail-product.component';

@NgModule({
  declarations: [
    MerchantDetailProductComponent,
    MerchantEditProductComponent,
    MerchantAddNewProductComponent,
    // EvoucherDetailComponent,
    EvoucherListComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    CKEditorModule,
    // NgbModule,
    NgbDatepickerModule,
    FormBuilderTableModule,
    MatCheckboxModule,
  ],
  exports:[
    MerchantDetailProductComponent,
    MerchantEditProductComponent,
    MerchantAddNewProductComponent,
    // EvoucherDetailComponent,
    EvoucherListComponent
  ],
  bootstrap:[
    NgbPagination,
  ]
})
export class ProductsModule { }
