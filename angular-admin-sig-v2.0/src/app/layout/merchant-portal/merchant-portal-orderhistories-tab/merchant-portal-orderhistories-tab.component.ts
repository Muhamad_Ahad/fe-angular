import { Component, OnInit } from '@angular/core';
import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-merchant-portal-orderhistories-tab',
  templateUrl: './merchant-portal-orderhistories-tab.component.html',
  styleUrls: ['./merchant-portal-orderhistories-tab.component.scss']
})
export class MerchantPortalOrderhistoriesTabComponent implements OnInit {

  data: any = [];
  service: OrderhistoryService
  errorLabel: string;
  // topBar: topBarMenu[] = [
  //   { label: "All Order", routerLink: '/administrator/order-history-summary', active: true },
  //   { label: "Belum di proses", routerLink: '/administrator/order-amount-history-summary' },
  //   { label: "Packaging", routerLink: '/administrator/cart-abandonment-summary' },
  //   { label: "Proses Pengiriman", routerLink: '/administrator/cart-abandonment-summary' },
  //   { label: "Delivered", routerLink: '/administrator/cart-abandonment-summary' },
  //   { label: "Cancelled", routerLink: '/administrator/cart-abandonment-summary' },
  // ]
  
  constructor(private orderHistoryService: OrderhistoryService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.firstLoad();
  }

  async firstLoad(){

  }
}
