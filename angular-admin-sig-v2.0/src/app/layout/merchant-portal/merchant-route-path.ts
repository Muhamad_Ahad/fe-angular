import {MerchantAddNewProductComponent} from './separated-modules/merchant-products/products/merchant-add-new-product/merchant-add-new-product.component';
import {MerchantHomepageComponent} from './merchant-homepage/merchant-homepage.component';
import {MerchantProductsComponent} from './separated-modules/merchant-products/merchant-products.component';
// import { ProductDetailComponent }    from './products/product-detail/product-detail.component';
import {ProfileComponent} from './profile/profile.component';
import {OrderHistoriesComponent} from './order-histories/order-histories.component';
import {MerchantEditProductComponent} from './separated-modules/merchant-products/products/merchant-edit-product/merchant-edit-product.component';
import {OrderDetailComponent} from './order-detail/order-detail.component';
import {OrderHistorySummaryComponent} from '../modules/admin-portal/admin-order-history/order-history-summary/orderhistorysummary.component';
import {StatisticsComponent} from './statistics/statistics.component';
import {EvoucherListComponent} from './separated-modules/merchant-products/products/evoucher-list/evoucher-list.component';
import {SalesorderComponent} from '../modules/salesorder/salesorder.component';
import {MerchantSalesOrderComponent} from './merchant-sales-order/merchant-sales-order.component';
import {EvoucherDetailComponent} from './separated-modules/merchant-products/products/evoucher-list/evoucher-detail/evoucher-detail.component';
import {MerchantEvoucherSalesReportComponent} from './merchant-evoucher-sales-report/merchant-evoucher-sales-report.component';
import { ShippingLabelComponent } from './order-histories/shipping-label/shipping-label.component';
import { MerchantPortalInboxComponent } from './merchant-portal-inbox/merchant-portal-inbox.component';
import { MerchantPortalDetailInboxComponent } from './merchant-portal-detail-inbox/merchant-portal-detail-inbox.component';
import { MerchantMyEscrowComponent } from './merchant-my-escrow/merchant-my-escrow.component';
import { MerchantWithdrawalComponent } from './merchant-my-escrow/merchant-withdrawal/merchant-withdrawal.component';
import { MerchantEscrowDetailComponent } from './merchant-my-escrow/merchant-escrow-detail/merchant-escrow-detail.component';
import { MerchantDetailProductComponent } from './separated-modules/merchant-products/products/merchant-detail-product/merchant-detail-product.component';
import { UpgradeGenuineComponent } from './upgrade-genuine/upgrade-genuine.component';
import { ReportComponent } from '../modules/report/report.component';
import { ReportDetailComponent } from '../modules/report/detail/report.detail.component';
import { ReportEditComponent } from '../modules/report/edit/report.edit.component'
import { ReportExcelDetailComponent } from '../modules/report/detail/report.excel.detail.component';


export const path = [
    {path: 'homepage', component: MerchantHomepageComponent, data: {animation: 'homepage'}},
    {path: 'add', component: MerchantAddNewProductComponent, data: {animation: 'addNewProductPage'}},
    {path: 'my-money', component: MerchantMyEscrowComponent, data: {animation: 'merchantMyEscrow'}},
    {path: 'my-money/detail', component: MerchantEscrowDetailComponent, data: {animation: 'MerchantEscrowDetailComponent'}},
    {path: 'my-money/withdrawal', component: MerchantWithdrawalComponent, data: {animation: 'merchantMyEscrowWithdrawal'}},
    {path: 'product-list/detail', component: MerchantDetailProductComponent, data: {animation: 'MerchantDetailProductPage'}},
    {path: 'product-list/edit', component: MerchantEditProductComponent, data: {animation: 'MerchantEditProductPage'}},
    {path: 'product-list/generated-voucher/list', component: EvoucherListComponent, data: {animation: 'EvoucherListComponent'}},
    {path: 'product-list/generated-voucher/list/detail', component: EvoucherDetailComponent, data: {animation: 'EvoucherDetailComponent'}},
    {path: 'product-list', component: MerchantProductsComponent, data: {animation: 'productListPage'}},
    {path: 'homepage/upgrade-genuine', component: UpgradeGenuineComponent, data: {animation: 'UpgradeGenuineComponent'}},
    // { path: 'product-list/detail', component: ProductDetailComponent,  data: {animation: 'producDetailPage'} },
    // {path: 'reportadmin', loadChildren: '../modules/report/report.module#ReportModule' , label: ''},
    {path: 'report', component: ReportComponent, data: {animation: 'UpgradeGenuineComponent'}},
    {path: 'report/detail', component: ReportDetailComponent, data: {animation: 'UpgradeGenuineComponent'}},
    {path: 'report/edit', component: ReportEditComponent, data: {animation: 'UpgradeGenuineComponent'}},
    {path: 'report/exceldetail', component: ReportExcelDetailComponent, data: {animation: 'UpgradeGenuineComponent'}},
    {path: 'profile', component: ProfileComponent, data: {animation: 'profilePage'}},
    {path: 'order-histories', component: OrderHistoriesComponent, data: {animation: 'transactionPage'}},
    {path: 'order-histories/shipping-label', component: ShippingLabelComponent, data: {animation: 'transactionPage'}},
    {path: 'order-detail/edit', component: OrderDetailComponent, data: {animation: 'transactionPage'}},
    {path: 'statistics', component: StatisticsComponent, data: {animation: 'transactionPage'}},
    {path: 'sales-order', component: MerchantSalesOrderComponent, data: {animation: 'transactionPage'}},
    {path: 'evoucher-sales-report', component: MerchantEvoucherSalesReportComponent, data: {animation: 'transactionPage'}},
    {path: 'inbox', component: MerchantPortalInboxComponent, data: {animation: 'MerchantPortalInboxComponent'}},
    {path: 'inbox-detail', component: MerchantPortalDetailInboxComponent, data: {animation: 'MerchantPortalInboxComponent'}},
];
