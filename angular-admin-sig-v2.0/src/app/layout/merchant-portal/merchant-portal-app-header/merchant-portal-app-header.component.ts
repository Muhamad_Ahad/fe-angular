import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { routePath } from '../../routing.path'
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'merchant-portal-app-header',
  templateUrl: './merchant-portal-app-header.component.html',
  styleUrls: ['./merchant-portal-app-header.component.scss']
})
export class MerchantPortalAppHeaderComponent implements OnInit {
  dateTimeCurrent: string;
  dateCurrent: string;
  timeCurrent;
  password: string;
  closeResult: string;
  pushRightClass: string = 'push-right';
  dataSearch: string =''
  developmentMode: string = 'production';
  // time: Date;
  time = new Date()
  routePath: any = routePath
  constructor(private translate: TranslateService, public router: Router, private modalService: NgbModal) {

      this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
      this.translate.setDefaultLang('en');
      const browserLang = this.translate.getBrowserLang();
      this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');

      this.router.events.subscribe(val => {
          if (
              val instanceof NavigationEnd &&
              window.innerWidth <= 992 &&
              this.isToggled()
          ) {
              this.toggleSidebar();
          }
      });
  }

  ngOnInit() {
      this.dateTimeCurrent = Date();
      var clock = new Date();
      const arrayDate = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
      const splittedDate = this.dateTimeCurrent.split(' ');
      // switch (splittedDate[0]) {
      //     case 'Mon':
      //         splittedDate[0] = arrayDate[0];
      //         break;
      //     case 'Tue':
      //         splittedDate[0] = arrayDate[1];
      //         break;
      //     case 'Wed':
      //         splittedDate[0] = arrayDate[2];
      //         break;    
      //     case 'Thu':
      //         splittedDate[0] = arrayDate[3];
      //         break; 
      //     case 'Fri':
      //         splittedDate[0] = arrayDate[4];
      //         break;
      //     case 'Sat':
      //         splittedDate[0] = arrayDate[5];
      //         break;
      //     case 'Sun':
      //         splittedDate[0] = arrayDate[6];
      //         break;      
      //     default:
      //         break;
      // }
      this.dateCurrent = splittedDate[0] + ', ' +  splittedDate[2] + ' ' + splittedDate[1] + ' ' + splittedDate[3];
      const res = (splittedDate[4]).split(':');
      this.timeCurrent = setInterval(() => {
          this.time = new Date()
      }, 1000)
      console.log(this.timeCurrent)
      // this.timeCurrent = setInterval(() => {
      //     clock = new Date(clock.setSeconds(clock.getSeconds()+ 1));
      //     this.time = clock;
      // })
      // console.log(this.timeCurrent)
  }

  // clock(){
  //     var today = new Date();
  //     var hours = today.getHours();
  //     var minutes = today.getMinutes();
  //     var second = today.getSeconds();
  //     console.log("result", today)
  // }

  onSubmit(){
      console.log(this.password);
      if(this.password == "devmode1234"){
          localStorage.setItem('devmode', 'active' );
          this.devModeLoggedout();
          window.location.href = "/login";
      }
      else{
          alert("wrong password");
      }
  }

  isToggled(): boolean {
      const dom: Element = document.querySelector('body');
      return dom.classList.contains(this.pushRightClass);
  }

  toggleSidebar() {
      const dom: any = document.querySelector('body');
      dom.classList.toggle(this.pushRightClass);
  }

  rltAndLtr() {
      const dom: any = document.querySelector('body');
      dom.classList.toggle('rtl');
  }

  onLoggedout() {
      localStorage.removeItem('isLoggedin');
      localStorage.removeItem('tokenlogin');
      localStorage.removeItem('devmode');
  }
  devModeLoggedout() {
      localStorage.removeItem('isLoggedin');
      localStorage.removeItem('tokenlogin');
  }

  changeLang(language: string) {
      this.translate.use(language);
  }

  devmodeon(content){
      if(localStorage.getItem('devmode')=='active'){
          localStorage.removeItem('devmode');
          this.onLoggedout();
          window.location.href = "/login";
      }
      else{
          // this.open(content);
          localStorage.setItem('devmode', 'active' );
          this.devModeLoggedout();
          window.location.href = "/login";
      }
      
  }

  // devmodeoff(){
  //     localStorage.removeItem('devmode');
  //     this.onLoggedout();
  //     window.location.href = "/login";
  // }

  onSearch() {
      // console.log('routepath', routePath);    
  }

  onDataChanged(){
      console.log('changed', this.dataSearch)
      this.router.navigateByUrl(this.dataSearch);
      this.dataSearch = ''
    }

    open(content) {
      if(localStorage.getItem('devmode')=='active'){
          localStorage.removeItem('devmode');
          this.onLoggedout();
          window.location.href = "/login";
      }
      else{
          this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
              this.closeResult = `Closed with: ${result}`;
            }, (reason) => {
              this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            });
      }
    }

    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
          var element = <HTMLInputElement> document.getElementById("checkbox");
          element.checked = false;
        return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
         var element = <HTMLInputElement> document.getElementById("checkbox");
         element.checked = false;
        return 'by clicking on a backdrop';
      } else {
        return  `with: ${reason}`;
      }
    }


  
}
