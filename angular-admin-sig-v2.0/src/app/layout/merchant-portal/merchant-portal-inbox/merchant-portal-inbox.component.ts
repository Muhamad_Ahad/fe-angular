import { Component, OnInit } from '@angular/core';
import { InboxService } from '../../../services/inbox/inbox.service';
import { Router, ActivatedRoute } from '@angular/router';
import { makeBindingParser } from '@angular/compiler';

@Component({
	selector: 'app-merchant-portal-inbox',
	templateUrl: './merchant-portal-inbox.component.html',
	styleUrls: [ './merchant-portal-inbox.component.scss' ]
})
export class MerchantPortalInboxComponent implements OnInit {
	message: any;
	getId: any;
	detail: any;
	inboxData: any = [
		{
			// title: 'test',
			// message: 'test',
			// short_description: 'New Order Coming!'
		}
	];

	hideColumnWhenOpen: boolean;
	selectedData: any;

	constructor(private inboxService: InboxService, private router: Router, private activatedRoute: ActivatedRoute) {}

	ngOnInit() {
		// const list =  this.inboxService.getInbox();
		this.firstLoad();
		// this.secondLoad();
	}

	async firstLoad() {
    let result;
    
		try {
			this.inboxData = [];
			result = await this.inboxService.getInbox();
			console.log('ISI INBOX', result);
			/** making multimple inboxData samples */
			let dataSamples = [];

			if (result.result) {
				let data = result.result;
				data.forEach((value) => {
          // value.time = value.updated_date;
          // console.log("result time", value.time)
					// value.description = value.cta_value = 'New Order Coming!';
          var html = value.title;
          var div = document.createElement('div');
          div.innerHTML = html;
        //   console.log("result text", div.innerText)
          value.title_message = div.innerText
          this.inboxData.push(value);
          
          
          
				});
				console.log("testttt", this.inboxData.length);
			}
		} catch (e) {
			console.log('Error', e);
		}
		// for (let n = 0; n < 25; n++) {
		//   let read;
		//   if (n % 5) {
		//     read = 'unread'
		//   }
		//   else {
		//     read = 'read'
		//   }

		//   this.getId = result.result[0]._id;
		//   console.log("hasil", this.getId)

		//   // this.inboxData.push({
		//   //   title: 'lorem ipsum ' + n,
		//   //   message: this.inboxData[0].message,
		//   //   short_description: 'new data lorem ipsum dolor sit amet ' + n,
		//   //   time_string: '9:' + n + ' AM',
		//   //   read: read
		//   // })
		// }
	}

	// async secondLoad() {
	// 	var html = '<p>Hello, <b>World</b>';
	// 	var div = document.createElement('div');
	// 	div.innerHTML = html;
	// 	alert(div.innerText); // Hello, World
	// }

	goToOrderDetail(order_id){
		console.log('ini IDNya',order_id)
		this.router.navigate(['merchant-portal/order-detail/edit'], {queryParams : {id: order_id}})
	}

	delete(id){
		console.log("button")
	}

	public async messageDetail(inboxData) {
		this.hideColumnWhenOpen = true;
		this.selectedData = inboxData;
		this.message = this.selectedData;
		console.log(this.selectedData.reference.order_id)
		// this.detail = await this.inboxService.detailInbox(this.getId);
		this.inboxService.readInbox(inboxData._id);
		this.firstLoad();
	}

	closeData() {
		this.hideColumnWhenOpen = false;
	}
}
