export const adminManagementRoutePath = [
    { path: 'admin', loadChildren: '../modules/admin/admin.module#AdminModule' , label: ''},
    { path: 'admin-group', loadChildren: '../modules/admin-group/admin-group.module#AdminGroupModule' , label: ''},
]