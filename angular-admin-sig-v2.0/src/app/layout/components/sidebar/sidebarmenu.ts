export let sidebarmenu ={
    "Order History":[
                    {label: "Summary", routerLink:'/administrator/order-history-summary'},
                    {label: "All History", routerLink:'/administrator/orderhistoryallhistoryadmin'},
                    {label: "Success", routerLink:'/administrator/orderhistorysuccessadmin'},
                    // {label: "Pending", routerLink:'/administrator/orderhistorypendingadmin'},
                    {label: "Cancel", routerLink:'/administrator/orderhistorycanceladmin'},
                    // {label: "Failed", routerLink:'/administrator/orderhistoryfailedadmin'},
                    {label: "Waiting", routerLink:'/administrator/orderhistorywaitingadmin'},
                    // {label: "Error", routerLink:'/orderhistoryerroradmin'},
                    // {label: "Wait for confirm", routerLink:'/orderhistoryerroradmin'},
                ],
    "Sales Redemption":[
                    {label: "Report", routerLink:'/administrator/salesredemption'},
                    {label: "Generate BAST", routerLink:'/administrator/salesredemptionbast'}
                ],
    "Member":[
                    // {label: "Summary", routerLink:'/administrator/membersummary'},
                    // {label: "All Members", routerLink:'/administrator/allmembersadmin'},
                    {label: "Member Point", routerLink:'/administrator/memberadmin'},
                    {label: "Data Member", routerLink:'/administrator/datamember'},
                    // {label: "Guests", routerLink:'/administrator/guestadmin'},
                    // {label: "Memberships", routerLink:'/administrator/membershipadmin'},
                ],
    "Products":[
                    // {label: "Upload Product CSV", routerLink:'/administrator/testproductadmin'},
                    {label: "Add", routerLink:'/administrator/productadmin/add'},
                    {label: "Product", routerLink:'/administrator/productadmin'},
                    {label: "Evoucher", routerLink:'/administrator/vouchers'},
                ],
    "Payment":[
                    {label: "Payment Gateway", routerLink:'/administrator/paymentgatewayadmin'}
                ],
    "Loyalty":[
                    // {label: "Points Model Group", routerLink:'/administrator/pointsgroup'},
                    {label: "Points Models Setup", routerLink:'/administrator/pointsmodels'},
                    // {label: "Campaign Management", routerLink:'/administrator/pointscampaign'},
                    // {label: "Promo Campaign", routerLink:'/administrator/promocampaign'},
                ],
    "Point Movement":[
                    {label: "Based on Product", routerLink:'/administrator/pointmovement'},
                    {label: "Based on Order", routerLink:'/administrator/pointmovementorder'},
                ],
    "Notification":[
                    {label: "Notification Group", routerLink:'/administrator/notificationgroupadmin'},
                    {label: "Notification Setting", routerLink:'/administrator/notificationsettingadmin'},
                    {label: "Notification Broadcast", routerLink:'/administrator/notificationbroadcastadmin'},
                    {label: "Notification Message", routerLink:'/administrator/notificationmessageadmin'},
                ],
    "Evoucher":[
                    // {label: "Evoucher Generated", routerLink:'/administrator/evoucheradmin'},
                    {label: "Evoucher Sales Report", routerLink:'/administrator/evouchersalesreport'},
                    {label: "Generated Voucher List", routerLink:'/administrator/evoucheradmin'},
                ],
    "Report":[
                    {label: "Sales Summary report", routerLink:'/administrator/order-history-summary'},
                    {label: "Member Demography", routerLink:'/administrator/member-demography-report'},
                    {label: "Sales Order", routerLink:'/administrator/salesorder'},
                    {label: "Traffic Activity", routerLink:'/administrator/activity-report'},
                    {label: "Product Activity", routerLink:'/administrator/product-activity-report'},
                    {label: "Report", routerLink:'/administrator/reportadmin'},
                    {label: "Report Error", routerLink:'/administrator/reporterror'},
                    {label: "CRR", routerLink:'/administrator/crr'},
                    {label: "CNR", routerLink:'/administrator/cnr' },
                ],

    "Product":[
        {label:"Product", routerLink:'/administrator/product'},
        {label:"Evoucher", routerLink:'/administrator/evoucher'}
    ]
};