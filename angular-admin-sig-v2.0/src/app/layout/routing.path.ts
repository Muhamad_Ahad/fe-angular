import { RouterMasterComponent } from "./router-master/router-master.component";

export const routePath:any = [
    { path: '', component:RouterMasterComponent, label:"router master component"},
    { path: 'administrator', loadChildren: './administrator/administrator.module#AdministratorModule' , label: 'Administrator'},
    { path: 'merchant-portal', loadChildren: './merchant-portal/merchant-portal.module#MerchantPortalModule', label: "Merchant portal" },
    { path: 'admin-management', loadChildren: './admin-management/admin-management.module#AdminManagementModule' , label: 'Admin Management'},


]
