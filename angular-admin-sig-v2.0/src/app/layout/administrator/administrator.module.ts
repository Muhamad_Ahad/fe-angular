import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdministratorRoutingModule } from './administrator-routing.module';
import { AdministratorComponent } from './administrator.component';
import { SidebarComponent } from '../components/sidebar/sidebar.component';
import { NestedSidebarComponent } from '../components/nested-sidebar/nested-sidebar.component';
// import { HeaderComponent } from '../components/header/header.component';
import { HeaderModule } from '../components/header/header.module';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../component-libs/form-builder-table/form-builder-table.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsComponentModule } from '../modules/bs-component/bs-component.module';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { OrderhistoryallhistoryModule } from '../modules/orderhistoryallhistory/orderhistoryallhistory.module';

@NgModule({
  declarations: [AdministratorComponent,SidebarComponent, NestedSidebarComponent],
  imports: [
    CommonModule,
    AdministratorRoutingModule,
    NgbDropdownModule,
    TranslateModule,
    FormBuilderTableModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    // FormBuilderTableModule,
    BsComponentModule,
    HeaderModule,
    // CKEditorModule,
    // OrderhistoryallhistoryModule,
  ]
})
export class AdministratorModule { }
