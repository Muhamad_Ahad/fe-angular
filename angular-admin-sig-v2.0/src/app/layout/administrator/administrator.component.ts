import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PermissionObserver } from '../../services/observerable/permission-observer';

@Component({
  selector: 'app-administrator',
  templateUrl: './administrator.component.html',
  styleUrls: ['./administrator.component.scss']
})
export class AdministratorComponent implements OnInit {

  collapedSideBar: boolean;

  constructor(private route:Router, private castPermission: PermissionObserver) {
    this.castPermission.currentPermission.subscribe((permissionVal) => {
      if(permissionVal != 'admin'){
        this.route.navigateByUrl("/");
      }
    });
  }

  ngOnInit() {}

  receiveCollapsed($event) {
      this.collapedSideBar = $event;
  }

}
