import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../../router.animations';
import { EVoucherService } from '../../../../../services/e-voucher/e-voucher.service';
import { del } from 'selenium-webdriver/http';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-evoucher-stock-report-detail',
  templateUrl: './evoucher-stock-report.detail.component.html',
  styleUrls: ['./evoucher-stock-report.detail.component.scss'],
  animations: [routerTransition()]
})

export class EVStockDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  evoucherStockDetail;
  processNumber;
  processNow: any = false;
  service;
  edit: boolean = false;
  errorLabel: any = false;
  constructor(
    public evoucherService: EVoucherService, 
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    this.evoucherStockDetail = this.detail;
    this.processNumber = this.detail.process_number;
    try {
      const result : any= await this.evoucherService.detailEvoucherStock(this.processNumber);
      console.warn("result detail stock", result)
      if(result.length > 0){
        if(result[0].process_number == this.detail.process_number){
          this.evoucherStockDetail = result[0];
        }
      }
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  backToHere() {
    // this.evoucherStockDetail = false;
    this.back[1](this.back[0]);
    // this.firstLoad();
  }

  isProcessNow() {
    this.processNow = !this.processNow;
  }

  async processEvoucher(process_number) {
    try {
      let dataProcess = 
      {
        "process_number":[process_number],
        "status":"approve"
      }
      let result: any = await this.evoucherService.processEvoucherCode(dataProcess);
      console.warn("result process", result)
      if (result) {
        Swal.fire({
          title: 'Success',
          text: 'Voucher Code is Approved',
          icon: 'success',
          confirmButtonText: 'Ok',
        }).then((result) => {
          if(result.isConfirmed){
            this.backToHere();
          }
        });
      }
    } catch(e) {
      console.warn("error appove", e);
      alert("Error");
    }
  }

  private async loadDetail(historyID: string){
    try {
      this.service = this.evoucherService;
      let result: any = await this.evoucherService.detailEvoucherStock(historyID);
      this.evoucherStockDetail = result.result[0];
      console.log(this.evoucherStockDetail);
    } catch (error) {
      
    }
  }



}
