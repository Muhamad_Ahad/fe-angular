import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { routerTransition } from '../../../../router.animations';
import { ActivatedRoute, Router } from '@angular/router';
import { EVoucherService } from '../../../../services/e-voucher/e-voucher.service';
import { FormOptions, TableFormat, eVouchersSalesReportTableFormat } from '../../../../object-interface/common.object';

@Component({
  selector: 'app-evoucher-stock-report',
  templateUrl: './evoucher-stock-report.component.html',
  styleUrls: ['./evoucher-stock-report.component.scss'],
  animations: [routerTransition()]
})
export class EvoucherStockReportComponent implements OnInit {
  swaper = true;
  Evouchers       : any = [];
  row_id        : any = "process_number";

  tableFormat   : TableFormat = {
    title           : 'E-Voucher Stock Report',
    label_headers   : [
                  {label: 'Process Number', visible: true, type: 'string', data_row_name: 'process_number'},
                  {label: 'Process Type', visible: true, type: 'string', data_row_name: 'process_type'},
                  {label: 'Status', visible: true, type: 'string', data_row_name: 'status'},
                  {label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_qty'},
                  {label: 'Total SKU', visible: true, type: 'string', data_row_name: 'total_sku'},
                  {label: 'Total Value', visible: true, type: 'string', data_row_name: 'total_value'},
                  {label: 'Total Price', visible: true, type: 'string', data_row_name: 'total_price'},
                  { label: 'Requested Date', visible: true, type: 'date', data_row_name: 'request_date' },
                  // { label: 'Approved Date', visible: true, type: 'date', data_row_name: 'approve_at' },
                ],
    row_primary_key : 'process_number',
    formOptions     : {
                    addForm   : true,
                    row_id    : this.row_id,
                    this      : this,
                    result_var_name : 'Evouchers',
                    detail_function : [this, 'callDetail'] 
                    }
  };

  tableFormat2   : TableFormat = {
    title           : 'E-Voucher Stock Report',
    label_headers   : [
                  {label: 'Process Number', visible: true, type: 'string', data_row_name: 'process_number'},
                  {label: 'Process Type', visible: true, type: 'string', data_row_name: 'process_type'},
                  {label: 'Status', visible: true, type: 'string', data_row_name: 'status'},
                  {label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_qty'},
                  {label: 'Total SKU', visible: true, type: 'string', data_row_name: 'total_sku'},
                  {label: 'Total Value', visible: true, type: 'string', data_row_name: 'total_value'},
                  {label: 'Total Price', visible: true, type: 'string', data_row_name: 'total_price'},
                  { label: 'Requested Date', visible: true, type: 'date', data_row_name: 'request_date' },
                  // { label: 'Approved Date', visible: true, type: 'date', data_row_name: 'approve_at' },
                ],
    row_primary_key : 'process_number',
    formOptions     : {
                    addForm   : true,
                    row_id    : this.row_id,
                    this      : this,
                    result_var_name : 'Evouchers',
                    detail_function : [this, 'callDetail'] 
                    }
  };

  form_input    : any = {};
  errorLabel    : any = false;
  errorMessage  : any = false;
  totalPage     : 0;
 
  evoucherStockDetail: any = false;
  service       : any ;
  srcDownload   : any ;
  

  // private options = new RequestOptions(
  //   { headers: new Headers({ 'Content-Type': 'application/json' }) });
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public evoucherService: EVoucherService, 
    public sanitizer: DomSanitizer) {

    this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
  }

  ngOnInit() {
    this.firstLoad();
  }

  async firstLoad() {
    try{
      this.service = this.evoucherService;
      let result:any = {}
      if(this.swaper == true){
        result = await this.evoucherService.getEvoucherStockReport('REQUESTED');
      }else{
        result= await this.evoucherService.getEvoucherStockReport('APPROVED');
      }

      // const result: any = await this.evoucherService.getEvoucherStockReport();
      this.totalPage = result.total_page;
      this.Evouchers = result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);


      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
      }
    }
  }

  public async callDetail(data, rowData) {
    console.warn("data", data)
    console.warn("rowData", rowData)
    try {      
      this.evoucherStockDetail = this.Evouchers.find(evoucher => evoucher.process_number == rowData.process_number);
      // console.warn("Member detail",this.memberDetail);
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
      }
    }

    // console.warn("_id", _id);
    // this.router.navigate(['administrator/evoucherstockreport/detail'], {queryParams: {id:_id}})
  }

  swapClick(bool) {
    this.swaper = bool;
    this.firstLoad();
  }

  public async backToHere(obj) {
    obj.evoucherStockDetail = false;
    obj.firstLoad();
  }



}