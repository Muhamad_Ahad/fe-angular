import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PromotionComponent } from './promotion.component';
import { PromotionGenerateComponent } from './generate/promotion.generate.component';

const routes: Routes = [
    { path: '', component: PromotionComponent },
    { path: 'generate', component: PromotionGenerateComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PromotionRoutingModule {
}
