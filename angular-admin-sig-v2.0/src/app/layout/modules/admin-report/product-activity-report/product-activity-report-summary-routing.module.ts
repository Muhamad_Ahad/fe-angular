import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductActivityReportSummaryComponent } from './product-activity-report-summary.component';

const routes: Routes = [
  {
      path: '', component: ProductActivityReportSummaryComponent,

  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductActivityReportSummaryRoutingModule { }
