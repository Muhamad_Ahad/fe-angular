import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActivityReportSummaryComponent } from './activity-report-summary.component';

const routes: Routes = [
  {
      path: '', component: ActivityReportSummaryComponent,

  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActivityReportSummaryRoutingModule { }
