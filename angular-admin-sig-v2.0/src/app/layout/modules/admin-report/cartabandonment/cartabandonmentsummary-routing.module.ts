import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CartAbandonmentSummaryComponent } from './cartabandonmentsummary.component';

const routes: Routes = [
  {
      path: '', component: CartAbandonmentSummaryComponent,

  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CartAbandonmentSummaryRoutingModule { }
