import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { DomSanitizer } from '@angular/platform-browser';
import { TopBarMenuItem, GeneralComponent } from '../../../../object-interface/common.object';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';
import { ActivityService } from '../../../../services/activity/activity.service';


@Component({
  selector: 'app-CartAbandonmentsummary-CartAbandonment',
  templateUrl: './cartabandonmentsummary.component.html',
  styleUrls: ['./cartabandonmentsummary.component.scss'],
  animations: [routerTransition()]
})


export class CartAbandonmentSummaryComponent implements OnInit, GeneralComponent {

  title: "Cart Abandonment Page"
  data: any = [];
  service;
  errorLabel: string
  topBarMenu: TopBarMenuItem[] = [
    { label: "Number of Sales", routerLink: '/administrator/order-history-summary' },
    { label: "Amount of Sales", routerLink: '/administrator/order-amount-history-summary' },
    { label: "Abondonment Rate", routerLink: '/administrator/cart-abandonment-summary', active: true }
  ];
  topMembers: []
  avgData = {
    monthly: {
      all: 0,
      pending: 0,
      paid: 0,
      checkout: 0,
      waiting: 0,
      cancel: 0
    },
    daily: {
      all: 0,
      pending: 0,
      paid: 0,
      checkout: 0,
      waiting: 0,
      cancel: 0
    },
    yearly: {
      all: 0,
      pending: 0,
      paid: 0,
      checkout: 0,
      waiting: 0,
      cancel: 0
    },
    hourly: {
      all: 0, pending: 0, paid: 0, checkout: 0, waiting: 0, cancel: 0
    }
  }
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          callback: this.currencyFormatter,
        }
      }]
    },
  };

  public barChartLabels: any = [];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any = [
    { data: [], label: 'Abandonment Rate' },
  ];

  public optO = { fill: false, borderWidth: 1, }
  public analitycsData: any = {
    monthly: {
      barChartData: [{ data: [], label: 'Abandonment Rate', ...this.optO }],
      barChartLabels: []
    },
    daily: {
      barChartData: [{ data: [], label: 'Abandonment Rate', ...this.optO }],
      barChartLabels: []
    },
    hourly: {
      barChartData: [{ data: [], label: 'Abandonment Rate', ...this.optO }],
      barChartLabels: []
    },
    yearly: {
      barChartData: [{ data: [], label: 'Abandonment Rate', ...this.optO }],
      barChartLabels: []
    }
  }

  constructor(public orderhistoryService: OrderhistoryService,
    public sanitizer: DomSanitizer,
    public activityService: ActivityService) {

    this.service = orderhistoryService;
    this.activityService.addLog({ page: this.title });
  }

  onUpdateCart(data) {
    const clonedData = JSON.parse(JSON.stringify(data.barChartData));
    const clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = [];

    data.barChartLabels.forEach(() => { clone[0].data.push(Math.round(Math.random() * 100)) });
    data.barChartData = clone;

    setTimeout(() => {
      data.barChartData = clonedData;
    }, 500)

  }

  async ngOnInit() {
    let currentDate = new Date();
    console.log(this.barChartLabels);
    this.firstLoad();

  }

  generateAbandonmentChartData(PaidTransaction, AllOrder) {

    let barChartLabels = [];
    let newData = [];


    let total = 0;
    // console.log(PaidTransaction);
    // PaidTransaction.forEach((val , i) => {
    //   console.log("VAL I", val, i);
    // });

    for (let data in PaidTransaction) {

      let rate = !AllOrder[data] || AllOrder[data] <= 0 ? 0 : ((1 - (PaidTransaction[data] / AllOrder[data])) * 100);

      if (rate == 0) {
        continue;

      }


      let floatRate = parseFloat(rate.toFixed(2));
      barChartLabels.push(data)
      total += floatRate;
      newData.push(floatRate)

    }
    console.log("new Data", newData)


    return [barChartLabels, newData, total]
  }

  async firstLoad() {

    try {
      this.service = this.orderhistoryService;
      const result: any = await this.orderhistoryService.getOrderHistorySummaryByDate('today');
      this.data = result.result;

      this.data.shopping_cart_abandonment_rate = this.data.current_order_history.total == 0 ? 0 :
        (1 - (this.data.current_order_history_paid.total / this.data.current_order_history.total)) * 100;


      if (this.data.order_history_by_month) {

        const cvrtdDataMonthly: any = this.generateAbandonmentChartData(
          this.data.order_history_by_month_paid.value,
          this.data.order_history_by_month.value);

        this.avgData.monthly.all = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_month.value));
        this.avgData.monthly.paid = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_month_paid.value));

        // console.log("cvrtdDataMonthly", cvrtdDataMonthly);
        const clone = JSON.parse(JSON.stringify(this.analitycsData.monthly.barChartData));
        clone[0].data = cvrtdDataMonthly[1];


        console.log("cvrtdDataMonthly clone", clone);

        this.barChartLabels = cvrtdDataMonthly[0]
        this.analitycsData.monthly.barChartLabels = cvrtdDataMonthly[0]

        // this.barChartLabels = cvrtdDataMonthly[0]
        // this.analitycsData.monthly.barChartLabels = cvrtdDataMonthly[0]
        // this.barChartData   = clone;
        this.analitycsData.monthly.barChartData = clone;

      }
      if (this.data.order_history_by_the_day) {
        console.log("cvrtdDataDaily");

        const cvrtdDataDaily: any = this.generateAbandonmentChartData(
          this.data.order_history_by_the_day_paid.value,
          this.data.order_history_by_the_day.value
        );

        this.avgData.daily.all = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_the_day.value));
        this.avgData.daily.paid = Math.floor(this.calculateTheAverageValue(this.data.order_history_by_the_day_paid.value));


        const clone = JSON.parse(JSON.stringify(this.analitycsData.daily.barChartData));


        clone[0].data = cvrtdDataDaily[1];


        this.analitycsData.daily.barChartLabels = cvrtdDataDaily[0]
        this.barChartLabels = cvrtdDataDaily[0]
        // this.barChartData   = clone;
        this.analitycsData.daily.barChartData = clone;

      }
      if (this.data.order_history_hourly) {

        const cvrtdDataHourly: any = this.generateAbandonmentChartData(
          this.data.order_history_hourly_paid.value,
          this.data.order_history_hourly.value
        );

        this.avgData.hourly.all = Math.floor(this.calculateTheAverageValue(this.data.order_history_hourly.value));
        this.avgData.hourly.paid = Math.floor(this.calculateTheAverageValue(this.data.order_history_hourly_paid.value));


        const clone: any = this.analitycsData.hourly.barChartData;

        SETUP_DATA_FOR_HOURLY: {
          clone[0].data = cvrtdDataHourly[1];


        }

        this.analitycsData.hourly.barChartLabels = cvrtdDataHourly[0]
        this.barChartLabels = cvrtdDataHourly[0]
        // this.barChartData   = clone;
        this.analitycsData.hourly.barChartData = clone;

      }

      if (this.data.order_history_yearly) {

        const cvrtdDataYearly: any = this.generateAbandonmentChartData(
          this.data.order_history_yearly_paid.value,
          this.data.order_history_yearly.value
        );
        this.avgData.yearly.all = Math.floor(this.calculateTheAverageValue(this.data.order_history_yearly.value));
        this.avgData.yearly.paid = Math.floor(this.calculateTheAverageValue(this.data.order_history_yearly_paid.value));

        // const clone = JSON.parse(JSON.stringify(this.analitycsData.yearly.barChartData));
        const clone: any = this.analitycsData.yearly.barChartData;

        SETUP_DATA_FOR_HOURLY: {
          clone[0].data = cvrtdDataYearly[1];
        }

        this.analitycsData.yearly.barChartLabels = cvrtdDataYearly[0]
        this.barChartLabels = cvrtdDataYearly[0]
        this.analitycsData.yearly.barChartData = clone;

        console.log("his.analitycsData yearly", this.analitycsData.yearly)

        TOP_MEMBERS_SETUP: {
          this.topMembers = this.data.top_users.value
        }
      }
    } catch (e) {
      console.log("this e result", e)

      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  calculateTheAverageValue(_value: Object) {
    let howMany: number = Object.keys(_value).length
    let sum = 0;

    Reflect.ownKeys(_value).forEach((key) => {
      sum = sum + _value[key];
    });

    let avg = sum / howMany;
    return avg;
  }


  currencyFormatter(value, index, values) {

    // add comma as thousand separator
    return value + '%';

  }

}
