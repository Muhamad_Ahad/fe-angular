import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';
import { isFunction } from '../../../../object-interface/common.function';


@Component({
  selector: 'app-orderhistorypending-edit',
  templateUrl: './orderhistorypending.edit.component.html',
  styleUrls: ['./orderhistorypending.edit.component.scss'],
  animations: [routerTransition()]
})

export class OrderhistorypendingEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  public loading: boolean = false;
  public done: boolean = false;
  public shippingStatusList: any = [
    { label: 'on checking and processing', value: 'on process', id: 'on-process', checked: true, hovered: false },
    { label: 'Warehouse Packaging', value: 'on packaging', id: 'warehouse-packaging', hovered: false },
    { label: 'On Delivery process', value: 'on delivery', id: 'on-delivery', hovered: false },
    { label: 'Delivered', value: 'delivered', id: 'delivered', hovered: false }
  ]

  public shippingServices: any = [
    { label: 'no services', value: 'no-services', id: 'noshipping' },
    // {label: 'JNE', value: 'JNE', id:'jne'},
    { label: 'SAP', value: 'SAP', id: 'sap' },
    { label: 'Si Cepat', value: 'Si Cepat', id: 'sicepat' },
    // {label: 'Gojek', value: 'Gojek', id:'gojek'}
  ]

  public transactionStatus: any = [
    { label: "PENDING", value: "PENDING" }
    , { label: "CANCEL", value: "CANCEL" }
    , { label: "ORDER", value: "ORDER" }
    , { label: "CHECKOUT", value: "CHECKOUT" }
    , { label: "PAID", value: "PAID" }
    , { label: "ERROR", value: "ERROR" }
    , { label: "WAITING", value: "WAITING" }
  ];

  errorLabel: any = false;
  transaction_status: any;
  merchantMode = false;

  constructor(public orderhistoryService: OrderhistoryService) { }

  ngOnInit() {
    this.firstLoad();

  }

  isShippingValueExists(value) {
    for (let element of this.detail.shipping_info) {
      console.log("value", value, element.label);
      if (element.label.trim().toLowerCase() == value.trim().toLowerCase()) { return [true, element.created_date] }
    }
    return false;
  }

  ngOnChange() {
    console.log("HELLO");
  }

  async firstLoad() {

    try {
      if (this.detail.billings) {
        let billings = this.detail.billings
        let additionalBillings = [];
        if (billings.discount) {
          additionalBillings.push({
            product_name: billings.discount.product_name,
            value: billings.discount.value
          })
        }

        if (this.detail.payment_detail.payment_via != 'manual banking') {
          if (billings.unique_amount) {
            delete billings.unique_amount;
          }
        }

        for (let prop in billings) {
          console.log("PROP", prop);
          if (prop == 'shipping_fee' || prop == 'unique_amount') {
            additionalBillings.push({
              product_name: prop,
              value: billings[prop]
            })
          }
        }
        console.log(this.detail.billings);
        this.detail.additionalBillings = additionalBillings
      }

      if (this.detail.merchant) {
        this.merchantMode = true;
      }
      if (!this.detail.shipping_services) {
        this.detail.shipping_services = 'no-services';
        // console.log("Result", shippingServices)
      }
      if (this.detail.status) {
        this.detail.status = this.detail.status.trim().toUpperCase();
      }

      if (this.detail.shipping_info) {
        console.log('this.detail.shipping_info', this.detail.shipping_info)
        this.shippingStatusList.forEach((element, index) => {
          let check = this.isShippingValueExists(element.value);
          if (check) {
            this.shippingStatusList[index].checked = check[0];
            this.shippingStatusList[index].created_date = check[1];
          }
          else {
            this.shippingStatusList[index].checked = false;
          }
        });
      }

      this.detail.previous_status = await this.detail.status;
      this.transactionStatus.forEach((element, index) => {
        if (element.value == this.detail.transaction_status) {
          this.transactionStatus[index].selected = 1;
        }
      });
    }
    catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

    // this.detail.previous_member_id = this.detail.member_id;
    // this.memberStatus.forEach((element, index) => {
    //     if(element.value == this.detail.member_status){
    //         this.memberStatus[index].selected = 1;
    //     }
    // });

  }

  backToDetail() {
    if (this.back[0][this.back[1]] && !isFunction(this.back[0])) {
      this.back[0][this.back[1]];
    }
    else if (isFunction(this.back[0])) {
      this.back[0]();
    }
    else {
      window.history.back();
    }
  }

  changeshippingStatusList(index) {
    for (let n = 0; n <= index; n++) {
      //  console.log("HOVERED",  this.shippingStatusList[n].hovered)
      this.shippingStatusList[n].checked = true;
    }


    for (let n = index + 1; n < this.shippingStatusList.length; n++) {
      this.shippingStatusList[n].checked = false;
    }
  }

  hoverShippingStatusList(index, objectHover) {

    // console.log("HOVERED", index)

    for (let n = 0; n <= index; n++) {
      //  console.log("HOVERED",  this.shippingStatusList[n].hovered)
      this.shippingStatusList[n].hovered = true;
    }


    for (let n = index + 1; n < this.shippingStatusList.length; n++) {
      this.shippingStatusList[n].hovered = false;
    }

  }

  convertShippingStatusToShippingInfo() {

    let newData = []
    this.shippingStatusList.forEach((element, index) => {
      if (element.checked) {
        newData.push({ value: element.value })
      }

    });
    return newData;
  }
  async saveThis() {
    try {
      this.loading = !this.loading;
      let frm = JSON.parse(JSON.stringify(this.detail));
      frm.shipping_info = this.convertShippingStatusToShippingInfo();
      // this.detail.shipping_status = this.valu
      delete frm.buyer_detail
      delete frm.payment_expire_date
      delete frm.products
      await this.orderhistoryService.updateOrderHistoryAllHistory(frm);
      this.loading = !this.loading;
      this.done = true;
      alert("Edit submitted")
    }
    catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }
  
}
