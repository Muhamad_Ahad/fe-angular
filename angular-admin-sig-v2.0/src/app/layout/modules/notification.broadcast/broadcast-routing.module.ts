import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BroadcastComponent } from './broadcast.component';
import { BroadcastAddComponent } from './add/broadcast.add.component';
import { BroadcastDetailComponent } from './detail/broadcast.detail.component';

const routes: Routes = [
  {
      path: '', component: BroadcastComponent,

  },
   {
      path:'add', component: BroadcastAddComponent
  },
  {
    path:'detail', component: BroadcastDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BroadcastRoutingModule{ }
