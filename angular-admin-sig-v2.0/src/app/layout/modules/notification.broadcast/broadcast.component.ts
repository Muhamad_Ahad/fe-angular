import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { BroadcastService } from '../../../services/notification.broadcast/broadcast.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
  selector: 'app-notification-setting',
  templateUrl: './broadcast.component.html',
  styleUrls: ['./broadcast.component.scss'],
  animations: [routerTransition()]
})

export class BroadcastComponent implements OnInit {

  Broadcast       : any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Notification Broadcast Detail',
                                  label_headers   : [
                                    {label: 'Organization ID', visible: true, type: 'string', data_row_name: 'org_id'},
                                    {label: 'From', visible: true, type: 'string', data_row_name: 'message_from'},
                                      // {label: 'Setting ID', visible: false, type: 'string', data_row_name: 'setting_id'},
                                      {label: 'Group ID', visible: true, type: 'string', data_row_name: 'group_id'},
                                      // {label: 'Template ID', visible: false, type: 'string', data_row_name: 'template_id'},
                                      {label: 'Status',
                                          options: [
                                            'active',
                                            'inactive',
                                          ],
                                          visible: true, type: 'list', data_row_name: 'status'},
                                      ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                      addForm: true,
                                                    this  : this,
                                                    result_var_name: 'Broadcast',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  form_input    : any = {};
  errorLabel : any = false;

  broadcastDetail: any = false;
  service     : any ;

  constructor(public broadcastService:BroadcastService) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    try {
      this.service    = this.broadcastService;
      const result: any  = await this.broadcastService.getBroadcastLint();
      this.Broadcast = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  public async callDetail(_id) {
    
    try {
      this.service    = this.broadcastService;
      let result: any = await this.broadcastService.detailBroadcast(_id);
      this.broadcastDetail = result.result;
      console.log('detail', this.broadcastDetail);
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj) {
    obj.broadcastDetail = false;
  }

}
