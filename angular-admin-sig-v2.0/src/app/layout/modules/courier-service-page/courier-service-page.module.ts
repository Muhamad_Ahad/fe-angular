import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CourierServicePageRoutingModule } from './courier-service-page.routing.module';
import { CourierServicePageComponent } from './courier-service-page.component';
import { PageHeaderModule } from '../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { CourierservicepageDetailComponent } from './detail/courierservicepage.detail.component';
import { CourierservicepageEditComponent } from './edit/courierservicepage.edit.component';

@NgModule({
    imports: [
        CommonModule, 
        CourierServicePageRoutingModule,
        PageHeaderModule, 
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        FormBuilderTableModule,
        BsComponentModule    
    ],
    declarations: [
        CourierServicePageComponent,
        CourierservicepageDetailComponent,
        CourierservicepageEditComponent
    ]
})
export class CourierServicePageModule {}