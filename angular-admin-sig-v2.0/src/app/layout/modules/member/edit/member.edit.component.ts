import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { MemberService } from '../../../../services/member/member.service';


@Component({
  selector: 'app-member-edit',
  templateUrl: './member.edit.component.html',
  styleUrls: ['./member.edit.component.scss'],
  animations: [routerTransition()]
})

export class MemberEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;
  errorLabel : any = false;
  member_status:any;
  activation_status:any;
  
  public loading        : boolean = false;

  public activationStatus: any = [
    {label: 'ACTIVATED', value: 'ACTIVATED'},
    {label: 'INACTIVED', value: 'INACTIVED'}
  ];

  public memberStatus: any = [
    {label: 'ACTIVE', value: 'ACTIVE'},
    {label: 'INACTIVE', value: 'INACTIVE'}
  ];

  constructor(public memberService: MemberService) {
  }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    console.log(this.detail)
    this.detail.previous_member_id = this.detail.member_id;
    this.activationStatus.forEach((element, index, products_ids) => {
        if (element.value == this.detail.activation_status) {
            this.activationStatus[index].selected = 1;
        }
    });

    this.detail.previous_member_id = this.detail.member_id;
    this.memberStatus.forEach((element, index, products_ids) => {
        if (element.value == this.detail.member_status) {
            this.memberStatus[index].selected = 1;
        }
    });

  }

  backToDetail() {
    // console.log(this.back);
    this.back[0][this.back[1]]();
  }

  async saveThis() {
    console.log("data", this.detail)
    let details = this.detail.buyer_detail[0]
    let new_form ={
      username : this.detail.username,
      address : {
        id : details.id,
      name : details.name,
      address : details.address,
      cell_phone : details.cell_phone,
      province_name : details.province_name,
			city_name : details.city_name,
			subdistrict_name : details.subdistrict_name,
			postal_code : details.postal_code,
			email : details.email,
      use_as : details.use_as,
      village_name: details.village_name
      }
      
    }
    try {
      // this.loading = !this.loading;
      await this.memberService.updateMemberAddress(new_form);
      alert("Edit berhasil");
      // this.loading = !this.loading;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
      alert("Error");
    }
  }

}
