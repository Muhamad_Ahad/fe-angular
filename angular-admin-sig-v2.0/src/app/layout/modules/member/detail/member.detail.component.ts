import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { MemberService } from '../../../../services/member/member.service';
import { del } from 'selenium-webdriver/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-member-detail',
  templateUrl: './member.detail.component.html',
  styleUrls: ['./member.detail.component.scss'],
  animations: [routerTransition()]
})

export class MemberDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  edit: boolean = false;
  errorLabel : any = false;
  constructor(public memberService: MemberService,private router: Router,private route: ActivatedRoute) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad(){


  }

  editThis() {
    // console.log(this.edit );
    this.edit = !this.edit;
    // console.log(this.edit );
  }
  backToTable() {
    // console.log(this.back);
    this.back[1](this.back[0]);
  }

  async delete(){
    try {
      const delResult: any = await this.memberService.deleteMember(this.detail);
      console.log(delResult);
      if (delResult.error == false) {
        console.log(this.back[0]);
        this.back[0].memberDetail = false;
        this.back[0].firstLoad();
      // delete this.back[0].prodDetail;
      }
    } catch (error) {
      throw new TypeError(error.error.error);
    }
  } catch (e) {
    this.errorLabel = ((<Error>e).message);//conversion to Error type
  }

  async pointHistory(){
    console.log('here')
    // this.router.navigate(['/member-point-history']);
    this.router.navigate(['administrator/memberadmin/point-history'],  {queryParams: {id: this.detail.email }})
  }

}
