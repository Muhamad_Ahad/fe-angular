import { ProductService } from '../../../../services/product/product.service';
import { Component, OnInit, Input, NgZone } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';
import Swal from 'sweetalert2';
import { basename } from 'path';
// import { ProductService } from '../../../../services/product/product.service';


@Component({
  selector: 'app-finance-invoice-report-edit',
  templateUrl: './finance-invoice-report.edit.component.html',
  styleUrls: ['./finance-invoice-report.edit.component.scss'],
  animations: [routerTransition()]
})

export class FinanceInvoiceReportEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;
  errorLabel : any = false;
  member_status:any;
  activation_status:any;

  private allBast : any = [];
  private allSuratKuasa : any = [];
  invoiceNumber: any = "";
  orderID: any;

  optionsKuasa1: any = [
    {label:"KUASA", value:"true"},
    {label:"TIDAK KUASA", value:"false"}
  ];
  optionsKuasa2 : any =  [
    {label:"KUASA", value:"KUASA"},
    {label:"TIDAK KUASA", value:"TIDAK KUASA"}
  ];
  optionsKuasa3 : any = [
    {label:"KUASA", value:"ya"},
    {label:"TIDAK KUASA", value:"tidak"}
  ];
  private updateSingle: any;
  
  public loading        : boolean = false;

  public activationStatus: any = [
    {label: 'ACTIVATED', value: 'ACTIVATED'},
    {label: 'INACTIVED', value: 'INACTIVED'}
  ];

  public memberStatus: any = [
    {label: 'ACTIVE', value: 'ACTIVE'},
    {label: 'INACTIVE', value: 'INACTIVE'}
  ];

  startUploading: any = false;
  cancel = false;
  progressBar = 0;

  errorFile: any = false;
  selectedFile: any;
  selFile: any;
  loadingKTPPemilik: boolean = false;
  loadingNPWPPemilik: boolean = false;
  loadingKTPPenerima: boolean = false;
  loadingNPWPPenerima: boolean = false;
  loadingBAST: any = [];
  loadingSuratKuasa: any = [];

  constructor(public OrderhistoryService: OrderhistoryService, private zone: NgZone) {
  }

  ngOnInit() {
    this.firstLoad();

  }

  checkValueKuasa(){
    if(this.updateSingle["hadiah_dikuasakan"] == "true" || this.updateSingle["hadiah_dikuasakan"] == "false"){
      let options = this.optionsKuasa1.map(el => {
        if(el.value == this.updateSingle["hadiah_dikuasakan"]){
          el.selected = 1;
        }
        return el;
      });
      this.optionsKuasa1 = options;
    }
  
    if(this.updateSingle["hadiah_dikuasakan"] == "KUASA" || this.updateSingle["hadiah_dikuasakan"] == "TIDAK KUASA"){
      let options = this.optionsKuasa2.map(el => {
        if(el.value == this.updateSingle["hadiah_dikuasakan"]){
          el.selected = 1;
        }
        return el;
      });
      this.optionsKuasa2 = options;
    }
  
    if(this.updateSingle["hadiah_dikuasakan"] == "ya" || this.updateSingle["hadiah_dikuasakan"] == "tidak"){
      let options = this.optionsKuasa3.map(el => {
        if(el.value == this.updateSingle["hadiah_dikuasakan"]){
          el.selected = 1;
        }
        return el;
      });
      this.optionsKuasa3 = options;
    }

    if(this.updateSingle["hadiah_dikuasakan"] == "-"){
      console.warn("im here!")
      let options = this.optionsKuasa2.map(el => {
        if(el.value == this.updateSingle["hadiah_dikuasakan"]){
          el.selected = 1;
        }
        return el;
      });
      this.optionsKuasa2 = options;
    }

  }

  isArray(curVar : any){
    return Array.isArray(curVar) ? true : false;
  }

  assignArrayBoolean(curVar : any, varCont : any){
    if(this.isArray(curVar)){
      curVar.forEach(element => {
        varCont.push(false);
      });
    }
  }

  async firstLoad() {
    // console.warn("detail",this.detail);

    this.updateSingle = JSON.parse(JSON.stringify(this.detail.member_detail));
    this.orderID = this.detail.order_id;

    if (this.invoiceNumber == "") {
      this.invoiceNumber = this.detail.additional_info.invoice_no;
    } else {
      this.invoiceNumber = this.invoiceNumber;
      this.detail.additional_info.invoice_no = this.invoiceNumber;
    }

    this.checkValueKuasa();
    
    this.detail.previous_member_id = this.detail.member_id;
    this.activationStatus.forEach((element, index, products_ids) => {
        if (element.value == this.detail.activation_status) {
            this.activationStatus[index].selected = 1;
        }
    });

    this.detail.previous_member_id = this.detail.member_id;
    this.memberStatus.forEach((element, index, products_ids) => {
        if (element.value == this.detail.member_status) {
            this.memberStatus[index].selected = 1;
        }
    });
  }

  async updateInvoiceNumber(){
    try
    {
        this.startUploading = true;
        this.cancel = false;
        let payload = {
          type : 'application/form-data',
        }
        console.warn("order id", this.orderID)
        console.warn("this.invoiceNumber", this.invoiceNumber);
        if(this.invoiceNumber && this.orderID) {
          // console.warn('text', this.invoiceNumber,this);
          const result: any = await this.OrderhistoryService.uploadInvoiceNumber(this.invoiceNumber,this,payload,this.orderID);
        }
      } 
      catch (e) {
        this.startUploading = false;
        this.errorLabel = ((<Error>e).message);//conversion to Error type

        let message = this.errorLabel;
        if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
          message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
        }
        Swal.fire({
          icon: 'error',
          title: message,
        });
      }
  }

  updateProgressBar(value){
    this.progressBar = value;
  }

  backToDetail() {
    // console.log("Edit member",this.back);
    this.back[0][this.back[1]]();
    // this.back[0]();
  }

  validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
  }

  openUpload(type:any, index:any){
    let upFile : any = document.getElementById(type+index);
    upFile.click();
  }


  convertDate(time){
    const date:any =  new Date(time);
    const dateString = date.toString();
    const retDate = dateString.split("GMT");
    
    return retDate[0];
  }

}
