import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FinanceInvoiceReportComponent } from './finance-invoice-report.component';
import { UpdateInvoiceComponent } from './update-invoice/finance-invoice-report.update-invoice.component';

const routes: Routes = [
  {
    path: '', component: FinanceInvoiceReportComponent
  },
  {
    path:'update-invoice', component: UpdateInvoiceComponent
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FinanceInvoiceReportRoutingModule { }
