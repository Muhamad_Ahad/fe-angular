import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';
import * as content from '../../../../assets/json/content.json';

@Component({
  selector: 'app-finance-invoice-report',
  templateUrl: './finance-invoice-report.component.html',
  styleUrls: ['./finance-invoice-report.component.scss']
})
export class FinanceInvoiceReportComponent implements OnInit {

  SalesRedemption: any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Finance Invoice report',
                                  label_headers   : [
                                    {label: 'Tanggal Proses', visible: true, type: 'date', data_row_name: 'approve_date'},
                                    {label: 'Tanggal Upload BAST', visible: true, type: 'bast_date', data_row_name: 'additional_info'},
                                    {label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id'},
                                    {label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko'},
                                    {label: 'AWB Number', visible: true, type: 'awb_number', data_row_name: 'delivery_detail'},
                                    {label: 'Nama Toko', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail'},
                                    {label: 'Alamat Toko', visible: true, type: 'form_alamat_toko', data_row_name: 'member_detail'},
                                    {label: 'Nama Pemilik', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail'},
                                    {label: 'No Telp Pemilik', visible: true, type: 'string', data_row_name: 'no_wa_pemilik'},
                                    {label: 'KTP Pemilik', visible: true, type: 'form_ktp_pemilik', data_row_name: 'member_detail'},
                                    {label: 'Foto KTP Pemilik', visible: true, type: 'form_foto_ktp_pemilik', data_row_name: 'member_detail'},
                                    {label: 'NPWP Pemilik', visible: true, type: 'form_npwp_pemilik', data_row_name: 'member_detail'},
                                    {label: 'Foto NPWP Pemilik', visible: true, type: 'form_foto_npwp_pemilik', data_row_name: 'member_detail'},
                                    {label: 'Hadiah Dikuasakan', visible: true, type: 'form_hadiah_dikuasakan', data_row_name: 'member_detail'},
                                    {label: 'Nama Penerima', visible: true, type: 'form_nama_penerima', data_row_name: 'member_detail'},
                                    {label: 'Alamat Penerima', visible: true, type: 'form_alamat_penerima', data_row_name: 'member_detail'},
                                    {label: 'No Telp Penerima', visible: true, type: 'form_wa_penerima', data_row_name: 'member_detail'},
                                    {label: 'KTP Penerima', visible: true, type: 'form_ktp_penerima', data_row_name: 'member_detail'},
                                    {label: 'Foto KTP Penerima', visible: true, type: 'form_foto_ktp_penerima', data_row_name: 'member_detail'},
                                    {label: 'NPWP Penerima', visible: true, type: 'form_npwp_penerima', data_row_name: 'member_detail'},
                                    {label: 'Foto NPWP Penerima', visible: true, type: 'form_foto_npwp_penerima', data_row_name: 'member_detail'},
                                    {label: 'Hadiah', visible: true, type: 'product_redeem', data_row_name: 'products'},
                                    {label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products'},
                                    {label: 'Product Code', visible: true, type: 'product_code_redeem', data_row_name: 'products'},
                                    {label: 'Price', visible: true, type: 'product_price_redeem', data_row_name: 'products'},
                                    {label: 'Total Order Price', visible: true, type: 'string', data_row_name: 'sum_total'},
                                    {label: 'Group', visible: true, type: 'form_group', data_row_name: 'member_detail'},
                                    {label: 'BAST', visible: true, type: 'bast', data_row_name: 'additional_info'},
                                    {label: 'Surat Kuasa', visible: true, type: 'surat_kuasa', data_row_name: 'additional_info'},
                                    {label: 'No Invoice', visible: true, type: 'value_invoice', data_row_name: 'invoice_no'},
                                    {label: 'Tanggal Invoice', visible: true, type: 'date_invoice', data_row_name: 'invoice_no'},
                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'SalesRedemption',
                                                    detail_function: [this, 'callDetail'],
                                                    salesRedemption : true,
                                                    updateInvoice: true,
                                                  },
                                                  show_checkbox_options: true
  };

  tableFormat2        : TableFormat = {
    title           : 'Finance Invoice report',
    label_headers   : [
      {label: 'Tanggal Proses', visible: true, type: 'date', data_row_name: 'approve_date'},
      {label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id'},
      {label: 'PO Number', visible: true, type: 'value_po', data_row_name: 'po_no'},
      {label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko'},
      {label: 'AWB Number', visible: true, type: 'awb_number', data_row_name: 'delivery_detail'},
      {label: 'Nama Pelanggan', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail'},
      {label: 'No Telp Pelanggan', visible: true, type: 'string', data_row_name: 'no_wa_pemilik'},
      {label: 'Alamat Pengiriman', visible: true, type: 'form_alamat_penerima', data_row_name: 'member_detail'},
      {label: 'Hadiah', visible: true, type: 'product_redeem', data_row_name: 'products'},
      {label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products'},
      {label: 'Product Code', visible: true, type: 'product_code_redeem', data_row_name: 'products'},
      {label: 'Price', visible: true, type: 'product_price_redeem', data_row_name: 'products'},
      {label: 'Total Order Price', visible: true, type: 'string', data_row_name: 'sum_total'},
      {label: 'Program', visible: true, type: 'form_group', data_row_name: 'member_detail'},
      {label: 'No Invoice', visible: true, type: 'value_invoice', data_row_name: 'invoice_no'},
      {label: 'Tanggal Invoice', visible: true, type: 'date_invoice', data_row_name: 'invoice_no'},
    ],
    row_primary_key : '_id',
    formOptions     : {
                      row_id: '_id',
                      this  : this,
                      result_var_name: 'SalesRedemption',
                      detail_function: [this, 'callDetail'],
                      salesRedemption : true,
                      updateInvoice: true,
                    },
                    show_checkbox_options: true
  };
  tableFormat3        : TableFormat = {
    title           : 'Finance Invoice report',
    label_headers   : [
      {label: 'Tanggal Proses', visible: true, type: 'date', data_row_name: 'approve_date'},
      {label: 'Tanggal Upload BAST', visible: true, type: 'bast_date', data_row_name: 'additional_info'},
      {label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id'},
      {label: 'AWB Number', visible: true, type: 'awb_number', data_row_name: 'delivery_detail'},
      {label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko'},
      {label: 'Nama Program', visible: true, type: 'form_cluster', data_row_name: 'member_detail'},
      {label: 'Nama Entitas', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail'},
      // {label: 'Alamat', visible: true, type: 'form_alamat_toko', data_row_name: 'member_detail'},
      {label: 'Nama PIC', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail'},
      {label: 'No Telp PIC', visible: true, type: 'string', data_row_name: 'no_wa_pemilik'},
      {label: 'KTP PIC', visible: true, type: 'form_ktp_pemilik', data_row_name: 'member_detail'},
      {label: 'Foto KTP PIC', visible: true, type: 'form_foto_ktp_pemilik', data_row_name: 'member_detail'},
      {label: 'NPWP PIC', visible: true, type: 'form_npwp_pemilik', data_row_name: 'member_detail'},
      {label: 'Foto NPWP PIC', visible: true, type: 'form_foto_npwp_pemilik', data_row_name: 'member_detail'},
      {label: 'Hadiah Dikuasakan', visible: true, type: 'form_hadiah_dikuasakan', data_row_name: 'member_detail'},
      {label: 'Nama Penerima', visible: true, type: 'form_nama_penerima', data_row_name: 'member_detail'},
      {label: 'Jabatan', visible: true, type: 'form_group', data_row_name: 'member_detail'},
      {label: 'Alamat Penerima', visible: true, type: 'form_alamat_penerima', data_row_name: 'member_detail'},
      {label: 'No Telp Penerima', visible: true, type: 'form_wa_penerima', data_row_name: 'member_detail'},
      {label: 'KTP Penerima', visible: true, type: 'form_ktp_penerima', data_row_name: 'member_detail'},
      {label: 'Foto KTP Penerima', visible: true, type: 'form_foto_ktp_penerima', data_row_name: 'member_detail'},
      {label: 'NPWP Penerima', visible: true, type: 'form_npwp_penerima', data_row_name: 'member_detail'},
      {label: 'Foto NPWP Penerima', visible: true, type: 'form_foto_npwp_penerima', data_row_name: 'member_detail'},
      {label: 'Hadiah', visible: true, type: 'product_redeem', data_row_name: 'products'},
      {label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products'},
      {label: 'Product Code', visible: true, type: 'product_code_redeem', data_row_name: 'products'},
      {label: 'Price', visible: true, type: 'product_price_redeem', data_row_name: 'products'},
      {label: 'Total Order Price', visible: true, type: 'string', data_row_name: 'sum_total'},
      {label: 'BAST', visible: true, type: 'bast', data_row_name: 'additional_info'},
      {label: 'Surat Kuasa', visible: true, type: 'surat_kuasa', data_row_name: 'additional_info'},
      {label: 'No Invoice', visible: true, type: 'value_invoice', data_row_name: 'invoice_no'},
      {label: 'Tanggal Invoice', visible: true, type: 'date_invoice', data_row_name: 'invoice_no'},
    ],
    row_primary_key : '_id',
    formOptions     : {
                      row_id: '_id',
                      this  : this,
                      result_var_name: 'SalesRedemption',
                      detail_function: [this, 'callDetail'],
                      salesRedemption : true,
                      updateInvoice: true,
                    },
                    show_checkbox_options: true
  };
public contentList : any = (content as any).default;
  form_input    : any = {};
  errorLabel : any = false;
  errorMessage  : any = false;
  totalPage  = 0;

  invoiceDetail: any = false;
  service: any;
  mci_project: any = false;
  programType:any = "";

  constructor(public OrderhistoryService:OrderhistoryService) { }

  ngOnInit() {
    this.firstLoad();

  }
  
  async firstLoad(){
    try{
      const program = localStorage.getItem('programName');
      let _this = this;
      this.contentList.filter(function(element) {
          if(element.appLabel == program) {
              if(element.type == "reguler") {
                  _this.mci_project = true;
              }  else if(element.type == "custom_kontraktual") {
                _this.programType = "custom_kontraktual";
              } else {
                  _this.mci_project = false;
              }
          }
      });

      this.service    = this.OrderhistoryService;
      let result: any;

      if(this.mci_project == true) {
        result  = await this.OrderhistoryService.getCompleteFinanceInvoiceReport();
      } else {
        result  = await this.OrderhistoryService.getFinanceInvoiceReport();
      }

      this.totalPage = result.total_page;
      this.SalesRedemption = result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
      }
    }
  }
  
  public async callDetail(data, rowData){
    try{
      this.invoiceDetail = this.SalesRedemption.find(order => order.order_id == rowData.order_id);
      // console.warn("invoice detail",this.invoiceDetail);
      
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj){
    obj.invoiceDetail = false;
    obj.firstLoad();
  }

  addMonths(date, months) {
    date.setMonth(date.getMonth() + months);
    return date;
  }
}
