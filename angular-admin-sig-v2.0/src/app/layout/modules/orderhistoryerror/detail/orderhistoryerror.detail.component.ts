import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';

@Component({
  selector: 'app-orderhistoryerror-detail',
  templateUrl: './orderhistoryerror.detail.component.html',
  styleUrls: ['./orderhistoryerror.detail.component.scss'],
  animations: [routerTransition()]
})

export class OrderhistoryerrorDetailComponent implements OnInit {
  @Input() public detail:any;
  @Input() public back;

  edit:boolean = false;
  
  constructor(public orderhistoryService:OrderhistoryService) {
  }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    
    
  }

  editThis(){
    console.log(this.edit );
    this.edit = !this.edit;
    console.log(this.edit );
  }
  backToTable(){
    console.log(this.back);
    this.back[1](this.back[0]);
  }

}
