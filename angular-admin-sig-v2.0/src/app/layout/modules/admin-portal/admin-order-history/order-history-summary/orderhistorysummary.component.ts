import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../../router.animations';
import { OrderhistoryService } from '../../../../../services/orderhistory/orderhistory.service';
import { DomSanitizer } from '@angular/platform-browser';
import { TopBarMenuItem } from '../../../../../object-interface/common.object';
import { analitycsData, generateChartData, calculateTheAverageValue } from './order-histories-object-setup';

@Component({
  selector: 'app-order-history-summary',
  templateUrl: './orderhistorysummary.component.html',
  styleUrls: ['./orderhistorysummary.component.scss'],
  animations: [routerTransition()]
})

export class OrderHistorySummaryComponent implements OnInit {
  data: any = [];
  service: OrderhistoryService
  errorLabel: string
  topBarMenu: TopBarMenuItem[] = [
    { label: "Number of Sales", routerLink: '/administrator/order-history-summary', active: true },
    { label: "Amount of Sales", routerLink: '/administrator/order-amount-history-summary' },
    { label: "Abondonment Rate", routerLink: '/administrator/cart-abandonment-summary' }
  ];
  topMembers: []
  avgData = {
    monthly: {
      all: 0,
      pending: 0,
      paid: 0,
      cancel: 0,
      waiting: 0,
      checkout: 0
    },
    daily: {
      all: 0,
      pending: 0,
      paid: 0,
      cancel: 0,
      waiting: 0,
      checkout: 0
    },
    yearly: {
      all: 0,
      pending: 0,
      paid: 0,
      cancel: 0,
      waiting: 0,
      checkout: 0
    },
    hourly: {
      all: 0,
      pending: 0,
      paid: 0,
      cancel: 0,
      waiting: 0,
      checkout: 0
    }
  }
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    // elements: {
    //     line: {
    //         tension: 0 // disables bezier curves
    //     }
    // }
  };
  public barChartLabels: any = [];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any = [
    { data: [], label: 'All Order' },
  ];

  public analitycsData: any = analitycsData;

  @Input() permissionType: any;

  constructor(public OrderhistoryService: OrderhistoryService, public sanitizer: DomSanitizer) {

  }


  ngOnInit() {
    

    if (this.permissionType) {
      if (this.permissionType == 'merchant') {
        this.topBarMenu = null;
      }
    }
    let currentDate = new Date();
    this.firstLoad();

  }


  ngAfterViewChecked() {
    // console.log('hero', this.permissionType)
  }

  onUpdateCart(data) {
    const clonedData = JSON.parse(JSON.stringify(data.barChartData));
    const clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = [];
    data.barChartLabels.forEach(() => {
      clone[0].data.push(Math.round(Math.random() * 100))
    });
    data.barChartData = clone;

    setTimeout(() => {
      data.barChartData = clonedData;
    }, 500)

  }

  genChartData(dataValues) {
    return generateChartData(dataValues)
  }

  graphicDataSetup(obj, valGroup){

  }

  async firstLoad() {

    try {
      this.service = this.OrderhistoryService;
      const result: any = await this.OrderhistoryService.getOrderHistorySummaryByDate('today');
      this.data = result.result;

      this.data.shopping_cart_abandonment_rate = this.data.current_order_history.total == 0 ? 0 :
        (1 - (this.data.current_order_history_paid.total / this.data.current_order_history.total)) * 100;

      if (this.data.order_history_by_month) {

        let valGroup = {
          all:      this.data.order_history_by_month.value,
          pending:  this.data.order_history_by_month_pending.value,
          paid:     this.data.order_history_by_month_paid.value,
          cancel:   this.data.order_history_by_month_cancel.value,
          waiting:  this.data.order_history_by_month_waiting.value,
          checkout: this.data.order_history_by_month_checkout.value
        };

        let convertedDataMonthlyObject:any = {}


        let clone:any 
        let dataClone = []

        for(let prop in valGroup){
          convertedDataMonthlyObject[prop] = this.genChartData(valGroup[prop])
          this.avgData.monthly[prop] = Math.floor(calculateTheAverageValue(valGroup[prop]))
          dataClone.push({ data: convertedDataMonthlyObject[prop][1], label: prop, fill: false})
        }
        
        clone = this.dataSetup(this.analitycsData.monthly.barChartData, dataClone);

        this.barChartLabels = convertedDataMonthlyObject.all[0]

        this.analitycsData.monthly.barChartLabels = convertedDataMonthlyObject.all[0]
        this.analitycsData.monthly.barChartData   = clone;

      }

      if (this.data.order_history_by_the_day) {

        const convertedDataDaily: any     = this.genChartData(this.data.order_history_by_the_day.value);
        const convertedDataDailyPending   = this.genChartData(this.data.order_history_by_the_day_pending.value);
        const convertedDataDailyPaid: any = this.genChartData(this.data.order_history_by_the_day_paid.value);
        const convertedDataDailyCancel    = this.genChartData(this.data.order_history_by_the_day_cancel.value);
        const convertedDataDailyWaiting   = this.genChartData(this.data.order_history_by_the_day_waiting.value);
        const convertedDataDailyCheckout  = this.genChartData(this.data.order_history_by_the_day_checkout.value);

        this.avgData.daily.all = Math.floor(calculateTheAverageValue(this.data.order_history_by_the_day.value));
        this.avgData.daily.pending = Math.floor(calculateTheAverageValue(this.data.order_history_by_the_day_pending.value));
        this.avgData.daily.paid = Math.floor(calculateTheAverageValue(this.data.order_history_by_the_day_paid.value));
        this.avgData.daily.cancel = Math.floor(calculateTheAverageValue(this.data.order_history_by_the_day_cancel.value));
        this.avgData.daily.waiting = Math.floor(calculateTheAverageValue(this.data.order_history_by_the_day_waiting.value));
        this.avgData.daily.checkout = Math.floor(calculateTheAverageValue(this.data.order_history_by_the_day_checkout.value));

        
        let clone = JSON.parse(JSON.stringify(this.analitycsData.daily.barChartData));

        clone[0].data = convertedDataDaily[1];
        
        let dataClone = [
          clone[0],
          { data: convertedDataDailyPending[1], label: "Pending", fill: false},
          { data: convertedDataDailyPaid[1], label: "Paid", fill: false},
          { data: convertedDataDailyCancel[1], label: "Cancel", fill: false},
          { data: convertedDataDailyWaiting[1], label: "Waiting", fill: false},
          { data: convertedDataDailyCheckout[1], label: "Checkout", fill: false},
        ];

        clone = this.dataSetup(clone, dataClone)
       
        this.analitycsData.daily.barChartLabels = convertedDataDaily[0]
        this.barChartLabels = convertedDataDaily[0]
        // this.barChartData   = clone;
        this.analitycsData.daily.barChartData = clone;
        // console.log("his.analitycsData", this.analitycsData.daily)

      }
      if (this.data.order_history_hourly) {

        const convertedDataHourly: any = this.genChartData(this.data.order_history_hourly.value);
        const convertedDataHourlyPending = this.genChartData(this.data.order_history_hourly_pending.value);
        const convertedDataHourlyPaid: any = this.genChartData(this.data.order_history_hourly_paid.value);
        const convertedDataHourlyCancel = this.genChartData(this.data.order_history_hourly_cancel.value);
        const convertedDataHourlyCheckout = this.genChartData(this.data.order_history_hourly_checkout.value);
        const convertedDataHourlyWaiting = this.genChartData(this.data.order_history_hourly_waiting.value);

        this.avgData.hourly.all = Math.floor(calculateTheAverageValue(this.data.order_history_hourly.value));
        this.avgData.hourly.pending = Math.floor(calculateTheAverageValue(this.data.order_history_hourly_pending.value));
        this.avgData.hourly.paid = Math.floor(calculateTheAverageValue(this.data.order_history_hourly_paid.value));
        this.avgData.hourly.cancel = Math.floor(calculateTheAverageValue(this.data.order_history_hourly_cancel.value));
        this.avgData.hourly.waiting = Math.floor(calculateTheAverageValue(this.data.order_history_hourly_waiting.value));
        this.avgData.hourly.checkout = Math.floor(calculateTheAverageValue(this.data.order_history_hourly_checkout.value));


        // const clone = JSON.parse(JSON.stringify(this.analitycsData.hourly.barChartData));
        let clone: any = this.analitycsData.hourly.barChartData;

        SETUP_DATA_FOR_HOURLY: {
          clone[0].data = convertedDataHourly[1];

          let dataClone = [
            clone[0],
            { data: convertedDataHourlyPending[1], label: "Pending", fill: false},
            { data: convertedDataHourlyPaid[1], label: "Paid", fill: false},
            { data: convertedDataHourlyCancel[1], label: "Cancel", fill: false},
            { data: convertedDataHourlyWaiting[1], label: "Waiting", fill: false},
            { data: convertedDataHourlyCheckout[1], label: "Checkout", fill: false},
          ];

          clone = this.dataSetup(clone, dataClone);
        }

        this.analitycsData.hourly.barChartLabels = convertedDataHourly[0]
        this.barChartLabels = convertedDataHourly[0]
        // this.barChartData   = clone;
        this.analitycsData.hourly.barChartData = clone;

      }

      if (this.data.order_history_yearly) {

        let convertedDataYearlyObject: any = {
          yearly: this.genChartData(this.data.order_history_yearly.value),
          pending: this.genChartData(this.data.order_history_yearly_pending.value),
          paid: this.genChartData(this.data.order_history_yearly_paid.value),
          cancel: this.genChartData(this.data.order_history_yearly_cancel.value),
          checkout: this.genChartData(this.data.order_history_yearly_checkout.value),
          waiting: this.genChartData(this.data.order_history_yearly_waiting.value),
        }
        const convertedDataYearly = convertedDataYearlyObject.yearly;
        const convertedDataYearlyPending = convertedDataYearlyObject.pending;
        const convertedDataYearlyPaid = convertedDataYearlyObject.paid;
        const convertedDataYearlyCancel = convertedDataYearlyObject.cancel;
        const convertedDataYearlyCheckout = convertedDataYearlyObject.checkout;
        const convertedDataYearlyWaiting = convertedDataYearlyObject.waiting;

        this.avgData.yearly.all = Math.floor(calculateTheAverageValue(this.data.order_history_yearly.value));
        this.avgData.yearly.pending = Math.floor(calculateTheAverageValue(this.data.order_history_yearly_pending.value));
        this.avgData.yearly.paid = Math.floor(calculateTheAverageValue(this.data.order_history_yearly_paid.value));
        this.avgData.yearly.cancel = Math.floor(calculateTheAverageValue(this.data.order_history_yearly_cancel.value));
        this.avgData.yearly.waiting = Math.floor(calculateTheAverageValue(this.data.order_history_yearly_waiting.value));
        this.avgData.yearly.checkout = Math.floor(calculateTheAverageValue(this.data.order_history_yearly_checkout.value));

        // const clone = JSON.parse(JSON.stringify(this.analitycsData.yearly.barChartData));
        let clone: any = this.analitycsData.yearly.barChartData;

        
        SETUP_DATA_FOR_HOURLY: {
          
          clone[0].data = convertedDataYearly[1];

          let dataClone = [
            clone[0],
            { data: convertedDataYearlyPending[1], label: "Pending", fill: false},
            { data: convertedDataYearlyPaid[1], label: "Paid", fill: false},
            { data: convertedDataYearlyCancel[1], label: "Cancel", fill: false},
            { data: convertedDataYearlyWaiting[1], label: "Waiting", fill: false},
            { data: convertedDataYearlyCheckout[1], label: "Checkout", fill: false},
          ];

          clone = this.dataSetup(clone, dataClone);
        }

        this.analitycsData.yearly.barChartLabels = convertedDataYearly[0]
        this.barChartLabels = convertedDataYearly[0]
        // this.barChartData   = clone;
        this.analitycsData.yearly.barChartData = clone;
        // console.log("his.analitycsData yearly", this.analitycsData.yearly)

        TOP_MEMBERS_SETUP: {
          this.topMembers = this.data.top_users.value
        }
      }
    } catch (e) {
      console.log("this e result", e)

      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  dataSetup(clone, dataClone){
    dataClone.forEach((e, i) =>{
      if(clone[i]){
        clone[i] = e;
      }
      else{
        clone.push(e);
      }
    })
    return clone;
  }


  public async getCRR(crr) {
    let result: any;
    this.service = this.OrderhistoryService;
    result = await this.OrderhistoryService.getCRR(crr);
    this.data = result.values;
  }




}
