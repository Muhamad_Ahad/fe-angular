import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MembershipComponent } from './membership.component';
import { MembershipAddComponent } from './add/membership.add.component';
import { MembershipDetailComponent } from './detail/membership.detail.component';

const routes: Routes = [
  {
      path: '', component: MembershipComponent,

  },
  {
      path:'add', component: MembershipAddComponent
  },
  {
    path:'detail', component: MembershipDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MembershipRoutingModule { }
