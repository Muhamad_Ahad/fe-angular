import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MembershipComponent } from './membership.component';
import { MembershipAddComponent } from './add/membership.add.component';
import { FormInputComponent }  from '../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from './../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { FormSelectComponent } from '../../../component-libs/form-select/form-select.component';
import { MembershipDetailComponent } from './detail/membership.detail.component';
import { MembershipRoutingModule } from './membership-routing.module';
import { MembershipEditComponent } from './edit/membership.edit.component';
import { BsComponentModule } from '../bs-component/bs-component.module';
// import { NgxEditorModule } from 'ngx-editor';


@NgModule({
  imports: [CommonModule,
    MembershipRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    // NgxEditorModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule
],
  
  declarations: [
    MembershipComponent, MembershipAddComponent, MembershipDetailComponent,MembershipEditComponent]
})
export class MembershipModule { }
