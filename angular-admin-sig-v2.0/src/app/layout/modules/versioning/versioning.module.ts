import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VersioningComponent } from './versioning.component';
import { VersioningRoutingModule } from './versioning.routing';
import { PageHeaderModule } from '../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { Ng2CompleterModule } from 'ng2-completer';
import { VersioningDetailComponent } from './detail/versioning.detail.component';
import { VersioningEditComponent } from './edit/versioning.edit.component';

@NgModule({
  imports: [
    CommonModule,
    VersioningRoutingModule,
    PageHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule, 
  ],
  declarations: [VersioningComponent, VersioningDetailComponent, VersioningEditComponent],
})
export class VersioningModule { }
