import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-orderhistoryallhistory-detail',
  templateUrl: './orderhistoryallhistory.detail.component.html',
  styleUrls: ['./orderhistoryallhistory.detail.component.scss'],
  animations: [routerTransition()]
})

export class OrderhistoryallhistoryDetailComponent implements OnInit {

  @Input() public detail: any;
  @Input() public back;
  
  orderhistoryDetail:any;
  service;
  errorLabel;

  constructor(
    private orderhistoryService: OrderhistoryService,
    private route: ActivatedRoute,
    private router:Router
    ) { }

  ngOnInit() {
    this.firstLoad();
  }

  async firstLoad(){
    console.log("detail sales order", this.detail);
    this.orderhistoryDetail = JSON.parse(JSON.stringify(this.detail));
   
    // this.route.queryParams.subscribe( async (params) =>{

    //   let historyID    = params.id;
    //   this.loadHistoryDetail(historyID)

     
    // })
    // console.log("firstload", this.firstLoad)

    
  }

  // private async loadHistoryDetail(historyID:string){
  //   try{
  //     this.service    = this.orderhistoryService;
  //     let result:any  = await this.orderhistoryService.detailOrderhistory(historyID);
  //     // console.log("result", result.result);
  //     this.orderhistoryDetail = result.result;
  //     console.log("detail barang", this.orderhistoryDetail)
      
  //     // this.orderhistoryDetail.merchant = true;
  //   }
  //   catch(e){
  //     this.errorLabel = ((<Error>e).message);//conversion to Error type
  //   }
  // }

}
