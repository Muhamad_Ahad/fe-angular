import { OrderhistoryService } from './../../../../services/orderhistory/orderhistory.service';
import { Component, OnInit, Input } from '@angular/core';
import { NgxBarcodeModule } from 'ngx-barcode';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-receipt',
  templateUrl: './receipt.component.html',
  styleUrls: ['./receipt.component.scss']
})
export class ReceiptComponent implements OnInit {


  order_id;
  detail;
  service;
  errorLabel;
  rupiah: any;
  value: any;



  // dataList = order_id;


  constructor(private OrderhistoryService: OrderhistoryService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.firstLoad()
    // this.order_id();
    // this.hasil();
  }



  get values(): string[] {
    return this.value.split('\n');
  }

  async firstLoad() {

    this.route.queryParams.subscribe(async (params) => {
      let historyID = params.id;
      this.loadDetail(historyID)
    })

  }

  private async loadDetail(historyID: string) {
    try {
      this.service = this.OrderhistoryService
      const result: any = await this.OrderhistoryService.detailOrderhistory(historyID);
      console.log("HASIL", result)
      this.detail = result.result;
      this.rupiah = this.detail.billings.sum_total;
      console.log(this.rupiah)
      this.rupiah = await this.formatRupiah(this.rupiah, 'Rp ');
    }
    catch (e) {
      this.errorLabel = ((<Error>e).message);
    }
  }

  public async printPage() {
    window.print();
  }

  /* Fungsi formatRupiah */
  private async formatRupiah(angka, prefix) {
    var number_string = angka.toString().replace(/[^,\d]/g, ''),
      split = number_string.split(','),
      sisa = split[0].length % 3,
      rupiah = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      let separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp ' + rupiah : '');
  }


}
