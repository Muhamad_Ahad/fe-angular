import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';
// import { NumberFormatStyle } from '@angular/common';
import { FormOptions, TableFormat } from '../../../object-interface/common.object';
import Swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { ProgramNameService } from '../../../services/program-name.service';
// import { Router } from '@angular/router';
import * as content from '../../../../assets/json/content.json';

@Component({
  selector: 'app-orderhistoryallhistory',
  templateUrl: './orderhistoryallhistory.component.html',
  styleUrls: ['./orderhistoryallhistory.component.scss'],
  animations: [routerTransition()]
})

export class OrderhistoryallhistoryComponent implements OnInit {
  @Input() public back;

  public contentList : any = (content as any).default;

  Orderhistoryallhistory: any = [];

  tableFormat: TableFormat = {

    title: 'Order All History Detail Page',

    label_headers: [
      { label: 'Request Date', visible: true, type: 'date', data_row_name: 'request_date' },
      { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
      { label: 'ID Toko', visible: true, type: 'string', data_row_name: 'username' },
      { label: 'Nama Toko', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail' },
      { label: 'Nama Pemilik', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail' },
      { label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_quantity' },
      { label: 'Total Price', visible: true, type: 'string', data_row_name: 'sum_total' },
      { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
    ],

    row_primary_key: 'order_id',
    formOptions: {
      row_id: 'order_id',
      this: this,
      result_var_name: 'Orderhistoryallhistory',
      detail_function: [this, 'callDetail'],
    },
    show_checkbox_options: true
  };

  tableFormat2: TableFormat = {

    title: 'Order All History Detail Page',

    label_headers: [
      { label: 'Request Date', visible: true, type: 'date', data_row_name: 'request_date' },
      { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
      { label: 'Nomor PO', visible: true, type: 'value_po', data_row_name: 'po_no'},
      { label: 'ID Bisnis', visible: true, type: 'form_business_id', data_row_name: 'member_detail' },
      { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'username' },
      { label: 'Nama Pelanggan', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail' },
      { label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_quantity' },
      { label: 'Total Price', visible: true, type: 'string', data_row_name: 'sum_total' },
      { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
    ],

    row_primary_key: 'order_id',
    formOptions: {
      row_id: 'order_id',
      this: this,
      result_var_name: 'Orderhistoryallhistory',
      detail_function: [this, 'callDetail'],
    },
    show_checkbox_options: true
  };

  tableFormat3: TableFormat = {

    title: 'Order All History Detail Page',

    label_headers: [
      { label: 'Request Date', visible: true, type: 'date', data_row_name: 'request_date' },
      { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
      { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'username' },
      { label: 'Nama Program', visible: true, type: 'form_cluster', data_row_name: 'member_detail' },
      { label: 'Nama Entitas', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail' },
      { label: 'Nama PIC', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail' },
      { label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_quantity' },
      { label: 'Total Price', visible: true, type: 'string', data_row_name: 'sum_total' },
      { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
    ],

    row_primary_key: 'order_id',
    formOptions: {
      row_id: 'order_id',
      this: this,
      result_var_name: 'Orderhistoryallhistory',
      detail_function: [this, 'callDetail'],
    },
    show_checkbox_options: true
  };

  form_input: any = {};
  errorLabel: any = false;
  totalPage: 0;

  orderhistoryallhistoryDetail: any = false;

  service: any;

  mci_project: any = false;

  errorMessage: any = false;

  programType: any = "";

  constructor(
    public orderhistoryService: OrderhistoryService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private programNameService: ProgramNameService
    ) { }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    try {
      let paramId = this.activatedRoute.snapshot.queryParamMap.get('order_id');
      let paramAppLabel = this.activatedRoute.snapshot.queryParamMap.get('app_label');

      if(paramId && paramAppLabel) {
        // localStorage.setItem('programName', paramAppLabel);
        // this.programNameService.programName$.next(paramAppLabel);
        this.programNameService.setData(paramAppLabel);
        
        const resultDetail: any = await this.orderhistoryService.getOrderDetail(paramId);

        if(resultDetail && resultDetail.order_id) this.orderhistoryallhistoryDetail = resultDetail;
      }

      const program = localStorage.getItem('programName');
      let _this = this;
      this.contentList.filter(function(element) {
          if(element.appLabel == program) {
              if(element.type == "reguler") {
                  _this.mci_project = true;
                  _this.programType = "reguler";
              } else if(element.type == "custom_kontraktual") {
                _this.mci_project = false;
                _this.programType = "custom_kontraktual";
              } else {
                  _this.mci_project = false;
                  _this.programType = "custom";
              }
          }
      });

      this.service = this.orderhistoryService;
      const result: any = await this.orderhistoryService.getAllreportSalesOrder();
      this.totalPage = result.total_page;
      
      this.Orderhistoryallhistory = result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message); // conversion to Error type

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
				this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
			}
    }
  }

  public async callDetail(data, rowData) {
    // this.orderhistoryallhistoryDetail = this.Orderhistoryallhistory.find(order => order.order_id == rowData.order_id);
    this.orderhistoryallhistoryDetail = await this.orderhistoryService.getOrderDetail(rowData.order_id);
  }

  public async backToHere(obj) {
    obj.orderhistoryallhistoryDetail = false;
    obj.router.navigate([]).then(() => {
      obj.firstLoad();
    });
  }

  public async processSelectedOrders(obj, orders){
    if(orders.length > 0){
      Swal.fire({
        title: 'Confirmation',
        text: 'Apakah anda yakin ingin memproses semua order yang dipilih',
        icon: 'success',
        confirmButtonText: 'process',
        cancelButtonText:"cancel",
        showCancelButton:true
        }).then(async (result) => {
        if(result.isConfirmed){
          await orders.forEach(async(order_id) => {
            const payload = {
              order_id
            }
            try {
              await obj.orderhistoryService.processOrder(payload);
            } catch (e) {
            }
            Swal.fire('Success', 'Semua order terpilih telah disetujui', 'success');
          });

        }
      });
    }
  }

}
