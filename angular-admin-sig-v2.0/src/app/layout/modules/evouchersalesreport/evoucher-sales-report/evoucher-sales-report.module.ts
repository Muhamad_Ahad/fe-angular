import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EvoucherSalesReportRoutingModule } from './evoucher-sales-report-routing.module';
import { EvoucherSalesReportComponent } from './evoucher-sales-report.component';
import { FormBuilderTableModule } from '../../../../component-libs/form-builder-table/form-builder-table.module';
import { MerchantEvoucherSalesReportComponent } from '../../../merchant-portal/merchant-evoucher-sales-report/merchant-evoucher-sales-report.component';


@NgModule({
  imports: [
    CommonModule,
    EvoucherSalesReportRoutingModule,
    FormBuilderTableModule
  ],
  declarations: [
    EvoucherSalesReportComponent, 
    MerchantEvoucherSalesReportComponent
  ],
  exports:[
    EvoucherSalesReportComponent, 
    MerchantEvoucherSalesReportComponent
  ]
})
export class EvoucherSalesReportModule { }
