import { EscrowTransactionRoutingModule } from './escrow-transaction-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {  NgbPagination } from '@ng-bootstrap/ng-bootstrap';
import { PageHeaderModule } from './../../../shared';
import { FormBuilderTableComponent } from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { EscrowTransactionComponent } from './escrow-transaction.component';

import { DetailComponent } from './detail/detail.component';

@NgModule({
  imports: [
    CommonModule,
    EscrowTransactionRoutingModule,
    PageHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule
  ],
  bootstrap:[
    NgbPagination
  ],
  declarations: [EscrowTransactionComponent, DetailComponent]
})
export class EscrowTransactionModule { }
