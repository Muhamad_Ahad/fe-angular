import { Component, OnInit, Input } from '@angular/core';
import { EscrowTransactionService } from '../../../../services/escrow-transaction/escrow-transaction.service';
import { Router, ActivatedRoute } from '@angular/router';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { error } from 'protractor';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  @Input() public detail: any;
  
  escrowdata: any;
  public adds: any[] = [{
      value: 0,
      description: ' ',
      type:''

  }];
  additionalCharges = ["1"];
  additionalCargesNumber = 1;
  data: any = {};
  errorLabel;
  additional = false;
  change = false;
  prev_balance;
  textClicked = false;
  how_much;
  loading = false;
  current_balance;
  remark: "";
  description: ""
  type: "";
  saveAdd = false;
  rejectNote = "";
  tempstatus = "";
  isClicked2 = false;
  isClicked = false;
  withdrawal_status: "";
  formEditable: any = {};
  popUpForm: boolean;
  errorMessage: string;
  constructor(
    public EscrowTransactionService: EscrowTransactionService,
    private route: ActivatedRoute,
    private router: Router,
    public orderhistoryService: OrderhistoryService,
  ) { }
  ngOnInit() {
    this.firstLoad();
  }

  async firstLoad() {
    this.route.queryParams.subscribe(async (params) => {
      let escrow_id = params.id;
      console.log("ID", escrow_id)
      this.loadEscrowDetail(escrow_id);
    })
    console.log("ini", this.data)

  }
  changestatus() {
    this.change = true;
  }
  valueCheck() {
    this.rejectNote == "" ? this.textClicked = false : this.textClicked = true;
  }
  onpaidclick() {
    //   this.orderHistoryDetail.status = "PAID";
    this.tempstatus = "RELEASE";
    this.isClicked = true;
    this.isClicked2 = false;
    this.popUpForm = true;

  }
  onAddClick (){
    this.isClicked2 = this.isClicked = false;
    this.popUpForm =  this.saveAdd = this.isClicked = true;
    
  }
  onDoneNew = () => {

    this.popUpForm = false;
    this.firstLoad();
    //console.log("adsa")
  }
  oncancelclick() {
    // this.orderHistoryDetail.status = "CANCEL";
    this.tempstatus = "REJECT";
    this.isClicked2 = true;
    this.isClicked = false;
    this.popUpForm = true;

  }
  gotoMerchant(merchant_username){
    this.router.navigate(['administrator/merchant/detail'], {queryParams: {merchant_username: merchant_username}})
  }
  async onConfirmReject() {
    this.loading = true;
    this.data.withdrawal_status = "REJECTED";
    let form = this.data._id;
    let form2 = {
      description : this.rejectNote,
      remark: "Withdrawal Rejected"
    }
    console.log("release dipencet", form)
    console.log("ini form2", form2)
    try {
   
      let result = await this.EscrowTransactionService.escrowWithdrawalRejectRequest(form, form2)
      if (!result.error) {
        Swal.fire("Reject", "Order has been Rejected", "success")
        this.onDoneNew();
        this.router.navigate(['administrator/escrow-transaction'])
      }
      this.loading = false
    }
    catch (e) {
      this.errorMessage = (<Error>e).message

    }
  }
  async onConfirmHoldRefund() {
    this.data.withdrawal_status = "REFUND";
    let form = this.data.reference_info;
    let form2 = {
      description : this.rejectNote,
      remark: "Hold transaction Refunded"
    }
    
    try {
      let result = await this.orderhistoryService.holdMerchantRefund(form, form2)
      if (!result.error) {
        Swal.fire("Refund", "Order has been Refunded", "success")
        this.onDoneNew();
        this.router.navigate(['administrator/escrow-transaction'])
      }
    }
    catch (e) {
      this.errorMessage = (<Error>e).message

    }
  }
  async onConfirmHoldRelease(){
    this.loading = true;
    this.data.transaction_status = "RELEASE";
    let form = this.data.reference_info;
    let form2 = {
      description: "Confirm Hold Release Process",
      remark: "Release Hold Confirmed"
    }
   
    try {
      let result = await this.orderhistoryService.holdMerchantRelease(form, form2)
      if (!result.error) {
        Swal.fire("Release", "Order Hold has been Released", "success")
        this.onDoneNew();
        this.router.navigate(['administrator/escrow-transaction'])
      }
      this.loading = false;
    }
    catch (e) {
      this.errorMessage = (<Error>e).message
    }
  }

  async onConfirmAdd() {
    console.log('here');
    let values= {
      values :this.adds
    } 
    try{
      let result = await this.EscrowTransactionService.additionalTransaction(this.data._id, values);
      if(!result.error){
        this.popUpForm =  this.saveAdd = this.isClicked = false;
        console.log('ini arraynya broo',this.adds)
        this.adds=[];
        this.adds.push({
          value: undefined,
          description: '',
          type:''
        })
        this.additional = false;
        this.firstLoad();
      }else{
        this.errorLabel = result.error;
        this.popUpForm =  this.saveAdd = this.isClicked = false;
      }
    }catch(e){
      this.errorMessage = (<Error>e).message
    }


  }

  async onConfirmRelease() {
    this.loading = true;
    this.data.withdrawal_status = "RELEASE";
    let form = this.data._id;
    let form2 = {
      description: "Confirm Release Process",
      remark: "Release Withdrawal Confirmed"
    }
    try {
      let result = await this.EscrowTransactionService.escrowWithdrawalReleaseRequest(form, form2)
      if (!result.error) {
        Swal.fire("Release", "Order has been Released", "success")
        this.onDoneNew();
        this.router.navigate(['administrator/escrow-transaction'])
      }
      this.loading = false;
    }
    catch (e) {
      this.errorMessage = (<Error>e).message
    }
 
  
  }
  private async loadEscrowDetail(escrowID: string) {
    try {

      let result = await this.EscrowTransactionService.detailEscrowtransaction(escrowID);
      // console.log("result", result.result);
      this.data = result.result;
      // this.data.how_much = (this.data.how_much); 
      console.log("detail escrow", this.data)

      // this.orderhistoryDetail.merchant = true;
    }
    catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }
  
  public prints(){
    window.print()
  }
  stringToNumber(str: any) {
    if (typeof str == 'number') {
      str = str + '';
    }

    let s = str.toLowerCase().replace(/[^0-9]/g, '');

    return parseInt(s);
  }
  addInputCharges() {
    this.additionalCargesNumber++;
    this.adds.push({
      value: undefined,
      description: '',
      type:''
    })
    
    
  }

}
