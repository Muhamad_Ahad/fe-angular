import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EscrowTransactionComponent } from './escrow-transaction.component';
import { DetailComponent } from  './detail/detail.component'

const routes: Routes = [
  {
      path: '', component: EscrowTransactionComponent
  },
  {
    path: 'detail', component: DetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EscrowTransactionRoutingModule { }
