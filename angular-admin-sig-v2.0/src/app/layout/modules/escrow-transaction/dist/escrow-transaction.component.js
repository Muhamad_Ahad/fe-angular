"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.EscrowTransactionComponent = void 0;
var core_1 = require("@angular/core");
var EscrowTransactionComponent = /** @class */ (function () {
    function EscrowTransactionComponent(EscrowTransactionService, router) {
        this.EscrowTransactionService = EscrowTransactionService;
        this.router = router;
        this.EscrowData = [];
        this.tableFormat = {
            title: 'Escrow Transactions',
            label_headers: [
                { label: 'Owner ID', visible: true, type: 'string', data_row_name: 'owner_id' },
                { label: 'Nominal', visible: true, type: 'number', data_row_name: 'how_much' },
                { label: 'Description', visible: true, type: 'string', data_row_name: 'description' },
                { label: 'Created Date', visible: true, type: 'date', data_row_name: 'created_date' },
                { label: 'No. Ref', visible: true, type: 'string', data_row_name: 'record_id' },
                { label: 'Debit', visible: true, type: 'escrow-d', data_row_name: 'how_much' },
                { label: 'Credit', visible: true, type: 'escrow-c', data_row_name: 'how_much' },
                { label: 'Type', visible: true, options: ['withdrawal', 'plus', 'minus', 'hold'], type: 'list-escrow', data_row_name: 'type' },
                { label: 'status', visible: true, options: ['PENDING', 'RELEASED', 'HOLD', 'SUCCEESS'], type: 'list-escrow-status', data_row_name: 'transaction_status' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                "this": this,
                result_var_name: 'EscrowData',
                detail_function: [this, 'callDetail']
            }
        };
        this.form_input = {};
        this.errorLabel = false;
        this.totalPage = 0;
        this.page = 1;
        this.pageSize = 50;
        this.pointstransactionDetail = false;
        this.currentPage = 1;
    }
    EscrowTransactionComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    EscrowTransactionComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.EscrowTransactionService;
                        return [4 /*yield*/, this.EscrowTransactionService.getEscrowtransactionLint()];
                    case 1:
                        result = _a.sent();
                        console.log("result", result);
                        this.totalPage = result.result.total_page;
                        this.pages = result.result.total_page;
                        console.log(" pages", this.pages);
                        this.EscrowData = result.result.values;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    EscrowTransactionComponent.prototype.callDetail = function (pointstransaction_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        result = void 0;
                        this.service = this.EscrowTransactionService;
                        return [4 /*yield*/, this.EscrowTransactionService.detailEscrowtransaction(pointstransaction_id)];
                    case 1:
                        result = _a.sent();
                        console.log(result);
                        this.pointstransactionDetail = result.result[0];
                        console.log('ini', this.pointstransactionDetail);
                        this.router.navigate(['administrator/escrow-transaction/detail'], { queryParams: { id: pointstransaction_id } });
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    EscrowTransactionComponent.prototype.onChangePage = function (pageNumber) {
        if (pageNumber <= 0) {
            pageNumber = 1;
        }
        if (pageNumber >= this.total_page) {
            pageNumber = this.total_page;
        }
        this.currentPage = pageNumber;
        console.log(pageNumber, this.currentPage, this.total_page);
        this.valuechange({}, false, false);
    };
    EscrowTransactionComponent.prototype.pageLimitChanges = function (event) {
        console.log('Test limiter', this.pageLimits);
        this.valuechange({}, false, false);
    };
    EscrowTransactionComponent.prototype.pageLimiter = function () {
        return [
            { id: '10', name: '10' },
            { id: '20', name: '20' },
            { id: '50', name: '50' },
            { id: '100', name: '100' }
        ];
    };
    EscrowTransactionComponent.prototype.isCurrentPage = function (tp) {
        if (tp == this.currentPage) {
            return 'current-page';
        }
        else
            return '';
    };
    EscrowTransactionComponent.prototype.pagination = function (c, m) {
        var current = c, last = m, delta = 2, left = current - delta, right = current + delta + 1, range = [], rangeWithDots = [], l;
        for (var i = 1; i <= last; i++) {
            if (i == 1 || i == last || i >= left && i < right) {
                range.push(i);
            }
        }
        for (var _i = 0, range_1 = range; _i < range_1.length; _i++) {
            var i = range_1[_i];
            if (l) {
                if (i - l === 2) {
                    rangeWithDots.push(l + 1);
                }
                else if (i - l !== 1) {
                    rangeWithDots.push('...');
                }
            }
            rangeWithDots.push(i);
            l = i;
        }
        return rangeWithDots;
    };
    EscrowTransactionComponent.prototype.buildPagesNumbers = function (totalPage) {
        this.pageNumbering = [];
        var maxNumber = 8;
        this.total_page = totalPage;
        this.pageNumbering = this.pagination(this.currentPage, totalPage);
        console.log(" numbering page ", this.pageNumbering);
    };
    EscrowTransactionComponent.prototype.orderChange = function (event, orderBy) {
        if (this.orderBy[orderBy]) {
            this.orderBy[orderBy].asc = !this.orderBy[orderBy].asc;
        }
        else {
            this.orderBy = [];
            this.orderBy[orderBy] = { asc: false };
        }
        this.valuechange(event, orderBy);
    };
    EscrowTransactionComponent.prototype.valuechange = function (event, input_name, download) {
        return __awaiter(this, void 0, void 0, function () {
            var searchCallback, filter, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        searchCallback = this.searchCallback;
                        console.log(searchCallback);
                        filter = {
                            search: '',
                            order_by: {},
                            limit_per_page: this.pageSize,
                            current_page: this.currentPage,
                            download: false
                        };
                        return [4 /*yield*/, this.EscrowTransactionService.searchEscrowtransactionLint(filter)];
                    case 1:
                        result = _a.sent();
                        this.EscrowData = result.result.values;
                        return [2 /*return*/];
                }
            });
        });
    };
    EscrowTransactionComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.pointstransactionDetail = false;
                return [2 /*return*/];
            });
        });
    };
    EscrowTransactionComponent = __decorate([
        core_1.Component({
            selector: 'app-escrow-transaction',
            templateUrl: './escrow-transaction.component.html',
            styleUrls: ['./escrow-transaction.component.scss']
        })
    ], EscrowTransactionComponent);
    return EscrowTransactionComponent;
}());
exports.EscrowTransactionComponent = EscrowTransactionComponent;
