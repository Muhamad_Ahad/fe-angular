import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { EscrowTransactionService } from '../../../services/escrow-transaction/escrow-transaction.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';
import { Router } from '@angular/router';
@Component({
  selector: 'app-admin-escrow-report',
  templateUrl: './admin-escrow-report.component.html',
  styleUrls: ['./admin-escrow-report.component.scss']
})
export class AdminEscrowReportComponent implements OnInit {
  EscrowData: any = [];
  Pointstransaction: any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Escrow Report Page',
                                  label_headers   : [
                                    {label: 'Tgl. Order', visible: true, type: 'date', data_row_name: 'order_date'},
                                    {label: 'Order ID', visible: true, type: 'string', data_row_name: 'reference_info'},
                                    {label: 'Merchant ID', visible: true, type: 'string', data_row_name: 'merchant_username'},
                                    {label: 'Record ID', visible: true, type: 'string', data_row_name: 'owner_id'},
                                    {label: 'Merchant Name', visible: true, type: 'string', data_row_name: 'merchant_name'},
                                    {label: 'Amount Order', visible: true, type: 'string', data_row_name: 'order_value'},
                                    {label: 'MDR', visible: true, type: 'string', data_row_name: 'mdr_value'},
                                    {label: 'Order After MDR', visible: true, type: 'string', data_row_name: 'total_amount'},
                                    {label: 'Withdraw Amount', visible: true, type: 'string', data_row_name: 'withdraw_amount'},
                                    
                                    {label: 'Balance Escrow', visible: true, type: 'string', data_row_name: 'current_balance'},
                                    {label: 'status', visible: true, options: ['PENDING', 'RELEASED','HOLD','SUCCEESS'], type: 'list-escrow-status', data_row_name: 'transaction_status'},
                                    
                                    {label: 'Request Date', visible: true, type: 'date', data_row_name: 'request_date'},
                                    {label: 'Approval Date', visible: true, type: 'date', data_row_name: 'approved_date'},
                                    
                                    // {label: 'Debit', visible: true, type: 'escrow-d', data_row_name: 'how_much'},
                                    // {label: 'Credit', visible: true, type: 'escrow-c', data_row_name: 'how_much'},
                                    // {label: 'Type', visible: true, options: ['withdrawal', 'plus', 'minus','hold'], type: 'list-escrow', data_row_name: 'type'},
                                   
                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'EscrowData',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  form_input    : any = {};
  errorLabel : any = false;
  totalPage  = 0;
  page = 1;
  pageSize = 50;
  pointstransactionDetail: any = false;
  service: any;
  pages: any;
  currentPage= 1;
  pageNumbering: any[];
  total_page: any;
  orderBy: any;
  searchCallback: any;
  onSearchActive: boolean;
  showLoading: boolean;
  options: any;
  table_data: any;
  pageLimits: any;

  constructor(public EscrowTransactionService:EscrowTransactionService, private router: Router) { }

  ngOnInit() {
    this.firstLoad();

  }
  
  async firstLoad(){
    // console.log(' admin payment')
    try{
      this.service    = this.EscrowTransactionService;
      let result: any = await this.EscrowTransactionService.getEscrowReporttransactionLint();
      console.log("result", result)
      this.totalPage  = result.result.total_page
      this.pages = result.result.total_page
      console.log(" pages",this.pages)
      this.EscrowData = result.result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }
  
  public async callDetail(pointstransaction_id){
    // try{
    //   let result: any;
    //   this.service    = this.EscrowTransactionService;
    //   result          = await this.EscrowTransactionService.detailEscrowtransaction(pointstransaction_id);
    //   console.log(result);

    //   this.pointstransactionDetail = result.result[0];
    //   console.log('ini',this.pointstransactionDetail)
      
    //   this.router.navigate(['administrator/escrow-transaction/detail'],  {queryParams: {id: pointstransaction_id }})
      
    // } catch (e) {
    //   this.errorLabel = ((<Error>e).message);//conversion to Error type
    // }
  }

  public async backToHere(obj){
    obj.pointstransactionDetail = false;
  }
  addMonths(date, months) {
    date.setMonth(date.getMonth() + months);
    return date;
  }
}

