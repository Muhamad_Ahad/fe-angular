import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminEscrowReportComponent } from './admin-escrow-report.component';

describe('AdminEscrowReportComponent', () => {
  let component: AdminEscrowReportComponent;
  let fixture: ComponentFixture<AdminEscrowReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminEscrowReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEscrowReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
