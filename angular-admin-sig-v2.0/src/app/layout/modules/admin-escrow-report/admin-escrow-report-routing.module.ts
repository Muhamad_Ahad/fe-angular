import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminEscrowReportComponent} from './admin-escrow-report.component'


const routes: Routes = [
  {
    path: '', component: AdminEscrowReportComponent,

}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminEscrowReportRoutingModule { }
