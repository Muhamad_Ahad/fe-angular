import { Component, OnInit, Input }  from '@angular/core';
import { routerTransition }   from '../../../router.animations';
import { MerchantService } from '../../../services/merchant/merchant.service';
import { FormOptions, TableFormat} from '../../../object-interface/common.object';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector    : 'app-merchant',
  templateUrl : './merchant.component.html',
  styleUrls   : ['./merchant.component.scss'],
  animations  : [routerTransition()]
})

export class MerchantComponent implements OnInit {
  @Input() public detail: any;

  // this section belongs to product item list
  Products      : any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Merchant Detail',
                                  label_headers   : [
                                    {label: 'Merchant Code', visible: true, type: 'string', data_row_name: 'merchant_name'},
                                    {label: 'Merchant Username', visible: true, type: 'string', data_row_name: 'merchant_username'},
                                    {label: 'Contact Person', visible: true, type: 'string', data_row_name: 'full_name'},
                                    {label: 'Office Phone', visible: true, type: 'string', data_row_name: 'cell_phone'},
                                    {label: 'HandPhone', visible: true, type: 'string', data_row_name: 'cell_phone'},
                                    {label: 'Fax', visible: true, type: 'string', data_row_name: 'cell_phone'},
                                    {label: 'Email', visible: true, type: 'string', data_row_name: 'email'},
                                    {label: 'Status', visible: true, type: 'string', data_row_name: 'status'},
                                    
                                  ],
                                  row_primary_key : 'merchant_username',
                                  formOptions     : {
                                                    row_id: 'merchant_username',
                                                    addForm: true,
                                                    this  : this,
                                                    result_var_name: 'Products',
                                                    detail_function: [this, 'callDetail'],
                                                    // customButtons:[
                                                    //   // {label: 'Approve', func:(f)=>{this.approval(f)}},
                                                    // ]
                                                  },
                                  // show_checkbox_options: true
  };
  form_input    : any = {};
  errorLabel : any = false;
  totalPage : 0;
  merchant_username:any;
  prodDetail              : any = false;
  service                 : any;
  selectedFile            : File    = null;
  progressBar             :number   = 0;
  cancel                  :boolean  = false;
  errorFile               : any     = false;
  mercDetail: any;

  constructor(public merchantService: MerchantService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.firstLoad();
  }

  
  async firstLoad(){
    try{
      let params = {
        'type':'merchant'
      }
      this.service    = this.merchantService;
      let result          = await this.merchantService.getMerchant(params);
      this.totalPage = result.result.total_page;
      console.log("test",result)
      this.Products   = result.result.values;
      console.log("lists", this.Products)
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async callDetail(merchant_username){
    console.log("hasil", merchant_username  )
    this.router.navigate(['administrator/merchant/detail'], {queryParams: {merchant_username: merchant_username}})
    
  //   try{

  //     this.route.queryParams.subscribe(async (params) => {

  //     })
  //     let result: any;
  //     this.service    = this.merchantService;
  //     result          = await this.merchantService.detailMerchant(merchant_username);
  //     console.log("result", result)
  //     if(!result.error && result.error == false){
  //       // this.prodDetail = result.result[0];
  //       // this.prodDetail = true;
  //       // console.log("testing 2", result)
  //     }
  //     // console.log("testing", this.prodDetail)
  // } catch (e) {
  //     this.errorLabel = ((<Error>e).message);//conversion to Error type
  //   }
  }

  public async callOutlet(outlet_id) {
    try {
      let result: any;
      this.service = this.merchantService;
      result = await this.merchantService.getOutlet(outlet_id);
      if (!result.error && result.error == false) {
        this.prodDetail = result.result[0];
      }
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj){
    obj.prodDetail = false;
    // delete obj.prodDetail;
    
  }

  onFileSelected(event){
    this.errorFile = false;
    let fileMaxSize = 3000000;// let say 3Mb
    Array.from(event.target.files).forEach((file: any) => {
        if(file.size > fileMaxSize){
          this.errorFile="maximum file is 3Mb";
        }
    });
    this.selectedFile = event.target.files[0];

  } 

  updateProgressBar(value){
    this.progressBar = value;
  }

  cancelThis(){
    this.cancel = !this.cancel;
  }

  // async onUpload(){
  //   try{
  //     this.cancel = false;
  //     if(this.selectedFile){
  //       await this.merchantService.upload(this.selectedFile,this,'product');
  //       }
  //   } catch (e) {
  //     this.errorLabel = ((<Error>e).message);//conversion to Error type
  //   }
  // }
}
