import { MerchantService } from '../../../../services/merchant/merchant.service';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';
import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { del } from 'selenium-webdriver/http';
import { SwapStorageService } from '../../../../services/swap-storage/swap-storage-service';
import { Router, ActivatedRoute } from '@angular/router';
import { DashboardComponent } from '../../../dashboard/dashboard.component';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-merchant-detail',
  templateUrl: './merchant.detail.component.html',
  styleUrls: ['./merchant.detail.component.scss'],
  animations: [routerTransition()]
})

export class MerchantDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;
  merchantUsername
  edit: boolean = false;
  editMdr:boolean= false;
  errorLabel: any = false;
  note = "";
  mdrType = [
    { label: 'Percentage', value: 'percentage' },
    { label: 'Value', value: 'value' }
  ]
  formMDR= 
    {
    merchant_username:'',mdr_type:'percentage',mdr_value:0
    }
  booking_id = "";
  constructor(public merchantService: MerchantService,
    private swapMerchantService: SwapStorageService,
    private router: Router,
    private route: ActivatedRoute,
    public orderhistoryService: OrderhistoryService,
    ) { }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    console.log("ini detail",this.detail);
    this.route.queryParams.subscribe(params => {
      this.merchantUsername = params.merchant_username;
      console.log("params ",params)
      this.loadMerchantDetail(this.merchantUsername)
    })
  }

  async loadMerchantDetail(username){
    // i add this param variable to match requiremnt of parameter from backend to get detail of merchant
    let param = '?merchant_username='+username
     let result = await this.merchantService.detailMerchant(param)
     console.log(" ini hasil ", result)

     this.detail = result.result;
     this.formMDR.mdr_type = this.detail.merchant_mdr.mdr_type;
     this.formMDR.mdr_value = this.detail.merchant_mdr.mdr_value;
     console.log(' mdr type', this.formMDR.mdr_type)
  }
  // merchant(){
    
  // }

  async editThis() {
    this.edit = !this.edit;
  }
  stringToNumber(str: any) {
    if (typeof str == 'number') {
      str = str + '';
    }

    let s = str.toLowerCase().replace(/[^0-9]/g, '');

    return parseInt(s);
  }
  editMDR(){
    this.editMdr = true;
  }
  closeMDR(){
    this.editMdr = false;
  }
  async saveMDR(){
    try {
      let param= {
        'merchant_username':this.detail.merchant_username,
        'mdr_type':this.formMDR.mdr_type,
        'mdr_value':this.stringToNumber(this.formMDR.mdr_value) 
      }
      let result = await this.merchantService.setMDR(param)
      if(result) {
        Swal.fire("Success", "MDR has been set", "success")
         this.detail.merchant_mdr.mdr_type = this.formMDR.mdr_type 
        this.detail.merchant_mdr.mdr_value =this.formMDR.mdr_value 
      }
     
    }catch(e){
      console.log(e);
      Swal.fire("Error", "MDR set error", "error")
    }
    this.closeMDR()
  }

  backToTable() {
    this.back[1](this.back[0]);
  }

  async deleteThis() {

    const deleteMerchant = await this.merchantService.deleteMerchant(this.detail);
    if (deleteMerchant.result.status =='success') {
      alert("Merchant Berhasil Terhapus")
    }
  }
  toEscrow(){
    this.router.navigate(['administrator/merchantescrowreport'], {queryParams: {merchant_username: this.merchantUsername}})
  }
  toSalesOrder(){
    this.router.navigate(['administrator/merchantsalesorder'], {queryParams: {merchant_username: this.merchantUsername}})
  }

  async hold() {
    let form = {
      value : 10000,
      description : this.note,
      remark : "hold transaction process"
    }
    const holdMerchant = await this.orderhistoryService.holdMerchant(this.booking_id,form);
  }

  swapToMerchant(){
    this.swapMerchantService.set(this.detail.merchant_username);
    this.router.navigate(['/merchant-portal/'])
  }

}
