
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MerchantComponent } from './merchant.component';
import { MerchantAddComponent } from './add/merchant.add.component';
import { MerchantDetailComponent } from './detail/merchant.detail.component';

const routes: Routes = [
  {
      path: '', component: MerchantComponent
  },
  {
    path:'add', component: MerchantAddComponent
  },
  {
    path:'detail', component: MerchantDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MerchantRoutingModule { }
