import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotificationsettingComponent } from './notificationsetting.component';
import { NotificationsettingAddComponent } from './add/notificationsetting.add.component';
import { FormInputComponent }  from '../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from './../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { FormSelectComponent } from '../../../component-libs/form-select/form-select.component';
import { NotificationsettingDetailComponent } from './detail/notificationsetting.detail.component';
import { NotificationsettingRoutingModule } from './notificationsetting-routing.module';
import { NotificationsettingEditComponent } from './edit/notificationsetting.edit.component';
import { BsComponentModule } from '../bs-component/bs-component.module';

@NgModule({
  imports: [CommonModule,
    NotificationsettingRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule
],
  
  declarations: [
    NotificationsettingComponent, NotificationsettingAddComponent, NotificationsettingDetailComponent, NotificationsettingEditComponent] 
})
export class NotificationsettingModule { }
