import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MemberSummaryComponent } from './membersummary.component';

const routes: Routes = [
  {
      path: '', component: MemberSummaryComponent,

  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MemberSummaryRoutingModule { }
