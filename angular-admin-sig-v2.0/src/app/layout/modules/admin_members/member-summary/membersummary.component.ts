import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { MemberService } from '../../../../services/member/member.service';
import { DomSanitizer } from '@angular/platform-browser';


// import { Http, Response, Headers, RequestOptions, RequestMethod, RequestOptionsArgs, URLSearchParams, ResponseContentType } from '@angular/http';
// import { Observable } from 'rxjs';
// import { map } from "rxjs/operators";
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';
// import 'rxjs/Rx' ;


@Component({
  selector: 'app-membersummary-member',
  templateUrl: './membersummary.component.html',
  styleUrls: ['./membersummary.component.scss'],
  animations: [routerTransition()]
})

export class MemberSummaryComponent implements OnInit {

  data       : any = [];
  service    : MemberService
  errorLabel : string

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: string[] = [];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any[] = [
      { data: [], label: 'Registered member' },
  ];

  public barChartData2: any[] = [
    {data: [], label: 'Guest'}
  ]

  public barChartData3: any [] = [
    {data:[], label: 'Guest Daily'}
  ]

  public barChartData4: any [] = [
    {data: [], label: 'Guest Monthly'}
  ]



  // private options = new RequestOptions(
  //   { headers: new Headers({ 'Content-Type': 'application/json' }) });
  constructor(public memberService:MemberService , public sanitizer:DomSanitizer) {
    this.firstLoad();
    // this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
  }

  onOnUpdateCart(){
    const clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = [];
    this.barChartLabels.forEach(()=>{
        clone[0].data.push(Math.round(Math.random() * 100))
    });
    this.barChartData   = clone;
    
    setTimeout(()=>{
      this.firstLoad();
    },300)
    
  }

  async ngOnInit() {
    let currentDate = new Date();
    console.log(this.barChartLabels);
    this.onOnUpdateCart();

  }

  async firstLoad() {

    try {
      this.service       = this.memberService;
      const result: any  = await this.memberService.getMemberSummaryByDate('today');
      console.log("hehe", result)
      this.data          = result.result;
      if(this.data.registered_by_month){
        let barChartLabels = [];
        let newData        = [];

        let allData:any[];
        allData = this.data.registered_member_daily.value;

        for(let data in allData){
          // console.log('allData data', allData[data])
          barChartLabels.push(data)
          newData.push(allData[data])

        }
        allData = null; //clearing memory

        const clone = JSON.parse(JSON.stringify(this.barChartData));
        clone[0].data = newData;
        this.barChartLabels = barChartLabels
        this.barChartData   = clone;
        console.log('awd ' ,this.barChartData)

      }
    } catch (e) {
      console.log("this e result", e)

      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
    
    if(this.data.registered_by_month){
      let barChartLabels = [];
      let newData = [];
      let allData:any;

      allData = this.data.registered_by_month.value;

      for(let data in allData){
        barChartLabels.push(data)
        newData.push(allData[data]);

      }

      allData = null;
      const clone = JSON.parse(JSON.stringify(this.barChartData2));
      clone[0].data = newData;
      this.barChartLabels = barChartLabels
      this.barChartData2 = clone;
    

      }

    if (this.data.registered_guest_daily) {
      let barChartLabels = [];
      let newData = [];
      let allData: any;

      allData = this.data.registered_guest_daily.value;

      for (let data in allData) {
        barChartLabels.push(data)
        newData.push(allData[data]);

      }

      allData = null;
      const clone = JSON.parse(JSON.stringify(this.barChartData3));
      clone[0].data = newData;
      this.barChartLabels = barChartLabels
      this.barChartData3 = clone;
      console.log('test',this.barChartData3)

    }

    if (this.data.registered_guest_by_month) {
      let barChartLabels = [];
      let newData = [];
      let allData: any;

      allData = this.data.registered_guest_by_month.value;

      for (let data in allData) {
        barChartLabels.push(data)
        newData.push(allData[data]);

      }

      allData = null;
      const clone = JSON.parse(JSON.stringify(this.barChartData4));
      clone[0].data = newData;
      this.barChartLabels = barChartLabels
      this.barChartData4 = clone;


    }

    }

  }




