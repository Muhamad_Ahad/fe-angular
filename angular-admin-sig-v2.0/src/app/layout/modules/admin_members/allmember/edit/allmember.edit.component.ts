import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../../router.animations';
import { MemberService } from '../../../../../services/member/member.service';


@Component({
  selector: 'app-all-member-edit',
  templateUrl: './allmember.edit.component.html',
  styleUrls: ['./allmember.edit.component.scss'],
  animations: [routerTransition()]
})

export class AllMemberEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;
  errorLabel : any = false;

  public loading        : boolean = false;

  public activationStatus: any = [
    {label: 'ACTIVATED', value: 'ACTIVATED'},
    {label: 'INACTIVED', value: 'INACTIVED'}
  ];

  public memberStatus: any = [
    {label: 'ACTIVE', value: 'ACTIVE'},
    {label: 'INACTIVE', value: 'INACTIVE'}
  ];

  constructor(public memberService: MemberService) {
  }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    this.detail.previous_member_id = this.detail.member_id;
    this.activationStatus.forEach((element, index) => {
        if (element.value == this.detail.activation_status) {
            this.activationStatus[index].selected = 1;
        }
    });

    this.detail.previous_member_id = this.detail.member_id;
    this.memberStatus.forEach((element, index) => {
        if (element.value == this.detail.member_status) {
            this.memberStatus[index].selected = 1;
        }
    });

  }

  backToDetail() {
    // console.log(this.back);
    this.back[0][this.back[1]]();
  }

  async saveThis() {
    try {
      this.loading = !this.loading;
      await this.memberService.updateMemberID(this.detail);
      this.loading = !this.loading;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

}
