import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromotionRuleComponent } from './promotion-rule.component';

describe('PromotionRuleComponent', () => {
  let component: PromotionRuleComponent;
  let fixture: ComponentFixture<PromotionRuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromotionRuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotionRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
