import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { MemberService } from '../../../services/member/member.service';
import { DomSanitizer } from '@angular/platform-browser';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';
import { _daysInMonth } from 'ngx-bootstrap/chronos/utils/date-getters';

// import { Http, Response, Headers, RequestOptions, RequestMethod, RequestOptionsArgs, URLSearchParams, ResponseContentType } from '@angular/http';
// import { Observable } from 'rxjs';
// import { map } from "rxjs/operators";
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';
// import 'rxjs/Rx' ;


@Component({
  selector: 'app-data-member',
  templateUrl: './data-member.component.html',
  styleUrls: ['./data-member.component.scss'],
  animations: [routerTransition()]
})

export class DataMemberComponent implements OnInit {

  Members       : any = [];
  row_id        : any = "_id";

  tableFormat   : TableFormat = {
    title           : 'Member Report',
    label_headers   : [
                  {label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'username'},
                  {label: 'Nama Pelanggan',     visible: true, type: 'string', data_row_name: 'full_name'},
                  {label: 'No. WA Pelanggan',     visible: true, type: 'string', data_row_name: 'cell_phone'},
                  // {label: 'Penerima Hadiah',     visible: true, type: 'penerima_hadiah', data_row_name: 'member_address'},
                  // {label: 'No. Telp Penerima',     visible: true, type: 'telp_penerima', data_row_name: 'member_address'},
                  // {label: 'Alamat Penerima',     visible: true, type: 'alamat_penerima', data_row_name: 'member_address'},
                  // {label: 'Desa/Kelurahan',     visible: true, type: 'kelurahan_penerima', data_row_name: 'member_address'},
                  // {label: 'Kecamatan',     visible: true, type: 'kecamatan_penerima', data_row_name: 'member_address'},
                  // {label: 'Kabupaten/Kota',     visible: true, type: 'kota_penerima', data_row_name: 'member_address'},
                  // {label: 'Provinsi',     visible: true, type: 'provinsi_penerima', data_row_name: 'member_address'},
                  // {label: 'Kode Pos',     visible: true, type: 'kode_pos_penerima', data_row_name: 'member_address'},
                  {label: 'Point Balance',     visible: true, type: 'string', data_row_name: 'point_balance'},
                  // {label: 'Point Expiry Date',     visible: true, type: 'expire_date', data_row_name: 'points'},
                  {label: 'Status',     visible: true, type: 'string', data_row_name: 'status'},
                  {label: 'Updated Date',     visible: true, type: 'date', data_row_name: 'updated_date'},
                  {label: 'Created Date',     visible: true, type: 'date', data_row_name: 'created_date'},
                ],
    row_primary_key : '_id',
    formOptions     : {
                    addForm   : true,
                    row_id    : this.row_id,
                    this      : this,
                    result_var_name : 'Members',
                    detail_function : [this, 'callDetail'] 
                    }
                  };

  form_input    : any = {};
  errorLabel    : any = false;
  errorMessage  : any = false;
  totalPage     : 0;
 
  memberDetail  : any = false;
  service       : any ;
  srcDownload   : any ;
  

  // private options = new RequestOptions(
  //   { headers: new Headers({ 'Content-Type': 'application/json' }) });
  constructor(
    public memberService:MemberService , 
    public sanitizer:DomSanitizer
    ) {
    this.tableFormat.formOptions.addForm = false;
    this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
  }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    try {
      // let params = {
      //   'type':'member'
      // }
      this.service    = this.memberService;

      const result: any  = await this.memberService.getMemberAllReport();
      // console.warn("data member", result);
      this.totalPage = result.total_page;
      this.Members = result.values;
      // console.warn("Members is called", this.Members);
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
      }
    }

  }

  public async callDetail(data, rowData) {
    
    try {
      // this.service    = this.memberService;
      // let result: any = await this.memberService.getMemberReportByID(_id);
      // this.memberDetail = result.values[0];
      
      this.memberDetail = this.Members.find(member => member.username == rowData.username);
      // console.warn("Member detail",this.memberDetail);
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj) {
    obj.memberDetail = false;
    obj.firstLoad();
  }

  onDownload(downloadLint)
  {
    let srcDownload = downloadLint;
    //let srcDownload = "http://localhost:8888/f3/assets/csv/members_report.csv";
    this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl(srcDownload);
    //console.log(this.srcDownload);

    this.getDownloadFileLint();
  }

  public async getDownloadFileLint() {
    try
    {

      let result: any;
      this.service    = this.memberService;
      result= await this.memberService.getDownloadFileLint();
      //console.log(result.result);
      let downloadLint = result.result;
      //console.log(downloadLint);

      //To running other subfunction with together automatically
      this.onDownload(downloadLint);
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  } 

  // public async pagination() {
  //   try
  //   {
  //     let result: any;
  //     this.service = this.memberService;
  //     result = await this.memberService.getDownloadFileLint();
  //     //console.log(result.result);
  //     let downloadLint = result.result;
  //     //console.log(downloadLint);

  //     //To running other subfunction with together automatically
  //     this.onDownload(downloadLint);
  //   }
  //   catch (e) {
  //     this.errorLabel = ((<Error>e).message);//conversion to Error type
  //   }  
  // }

}
