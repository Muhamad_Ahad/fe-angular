import { ProductService } from './../../../../services/product/product.service';
import { Component, OnInit, Input, NgZone } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { MemberService } from '../../../../services/member/member.service';
import Swal from 'sweetalert2';
import { basename } from 'path';
// import { ProductService } from '../../../../services/product/product.service';
import * as content from '../../../../../assets/json/content.json';


@Component({
  selector: 'app-data-member-edit',
  templateUrl: './data-member.edit.component.html',
  styleUrls: ['./data-member.edit.component.scss'],
  animations: [routerTransition()]
})

export class DataMemberEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  public contentList : any = (content as any).default;
  errorLabel : any = false;
  member_status:any;
  activation_status:any;

  private allBast : any = [];
  private allSuratKuasa : any = [];

  optionsKuasa1: any = [
    {label:"KUASA", value:"true"},
    {label:"TIDAK KUASA", value:"false"}
  ];
  optionsKuasa2 : any =  [
    {label:"KUASA", value:"KUASA"},
    {label:"TIDAK KUASA", value:"TIDAK KUASA"}
  ];
  optionsKuasa3 : any = [
    {label:"KUASA", value:"ya"},
    {label:"TIDAK KUASA", value:"tidak"}
  ];
  private updateSingle: any;
  
  public loading        : boolean = false;

  public activationStatus: any = [
    {label: 'ACTIVATED', value: 'ACTIVATED'},
    {label: 'INACTIVED', value: 'INACTIVED'}
  ];

  public memberStatus: any = [
    {label: 'ACTIVE', value: 'ACTIVE'},
    {label: 'INACTIVE', value: 'INACTIVE'}
  ];

  errorFile: any = false;
  selectedFile: any;
  selFile: any;
  loadingKTPPemilik: boolean = false;
  loadingNPWPPemilik: boolean = false;
  loadingKTPPenerima: boolean = false;
  loadingNPWPPenerima: boolean = false;
  loadingBAST: any = [];
  loadingSuratKuasa: any = [];

  mci_project: any = false;

  constructor(public memberService: MemberService, public productService:ProductService, private zone: NgZone) {
  }

  ngOnInit() {
    this.firstLoad();

  }

  checkValueKuasa(){
    if(this.updateSingle["hadiah_dikuasakan"] == "true" || this.updateSingle["hadiah_dikuasakan"] == "false"){
      let options = this.optionsKuasa1.map(el => {
        if(el.value == this.updateSingle["hadiah_dikuasakan"]){
          el.selected = 1;
        }
        return el;
      });
      this.optionsKuasa1 = options;
    }
  
    if(this.updateSingle["hadiah_dikuasakan"] == "KUASA" || this.updateSingle["hadiah_dikuasakan"] == "TIDAK KUASA"){
      let options = this.optionsKuasa2.map(el => {
        if(el.value == this.updateSingle["hadiah_dikuasakan"]){
          el.selected = 1;
        }
        return el;
      });
      this.optionsKuasa2 = options;
    }
  
    if(this.updateSingle["hadiah_dikuasakan"] == "ya" || this.updateSingle["hadiah_dikuasakan"] == "tidak"){
      let options = this.optionsKuasa3.map(el => {
        if(el.value == this.updateSingle["hadiah_dikuasakan"]){
          el.selected = 1;
        }
        return el;
      });
      this.optionsKuasa3 = options;
    }

    if(this.updateSingle["hadiah_dikuasakan"] == "-"){
      console.warn("im here!")
      let options = this.optionsKuasa2.map(el => {
        if(el.value == this.updateSingle["hadiah_dikuasakan"]){
          el.selected = 1;
        }
        return el;
      });
      this.optionsKuasa2 = options;
    }

  }

  isArray(curVar : any){
    return Array.isArray(curVar) ? true : false;
  }

  assignArrayBoolean(curVar : any, varCont : any){
    if(this.isArray(curVar)){
      curVar.forEach(element => {
        varCont.push(false);
      });
    }
  }

  async firstLoad() {
    // console.warn("detail",this.detail);

    const program = localStorage.getItem('programName');
    let _this = this;
    this.contentList.filter(function(element) {
        if(element.appLabel == program) {
            if(element.type == "reguler") {
                _this.mci_project = true;
            } else {
                _this.mci_project = false;
            }
        }
    });

    this.updateSingle = JSON.parse(JSON.stringify(this.detail.input_form_data));
    this.updateSingle.foto_bast = this.isArray(this.updateSingle.foto_bast) ? this.updateSingle.foto_bast: [];
    this.allBast = this.updateSingle.foto_bast;
    this.updateSingle.foto_surat_kuasa = this.isArray(this.updateSingle.foto_surat_kuasa) ? this.updateSingle.foto_surat_kuasa: [];
    this.allSuratKuasa = this.updateSingle.foto_surat_kuasa;
    this.assignArrayBoolean(this.allBast, this.loadingBAST);
    this.assignArrayBoolean(this.allSuratKuasa, this.loadingSuratKuasa);
    // console.log("loadingBAST", this.loadingBAST);
    // this.assignArrayBoolean(this.allSuratKuasa);
    // console.log("count", this.allBast, this.allSuratKuasa);
    this.checkValueKuasa();
    
    this.detail.previous_member_id = this.detail.member_id;
    this.activationStatus.forEach((element, index, products_ids) => {
        if (element.value == this.detail.activation_status) {
            this.activationStatus[index].selected = 1;
        }
    });

    this.detail.previous_member_id = this.detail.member_id;
    this.memberStatus.forEach((element, index, products_ids) => {
        if (element.value == this.detail.member_status) {
            this.memberStatus[index].selected = 1;
        }
    });

  }

  backToDetail() {
    // console.log("Edit member",this.back);
    this.back[0][this.back[1]]();
    // this.back[0]();
  }

  validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
  }

  openUpload(type:any, index:any){
    let upFile : any = document.getElementById(type+index);
    upFile.click();
  }


  async saveThis() {
    
    let new_form = {
      // key:"ps-form-1",
      id_pel: this.detail.input_form_data.id_pel,
    };

    if(this.updateSingle.foto_bast && this.updateSingle.foto_bast.length > 0){
      let currentBast = this.updateSingle.foto_bast.filter((element : any) => {
        if(element.url && this.validURL(element.url)){
          return element;
        }
      });
      this.updateSingle.foto_bast = currentBast;
    }
    if(this.updateSingle.foto_surat_kuasa && this.updateSingle.foto_surat_kuasa.length > 0){
      let currentSuratKuasa = this.updateSingle.foto_surat_kuasa.filter((element:any) => {
        if(element.url && this.validURL(element.url)){
          return element;
        }
      });
      this.updateSingle.foto_surat_kuasa = currentSuratKuasa;
    }
    
    for(let prop in this.updateSingle){
      if(this.updateSingle[prop] != this.detail[prop] && this.updateSingle[prop] != ""){
        if(prop == 'hadiah_dikuasakan') {
          let kuasa = ["Ya", "true", "ya"];
          let nonKuasa = ["Tidak", "false", "tidak"];
          if(kuasa.includes(this.updateSingle[prop])){
            new_form[prop] = "KUASA";
          } else if(nonKuasa.includes(this.updateSingle[prop])){
            new_form[prop] = "TIDAK KUASA";
          }else {
            new_form[prop] = this.updateSingle[prop];
          }
        } else {
          new_form[prop] = this.updateSingle[prop];
        }
      }
      if(prop == 'foto_bast' || prop == 'foto_surat_kuasa'){
        new_form[prop] = this.updateSingle[prop];
      }
    }


    // console.warn("new form", new_form);
    // console.warn("updateSingle", this.updateSingle);
    // console.warn("detail", this.detail);

      
    try {
      await this.memberService.updateSingleFormMember(new_form);
      this.detail = JSON.parse(JSON.stringify(this.updateSingle));
      //  Swal.fire('Success', 'Edit data pelanggan berhasil', 'success');
      Swal.fire({
        title: 'Success',
        text: 'Edit data pelanggan berhasil',
        icon: 'success',
        confirmButtonText: 'Ok',
      }).then((result) => {
        if(result.isConfirmed){
          this.backToDetail();
        }
      });
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
      // alert("Error");
      let message = this.errorLabel;
      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
      }
      Swal.fire({
        icon: 'error',
        title: message,
      });
    }
  }

  async onFileSelected2(event, img) {
		var reader = new FileReader()
		try {
			this.errorFile = false;
			let fileMaxSize = 3000000; // let say 3Mb
			Array.from(event.target.files).forEach((file: any) => {
				if (file.size > fileMaxSize) {
					this.errorFile = 'Maximum File Upload is 3MB';
				}
			});
			this.selectedFile = event.target.files[0];

			reader.onload = (event: any) => {
				this.selFile = event.target.result;
			}
			if(event.target.files[0]){
        reader.readAsDataURL(event.target.files[0]);
      }
			if (this.selectedFile) {
        let valueImage = this.checkLoading(img, true);
				
				const logo = await this.productService.upload(this.selectedFile, this, 'image', (result) => {
          console.warn("RSULT IMG", result)
          this.updateSingle[valueImage] = result.base_url + result.pic_big_path;
          this.checkLoading(img, false);
					// console.log("updateSingle", this.updateSingle);
					// this.callImage(result, img);
				});
			}
			
		} catch (e) {
			this.errorLabel = (<Error>e).message; //conversion to Error type

      let message = this.errorLabel;
      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
      }
      Swal.fire({
        icon: 'error',
        title: message,
      });
		}
  }

  async onFileSelected(event, typeImg: any, idx : any) {
    if(typeImg == 'bast'){
      this.loadingBAST[idx] = true;
    }

    else if(typeImg == 'suratKuasa'){
      this.loadingSuratKuasa[idx] = true;
    }

		var reader = new FileReader();
		try {
			this.errorFile = false;
			let fileMaxSize = 3000000; // let say 3Mb
			Array.from(event.target.files).forEach((file: any) => {
				if (file.size > fileMaxSize) {
					this.errorFile = 'Maximum File Upload is 3MB';
				}
			});
			this.selectedFile = event.target.files[0];

			reader.onload = (event: any) => {
				this.selFile = event.target.result;
			}
			if(event.target.files[0]){
        reader.readAsDataURL(event.target.files[0]);
      }
			if (this.selectedFile) {
				await this.productService.upload(this.selectedFile, this, 'image', (result) => {
          const dataImage = {
            "url": result.base_url + result.pic_big_path,
            "updated_date": Date.now()
          }
          if(typeImg == 'bast'){
            this.updateSingle.foto_bast[idx] = {...this.updateSingle.foto_bast[idx], ...dataImage};
            this.loadingBAST[idx] = false;

          }

          else if(typeImg == 'suratKuasa'){
            this.updateSingle.foto_surat_kuasa[idx] = {...this.updateSingle.foto_surat_kuasa[idx], ...dataImage};
            this.loadingSuratKuasa[idx] = false;
          }

          
				});
			}
			
		} catch (e) {
			this.errorLabel = (<Error>e).message; //conversion to Error type

      let message = this.errorLabel;
      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
      }
      Swal.fire({
        icon: 'error',
        title: message,
      });
		}
  }

  addMoreUpload(typeImg : any)
  {
    const dummyImg = {
      "url" : "",
      "updated_date" : Date.now(),
      "desc": ""
    }
    if(typeImg == 'bast'){
      // if(!this.isArray(this.updateSingle.foto_bast)) this.updateSingle.foto_bast = [];
      this.updateSingle.foto_bast.push(dummyImg);
      this.loadingBAST.push(false);
    }else if(typeImg == 'suratKuasa'){
      // if(!this.isArray(this.updateSingle.foto_surat_kuasa)) this.updateSingle.foto_surat_kuasa = [];
      this.updateSingle.foto_surat_kuasa.push(dummyImg);
      this.loadingSuratKuasa.push(false);
    }
  }

  deleteImage(type : any, idx : any, url : any){
    Swal.fire({
      title: "Delete",
      text : "Are you sure want to delete this ?",
      imageUrl : url,
      imageHeight : 250,
      imageAlt : type,
      showCancelButton:true,
      focusConfirm:false,
    }).then(res =>{
      if(res.isConfirmed){
        if(type == "bast"){
          this.updateSingle.foto_bast.splice(idx,1);
          this.loadingBAST.splice(idx,1);
          // console.log("BAST DELETE", this.updateSingle.foto_bast,this.allBast);
        }else if(type="suratKuasa"){
          this.updateSingle.foto_surat_kuasa.splice(idx,1);
          this.loadingSuratKuasa.splice(idx,1);
          // console.log("SURAT KUASA DELETE", this.updateSingle.foto_surat_kuasa,this.allSuratKuasa);
        }
      }
    });
  }

  convertDate(time){
    const date:any =  new Date(time);
    const dateString = date.toString();
    const retDate = dateString.split("GMT");
    
    return retDate[0];
  }

  checkLoading(fieldName, fieldValue){
    let valueImage:any;
    switch (fieldName) {
      case 'ktpPemilik':
        valueImage = 'foto_ktp_pemilik';
        this.loadingKTPPemilik = fieldValue;
        break;

      case 'npwpPemilik':
        valueImage = 'foto_npwp_pemilik';
        this.loadingNPWPPemilik = fieldValue;
        break;

      case 'ktpPenerima':
        valueImage = 'foto_ktp_penerima';
        this.loadingKTPPenerima = fieldValue;
        break;

      case 'npwpPenerima':
        valueImage = 'foto_npwp_penerima';
        this.loadingNPWPPenerima =fieldValue;
        break;
      case 'bast':
        valueImage = 'foto_bast';
        this.loadingBAST = fieldValue;
        break;
      case 'suratKuasa' :
        valueImage = 'foto_surat_kuasa';
        this.loadingSuratKuasa = fieldValue;
    }
    return valueImage;
  }

}
