import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DataMemberComponent } from './data-member.component';
import { DataMemberAddComponent } from './add/data-member.add.component';
import { DataMemberDetailComponent } from './detail/data-member.detail.component';

const routes: Routes = [
  {
      path: '', component: DataMemberComponent,

  },
  {
      path:'add', component: DataMemberAddComponent
  },
  {
    path:'detail', component: DataMemberDetailComponent
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataMemberRoutingModule { }
