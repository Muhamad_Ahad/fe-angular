import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { PointsGroupService } from '../../../services/points.group/pointsgroup.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
  selector: 'app-pints-group',
  templateUrl: './points-group.component.html',
  styleUrls: ['./points-group.component.scss'],
  animations: [routerTransition()]
})

export class PointsGroupComponent implements OnInit {
  @Input() public detail: any;

  PointsGroup       : any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Active Points Group Detail',
                                  label_headers   : [
                                    {label: 'Campaign ID', visible: true, type: 'string', data_row_name: 'campaign_id'},
                                    {label: 'Group Code', visible: true, type: 'string', data_row_name: 'group_code'},
                                      {label: 'Group Name', visible: true, type: 'string', data_row_name: 'group_name'},
                                      // {label: 'Description', visible: false, type: 'string', data_row_name: 'description'},
                                      {label: 'Status',
                                          options: [
                                            'active',
                                            'inactive',
                                          ],
                                          visible: true, type: 'list', data_row_name: 'status'},
                                      // {label: 'Start Date', visible: false, type: 'date', data_row_name: 'start_date'},
                                      // {label: 'End Date', visible: false, type: 'date', data_row_name: 'start_date'},
                                      ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'PointsGroup',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  form_input    : any = {};
  errorLabel : any = false;
  pointsGroupDetail: any = false;
  service     : any ;


  constructor(public pointsgroupService:PointsGroupService) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    try {
      
      this.service    = this.pointsgroupService;
      const result: any  = await this.pointsgroupService.getAllPointsGroupLint();
      this.PointsGroup = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  public async callDetail(_id) {
    
    try {
      
      this.service    = this.pointsgroupService;
      let result: any = await this.pointsgroupService.detailPointsGroup(_id);
      this.pointsGroupDetail = result.result;
      console.log(this.pointsGroupDetail);
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj) {
    obj.pointsGroupDetail = false;
  }

}
