
import { Component, OnInit }  from '@angular/core';
import { routerTransition }   from '../../../router.animations';
import { ProductService }     from '../../../services/product/product.service';
import { count } from 'rxjs/operators';
import {FormOptions, TableFormat, productsTableFormat} from '../../../object-interface/common.object';
import { Router } from '@angular/router';

@Component({
  selector    : 'app-product',
  templateUrl : './voucher.component.html',
  styleUrls   : ['./voucher.component.scss'],
  animations  : [routerTransition()]
})

export class VouchersComponent implements OnInit {

  // this section belongs to product item list
  Products      : any = [];
  tableFormat   : TableFormat = {
    title           : 'E-Voucher Data',
    label_headers   : [
      {label: 'E-Voucher Name', visible: true, type: 'string', data_row_name: 'product_name'},
      {label: 'E-Voucher Code', visible: true, type: 'string', data_row_name: 'product_code'},
      // {label: 'Type', visible: true, type: 'string', data_row_name: 'type'},
      {label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_qty'},
      {label: 'Status',visible:true, type: 'string', data_row_name: 'status'},
      {label: 'Created Date', visible: true, type: 'string', data_row_name: 'created_date'},
      {label: 'Updated Date', visible: true, type: 'string', data_row_name: 'updated_date'},
    ],
    row_primary_key : 'product_code',
    formOptions     : {
                      row_id: 'product_code',
                      addForm: true,
                      this  : this,
                      result_var_name: 'Products',
                      detail_function: [this, 'callDetail'],
                      customButtons:[
                        {label: 'Approve', func:(f)=>{this.approval(f)}},
                      ]
                    },
    show_checkbox_options: true
  }

  form_input    : any = {};
  errorLabel : any = false;
  errorMessage  : any = false;
  totalPage  = 0;
  page = 1;
  pageSize = 50;

  prodDetail              : any = false;
  service                 : any;

  selectedFile            : File    = null;
  progressBar             :number   = 0;
  cancel                  :boolean  = false;
  errorFile               : any     = false;

  constructor(public productService:ProductService, private router: Router) { }

  ngOnInit() {
    this.firstLoad();
  }

  async approval(listedData:any[]){
    // console.log("masuk coy")
    if(listedData == undefined || listedData.length==0){
      return false;
    }
    let listOfData=[];
//
    listedData.forEach((element, index) => {
      listOfData.push(element._id);
    });
    listedData = [];
    if(listOfData.length == 0) return false;
    this.service        = this.productService;
    let result          = await this.productService.approvedProducts({product_ids: listOfData});
    if(result.result.status == 'success'){
      alert("the Current Data Below has been approved");
      this.firstLoad();
    }
  }
  
  async firstLoad(){
    try{
      let result: any;
      this.service    = this.productService;
      result          = await this.productService.getEvoucherReport();
      this.totalPage  = result.total_page;
      this.Products   = result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

			if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
				this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
			}
    }
  }

  public async callDetail(_id)
  {
    this.router.navigate(['administrator/productadmin/edit'], {queryParams: {product_code:_id}})
    // try{
    //   let result: any;
    //   this.service    = this.productService;
    //   result          = await this.productService.detailProducts(_id);
    //   if(!result.error && result.error == false){
    //     this.prodDetail = result.result[0];
    //   }
    // } catch (e) {
    //   this.errorLabel = ((<Error>e).message);//conversion to Error type
    // }
  }

  public async backToHere(obj){
    obj.prodDetail = false;
    // delete obj.prodDetail;
    
  }

  onFileSelected(event){
    this.errorFile = false;
    let fileMaxSize = 3000000;// let say 3Mb
    Array.from(event.target.files).forEach((file: any) => {
        if(file.size > fileMaxSize){
          this.errorFile="maximum file is 3Mb";
        }
    });
    this.selectedFile = event.target.files[0];

  } 

  updateProgressBar(value){
    this.progressBar = value;
  }

  cancelThis(){
    
    this.cancel = !this.cancel;
  }
  callAfterUpload(result) {
    console.log("THE RESULT", result);

  }

  async onUpload(){
    try{
      this.cancel = false;
      if(this.selectedFile){
        await this.productService.uploadFile(this.selectedFile,this,'product', this.callAfterUpload);
        }
        alert("Upload Success")
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
    if(this.selectedFile) {}
  }
}
