import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { ProductService } from '../../../../services/product/product.service';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-product-edit',
  templateUrl: './product.edit.component.html',
  styleUrls: ['./product.edit.component.scss'],
  animations: [routerTransition()]
})



export class ProductEditComponent implements OnInit {
  selectedFile = null;
  @Input() public detail: any;
  @Input() public back;
  public loading: boolean = false;
  public categoryProduct: any = [];

  public editStatus: any = [
    { label: "ACTIVE", value: 1 }
    , { label: "INACTIVE", value: 0 }
  ];

  errorLabel: any = false;
  category;
  product_status: any;
  cancel: boolean = false;

  url: '';
  errorFile: any = false;
  rowOfTable;
  currentValue;
  product_code = [];
  product_code_data: any;
  service: any;
  description: any;
  progressBar = 0;
  // products_ids: any = [];
  status: any;
  product_sku: any = []
  public Editor = ClassicEditor;
  closeResult: string;

  productData: any = {}

  merchantData

  // listedData: any[];




  constructor(public productService: ProductService, private modalService: NgbModal) { }

  ngOnInit() {
    this.getCategory();
    this.firstLoad();
    // this.setStatus()

  }

  async getCategory() {
    this.category = await this.productService.getCategoryLint();
    this.category.result.forEach((element) => {
      this.categoryProduct.push({ label: element.name, value: element.name });
    });

    this.category = this.categoryProduct[0].value;
  }

  selectCategory(event) {
    this.category = event.target.value
    this.productData.category = [event.target.value]
  }

  async onUpload() {
    try {
      this.cancel = false;
      let result
      if (this.selectedFile) {
        console.log("result", result)
        result = await this.productService.upload(this.selectedFile, this, 'image', (result) => { this.callAfterUpload(result) });
      }

    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
    //   this.onUpload.
  }

  callAfterUpload(result) {

    this.productData = { ...this.productData, ...result };
    console.log("THE RESULT", result);
    console.log("this.productData", this.productData)
  }

  async setStatus() {
    this.service = await this.productService.statusEdit
    this.productData.previous_status = this.productData.status;
    this.editStatus.forEach((element, index) => {
      if (element.value == this.productData.active) {
        this.editStatus[index].selected = 1;
      }
      if (element.value == this.productData.active) {
        this.editStatus[index].selected = 0;
      }
    })

  }
  cancelThis() {
    this.cancel = !this.cancel
  }


  async firstLoad() {

    try {
      this.detail.previous_product_code = this.detail.product_code
      if (this.detail._id)
        delete this.detail._id;
      this.categoryProduct.forEach((element, index, products_ids) => {
        if (element.value == this.detail.status) {
          this.categoryProduct[index].selected = 1;
        }
      });

      console.log("SKU", this.detail.product_sku)

      Object.keys(this.detail).forEach(key => {
        this.productData[key] = this.detail[key]
      });
      console.log("product data : ", this.productData)

    }
    catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  async removeSKU(sku) {
    console.log("sku : ", sku)
  }

  backToDetail() {
    history.back();
  }

  async saveThis() {

    try {
      this.productData.min_order = parseInt(this.productData.min_order)
      if (this.productData.type == 'Product') {
        this.productData.dimensions.length = parseInt(this.productData.dimensions.length)
        this.productData.dimensions.width = parseInt(this.productData.dimensions.width)
        this.productData.dimensions.height = parseInt(this.productData.dimensions.height)

      }
      this.productData.price = parseInt(this.productData.price)
      this.productData.fixed_price = parseInt(this.productData.fixed_price)
      this.productData.qty = parseInt(this.productData.qty)
      let prevProductCode = this.productData.previous_product_code
      delete this.productData.previous_product_code
      delete this.productData.status
      console.log("RESPON", this.productData)
      if (this.productData.active)
        this.productData.active = parseInt(this.productData.active)
      this.loading = !this.loading;
      let result = await this.productService.updateProduct(this.productData, prevProductCode);
      if (result) {
        alert("Saved!")
      }
      // console.log("this.detail", this.detail)
      this.loading = !this.loading;

    }
    catch (e) {
      alert(this.errorLabel = ((<Error>e).message));//conversion to Error type
    }
  }

  // async saveSKU(sku){


  //   var item = this.detail.product_sku;
  //   var push = item.push(sku);
  //   console.log("Berhasil ga", push)

  // }

  //  

  onFileSelected(event) {
    this.errorFile = false;
    let fileMaxSize = 3000000;// let say 3Mb
    Array.from(event.target.files).forEach((file: any) => {
      if (file.size > fileMaxSize) {
        this.errorFile = "maximum file is 3Mb";
      }
    });
    this.selectedFile = event.target.files[0];
  }


  //MODALS FOR ADDING SKU
  // open(content) {
  //   this.modalService.open(content).result.then((result) => {
  //     this.closeResult = `Closed with: ${result}`;
  //   });
  // }

  // elements: any=[
  //   {id:1, first:'Test', last:'01', handle:'@google'}
  // ];

  // headElements = ['id', 'first', 'last', 'handle'];


}  
