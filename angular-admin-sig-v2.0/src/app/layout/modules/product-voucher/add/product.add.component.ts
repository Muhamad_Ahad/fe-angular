import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { ProductService } from '../../../../services/product/product.service';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ControlValueAccessor } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-product-add',
  templateUrl: './product.add.component.html',
  styleUrls: ['./product.add.component.scss'],
  animations: [routerTransition()]
})

export class ProductAddComponent implements OnInit {
  public name: string = "";
  public categoryProduct: any = [];
  @Input() public back;
  public productType: any = [];
  public merchantList: any = [];
  public detail: any;
  public merchant_group: any = [];
  public condition: any = [
    { label: "NEW", value: "new" },
    { label: "SECOND", value: "second" },
  ]

  public redeem_type: any = [
    { label: "Voucher Link", value: "voucher_link" },
    { label: "Voucher Code", value: "voucher_code" }

  ]

  // condition:any;

  Products: any = [];
  service: any;
  public loading: boolean = false;
  /* public productStatus = [
    {label:"ACTIVE", value:"ACTIVE", selected:1}
   ,{label:"INACTIVE", value:"INACTIVE"}
 ]; */

  toggleDelete = 0;
  selectedFile = null;
  progressBar: number = 0;
  cancel: boolean = false;
  errorFile: any = false;
  errorLabel: any = false;
  category;
  base_url: any;
  pic_big_path: any;
  pic_medium_path: any;
  pic_small_path: any;
  pic_file_name: any;
  qty: any;
  owner_name: any;
  owner_id: any;
  hashtags: any;
  discount: any;
  point: any;
  price_after_discount: any;
  price: any;
  description: any;
  product_code: any;
  product_type: any;
  // evoucher_redeem:any;
  merchant_username: any;
  url: '';
  keyname: any;
  newData = "JUmp start";
  data: any = {
    merchant_username: '',
    type: '',
    category: [],
    price: 0,
    fixed_price: 0,
    tnc: '',
    description: '',
    qty: 0,
    product_name: '',
    product_code: '',
    condition: '',
    weight: '',
    dimensions: {
      length: 0,
      width: 0,
      height: 0
    },
    // dimension_length: '',
    // dimension_width: '',
    // dimension_height: '',
    min_order: '',
    // redeem_type:''
  }
  public Editor = ClassicEditor;


  constructor(public productService: ProductService, private router: Router) { }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    this.getCategory();
    // this.getProductType();
    this.getMerchant();
    this.getKeyname();
    console.log(this.merchant_group);
    // this.getConditionType();
    // this.getRedeemType();
    // this.redeemGroup();

  }

  async getCategory() {
    this.category = await this.productService.getCategoryLint();
    this.category.result.forEach((element) => {
      this.categoryProduct.push({ label: element.name, value: element.name });
    });

    this.category = this.categoryProduct[0].value;

  }

  async getKeyname() {
    this.keyname = await this.productService.configGroup();
    // console.log("PERBEDAAN MERCHANT GROUP/ PRODUCTCONFIG", this.keyname)l
    this.keyname.result.merchant_group.forEach((element) => {
      this.merchant_group.push({ label: element.name, value: element.name })
    });
    this.data.merchant_group = this.merchant_group[0].value
    // console.log("DISINI BRAY", this.merchant_group);
  }



  // async redeemGroup(){
  //   this.keyname = await this.productService.redeemGroup();

  // }

  async getProductType() {
    this.product_type = await this.productService.configuration();
    console.log("KELUAR GA:", this.product_type);
    this.product_type.result.product_type.forEach((element) => {
      this.productType.push({ label: element, value: element });
    });

    this.data.type = this.productType[0].value;
  }




  async getRedeemType() {
    this.product_type = await this.productService.configuration();
    // console.log("KELUAR GA:", this.product_type);
    this.product_type.result.evoucher_redeem.forEach((element) => {
      this.productType.push({ label: element, value: element });
    });

    this.data.type = this.productType[0].value;
  }

  async getConditionType() {
    this.product_type = await this.productService.configuration();
    this.product_type.result.product_condition.forEach((element) => {
      this.productType.push({ label: element, value: element });
      console.log("DISINI", this.productType);
    });

    this.data.type = this.productType[0].value;
  }

  async getMerchant() {
    this.merchant_username = await this.productService.getMerchantList();
    this.merchant_username.result.forEach((element) => {
      this.merchantList.push({ label: element.merchant_name, value: element.merchant_username });

    });

    this.data.merchant_username = this.merchantList[0].value;
    // this.form.merchant_username = this.merchantList[0].value;

    // console.log('merchant Data : ', this.data)
  }

  selectCategory(event) {
    this.category = event.target.value
    this.data.category = [event.target.value]
  }



  async saveThis() {

    try {
      // console.log(this.dimensions)
      this.data.price = parseInt(this.data.price)
      this.data.fixed_price = parseInt(this.data.fixed_price)
      this.data.min_order = parseInt(this.data.min_order)
      if (this.data.type == 'product') {
        this.data.weight = parseFloat(this.data.weight)
        this.data.dimensions.length = parseFloat(this.data.dimensions.length)
        this.data.dimensions.width = parseFloat(this.data.dimensions.width)
        this.data.dimensions.height = parseFloat(this.data.dimensions.height)
        delete this.data.merchant_group
      } else {
        delete this.data.weight
        delete this.data.dimensions
        delete this.data.condition
      }
      this.data.qty = parseInt(this.data.qty)
      this.service = this.productService;
      let result: any = await this.productService.addProductsLint(this.data);
      this.Products = result.result;

      alert("Product has been added!");
      this.router.navigate(['administrator/productadmin/edit'], { queryParams: { id: this.data._id } })

      // window.location.href="/productadmin";
    }
    catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  onFileSelected(event) {
    try {
      this.errorFile = false;
      let fileMaxSize = 3000000;// let say 3Mb
      Array.from(event.target.files).forEach((file: any) => {
        if (file.size > fileMaxSize) {
          this.errorFile = "Maximum File Upload is 3MB";
        }
      });
      this.selectedFile = event.target.files[0];
    }
    catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        let _event: any = event;
        this.url = _event.target.result;
      }
    }
  }

  updateProgressBar(value) {
    this.progressBar = value;
  }

  cancelThis() {
    this.cancel = !this.cancel;
  }




  async onUpload() {
    try {
      this.cancel = false;
      let result
      if (this.selectedFile) {
        console.log("result", result)
        result = await this.productService.upload(this.selectedFile, this, 'image', (result) => { this.callAfterUpload(result) });
      }

      if (result) {
        let product = this.detail
        this.detail = {
          ...product, ...result
        }

      }
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
    //   this.onUpload.
    return false;
  }

  callAfterUpload(result) {
    this.data = { ...this.data, ...result }
  }

  backToDetail() {
    window.history.back();

    // this.back[0][this.back[1]]();
  }

}
