import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { ReporterrorService } from '../../../../services/reporterror/reporterror.service';


@Component({
  selector: 'app-reporterror-edit',
  templateUrl: './reporterror.edit.component.html',
  styleUrls: ['./reporterror.edit.component.scss'],
  animations: [routerTransition()]
})

export class ReporterrorEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  public loading        :boolean = false;
  
  errorLabel    : any = false;
  transaction_status : any;


  constructor(public reporterrorService:ReporterrorService) {}

  ngOnInit() {
    this.firstLoad();
    
  }

  // isShippingValueExists(value){
  //   for(let element of this.detail.shipping_info){
  //   console.log("value", value, element.label);
  //     if(element.label.trim().toLowerCase() == value.trim().toLowerCase()) { return [true, element.created_date]}
  //   }
  //   return false;
  // }
  // ngOnChange(){
  //   console.log("HELLO");
  // }

  
  async firstLoad(){
    
    try 
    {
      // if (!this.detail.shipping_services ){
      //   this.detail.shipping_services = 'no-services';
      //   // console.log("Result", shippingServices)
      // }
      if(this.detail.status ){
        this.detail.status = this.detail.status.trim().toLowerCase();
      }
      
      // if(this.detail.shipping_info){
      //   this.shippingStatusList.forEach((element ,index)=> {
      //     let check = this.isShippingValueExists(element.value);
      //     if(check){
      //       this.shippingStatusList[index].checked = check[0];
      //       this.shippingStatusList[index].created_date = check[1];
      //     }
      //     else{
      //       this.shippingStatusList[index].checked = 0;
      //     }
      //   });
      // }

      this.detail.previous_status = await this.detail.status;
      // this.transactionStatus.forEach((element, index) => {
      // if(element.value == this.detail.transaction_status){
      //         this.transactionStatus[index].selected = 1;
      //     }
      // });
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

    // this.detail.previous_member_id = this.detail.member_id;
    // this.memberStatus.forEach((element, index) => {
    //     if(element.value == this.detail.member_status){
    //         this.memberStatus[index].selected = 1;
    //     }
    // });
    
  }

  backToDetail(){
    //console.log(this.back);
    this.back[0][this.back[1]]();
  }

  // convertShippingStatusToShippingInfo(){
  
  //   let newData = []
  //   this.shippingStatusList.forEach((element ,index)=> {
  //     if(element.checked) {
  //       newData.push({value: element.value})
  //     }
      
  //   });
  //   return newData;
  // }
  async saveThis(){
    try 
    {
      this.loading=!this.loading;
      let frm = JSON.parse(JSON.stringify(this.detail));

      // frm.shipping_info = this.convertShippingStatusToShippingInfo();
      // this.detail.shipping_status = this.valu
      await this.reporterrorService.updateReporterror(frm);
      this.loading=!this.loading;
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }
  
}
