import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReporterrorRoutingModule } from './reporterror-routing.module';
import { ReporterrorComponent } from './reporterror.component';
import { PageHeaderModule } from '../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { ReporterrorDetailComponent } from './detail/reporterror.detail.component';
import { ReporterrorEditComponent } from './edit/reporterror.edit.component';
// import { ReporterrorExcelDetailComponent } from './detail/reporterror.excel.detail.component';


@NgModule({
  imports: [
    CommonModule, 
    ReporterrorRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule],
  declarations: [ReporterrorComponent,ReporterrorDetailComponent,ReporterrorEditComponent]
  // declarations: [ReporterrorComponent]
})
export class ReporterrorModule { }
