import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { PointsTransactionService } from '../../../../services/points-transaction/points-transaction.service';

@Component ({
  selector: 'app-pointstransaction-detail',
  templateUrl: './pointstransaction.detail.component.html',
  styleUrls: ['./pointstransaction.detail.component.scss'],
  animations: [routerTransition()]
  })

export class PointstransactionDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  edit:boolean = false;
  
  constructor(public pointstransactionService:PointsTransactionService) {
  }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    
  }

  editThis(){
    console.log(this.edit );
    this.edit = !this.edit;
    console.log(this.edit );
  }
  backToTable(){
    console.log(this.back);
    this.back[1](this.back[0]);
  }

}
