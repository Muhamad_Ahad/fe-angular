import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderhistorywaitingComponent } from './orderhistorywaiting.component';
import { OrderhistorywaitingDetailComponent } from './detail/orderhistorywaiting.detail.component';

// import { OrderhistorysummaryDetailComponent } from './detail/orderhistorysummary.detail.component';
// import { OrderhistorysummaryEditComponent } from './edit/orderhistorysummary.edit.component';


const routes: Routes = [
  {
      path: '', component: OrderhistorywaitingComponent
  },
  // {
  //   path:'detail', component: OrderhistorysuccessDetailComponent
  // },
  // {
  //   path:'edit', component: OrderhistorysummaryEditComponent
  // }

  // {
  //   path: '', component: OrderhistorywaitingDetailComponent
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderhistorywaitingRoutingModule { }
