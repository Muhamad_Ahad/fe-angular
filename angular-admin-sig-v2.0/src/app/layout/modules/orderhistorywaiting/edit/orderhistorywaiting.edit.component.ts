import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';
import { isFunction } from '../../../../object-interface/common.function';
import Swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { ProgramNameService } from '../../../../services/program-name.service';
import * as content from '../../../../../assets/json/content.json';

@Component({
  selector: 'app-orderhistorywaiting-edit',
  templateUrl: './orderhistorywaiting.edit.component.html',
  styleUrls: ['./orderhistorywaiting.edit.component.scss'],
  animations: [routerTransition()]
})

export class OrderhistorywaitingEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  public loading: boolean = false;
  public done: boolean = false;
  programType:any = "";
  orderHistoryDetail;
	currentPermission;
  paramAppLabel: any;
  contentProgram : any = (content as any).default;
  public contentList : any = (content as any).default;
  public shippingStatusList: any = [
    {
			label: 'on checking and processing',
			value: 'on_processing',
			id: 'on-process',
			checked: true,
			hovered: false,
			values: 'on_processing'
		},
		{
			label: 'Warehouse Packaging',
			value: 'on_packaging',
			id: 'warehouse-packaging',
			hovered: false,
			values: 'on_packaging'
		},
		{
			label: 'On Delivery process',
			value: 'on_delivery',
			id: 'on-delivery',
			hovered: false,
			values: 'on_delivery'
		},
		{
			label: 'Delivered',
			value: 'delivered',
			id: 'delivered',
			hovered: false,
			values: 'delivered'
		}
  ]

  public shippingServices: any = [
    { label: 'no services', value: 'no-services', id: 'noshipping' },
    // {label: 'JNE', value: 'JNE', id:'jne'},
    { label: 'SAP', value: 'SAP', id: 'sap' },
    { label: 'Si Cepat', value: 'Si Cepat', id: 'sicepat' },
    // {label: 'Gojek', value: 'Gojek', id:'gojek'}
  ]

  public transactionStatus: any = [
    { label: 'PAID', value: 'PAID' },
		{ label: 'CANCEL', value: 'CANCEL' },
		{ label: 'WAITING FOR CONFIRMATION', value: 'WAITING FOR CONFIRMATION' }
  ];

  errorLabel: any = false;
  transaction_status: any;
  merchantMode = false;

  constructor(public orderhistoryService: OrderhistoryService, private router: Router,  private activatedRoute: ActivatedRoute, private programNameService: ProgramNameService,) { }

  ngOnInit() {
    this.firstLoad();

  }

  replaceVarian(value) {
    return JSON.stringify(value).replace('{','').replace('}','').replace(/[',]+/g, ', ').replace(/['"]+/g, '')
  }

  isShippingValueExists(value) {
    for (let element of this.detail.shipping_info) {
      console.log("value", value, element.label);
      if (element.label.trim().toLowerCase() == value.trim().toLowerCase()) { return [true, element.created_date] }
    }
    return false;
  }

  ngOnChange() {
    console.log("HELLO");
  }

  async firstLoad() {
    this.paramAppLabel = this.activatedRoute.snapshot.queryParamMap.get('app_label');
    if(!this.paramAppLabel) this.paramAppLabel = localStorage.getItem('programName');
    
    const program = localStorage.getItem('programName');
    let _this = this;
    this.contentList.filter(function(element) {
			if(element.appLabel == program) {
				if(element.type == "reguler") {
					_this.programType = "reguler";
				} else if(element.type == "custom_kontraktual") {
				  _this.programType = "custom_kontraktual";
				} else {
					_this.programType = "custom";
				}
			}
    });
    
    this.currentPermission = 'admin';
		this.orderHistoryDetail = this.detail;

    try {
      if (this.detail.billings) {
        let billings = this.detail.billings
        let additionalBillings = [];
        if (billings.discount) {
          additionalBillings.push({
            product_name: billings.discount.product_name,
            value: billings.discount.value
          })
        }

        if (this.detail.payment_detail.payment_via != 'manual banking') {
          if (billings.unique_amount) {
            delete billings.unique_amount;
          }
        }

        for (let prop in billings) {
          console.log("PROP", prop);
          if (prop == 'shipping_fee' || prop == 'unique_amount') {
            additionalBillings.push({
              product_name: prop,
              value: billings[prop]
            })
          }
        }
        console.log(this.detail.billings);
        this.detail.additionalBillings = additionalBillings
      }

      if (this.detail.merchant) {
        this.merchantMode = true;
      }
      if (!this.detail.shipping_services) {
        this.detail.shipping_services = 'no-services';
        // console.log("Result", shippingServices)
      }
      if (this.detail.status) {
        this.detail.status = this.detail.status.trim().toUpperCase();
      }

      if (this.detail.shipping_info) {
        console.log('this.detail.shipping_info', this.detail.shipping_info)
        this.shippingStatusList.forEach((element, index) => {
          let check = this.isShippingValueExists(element.value);
          if (check) {
            this.shippingStatusList[index].checked = check[0];
            this.shippingStatusList[index].created_date = check[1];
          }
          else {
            this.shippingStatusList[index].checked = false;
          }
        });
      }

      this.detail.previous_status = await this.detail.status;
      this.transactionStatus.forEach((element, index) => {
        if (element.value == this.detail.transaction_status) {
          this.transactionStatus[index].selected = 1;
        }
      });
    }
    catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

    // this.detail.previous_member_id = this.detail.member_id;
    // this.memberStatus.forEach((element, index) => {
    //     if(element.value == this.detail.member_status){
    //         this.memberStatus[index].selected = 1;
    //     }
    // });

  }

  getDetailProgram(value) {
    let detailProgram = this.contentProgram.find(element => element.appLabel == value);
    return detailProgram && detailProgram.title ? detailProgram.title : "";
  }

  backToDetail() {
    const programName = localStorage.getItem('programName');
    if(programName && programName != this.paramAppLabel) {
        Swal.fire({
          text: `Tab lain sedang mengakses program ${this.getDetailProgram(programName)}.`,
          icon: 'warning',
          confirmButtonText: `Tetap di ${this.getDetailProgram(this.paramAppLabel)}`,
          cancelButtonText: `Pindah ke ${this.getDetailProgram(programName)}`,
          cancelButtonColor:'#3086d6',
          showCancelButton: true,
        }).then((result) => {
            if(result.isConfirmed){
                this.programNameService.setData(this.paramAppLabel);
      this.back[1](this.back[0]);
            } else {
                this.programNameService.setData(programName);
      this.back[1](this.back[0]);
            }
        });
    } else if(this.paramAppLabel && !programName) {
      this.programNameService.setData(this.paramAppLabel);
      this.back[1](this.back[0]);
    } else {
      this.back[1](this.back[0]);
    }
  }

  changeshippingStatusList(index) {
    for (let n = 0; n <= index; n++) {
      //  console.log("HOVERED",  this.shippingStatusList[n].hovered)
      this.shippingStatusList[n].checked = true;
    }


    for (let n = index + 1; n < this.shippingStatusList.length; n++) {
      this.shippingStatusList[n].checked = false;
    }
  }

  hoverShippingStatusList(index, objectHover) {

    // console.log("HOVERED", index)

    for (let n = 0; n <= index; n++) {
      //  console.log("HOVERED",  this.shippingStatusList[n].hovered)
      this.shippingStatusList[n].hovered = true;
    }


    for (let n = index + 1; n < this.shippingStatusList.length; n++) {
      this.shippingStatusList[n].hovered = false;
    }

  }

  convertShippingStatusToShippingInfo() {

    let newData = []
    this.shippingStatusList.forEach((element, index) => {
      if (element.checked) {
        newData.push({ value: element.value })
      }

    });
    return newData;
  }
  async saveThis() {
    try {
      this.loading = !this.loading;
      let frm = JSON.parse(JSON.stringify(this.detail));
      frm.shipping_info = this.convertShippingStatusToShippingInfo();
      // this.detail.shipping_status = this.valu
      delete frm.buyer_detail
      delete frm.payment_expire_date
      delete frm.products
      await this.orderhistoryService.updateOrderHistoryAllHistory(frm);
      this.loading = !this.loading;
      this.done = true;
      alert("Edit submitted")
    }
    catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

      let message = this.errorLabel;
      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
      }
      Swal.fire({
        icon: 'error',
        title: message,
      });
    }
  }

  async cancelOrder(){
		if(!this.detail.order_id) return;
		Swal.fire({
			title: 'Confirmation',
			text: 'Apakah anda yakin ingin membatalkan order : '+this.detail.order_id,
			icon: 'error',
			confirmButtonText: 'Ok',
			cancelButtonText:"cancel",
			showCancelButton:true
		  }).then(async (result) => {
			if(result.isConfirmed){
				const payload = {
					order_id:this.detail.order_id
				}
				try {
					const result = await this.orderhistoryService.cancelOrder(payload);
					Swal.fire({
						title:"Cancel",
						text: 'Order : '+this.detail.order_id+' telah dibatalkan',
						icon: 'error',
						confirmButtonText: 'Ok',
						showCancelButton:false,
					}).then((result) => {
						if(result.isConfirmed){
							this.backToDetail();
					}});
				} catch (e) {
					this.errorLabel = ((<Error>e).message);

          let message = this.errorLabel;
          if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
          }
          Swal.fire({
            icon: 'error',
            title: message,
          });
				}
			}
		  });
	}

  async processOrder(){
		if(!this.detail.order_id) return;
		Swal.fire({
			title: 'Confirmation',
			text: 'Apakah anda yakin ingin memproses order : '+this.detail.order_id,
			icon: 'success',
			confirmButtonText: 'process',
			cancelButtonText:"cancel",
			showCancelButton:true
		  }).then(async (result) => {
			if(result.isConfirmed){
				const payload = {
					order_id:this.detail.order_id
				}
				try {
					const result = await this.orderhistoryService.processOrder(payload);
					Swal.fire({
						title:"Success",
						text: 'Order : '+this.detail.order_id+ ' telah disetujui',
						icon: 'success',
						confirmButtonText: 'Ok',
						showCancelButton:false,
					}).then((result) => {
						if(result.isConfirmed){
							this.backToDetail();
					}});
				} catch (e) {
					this.errorLabel = ((<Error>e).message);

          let message = this.errorLabel;
          if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
          }
          Swal.fire({
            icon: 'error',
            title: message,
          });
				}
			}
		});
	}
  
}
