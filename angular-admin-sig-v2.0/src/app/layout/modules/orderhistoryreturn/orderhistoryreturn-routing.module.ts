import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderhistoryreturnComponent } from './orderhistoryreturn.component';
import { OrderhistoryreturnDetailComponent } from './detail/orderhistoryreturn.detail.component';
import { OrderhistoryreturnEditComponent } from './edit/orderhistoryreturn.edit.component';

// import { OrderhistorysummaryDetailComponent } from './detail/orderhistorysummary.detail.component';
// import { OrderhistorysummaryEditComponent } from './edit/orderhistorysummary.edit.component';


const routes: Routes = [
  {
      path: '', component: OrderhistoryreturnComponent
  },
  // {
  //   path:'detail', component: OrderhistoryreturnDetailComponent
  // },
  // {
  //   path:'edit', component: OrderhistorysummaryEditComponent
  // }

  {
    path:'edit', component: OrderhistoryreturnDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderhistoryreturnRoutingModule { }
