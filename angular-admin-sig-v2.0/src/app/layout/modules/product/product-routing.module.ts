
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductComponent } from './product.component';
import { ProductAddComponent } from './add/product.add.component';
import { ProductAddBulkComponent } from './add-bulk/product.add-bulk.component';
import { ProductDetailComponent } from './detail/product.detail.component';
import { MerchantEditProductComponent } from '../../merchant-portal/separated-modules/merchant-products/products/merchant-edit-product/merchant-edit-product.component';
import { ProductEditComponent } from './edit/product.edit.component';

const routes: Routes = [
  {
      path: '', component: ProductComponent
  },
  {
    path:'add', component: ProductAddComponent
  },
  {
    path:'add-bulk', component: ProductAddBulkComponent
  },
  // {
  //   path:'detail', component: ProductDetailComponent
  // },
  {
    path: 'edit', component: ProductEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
