import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BannerComponent } from './banner.component';
import { BannerDetailComponent } from './detail/banner.detail.component';
import { BannerAddComponent } from './add/banner.add.component';


const routes: Routes = [
  {
      path: '', component: BannerComponent
  },
  {
    path:'detail', component: BannerDetailComponent
  },
  {
    path:'add', component: BannerAddComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BannerRoutingModule { }
