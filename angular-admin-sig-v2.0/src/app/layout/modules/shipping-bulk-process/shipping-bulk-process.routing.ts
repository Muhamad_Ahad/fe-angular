import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShippingBulkProcessComponent } from './shipping-bulk-process.component';

const routes: Routes = [
  {
      path: '', component: ShippingBulkProcessComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShippingBulkProcessComponentRoutingModule { }
