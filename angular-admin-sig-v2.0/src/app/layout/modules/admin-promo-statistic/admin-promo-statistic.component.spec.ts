import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPromoStatisticComponent } from './admin-promo-statistic.component';

describe('AdminPromoStatisticComponent', () => {
  let component: AdminPromoStatisticComponent;
  let fixture: ComponentFixture<AdminPromoStatisticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPromoStatisticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPromoStatisticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
