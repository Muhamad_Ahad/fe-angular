import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminPromoStatisticComponent } from './admin-promo-statistic.component'


const routes: Routes = [
   {
    path: '', component: AdminPromoStatisticComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminPromoStatisticRoutingModule { }
