import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MediaComponent } from './media.component';
// import { MemberAddComponent } from './add/member.add.component';
import { FormInputComponent }  from '../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from './../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { FormSelectComponent } from '../../../component-libs/form-select/form-select.component';
import { MediaRoutingModule } from './media-routing.module';

import { BsComponentModule } from '../bs-component/bs-component.module';
import { MediaDetailComponent } from './detail/media.detail.component';
import { MediaEditComponent } from './edit/media.edit.component';

@NgModule({
  imports: [CommonModule,
    MediaRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule
],
  
  declarations: [
    MediaComponent, MediaDetailComponent, MediaEditComponent]
})
export class MediaModule { }
