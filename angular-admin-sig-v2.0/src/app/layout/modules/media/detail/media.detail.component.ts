import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { MediaService } from '../../../../services/media/media.service';


@Component({
  selector: 'app-media-detail',
  templateUrl: './media.detail.component.html',
  styleUrls: ['./media.detail.component.scss'],
  animations: [routerTransition()]
})

export class MediaDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  edit:boolean = false;
  
  constructor(public mediaService:MediaService) {
  }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    
    
  }

  editThis(){
    console.log(this.edit );
    this.edit = !this.edit;
    console.log(this.edit );
  }
  backToTable(){
    console.log(this.back);
    this.back[1](this.back[0]);
  }

}
