import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { POBulkUpdateComponent } from './po-bulk-update.component';

const routes: Routes = [
  {
      path: '', component: POBulkUpdateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class POBulkUpdateComponentRoutingModule { }
