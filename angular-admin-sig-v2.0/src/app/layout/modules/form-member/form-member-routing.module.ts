import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormMemberComponent } from './form-member.component';
import { FormMemberAddComponent } from './add/form-member.add.component';
import { FormMemberAddBulkComponent } from './add-bulk/form-member.add-bulk.component';
import { FormMemberDetailComponent } from './detail/form-member.detail.component';
import { ListOrderComponent } from './list-order/list-order.component';

const routes: Routes = [
  {
      path: '', component: FormMemberComponent,

  },
  {
      path:'add', component: FormMemberAddComponent
  },
  {
    path:'add-bulk', component: FormMemberAddBulkComponent
  },
  {
    path:'detail', component: FormMemberDetailComponent
  },
  {
    path:'list-order', component: ListOrderComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataMemberRoutingModule { }
