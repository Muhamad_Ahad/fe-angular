import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormMemberComponent } from './form-member.component';
import { FormMemberAddComponent } from './add/form-member.add.component';
import { FormMemberAddBulkComponent } from './add-bulk/form-member.add-bulk.component';
import { FormInputComponent }  from '../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from '../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { DataMemberRoutingModule } from './form-member-routing.module';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { FormMemberDetailComponent } from './detail/form-member.detail.component';
import { FormMemberEditComponent } from './edit/form-member.edit.component';
import { ShowPasswordComponent } from './show-password/show-password.component';
import { AdditonalDocumentComponent } from './additional-document/additional-document.component';
import { EditAdditonalDocumentComponent } from './additional-document/edit/additional-document.edit.component';
import { RedeemLoginComponent } from './redeem-login/redeem-login.component';
import { ListOrderComponent } from './list-order/list-order.component';
import { EditListOrderComponent } from './list-order/edit/list-order.edit.component';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  imports: [CommonModule,
    DataMemberRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    AutocompleteLibModule,
    PdfViewerModule
  ],
  
  declarations: [
    FormMemberComponent, 
    FormMemberAddComponent,
    FormMemberAddBulkComponent,
    FormMemberDetailComponent, 
    FormMemberEditComponent,
    ShowPasswordComponent,
    AdditonalDocumentComponent,
    EditAdditonalDocumentComponent,
    RedeemLoginComponent,
    ListOrderComponent,
    EditListOrderComponent
  ],

  // exports : [MemberComponent]
})
export class FormMemberModule { }
