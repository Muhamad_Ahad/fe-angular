import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { NotificationService } from '../../../services/notification/notification.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
  selector: 'app-notificationsgroup',
  templateUrl: './notificationgroup.component.html',
  styleUrls: ['./notificationgroup.component.scss'],
  animations: [routerTransition()]
})

export class NotificationGroupComponent implements OnInit {

  NotificationGroup       : any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Active Notifications Group Detail',
                                  label_headers   : [
                                      {label: 'Notification Group ID', visible: true, type: 'string', data_row_name: '_id'},
                                    {label: 'Organization ID', visible: true, type: 'string', data_row_name: 'org_id'},
                                    {label: 'Name', visible: true, type: 'string', data_row_name: 'name'},
                                      // {label: 'Description', visible: false, type: 'string', data_row_name: 'description'},
                                      // {label: 'Topic', visible: false, type: 'string', data_row_name: 'topic'},
                                      {label: 'Status',
                                          options: [
                                            'active',
                                            'inactive',
                                          ],
                                          visible: true, type: 'list', data_row_name: 'status'},
                                      ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'NotificationGroup',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  form_input    : any = {};
  errorLabel : any = false;
  notificationgroupDetail: any = false;
  service     : any ;


  constructor(public notificationService:NotificationService) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    try {
      this.service    = this.notificationService;
      const result: any  = await this.notificationService.getNotificationGroupLint();
      this.NotificationGroup = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  public async callDetail(_id) {    
    try {
      this.service    = this.notificationService;
      let result: any = await this.notificationService.detailNotificationGroup(_id);
      this.notificationgroupDetail = result.result[0];
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  public async backToHere(obj) {
    obj.notificationgroupDetail = false;
  }

}
