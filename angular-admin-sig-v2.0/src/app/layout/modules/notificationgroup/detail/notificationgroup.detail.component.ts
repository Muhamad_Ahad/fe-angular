import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { NotificationService } from '../../../../services/notification/notification.service';

@Component({
  selector: 'app-notificationgroup-detail',
  templateUrl: './notificationgroup.detail.component.html',
  styleUrls: ['./notificationgroup.detail.component.scss'],
  animations: [routerTransition()]
})

export class NotificationGroupDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  MemberID      : any = [];
  table_title   : any = "Member ID List";
  table_headers : any = ["Member ID"];
  table_rows    : any = ["member_id"];
  row_id        : any = "_id";
  form_input    : any = {};
  formOptions   : any = {
                      check_box : true,
                      row_id    : this.row_id,
                      this      : this,
                      result_var_name : 'MemberID',
                      submit_function : [this, 'submit'],
                    }
  memberIDDetail: any = false;
  service     : any ;

  errorLabel    : any = false;
  edit:boolean = false;
  
  constructor(public notificationService:NotificationService) {
  }

  ngOnInit() {
    this.firstLoad();
    
  }

  public async submit(data){
    try{
      let result = await this.notificationService.updateTokenList(data, this.detail._id);
    }
    catch (e){
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  async firstLoad(){
    try {
      this.service    = this.notificationService;
      const result: any  = await this.notificationService.getMemberIDLint();
      this.MemberID = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }    
  }

  editThis(){
    console.log(this.edit );
    this.edit = !this.edit;
    console.log(this.edit );
  }
  backToTable(){
    console.log(this.back);
    this.back[1](this.back[0]);
  }

  // async deleteThis(){
  //   try{
  //     let delResult: any = await this.notificationService.delete(this.detail);
  //     //console.log(delResult);
  //     if(delResult.error==false){
  //         console.log(this.back[0]);
  //         this.back[0].notificationDetail=false;
  //         this.back[0].firstLoad();
  //         // delete this.back[0].notificationDetail;
  //     } 
  //   } catch (e) {
  //       this.errorLabel = ((<Error>e).message);//conversion to Error type
  //   }      
    
  // }
}
