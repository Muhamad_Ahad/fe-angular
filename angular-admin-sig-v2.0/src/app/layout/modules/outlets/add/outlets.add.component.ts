import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { OutletsService } from '../../../../services/outlets/outlets.service';

@Component({
  selector: 'app-moutlet-add',
  templateUrl: './outlets.add.component.html',
  styleUrls: ['./outlets.add.component.scss'],
  animations: [routerTransition()]
})

export class OutletsAddComponent implements OnInit {
  public name: string = "";
  @Input() public back;
  terminal_id: any;
  Outlets: any = [];
  service: any;
  errorLabel: any = false;
  outlet_name: any;
  outlet_code: any;
  outlet_type: any;
  id: any;
  org_id: any;
  contact_person: any;
  email: any;
  work_phone: any;
  cell_phone: any;
  fax_phone: any;
  status: any;
  support_no: any;
  base_url: any;
  city: any;
  state: any;
  country: any;
  address1: any;
  address2: any;
  address3: any;
  postal_code: any;
  pin_number: any;



  public outletStatus: any = [
    { label: 'ACTIVE', value: 'ACTIVE' },
    { label: 'INACTIVE', value: 'INACTIVE' }
  ];

  constructor(public outletsService: OutletsService) {
    let form_add: any = [
      { label: "Terminal ID", type: "text", value: "", data_binding: 'terminal_id' },
      { label: "Outlet Name", type: "text", value: "", data_binding: 'outlet_name' },
      { label: "Outlet Code", type: "text", value: "", data_binding: 'outlet_code' },
      { label: "Outlet Type", type: "text", value: "", data_binding: 'outlet_type' },
      { label: "Outlet ID", type: "text", value: "", data_binding: 'id' },
      { label: "Merchant ID", type: "text", value: "", data_binding: 'merchant_id' },
      { label: "Organization ID", type: "text", value: "", data_binding: 'org_id' },
      { label: "Contact Person", type: "text", value: "", data_binding: 'contact_person' },
      { label: "Email", type: "text", value: "", data_binding: 'email' },
      { label: "Work Phone", type: "text", value: "", data_binding: 'work_phone' },
      { label: "Cell Phone", type: "text", value: "", data_binding: 'cell_phone' },
      { label: "Fax Phone", type: "text", value: "", data_binding: 'fax_phone' },
      { label: "PIN Number", type: "password", value: "", data_binding: 'pin_number' },
      { label: "Status", type: "text", value: "", data_binding: 'status' },
      { label: "Support No", type: "text", value: "", data_binding: 'support_no' },
      { label: "URL ", type: "text", value: "", data_binding: 'base_url' },
      { label: "City", type: "text", value: "", data_binding: 'city' },
      { label: "State", type: "text", value: "", data_binding: 'state' },
      { label: "Country", type: "text", value: "", data_binding: 'country' },
      { label: "Address1", type: "textarea", value: "", data_binding: 'address1' },
      { label: "Address2", type: "textarea", value: "", data_binding: 'address2' },
      { label: "Address3", type: "textarea", value: "", data_binding: 'address3' },
      { label: "Postal Code", type: "text", value: "", data_binding: 'postal_code' },
    ];
  }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    // this.service = this.memberService;
    // let result: any = await this.memberService.getMembersLint();
    // this.Members = result.result;

  }

  async getKeyname() {

  }

  getRegionList($event) {

  }

  formSubmitAddOutlets(form) {
    // console.log(form);
    // console.log(JSON.stringify(form));

    try {
      this.service = this.outletsService;
      const result: any = this.outletsService.addMerchant(form);
      this.Outlets = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  backTo(){
    window.history.back();
  }


}