import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PointMovementOrderComponent } from './point-movement-order.component'
// import { BastComponent} from './bast/bast.component'


const routes: Routes = [
  {
    path: '', component: PointMovementOrderComponent
},
// {
//   path:'bast', component: BastComponent
// }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PointMovementOrderRoutingModule { }
