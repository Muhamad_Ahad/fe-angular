import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PointMovementOrderComponent } from './point-movement-order.component';

describe('PointMovementOrderComponent', () => {
  let component: PointMovementOrderComponent;
  let fixture: ComponentFixture<PointMovementOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PointMovementOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointMovementOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
