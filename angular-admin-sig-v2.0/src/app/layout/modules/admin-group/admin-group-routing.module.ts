import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminGroupComponent } from './admin-group.component';
import { AdminGroupAddComponent } from './add/admin-group.add.component';
import { AdminGroupDetailComponent } from './detail/admin-group.detail.component';
// import { UserPointHistoryComponent } from './detail/member-point-history/member-point-history.component';

const routes: Routes = [
  {
      path: '', component: AdminGroupComponent,

  },
  {
      path:'add', component: AdminGroupAddComponent
  },
  {
    path:'detail', component: AdminGroupDetailComponent
},
  

    // { path: 'detail', loadChildren: () => import(`./detail/member.detail-routing.module`).then(m => m.MemberDetailRoutingModule) },
    // {
    //   path:'point-history', component: MemberPointHistoryComponent
    // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
