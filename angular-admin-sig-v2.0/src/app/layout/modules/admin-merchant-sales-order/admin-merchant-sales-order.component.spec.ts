import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMerchantSalesOrderComponent } from './admin-merchant-sales-order.component';

describe('AdminMerchantSalesOrderComponent', () => {
  let component: AdminMerchantSalesOrderComponent;
  let fixture: ComponentFixture<AdminMerchantSalesOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminMerchantSalesOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminMerchantSalesOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
