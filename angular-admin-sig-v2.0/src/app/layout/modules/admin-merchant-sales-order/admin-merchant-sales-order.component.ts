import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admin-merchant-sales-order',
  templateUrl: './admin-merchant-sales-order.component.html',
  styleUrls: ['./admin-merchant-sales-order.component.scss']
})
export class AdminMerchantSalesOrderComponent implements OnInit {

  swaper = true;
  Pointstransaction: any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Sales Order Report Page (Product)',
                                  label_headers   : [
                                    { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
                                    { label: 'Buyer Name', visible: true, type: 'string', data_row_name: 'buyer_name' },
                                    { label: 'Product Name', visible: true, type: 'string', data_row_name: 'product_name' },
                                    { label: 'Quantity', visible: true, type: 'string', data_row_name: 'total_quantity' },
                                     { label: 'Shipping Fee', visible: true, type: 'string', data_row_name: 'shipping_fee' },
                                    { label: 'Total Product Price', visible: true, type: 'string', data_row_name: 'total_product_price' },
                                    // { label: 'Discount Price', visible: false, type: 'string', data_row_name: 'discount_price' },
                                    { label: 'Sum Total', visible: false, type: 'string', data_row_name: 'sum_total' },
                                    { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
                                    // { label: 'Email', visible: false, type: 'string', data_row_name: 'user_email' },
                                    // { label: 'Order Date', visible: false, type: 'string', data_row_name: 'order_date' },
                                    // { label: 'Username', visible: false, type: 'string', data_row_name: 'username' },
                                    // { label: 'Payment Method', visible: false, type: 'string', data_row_name: 'payment_method' },
                            
                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: 'invoice_id',
                                                    this  : this,
                                                    result_var_name: 'Pointstransaction',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };

  form_input    : any = {};
  errorLabel : any = false;
  totalPage  = 0;
  merchantUsername : any;
  pointstransactionDetail: any = false;
  service: any;
  constructor(public OrderhistoryService:OrderhistoryService,  private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.merchantUsername = params.merchant_username;
      console.log("params ",params)
    
    })
    this.firstLoad();

  }
  
  async firstLoad(){
    try{
      var params = {
         search: {based_on: "products"},
         current_page:1,
         limit_per_page:50
        }
        await localStorage.setItem('merchantsales',this.merchantUsername)
      this.service    = this.OrderhistoryService;
      let result:any = {}
      result = await this.OrderhistoryService.getSalesOrderMerchantInt(this.merchantUsername);
        console.log(result)
    
      this.totalPage = result.result.total_page
      this.Pointstransaction = result.result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }
  swapClick(bool) {
    this.swaper = bool;
    this.firstLoad();
  }
  public async callDetail(orderhistory_id) {
    this.router.navigate(['administrator/orderhistoryallhistoryadmin/edit'],  {queryParams: {id: orderhistory_id }})
    // try {
    //   let result: any;
    //   this.service = this.orderhistoryService;
    //   result       = await this.orderhistoryService.detailOrderhistoryallhistory(orderhistory_id);
    //   // console.log(result);
    //   this.orderhistoryallhistoryDetail = result.result[0];

    // } catch (e) {
    //   this.errorLabel = ((<Error>e).message); // conversion to Error type
    // }
  }

  public async backToHere(obj){
    obj.pointstransactionDetail = false;
  }
  addMonths(date, months) {
    date.setMonth(date.getMonth() + months);
    return date;
  }
}

