import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromoCampaignDetailComponent } from './promo-campaign-detail.component';

describe('PromoCampaignDetailComponent', () => {
  let component: PromoCampaignDetailComponent;
  let fixture: ComponentFixture<PromoCampaignDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromoCampaignDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromoCampaignDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
