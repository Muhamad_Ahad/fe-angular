import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PromoCampaignComponent } from './promo-campaign.component';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { PromoCampaignAddComponent } from './add/promo-campaign-add.component';
import { PromoCampaignEditComponent } from './edit/promo-campaign-edit.component';
import { PromoCampaignDetailComponent } from './detail/promo-campaign-detail.component';
import { PromoCampaignRoutingModule } from './promo-campaign-routing.module';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { PageHeaderModule } from './../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatCheckboxModule} from '@angular/material/checkbox';

@NgModule({
  imports: [
    CommonModule,
    FormBuilderTableModule,
    PromoCampaignRoutingModule,
    BsComponentModule,
    NgbModule,
    ReactiveFormsModule,
    PageHeaderModule, 
    FormsModule,
    MatCheckboxModule
  ],
  declarations: [PromoCampaignComponent, PromoCampaignAddComponent, PromoCampaignEditComponent, PromoCampaignDetailComponent],
})
export class PromoCampaignModule { }
