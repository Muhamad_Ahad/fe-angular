
import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { ShoppingcartService } from '../../../services/shoppingcart/shoppingcart.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
  selector: 'app-shoppingcart',
  templateUrl: './shoppingcart.component.html',
  styleUrls: ['./shoppingcart.component.scss'],
  animations: [routerTransition()]
})

export class ShoppingcartComponent implements OnInit {

  Shoppingcart: any = [];
  tableFormat: TableFormat = {
        title: 'Shopping Cart Detail Page',
        label_headers: [
            { label: 'ID', visible: true, type: 'string', data_row_name: '_id' },
            { label: 'Shopping Cart ID', visible: true, type: 'string', data_row_name: 'shoppingcart_id' },
            { label: 'Name', visible: true, type: 'string', data_row_name: 'name' },
            // { label: 'Color', visible: false, type: 'string', data_row_name: 'color' },
            { label: 'User ID', visible: true, type: 'string', data_row_name: 'user_id' },
            { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
            // { label: 'Total Price', visible: false, type: 'number', data_row_name: 'total_price' },
            // { label: 'Total Quantity', visible: false, type: 'number', data_row_name: 'total_quantity' },
        ],
        row_primary_key: '_id',
        formOptions: {
            row_id: '_id',
            this: this,
            result_var_name: 'Shoppingcart',
            detail_function: [this, 'callDetail']
        }
    };


  row_id: any = "status";
  errorLabel : any = false;

  shoppingcartDetail: any=false;
  service: any;
  constructor(public shoppingcartService:ShoppingcartService) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad(){
    try{
      this.service    = this.shoppingcartService;
      let result: any  = await this.shoppingcartService.getShoppingcartLint();

      this.Shoppingcart = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async callDetail(status)
  {
      try{
      let result: any;
      this.service    = this.shoppingcartService;
      result          = await this.shoppingcartService.detailShoppingcart(status);
      if(!result.error && result.error == false){
      this.shoppingcartDetail = result.result[0];
      // this.shoppingcartDetail = result.result.products[0];
      }
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj){
    obj.shoppingcartDetail = false;
  }

}
