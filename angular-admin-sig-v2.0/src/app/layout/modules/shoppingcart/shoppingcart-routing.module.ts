import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShoppingcartComponent } from './shoppingcart.component';
import { ShoppingcartDetailComponent } from './detail/shoppingcart.detail.component';


const routes: Routes = [
  {
    path: '', component: ShoppingcartComponent
  },
  {
    path: 'detail', component: ShoppingcartDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShoppingcartRoutingModule { }
