import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { ArticleService } from '../../../../services/article/article.service';
import { del } from 'selenium-webdriver/http';


@Component({
  selector: 'app-article-detail',
  templateUrl: './article.detail.component.html',
  styleUrls: ['./article.detail.component.scss'],
  animations: [routerTransition()]
})

export class ArticleDetailComponent implements OnInit {
  @Input() public detail:any;
  @Input() public back;

  edit:boolean = false;
  errorLabel    :any = false;
  constructor(public articleService:ArticleService) {
  }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    
    
  }

  editThis(){
    this.edit = !this.edit;
  }

  backToTable(){
    this.back[1](this.back[0]);
  }

  async deleteThis(){
    try{
      let delResult:any = await this.articleService.deleteArticle(this.detail);
      console.log(delResult);
      if(delResult.error==false){
          //console.log(this.back[0]);
          this.back[0].articleDetail=false;
          this.back[0].firstLoad();
          // delete this.back[0].prodDetail;
      } 
    } catch (e) {
        this.errorLabel = ((<Error>e).message);//conversion to Error type
    }      
    
  }
  
}
