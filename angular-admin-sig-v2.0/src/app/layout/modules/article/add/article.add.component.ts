import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { ArticleService } from '../../../../services/article/article.service';

@Component({
  selector: 'app-article-add',
  templateUrl: './article.add.component.html',
  styleUrls: ['./article.add.component.scss'],
  animations: [routerTransition()]
})

export class ArticleAddComponent implements OnInit {
  public name:string = "";
  Article:any = [];
  service:any;
  title:any;
  description:any;
  status:any;
  org_id:any;
  
  public product_type = [
    {label:"ACTIVE", value:"ACTIVE", selected:1}
   ,{label:"INACTIVE", value:"INACTIVE"}
 ];
 selectedFile  = null;
  progressBar   :number= 0;
  cancel        :boolean = false;
  errorFile     :any = false;
  errorLabel    :any = false;
  
  constructor(public articleService:ArticleService) {
    let form_add     :any = [
      { label:"Title",  type: "text",  value: "", data_binding: 'title'  },
      { label:"Description",  type: "text",  value: "", data_binding: 'description'  },
      { label:"Status",  type: "text",  value: "", data_binding: 'status' },
      { label:"Organization ID",  type: "text",  value: "", data_binding: 'org_id' },
    ];
   }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    // this.service = this.memberService;
    // let result:any = await this.memberService.getMembersLint();
    // this.Members = result.result;
    
  }

  async formSubmitAddArticle(form) {
    // console.log(JSON.stringify(form));
    
    try 
    {
      this.service    = this.articleService;
      //console.log(form);
      let result:any  = await this.articleService.addArticleLint(form);
      //console.log(result);
      this.Article = result.result;
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
    
  }

  updateProgressBar(value){
    this.progressBar = value;
  }

  cancelThis(){
    this.cancel = !this.cancel;
  }

}
