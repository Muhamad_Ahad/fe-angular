import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';
import * as content from '../../../../assets/json/content.json';

@Component({
  selector: 'app-admin-stock',
  templateUrl: './admin-stock.component.html',
  styleUrls: ['./admin-stock.component.scss']
})
export class AdminStockComponent implements OnInit {

  SalesRedemption: any = [];
  swaper = true;
  tableFormat        : TableFormat = {
                                  title           : 'Sales Redemption report',
                                  label_headers   : [
                                    {label: 'Tanggal Order', visible: true, type: 'date', data_row_name: 'request_date'},
                                    {label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id'},
                                    {label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko'},
                                    // {label: 'Member', visible: true, type: 'string', data_row_name: 'member_name'},
                                    {label: 'Nama Pemilik', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail'},
                                    {label: 'Nama Penerima', visible: true, type: 'form_nama_penerima', data_row_name: 'member_detail'},
                                    {label: 'Nama Toko', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail'},
                                    {label: 'Group', visible: true, type: 'form_group', data_row_name: 'member_detail'},
                                    {label: 'Alamat Kirim', visible: true, type: 'form_alamat_penerima', data_row_name: 'member_detail'},
                                    {label: 'No WA Penerima', visible: true, type: 'form_wa_penerima', data_row_name: 'member_detail'},
                                    {label: 'Hadiah Dikuasakan', visible: true, type: 'form_hadiah_dikuasakan', data_row_name: 'member_detail'},
                                    {label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products'},
                                    // {label: 'Points', visible: true, type: 'string', data_row_name: 'points'},
                                    {label: 'Hadiah Redeem', visible: true, type: 'product_redeem', data_row_name: 'products'},
                                    {label: 'Product Code', visible: true, type: 'product_code_redeem', data_row_name: 'products'},
                                    {label: 'Points Redeem', visible: true, type: 'string', data_row_name: 'sum_total'},
                                    // {label: 'Updated Date', visible: true, type: 'date', data_row_name: 'updated_date'},
                                    {label: 'Status Order', visible: true, type: 'string', data_row_name: 'status'},
                                    {label: 'Status Pengiriman', visible: true, type: 'last_shipping_info', data_row_name: 'delivery_detail'},
                                    {label: 'Tanggal Kirim', visible: true, type: 'delivery_date', data_row_name: 'delivery_detail'},
                                    {label: 'Tanggal Terima', visible: true, type: 'delivered_date', data_row_name: 'delivery_detail'},
                                    {label: 'Nama Penerima Langsung', visible: true, type: 'receiver_name_sap', data_row_name: 'delivery_detail'},
                                    {label: 'Status Penerima', visible: true, type: 'relation_name', data_row_name: 'delivery_detail'},
                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'SalesRedemption',
                                                    detail_function: [this, 'callDetail'],
                                                    salesRedemption : true,
                                                  },
                                                  show_checkbox_options: true
  };
  tableFormat2        : TableFormat = {
    title           : 'Sales Redemption report',
    label_headers   : [
      {label: 'Tanggal Order', visible: true, type: 'date', data_row_name: 'request_date'},
      {label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id'},
      {label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko'},
      {label: 'Nama Penerima', visible: true, type: 'form_nama_penerima', data_row_name: 'member_detail'},
      {label: 'No WA Penerima', visible: true, type: 'form_wa_penerima', data_row_name: 'member_detail'},
      {label: 'Alamat Kirim', visible: true, type: 'form_alamat_penerima', data_row_name: 'member_detail'},
      {label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products'},
      {label: 'Hadiah Redeem', visible: true, type: 'product_redeem', data_row_name: 'products'},
      {label: 'Product Code', visible: true, type: 'product_code_redeem', data_row_name: 'products'},
      {label: 'Points Redeem', visible: true, type: 'string', data_row_name: 'sum_total'},
      {label: 'Program', visible: true, type: 'form_group', data_row_name: 'member_detail'},
      {label: 'Status Order', visible: true, type: 'string', data_row_name: 'status'},
      {label: 'Status Pengiriman', visible: true, type: 'last_shipping_info', data_row_name: 'delivery_detail'},
      {label: 'Tanggal Kirim', visible: true, type: 'delivery_date', data_row_name: 'delivery_detail'},
      {label: 'Tanggal Terima', visible: true, type: 'delivered_date', data_row_name: 'delivery_detail'},
      {label: 'Nama Penerima Langsung', visible: true, type: 'receiver_name_sap', data_row_name: 'delivery_detail'},
      {label: 'Status Penerima', visible: true, type: 'relation_name', data_row_name: 'delivery_detail'},
    ],
    row_primary_key : '_id',
    formOptions     : {
                      row_id: '_id',
                      this  : this,
                      result_var_name: 'SalesRedemption',
                      detail_function: [this, 'callDetail'],
                      salesRedemption : true,
                    },
                    show_checkbox_options: true
};
tableFormat3        : TableFormat = {
  title           : 'Sales Redemption report',
  label_headers   : [
    {label: 'Tanggal Order', visible: true, type: 'date', data_row_name: 'request_date'},
    {label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id'},
    {label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko'},
    { label: 'Nama Program', visible: true, type: 'form_cluster', data_row_name: 'member_detail' },
    // {label: 'Member', visible: true, type: 'string', data_row_name: 'member_name'},
    {label: 'Nama PIC', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail'},
    {label: 'Nama Penerima', visible: true, type: 'form_nama_penerima', data_row_name: 'member_detail'},
    {label: 'Nama Entitas', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail'},
    // {label: 'Group', visible: true, type: 'form_group', data_row_name: 'member_detail'},
    {label: 'Alamat Kirim', visible: true, type: 'form_alamat_penerima', data_row_name: 'member_detail'},
    {label: 'No WA Penerima', visible: true, type: 'form_wa_penerima', data_row_name: 'member_detail'},
    {label: 'Hadiah Dikuasakan', visible: true, type: 'form_hadiah_dikuasakan', data_row_name: 'member_detail'},
    {label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products'},
    // {label: 'Points', visible: true, type: 'string', data_row_name: 'points'},
    {label: 'Hadiah Redeem', visible: true, type: 'product_redeem', data_row_name: 'products'},
    {label: 'Product Code', visible: true, type: 'product_code_redeem', data_row_name: 'products'},
    {label: 'Points Redeem', visible: true, type: 'string', data_row_name: 'sum_total'},
    {label: 'Status Order', visible: true, type: 'string', data_row_name: 'status'},
    // {label: 'Updated Date', visible: true, type: 'date', data_row_name: 'updated_date'},
    {label: 'Status Pengiriman', visible: true, type: 'last_shipping_info', data_row_name: 'delivery_detail'},
    {label: 'Tanggal Kirim', visible: true, type: 'delivery_date', data_row_name: 'delivery_detail'},
    {label: 'Tanggal Terima', visible: true, type: 'delivered_date', data_row_name: 'delivery_detail'},
    {label: 'Nama Penerima Langsung', visible: true, type: 'receiver_name_sap', data_row_name: 'delivery_detail'},
    {label: 'Status Penerima', visible: true, type: 'relation_name', data_row_name: 'delivery_detail'},
  ],
  row_primary_key : '_id',
  formOptions     : {
                    row_id: '_id',
                    this  : this,
                    result_var_name: 'SalesRedemption',
                    detail_function: [this, 'callDetail'],
                    salesRedemption : true,
                  },
                  show_checkbox_options: true
};
  public contentList : any = (content as any).default;
  form_input    : any = {};
  errorLabel : any = false;
  totalPage  = 0;

  SalesRedemptionDetail: any = false;
  mci_project: any = false;
  service: any;
  programType: any = "";
  constructor(public OrderhistoryService:OrderhistoryService) { }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad(){
    try{
      var params = {
        search : {
          type: {
            in:["product","voucher"]
          }
          // active : '1'
        },
        limit_per_page : 50,
        current_page: 1,
        order_by: {request_date:-1}
      }

      const program = localStorage.getItem('programName');
      let _this = this;
      this.contentList.filter(function(element) {
        if(element.appLabel == program) {
            if(element.type == "reguler") {
                _this.mci_project = true;
                _this.programType = "reguler";
            } else if(element.type == "custom_kontraktual") {
              _this.mci_project = false;
              _this.programType = "custom_kontraktual";
            } else {
                _this.mci_project = false;
                _this.programType = "custom";
            }
        }
      });
      
      this.service    = this.OrderhistoryService;
      let result: any;

      if (this.swaper == true){
        result = await this.OrderhistoryService.getRedemptionReportint();
      } else{
        result = await this.OrderhistoryService.getRedemptionCanceledReportint();
      }

      this.totalPage = result.total_page;
      this.SalesRedemption = result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }
  
  public async callDetail(SalesRedemption_id){
    try{
      // let result: any;
      // this.service    = this.OrderhistoryService;
      // result          = await this.OrderhistoryService.detailPointstransaction(SalesRedemption_id);
      // console.log(result);

      // this.SalesRedemptionDetail = result.result[0];

    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj){
    obj.SalesRedemptionDetail = false;
  }

  addMonths(date, months) {
    date.setMonth(date.getMonth() + months);
    return date;
  }

  swapClick(bool) {
    this.swaper = bool;
    this.firstLoad();
  }
}
