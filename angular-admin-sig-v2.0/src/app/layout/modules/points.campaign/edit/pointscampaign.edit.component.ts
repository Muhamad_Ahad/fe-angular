import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { PointsCampaignService } from '../../../../services/points.campaign/pointscampaign.service';


@Component({
  selector: 'app-points-campaign-edit',
  templateUrl: './pointscampaign.edit.component.html',
  styleUrls: ['./pointscampaign.edit.component.scss'],
  animations: [routerTransition()]
})

export class PointsCampaignEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;
  @Input() public merchantList: any;
  errorLabel : any = false;
  merchant_id: any;
  status     : any;

  public loading        : boolean = false;
  public campaignStatus: any = [
    {label: 'ACTIVE', value: 'ACTIVE'},
    {label: 'INACTIVE', value: 'INACTIVE'}
  ];

  constructor(public pointscampaignService: PointsCampaignService) {
  }

  ngOnInit() {
    this.firstLoad();
  }

  firstLoad() {
    this.detail.previous_status = this.detail.status;
    this.campaignStatus.forEach((element, index) => {
        if (element.value == this.detail.status) {
            this.campaignStatus[index].selected = 1;
        }
    });

    this.detail.previous_merchant_id = this.detail.merchant_id;
    this.merchantList.forEach((element, index) => {
        if(element.value == this.detail.merchant_id){
            this.merchantList[index].selected = 1;
        }
    });

  }

  backToDetail() {
    // console.log(this.back);
    this.back[0][this.back[1]]();
  }

  async saveThis() {
    try {
      this.loading = !this.loading;
      await this.pointscampaignService.updatePointsCampaignID(this.detail);
      this.loading = !this.loading;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

}
