import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PointsCampaignComponent } from './pointscampaign.component';
import { PointsCampaignAddComponent } from './add/pointscampaign.add.component';
import { FormInputComponent }  from '../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from './../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { FormSelectComponent } from '../../../component-libs/form-select/form-select.component';
import { PointsCampaignDetailComponent } from './detail/pointscampaign.detail.component';
import { PointsCampaignRoutingModule } from './pointscampaign-routing.module';
import { PointsCampaignEditComponent } from './edit/pointscampaign.edit.component';
import { BsComponentModule } from '../bs-component/bs-component.module';

@NgModule({
  imports: [CommonModule,
    PointsCampaignRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule
],
  
  declarations: [
    PointsCampaignComponent, PointsCampaignAddComponent, PointsCampaignDetailComponent,PointsCampaignEditComponent]
})
export class PointsCampaignModule { }
