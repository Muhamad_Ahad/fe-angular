import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderhistoryRoutingModule } from './orderhistory-routing.module';
import { OrderhistoryComponent } from './orderhistory.component';
import { PageHeaderModule } from './../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { OrderhistoryDetailComponent } from './detail/orderhistory.detail.component';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { OrderhistoryEditComponent } from './edit/orderhistory.edit.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
@NgModule({
  imports: [
    CommonModule, 
    OrderhistoryRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    MatCheckboxModule,
    FormBuilderTableModule,
    BsComponentModule],
  declarations: [OrderhistoryComponent, OrderhistoryDetailComponent, OrderhistoryEditComponent,]
})
export class OrderhistoryModule { }
