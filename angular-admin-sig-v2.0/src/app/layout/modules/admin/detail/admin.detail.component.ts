import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { AdminService } from '../../../../services/admin/admin.service';
import { del } from 'selenium-webdriver/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admin-detail',
  templateUrl: './admin.detail.component.html',
  styleUrls: ['./admin.detail.component.scss'],
  animations: [routerTransition()]
})

export class AdminDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  edit: boolean = false;
  isChangePassword: boolean = false;
  errorLabel : any = false;
  adminDetail:any;
  constructor(public adminService: AdminService,private router: Router,private route: ActivatedRoute) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad(){
    let _id = this.detail._id;
    try {
      const result : any= await this.adminService.getAdminByID(_id);
      if(result.length > 0){
        if(result[0]._id == this.detail._id){
          this.adminDetail = result[0];
          // console.log("data member from firstload detail", this.memberDetail);
        }
      }
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  editThis() {
    this.edit = !this.edit;
    if(this.edit == false){
      this.firstLoad();
    }
  }

  changePasswordThis() {
    this.isChangePassword = !this.isChangePassword;
    if(this.isChangePassword == false){
      this.firstLoad();
    }
  }

  changePassword() {
    this.isChangePassword = !this.isChangePassword;
  }

  backToTable() {
    // console.log(this.back);
    this.back[1](this.back[0]);
  }

}
