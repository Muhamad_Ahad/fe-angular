"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.MemberRoutingModule = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var member_component_1 = require("./admin.component");
var member_add_component_1 = require("./add/member.add.component");
var member_detail_component_1 = require("./detail/member.detail.component");
var member_point_history_component_1 = require("./detail/member-point-history/member-point-history.component");
var routes = [
    {
        path: '', component: member_component_1.MemberComponent
    },
    {
        path: 'add', component: member_add_component_1.MemberAddComponent
    },
    {
        path: 'detail', component: member_detail_component_1.MemberDetailComponent
    },
    // { path: 'detail', loadChildren: () => import(`./detail/member.detail-routing.module`).then(m => m.MemberDetailRoutingModule) },
    {
        path: 'point-history', component: member_point_history_component_1.MemberPointHistoryComponent
    }
];
var MemberRoutingModule = /** @class */ (function () {
    function MemberRoutingModule() {
    }
    MemberRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], MemberRoutingModule);
    return MemberRoutingModule;
}());
exports.MemberRoutingModule = MemberRoutingModule;
