"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.MemberComponent = void 0;
var core_1 = require("@angular/core");
var router_animations_1 = require("../../../router.animations");
// import { Http, Response, Headers, RequestOptions, RequestMethod, RequestOptionsArgs, URLSearchParams, ResponseContentType } from '@angular/http';
// import { Observable } from 'rxjs';
// import { map } from "rxjs/operators";
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';
// import 'rxjs/Rx' ;
var MemberComponent = /** @class */ (function () {
    // private options = new RequestOptions(
    //   { headers: new Headers({ 'Content-Type': 'application/json' }) });
    function MemberComponent(memberService, sanitizer) {
        this.memberService = memberService;
        this.sanitizer = sanitizer;
        this.Members = [];
        this.row_id = "_id";
        this.tableFormat = {
            title: 'Active Members Detail',
            label_headers: [
                { label: 'Name', visible: true, type: 'string', data_row_name: 'full_name' },
                { label: 'Email', visible: true, type: 'string', data_row_name: 'email' },
                { label: 'Birthdate', visible: true, type: 'date', data_row_name: 'dob' },
                { label: 'Phone Number', visible: true, type: 'string', data_row_name: 'cell_phone' },
                { label: 'Gender', visible: true, type: 'string', data_row_name: 'gender' },
                { label: 'Address', visible: true, type: 'string', data_row_name: 'address' },
                { label: 'Membership', visible: true, type: 'string', data_row_name: 'membership' },
                { label: 'Points', visible: true, type: 'string', data_row_name: 'points' },
                { label: 'Join Date', visible: true, type: 'date', data_row_name: 'created_date' },
            ],
            row_primary_key: '_id',
            formOptions: {
                addForm: true,
                row_id: this.row_id,
                "this": this,
                result_var_name: 'Members',
                detail_function: [this, 'callDetail']
            }
        };
        this.form_input = {};
        this.errorLabel = false;
        this.memberDetail = false;
        this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
    }
    MemberComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    MemberComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        // let params = {
                        //   'type':'member'
                        // }
                        this.service = this.memberService;
                        return [4 /*yield*/, this.memberService.getMemberLint()];
                    case 1:
                        result = _a.sent();
                        this.totalPage = result.result.total_page;
                        this.Members = result.result.values;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MemberComponent.prototype.callDetail = function (_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.memberService;
                        return [4 /*yield*/, this.memberService.detailMember(_id)];
                    case 1:
                        result = _a.sent();
                        this.memberDetail = result.result[0];
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MemberComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.memberDetail = false;
                return [2 /*return*/];
            });
        });
    };
    MemberComponent.prototype.onDownload = function (downloadLint) {
        var srcDownload = downloadLint;
        //let srcDownload = "http://localhost:8888/f3/assets/csv/members_report.csv";
        this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl(srcDownload);
        //console.log(this.srcDownload);
        this.getDownloadFileLint();
    };
    MemberComponent.prototype.getDownloadFileLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, downloadLint, e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        result = void 0;
                        this.service = this.memberService;
                        return [4 /*yield*/, this.memberService.getDownloadFileLint()];
                    case 1:
                        result = _a.sent();
                        downloadLint = result.result;
                        //console.log(downloadLint);
                        //To running other subfunction with together automatically
                        this.onDownload(downloadLint);
                        return [3 /*break*/, 3];
                    case 2:
                        e_3 = _a.sent();
                        this.errorLabel = (e_3.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MemberComponent = __decorate([
        core_1.Component({
            selector: 'app-member',
            templateUrl: './member.component.html',
            styleUrls: ['./member.component.scss'],
            animations: [router_animations_1.routerTransition()]
        })
    ], MemberComponent);
    return MemberComponent;
}());
exports.MemberComponent = MemberComponent;
