import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderhistorycompleteRoutingModule } from './orderhistorycomplete-routing.module';
import { OrderhistorycompleteComponent } from './orderhistorycomplete.component';
import { PageHeaderModule } from '../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { OrderhistorycompleteDetailComponent } from './detail/orderhistorycomplete.detail.component';
import { OrderhistorycompleteEditComponent } from './edit/orderhistorycomplete.edit.component';
import { NgxBarcodeModule } from 'ngx-barcode';
import {MatButtonModule} from '@angular/material/button';
import {MatRadioModule} from '@angular/material/radio';


@NgModule({
  imports: [
    CommonModule, 
    OrderhistorycompleteRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    NgxBarcodeModule,
    MatButtonModule,
    MatRadioModule
  ],
  declarations: [
    OrderhistorycompleteComponent,
    OrderhistorycompleteDetailComponent,
    OrderhistorycompleteEditComponent
  ],
  exports:[
    OrderhistorycompleteComponent,
    OrderhistorycompleteDetailComponent,
    OrderhistorycompleteEditComponent
  ]
})
export class OrderhistorycompleteModule { }
