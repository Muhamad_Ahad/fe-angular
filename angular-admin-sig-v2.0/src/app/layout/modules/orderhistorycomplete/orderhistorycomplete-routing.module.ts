import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderhistorycompleteComponent } from './orderhistorycomplete.component';
import { OrderhistorycompleteDetailComponent } from './detail/orderhistorycomplete.detail.component';
import { OrderhistorycompleteEditComponent } from './edit/orderhistorycomplete.edit.component';

// import { OrderhistorysummaryDetailComponent } from './detail/orderhistorysummary.detail.component';
// import { OrderhistorysummaryEditComponent } from './edit/orderhistorysummary.edit.component';


const routes: Routes = [
  {
      path: '', component: OrderhistorycompleteComponent
  },
  // {
  //   path:'detail', component: OrderhistorycompleteDetailComponent
  // },
  // {
  //   path:'edit', component: OrderhistorysummaryEditComponent
  // }

  {
    path:'edit', component: OrderhistorycompleteDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderhistorycompleteRoutingModule { }
