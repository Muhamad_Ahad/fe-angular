import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { NotificationService } from '../../../../services/notification/notification.service';

@Component({
  selector: 'app-notificationgroup-detail',
  templateUrl: './notificationgroup.detail.component.html',
  styleUrls: ['./notificationgroup.detail.component.scss'],
  animations: [routerTransition()]
})

export class NotificationGroupDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;
  errorLabel    : any = false;
  edit:boolean = false;
  
  constructor(public notificationService:NotificationService) {
  }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    
    
  }

  editThis(){
    console.log(this.edit );
    this.edit = !this.edit;
    console.log(this.edit );
  }
  backToTable(){
    console.log(this.back);
    this.back[1](this.back[0]);
  }

  // async deleteThis(){
  //   try{
  //     let delResult: any = await this.notificationService.delete(this.detail);
  //     //console.log(delResult);
  //     if(delResult.error==false){
  //         console.log(this.back[0]);
  //         this.back[0].notificationDetail=false;
  //         this.back[0].firstLoad();
  //         // delete this.back[0].notificationDetail;
  //     } 
  //   } catch (e) {
  //       this.errorLabel = ((<Error>e).message);//conversion to Error type
  //   }      
    
  // }
}
