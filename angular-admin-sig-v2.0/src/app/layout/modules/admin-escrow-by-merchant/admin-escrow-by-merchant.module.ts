import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminEscrowByMerchantComponent } from './admin-escrow-by-merchant.component';
import { AdminEscrowByMerchantRoutingModule} from './admin-escrow-by-merchant-routing.module';

import { PageHeaderModule } from './../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { BsComponentModule } from '../bs-component/bs-component.module';


@NgModule({
  imports: [
    CommonModule,
    AdminEscrowByMerchantRoutingModule,
    PageHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule
  ],
  declarations: [AdminEscrowByMerchantComponent],
  
})
export class AdminEscrowByMerchantModule { }
