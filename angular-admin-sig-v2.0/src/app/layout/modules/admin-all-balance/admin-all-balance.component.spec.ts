import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAllBalanceComponent } from './admin-all-balance.component';

describe('AdminAllBalanceComponent', () => {
  let component: AdminAllBalanceComponent;
  let fixture: ComponentFixture<AdminAllBalanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminAllBalanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAllBalanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
