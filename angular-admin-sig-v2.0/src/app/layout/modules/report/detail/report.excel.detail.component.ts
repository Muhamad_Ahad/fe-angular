import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { ReportService } from '../../../../services/report/report.service';
import { del } from 'selenium-webdriver/http';


@Component({
  selector: 'app-report-excel-detail',
  templateUrl: './report.excel.detail.component.html',
  styleUrls: ['./report.excel.detail.component.scss'],
  animations: [routerTransition()]
})

export class ReportExcelDetailComponent implements OnInit {
  @Input() public detail: any;
  // @Input() public back;

  edit:boolean = false;
  errorLabel    : any = false;
  constructor(public reportService:ReportService) {
  }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    
    
  }

  editThis(){
    this.edit = !this.edit;
  }

  // backToTable(){
  //   this.back[1](this.back[0]);
  // }

  async deleteThis(){
    // try{
    //   let delResult: any = await this.testproductService.delete(this.detail);
    //   console.log(delResult);
    //   if(delResult.error==false){
    //       console.log(this.back[0]);
    //       this.back[0].testproductDetail=false;
    //       this.back[0].firstLoad();
    //       // delete this.back[0].testproductDetail;
    //   } 
    // } catch (e) {
    //     this.errorLabel = ((<Error>e).message);//conversion to Error type
    // }      
    
  }
  
}
