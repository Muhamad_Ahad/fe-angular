import { Component, OnInit, Input  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';
import { async } from 'q';
import { MemberService } from '../../../../services/member/member.service';
import * as content from '../../../../../assets/json/content.json';
@Component({
  selector: 'app-packing-list-generate',
  templateUrl: './packing-list.generate.component.html',
  styleUrls: ['./packing-list.generate.component.scss']
})
export class PackingListGenerateComponent implements OnInit {
	// @Input() public data: any;
	// @Input() public thisParent;
  @Input() public detail: any;
  @Input() public back;


  public contentList : any = (content as any).default;
  titleProgram: any = "";
  service;
  getData = [];
  totalResult = [];
  totalWeight = [];
  perWeight = [];
  endTotalResult = [];
  special = [];
  loading = false;
  same = false;
  merchant_data ={};
  memberCity:any;
  courier_list: any;
  current_courier: any;
  courier_image:any;
  courier_image_array: any = [];
  sig_project: any = false;
  sig_kontraktual: any = false;

  delivery_list: any = [];
    
  errorMessage: string;
  constructor(private OrderhistoryService: OrderhistoryService, private router: Router, private route: ActivatedRoute,private memberService: MemberService,) {
    // this.getData = route.snapshot.params['invoiceIds']
    //   .split(',');
  }

  ngOnInit() {
    this.firstload();
    // this.print();
  }

  async firstload() {
    // console.log("this.back 2", this.back);
    // console.warn("detail generate", this.detail);
    const program = localStorage.getItem('programName');
    let _this = this;
    this.contentList.filter(function(element) {
        if(element.appLabel == program) {
          if (program && program == "retail_poin_sahabat") {
            _this.sig_project = true;
          } else if (program && program == "sig_kontraktual") {
            _this.sig_kontraktual = true;
            _this.titleProgram = element.title;
          } else {
            _this.titleProgram = element.title;
          }
        }
    });
    // console.log("titleProgram", this.titleProgram);
    
    // console.warn('this data', this.detail)
    this.route.queryParams.subscribe(async (params) => {
      this.getData = params.data
      this.service = this.OrderhistoryService
      // console.warn("this getdata", this.getData)
    
    })
     
    

      // this.detail.forEach(element => {
      //   console.log("data", element)
      //   if(element.member_detail.hadiah_dikuasakan == 'ya' || element.member_detail.hadiah_dikuasakan == 'KUASA' || element.member_detail.hadiah_dikuasakan == 'true' ){
      //    this.special.push(element)
      //   }
      // });
    // window.print();
    this.loading = false;

    // console.log("detail.delivery_list", this.detail.delivery_list);

    this.detail.delivery_list.forEach((result) => {
      console.log("result", result);
      if (this.delivery_list.filter(e => e.reference_no === result.reference_no).length > 0) {
        this.delivery_list.filter(e => e.reference_no === result.reference_no)[0].product_list.push(result.product_list);
      } else {
        this.delivery_list.push(
          {
            awb_number: result.awb_number,
            reference_no : result.reference_no,
            destination: result.destination,
            status: result.status,
            product_list:[result.product_list],
            po_no:result.po_no
          }
        );
      }
    });

    console.log("this.delivery_list", this.delivery_list);
  }

  public prints(){
    window.print()
  }

  backToHere() {
      // console.log("obj", obj);
    console.log("this.back", this.back);
      console.log("this.back[1]", this.back[1]);
      console.log("this.back[0]", this.back[0]);
    // this.evoucherStockDetail = false;
    this.back[1](this.back[0]);
    // this.firstLoad();
  }
  
}
