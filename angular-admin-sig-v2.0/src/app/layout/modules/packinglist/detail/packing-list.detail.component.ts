import { formatCurrency } from '@angular/common';
import { OrderhistoryService } from './../../../../services/orderhistory/orderhistory.service';
import { Component, OnInit, Input } from '@angular/core';
import { del } from 'selenium-webdriver/http';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { routerTransition } from '../../../../router.animations';
import { EVoucherService } from '../../../../services/e-voucher/e-voucher.service';
import {FormOptions, TableFormat} from '../../../../object-interface/common.object';

@Component({
  selector: 'app-packing-list-detail',
  templateUrl: './packing-list.detail.component.html',
  styleUrls: ['./packing-list.detail.component.scss'],
  animations: [routerTransition()]
})

export class PackingListReportDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  packingListDetail;
  packingNumber;
  processNow: any = false;
  service;
  edit: boolean = false;
  errorLabel: any = false;
  isGenerate: boolean = false;
  constructor(
    public evoucherService: EVoucherService, 
    private route: ActivatedRoute,
    private router: Router,
    public orderHistoryService:OrderhistoryService) {
  }
  // packingList : any;
  orders       : any = [];
  row_id        : any = "packing_number";
  totalPage     : 0;
  errorMessage  : any = false;
  pointDetail  : any = false;
  tableFormat   : TableFormat = {
    title           : 'Packing List Detail',
    label_headers   : [
      {label: 'PO',     visible: true, type: 'value_po', data_row_name: 'po_no'},
      {label: 'AWB Number', visible: true, type: 'string', data_row_name: 'awb_number' },
      {label: 'Package ID', visible: true, type: 'string', data_row_name: 'reference_no'},
      {label: 'Nama', visible: true, type: 'receiver_name', data_row_name: 'destination'},
      {label: 'Alamat', visible: true, type: 'shipping_address', data_row_name: 'destination'},
      {label: 'No Telp', visible: true, type: 'phone_number', data_row_name: 'destination'},
      {label: 'Nama Barang', visible: true, type: 'product_redeem', data_row_name: 'product_list'},
      {label: 'Qty', visible: true, type: 'product_quantity', data_row_name: 'product_list'},
      {label: 'Total Price', visible: true, type: 'product_price_redeem', data_row_name: 'product_list'},
      
    ],
    row_primary_key : '_id',
    formOptions     : {
      row_id    : this.row_id,
      this      : this,
      result_var_name : 'orders',
      detail_function : [this, 'callDetail'],
      removePackinglist : true
    },
    show_checkbox_options : true
  };

  ngOnInit() {
    console.warn("DETAIL", this.detail);
    this.packingListDetail = this.detail;
    this.packingNumber = this.detail.packing_no;
    this.firstLoad();

  }

  async firstLoad() {
    console.log("this.back", this.back);
    this.service = this.orderHistoryService;

    this.processNow = false;
    this.edit = false;
    this.errorLabel = false;
    this.orders = [];
    this.errorMessage  = false;
    this.pointDetail  = false;
    

    // try {
      if(this.packingListDetail.status != 'ACTIVE'){
        this.tableFormat.show_checkbox_options = false;
        delete this.tableFormat.formOptions.removePackinglist;
      }
      this.orders = this.packingListDetail.delivery_list;
      // this.orders.foreach((order: any, index : number) => {
      //   this.orders[index].reference_no = this.packingList.reference_no[index];
      // });
    // } catch(e) {
    //   this.errorLabel = ((<Error>e).message);//conversion to Error type

    //   if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
    //     this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
    //   }
    // }


    // try {
    //   const result : any= await this.evoucherService.detailEvoucherStock(this.packingNumber);
    //   console.warn("result detail stock", result)
    //   if(result.length > 0){
    //     if(result[0].process_number == this.detail.process_number){
    //       this.packingListDetail = result[0];
    //     }
    //   }
    // } catch (e) {
    //   this.errorLabel = ((<Error>e).message);//conversion to Error type
    // }
  }

  public async callDetail(param){
  }

  backToHere() {
    // this.evoucherStockDetail = false;
    this.back[1](this.back[0]);
    // this.firstLoad();
  }

  backHere(obj){
    obj.isGenerate = false;
  }

  isProcessNow() {
    this.processNow = !this.processNow;
  }

  async processPackingList() {
    Swal.fire({
      title: 'Confirmation',
      text: 'Anda yakin proses packing list ?',
      confirmButtonText: 'Process',
      cancelButtonText:"Cancel",
      showCancelButton:true
      }).then(async (result) => {
      if(result.isConfirmed){
        
        if(this.orders.length > 0){
          
          try {
            let dataProcess = 
            {
              "packing_no": this.packingListDetail.packing_no
            }
            let result: any = await this.orderHistoryService.processPackingList(dataProcess);
            console.warn("result process", result)
            if (result) {
              Swal.fire({
                title: 'Success',
                text: 'Packing List Processed',
                icon: 'success',
                confirmButtonText: 'Ok',
              }).then( async (result) => {
                try {
                  let result = await this.orderHistoryService.searchPackingListDetailReport(this.packingNumber);
                  if(result.values.length < 1) this.backToHere();
                  this.totalPage = result.total_page;
                  this.packingListDetail = result.values[0];
                  this.firstLoad();
                } catch (e) {
                   Swal.fire("Failed",(<Error>e).message, "error"); 
                }
              });
            }
          } catch(e) {
            console.warn("error appove", e);
            // alert("Error");
      
            this.errorLabel = ((<Error>e).message);
            // conversion to Error type
      
            let message = this.errorLabel;
            if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
              message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
            }
            Swal.fire({
              icon: 'error',
              title: message,
            });
          }
        }
      }
      });
  }

  async generatePackingList(){
    this.isGenerate = true;
  }

  // private async loadDetail(historyID: string){
  //   try {
  //     this.service = this.evoucherService;
  //     let result: any = await this.evoucherService.detailEvoucherStock(historyID);
  //     this.packingListDetail = result.result[0];
  //     console.log(this.packingListDetail);
  //   } catch (error) {
      
  //   }
  // }

  public async processSelectedOrders(obj, orders){
    if(orders.length > 0){
      Swal.fire({
        title: 'Confirmation',
        text: 'Hapus dari packing list ?',
        confirmButtonText: 'Process',
        cancelButtonText:"Cancel",
        showCancelButton:true
        }).then(async (result) => {
        if(result.isConfirmed){

          let listReferenceNo = [];
          console.warn("orders", orders);
          orders.forEach((reference_no : any) => {
            listReferenceNo.push(reference_no)
          });
          if(listReferenceNo.length > 0){
            try {
              let payload = {
                packing_no : this.packingListDetail.packing_no,
                reference_no : listReferenceNo
              }
              console.warn("payload remove", payload);
             await this.orderHistoryService.removePackingList(payload);
             Swal.fire({
               title : 'Success',
               text : 'remove from packing list success',
               confirmButtonText : 'ok'
             }).then(async (result) => {
               if(result.isConfirmed){
                 try {
                   let result = await this.orderHistoryService.searchPackingListDetailReport(this.packingNumber);
                   if(result.values.length < 1) this.backToHere();
                   this.totalPage = result.total_page;
                   this.packingListDetail = result.values[0];
                   this.firstLoad();
                 } catch (e) {
                    Swal.fire("Failed",(<Error>e).message, "error"); 
                 }
               }
             });
             
            } catch (e) {
              Swal.fire("Failed",(<Error>e).message, "error");
              
            }

          }

          // await orders.forEach(async(order_id, index) => {
          //   const payload = {
          //     order_id
          //   }
          //   console.log(payload);
          //   try {
          //     await obj.orderhistoryService.processOrder(payload).then(() => {
          //       this.processCount ++;
                
          //       if(this.processCount >= orders.length) { 
          //         this.showLoadingSpinner = false;

          //         Swal.fire({
          //           title: 'Success',
          //           text: 'Semua order terpilih telah diproses',
          //           icon: 'success',
          //           confirmButtonText: 'Ok',
          //           showCancelButton:false
          //           }).then(async (result) => {
          //             if(result.isConfirmed){
          //               console.log("confirmed");
          //               obj.firstLoad();
          //             }
          //         });
          //       }
          //     });
              

          //     Swal.fire({
          //       title: 'Success',
          //       text: 'Semua order terpilih telah diproses',
          //       icon: 'success',
          //       confirmButtonText: 'Ok',
          //       showCancelButton:false
          //       }).then(async (result) => {
          //         if(result.isConfirmed){
          //           obj.firstLoad();
          //         }
          //       });

          //     setTimeout(() => {
          //       this.processCount ++;
          //       console.log(this.processCount);
          //       console.log("orders.length", orders.length);
          //       if(this.processCount >= orders.length) { 
          //         this.showLoadingSpinner = false;
          //       }
          //     }, index * 500);
          //   } catch (e) {
          //     this.errorLabel = ((<Error>e).message);//conversion to Error type

          //     let message = this.errorLabel;
          //     if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
          //       message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
          //     }
          //     Swal.fire({
          //       icon: 'error',
          //       title: message,
          //     });
          //   }
          // });
        }
      });
    }
  }
  


}
