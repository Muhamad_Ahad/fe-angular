import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { PromotionService } from '../../../../services/promotion/promotion.service';

@Component({
  selector: 'app-codepage-detail',
  templateUrl: './codepage.detail.component.html',
  styleUrls: ['./codepage.detail.component.scss'],
  animations: [routerTransition()]
})

export class CodepageDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  edit = false;

  constructor(public promotionService: PromotionService) {
  }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {


  }

  editThis() {
    console.log(this.edit );
    this.edit = !this.edit;
    console.log(this.edit );
  }
  backToTable() {
    console.log(this.back);
    this.back[1](this.back[0]);
  }

}
