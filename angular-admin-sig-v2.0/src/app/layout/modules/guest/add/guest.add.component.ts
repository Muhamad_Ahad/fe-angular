import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { MemberService } from '../../../../services/member/member.service';

@Component({
  selector: 'app-guest-add',
  templateUrl: './guest.add.component.html',
  styleUrls: ['./guest.add.component.scss'],
  animations: [routerTransition()]
})

export class GuestAddComponent implements OnInit {
  public name:string = "";
  Guest:any = [];
  service:any;
  public member_type = [
    {label:"superuser", value:"superuser"}
   ,{label:"member", value:"member"}
   ,{label:"marketing", value:"marketing"}
   ,{label:"admin", value:"admin"}
   ,{label:"merchant", value:"merchant"}
   ,{label:"guest", value:"guest", selected:1}
 ];
 selectedFile  = null;
  progressBar   :number= 0;
  cancel        :boolean = false;
  errorFile     :any = false;
  errorLabel    :any = false;
  full_name:any;
  point_balance:any;
  type:any;
  org_id:any;
  
  constructor(public memberService:MemberService) {
    let form_add     :any = [
      { label:"Full Name",  type: "text",  value: "", data_binding: 'full_name'  },
      { label:"Point Balance",  type: "text",  value: "", data_binding: 'point_balance'  },
      { label:"Member Type",  type: "password",  value: "", data_binding: 'type'  },
      { label:"Org ID",  type: "password",  value: "", data_binding: 'org_id'  },
      
    ];
   }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    // this.service = this.memberService;
    // let result:any = await this.memberService.getMembersLint();
    // this.Members = result.result;
    
  }

  async formSubmitAddGuest(form){
    //console.log(JSON.stringify(form));
    
    try 
    {
      this.service    = this.memberService;
      /* let result:any  = await this.memberService.addNewGuest(form);
      console.log(result);
      this.Guest = result.result; */
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }
}
