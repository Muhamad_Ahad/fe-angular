import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GuestComponent } from './guest.component';
import { GuestAddComponent } from './add/guest.add.component';
import { GuestDetailComponent } from './detail/guest.detail.component';

const routes: Routes = [
  {
      path: '', component: GuestComponent,

  },
   {
      path:'add', component: GuestAddComponent
  },
  {
    path:'detail', component: GuestDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GuestRoutingModule { }
