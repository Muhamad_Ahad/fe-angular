import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { Router } from '@angular/router';
import { PermissionObserver } from '../../../services/observerable/permission-observer';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()]
})
export class DashboardComponent implements OnInit {
    public alerts: Array<any> = [];
    public sliders: Array<any> = [];

    constructor(private router: Router, private castPermission: PermissionObserver) {


        
        // this.sliders.push(
        //     {
        //         imagePath: 'assets/images/1.jpg',
        //         // label: 'First slide label',
        //         // text:'Nulla vitae elit libero, a pharetra augue mollis interdum.'
        //     },
        //     {
        //         imagePath: 'assets/images/00_evoucher-product_item.jpg',
        //         // label: 'Second slide label',
        //         // text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
        //     },
        //     {
        //         imagePath: 'assets/images/promo_funworld.jpg',
        //         // label: 'Third slide label',
        //         // text:
        //         //     'Praesent commodo cursus magna, vel scelerisque nisl consectetur.'
        //     },
        //     {
        //         imagePath: 'assets/images/5.jpg',
        //         // label: 'Third slide label',
        //         // text:
        //         //     'Praesent commodo cursus magna, vel scelerisque nisl consectetur.'
        //     },
        //     {
        //         imagePath: 'assets/images/a.jpg',
        //         // label: 'Third slide label',
        //         // text:
        //         //     'Praesent commodo cursus magna, vel scelerisque nisl consectetur.'
        //     }
        // );

        // this.alerts.push(
        //     {
        //         id: 1,
        //         type: 'success',
        //         message: 'halaman ini adalah halaman dashboard untuk Pengguna'
        //     },
        //     {
        //         id: 2,
        //         type: 'warning',
        //         message: 'Halaman Dashboard sudah bisa digunakan sejak per tanggal 10 Juni 2019'
        //     }
        // );
    }

    ngOnInit() {
      
        

    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }
}
