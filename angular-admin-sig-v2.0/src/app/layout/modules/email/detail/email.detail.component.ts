import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { EmailService } from '../../../../services/email/email.service';

@Component({
  selector: 'app-email-detail',
  templateUrl: './email.detail.component.html',
  styleUrls: ['./email.detail.component.scss'],
  animations: [routerTransition()]
})

export class EmailDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;
  errorLabel    : any = false;
  edit:boolean = false;
  
  constructor(public emailService:EmailService) {
  }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    
    
  }

  editThis(){
    console.log(this.edit );
    this.edit = !this.edit;
    console.log(this.edit );
  }
  
  backToTable(){
    console.log(this.back);
    this.back[1](this.back[0]);
  }

  async deleteThis(){
    try{
      let delResult: any = await this.emailService.delete(this.detail);
      //console.log(delResult);
      if(delResult.error==false){
          console.log(this.back[0]);
          this.back[0].emailDetail=false;
          this.back[0].firstLoad();
          // delete this.back[0].emailDetail;
      } 
    } catch (e) {
        this.errorLabel = ((<Error>e).message);//conversion to Error type
    }      
    
  }
}
