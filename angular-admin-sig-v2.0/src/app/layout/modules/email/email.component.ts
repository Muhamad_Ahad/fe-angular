import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { EmailService } from '../../../services/email/email.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.scss'],
  animations: [routerTransition()]
})

export class EmailComponent implements OnInit {

  Email       : any = [];

    tableFormat: TableFormat = {
        title: 'Active Email Detail',
        label_headers: [
            { label: 'Sender', visible: true, type: 'string', data_row_name: 'sender_email' },
            { label: 'Recipient', visible: true, type: 'string', data_row_name: 'recipient_email' },
            { label: 'CC Recipient', visible: true, type: 'string', data_row_name: 'CC' },
            { label: 'Subject', visible: true, type: 'string', data_row_name: 'subject' },
            // { label: 'Body', visible: false, type: 'string', data_row_name: 'body' },
            // { label: 'type', visible: false, type: 'string', data_row_name: 'email_type' }
        ],
        row_primary_key: '_id',
        formOptions: {
            row_id: '_id',
            this: this,
            result_var_name: 'Email',
            detail_function: [this, 'callDetail'],
        }
    };

  row_id        : any = "_id";
  errorLabel : any = false;
  emailDetail: any = false;
  service     : any ;


  constructor(public emailService:EmailService) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    try {
      this.service    = this.emailService;
      const result: any  = await this.emailService.getEmailLint();
      //console.log(result.result);
      this.Email = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  public async callDetail(_id) {

    try {
      this.service    = this.emailService;
      let result: any = await this.emailService.detailEmail(_id);
      //console.log(result);
      this.emailDetail = result.result[0];
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj) {
    obj.emailDetail = false;
  }

}
