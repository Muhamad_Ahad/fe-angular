import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { ProductService } from '../../../../services/product/product.service';
import { PackagebundleService } from '../../../../services/packagebundle/packagebundle.service';
// import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-generate',
  templateUrl: './packagebundle.generate.component.html',
  styleUrls: ['./packagebundle.generate.component.scss'],
  animations: [routerTransition()]
})
export class PackagebundleGenerateComponent implements OnInit {
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  errorLabel : any = false;
  qty:any;
  Products: any = [];
  service: any;
  rowOfTable;
  currentValue;
  promo_deals_data: any;
  title: any;
  number: 0;
  description;
  fixed_value;
  discount;
  product_code: any;
  product_name: any;
  merchant_username:any;
  constructor(public productService: ProductService, public packageBundleService: PackagebundleService ) { }

  selectedLevel;
   selected(){
     console.log(this.selectedLevel);
   }

  ngOnInit() {
    this.firstLoad();
    // this.bundle();
  }

  // async bundle(){
  //   let result = await this.productService.packageBundle();
  //   console.log("result", result);
  // }

  async firstLoad(){
    let result = await this.productService.getDropdown();
    console.log("sekutt", result)
    this.rowOfTable = result.result.values
    let productStringListOfName = result.result;
    this.dropdownList = [];
    productStringListOfName.forEach((obj, index) => {
                  this.dropdownList.push({ item_id: obj._id, item_text: obj.product_name+"-"+obj.merchant_username })
        })
    this.selectedItems = [
    ];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      textItem: 'item',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 15,
      allowSearchFilter: true
    };

  let dataDropdown : standardDropDown[] = [];

  this.promo_deals_data = dataDropdown;
  // this.promo_deals_data[0].selected = 1;

 


  }

  backTo(){
    window.location.href = '/packagebundle';
  }

  async formSubmitPackagebundle(form){
    try 
    {
      form.products =[];
      // let obj = {}
      // this.selectedItems.forEach(item => obj[item.item_id] = item.item_text);
      // let json = JSON.stringify(obj);
      form.qty = Number(form.qty);
      if (form.discount > 1){
        alert("Discount must be in decimal")
      }
      else{
        this.selectedItems.forEach((element, index)=>{
          form.products.push(element.product_code);
          form.products.push(element.qty);
        })
      this.service    = this.productService;
      let result: any  = await this.productService.packageBundle();
      this.Products = result.result;
      alert("Generate Success");
      window.location.href = "/Packagebundle"
      }
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
    
  }

  onItemSelect(item: any) {
    // console.log(this.selectedItems);
  }
  onSelectAll(items: any) {
    // console.log(items);
  }

}

interface standardDropDown{
  label
  value
  selected
}

interface dropdownList{
  idField
  textField
  selectAllText
  unSelectAllText
}

