
import { PackagebundleService } from '../../../../services/packagebundle/packagebundle.service';
import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';


@Component({
  selector: 'app-packagebundle-detail',
  templateUrl: './packagebundle.detail.component.html',
  styleUrls: ['./packagebundle.detail.component.scss'],
  animations: [routerTransition()]
})

export class PackagebundleDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  edit = false;

  errorLabel:any;

  constructor(public PackagebundleService: PackagebundleService) {
  }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {


  }

  editThis() {
    console.log(this.edit );
    this.edit = !this.edit;
    console.log(this.edit );
  }

  backToTable() {
    console.log(this.back);
    this.back[1](this.back[0]);
  }

  async deleteThis(){
    try{
      let delResult: any = await this.PackagebundleService.deletePackagebundle(this.detail);
      //console.log(delResult);
      if(delResult.error == false){
          this.back[0].firstLoad();
          alert("Data has been removed");
          // window.location.href = "/Packagebundle";
          // delete this.back[0].emailDetail;
      } 
    } catch (e) {
        this.errorLabel = ((<Error>e).message);//conversion to Error type
    }      
    
  }

}
