import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PromodealsComponent } from './promodeals.component';
import { PromodealsGenerateComponent } from './generate/promodeals.generate.component';

const routes: Routes = [
    { path: '', component: PromodealsComponent },
    { path: 'generate', component: PromodealsGenerateComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PromodealsRoutingModule {
}
