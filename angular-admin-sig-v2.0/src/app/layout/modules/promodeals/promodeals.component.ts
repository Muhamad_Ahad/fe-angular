import { Component, OnInit } from '@angular/core';
import {routerTransition} from '../../../router.animations';
import { PromodealsService } from '../../../services/promodeals/promodeals.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
  selector: 'app-promodeals',
  templateUrl: './promodeals.component.html',
  styleUrls: ['./promodeals.component.scss'],
    animations: [routerTransition()]
})
export class PromodealsComponent implements OnInit {

    promodeals: any = [];
  tableFormat: TableFormat = {
                                  title           : 'Promo Deals Detail Page',
                                  label_headers   : [
                                    // {label: 'Promo Deals ID', visible: false, type: 'date', data_row_name: '_id'},
                                    {label: 'Title', visible: true, type: 'string', data_row_name: 'title'},
                                    {label: 'Description', visible: true, type: 'string', data_row_name: 'description'},
                                    {label: 'Discount (decimal)', visible: true, type: 'number', data_row_name: 'discount'},
                                    // {label: 'Type', visible: false, type: 'string', data_row_name: 'type'},
                                    {label: 'Created Date', visible: true, type: 'date', data_row_name: 'created_date'},
                                    {label: 'Value', visible: true, type: "number", data_row_name: 'fixed_value'},
                                    // {label: 'Updated Date', visible: false, type: "date", data_row_name: 'updated_date'},

                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                    addFormGenerate   : true,
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'promodeals',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  form_input: any = {};
  errorLabel: any = false;

  promodealsDetail: any = false;
  checkDetails: any;
  service: any;

  constructor(
      public PromodealsService: PromodealsService
  ) { }

  ngOnInit() {
      this.firstLoad();
  }

  async firstLoad() {
    try {
      this.service    = this.PromodealsService;
      const result: any  = await this.PromodealsService.getPromodealsLint({});
      this.promodeals = result.result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message); // conversion to Error type
    }
  }

  public async callDetail(promodeals_id) {
    try {
      let result: any;
      this.service    = this.PromodealsService;
      result          = await this.PromodealsService.detailPromodeals(promodeals_id)
      if(result.result.length == 0){
        alert('Data Has Been Removed');
      }
      else{
        this.promodealsDetail = result.result[0];
      }
    } catch (e) {
      this.errorLabel = ((<Error>e).message); // conversion to Error type
    }
  }

  public async backToHere(obj) {
    obj.promodealsDetail = false;
  }


}
