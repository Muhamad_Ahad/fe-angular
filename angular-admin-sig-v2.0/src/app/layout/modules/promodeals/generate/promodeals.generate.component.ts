import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { ProductService }     from '../../../../services/product/product.service';
import { PromodealsService }     from '../../../../services/promodeals/promodeals.service';
// import { Observable } from 'rxjs/Observable';
import { ChatComponent } from '../../dashboard/components';

@Component({
  selector: 'app-generate',
  templateUrl: './promodeals.generate.component.html',
  styleUrls: ['./promodeals.generate.component.scss'],
  animations: [routerTransition()]
})
export class PromodealsGenerateComponent implements OnInit {
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  errorLabel : any = false;
  qty:any;
  Products: any = [];
  service: any;
  rowOfTable;
  currentValue;
  promo_deals_data: any;
  title: any;
  number: 0;
  form;
  fixed_value
  discount
  description
  constructor(public PromodealsService: PromodealsService ) { }

  selectedLevel;
   selected(){
     console.log(this.selectedLevel);
   }

  ngOnInit() {
    this.firstLoad();
  }

  async firstLoad(){
    let getPromodealsData = await this.PromodealsService.getDropdown();
    this.rowOfTable = getPromodealsData.result.values
    let productStringListOfName = getPromodealsData.result;
    this.dropdownList = [];
    productStringListOfName.forEach((obj, index) => {
                  this.dropdownList.push( { item_id: obj._id, item_text: obj.product_name+"-"+obj.merchant_username } )
        })
    this.selectedItems = [
    ];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      textItem: 'item',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 15,
      allowSearchFilter: true
    };
  //   this.currentValue = productStringListOfName[0].product_name;
  //   productStringListOfName.forEach((obj, index) => {
  //     // this.product_code.push(obj.product_code)
  //     // this.product_name.push(obj.product_name)
  //     // this.merch_username.push(obj.merchant_username)
  // })

  // let dataDropdown : standardDropDown[] = [];

  //   products.forEach((data, index)=>{
  //     dataDropdown.push(
  //       {
  //         label: data['product_code'] + " - " +  data['product_name'] + " - "+ data['merchant_username'],
  //         value : data['product_code'],
  //         selected : 0}
  //         );
  //   })
  let dataDropdown : standardDropDown[] = [];

  // productStringListOfName.forEach((obj, index) => {
  //             this.dropdownList.push("item_text: ",obj.product_code)
  //   })

  // products.forEach((data, index)=>{
  //   this.dropdownList.push(
  //     {
  //       label: data['product_name'] + " - " +  data['product_code'] + " - "+ data['merchant_username'],
  //       value : data['product_name'],
  //       selected : 0}
  //       );
  // })

  this.promo_deals_data = dataDropdown;
  // this.promo_deals_data[0].selected = 1;

  


  }

  async formSubmitPromodeals(form){
    try 
    {
      form.products =[];
      // let obj = {}
      // this.selectedItems.forEach(item => obj[item.item_id] = item.item_text);
      // let json = JSON.stringify(obj);
      form.qty = Number(form.qty);
      if (form.discount > 1){
        alert("Discount must be in decimal")
      }
      else{
        this.selectedItems.forEach((element, index)=>{
          form.products.push(element.item_id);
      })
      this.service    = this.PromodealsService;
      let result: any  = await this.PromodealsService.addPromo(form);
      this.Products = result.result;
      alert("Generate Success");
      window.location.href = "/promodeals"
      }
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
    
  }

  onItemSelect(item: any) {
    // console.log(this.selectedItems);
  }
  onSelectAll(items: any) {
    // console.log(items);
  }

}

interface standardDropDown{
  label
  value
  selected
}

interface dropdownList{
  idField
  textField
  selectAllText
  unSelectAllText
}

