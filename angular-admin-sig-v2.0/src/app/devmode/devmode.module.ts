import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DevmodeRoutingModule } from './devmode-routing.module';
import { DevmodeComponent } from './devmode.component';

@NgModule({
  declarations: [DevmodeComponent],
  imports: [
    CommonModule,
    DevmodeRoutingModule
  ]
})
export class DevmodeModule { }
