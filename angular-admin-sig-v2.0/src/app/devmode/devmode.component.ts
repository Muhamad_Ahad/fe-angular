import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-devmode',
  templateUrl: './devmode.component.html',
  styleUrls: ['./devmode.component.scss']
})
export class DevmodeComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    try{
      localStorage.setItem('devmode', 'active' );
      this.router.navigateByUrl('/login');
    }
    catch(e){

    }
  }

}
