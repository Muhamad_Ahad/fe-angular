import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { PageHeaderModule } from '../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../component-libs/form-builder-table/form-builder-table.module';
import { BsComponentModule } from '../layout/modules/bs-component/bs-component.module';
import { LogoutComponent } from './logout.component';

@NgModule({
    imports: [CommonModule, LoginRoutingModule,PageHeaderModule, 
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        FormBuilderTableModule,
        BsComponentModule
    ],
    declarations: [LoginComponent, LogoutComponent]
})
export class LoginModule {}
