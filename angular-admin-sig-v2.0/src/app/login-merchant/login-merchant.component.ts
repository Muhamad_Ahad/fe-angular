import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from '../services/login/login.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-login-merchant',
  templateUrl: './login-merchant.component.html',
  styleUrls: ['./login-merchant.component.scss']
})
export class LoginMerchantComponent implements OnInit {
  errorLabel: any = false;
  showLoading: boolean = false;
  email = new FormControl('', [Validators.required, Validators.email]);
  password: string="";
  hide =true;
  checked = false;
  errorMessage;
  public form : any = {
    email: "",
    password: "",
    }

  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
        this.email.hasError('email') ? 'Not a valid email' :
            '';
  }
  constructor(private loginService: LoginService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
  }

  registerClicked(){
    this.router.navigate(['/register'])
  }
  forgotPasswordClicked(){
    this.router.navigate(['/reset-password'])
  }

  async autoLogin() {
    this.showLoading = true;
    console.log(this.password)
    console.log(this.email)

    var login_data = {
      "email": this.form.email,
      "password": this.form.password
    }

    console.log(login_data);
    try {
      
      let result = await this.loginService.memberLogin(login_data);

    if (!result.error) {
      console.log(result);
      localStorage.setItem('isLoggedin', 'true');
      localStorage.setItem('tokenlogin', result.result.token);
      
      if(result.result.permission == 'merchant'){
        this.router.navigateByUrl('/merchant-portal/homepage');
      }else {
        this.router.navigateByUrl('/register/open-merchant')
      } 
    }else {
      this.showLoading = false;
      this.errorMessage = result.error;
    }
    }catch(e){
      this.showLoading = false;
      this.errorMessage = ((<Error>e).message)
    }
    
  }
  
  }

  

