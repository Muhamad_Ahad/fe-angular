import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginMerchantComponent } from './login-merchant.component';


const routes: Routes = [
  {
    path: '',
    component: LoginMerchantComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginMerchantRoutingModule { }
