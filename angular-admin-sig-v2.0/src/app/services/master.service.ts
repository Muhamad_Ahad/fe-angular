import { Injectable, Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { api_url } from '../../environments/environment';
//import { url } from 'inspector';

interface Result {
  error: any;
  result: any;
}

@Injectable({
  providedIn: 'root',

})


export class MasterService {
  public api_url = '';
  public http: HttpClient;
  public myToken: string;
  public myStatus: string;

  constructor(public httpGet: HttpClient) {
    this.http    = httpGet;
    this.api_url = api_url();

  }

  async getToken() {
    const myToken  = await localStorage.getItem('tokenlogin');
    const myStatus = await localStorage.getItem('isLoggedin');

    if (myToken && myStatus) {
      this.myToken  = myToken;
      this.myStatus = myStatus;
    }
  }

  public  checkingUrl(_url) {
    const indexOfHttp = _url.indexOf('http');
    let url: string;

    url = _url;
    if (indexOfHttp < 0) {
      url = this.api_url + _url;
    }
    return url;
  }
  
  public async get(_params, _url, headers?: any) {
    let result = null;
    try {
      result = await this.getRest(_params, _url, headers).toPromise();
    } catch (error) {
      this.checkStatus(error);
      if(error.status == 401){
        console.warn("MASUK SINI NIH")
        localStorage.removeItem('isLoggedin');
        localStorage.removeItem('tokenlogin');
        localStorage.removeItem('devmode');
        // window.location.href = "/login"
        
      }
      let msg = error.error?(error.error.error?error.error.error: (<Error>error).message) : (<Error>error).message;
      
      throw new Error(JSON.stringify(msg, null, 2));
    }

    return result;
  }

  public async post(_params, _url, headers?: any) {

    let result;
    try {
      result = await this.postRest(_params, _url, headers).toPromise();
    } catch (error) {
      this.checkStatus(error);

      let throwError= error;
      let myToken   = localStorage.getItem('tokenlogin');
      let myStatus  = localStorage.getItem('isLoggedin');

      if(error.status == 401 && myToken && myStatus){
        throw new Error( "you're not allowed to go Forward please check again. call your administrator");

        // localStorage.removeItem('isLoggedin');
        // localStorage.removeItem('tokenlogin');
        // localStorage.removeItem('devmode');
        // window.location.href = "/login"
        
      }

      if (error.error) {
          throwError = error.error; // the result from server is {"error": "error message"}
      }

      let msg = error.error?(error.error.error?error.error.error: (<Error>error).message) : (<Error>error).message;
      
      throw new Error(JSON.stringify(msg, null, 2));
    }

    return result;
  }

  public async add(_params, _url, headers?: any) {
    let result;
    try {

      result = await this.postRest(_params, _url, headers).toPromise();
      console.log('result = ', result);
    } catch (error) {
      this.checkStatus(error);
      if(error.status == 401){
        throw new Error( "you're not aloowed to go Forward please check again");
        // localStorage.removeItem('isLoggedin');
        // localStorage.removeItem('tokenlogin');
        // localStorage.removeItem('devmode');
        // window.location.href = "/login"
        
      }

      // console.log('resulterror = ', (<Error>error).message);
      let msg = error.error?(error.error.error?error.error.error: (<Error>error).message) : (<Error>error).message;
      
      throw new Error(JSON.stringify(msg, null, 2));
      // result = {error:error.error.error,error_message: error.error.error}
    }

    return result;
  }

  public async update(_params, _url, headers?: any) {

    let result = null;
    try {
      result = await this.updateRest(_params, _url, headers).toPromise();
    } catch (error) {
      this.checkStatus(error);
      if(error.status == 401){
        localStorage.removeItem('isLoggedin');
        localStorage.removeItem('tokenlogin');
        localStorage.removeItem('devmode');
        // window.location.href = "/login"
        
      }
      let msg = error.error?(error.error.error?error.error.error: (<Error>error).message) : (<Error>error).message;
      
      throw new Error(JSON.stringify(msg, null, 2));
    }

    return result;
  }

  public async delete(_params, _url, headers?: any) {

    let result = null;
    try {
      result = await this.deleteRest(_params, _url, headers).toPromise();
    } catch (error) {
      this.checkStatus(error);
      if(error.status == 401){
        localStorage.removeItem('isLoggedin');
        localStorage.removeItem('tokenlogin');
        localStorage.removeItem('devmode');
        // window.location.href = "/login"
        
      }
      let msg = error.error?(error.error.error?error.error.error: (<Error>error).message) : (<Error>error).message;
      
      throw new Error(JSON.stringify(msg, null, 2));
    }
    return result;
  }

  private getRest(_params, _url, additionalHeaders?: any): Observable<Result[]> {

    const uri = this.checkingUrl(_url);

    let   httpParams = new HttpParams();
    const arrayParams= Object.assign({}, _params);
          httpParams = httpParams.append('request', JSON.stringify(arrayParams));

    let headerParams = {
      'Content-Type': 'application/json',
    };

    if (additionalHeaders) {
      headerParams = {...headerParams, ...additionalHeaders};
    }

    const httpOpt = {
      headers: new HttpHeaders(headerParams) ,
      params : {}
    };

    if (_params) {
      httpOpt.params = _params;
    }
// console.log('Params 1 : ', httpOpt.params)
// console.log('Params 2 : ', httpParams)
    
    return this.http.get<Result[]>(uri, httpOpt);
  }

  private postRest(_params, _url, additionalHeaders?: any): Observable<Result[]> {

    const uri         = this.checkingUrl(_url);
    let   httpParams  = new HttpParams();
    const arrayParams = Object.assign({}, _params);

    httpParams = httpParams.append('request', JSON.stringify(arrayParams));

    let headerParams = {
      'Content-Type': 'application/json',
    };

    if (additionalHeaders) {
      headerParams = {...headerParams, ...additionalHeaders};
    }

    const httpOpt = {
      headers: new HttpHeaders(headerParams),
      // params  : httpParams,
    };
    // console.log('headers : ', headerParams);
    // console.log('url : ', uri);
    return this.http.post<Result[]>(uri, arrayParams, httpOpt);

  }
  private updateRest(_params, _url, additionalHeaders?: any): Observable<Result[]> {

    const uri = this.checkingUrl(_url);

    let httpParams = new HttpParams();
    const arrayParams = Object.assign({}, _params);
    httpParams = httpParams.append('request', JSON.stringify(arrayParams));

    let headerParams = {
      'Content-Type': 'application/json',
    };

    if (additionalHeaders) {
      headerParams = {...headerParams, ...additionalHeaders};
    }

    const httpOpt = {
      headers: new HttpHeaders(headerParams),
      // params  : httpParams
    };

    return this.http.put<Result[]>(uri, arrayParams, httpOpt);
  }

  private deleteRest(_params, _url, additionalHeaders?: any): Observable<Result[]> {

    const uri = this.checkingUrl(_url);

    let httpParams = new HttpParams();
    const arrayParams = Object.assign({}, _params);
    httpParams = httpParams.append('request', JSON.stringify(arrayParams));

    let headerParams = {
      'Content-Type': 'application/json',
    };

    if (additionalHeaders) {
      headerParams = {...headerParams, ...additionalHeaders};
    }

    const httpOpt = {
      headers: new HttpHeaders(headerParams),
      params: httpParams
    };

    return this.http.delete<Result[]>(uri, httpOpt);
  }

  private searchResultRest(_params, _url, additionalHeaders?: any): Observable<Result[]> {

    const uri = this.checkingUrl(_url);

    let httpParams = new HttpParams();
    _params.search = Object.assign({}, _params.search);
    _params.order_by = Object.assign({}, _params.order_by);

    const arrayParams = Object.assign({}, _params);
    httpParams = httpParams.append('request', JSON.stringify(arrayParams));
    let headerParams = {
      'Content-Type': 'application/json',
    };

    if (additionalHeaders) {
      headerParams = {...headerParams, ...additionalHeaders};
    }

    const httpOpt = {
      headers: new HttpHeaders(headerParams),
      params: httpParams
    };

    return this.http.get<Result[]>(uri, httpOpt);
  }

  public async searchResult(_params, _url, additionalHeaders?: any) {
    let result;
    try {
      result = await this.searchResultRest(_params, _url, additionalHeaders).toPromise();
      console.log(result)
    } catch (error) {
      console.log(error)
      this.checkStatus(error);
      let msg = error.error?(error.error.error?error.error.error: (<Error>error).message) : (<Error>error).message;
      
      throw new Error(JSON.stringify(msg, null, 2));
    }
    return result;
  }

  private checkStatus(error) {
    if (error.status == 401) {
      //localStorage.removeItem('tokenlogin')
      //window.location.reload()
    }
  }

}
