import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { BehaviorSubject, Observable, of } from 'rxjs';
import * as content from '../../assets/json/content.json';

@Injectable()
export class ProgramNameService {
    public programName$: BehaviorSubject<string> = new BehaviorSubject<string>("");
    public contentList : any = (content as any).default;

    constructor(private titleService: Title) {
    }

    currentData() : Observable<string> {
        return this.programName$.asObservable();
    }

    public setData(program:any) {
        if(program) localStorage.setItem("programName",program);

        let _this = this;

        this.contentList.filter(function(element) {
            if(element.appLabel == program) {
                _this.changeName(element.title);
            }
        });
    }

    public changeName(value) {
        this.programName$.next(value);
        this.titleService.setTitle(value);
    }
}