import { Injectable, Component, OnInit } from '@angular/core';

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { api_url, getTokenHeader } from '../../../environments/environment';

import { MasterService } from '../master.service';

interface Courierservicepage {
  error: any,
  result: any
}

@Injectable({
  providedIn: 'root'
})

export class CourierServicePageService {
  courses$: Observable<Courierservicepage[]>;

  api_url : string = "";

  constructor(private http: HttpClient, public myService: MasterService) {
    this.api_url = api_url();

   }

   getCourierservicepage(params): Observable<Courierservicepage[]>{
    // const uri = 'http://192.168.1.198/f3/v1/orderhistory/all';
    // console.log(uri);
    // return this.http.get<Orderhistory[]>(uri);
    let myToken = localStorage.getItem('tokenlogin');
    let myStatus = localStorage.getItem('isLoggedin');

    if (myStatus == 'true') {
      try {
        if (params) {

          let httpParams = new HttpParams();
          params.search = Object.assign({}, params.search);
          params.order_by = Object.assign({}, params.order_by);
          let arrayParams = Object.assign({}, params);
          httpParams = httpParams.append('request', JSON.stringify(arrayParams));

          const httpOpt = {
            headers: new HttpHeaders(
              {
                'Content-Type': 'application/json',
                'Authorization': myToken,
              }
            ),
            params: httpParams
          };

          const uri = this.api_url + 'courier-services/summary';
          console.log("URI courier service page", uri)
          return this.http.get<Courierservicepage[]>(uri, httpOpt);
        }
      } catch (error) {
        throw new TypeError(error);
      }
    } else {
      localStorage.removeItem('isLoggedin');
      window.location.reload();
    }
   }

  public async getCourierservicepageLint() {
    let result;

    const customHeaders = getTokenHeader();


    try {
      const url = 'courier-services/summary';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);
    return result;
  }

  public async searchCourierservicepageLint(params) {
    //console.log(params);
    let result;
    const customHeaders = getTokenHeader();

    try {
      const url = 'courier-services/summary';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async detailCourierservicepage(_id) {
    let result;
    const customHeaders = getTokenHeader();

    try {
      const url = 'courier-services/' + _id;
      // const url = 'courier-services/summary?request={"search": {"_id":"'+orderhistory_id+'"}}';
      // const url = 'https://api2.locard.co.id/f3/v1/courier-services/summary?request={"search": {"courier":"sap"}}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async updateCourierservicepage(params) {
    let result;
    const customHeaders = getTokenHeader();
    console.log(params);
    try {
      const url = 'courier-services/update/' + params._id;
      result = await this.myService.update(params, url, customHeaders);
      console.log(url, result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }
}
