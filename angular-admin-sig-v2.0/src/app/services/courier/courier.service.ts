import { Injectable, Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { api_url, getTokenHeader } from '../../../environments/environment';

import { MasterService } from '../master.service';

interface CourierService {
	error: any;
	result: any;
}

@Injectable({
	providedIn: 'root'
})
export class CourierServiceService {
	forEach(arg0: (el: any, i: any) => void) {
		throw new Error('Method not implemented.');
	}
	courses$: Observable<CourierService[]>;

	api_url = '';

	constructor(private http: HttpClient, public myService: MasterService) {
		this.api_url = api_url();
	}

	public async getCourier(params) {
		let result;
		// console.log(result);
		const customHeaders = getTokenHeader();

		try {
			const url = 'courier/list/supported';
			result = await this.myService.post(params, url, customHeaders);
		} catch (error) {
			throw new TypeError(error);
		}
		return result;
	}

	public async getCourierAll() {
		let result;
		// console.log(result);
		const customHeaders = getTokenHeader();

		try {
			const url = 'courier/list/all';
			result = await this.myService.get(null, url, customHeaders);
		} catch (error) {
			throw new TypeError(error);
		}
		return result;
	}
}
