import { Injectable,Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable , of} from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

interface Feeds {
  error: any,
  result: any
}

@Injectable({
  providedIn: 'root',
  
})

export class FeedService {
  courses$: Observable<Feeds[]>;

  constructor(private http: HttpClient) { }

  getFeeds():Observable<Feeds[]>{
    const uri = 'http://192.168.1.198/f3/v1/masterload/5';
    return this.http.get<Feeds[]>(uri);
  }
}
