import { Injectable, Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { api_url, getTokenHeader } from '../../../environments/environment';

import { MasterService } from '../master.service';

interface Payment {
  error: any;
  result: any;
}

@Injectable({
  providedIn: 'root',
})



export class PaymentService {
  courses$: Observable<Payment[]>;

  api_url = '';

  constructor(private http: HttpClient, public myService: MasterService) {
    this.api_url = api_url();

  }


public async getPaymentReportint(params) {
    let result;
    

    // {"search":{"type":"evoucher"},"order_by":{},"limit_per_page":50,"current_page":1}

    const customHeaders = getTokenHeader();
    try {
      const url = 'payment/report?request={"search":{},"limit_per_page":50,"current_page":1}'

      result = await this.myService.get(null, url, customHeaders);
      // console.log("resultnya om", result.result.values);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;

  }

  public async searchPaymentReportint(params){
    console.log(params)
    let result;
    const customHeaders = getTokenHeader();
    try{
      const url = 'payment/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

}