import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LibTabComponent } from './lib-tab/lib-tab.component';
// import { UploadComponent } from './upload/upload.component';

@NgModule({
  declarations: [
    LibTabComponent,
    // UploadComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    LibTabComponent
  ]
})
export class ComponentLibsModule { }
