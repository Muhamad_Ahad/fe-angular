import { Component, OnInit, Input, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'form-detail',
  templateUrl: './form-detail.component.html',
  styleUrls: ['./form-detail.component.scss'],
  providers:[
    { provide: NG_VALUE_ACCESSOR, 
      useExisting: FormDetailComponent, 
      multi:true}
  ]
})
export class FormDetailComponent implements ControlValueAccessor {

  select_value: string;
  @Input() data: any;
  inputType: string;

  constructor(){
   
  }

  private onChange:(value: string)=>void;

  whenItChange(){
    this.onChange(this.select_value);
  }

  writeValue(value: string){
    this.data.forEach((key,index) => {
      if(key.selected){
        this.select_value = key.value;
        return true;
      }
    });
    

    // this.select_value = value;
   
  }

  registerOnChange(onChange: (value: string)=> void){
    this.onChange = onChange;
    // todo
  }

  registerOnTouched(){}

  changePasswordType(){
    console.log(this.inputType);
  }

  
}
