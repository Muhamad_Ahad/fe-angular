export function api_url(){
    return getHost()+'v1/';
    // return 'http://localhost/locard/f3/v1/';
}

export function getHost(){
    if(localStorage.getItem('devmode')== 'active'){
        // return 'http://192.168.56.4/f3/';
        return 'https://api2.dev.locard.co.id/f3/';
        
    }
    else{
        return 'https://api2.locard.co.id/f3/';
    }
    //  return 'https://api2.locard.co.id/f3/';
    return 'http://192.168.56.4/f3/';
}

export function getAPIKeyHeader(){
    //Locard API Key
    let Api_Key = '100B14A363F34BF425ED44F0124C9D4A1D85A73A157D265160F720BB9B02CDF9'
    let customHeaders
    // return customHeaders

    if(localStorage.getItem('devmode') == 'active'){
         Api_Key = '41418C656335D6BA2B58C9F5B404CCF0B894070466F0D038D6B991F9F67B70B6'
         customHeaders = {'Api_key': Api_Key }
         return customHeaders
    } else {
         Api_Key = '100B14A363F34BF425ED44F0124C9D4A1D85A73A157D265160F720BB9B02CDF9'
        customHeaders = { 'Api_key': Api_Key } 
    } return customHeaders
}


export function getTokenHeader(){
    const myToken = localStorage.getItem('tokenlogin')
    const customHeaders = { 'Authorization': myToken }
    return customHeaders
}

export function errorConvertToMessage(errorMessage: string){
    errorMessage = errorMessage.replace("Error:", "");
    let errorJSON:any = JSON.parse(errorMessage);
    return errorJSON.error;
}
