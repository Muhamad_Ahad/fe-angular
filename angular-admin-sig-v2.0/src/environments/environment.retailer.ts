export const environment = {
    production: true
  };
  
  export function api_url(){
    return getHost()+'v1/';
    // return 'http://localhost/locard/f3/v1/';
  }
  
  export function getHost(){
    if(localStorage.getItem('devmode')== 'active'){
        // return 'http://192.168.56.4/f3/';
        return 'https://api2.dev.locard.co.id/f3/';
        
    }
    else{
        return 'https://api2.locard.co.id/f3/';
    }
    //  return 'https://api2.locard.co.id/f3/';
    return 'https://192.168.56.4/f3/';
  }
  
  export function getAPIKeyHeader(){
    //Locard API Key
    let Api_Key = '87E12038912E1E8387F99677EA16387B6FB6DE93929055D519561DB4AD5149A6'
    let customHeaders
    // return customHeaders
  
    if(localStorage.getItem('devmode') == 'active'){
         Api_Key = '87E12038912E1E8387F99677EA16387B6FB6DE93929055D519561DB4AD5149A6'
         customHeaders = {'Api_key': Api_Key }
         return customHeaders
    } else {
         Api_Key = '87E12038912E1E8387F99677EA16387B6FB6DE93929055D519561DB4AD5149A6'
        customHeaders = { 'Api_key': Api_Key } 
    } return customHeaders
  }
  
  
  export function getTokenHeader(){
    const myToken = localStorage.getItem('tokenlogin')
    const customHeaders = { 'Authorization': myToken }
    return customHeaders
  }
  
  export function errorConvertToMessage(errorMessage: string){
    errorMessage = errorMessage.replace("Error:", "");
    let errorJSON:any = JSON.parse(errorMessage);
    return errorJSON.error;
  }
  